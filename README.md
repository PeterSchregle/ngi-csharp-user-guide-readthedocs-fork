Building the nGI User Guide
---------------------------

Link to the nGI User Guide: [http://ngi-csharp-user-guide-readthedocs-fork.readthedocs.io/](http://ngi-csharp-user-guide-readthedocs-fork.readthedocs.io/)

## Tools

[Markdown](http://daringfireball.net/projects/markdown/ "Markdown") is used to write the documentation.

[Bitbucket](http://bitbucket.org "Bitbucket") is used to store the documentation sources in a public repository.

[ReadTheDocs](http://readthedocs.org "ReadTheDocs") is used to build and serve the built documenation to the world.

## Process 

1. Documentation is changed in the private [nvision-user-guide](https://bitbucket.org/impuls-imaging/nvision-user-guide.git "nvision-user-guide") repository and committed and pushed.
2. The public fork [nvision-user-guide-fork](http://https://PeterSchregle@bitbucket.org/PeterSchregle/nvision-user-guide-fork.git "nvision-user-guide-fork") is synced manually.
3. Bitbucket notifies ReadTheDocs because the ngi-user-guide-fork directory has set a ReadTheDocs hook.
4. ReadTheDocs builds the documentation and serves it at [http://nvision-user-guide.readthedocs.org/](http://nvision-user-guide.readthedocs.org/ "nvision-user-guide.readthedocs.org").

## Setup

### Setup Mkdocs

Follow the instructions at [http://www.mkdocs.org](http://www.mkdocs.org) if you want to build documentation locally.

### Repositories

The private repository for the documentation is 

	https://PeterSchregle@bitbucket.org/PeterSchregle/nvision-user-guide.git

A public fork of it is

	http://bitbucket.org/PeterSchregle/nvision-user-guide-fork.git

ReadTheDocs needs public access so that it can grab the files. The esiest way I found was to create a fork with public access.

### Setup ReadTheDocs

The important setting is the repository:

	http://bitbucket.org/PeterSchregle/nvision-user-guide-fork.git
