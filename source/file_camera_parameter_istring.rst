Class *CameraParameterIstring*
------------------------------

**Namespace:** Ngi

**Module:** Camera

The class **CameraParameterIstring** contains the following properties:

+-----------------+-------+-------+---------------+
| Property        | Get   | Set   | Description   |
+=================+=======+=======+===============+
| ``Value``       | \*    | \*    |               |
+-----------------+-------+-------+---------------+
| ``MaxLength``   | \*    |       |               |
+-----------------+-------+-------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Value*
^^^^^^^^^^^^^^^^

``System.String Value``

Property *MaxLength*
^^^^^^^^^^^^^^^^^^^^

``System.Int64 MaxLength``
