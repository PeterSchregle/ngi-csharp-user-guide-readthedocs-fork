Class *WidgetVerticalCursor*
----------------------------

A widget\_vertical\_cursor draws a vertical line.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetVerticalCursor** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetVerticalCursor** contains the following properties:

+----------------+-------+-------+-----------------------------------------------------+
| Property       | Get   | Set   | Description                                         |
+================+=======+=======+=====================================================+
| ``Position``   | \*    | \*    | The position (horizontal location) of the cursor.   |
+----------------+-------+-------+-----------------------------------------------------+
| ``Pen``        | \*    | \*    | The pen of the cursor.                              |
+----------------+-------+-------+-----------------------------------------------------+

Description
~~~~~~~~~~~

It can be positioned on some surface. It provides interaction, i.e. can be selected, move around. In general it can react on mouse and keyboard interactions. A widget\_vertical\_cursor has interactive widget capabilities.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetVerticalCursor*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetVerticalCursor()``

Default constructor.

The default constructor creates a cursor with default settings. By default the cursor is at the origin and the line is one-pixel wide black.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``System.Double Position``

The position (horizontal location) of the cursor.

Property *Pen*
^^^^^^^^^^^^^^

``PenByte Pen``

The pen of the cursor.
