Class *IsotropicScalingMatrix*
------------------------------

A matrix useful for affine 2D geometric scaling.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **IsotropicScalingMatrix** implements the following interfaces:

+----------------------------------------+
| Interface                              |
+========================================+
| ``IEquatableIsotropicScalingMatrix``   |
+----------------------------------------+
| ``ISerializable``                      |
+----------------------------------------+

The class **IsotropicScalingMatrix** contains the following properties:

+-------------------+-------+-------+----------------------------------------------------+
| Property          | Get   | Set   | Description                                        |
+===================+=======+=======+====================================================+
| ``Scaling``       | \*    | \*    | The scaling factor of the matrix.                  |
+-------------------+-------+-------+----------------------------------------------------+
| ``M11``           | \*    |       | The element m11 (row 1, column 1) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M12``           | \*    |       | The element m12 (row 1, column 2) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M13``           | \*    |       | The element m13 (row 1, column 3) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M21``           | \*    |       | The element m21 (row 2, column 1) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M22``           | \*    |       | The element m22 (row 2, column 2) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M23``           | \*    |       | The element m23 (row 2, column 3) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M31``           | \*    |       | The element m31 (row 3, column 1) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M32``           | \*    |       | The element m32 (row 3, column 2) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M33``           | \*    |       | The element m33 (row 3, column 3) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``Determinant``   | \*    |       | The determinant of the matrix.                     |
+-------------------+-------+-------+----------------------------------------------------+
| ``Trace``         | \*    |       | The trace of the matrix.                           |
+-------------------+-------+-------+----------------------------------------------------+
| ``Inverse``       | \*    |       | The inverse of the matrix.                         |
+-------------------+-------+-------+----------------------------------------------------+
| ``[index]``       | \*    |       | The matrix values as a parameterized property.     |
+-------------------+-------+-------+----------------------------------------------------+

The class **IsotropicScalingMatrix** contains the following methods:

+-----------------------+--------------------------------------------+
| Method                | Description                                |
+=======================+============================================+
| ``ResetToIdentity``   | Reset the matrix to the identity matrix.   |
+-----------------------+--------------------------------------------+
| ``Scale``             | Scale the matrix in place.                 |
+-----------------------+--------------------------------------------+

Description
~~~~~~~~~~~

The values in the matrix are arranged like this:

\| \| \| m11 m12 m13 \|

.. raw:: html

   <table rows="7" cols="1">

scaling\_ 0 0

m21 m22 m23

0 scaling\_ 0

m31 m32 m33

0 0 1

.. raw:: html

   </table>

The cells with 0 or 1 values are not stored within this object but implicit.

The determinant of such a matrix is the product of the two scaling factors.

The trace of such a matrix is the sum of the two scaling factors plus 1.

The size is implicitely 3x3 and related properties are not implemented.

The inverse can be quickly calculated by inverting the scaling factors, and the result is still a scaling matrix.

The transpose of a scaling matrix is identical to the original, so this is not implemented.

This storage assumes row-vectors and post-multiplication for transformations.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *IsotropicScalingMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``IsotropicScalingMatrix()``

Default constructor.

By default this is the identity matrix.

Constructors
~~~~~~~~~~~~

Constructor *IsotropicScalingMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``IsotropicScalingMatrix(System.Double scaling)``

Constructor.

The constructor has the following parameters:

+---------------+---------------------+-----------------------+
| Parameter     | Type                | Description           |
+===============+=====================+=======================+
| ``scaling``   | ``System.Double``   | The scaling factor.   |
+---------------+---------------------+-----------------------+

Constructs a scaling matrix that scales by the given factor in both dimensions.

Properties
~~~~~~~~~~

Property *Scaling*
^^^^^^^^^^^^^^^^^^

``System.Double Scaling``

The scaling factor of the matrix.

Property *M11*
^^^^^^^^^^^^^^

``System.Double M11``

The element m11 (row 1, column 1) of the matrix.

Property *M12*
^^^^^^^^^^^^^^

``System.Double M12``

The element m12 (row 1, column 2) of the matrix.

Property *M13*
^^^^^^^^^^^^^^

``System.Double M13``

The element m13 (row 1, column 3) of the matrix.

Property *M21*
^^^^^^^^^^^^^^

``System.Double M21``

The element m21 (row 2, column 1) of the matrix.

Property *M22*
^^^^^^^^^^^^^^

``System.Double M22``

The element m22 (row 2, column 2) of the matrix.

Property *M23*
^^^^^^^^^^^^^^

``System.Double M23``

The element m23 (row 2, column 3) of the matrix.

Property *M31*
^^^^^^^^^^^^^^

``System.Double M31``

The element m31 (row 3, column 1) of the matrix.

Property *M32*
^^^^^^^^^^^^^^

``System.Double M32``

The element m32 (row 3, column 2) of the matrix.

Property *M33*
^^^^^^^^^^^^^^

``System.Double M33``

The element m33 (row 3, column 3) of the matrix.

Property *Determinant*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double Determinant``

The determinant of the matrix.

Property *Trace*
^^^^^^^^^^^^^^^^

``System.Double Trace``

The trace of the matrix.

Property *Inverse*
^^^^^^^^^^^^^^^^^^

``IsotropicScalingMatrix Inverse``

The inverse of the matrix.

Property *[index]*
^^^^^^^^^^^^^^^^^^

``System.Double [index]``

The matrix values as a parameterized property.

This property allows you to retrieve the implicit values also, but you cannot set them.

Static Methods
~~~~~~~~~~~~~~

Method *Multiply*
^^^^^^^^^^^^^^^^^

``IsotropicScalingMatrix Multiply(IsotropicScalingMatrix a, IsotropicScalingMatrix b)``

Multiply two matrices.

The method **Multiply** has the following parameters:

+-------------+------------------------------+---------------+
| Parameter   | Type                         | Description   |
+=============+==============================+===============+
| ``a``       | ``IsotropicScalingMatrix``   |               |
+-------------+------------------------------+---------------+
| ``b``       | ``IsotropicScalingMatrix``   |               |
+-------------+------------------------------+---------------+

The product.

Methods
~~~~~~~

Method *ResetToIdentity*
^^^^^^^^^^^^^^^^^^^^^^^^

``void ResetToIdentity()``

Reset the matrix to the identity matrix.

The identity matrix has the following form. \| 1 0 0 \| \| 0 1 0 \| \| 0 0 1 \|A reference to this object.

Method *Scale*
^^^^^^^^^^^^^^

``void Scale(System.Double scaling)``

Scale the matrix in place.

The method **Scale** has the following parameters:

+---------------+---------------------+-----------------------+
| Parameter     | Type                | Description           |
+===============+=====================+=======================+
| ``scaling``   | ``System.Double``   | The scaling factor.   |
+---------------+---------------------+-----------------------+

Return a reference to the matrix.
