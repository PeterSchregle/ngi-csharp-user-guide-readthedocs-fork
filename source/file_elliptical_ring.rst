Class *EllipticalRing*
----------------------

The geometric elliptical\_ring primitive.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **EllipticalRing** implements the following interfaces:

+--------------------------------+
| Interface                      |
+================================+
| ``IEquatableEllipticalRing``   |
+--------------------------------+
| ``ISerializable``              |
+--------------------------------+
| ``INotifyPropertyChanged``     |
+--------------------------------+

The class **EllipticalRing** contains the following properties:

+---------------------+-------+-------+-------------------------------------------------+
| Property            | Get   | Set   | Description                                     |
+=====================+=======+=======+=================================================+
| ``Ellipse``         | \*    | \*    | The position of the elliptical\_ring.           |
+---------------------+-------+-------+-------------------------------------------------+
| ``Thickness``       | \*    | \*    | The thickness of the elliptical\_ring.          |
+---------------------+-------+-------+-------------------------------------------------+
| ``InnerEllipse``    | \*    |       | The inner ellipse of the elliptical\_ring.      |
+---------------------+-------+-------+-------------------------------------------------+
| ``OuterEllipse``    | \*    |       | The outer ellipse of the elliptical\_ring.      |
+---------------------+-------+-------+-------------------------------------------------+
| ``Area``            | \*    |       | The area of the elliptical\_ring.               |
+---------------------+-------+-------+-------------------------------------------------+
| ``Circumference``   | \*    |       | The circumference of the elliptical\_ring.      |
+---------------------+-------+-------+-------------------------------------------------+
| ``BoundingBox``     | \*    |       | Get the bounding box of the elliptical\_ring.   |
+---------------------+-------+-------+-------------------------------------------------+

The class **EllipticalRing** contains the following methods:

+------------------------+----------------------------------------------------------------------------+
| Method                 | Description                                                                |
+========================+============================================================================+
| ``Normalize``          | A normalized elliptical\_ring has positive radii and positive thickness.   |
+------------------------+----------------------------------------------------------------------------+
| ``NormalizeInPlace``   | A normalized elliptical\_ring has positive radii and positive thickness.   |
+------------------------+----------------------------------------------------------------------------+
| ``ToString``           | Provide string representation for debugging purposes.                      |
+------------------------+----------------------------------------------------------------------------+

Description
~~~~~~~~~~~

A elliptical\_ring can be seen as two ellipses with the same center but different radii.

The following operations are implemented: operator == : comparison for equality. operator != : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *EllipticalRing*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``EllipticalRing()``

Default constructor.

The default constructor builds a elliptical\_ring with a zero ellipse and thickness.

Constructors
~~~~~~~~~~~~

Constructor *EllipticalRing*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``EllipticalRing(Ellipse ellipse, System.Double thickness)``

Standard constructor.

The constructor has the following parameters:

+-----------------+---------------------+------------------+
| Parameter       | Type                | Description      |
+=================+=====================+==================+
| ``ellipse``     | ``Ellipse``         | The ellipse.     |
+-----------------+---------------------+------------------+
| ``thickness``   | ``System.Double``   | The thickness.   |
+-----------------+---------------------+------------------+

Construct a elliptical\_ring from its ellipse and thickness.

Properties
~~~~~~~~~~

Property *Ellipse*
^^^^^^^^^^^^^^^^^^

``Ellipse Ellipse``

The position of the elliptical\_ring.

Property *Thickness*
^^^^^^^^^^^^^^^^^^^^

``System.Double Thickness``

The thickness of the elliptical\_ring.

Property *InnerEllipse*
^^^^^^^^^^^^^^^^^^^^^^^

``Ellipse InnerEllipse``

The inner ellipse of the elliptical\_ring.

Property *OuterEllipse*
^^^^^^^^^^^^^^^^^^^^^^^

``Ellipse OuterEllipse``

The outer ellipse of the elliptical\_ring.

Property *Area*
^^^^^^^^^^^^^^^

``System.Double Area``

The area of the elliptical\_ring.

Property *Circumference*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double Circumference``

The circumference of the elliptical\_ring.

Property *BoundingBox*
^^^^^^^^^^^^^^^^^^^^^^

``BoxDouble BoundingBox``

Get the bounding box of the elliptical\_ring.

The bounding box is an axis aligned box that bounds the elliptical\_ring.

The bounding box of the elliptical\_ring.

Methods
~~~~~~~

Method *Normalize*
^^^^^^^^^^^^^^^^^^

``EllipticalRing Normalize()``

A normalized elliptical\_ring has positive radii and positive thickness.

This method checks these conditions and returns a normalized variant of the elliptical\_ring.

Method *NormalizeInPlace*
^^^^^^^^^^^^^^^^^^^^^^^^^

``void NormalizeInPlace()``

A normalized elliptical\_ring has positive radii and positive thickness.

This method checks these conditions and returns a normalized variant of the elliptical\_ring. It modifies the elliptical\_ring in place.

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO no brief description for variant

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO no description for variant
