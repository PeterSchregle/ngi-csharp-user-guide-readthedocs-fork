Class *IppException*
--------------------

The basic ipp exception type.

**Namespace:** Ngi

**Module:**

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *IppException*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``IppException()``

Constructor.
