Class *CameraPixelformatInfo*
-----------------------------

**Namespace:** Ngi

**Module:** Camera

The class **CameraPixelformatInfo** implements the following interfaces:

+---------------------------------------+
| Interface                             |
+=======================================+
| ``IEquatableCameraPixelformatInfo``   |
+---------------------------------------+

The class **CameraPixelformatInfo** contains the following properties:

+-------------------+-------+-------+-----------------------------------------------------+
| Property          | Get   | Set   | Description                                         |
+===================+=======+=======+=====================================================+
| ``Id``            | \*    |       | The numerical id of the pixelformat.                |
+-------------------+-------+-------+-----------------------------------------------------+
| ``Description``   | \*    |       | Human readable description of the pixelformat.      |
+-------------------+-------+-------+-----------------------------------------------------+
| ``IsColor``       | \*    |       | Indicate if this is a color or monochrome format.   |
+-------------------+-------+-------+-----------------------------------------------------+

The class **CameraPixelformatInfo** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``ToString``   |               |
+----------------+---------------+

The class **CameraPixelformatInfo** contains the following enumerations:

+-------------------+------------------------------+
| Enumeration       | Description                  |
+===================+==============================+
| ``Pixelformat``   | TODO documentation missing   |
+-------------------+------------------------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *CameraPixelformatInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``CameraPixelformatInfo()``

Constructors
~~~~~~~~~~~~

Constructor *CameraPixelformatInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``CameraPixelformatInfo(CameraPixelformatInfo.Pixelformat id, System.String description, System.Boolean isColor)``

The constructor has the following parameters:

+-------------------+-----------------------------------------+---------------+
| Parameter         | Type                                    | Description   |
+===================+=========================================+===============+
| ``id``            | ``CameraPixelformatInfo.Pixelformat``   | The id        |
+-------------------+-----------------------------------------+---------------+
| ``description``   | ``System.String``                       |               |
+-------------------+-----------------------------------------+---------------+
| ``isColor``       | ``System.Boolean``                      |               |
+-------------------+-----------------------------------------+---------------+

Properties
~~~~~~~~~~

Property *Id*
^^^^^^^^^^^^^

``CameraPixelformatInfo.Pixelformat Id``

The numerical id of the pixelformat.

Property *Description*
^^^^^^^^^^^^^^^^^^^^^^

``System.String Description``

Human readable description of the pixelformat.

Property *IsColor*
^^^^^^^^^^^^^^^^^^

``System.Boolean IsColor``

Indicate if this is a color or monochrome format.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Enumerations
~~~~~~~~~~~~

Enumeration *Pixelformat*
^^^^^^^^^^^^^^^^^^^^^^^^^

``enum Pixelformat``

TODO documentation missing

The enumeration **Pixelformat** has the following constants:

+--------------------+----------+---------------+
| Name               | Value    | Description   |
+====================+==========+===============+
| ``unknown``        | ``0``    |               |
+--------------------+----------+---------------+
| ``mono8``          | ``1``    |               |
+--------------------+----------+---------------+
| ``mono10``         | ``2``    |               |
+--------------------+----------+---------------+
| ``mono10Packed``   | ``3``    |               |
+--------------------+----------+---------------+
| ``mono12``         | ``4``    |               |
+--------------------+----------+---------------+
| ``mono12Packed``   | ``5``    |               |
+--------------------+----------+---------------+
| ``mono14``         | ``6``    |               |
+--------------------+----------+---------------+
| ``mono16``         | ``7``    |               |
+--------------------+----------+---------------+
| ``bgr8``           | ``8``    |               |
+--------------------+----------+---------------+
| ``rgb8``           | ``9``    |               |
+--------------------+----------+---------------+
| ``bgra8``          | ``10``   |               |
+--------------------+----------+---------------+
| ``bayerGr8``       | ``11``   |               |
+--------------------+----------+---------------+
| ``bayerRg8``       | ``12``   |               |
+--------------------+----------+---------------+
| ``bayerGb8``       | ``13``   |               |
+--------------------+----------+---------------+
| ``bayerBg8``       | ``14``   |               |
+--------------------+----------+---------------+
| ``bayerGr10``      | ``15``   |               |
+--------------------+----------+---------------+
| ``bayerRg10``      | ``16``   |               |
+--------------------+----------+---------------+
| ``bayerGb10``      | ``17``   |               |
+--------------------+----------+---------------+
| ``bayerBg10``      | ``18``   |               |
+--------------------+----------+---------------+
| ``bayerGr12``      | ``19``   |               |
+--------------------+----------+---------------+
| ``bayerRg12``      | ``20``   |               |
+--------------------+----------+---------------+
| ``bayerGb12``      | ``21``   |               |
+--------------------+----------+---------------+
| ``bayerBg12``      | ``22``   |               |
+--------------------+----------+---------------+
| ``bayerGr14``      | ``23``   |               |
+--------------------+----------+---------------+
| ``bayerRg14``      | ``24``   |               |
+--------------------+----------+---------------+
| ``bayerGb14``      | ``25``   |               |
+--------------------+----------+---------------+
| ``bayerBg14``      | ``26``   |               |
+--------------------+----------+---------------+
| ``bayerGr16``      | ``27``   |               |
+--------------------+----------+---------------+
| ``bayerRg16``      | ``28``   |               |
+--------------------+----------+---------------+
| ``bayerGb16``      | ``29``   |               |
+--------------------+----------+---------------+
| ``bayerBg16``      | ``30``   |               |
+--------------------+----------+---------------+

::

    enum Pixelformat
    {
      unknown = 0,
      mono8 = 1,
      mono10 = 2,
      mono10Packed = 3,
      mono12 = 4,
      mono12Packed = 5,
      mono14 = 6,
      mono16 = 7,
      bgr8 = 8,
      rgb8 = 9,
      bgra8 = 10,
      bayerGr8 = 11,
      bayerRg8 = 12,
      bayerGb8 = 13,
      bayerBg8 = 14,
      bayerGr10 = 15,
      bayerRg10 = 16,
      bayerGb10 = 17,
      bayerBg10 = 18,
      bayerGr12 = 19,
      bayerRg12 = 20,
      bayerGb12 = 21,
      bayerBg12 = 22,
      bayerGr14 = 23,
      bayerRg14 = 24,
      bayerGb14 = 25,
      bayerBg14 = 26,
      bayerGr16 = 27,
      bayerRg16 = 28,
      bayerGb16 = 29,
      bayerBg16 = 30,
    };

TODO documentation missing
