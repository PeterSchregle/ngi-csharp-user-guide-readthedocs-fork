Class *FreemanCodeEnumerator*
-----------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **FreemanCodeEnumerator** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``IEnumerator``              |
+------------------------------+
| ``IEnumeratorFreemanCode``   |
+------------------------------+

The class **FreemanCodeEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **FreemanCodeEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``FreemanCode Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
