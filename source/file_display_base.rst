Class *DisplayBase*
-------------------

The display\_base type.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **DisplayBase** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **DisplayBase** contains the following properties:

+-----------------------------+-------+-------+---------------------------------------------------------------------+
| Property                    | Get   | Set   | Description                                                         |
+=============================+=======+=======+=====================================================================+
| ``Graphics``                | \*    |       | The graphics context.                                               |
+-----------------------------+-------+-------+---------------------------------------------------------------------+
| ``Background``              | \*    | \*    | The background color.                                               |
+-----------------------------+-------+-------+---------------------------------------------------------------------+
| ``ScrollbarMode``           | \*    | \*    | The scrollbar mode.                                                 |
+-----------------------------+-------+-------+---------------------------------------------------------------------+
| ``ScrollRange``             | \*    |       |                                                                     |
+-----------------------------+-------+-------+---------------------------------------------------------------------+
| ``Cursor``                  | \*    | \*    | The cursor.                                                         |
+-----------------------------+-------+-------+---------------------------------------------------------------------+
| ``SizeToContent``           | \*    | \*    | If true the window size is determined by the content size.          |
+-----------------------------+-------+-------+---------------------------------------------------------------------+
| ``Zoom``                    | \*    | \*    | The zoom factor.                                                    |
+-----------------------------+-------+-------+---------------------------------------------------------------------+
| ``InteractiveZoom``         | \*    | \*    | Specifies if the widget supports interactive zoom.                  |
+-----------------------------+-------+-------+---------------------------------------------------------------------+
| ``InteractiveZoomFactor``   | \*    | \*    | Specifies the interactive zoom factor.                              |
+-----------------------------+-------+-------+---------------------------------------------------------------------+
| ``ZoomResetCharacter``      | \*    | \*    | This is the key code used to interactively reset the zoom factor.   |
+-----------------------------+-------+-------+---------------------------------------------------------------------+
| ``ZoomInCharacter``         | \*    | \*    | This is the key code used to interactively zoom in.                 |
+-----------------------------+-------+-------+---------------------------------------------------------------------+
| ``ZoomOutCharacter``        | \*    | \*    | This is the key code used to interactively zoom out.                |
+-----------------------------+-------+-------+---------------------------------------------------------------------+
| ``Scroll``                  | \*    | \*    | The scroll position.                                                |
+-----------------------------+-------+-------+---------------------------------------------------------------------+
| ``ClientBox``               | \*    |       | Get the client box in window coordinates.                           |
+-----------------------------+-------+-------+---------------------------------------------------------------------+
| ``WindowBox``               | \*    |       | Get the window box in screen coordinates.                           |
+-----------------------------+-------+-------+---------------------------------------------------------------------+
| ``RenderedImage``           | \*    |       | Get the rendered image.                                             |
+-----------------------------+-------+-------+---------------------------------------------------------------------+

The class **DisplayBase** contains the following methods:

+------------------------+----------------------------------+
| Method                 | Description                      |
+========================+==================================+
| ``UpdateNow``          |                                  |
+------------------------+----------------------------------+
| ``ZoomAboutCenter``    | Zoom about window center.        |
+------------------------+----------------------------------+
| ``ZoomAboutPoint``     | Zoom about an arbitrary point.   |
+------------------------+----------------------------------+
| ``ZoomToFit``          | Zoom to fit window size.         |
+------------------------+----------------------------------+
| ``Resize``             | Resize the window.               |
+------------------------+----------------------------------+
| ``ReparentChildren``   |                                  |
+------------------------+----------------------------------+

The class **DisplayBase** contains the following enumerations:

+--------------------------+------------------------------+
| Enumeration              | Description                  |
+==========================+==============================+
| ``ScrollbarModeFlags``   | TODO documentation missing   |
+--------------------------+------------------------------+
| ``ZoomToFitMode``        | TODO documentation missing   |
+--------------------------+------------------------------+

Description
~~~~~~~~~~~

The display\_base type has move-semantics. Support layout. Prohibit copying.

Constructors
~~~~~~~~~~~~

Constructor *DisplayBase*
^^^^^^^^^^^^^^^^^^^^^^^^^

``DisplayBase(System.String title)``

Constructs a top-level window (with title bar and frame).

The constructor has the following parameters:

+-------------+---------------------+---------------------------+
| Parameter   | Type                | Description               |
+=============+=====================+===========================+
| ``title``   | ``System.String``   | The title of the window   |
+-------------+---------------------+---------------------------+

The size of the window is determined by measuring its content.

A real operating system window has been created after this constructor has returned.

Constructor *DisplayBase*
^^^^^^^^^^^^^^^^^^^^^^^^^

``DisplayBase(System.String title, System.Boolean visible)``

Constructs a top-level window (with title bar and frame).

The constructor has the following parameters:

+---------------+----------------------+---------------------------+
| Parameter     | Type                 | Description               |
+===============+======================+===========================+
| ``title``     | ``System.String``    | The title of the window   |
+---------------+----------------------+---------------------------+
| ``visible``   | ``System.Boolean``   |                           |
+---------------+----------------------+---------------------------+

The size of the window needs to be specified with a parameter.

A real operating system window has been created after this constructor has returned.

Constructor *DisplayBase*
^^^^^^^^^^^^^^^^^^^^^^^^^

``DisplayBase(VectorInt32 size, System.String title)``

Constructs a child window.

The constructor has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``size``    | ``VectorInt32``     |               |
+-------------+---------------------+---------------+
| ``title``   | ``System.String``   |               |
+-------------+---------------------+---------------+

A real operating system window has been created after this constructor has returned.

Constructor *DisplayBase*
^^^^^^^^^^^^^^^^^^^^^^^^^

``DisplayBase(VectorInt32 size, System.String title, System.Boolean visible)``

Constructs a child window.

The constructor has the following parameters:

+---------------+----------------------+-------------------------+
| Parameter     | Type                 | Description             |
+===============+======================+=========================+
| ``size``      | ``VectorInt32``      |                         |
+---------------+----------------------+-------------------------+
| ``title``     | ``System.String``    |                         |
+---------------+----------------------+-------------------------+
| ``visible``   | ``System.Boolean``   | The window visiblity.   |
+---------------+----------------------+-------------------------+

A real operating system window has been created after this constructor has returned.

Constructor *DisplayBase*
^^^^^^^^^^^^^^^^^^^^^^^^^

``DisplayBase(System.IntPtr parent, BoxInt32 position)``

Constructs a window by subclassing an existing operating system window.

The constructor has the following parameters:

+----------------+---------------------+---------------+
| Parameter      | Type                | Description   |
+================+=====================+===============+
| ``parent``     | ``System.IntPtr``   |               |
+----------------+---------------------+---------------+
| ``position``   | ``BoxInt32``        |               |
+----------------+---------------------+---------------+

Constructor *DisplayBase*
^^^^^^^^^^^^^^^^^^^^^^^^^

``DisplayBase(System.IntPtr parent, BoxInt32 position, System.Boolean visible)``

Constructs an offscreen display.

The constructor has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``parent``     | ``System.IntPtr``    |               |
+----------------+----------------------+---------------+
| ``position``   | ``BoxInt32``         |               |
+----------------+----------------------+---------------+
| ``visible``    | ``System.Boolean``   |               |
+----------------+----------------------+---------------+

The content can be accessed via get\_offscreen\_image();

Constructor *DisplayBase*
^^^^^^^^^^^^^^^^^^^^^^^^^

``DisplayBase(System.IntPtr window)``

The constructor has the following parameters:

+--------------+---------------------+---------------+
| Parameter    | Type                | Description   |
+==============+=====================+===============+
| ``window``   | ``System.IntPtr``   |               |
+--------------+---------------------+---------------+

Constructor *DisplayBase*
^^^^^^^^^^^^^^^^^^^^^^^^^

``DisplayBase(VectorInt32 size)``

The constructor has the following parameters:

+-------------+-------------------+---------------+
| Parameter   | Type              | Description   |
+=============+===================+===============+
| ``size``    | ``VectorInt32``   |               |
+-------------+-------------------+---------------+

Properties
~~~~~~~~~~

Property *Graphics*
^^^^^^^^^^^^^^^^^^^

``GraphicsContext Graphics``

The graphics context.

Property *Background*
^^^^^^^^^^^^^^^^^^^^^

``RgbByte Background``

The background color.

Property *ScrollbarMode*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.Int32 ScrollbarMode``

The scrollbar mode.

Property *ScrollRange*
^^^^^^^^^^^^^^^^^^^^^^

``VectorDouble ScrollRange``

Property *Cursor*
^^^^^^^^^^^^^^^^^

``System.Int32 Cursor``

The cursor.

Property *SizeToContent*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean SizeToContent``

If true the window size is determined by the content size.

Property *Zoom*
^^^^^^^^^^^^^^^

``VectorDouble Zoom``

The zoom factor.

Property *InteractiveZoom*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean InteractiveZoom``

Specifies if the widget supports interactive zoom.

If this is true, the widget reacts to keyboard messages and the mouse wheel in order to zoom in or out.

The keyboard keys by default are the ESC key to reset the zoom factor, the + key to zoom in and the - key to zoom out. The keys can be changed with the zoom\_reset\_character, zoom\_in\_character and zoom\_out\_character properties.

Property *InteractiveZoomFactor*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double InteractiveZoomFactor``

Specifies the interactive zoom factor.

This is the factor that is used to multiplicatively increment the zoom factor when zooming in, and multiplicatively decrement the zoom factor when zooming out.

Property *ZoomResetCharacter*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Int32 ZoomResetCharacter``

This is the key code used to interactively reset the zoom factor.

By default, this is the ESC key (== 27).

This key only works if the interactive\_zoom property is set to true.

Property *ZoomInCharacter*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Int32 ZoomInCharacter``

This is the key code used to interactively zoom in.

By default, this is the + key (== '+').

This key only works if the interactive\_zoom property is set to true.

Property *ZoomOutCharacter*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Int32 ZoomOutCharacter``

This is the key code used to interactively zoom out.

By default, this is the - key (== '-').

This key only works if the interactive\_zoom property is set to true.

Property *Scroll*
^^^^^^^^^^^^^^^^^

``VectorDouble Scroll``

The scroll position.

This is the vector from the origin of the display coordinate system to the origin of the window coordinate system in window coordinates.

Property *ClientBox*
^^^^^^^^^^^^^^^^^^^^

``BoxInt32 ClientBox``

Get the client box in window coordinates.

The client coordinates specify the upper-left and lower-right corners of the client area. Because client coordinates are relative to the upper-left corner of a window's client area, the coordinates of the upper-left corner are (0,0).

Property *WindowBox*
^^^^^^^^^^^^^^^^^^^^

``BoxInt32 WindowBox``

Get the window box in screen coordinates.

The coordinates are given in screen coordinates that are relative to the upper-left corner of the screen.

Property *RenderedImage*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte RenderedImage``

Get the rendered image.

If this window is an off-screen window, the graphics is rendered and the resulting bitmap is read back from the graphics.

If this window is an on-screen window, a temporary off-screen graphics context is created and this window's scene is rendered into it. Then, this rendered image is read back from the temporary graphics.

In both cases, the rendered window contents is returned in the form of a bitmap.

Methods
~~~~~~~

Method *UpdateNow*
^^^^^^^^^^^^^^^^^^

``void UpdateNow()``

Method *ZoomAboutCenter*
^^^^^^^^^^^^^^^^^^^^^^^^

``void ZoomAboutCenter(VectorDouble zoom)``

Zoom about window center.

The method **ZoomAboutCenter** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``zoom``    | ``VectorDouble``   |               |
+-------------+--------------------+---------------+

This affects both the zoom factors and the scroll position.

Method *ZoomAboutPoint*
^^^^^^^^^^^^^^^^^^^^^^^

``void ZoomAboutPoint(PointDouble point, VectorDouble zoom)``

Zoom about an arbitrary point.

The method **ZoomAboutPoint** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``point``   | ``PointDouble``    |               |
+-------------+--------------------+---------------+
| ``zoom``    | ``VectorDouble``   |               |
+-------------+--------------------+---------------+

This affects both the zoom factors and the scroll position.

Method *ZoomToFit*
^^^^^^^^^^^^^^^^^^

``void ZoomToFit(DisplayBase.ZoomToFitMode mode)``

Zoom to fit window size.

The method **ZoomToFit** has the following parameters:

+-------------+---------------------------------+--------------------------------------------+
| Parameter   | Type                            | Description                                |
+=============+=================================+============================================+
| ``mode``    | ``DisplayBase.ZoomToFitMode``   | The mode of the zoom factor calculation.   |
+-------------+---------------------------------+--------------------------------------------+

This function calculates the zoom factors in a way that the contents fit the window size. The mode of the function affects how the zoom factors are calculated. This affects both the zoom factors and the scroll position.

Method *Resize*
^^^^^^^^^^^^^^^

``void Resize(VectorInt32 size)``

Resize the window.

The method **Resize** has the following parameters:

+-------------+-------------------+------------------------+
| Parameter   | Type              | Description            |
+=============+===================+========================+
| ``size``    | ``VectorInt32``   | The new window size.   |
+-------------+-------------------+------------------------+

Method *ReparentChildren*
^^^^^^^^^^^^^^^^^^^^^^^^^

``void ReparentChildren(DisplayBase other)``

The method **ReparentChildren** has the following parameters:

+-------------+-------------------+---------------+
| Parameter   | Type              | Description   |
+=============+===================+===============+
| ``other``   | ``DisplayBase``   |               |
+-------------+-------------------+---------------+

Enumerations
~~~~~~~~~~~~

Enumeration *ScrollbarModeFlags*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum ScrollbarModeFlags``

TODO documentation missing

The enumeration **ScrollbarModeFlags** has the following constants:

+----------------------------------+----------+---------------+
| Name                             | Value    | Description   |
+==================================+==========+===============+
| ``showScrollbars``               | ``0``    |               |
+----------------------------------+----------+---------------+
| ``noHorizontalScrollbar``        | ``1``    |               |
+----------------------------------+----------+---------------+
| ``disableHorizontalScrollbar``   | ``2``    |               |
+----------------------------------+----------+---------------+
| ``noVerticalScrollbar``          | ``16``   |               |
+----------------------------------+----------+---------------+
| ``disableVerticalScrollbar``     | ``32``   |               |
+----------------------------------+----------+---------------+
| ``noScrollbars``                 | ``17``   |               |
+----------------------------------+----------+---------------+
| ``disableScrollbars``            | ``34``   |               |
+----------------------------------+----------+---------------+

::

    enum ScrollbarModeFlags
    {
      showScrollbars = 0,
      noHorizontalScrollbar = 1,
      disableHorizontalScrollbar = 2,
      noVerticalScrollbar = 16,
      disableVerticalScrollbar = 32,
      noScrollbars = 17,
      disableScrollbars = 34,
    };

TODO documentation missing

Enumeration *ZoomToFitMode*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum ZoomToFitMode``

TODO documentation missing

The enumeration **ZoomToFitMode** has the following constants:

+---------------------------+---------+---------------+
| Name                      | Value   | Description   |
+===========================+=========+===============+
| ``isotropicShorter``      | ``0``   |               |
+---------------------------+---------+---------------+
| ``isotropicLonger``       | ``1``   |               |
+---------------------------+---------+---------------+
| ``isotropicHorizontal``   | ``2``   |               |
+---------------------------+---------+---------------+
| ``isotropicVertical``     | ``3``   |               |
+---------------------------+---------+---------------+
| ``anisotropic``           | ``4``   |               |
+---------------------------+---------+---------------+

::

    enum ZoomToFitMode
    {
      isotropicShorter = 0,
      isotropicLonger = 1,
      isotropicHorizontal = 2,
      isotropicVertical = 3,
      anisotropic = 4,
    };

TODO documentation missing

Events
~~~~~~

Event *Move*
^^^^^^^^^^^^

``void Move(DisplayBase window, PointInt32 position)``

TODO no brief description for variant

The event **Move** has the following parameters:

+----------------+-------------------+---------------+
| Parameter      | Type              | Description   |
+================+===================+===============+
| ``window``     | ``DisplayBase``   |               |
+----------------+-------------------+---------------+
| ``position``   | ``PointInt32``    |               |
+----------------+-------------------+---------------+

TODO no description for variant

Event *Size*
^^^^^^^^^^^^

``void Size(DisplayBase window, VectorInt32 size)``

TODO no brief description for variant

The event **Size** has the following parameters:

+--------------+-------------------+---------------+
| Parameter    | Type              | Description   |
+==============+===================+===============+
| ``window``   | ``DisplayBase``   |               |
+--------------+-------------------+---------------+
| ``size``     | ``VectorInt32``   |               |
+--------------+-------------------+---------------+

TODO no description for variant

Event *Paint*
^^^^^^^^^^^^^

``System.Boolean Paint(DisplayBase window, GraphicsContext context)``

TODO no brief description for variant

The event **Paint** has the following parameters:

+---------------+-----------------------+---------------+
| Parameter     | Type                  | Description   |
+===============+=======================+===============+
| ``window``    | ``DisplayBase``       |               |
+---------------+-----------------------+---------------+
| ``context``   | ``GraphicsContext``   |               |
+---------------+-----------------------+---------------+

TODO no description for variant

Event *Painted*
^^^^^^^^^^^^^^^

``void Painted(DisplayBase window, GraphicsContext context)``

TODO no brief description for variant

The event **Painted** has the following parameters:

+---------------+-----------------------+---------------+
| Parameter     | Type                  | Description   |
+===============+=======================+===============+
| ``window``    | ``DisplayBase``       |               |
+---------------+-----------------------+---------------+
| ``context``   | ``GraphicsContext``   |               |
+---------------+-----------------------+---------------+

TODO no description for variant

Event *PaintedInfo*
^^^^^^^^^^^^^^^^^^^

``void PaintedInfo(DisplayBase window, System.Double time)``

TODO no brief description for variant

The event **PaintedInfo** has the following parameters:

+--------------+---------------------+---------------+
| Parameter    | Type                | Description   |
+==============+=====================+===============+
| ``window``   | ``DisplayBase``     |               |
+--------------+---------------------+---------------+
| ``time``     | ``System.Double``   |               |
+--------------+---------------------+---------------+

TODO no description for variant
