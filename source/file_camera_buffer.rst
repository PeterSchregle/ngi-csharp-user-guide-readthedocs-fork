Class *CameraBuffer*
--------------------

A non-generic buffer with runtime description of its content.

**Namespace:** Ngi

**Module:** Camera

The class **CameraBuffer** contains the following properties:

+-----------------------+-------+-------+---------------+
| Property              | Get   | Set   | Description   |
+=======================+=======+=======+===============+
| ``Data``              | \*    |       |               |
+-----------------------+-------+-------+---------------+
| ``Width``             | \*    |       |               |
+-----------------------+-------+-------+---------------+
| ``Height``            | \*    |       |               |
+-----------------------+-------+-------+---------------+
| ``LinePitch``         | \*    |       |               |
+-----------------------+-------+-------+---------------+
| ``PixelformatInfo``   | \*    |       |               |
+-----------------------+-------+-------+---------------+
| ``Frameid``           | \*    |       |               |
+-----------------------+-------+-------+---------------+
| ``Timestamp``         | \*    |       |               |
+-----------------------+-------+-------+---------------+
| ``ChunkData``         | \*    |       |               |
+-----------------------+-------+-------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Data*
^^^^^^^^^^^^^^^

``System.IntPtr Data``

Property *Width*
^^^^^^^^^^^^^^^^

``System.Int32 Width``

Property *Height*
^^^^^^^^^^^^^^^^^

``System.Int32 Height``

Property *LinePitch*
^^^^^^^^^^^^^^^^^^^^

``System.Int32 LinePitch``

Property *PixelformatInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``CameraPixelformatInfo PixelformatInfo``

Property *Frameid*
^^^^^^^^^^^^^^^^^^

``System.UInt64 Frameid``

Property *Timestamp*
^^^^^^^^^^^^^^^^^^^^

``System.UInt64 Timestamp``

Property *ChunkData*
^^^^^^^^^^^^^^^^^^^^

``System.UInt32 ChunkData``
