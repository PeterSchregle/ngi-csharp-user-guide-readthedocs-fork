Class *RangeFunctor*
--------------------

This function object can be used to calculate the difference between the maximum and the minimum.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **RangeFunctor** contains the following variant parameters:

+------------+-----------------------------------------+
| Variant    | Description                             |
+============+=========================================+
| ``Type``   | TODO no brief description for variant   |
+------------+-----------------------------------------+

The class **RangeFunctor** contains the following properties:

+---------------+-------+-------+--------------------------------+
| Property      | Get   | Set   | Description                    |
+===============+=======+=======+================================+
| ``Minimum``   | \*    |       | The minimum of the elements.   |
+---------------+-------+-------+--------------------------------+
| ``Maximum``   | \*    |       | The maximum of the elements.   |
+---------------+-------+-------+--------------------------------+
| ``Range``     | \*    |       | The range of the elements.     |
+---------------+-------+-------+--------------------------------+

The class **RangeFunctor** contains the following methods:

+--------------------------+-----------------------------+
| Method                   | Description                 |
+==========================+=============================+
| ``ProcessNextElement``   | Process the next element.   |
+--------------------------+-----------------------------+

Description
~~~~~~~~~~~

Usually the function object is used in algorithms such as for\_each. You can use it both for algorithms in the STL as well as for algorithms in ngi.

Variants
~~~~~~~~

Variant *Type*
^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Type** has the following types:

+------------------+
| Type             |
+==================+
| ``Byte``         |
+------------------+
| ``UInt16``       |
+------------------+
| ``UInt32``       |
+------------------+
| ``Double``       |
+------------------+
| ``RgbByte``      |
+------------------+
| ``RgbUInt16``    |
+------------------+
| ``RgbUInt32``    |
+------------------+
| ``RgbDouble``    |
+------------------+
| ``RgbaByte``     |
+------------------+
| ``RgbaUInt16``   |
+------------------+
| ``RgbaUInt32``   |
+------------------+
| ``RgbaDouble``   |
+------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *RangeFunctor*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``RangeFunctor()``

Construct the functor.

Properties
~~~~~~~~~~

Property *Minimum*
^^^^^^^^^^^^^^^^^^

``System.Object Minimum``

The minimum of the elements.

Property *Maximum*
^^^^^^^^^^^^^^^^^^

``System.Object Maximum``

The maximum of the elements.

Property *Range*
^^^^^^^^^^^^^^^^

``System.Object Range``

The range of the elements.

Methods
~~~~~~~

Method *ProcessNextElement*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ProcessNextElement(System.Object element)``

Process the next element.

The method **ProcessNextElement** has the following parameters:

+---------------+---------------------+----------------------+
| Parameter     | Type                | Description          |
+===============+=====================+======================+
| ``element``   | ``System.Object``   | The element value.   |
+---------------+---------------------+----------------------+
