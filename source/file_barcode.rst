Class *Barcode*
---------------

A barcode result.

**Namespace:** Ngi

**Module:** BarcodeMatrixcode

The class **Barcode** implements the following interfaces:

+-------------------------+
| Interface               |
+=========================+
| ``IEquatableBarcode``   |
+-------------------------+
| ``ISerializable``       |
+-------------------------+

The class **Barcode** contains the following properties:

+---------------------+-------+-------+------------------------------------+
| Property            | Get   | Set   | Description                        |
+=====================+=======+=======+====================================+
| ``Text``            | \*    |       | The decoded text of the barcode.   |
+---------------------+-------+-------+------------------------------------+
| ``Position``        | \*    |       | The position of the barcode.       |
+---------------------+-------+-------+------------------------------------+
| ``SymbologyId``     | \*    |       | The symbology of the barcode.      |
+---------------------+-------+-------+------------------------------------+
| ``SymbologyName``   | \*    |       | The symbology of the barcode.      |
+---------------------+-------+-------+------------------------------------+

The class **Barcode** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

The class **Barcode** contains the following enumerations:

+---------------------+------------------------------+
| Enumeration         | Description                  |
+=====================+==============================+
| ``SymbologyType``   | TODO documentation missing   |
+---------------------+------------------------------+

Description
~~~~~~~~~~~

A barcode result consists of the read barcode, the symbology and the location of the barcode.

The following operators are implemented for a barcode: operator == : comparison for equality. operator != : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Barcode*
^^^^^^^^^^^^^^^^^^^^^

``Barcode()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *Barcode*
^^^^^^^^^^^^^^^^^^^^^

``Barcode(System.String text, PointListPointDouble position, Barcode.SymbologyType symbologyId)``

Constructor.

The constructor has the following parameters:

+-------------------+-----------------------------+------------------------------------+
| Parameter         | Type                        | Description                        |
+===================+=============================+====================================+
| ``text``          | ``System.String``           | The decoded text of the barcode.   |
+-------------------+-----------------------------+------------------------------------+
| ``position``      | ``PointListPointDouble``    | The position of the barcode.       |
+-------------------+-----------------------------+------------------------------------+
| ``symbologyId``   | ``Barcode.SymbologyType``   | The symbology of the barcode.      |
+-------------------+-----------------------------+------------------------------------+

Properties
~~~~~~~~~~

Property *Text*
^^^^^^^^^^^^^^^

``System.String Text``

The decoded text of the barcode.

Property *Position*
^^^^^^^^^^^^^^^^^^^

``PointListPointDouble Position``

The position of the barcode.

Property *SymbologyId*
^^^^^^^^^^^^^^^^^^^^^^

``Barcode.SymbologyType SymbologyId``

The symbology of the barcode.

Property *SymbologyName*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.String SymbologyName``

The symbology of the barcode.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Enumerations
~~~~~~~~~~~~

Enumeration *SymbologyType*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum SymbologyType``

TODO documentation missing

The enumeration **SymbologyType** has the following constants:

+-----------------------+----------+---------------+
| Name                  | Value    | Description   |
+=======================+==========+===============+
| ``unknown``           | ``0``    |               |
+-----------------------+----------+---------------+
| ``codabar``           | ``2``    |               |
+-----------------------+----------+---------------+
| ``code39``            | ``3``    |               |
+-----------------------+----------+---------------+
| ``code93``            | ``4``    |               |
+-----------------------+----------+---------------+
| ``code128``           | ``5``    |               |
+-----------------------+----------+---------------+
| ``ean8``              | ``9``    |               |
+-----------------------+----------+---------------+
| ``ean13``             | ``10``   |               |
+-----------------------+----------+---------------+
| ``itf``               | ``11``   |               |
+-----------------------+----------+---------------+
| ``upcA``              | ``15``   |               |
+-----------------------+----------+---------------+
| ``upcE``              | ``16``   |               |
+-----------------------+----------+---------------+
| ``upcEanExtension``   | ``17``   |               |
+-----------------------+----------+---------------+

::

    enum SymbologyType
    {
      unknown = 0,
      codabar = 2,
      code39 = 3,
      code93 = 4,
      code128 = 5,
      ean8 = 9,
      ean13 = 10,
      itf = 11,
      upcA = 15,
      upcE = 16,
      upcEanExtension = 17,
    };

TODO documentation missing
