Class *TypeMismatchException*
-----------------------------

The type mismatch exception type.

**Namespace:** Ngi

**Module:**

The class **TypeMismatchException** contains the following enumerations:

+--------------------+------------------------------+
| Enumeration        | Description                  |
+====================+==============================+
| ``ExceptionIds``   | TODO documentation missing   |
+--------------------+------------------------------+

Description
~~~~~~~~~~~

Derives from the basic exception type.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *TypeMismatchException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TypeMismatchException()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *TypeMismatchException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TypeMismatchException(System.String message)``

Construct an exception from a message.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+

Constructor *TypeMismatchException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TypeMismatchException(System.String message, System.UInt32 id)``

Construct an exception from a message and an id.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+
| ``id``        | ``System.UInt32``   | The exception id.        |
+---------------+---------------------+--------------------------+

Enumerations
~~~~~~~~~~~~

Enumeration *ExceptionIds*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum ExceptionIds``

TODO documentation missing

The enumeration **ExceptionIds** has the following constants:

+--------------------+---------+---------------+
| Name               | Value   | Description   |
+====================+=========+===============+
| ``notAnError``     | ``0``   |               |
+--------------------+---------+---------------+
| ``typeMismatch``   | ``1``   |               |
+--------------------+---------+---------------+

::

    enum ExceptionIds
    {
      notAnError = 0,
      typeMismatch = 1,
    };

TODO documentation missing
