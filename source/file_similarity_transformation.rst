Class *SimilarityTransformation*
--------------------------------

Similarity transformation of geometric primitives.

**Namespace:** Ngi

**Module:** ImageProcessing

Description
~~~~~~~~~~~

This class provides static methods to transform geometric primitives via a similarity transformation.

The class can transform these geometric primitives: point, vector, direction, line\_segment, ray, line, box, rectangle, circle, ellipse, arc elliptical\_arc, triangle, quadrilateral, polyline, quadratic\_bezier and geometry.

All primitives retain their nature, when transformed by a similarity transform.

Some primitives can have an integer coordinate type on input, but the resulting types are always of floating point coordinate type. This is because the transformation cannot retain the integer nature in general. To illustrate, a point<int> type is transformed and the result is a point<double> type.

Static Methods
~~~~~~~~~~~~~~

Method *Transform*
^^^^^^^^^^^^^^^^^^

``VectorDouble Transform(VectorDouble vector, SimilarityMatrix similarity)``

Transform a vector.

The method **Transform** has the following parameters:

+------------------+------------------------+---------------+
| Parameter        | Type                   | Description   |
+==================+========================+===============+
| ``vector``       | ``VectorDouble``       |               |
+------------------+------------------------+---------------+
| ``similarity``   | ``SimilarityMatrix``   |               |
+------------------+------------------------+---------------+

A vector transformed with a similarity transform results in another vector.

The similarity transformation is carried out with a matrix multiplication.

Returns the transformed vector.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``PointDouble Transform(PointDouble point, SimilarityMatrix similarity)``

Transform a point.

The method **Transform** has the following parameters:

+------------------+------------------------+---------------+
| Parameter        | Type                   | Description   |
+==================+========================+===============+
| ``point``        | ``PointDouble``        |               |
+------------------+------------------------+---------------+
| ``similarity``   | ``SimilarityMatrix``   |               |
+------------------+------------------------+---------------+

A point transformed with a similarity transform results in another point.

The similarity transformation is carried out with a matrix multiplication.

Returns the transformed vector.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Direction Transform(Direction direction, SimilarityMatrix similarity)``

Transform a direction.

The method **Transform** has the following parameters:

+------------------+------------------------+---------------+
| Parameter        | Type                   | Description   |
+==================+========================+===============+
| ``direction``    | ``Direction``          |               |
+------------------+------------------------+---------------+
| ``similarity``   | ``SimilarityMatrix``   |               |
+------------------+------------------------+---------------+

A direction transformed with a similarity transform is the very same direction, i.e. a no-op.

Returns the transformed direction.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``LineSegmentDouble Transform(LineSegmentDouble lineSegment, SimilarityMatrix similarity)``

A line segment transformed with a similarity transform results in another line segment.

The method **Transform** has the following parameters:

+-------------------+-------------------------+---------------+
| Parameter         | Type                    | Description   |
+===================+=========================+===============+
| ``lineSegment``   | ``LineSegmentDouble``   |               |
+-------------------+-------------------------+---------------+
| ``similarity``    | ``SimilarityMatrix``    |               |
+-------------------+-------------------------+---------------+

The similarity transformation is carried out with a matrix multiplication.

Returns the transformed line segment.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``RayDouble Transform(RayDouble ray, SimilarityMatrix similarity)``

Transform a ray.

The method **Transform** has the following parameters:

+------------------+------------------------+---------------+
| Parameter        | Type                   | Description   |
+==================+========================+===============+
| ``ray``          | ``RayDouble``          |               |
+------------------+------------------------+---------------+
| ``similarity``   | ``SimilarityMatrix``   |               |
+------------------+------------------------+---------------+

A ray transformed with a similarity transform results in another ray.

The similarity transformation is carried out with a matrix multiplication.

Returns the transformed ray.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``LineDouble Transform(LineDouble line, SimilarityMatrix similarity)``

Transform a line.

The method **Transform** has the following parameters:

+------------------+------------------------+---------------+
| Parameter        | Type                   | Description   |
+==================+========================+===============+
| ``line``         | ``LineDouble``         |               |
+------------------+------------------------+---------------+
| ``similarity``   | ``SimilarityMatrix``   |               |
+------------------+------------------------+---------------+

A line transformed with a similarity transform results in another line.

The similarity transformation is carried out with a matrix multiplication.

Returns the transformed line.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``BoxDouble Transform(BoxDouble box, SimilarityMatrix similarity)``

Transform a box.

The method **Transform** has the following parameters:

+------------------+------------------------+---------------+
| Parameter        | Type                   | Description   |
+==================+========================+===============+
| ``box``          | ``BoxDouble``          |               |
+------------------+------------------------+---------------+
| ``similarity``   | ``SimilarityMatrix``   |               |
+------------------+------------------------+---------------+

A box transformed with a similarity transform results in another box.

The similarity transformation is carried out with a matrix multiplication.

Returns the transformed box in the form of a rectangle.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Rectangle Transform(Rectangle rectangle, SimilarityMatrix similarity)``

Transform a rectangle.

The method **Transform** has the following parameters:

+------------------+------------------------+---------------+
| Parameter        | Type                   | Description   |
+==================+========================+===============+
| ``rectangle``    | ``Rectangle``          |               |
+------------------+------------------------+---------------+
| ``similarity``   | ``SimilarityMatrix``   |               |
+------------------+------------------------+---------------+

A rectangle transformed with a similarity transform results in another rectangle.

The similarity transformation is carried out with a matrix multiplication.

Returns the transformed box in the form of a rectangle.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Circle Transform(Circle circle, SimilarityMatrix similarity)``

Transform a circle.

The method **Transform** has the following parameters:

+------------------+------------------------+---------------+
| Parameter        | Type                   | Description   |
+==================+========================+===============+
| ``circle``       | ``Circle``             |               |
+------------------+------------------------+---------------+
| ``similarity``   | ``SimilarityMatrix``   |               |
+------------------+------------------------+---------------+

A circle transformed with a similarity transform results in another circle.

The similarity transformation is carried out with a matrix multiplication.

Returns the transformed circle in the form of an ellipse.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Ellipse Transform(Ellipse ellipse, SimilarityMatrix similarity)``

Transform an ellipse.

The method **Transform** has the following parameters:

+------------------+------------------------+---------------+
| Parameter        | Type                   | Description   |
+==================+========================+===============+
| ``ellipse``      | ``Ellipse``            |               |
+------------------+------------------------+---------------+
| ``similarity``   | ``SimilarityMatrix``   |               |
+------------------+------------------------+---------------+

An ellipse transformed with a similarity transform results in another ellipse.

The similarity transformation is carried out with a matrix multiplication.

Returns the transformed ellipse.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Arc Transform(Arc arc, SimilarityMatrix similarity)``

Transform an arc.

The method **Transform** has the following parameters:

+------------------+------------------------+---------------+
| Parameter        | Type                   | Description   |
+==================+========================+===============+
| ``arc``          | ``Arc``                |               |
+------------------+------------------------+---------------+
| ``similarity``   | ``SimilarityMatrix``   |               |
+------------------+------------------------+---------------+

An arc transformed with a similarity transform results in another arc.

The similarity transformation is carried out with a matrix multiplication.

Returns the transformed arc.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``EllipticalArc Transform(EllipticalArc ellipticalArc, SimilarityMatrix similarity)``

Transform an elliptical arc.

The method **Transform** has the following parameters:

+---------------------+------------------------+---------------+
| Parameter           | Type                   | Description   |
+=====================+========================+===============+
| ``ellipticalArc``   | ``EllipticalArc``      |               |
+---------------------+------------------------+---------------+
| ``similarity``      | ``SimilarityMatrix``   |               |
+---------------------+------------------------+---------------+

An elliptical arc transformed with a similarity transform results in another elliptical arc

The similarity transformation is carried out with a matrix multiplication.

Returns the transformed elliptical arc.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``TriangleDouble Transform(TriangleDouble triangle, SimilarityMatrix similarity)``

Transform a triangle.

The method **Transform** has the following parameters:

+------------------+------------------------+---------------+
| Parameter        | Type                   | Description   |
+==================+========================+===============+
| ``triangle``     | ``TriangleDouble``     |               |
+------------------+------------------------+---------------+
| ``similarity``   | ``SimilarityMatrix``   |               |
+------------------+------------------------+---------------+

A triangle transformed with a similarity transform results in another triangle.

The similarity transformation is carried out with a matrix multiplication.

Returns the transformed triangle.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``QuadrilateralDouble Transform(QuadrilateralDouble quadrilateral, SimilarityMatrix similarity)``

Transform a quadrilateral.

The method **Transform** has the following parameters:

+---------------------+---------------------------+---------------+
| Parameter           | Type                      | Description   |
+=====================+===========================+===============+
| ``quadrilateral``   | ``QuadrilateralDouble``   |               |
+---------------------+---------------------------+---------------+
| ``similarity``      | ``SimilarityMatrix``      |               |
+---------------------+---------------------------+---------------+

A quadrilateral transformed with a similarity transform results in another quadrilateral.

The similarity transformation is carried out with a matrix multiplication.

Returns the transformed quadrilateral.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``PolylineDouble Transform(PolylineDouble polyline, SimilarityMatrix similarity)``

Transform a polyline.

The method **Transform** has the following parameters:

+------------------+------------------------+---------------+
| Parameter        | Type                   | Description   |
+==================+========================+===============+
| ``polyline``     | ``PolylineDouble``     |               |
+------------------+------------------------+---------------+
| ``similarity``   | ``SimilarityMatrix``   |               |
+------------------+------------------------+---------------+

A polyline transformed with a similarity transform results in another polyline.

The similarity transformation is carried out with a matrix multiplication.

Returns the transformed polyline.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``QuadraticBezier Transform(QuadraticBezier bezier, SimilarityMatrix translation)``

Transform a quadratic bezier curve.

The method **Transform** has the following parameters:

+-------------------+------------------------+---------------+
| Parameter         | Type                   | Description   |
+===================+========================+===============+
| ``bezier``        | ``QuadraticBezier``    |               |
+-------------------+------------------------+---------------+
| ``translation``   | ``SimilarityMatrix``   |               |
+-------------------+------------------------+---------------+

A quadratic bezier curve transformed with a similarity transform results in another quadratic bezier curve.

The similarity transformation is carried out with a matrix multiplication.

Returns the transformed quadratic bezier curve.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``CubicBezier Transform(CubicBezier bezier, SimilarityMatrix translation)``

Transform a cubic bezier curve.

The method **Transform** has the following parameters:

+-------------------+------------------------+---------------+
| Parameter         | Type                   | Description   |
+===================+========================+===============+
| ``bezier``        | ``CubicBezier``        |               |
+-------------------+------------------------+---------------+
| ``translation``   | ``SimilarityMatrix``   |               |
+-------------------+------------------------+---------------+

A cubic bezier curve transformed with a similarity transform results in another cubic bezier curve.

The similarity transformation is carried out with a matrix multiplication.

Returns the transformed cubic bezier curve.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Geometry Transform(Geometry geometry, SimilarityMatrix similarity)``

Transform a geometry.

The method **Transform** has the following parameters:

+------------------+------------------------+---------------+
| Parameter        | Type                   | Description   |
+==================+========================+===============+
| ``geometry``     | ``Geometry``           |               |
+------------------+------------------------+---------------+
| ``similarity``   | ``SimilarityMatrix``   |               |
+------------------+------------------------+---------------+

A geometry transformed with a similarity transform results in another geometry.

The similarity transformation is carried out by affine transformation of each geometry element.

Returns the transformed geometry.
