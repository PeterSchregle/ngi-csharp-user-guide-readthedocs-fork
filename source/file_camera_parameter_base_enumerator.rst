Class *CameraParameterBaseEnumerator*
-------------------------------------

**Namespace:** Ngi

**Module:** Camera

The class **CameraParameterBaseEnumerator** implements the following interfaces:

+--------------------------------------+
| Interface                            |
+======================================+
| ``IEnumerator``                      |
+--------------------------------------+
| ``IEnumeratorCameraParameterBase``   |
+--------------------------------------+

The class **CameraParameterBaseEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **CameraParameterBaseEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``CameraParameterBase Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
