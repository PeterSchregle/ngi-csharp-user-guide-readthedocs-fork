Class *LicenseData*
-------------------

License data.

**Namespace:** Ngi

**Module:**

The class **LicenseData** implements the following interfaces:

+-----------------------------+
| Interface                   |
+=============================+
| ``IEquatableLicenseData``   |
+-----------------------------+
| ``ISerializable``           |
+-----------------------------+

The class **LicenseData** contains the following properties:

+-------------------------------+-------+-------+-------------------------------------------------------------------------------------------+
| Property                      | Get   | Set   | Description                                                                               |
+===============================+=======+=======+===========================================================================================+
| ``Valid``                     | \*    | \*    | The validity of the license\_data.                                                        |
+-------------------------------+-------+-------+-------------------------------------------------------------------------------------------+
| ``ProductId``                 | \*    | \*    | The product id.                                                                           |
+-------------------------------+-------+-------+-------------------------------------------------------------------------------------------+
| ``LicenseNumber``             | \*    | \*    | The license number.                                                                       |
+-------------------------------+-------+-------+-------------------------------------------------------------------------------------------+
| ``ExpirationDate``            | \*    | \*    | The expiration date. Only trials have an expiration date.                                 |
+-------------------------------+-------+-------+-------------------------------------------------------------------------------------------+
| ``UpdateExpirationDate``      | \*    | \*    | The update expiration date.                                                               |
+-------------------------------+-------+-------+-------------------------------------------------------------------------------------------+
| ``ImageProcessingModule``     | \*    | \*    | The image processing module.                                                              |
+-------------------------------+-------+-------+-------------------------------------------------------------------------------------------+
| ``CameraModule``              | \*    | \*    | The camera module.                                                                        |
+-------------------------------+-------+-------+-------------------------------------------------------------------------------------------+
| ``AnalysisMeasuringModule``   | \*    | \*    | The analysis and measurement module.                                                      |
+-------------------------------+-------+-------+-------------------------------------------------------------------------------------------+
| ``TemplateMatchingModule``    | \*    | \*    | The template matching module.                                                             |
+-------------------------------+-------+-------+-------------------------------------------------------------------------------------------+
| ``BarcodeMatrixcodeModule``   | \*    | \*    | The barcode and matrixcode module.                                                        |
+-------------------------------+-------+-------+-------------------------------------------------------------------------------------------+
| ``OcrOcvModule``              | \*    | \*    | The ocr and ocv module.                                                                   |
+-------------------------------+-------+-------+-------------------------------------------------------------------------------------------+
| ``CadPdfModule``              | \*    | \*    | The cad and pdf module.                                                                   |
+-------------------------------+-------+-------+-------------------------------------------------------------------------------------------+
| ``HalconInterfaceModule``     | \*    | \*    | The HALCON interface module.                                                              |
+-------------------------------+-------+-------+-------------------------------------------------------------------------------------------+
| ``NVisionDesigner``           | \*    | \*    | Flag set for nVision Designer (True: nVision Designer, False: nVision Runtime).           |
+-------------------------------+-------+-------+-------------------------------------------------------------------------------------------+
| ``NVisionUltimate``           | \*    | \*    | Flag set for nVision Designer Ultimate (True: nVision Ultimate, False: nVision MV/Lab).   |
+-------------------------------+-------+-------+-------------------------------------------------------------------------------------------+
| ``Modules``                   | \*    |       | Provide the module validity encoded in a 32 bit integer.                                  |
+-------------------------------+-------+-------+-------------------------------------------------------------------------------------------+

The class **LicenseData** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *LicenseData*
^^^^^^^^^^^^^^^^^^^^^^^^^

``LicenseData()``

Default constructor.

Properties
~~~~~~~~~~

Property *Valid*
^^^^^^^^^^^^^^^^

``System.Boolean Valid``

The validity of the license\_data.

Property *ProductId*
^^^^^^^^^^^^^^^^^^^^

``System.Int32 ProductId``

The product id.

Property *LicenseNumber*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.String LicenseNumber``

The license number.

The license number is the serial number coded into the dongle or the license key in the case of a software license.

Property *ExpirationDate*
^^^^^^^^^^^^^^^^^^^^^^^^^

``Date ExpirationDate``

The expiration date. Only trials have an expiration date.

Property *UpdateExpirationDate*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Date UpdateExpirationDate``

The update expiration date.

Software built after the update expiration date will not run.

Property *ImageProcessingModule*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean ImageProcessingModule``

The image processing module.

Property *CameraModule*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean CameraModule``

The camera module.

Property *AnalysisMeasuringModule*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean AnalysisMeasuringModule``

The analysis and measurement module.

Property *TemplateMatchingModule*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean TemplateMatchingModule``

The template matching module.

Property *BarcodeMatrixcodeModule*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean BarcodeMatrixcodeModule``

The barcode and matrixcode module.

Property *OcrOcvModule*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean OcrOcvModule``

The ocr and ocv module.

Property *CadPdfModule*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean CadPdfModule``

The cad and pdf module.

Property *HalconInterfaceModule*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean HalconInterfaceModule``

The HALCON interface module.

Property *NVisionDesigner*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean NVisionDesigner``

Flag set for nVision Designer (True: nVision Designer, False: nVision Runtime).

Property *NVisionUltimate*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean NVisionUltimate``

Flag set for nVision Designer Ultimate (True: nVision Ultimate, False: nVision MV/Lab).

Property *Modules*
^^^^^^^^^^^^^^^^^^

``System.Int32 Modules``

Provide the module validity encoded in a 32 bit integer.

The modules are encoded bitwise. The image\_processing\_module is bit 0, the analysis\_measuring\_module is bit 1, the template\_matching\_module is bit 2, the barcode\_matrixcode\_module is bit 3, the ocr\_ocv\_module is bit 4, the cad\_pdf\_module is bit 7 and the halcon\_interface\_module is bit 8.

n\_vision\_designer is bit 5 and n\_vision\_ultimate is bit 6. In total there is room for 20 modules in the licensing key.Previsously, the code was written so that if a module is added like the ones that are already there, previously issued licenses would gain access to the module automatically.

Now the code is written that previously issued license do not gain access to future modules automatically.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
