Module ImageProcessing
======================

The module **ImageProcessing** contains the following static classes:

+--------------------------------------+-------------------------------------------------------------+
| Class                                | Description                                                 |
+======================================+=============================================================+
| ``AffineTransformation``             | Affine transformation of geometric primitives.              |
+--------------------------------------+-------------------------------------------------------------+
| ``BasicAlgorithms``                  | Basic image processing functions.                           |
+--------------------------------------+-------------------------------------------------------------+
| ``BinningAlgorithms``                | Image binning functions.                                    |
+--------------------------------------+-------------------------------------------------------------+
| ``Calibration``                      | Camera calibration functions.                               |
+--------------------------------------+-------------------------------------------------------------+
| ``ColorAlgorithms``                  | Basic color processing functions.                           |
+--------------------------------------+-------------------------------------------------------------+
| ``FilterAlgorithms``                 | Image processing filter functions.                          |
+--------------------------------------+-------------------------------------------------------------+
| ``GeometryAlgorithms``               | Geometric image transformation functions.                   |
+--------------------------------------+-------------------------------------------------------------+
| ``IsotropicScalingTransformation``   | Isotropic scaling transformation of geometric primitives.   |
+--------------------------------------+-------------------------------------------------------------+
| ``MapAlgorithms``                    | Image mapping filter functions.                             |
+--------------------------------------+-------------------------------------------------------------+
| ``Mathematics``                      | Mathematics functions.                                      |
+--------------------------------------+-------------------------------------------------------------+
| ``MorphologyAlgorithms``             | Image morphology functions.                                 |
+--------------------------------------+-------------------------------------------------------------+
| ``PerspectiveTransformation``        | Perspective transformation of geometric primitives.         |
+--------------------------------------+-------------------------------------------------------------+
| ``PointAlgorithms``                  | Image point functions.                                      |
+--------------------------------------+-------------------------------------------------------------+
| ``PresetAlgorithms``                 | Image preset functions.                                     |
+--------------------------------------+-------------------------------------------------------------+
| ``RigidTransformation``              | Rotation transformation of geometric primitives.            |
+--------------------------------------+-------------------------------------------------------------+
| ``RotationTransformation``           | Rotation transformation of geometric primitives.            |
+--------------------------------------+-------------------------------------------------------------+
| ``ScalingTransformation``            | Scaling transformation of geometric primitives.             |
+--------------------------------------+-------------------------------------------------------------+
| ``SegmentationAlgorithms``           | Image segmentation functions.                               |
+--------------------------------------+-------------------------------------------------------------+
| ``SimilarityTransformation``         | Similarity transformation of geometric primitives.          |
+--------------------------------------+-------------------------------------------------------------+
| ``SplitAndCombineAlgorithms``        | Image segmentation functions.                               |
+--------------------------------------+-------------------------------------------------------------+
| ``Statistic``                        | Statistic functions.                                        |
+--------------------------------------+-------------------------------------------------------------+
| ``StereoAlgorithms``                 | Radiometric stereo functions.                               |
+--------------------------------------+-------------------------------------------------------------+
| ``SubsampleAlgorithms``              | Image subsampling functions.                                |
+--------------------------------------+-------------------------------------------------------------+
| ``TranslationTransformation``        | Translation transformation of geometric primitives.         |
+--------------------------------------+-------------------------------------------------------------+
| ``XcoderAlgorithms``                 | Encoder/decoder functions.                                  |
+--------------------------------------+-------------------------------------------------------------+

The module **ImageProcessing** contains the following classes:

+------------+----------------------------------------------------+
| Class      | Description                                        |
+============+====================================================+
| ``AffineMa | A matrix class useful for affine 2D geometric      |
| trix``     | transformations.                                   |
+------------+----------------------------------------------------+
| ``Angle``  | An angle in two-dimensional euclidean space.       |
+------------+----------------------------------------------------+
| ``Arc``    | An arc in two-dimensional euclidean space.         |
+------------+----------------------------------------------------+
| ``ArcEnume |                                                    |
| rator``    |                                                    |
+------------+----------------------------------------------------+
| ``ArcList` |                                                    |
| `          |                                                    |
+------------+----------------------------------------------------+
| ``Arithmet | This function object can be used to calculate the  |
| icMeanFunc | arithmetic mean of a sequence of values.           |
| tor``      |                                                    |
+------------+----------------------------------------------------+
| ``BarcodeD | Configuration for the barcode decoder.             |
| ecoder``   |                                                    |
+------------+----------------------------------------------------+
| ``Box``    | A box in two-dimensional euclidean space.          |
+------------+----------------------------------------------------+
| ``Box3d``  | A box in three-dimensional euclidean space.        |
+------------+----------------------------------------------------+
| ``Box3dEnu |                                                    |
| merator``  |                                                    |
+------------+----------------------------------------------------+
| ``Box3dLis |                                                    |
| t``        |                                                    |
+------------+----------------------------------------------------+
| ``BoxEnume |                                                    |
| rator``    |                                                    |
+------------+----------------------------------------------------+
| ``BoxList` |                                                    |
| `          |                                                    |
+------------+----------------------------------------------------+
| ``BrushEnu |                                                    |
| merator``  |                                                    |
+------------+----------------------------------------------------+
| ``BrushLis |                                                    |
| t``        |                                                    |
+------------+----------------------------------------------------+
| ``Buffer`` | The buffer class is a multidimensional container   |
|            | that arranges elements of a given type in a linear |
|            | arrangement.                                       |
+------------+----------------------------------------------------+
| ``BufferEn |                                                    |
| umerator`` |                                                    |
+------------+----------------------------------------------------+
| ``BufferLi |                                                    |
| st``       |                                                    |
+------------+----------------------------------------------------+
| ``CentralM | Class to hold central moments of the zeroth,       |
| oments``   | second and third order.                            |
+------------+----------------------------------------------------+
| ``ChainCod | Class that provides a chain code representation.   |
| e``        |                                                    |
+------------+----------------------------------------------------+
| ``CifxDriv |                                                    |
| er``       |                                                    |
+------------+----------------------------------------------------+
| ``CifxChan |                                                    |
| nel``      |                                                    |
+------------+----------------------------------------------------+
| ``Chord``  | A chord in a region.                               |
+------------+----------------------------------------------------+
| ``ChordEnu |                                                    |
| merator``  |                                                    |
+------------+----------------------------------------------------+
| ``ChordLis |                                                    |
| t``        |                                                    |
+------------+----------------------------------------------------+
| ``Circle`` | The geometric circle primitive.                    |
+------------+----------------------------------------------------+
| ``CircleEn |                                                    |
| umerator`` |                                                    |
+------------+----------------------------------------------------+
| ``CircleLi |                                                    |
| st``       |                                                    |
+------------+----------------------------------------------------+
| ``Color``  | The color type.                                    |
+------------+----------------------------------------------------+
| ``ColorEnu |                                                    |
| merator``  |                                                    |
+------------+----------------------------------------------------+
| ``ColorLis |                                                    |
| t``        |                                                    |
+------------+----------------------------------------------------+
| ``CornerRa | The corner\_radius describes the four corner radii |
| dius``     | of a rounded rectangle.                            |
+------------+----------------------------------------------------+
| ``CountToo |                                                    |
| lGroup``   |                                                    |
+------------+----------------------------------------------------+
| ``CountToo |                                                    |
| lGroupEnum |                                                    |
| erator``   |                                                    |
+------------+----------------------------------------------------+
| ``CountToo |                                                    |
| lGroupList |                                                    |
| ``         |                                                    |
+------------+----------------------------------------------------+
| ``CubicBez | The following operations are implemented:          |
| ier``      | operator== : comparison for equality.              |
+------------+----------------------------------------------------+
| ``DataEnum |                                                    |
| erator``   |                                                    |
+------------+----------------------------------------------------+
| ``DataList |                                                    |
| ``         |                                                    |
+------------+----------------------------------------------------+
| ``Directio | A direction in two-dimensional euclidean space.    |
| n``        |                                                    |
+------------+----------------------------------------------------+
| ``Display` | The display.                                       |
| `          |                                                    |
+------------+----------------------------------------------------+
| ``DisplayB | The display\_base type.                            |
| ase``      |                                                    |
+------------+----------------------------------------------------+
| ``DoubleEn |                                                    |
| umerator`` |                                                    |
+------------+----------------------------------------------------+
| ``DoubleLi |                                                    |
| st``       |                                                    |
+------------+----------------------------------------------------+
| ``DtOpenla |                                                    |
| yersSystem |                                                    |
| ``         |                                                    |
+------------+----------------------------------------------------+
| ``DtOpenla |                                                    |
| yersModule |                                                    |
| ``         |                                                    |
+------------+----------------------------------------------------+
| ``Ellipse` | The geometric ellipse primitive.                   |
| `          |                                                    |
+------------+----------------------------------------------------+
| ``EllipseE |                                                    |
| numerator` |                                                    |
| `          |                                                    |
+------------+----------------------------------------------------+
| ``EllipseL |                                                    |
| ist``      |                                                    |
+------------+----------------------------------------------------+
| ``Elliptic | The following operations are implemented:          |
| alArc``    | operator== : comparison for equality.              |
+------------+----------------------------------------------------+
| ``Elliptic |                                                    |
| alArcEnume |                                                    |
| rator``    |                                                    |
+------------+----------------------------------------------------+
| ``Elliptic |                                                    |
| alArcList` |                                                    |
| `          |                                                    |
+------------+----------------------------------------------------+
| ``Elliptic | The geometric elliptical\_ring primitive.          |
| alRing``   |                                                    |
+------------+----------------------------------------------------+
| ``Elliptic |                                                    |
| alRingEnum |                                                    |
| erator``   |                                                    |
+------------+----------------------------------------------------+
| ``Elliptic |                                                    |
| alRingList |                                                    |
| ``         |                                                    |
+------------+----------------------------------------------------+
| ``Elliptic | The geometric elliptical\_sector primitive.        |
| alSector`` |                                                    |
+------------+----------------------------------------------------+
| ``Elliptic |                                                    |
| alSectorEn |                                                    |
| umerator`` |                                                    |
+------------+----------------------------------------------------+
| ``Elliptic |                                                    |
| alSectorLi |                                                    |
| st``       |                                                    |
+------------+----------------------------------------------------+
| ``Extent`` | A two-dimensional extent.                          |
+------------+----------------------------------------------------+
| ``ExtentEn |                                                    |
| umerator`` |                                                    |
+------------+----------------------------------------------------+
| ``ExtentLi |                                                    |
| st``       |                                                    |
+------------+----------------------------------------------------+
| ``Extent3d | A three-dimensional extent.                        |
| ``         |                                                    |
+------------+----------------------------------------------------+
| ``Extent3d |                                                    |
| Enumerator |                                                    |
| ``         |                                                    |
+------------+----------------------------------------------------+
| ``Extent3d |                                                    |
| List``     |                                                    |
+------------+----------------------------------------------------+
| ``File``   | The file.                                          |
+------------+----------------------------------------------------+
| ``FitResul | The result of a fit procedure.                     |
| t``        |                                                    |
+------------+----------------------------------------------------+
| ``FlusserM | Class to hold Flusser's moments.                   |
| oments``   |                                                    |
+------------+----------------------------------------------------+
| ``Font``   | A drawing font.                                    |
+------------+----------------------------------------------------+
| ``FontEnum |                                                    |
| erator``   |                                                    |
+------------+----------------------------------------------------+
| ``FontList |                                                    |
| ``         |                                                    |
+------------+----------------------------------------------------+
| ``FreemanC | Freeman code representation.                       |
| ode``      |                                                    |
+------------+----------------------------------------------------+
| ``FreemanC |                                                    |
| odeEnumera |                                                    |
| tor``      |                                                    |
+------------+----------------------------------------------------+
| ``FreemanC |                                                    |
| odeList``  |                                                    |
+------------+----------------------------------------------------+
| ``Geometri | This function object can be used to calculate the  |
| cMeanFunct | geometric mean of a sequence of values.            |
| or``       |                                                    |
+------------+----------------------------------------------------+
| ``Geometry | The following operations are implemented:          |
| Element``  | operator== : comparison for equality.              |
+------------+----------------------------------------------------+
| ``Geometry |                                                    |
| ElementEnu |                                                    |
| merator``  |                                                    |
+------------+----------------------------------------------------+
| ``Geometry |                                                    |
| ElementLis |                                                    |
| t``        |                                                    |
+------------+----------------------------------------------------+
| ``Geometry | A geometry in two-dimensional euclidean space.     |
| ``         |                                                    |
+------------+----------------------------------------------------+
| ``Geometry |                                                    |
| Enumerator |                                                    |
| ``         |                                                    |
+------------+----------------------------------------------------+
| ``Geometry |                                                    |
| List``     |                                                    |
+------------+----------------------------------------------------+
| ``Geometry | The following operations are implemented:          |
| Start``    | operator== : comparison for equality.              |
+------------+----------------------------------------------------+
| ``Geometry | The following operations are implemented:          |
| LineSegmen | operator== : comparison for equality.              |
| t``        |                                                    |
+------------+----------------------------------------------------+
| ``Geometry | The following operations are implemented:          |
| Arc``      | operator== : comparison for equality.              |
+------------+----------------------------------------------------+
| ``Geometry | The following operations are implemented:          |
| CubicBezie | operator== : comparison for equality.              |
| r``        |                                                    |
+------------+----------------------------------------------------+
| ``Geometry | The following operations are implemented:          |
| Elliptical | operator== : comparison for equality.              |
| Arc``      |                                                    |
+------------+----------------------------------------------------+
| ``Geometry | The following operations are implemented:          |
| QuadraticB | operator== : comparison for equality.              |
| ezier``    |                                                    |
+------------+----------------------------------------------------+
| ``Graphics | The graphics context.                              |
| Context``  |                                                    |
+------------+----------------------------------------------------+
| ``Harmonic | This function object can be used to calculate the  |
| MeanFuncto | harmonic mean of a sequence of values.             |
| r``        |                                                    |
+------------+----------------------------------------------------+
| ``Histogra | ...                                                |
| m``        |                                                    |
+------------+----------------------------------------------------+
| ``Histogra |                                                    |
| mEnumerato |                                                    |
| r``        |                                                    |
+------------+----------------------------------------------------+
| ``Histogra |                                                    |
| mList``    |                                                    |
+------------+----------------------------------------------------+
| ``Hls``    | The hls color type.                                |
+------------+----------------------------------------------------+
| ``Hsi``    | The hsi color type.                                |
+------------+----------------------------------------------------+
| ``HuMoment | Class to hold Hu's moments.                        |
| s``        |                                                    |
+------------+----------------------------------------------------+
| ``Image``  | ...                                                |
+------------+----------------------------------------------------+
| ``ImageEnu |                                                    |
| merator``  |                                                    |
+------------+----------------------------------------------------+
| ``ImageFil | Information about an image fileformat.             |
| eformatInf |                                                    |
| o``        |                                                    |
+------------+----------------------------------------------------+
| ``ImageFil |                                                    |
| eformatInf |                                                    |
| oEnumerato |                                                    |
| r``        |                                                    |
+------------+----------------------------------------------------+
| ``ImageFil | A list of regions.                                 |
| eformatInf |                                                    |
| oList``    |                                                    |
+------------+----------------------------------------------------+
| ``ImageFil | The image\_file\_info class allows you to get      |
| eInfo``    | information about a picture stored in a file, such |
|            | as its size and type of data.                      |
+------------+----------------------------------------------------+
| ``ImageLis | It is a list.                                      |
| t``        |                                                    |
+------------+----------------------------------------------------+
| ``ImageWit | This class pairs an image with a palette.          |
| hPalette`` |                                                    |
+------------+----------------------------------------------------+
| ``ImagePyr | ...                                                |
| amid``     |                                                    |
+------------+----------------------------------------------------+
| ``ImagePyr |                                                    |
| amidEnumer |                                                    |
| ator``     |                                                    |
+------------+----------------------------------------------------+
| ``ImagePyr |                                                    |
| amidList`` |                                                    |
+------------+----------------------------------------------------+
| ``Index``  | A two-dimensional, zero-based index into a buffer. |
+------------+----------------------------------------------------+
| ``IndexEnu |                                                    |
| merator``  |                                                    |
+------------+----------------------------------------------------+
| ``IndexLis |                                                    |
| t``        |                                                    |
+------------+----------------------------------------------------+
| ``Index3d` | A three-dimensional, zero-based index into a       |
| `          | buffer.                                            |
+------------+----------------------------------------------------+
| ``Index3dE |                                                    |
| numerator` |                                                    |
| `          |                                                    |
+------------+----------------------------------------------------+
| ``Index3dL |                                                    |
| ist``      |                                                    |
+------------+----------------------------------------------------+
| ``Inpout`` |                                                    |
+------------+----------------------------------------------------+
| ``IoResour | The base class for files, serial ports and         |
| ce``       | sockets.                                           |
+------------+----------------------------------------------------+
| ``Isotropi | A matrix useful for affine 2D geometric scaling.   |
| cScalingMa |                                                    |
| trix``     |                                                    |
+------------+----------------------------------------------------+
| ``Lab``    | The lab color type.                                |
+------------+----------------------------------------------------+
| ``Line``   | A directed infinite line in two-dimensional        |
|            | euclidean space.                                   |
+------------+----------------------------------------------------+
| ``LineEnum |                                                    |
| erator``   |                                                    |
+------------+----------------------------------------------------+
| ``LineList |                                                    |
| ``         |                                                    |
+------------+----------------------------------------------------+
| ``LineSegm | A line\_segment in two-dimensional euclidean       |
| ent``      | space.                                             |
+------------+----------------------------------------------------+
| ``LineSegm |                                                    |
| entEnumera |                                                    |
| tor``      |                                                    |
+------------+----------------------------------------------------+
| ``LineSegm |                                                    |
| entList``  |                                                    |
+------------+----------------------------------------------------+
| ``Locator` | A template that provides a random-access locator   |
| `          | as a basis for a mutable or a const locator.       |
+------------+----------------------------------------------------+
| ``Matrixco | Configuration for the matrixcode decoder.          |
| deDecoder` |                                                    |
| `          |                                                    |
+------------+----------------------------------------------------+
| ``Matrix`` | A matrix class with fundamental operations of      |
|            | numerical linear algebra.                          |
+------------+----------------------------------------------------+
| ``MaximumF | This function object can be used to calculate the  |
| unctor``   | maximum of a sequence of values.                   |
+------------+----------------------------------------------------+
| ``MedianFu | This function object can be used to calculate the  |
| nctor``    | median.                                            |
+------------+----------------------------------------------------+
| ``Midrange | This function object can be used to calculate the  |
| Functor``  | arithmetic mean between the maximum and the        |
|            | minimum.                                           |
+------------+----------------------------------------------------+
| ``MinimumF | This function object can be used to calculate the  |
| unctor``   | minimum of a sequence of values.                   |
+------------+----------------------------------------------------+
| ``Moments` | Class to hold raw moments of the zeroth up to the  |
| `          | third order.                                       |
+------------+----------------------------------------------------+
| ``Mono``   | The mono color type.                               |
+------------+----------------------------------------------------+
| ``MonoAlph | The mono\_alpha pixel color type.                  |
| a``        |                                                    |
+------------+----------------------------------------------------+
| ``Normaliz | Class to hold normalized moments of the zeroth up  |
| edMoments` | to the third order.                                |
| `          |                                                    |
+------------+----------------------------------------------------+
| ``Number`` | The.                                               |
+------------+----------------------------------------------------+
| ``OrderedP | This object keeps a set of points in an            |
| oints``    | orthogonally ordered way of columns x rows cells.  |
+------------+----------------------------------------------------+
| ``Palette` | ...                                                |
| `          |                                                    |
+------------+----------------------------------------------------+
| ``PaletteE |                                                    |
| numerator` |                                                    |
| `          |                                                    |
+------------+----------------------------------------------------+
| ``PaletteL |                                                    |
| ist``      |                                                    |
+------------+----------------------------------------------------+
| ``Pen``    | A drawing pen.                                     |
+------------+----------------------------------------------------+
| ``PenEnume |                                                    |
| rator``    |                                                    |
+------------+----------------------------------------------------+
| ``PenList` |                                                    |
| `          |                                                    |
+------------+----------------------------------------------------+
| ``Perspect | A matrix useful for perspective 2D geometric       |
| iveMatrix` | transformations.                                   |
| `          |                                                    |
+------------+----------------------------------------------------+
| ``Point``  | A point in two-dimensional euclidean space.        |
+------------+----------------------------------------------------+
| ``Point3d` | A point in three-dimensional euclidean space.      |
| `          |                                                    |
+------------+----------------------------------------------------+
| ``Point3dE |                                                    |
| numerator` |                                                    |
| `          |                                                    |
+------------+----------------------------------------------------+
| ``Point3dL |                                                    |
| ist``      |                                                    |
+------------+----------------------------------------------------+
| ``PointEnu |                                                    |
| merator``  |                                                    |
+------------+----------------------------------------------------+
| ``PointLis | It is a list.                                      |
| t``        |                                                    |
+------------+----------------------------------------------------+
| ``Polyline | A polyline in two-dimensional euclidean space.     |
| ``         |                                                    |
+------------+----------------------------------------------------+
| ``Polyline |                                                    |
| Enumerator |                                                    |
| ``         |                                                    |
+------------+----------------------------------------------------+
| ``Polyline |                                                    |
| List``     |                                                    |
+------------+----------------------------------------------------+
| ``Pose``   | A pose consists of a location and a rotation.      |
+------------+----------------------------------------------------+
| ``Profile` | ...                                                |
| `          |                                                    |
+------------+----------------------------------------------------+
| ``ProfileE |                                                    |
| numerator` |                                                    |
| `          |                                                    |
+------------+----------------------------------------------------+
| ``ProfileL |                                                    |
| ist``      |                                                    |
+------------+----------------------------------------------------+
| ``Quadrati | The following operations are implemented:          |
| cBezier``  | operator== : comparison for equality.              |
+------------+----------------------------------------------------+
| ``Quadrati | This function object can be used to calculate the  |
| cMeanFunct | quadratic mean of a sequence of values.            |
| or``       |                                                    |
+------------+----------------------------------------------------+
| ``Quadrila | A quadrilateral in two-dimensional euclidean       |
| teral``    | space.                                             |
+------------+----------------------------------------------------+
| ``Quadrila |                                                    |
| teralEnume |                                                    |
| rator``    |                                                    |
+------------+----------------------------------------------------+
| ``Quadrila |                                                    |
| teralList` |                                                    |
| `          |                                                    |
+------------+----------------------------------------------------+
| ``Radiomet | The radiometric\_stereo\_result consists of        |
| ricStereoR | albedo, gradient (x,y), and angles of the nomals   |
| esult``    | with the camera/viewer direction.                  |
+------------+----------------------------------------------------+
| ``Range``  | A range is specified by a begin and an end.        |
+------------+----------------------------------------------------+
| ``RangeFun | This function object can be used to calculate the  |
| ctor``     | difference between the maximum and the minimum.    |
+------------+----------------------------------------------------+
| ``Ray``    | An infinite ray in two-dimensional euclidean       |
|            | space.                                             |
+------------+----------------------------------------------------+
| ``RayEnume |                                                    |
| rator``    |                                                    |
+------------+----------------------------------------------------+
| ``RayList` |                                                    |
| `          |                                                    |
+------------+----------------------------------------------------+
| ``Rectangl | The geometric rectangle primitive.                 |
| e``        |                                                    |
+------------+----------------------------------------------------+
| ``Rectangl |                                                    |
| eEnumerato |                                                    |
| r``        |                                                    |
+------------+----------------------------------------------------+
| ``Rectangl |                                                    |
| eList``    |                                                    |
+------------+----------------------------------------------------+
| ``Region`` | A region.                                          |
+------------+----------------------------------------------------+
| ``RegionEn |                                                    |
| umerator`` |                                                    |
+------------+----------------------------------------------------+
| ``RegionLi | A list of regions.                                 |
| st``       |                                                    |
+------------+----------------------------------------------------+
| ``RegionPy | The base class.                                    |
| ramid``    |                                                    |
+------------+----------------------------------------------------+
| ``Rgb``    | The RGB color type.                                |
+------------+----------------------------------------------------+
| ``Rgba``   | The RGBA color type.                               |
+------------+----------------------------------------------------+
| ``RigidMat | A matrix class useful for rigid 2D geometric       |
| rix``      | transformations.                                   |
+------------+----------------------------------------------------+
| ``Ring``   | The geometric ring primitive.                      |
+------------+----------------------------------------------------+
| ``RingEnum |                                                    |
| erator``   |                                                    |
+------------+----------------------------------------------------+
| ``RingList |                                                    |
| ``         |                                                    |
+------------+----------------------------------------------------+
| ``Rotation | A matrix useful for affine 2D rotation.            |
| Matrix``   |                                                    |
+------------+----------------------------------------------------+
| ``ScaleInv | Class to hold scale\_invariant\_moments of the     |
| ariantMome | second and third order.                            |
| nts``      |                                                    |
+------------+----------------------------------------------------+
| ``ScalingM | A matrix useful for affine 2D geometric scaling.   |
| atrix``    |                                                    |
+------------+----------------------------------------------------+
| ``Sector`` | The geometric sector primitive.                    |
+------------+----------------------------------------------------+
| ``SectorEn |                                                    |
| umerator`` |                                                    |
+------------+----------------------------------------------------+
| ``SectorLi |                                                    |
| st``       |                                                    |
+------------+----------------------------------------------------+
| ``SerialPo | The serial port.                                   |
| rt``       |                                                    |
+------------+----------------------------------------------------+
| ``Similari | A matrix useful for affine 2D geometric scaling.   |
| tyMatrix`` |                                                    |
+------------+----------------------------------------------------+
| ``SolidCol | A solid color brush.                               |
| orBrush``  |                                                    |
+------------+----------------------------------------------------+
| ``TcpipCli | The TCP/IP client.                                 |
| ent``      |                                                    |
+------------+----------------------------------------------------+
| ``TcpipSer | The TCP/IP server.                                 |
| ver``      |                                                    |
+------------+----------------------------------------------------+
| ``ThickLin | A thick\_line\_segment in two-dimensional          |
| eSegment`` | euclidean space.                                   |
+------------+----------------------------------------------------+
| ``ThickLin |                                                    |
| eSegmentEn |                                                    |
| umerator`` |                                                    |
+------------+----------------------------------------------------+
| ``ThickLin |                                                    |
| eSegmentLi |                                                    |
| st``       |                                                    |
+------------+----------------------------------------------------+
| ``Thicknes | The thickness describes the thickness of a frame   |
| s``        | around a rectangle.                                |
+------------+----------------------------------------------------+
| ``Translat | A matrix useful for 2D geometric translation.      |
| ionMatrix` |                                                    |
| `          |                                                    |
+------------+----------------------------------------------------+
| ``Triangle | A triangle in two-dimensional euclidean space.     |
| ``         |                                                    |
+------------+----------------------------------------------------+
| ``Triangle |                                                    |
| Enumerator |                                                    |
| ``         |                                                    |
+------------+----------------------------------------------------+
| ``Triangle |                                                    |
| List``     |                                                    |
+------------+----------------------------------------------------+
| ``Tweening | The base class for tweening functors.              |
| ``         |                                                    |
+------------+----------------------------------------------------+
| ``Tweening | Calculating functor that calculates a linear       |
| Linear``   | tweening.                                          |
+------------+----------------------------------------------------+
| ``Tweening | Calculating functor that calculates a binary       |
| Binary``   | tweening.                                          |
+------------+----------------------------------------------------+
| ``Tweening | Calculating functor that calculates a power        |
| Power1``   | tweening.                                          |
+------------+----------------------------------------------------+
| ``Tweening | Calculating functor that calculates a power        |
| Power2``   | tweening.                                          |
+------------+----------------------------------------------------+
| ``Tweening | Calculating functor that calculates a power        |
| Power3``   | tweening.                                          |
+------------+----------------------------------------------------+
| ``Tweening | Calculating functor that calculates a power        |
| Power4``   | tweening.                                          |
+------------+----------------------------------------------------+
| ``Tweening | Calculating functor that calculates a sinusoidal   |
| Sinusoidal | tweening.                                          |
| 1``        |                                                    |
+------------+----------------------------------------------------+
| ``Tweening | Calculating functor that calculates a sinusoidal   |
| Sinusoidal | tweening.                                          |
| 2``        |                                                    |
+------------+----------------------------------------------------+
| ``Tweening | Calculating functor that calculates a sinusoidal   |
| Sinusoidal | tweening.                                          |
| 3``        |                                                    |
+------------+----------------------------------------------------+
| ``Tweening | Calculating functor that calculates a sinusoidal   |
| Sinusoidal | tweening.                                          |
| 4``        |                                                    |
+------------+----------------------------------------------------+
| ``Uint16En |                                                    |
| umerator`` |                                                    |
+------------+----------------------------------------------------+
| ``Uint16Li |                                                    |
| st``       |                                                    |
+------------+----------------------------------------------------+
| ``Uint32En |                                                    |
| umerator`` |                                                    |
+------------+----------------------------------------------------+
| ``Uint32Li |                                                    |
| st``       |                                                    |
+------------+----------------------------------------------------+
| ``Vector`` | A vector in two-dimensional euclidean space.       |
+------------+----------------------------------------------------+
| ``Vector3d | A vector in three-dimensional euclidean space.     |
| ``         |                                                    |
+------------+----------------------------------------------------+
| ``Vector3d |                                                    |
| Enumerator |                                                    |
| ``         |                                                    |
+------------+----------------------------------------------------+
| ``Vector3d |                                                    |
| List``     |                                                    |
+------------+----------------------------------------------------+
| ``VectorEn |                                                    |
| umerator`` |                                                    |
+------------+----------------------------------------------------+
| ``VectorLi |                                                    |
| st``       |                                                    |
+------------+----------------------------------------------------+
| ``View``   | A template that provides a random-access view as a |
|            | basis for a mutable or a constant view.            |
+------------+----------------------------------------------------+
| ``WidgetAr | A widget\_arc displays an arc on a surface.        |
| c``        |                                                    |
+------------+----------------------------------------------------+
| ``WidgetBa | The abstract widget\_base type.                    |
| se``       |                                                    |
+------------+----------------------------------------------------+
| ``WidgetBa |                                                    |
| seEnumerat |                                                    |
| or``       |                                                    |
+------------+----------------------------------------------------+
| ``WidgetBa |                                                    |
| seList``   |                                                    |
+------------+----------------------------------------------------+
| ``WidgetBo | A widget\_box displays a box (axis parallel        |
| x``        | rectangle) on a surface.                           |
+------------+----------------------------------------------------+
| ``WidgetCi | A widget\_circle displays a circle on a surface.   |
| rcle``     |                                                    |
+------------+----------------------------------------------------+
| ``WidgetCo | A widget\_coordinate\_system sets a left-handed or |
| ordinateSy | right-handed coordinate system on a surface.       |
| stem``     |                                                    |
+------------+----------------------------------------------------+
| ``WidgetCu | A widget\_curve displays a curve on a surface.     |
| rve``      |                                                    |
+------------+----------------------------------------------------+
| ``WidgetCo | A widget\_count\_tool is a tool for interactive    |
| untTool``  | counting.                                          |
+------------+----------------------------------------------------+
| ``WidgetEl | A widget\_ellipse displays an ellipse on a         |
| lipse``    | surface.                                           |
+------------+----------------------------------------------------+
| ``WidgetHo | A widget\_horizontal\_cursor draws a horizontal    |
| rizontalCu | line.                                              |
| rsor``     |                                                    |
+------------+----------------------------------------------------+
| ``WidgetHo | A widget\_horizontal\_scale displays a scale on a  |
| rizontalSc | surface.                                           |
| ale``      |                                                    |
+------------+----------------------------------------------------+
| ``WidgetIm | A widget\_image displays an image on a surface.    |
| age``      |                                                    |
+------------+----------------------------------------------------+
| ``WidgetIn | The abstract widget\_interactive type.             |
| teractive` |                                                    |
| `          |                                                    |
+------------+----------------------------------------------------+
| ``WidgetLa | A widget\_layer implements a layer on a surface.   |
| yer``      |                                                    |
+------------+----------------------------------------------------+
| ``WidgetLa | The abstract widget\_layoutable type.              |
| youtable`` |                                                    |
+------------+----------------------------------------------------+
| ``WidgetLi | A widget\_line displays a line on a surface.       |
| ne``       |                                                    |
+------------+----------------------------------------------------+
| ``WidgetLi | A widget\_line\_segment displays a line segment on |
| neSegment` | a surface.                                         |
| `          |                                                    |
+------------+----------------------------------------------------+
| ``WidgetMo | A widget\_mouse provides interaction with the      |
| use``      | mouse on a surface.                                |
+------------+----------------------------------------------------+
| ``WidgetOr | A widget\_orthogonal\_cursor is a widget           |
| thogonalCu | displaying a pair of crossing horizontal and       |
| rsor``     | vertical lines.                                    |
+------------+----------------------------------------------------+
| ``WidgetOr | A widget\_orthogonal\_grid is a widget displaying  |
| thogonalGr | a grid of crossing horizontal and vertical lines.  |
| id``       |                                                    |
+------------+----------------------------------------------------+
| ``WidgetPa | A widget\_palette displays a palette on a surface. |
| lette``    |                                                    |
+------------+----------------------------------------------------+
| ``WidgetPo | A widget\_point displays a point on a surface.     |
| int``      |                                                    |
+------------+----------------------------------------------------+
| ``WidgetPo | A widget\_polar\_grid is a widget displaying a     |
| larGrid``  | grid of radial lines crossing a set of concentric  |
|            | circles.                                           |
+------------+----------------------------------------------------+
| ``WidgetPo | A widget\_polygon displays a polygon on a surface. |
| lygon``    |                                                    |
+------------+----------------------------------------------------+
| ``WidgetPo | A widget\_polyline displays a curve on a surface.  |
| lyline``   |                                                    |
+------------+----------------------------------------------------+
| ``WidgetQu | A widget\_quadrilateral displays a quadrilateral   |
| adrilatera | on a surface.                                      |
| l``        |                                                    |
+------------+----------------------------------------------------+
| ``WidgetRa | A widget\_ray displays a ray on a surface.         |
| y``        |                                                    |
+------------+----------------------------------------------------+
| ``WidgetRe | A widget\_rectangle displays a rectangle on a      |
| ctangle``  | surface.                                           |
+------------+----------------------------------------------------+
| ``WidgetRe | A widget\_region\_list displays a list of regions  |
| gionList`` | on a surface.                                      |
+------------+----------------------------------------------------+
| ``WidgetRu | A widget\_rule displays a rule for length          |
| le``       | measurement on a surface.                          |
+------------+----------------------------------------------------+
| ``WidgetSc | A widget\_scale\_bar displays ...                  |
| aleBar``   |                                                    |
+------------+----------------------------------------------------+
| ``WidgetTe | A widget\_text is usually used as a child of some  |
| xt``       | parent widget in order to render text within the   |
|            | bounds of this parent.                             |
+------------+----------------------------------------------------+
| ``WidgetTe | A widget\_text\_box is a rectangle that can be set |
| xtBox``    | and/or interactively moved on a drawing surface.   |
+------------+----------------------------------------------------+
| ``WidgetTh | A widget\_thick\_line\_segment displays a line     |
| ickLineSeg | segment on a surface.                              |
| ment``     |                                                    |
+------------+----------------------------------------------------+
| ``WidgetTr | A widget\_triangle displays a triangle on a        |
| iangle``   | surface.                                           |
+------------+----------------------------------------------------+
| ``WidgetVe | A widget\_vertical\_cursor draws a vertical line.  |
| rticalCurs |                                                    |
| or``       |                                                    |
+------------+----------------------------------------------------+
| ``WidgetVe | A widget\_vertical\_scale displays a scale on a    |
| rticalScal | surface.                                           |
| e``        |                                                    |
+------------+----------------------------------------------------+
| ``WidgetVi | A widget\_view displays the contents of a view on  |
| ew``       | a surface.                                         |
+------------+----------------------------------------------------+
| ``Xyz``    | The xyz color type.                                |
+------------+----------------------------------------------------+
| ``OcrChara | The ocr\_character as returned by the CharReader.  |
| cter``     |                                                    |
+------------+----------------------------------------------------+
| ``OcrChara |                                                    |
| cterList`` |                                                    |
+------------+----------------------------------------------------+
| ``OcrChara |                                                    |
| cterEnumer |                                                    |
| ator``     |                                                    |
+------------+----------------------------------------------------+
| ``OcrChara | The ocr\_character\_info as returned by the        |
| cterInfo`` | CharReader.                                        |
+------------+----------------------------------------------------+
| ``OcrChara |                                                    |
| cterInfoLi |                                                    |
| st``       |                                                    |
+------------+----------------------------------------------------+
| ``OcrChara |                                                    |
| cterInfoEn |                                                    |
| umerator`` |                                                    |
+------------+----------------------------------------------------+
