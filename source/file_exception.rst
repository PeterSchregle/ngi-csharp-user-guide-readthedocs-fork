Class *Exception*
-----------------

The basic NGI exception type.

**Namespace:** Ngi

**Module:**

The class **Exception** implements the following interfaces:

+---------------------------+
| Interface                 |
+===========================+
| ``IEquatableException``   |
+---------------------------+

The class **Exception** contains the following properties:

+---------------+-------+-------+--------------------------+
| Property      | Get   | Set   | Description              |
+===============+=======+=======+==========================+
| ``Message``   | \*    |       | The exception message.   |
+---------------+-------+-------+--------------------------+
| ``Id``        | \*    |       | The exception id.        |
+---------------+-------+-------+--------------------------+

The class **Exception** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

The basic NGI exception type serves as a base class for an exception hierarchy and it provides a few standard services for derived exceptions. It provides an integer id and an additional informational message for advanced error handling.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Exception*
^^^^^^^^^^^^^^^^^^^^^^^

``Exception()``

Constructor.

Constructors
~~~~~~~~~~~~

Constructor *Exception*
^^^^^^^^^^^^^^^^^^^^^^^

``Exception(System.String message)``

Constructor.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+

Constructor *Exception*
^^^^^^^^^^^^^^^^^^^^^^^

``Exception(System.String message, System.UInt32 id)``

Constructor.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+
| ``id``        | ``System.UInt32``   | The exception id.        |
+---------------+---------------------+--------------------------+

Properties
~~~~~~~~~~

Property *Message*
^^^^^^^^^^^^^^^^^^

``System.String Message``

The exception message.

Property *Id*
^^^^^^^^^^^^^

``System.UInt32 Id``

The exception id.

This id is unique within it's own exception class only.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
