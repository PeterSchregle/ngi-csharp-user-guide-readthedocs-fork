Class *SimilarityMatrix*
------------------------

A matrix useful for affine 2D geometric scaling.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **SimilarityMatrix** implements the following interfaces:

+----------------------------------+
| Interface                        |
+==================================+
| ``IEquatableSimilarityMatrix``   |
+----------------------------------+
| ``ISerializable``                |
+----------------------------------+

The class **SimilarityMatrix** contains the following properties:

+-------------------+-------+-------+----------------------------------------------------+
| Property          | Get   | Set   | Description                                        |
+===================+=======+=======+====================================================+
| ``Phi``           | \*    | \*    | The rotation angle.                                |
+-------------------+-------+-------+----------------------------------------------------+
| ``Translation``   | \*    | \*    | The translation vector of the matrix.              |
+-------------------+-------+-------+----------------------------------------------------+
| ``Scaling``       | \*    | \*    | The scaling vector of the matrix.                  |
+-------------------+-------+-------+----------------------------------------------------+
| ``M11``           | \*    |       | The element m11 (row 1, column 1) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M12``           | \*    |       | The element m12 (row 1, column 2) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M13``           | \*    |       | The element m13 (row 1, column 3) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M21``           | \*    |       | The element m21 (row 2, column 1) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M22``           | \*    |       | The element m22 (row 2, column 2) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M23``           | \*    |       | The element m23 (row 2, column 3) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M31``           | \*    |       | The element m31 (row 3, column 1) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M32``           | \*    |       | The element m32 (row 3, column 2) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M33``           | \*    |       | The element m33 (row 3, column 3) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``Determinant``   | \*    |       | The determinant of the matrix.                     |
+-------------------+-------+-------+----------------------------------------------------+
| ``Trace``         | \*    |       | The trace of the matrix.                           |
+-------------------+-------+-------+----------------------------------------------------+
| ``Inverse``       | \*    |       | The inverse of the matrix.                         |
+-------------------+-------+-------+----------------------------------------------------+
| ``[index]``       | \*    |       | The matrix values as a parameterized property.     |
+-------------------+-------+-------+----------------------------------------------------+

The class **SimilarityMatrix** contains the following methods:

+-----------------------+--------------------------------------------+
| Method                | Description                                |
+=======================+============================================+
| ``ResetToIdentity``   | Reset the matrix to the identity matrix.   |
+-----------------------+--------------------------------------------+

Description
~~~~~~~~~~~

The values in the matrix are arranged like this:

\| \|

.. raw:: html

   <table rows="5" cols="1">

m11 m12 m13

m21 m22 m23

m31 m32 m33

.. raw:: html

   </table>

The cells with 0 or 1 values are not stored within this object but implicit.

The determinant of such a matrix is the product of the two scaling factors.

The trace of such a matrix is the sum of the two scaling factors plus 1.

The size is implicitely 3x3 and related properties are not implemented.

The inverse can be quickly calculated by inverting the scaling factors, and the result is still a scaling matrix.

The transpose of a scaling matrix is identical to the original, so this is not implemented.

This storage assumes row-vectors and post-multiplication for transformations.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *SimilarityMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``SimilarityMatrix()``

Default constructor.

By default this is the identity matrix.

Constructors
~~~~~~~~~~~~

Constructor *SimilarityMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``SimilarityMatrix(System.Double phi, VectorDouble translation, System.Double scaling)``

Constructor.

The constructor has the following parameters:

+-------------------+---------------------+---------------------------+
| Parameter         | Type                | Description               |
+===================+=====================+===========================+
| ``phi``           | ``System.Double``   | The rotation\_angle.      |
+-------------------+---------------------+---------------------------+
| ``translation``   | ``VectorDouble``    | The translation vector.   |
+-------------------+---------------------+---------------------------+
| ``scaling``       | ``System.Double``   | The scaling factor.       |
+-------------------+---------------------+---------------------------+

Constructs a scaling matrix that scales by the given factor in both dimensions.

Constructor *SimilarityMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``SimilarityMatrix(System.Double phi, System.Double dx, System.Double dy, System.Double scaling)``

Constructor.

The constructor has the following parameters:

+---------------+---------------------+-------------------------------------------------------+
| Parameter     | Type                | Description                                           |
+===============+=====================+=======================================================+
| ``phi``       | ``System.Double``   | The rotation\_angle.                                  |
+---------------+---------------------+-------------------------------------------------------+
| ``dx``        | ``System.Double``   | The horizontal component of the translation vector.   |
+---------------+---------------------+-------------------------------------------------------+
| ``dy``        | ``System.Double``   | The vertical component of the translation vector.     |
+---------------+---------------------+-------------------------------------------------------+
| ``scaling``   | ``System.Double``   | The scaling factor.                                   |
+---------------+---------------------+-------------------------------------------------------+

Constructs a scaling matrix that scales by the given factor in both dimensions.

Constructor *SimilarityMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``SimilarityMatrix(RotationMatrix rhs)``

Copy constructor.

The constructor has the following parameters:

+-------------+----------------------+------------------------+
| Parameter   | Type                 | Description            |
+=============+======================+========================+
| ``rhs``     | ``RotationMatrix``   | The right hand side.   |
+-------------+----------------------+------------------------+

Constructor *SimilarityMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``SimilarityMatrix(TranslationMatrix rhs)``

Conversion constructor.

The constructor has the following parameters:

+-------------+-------------------------+------------------------+
| Parameter   | Type                    | Description            |
+=============+=========================+========================+
| ``rhs``     | ``TranslationMatrix``   | The right hand side.   |
+-------------+-------------------------+------------------------+

Converts a rotation matrix to a similarity matrix.

Constructor *SimilarityMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``SimilarityMatrix(RigidMatrix rhs)``

Conversion constructor.

The constructor has the following parameters:

+-------------+-------------------+------------------------+
| Parameter   | Type              | Description            |
+=============+===================+========================+
| ``rhs``     | ``RigidMatrix``   | The right hand side.   |
+-------------+-------------------+------------------------+

Converts a translation matrix to a similarity matrix.

Constructor *SimilarityMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``SimilarityMatrix(Pose rhs)``

Conversion constructor.

The constructor has the following parameters:

+-------------+------------+------------------------+
| Parameter   | Type       | Description            |
+=============+============+========================+
| ``rhs``     | ``Pose``   | The right hand side.   |
+-------------+------------+------------------------+

Converts a rigid matrix to a similarity matrix.

Constructor *SimilarityMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``SimilarityMatrix(IsotropicScalingMatrix rhs)``

Conversion constructor.

The constructor has the following parameters:

+-------------+------------------------------+------------------------+
| Parameter   | Type                         | Description            |
+=============+==============================+========================+
| ``rhs``     | ``IsotropicScalingMatrix``   | The right hand side.   |
+-------------+------------------------------+------------------------+

Converts an isotropic scaling matrix to a similarity matrix.

Properties
~~~~~~~~~~

Property *Phi*
^^^^^^^^^^^^^^

``System.Double Phi``

The rotation angle.

Property *Translation*
^^^^^^^^^^^^^^^^^^^^^^

``VectorDouble Translation``

The translation vector of the matrix.

Property *Scaling*
^^^^^^^^^^^^^^^^^^

``System.Double Scaling``

The scaling vector of the matrix.

Property *M11*
^^^^^^^^^^^^^^

``System.Double M11``

The element m11 (row 1, column 1) of the matrix.

Property *M12*
^^^^^^^^^^^^^^

``System.Double M12``

The element m12 (row 1, column 2) of the matrix.

Property *M13*
^^^^^^^^^^^^^^

``System.Double M13``

The element m13 (row 1, column 3) of the matrix.

Property *M21*
^^^^^^^^^^^^^^

``System.Double M21``

The element m21 (row 2, column 1) of the matrix.

Property *M22*
^^^^^^^^^^^^^^

``System.Double M22``

The element m22 (row 2, column 2) of the matrix.

Property *M23*
^^^^^^^^^^^^^^

``System.Double M23``

The element m23 (row 2, column 3) of the matrix.

Property *M31*
^^^^^^^^^^^^^^

``System.Double M31``

The element m31 (row 3, column 1) of the matrix.

Property *M32*
^^^^^^^^^^^^^^

``System.Double M32``

The element m32 (row 3, column 2) of the matrix.

Property *M33*
^^^^^^^^^^^^^^

``System.Double M33``

The element m33 (row 3, column 3) of the matrix.

Property *Determinant*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double Determinant``

The determinant of the matrix.

Property *Trace*
^^^^^^^^^^^^^^^^

``System.Double Trace``

The trace of the matrix.

Property *Inverse*
^^^^^^^^^^^^^^^^^^

``SimilarityMatrix Inverse``

The inverse of the matrix.

Property *[index]*
^^^^^^^^^^^^^^^^^^

``System.Double [index]``

The matrix values as a parameterized property.

This property allows you to retrieve the implicit values also.

Static Methods
~~~~~~~~~~~~~~

Method *Multiply*
^^^^^^^^^^^^^^^^^

``SimilarityMatrix Multiply(SimilarityMatrix a, SimilarityMatrix b)``

Multiply two matrices.

The method **Multiply** has the following parameters:

+-------------+------------------------+---------------+
| Parameter   | Type                   | Description   |
+=============+========================+===============+
| ``a``       | ``SimilarityMatrix``   |               |
+-------------+------------------------+---------------+
| ``b``       | ``SimilarityMatrix``   |               |
+-------------+------------------------+---------------+

The product.

Methods
~~~~~~~

Method *ResetToIdentity*
^^^^^^^^^^^^^^^^^^^^^^^^

``void ResetToIdentity()``

Reset the matrix to the identity matrix.

The identity matrix has the following form. \| 1 0 0 \| \| 0 1 0 \| \| 0 0 1 \|A reference to this object.
