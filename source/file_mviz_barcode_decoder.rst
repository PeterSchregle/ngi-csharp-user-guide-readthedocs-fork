Class *MvizBarcodeDecoder*
--------------------------

Configuration for the mViz based barcode decoder.

**Namespace:** Ngi

**Module:** BarcodeMatrixcode

The class **MvizBarcodeDecoder** implements the following interfaces:

+------------------------------------+
| Interface                          |
+====================================+
| ``IEquatableMvizBarcodeDecoder``   |
+------------------------------------+
| ``ISerializable``                  |
+------------------------------------+

The class **MvizBarcodeDecoder** contains the following properties:

+------------+--------+--------+----------------------------------------------------+
| Property   | Get    | Set    | Description                                        |
+============+========+========+====================================================+
| ``Code39`` | \*     | \*     | The code\_39 decoder.                              |
+------------+--------+--------+----------------------------------------------------+
| ``Code39Ex | \*     | \*     | The code\_39\_ext decoder.                         |
| t``        |        |        |                                                    |
+------------+--------+--------+----------------------------------------------------+
| ``Code93`` | \*     | \*     | The code\_93 decoder.                              |
+------------+--------+--------+----------------------------------------------------+
| ``Code128` | \*     | \*     | The code\_128 decoder.                             |
| `          |        |        |                                                    |
+------------+--------+--------+----------------------------------------------------+
| ``Ean8``   | \*     | \*     | The ean\_8 decoder.                                |
+------------+--------+--------+----------------------------------------------------+
| ``Ean13``  | \*     | \*     | The ean\_13 decoder.                               |
+------------+--------+--------+----------------------------------------------------+
| ``Itf``    | \*     | \*     | The ITF decoder.                                   |
+------------+--------+--------+----------------------------------------------------+
| ``UpcA``   | \*     | \*     | The upc\_a decoder.                                |
+------------+--------+--------+----------------------------------------------------+
| ``UpcE``   | \*     | \*     | The upc\_e decoder.                                |
+------------+--------+--------+----------------------------------------------------+
| ``Databar` | \*     | \*     | The databar decoder.                               |
| `          |        |        |                                                    |
+------------+--------+--------+----------------------------------------------------+
| ``DatabarL | \*     | \*     | The databar limited decoder.                       |
| imited``   |        |        |                                                    |
+------------+--------+--------+----------------------------------------------------+
| ``Codabar` | \*     | \*     | The dodabar decoder.                               |
| `          |        |        |                                                    |
+------------+--------+--------+----------------------------------------------------+
| ``Pharmaco | \*     | \*     | The pharmacode decoder.                            |
| de``       |        |        |                                                    |
+------------+--------+--------+----------------------------------------------------+
| ``Scanning | \*     | \*     | The number of scanning directions. 0: vertical, 1: |
| Directions |        |        | horizontal, 2: both, 3 and more: oblique, every    |
| ``         |        |        | 180/N degrees.                                     |
+------------+--------+--------+----------------------------------------------------+
| ``Scanning | \*     | \*     | The spacing between scanning-lines, in pixels;     |
| Density``  |        |        | small values favor the decoding rate and increase  |
|            |        |        | the running time. 5 is a good default.             |
+------------+--------+--------+----------------------------------------------------+
| ``NoiseThr | \*     | \*     | The noise threshold. Threshold level to get rid of |
| eshold``   |        |        | spurious bars caused by noise. 40 is a good        |
|            |        |        | default.                                           |
+------------+--------+--------+----------------------------------------------------+
| ``CheckQui | \*     | \*     | Check the quiet zone. When true, a quiet zone as   |
| etZone``   |        |        | large as the standard requires must be present;    |
|            |        |        | when set to false, it is advisable to disable      |
|            |        |        | other symbologies to avoid false matches.          |
+------------+--------+--------+----------------------------------------------------+
| ``MinimumB | \*     | \*     | The minimum number of bars for a successfull       |
| ars``      |        |        | decode.                                            |
+------------+--------+--------+----------------------------------------------------+

The class **MvizBarcodeDecoder** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

Various barcode symbologies can be configured as well as modes of decoding.

The following operators are implemented for a barcode\_decoder: operator == : comparison for equality. operator != : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *MvizBarcodeDecoder*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MvizBarcodeDecoder()``

Default constructor.

By default, all supported symbologies besides pharmacode are enabled. Scanning is done every 60 degrees, the spacing between scanning lines is 60 degrees, the noise threshold is 40, the quiet zone is checked, and the minimum number of bars is 3.

Constructors
~~~~~~~~~~~~

Constructor *MvizBarcodeDecoder*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MvizBarcodeDecoder(System.Boolean code39, System.Boolean code39Ext, System.Boolean code93, System.Boolean code128, System.Boolean ean8, System.Boolean ean13, System.Boolean itf, System.Boolean upcA, System.Boolean upcE, System.Boolean databar, System.Boolean databarLimited, System.Boolean codabar, System.Boolean pharmacode, System.Int32 scanningDirections, System.Int32 scanningDensity, System.Int32 noiseThreshold, System.Boolean checkQuietZone, System.Int32 minimumBars)``

Constructor.

The constructor has the following parameters:

+--------------+------------+----------------------------------------------------+
| Parameter    | Type       | Description                                        |
+==============+============+====================================================+
| ``code39``   | ``System.B | Code 39.                                           |
|              | oolean``   |                                                    |
+--------------+------------+----------------------------------------------------+
| ``code39Ext` | ``System.B | Code 39 extended.                                  |
| `            | oolean``   |                                                    |
+--------------+------------+----------------------------------------------------+
| ``code93``   | ``System.B | Code 93.                                           |
|              | oolean``   |                                                    |
+--------------+------------+----------------------------------------------------+
| ``code128``  | ``System.B | Code 128.                                          |
|              | oolean``   |                                                    |
+--------------+------------+----------------------------------------------------+
| ``ean8``     | ``System.B | EAN 8.                                             |
|              | oolean``   |                                                    |
+--------------+------------+----------------------------------------------------+
| ``ean13``    | ``System.B | EAN 13.                                            |
|              | oolean``   |                                                    |
+--------------+------------+----------------------------------------------------+
| ``itf``      | ``System.B | ITF.                                               |
|              | oolean``   |                                                    |
+--------------+------------+----------------------------------------------------+
| ``upcA``     | ``System.B | UPC A.                                             |
|              | oolean``   |                                                    |
+--------------+------------+----------------------------------------------------+
| ``upcE``     | ``System.B | UPC E.                                             |
|              | oolean``   |                                                    |
+--------------+------------+----------------------------------------------------+
| ``databar``  | ``System.B | Databar.                                           |
|              | oolean``   |                                                    |
+--------------+------------+----------------------------------------------------+
| ``databarLim | ``System.B | Databar Limited.                                   |
| ited``       | oolean``   |                                                    |
+--------------+------------+----------------------------------------------------+
| ``codabar``  | ``System.B | Codabar.                                           |
|              | oolean``   |                                                    |
+--------------+------------+----------------------------------------------------+
| ``pharmacode | ``System.B | Pharmacode.                                        |
| ``           | oolean``   |                                                    |
+--------------+------------+----------------------------------------------------+
| ``scanningDi | ``System.I | The number of scanning directions. 0: vertical, 1: |
| rections``   | nt32``     | horizontal, 2: both, 3 and more: oblique, every    |
|              |            | 180/N degrees.                                     |
+--------------+------------+----------------------------------------------------+
| ``scanningDe | ``System.I | The spacing between scanning-lines, in pixels;     |
| nsity``      | nt32``     | small values favor the decoding rate and increase  |
|              |            | the running time. 5 is a good default.             |
+--------------+------------+----------------------------------------------------+
| ``noiseThres | ``System.I | The noise threshold. Threshold level to get rid of |
| hold``       | nt32``     | spurious bars caused by noise. 40 is a good        |
|              |            | default.                                           |
+--------------+------------+----------------------------------------------------+
| ``checkQuiet | ``System.B | Check the quiet zone. When true, a quiet zone as   |
| Zone``       | oolean``   | large as the standard requires must be present;    |
|              |            | when set to false, it is advisable to disable      |
|              |            | other symbologies to avoid false matches.          |
+--------------+------------+----------------------------------------------------+
| ``minimumBar | ``System.I | The minimum number of bars for a successfull       |
| s``          | nt32``     | decode.                                            |
+--------------+------------+----------------------------------------------------+

Every supported symbology can be switched on or off by the respective boolen parameter. In addition, you can make the decoder work harder (and take longer) by setting the try\_harder parameter.

Properties
~~~~~~~~~~

Property *Code39*
^^^^^^^^^^^^^^^^^

``System.Boolean Code39``

The code\_39 decoder.

Property *Code39Ext*
^^^^^^^^^^^^^^^^^^^^

``System.Boolean Code39Ext``

The code\_39\_ext decoder.

Property *Code93*
^^^^^^^^^^^^^^^^^

``System.Boolean Code93``

The code\_93 decoder.

Property *Code128*
^^^^^^^^^^^^^^^^^^

``System.Boolean Code128``

The code\_128 decoder.

Property *Ean8*
^^^^^^^^^^^^^^^

``System.Boolean Ean8``

The ean\_8 decoder.

Property *Ean13*
^^^^^^^^^^^^^^^^

``System.Boolean Ean13``

The ean\_13 decoder.

Property *Itf*
^^^^^^^^^^^^^^

``System.Boolean Itf``

The ITF decoder.

Property *UpcA*
^^^^^^^^^^^^^^^

``System.Boolean UpcA``

The upc\_a decoder.

Property *UpcE*
^^^^^^^^^^^^^^^

``System.Boolean UpcE``

The upc\_e decoder.

Property *Databar*
^^^^^^^^^^^^^^^^^^

``System.Boolean Databar``

The databar decoder.

Property *DatabarLimited*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean DatabarLimited``

The databar limited decoder.

Property *Codabar*
^^^^^^^^^^^^^^^^^^

``System.Boolean Codabar``

The dodabar decoder.

Property *Pharmacode*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean Pharmacode``

The pharmacode decoder.

Property *ScanningDirections*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Int32 ScanningDirections``

The number of scanning directions. 0: vertical, 1: horizontal, 2: both, 3 and more: oblique, every 180/N degrees.

Property *ScanningDensity*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Int32 ScanningDensity``

The spacing between scanning-lines, in pixels; small values favor the decoding rate and increase the running time. 5 is a good default.

Property *NoiseThreshold*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Int32 NoiseThreshold``

The noise threshold. Threshold level to get rid of spurious bars caused by noise. 40 is a good default.

Property *CheckQuietZone*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean CheckQuietZone``

Check the quiet zone. When true, a quiet zone as large as the standard requires must be present; when set to false, it is advisable to disable other symbologies to avoid false matches.

Property *MinimumBars*
^^^^^^^^^^^^^^^^^^^^^^

``System.Int32 MinimumBars``

The minimum number of bars for a successfull decode.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
