Class *CameraParameterBase*
---------------------------

**Namespace:** Ngi

**Module:** Camera

The class **CameraParameterBase** contains the following properties:

+---------------------+-------+-------+---------------+
| Property            | Get   | Set   | Description   |
+=====================+=======+=======+===============+
| ``Name``            | \*    |       |               |
+---------------------+-------+-------+---------------+
| ``ToolTip``         | \*    |       |               |
+---------------------+-------+-------+---------------+
| ``Description``     | \*    |       |               |
+---------------------+-------+-------+---------------+
| ``DisplayName``     | \*    |       |               |
+---------------------+-------+-------+---------------+
| ``AccessMode``      | \*    |       |               |
+---------------------+-------+-------+---------------+
| ``Visibility``      | \*    |       |               |
+---------------------+-------+-------+---------------+
| ``IsDeprecated``    | \*    |       |               |
+---------------------+-------+-------+---------------+
| ``IsFeature``       | \*    |       |               |
+---------------------+-------+-------+---------------+
| ``IsReadable``      | \*    |       |               |
+---------------------+-------+-------+---------------+
| ``IsWritable``      | \*    |       |               |
+---------------------+-------+-------+---------------+
| ``IsImplemented``   | \*    |       |               |
+---------------------+-------+-------+---------------+
| ``IsAvailable``     | \*    |       |               |
+---------------------+-------+-------+---------------+

The class **CameraParameterBase** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``ToString``   |               |
+----------------+---------------+

The class **CameraParameterBase** contains the following enumerations:

+---------------------------+------------------------------+
| Enumeration               | Description                  |
+===========================+==============================+
| ``AccessModeType``        | TODO documentation missing   |
+---------------------------+------------------------------+
| ``VisibilityType``        | TODO documentation missing   |
+---------------------------+------------------------------+
| ``IncModeType``           | TODO documentation missing   |
+---------------------------+------------------------------+
| ``RepresentationType``    | TODO documentation missing   |
+---------------------------+------------------------------+
| ``DisplayNotationType``   | TODO documentation missing   |
+---------------------------+------------------------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Name*
^^^^^^^^^^^^^^^

``System.String Name``

Property *ToolTip*
^^^^^^^^^^^^^^^^^^

``System.String ToolTip``

Property *Description*
^^^^^^^^^^^^^^^^^^^^^^

``System.String Description``

Property *DisplayName*
^^^^^^^^^^^^^^^^^^^^^^

``System.String DisplayName``

Property *AccessMode*
^^^^^^^^^^^^^^^^^^^^^

``CameraParameterBase.AccessModeType AccessMode``

Property *Visibility*
^^^^^^^^^^^^^^^^^^^^^

``CameraParameterBase.VisibilityType Visibility``

Property *IsDeprecated*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsDeprecated``

Property *IsFeature*
^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFeature``

Property *IsReadable*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadable``

Property *IsWritable*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsWritable``

Property *IsImplemented*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsImplemented``

Property *IsAvailable*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsAvailable``

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Enumerations
~~~~~~~~~~~~

Enumeration *AccessModeType*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum AccessModeType``

TODO documentation missing

The enumeration **AccessModeType** has the following constants:

+----------+---------+---------------+
| Name     | Value   | Description   |
+==========+=========+===============+
| ``NI``   | ``0``   |               |
+----------+---------+---------------+
| ``NA``   | ``1``   |               |
+----------+---------+---------------+
| ``WO``   | ``2``   |               |
+----------+---------+---------------+
| ``RO``   | ``3``   |               |
+----------+---------+---------------+
| ``RW``   | ``4``   |               |
+----------+---------+---------------+

::

    enum AccessModeType
    {
      NI = 0,
      NA = 1,
      WO = 2,
      RO = 3,
      RW = 4,
    };

TODO documentation missing

Enumeration *VisibilityType*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum VisibilityType``

TODO documentation missing

The enumeration **VisibilityType** has the following constants:

+---------------------------+----------+---------------+
| Name                      | Value    | Description   |
+===========================+==========+===============+
| ``beginner``              | ``0``    |               |
+---------------------------+----------+---------------+
| ``expert``                | ``1``    |               |
+---------------------------+----------+---------------+
| ``guru``                  | ``2``    |               |
+---------------------------+----------+---------------+
| ``invisible``             | ``3``    |               |
+---------------------------+----------+---------------+
| ``undefinedVisibility``   | ``99``   |               |
+---------------------------+----------+---------------+

::

    enum VisibilityType
    {
      beginner = 0,
      expert = 1,
      guru = 2,
      invisible = 3,
      undefinedVisibility = 99,
    };

TODO documentation missing

Enumeration *IncModeType*
^^^^^^^^^^^^^^^^^^^^^^^^^

``enum IncModeType``

TODO documentation missing

The enumeration **IncModeType** has the following constants:

+----------------------+---------+---------------+
| Name                 | Value   | Description   |
+======================+=========+===============+
| ``noIncrement``      | ``0``   |               |
+----------------------+---------+---------------+
| ``fixedIncrement``   | ``1``   |               |
+----------------------+---------+---------------+
| ``listIncrement``    | ``2``   |               |
+----------------------+---------+---------------+

::

    enum IncModeType
    {
      noIncrement = 0,
      fixedIncrement = 1,
      listIncrement = 2,
    };

TODO documentation missing

Enumeration *RepresentationType*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum RepresentationType``

TODO documentation missing

The enumeration **RepresentationType** has the following constants:

+-------------------+---------+---------------+
| Name              | Value   | Description   |
+===================+=========+===============+
| ``linear``        | ``0``   |               |
+-------------------+---------+---------------+
| ``logarithmic``   | ``1``   |               |
+-------------------+---------+---------------+
| ``boolean``       | ``2``   |               |
+-------------------+---------+---------------+
| ``pureNumber``    | ``3``   |               |
+-------------------+---------+---------------+
| ``hexNumber``     | ``4``   |               |
+-------------------+---------+---------------+
| ``ipv4Address``   | ``5``   |               |
+-------------------+---------+---------------+
| ``macAddress``    | ``6``   |               |
+-------------------+---------+---------------+

::

    enum RepresentationType
    {
      linear = 0,
      logarithmic = 1,
      boolean = 2,
      pureNumber = 3,
      hexNumber = 4,
      ipv4Address = 5,
      macAddress = 6,
    };

TODO documentation missing

Enumeration *DisplayNotationType*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum DisplayNotationType``

TODO documentation missing

The enumeration **DisplayNotationType** has the following constants:

+--------------------+---------+---------------+
| Name               | Value   | Description   |
+====================+=========+===============+
| ``fnAutomatic``    | ``0``   |               |
+--------------------+---------+---------------+
| ``fnFixed``        | ``1``   |               |
+--------------------+---------+---------------+
| ``fnScientific``   | ``2``   |               |
+--------------------+---------+---------------+

::

    enum DisplayNotationType
    {
      fnAutomatic = 0,
      fnFixed = 1,
      fnScientific = 2,
    };

TODO documentation missing
