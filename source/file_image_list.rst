Class *ImageList*
-----------------

It is a list.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **ImageList** implements the following interfaces:

+----------------------------------+
| Interface                        |
+==================================+
| ``IListImageListImageVariant``   |
+----------------------------------+

The class **ImageList** contains the following variant parameters:

+-------------+-----------------------------------------+
| Variant     | Description                             |
+=============+=========================================+
| ``Image``   | TODO no brief description for variant   |
+-------------+-----------------------------------------+

The class **ImageList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **ImageList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Image*
^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Image** has the following types:

+-----------------------+
| Type                  |
+=======================+
| ``ImageByte``         |
+-----------------------+
| ``ImageUInt16``       |
+-----------------------+
| ``ImageUInt32``       |
+-----------------------+
| ``ImageDouble``       |
+-----------------------+
| ``ImageRgbByte``      |
+-----------------------+
| ``ImageRgbUInt16``    |
+-----------------------+
| ``ImageRgbUInt32``    |
+-----------------------+
| ``ImageRgbDouble``    |
+-----------------------+
| ``ImageRgbaByte``     |
+-----------------------+
| ``ImageRgbaUInt16``   |
+-----------------------+
| ``ImageRgbaUInt32``   |
+-----------------------+
| ``ImageRgbaDouble``   |
+-----------------------+
| ``ImageHlsByte``      |
+-----------------------+
| ``ImageHlsUInt16``    |
+-----------------------+
| ``ImageHlsDouble``    |
+-----------------------+
| ``ImageHsiByte``      |
+-----------------------+
| ``ImageHsiUInt16``    |
+-----------------------+
| ``ImageHsiDouble``    |
+-----------------------+
| ``ImageLabByte``      |
+-----------------------+
| ``ImageLabUInt16``    |
+-----------------------+
| ``ImageLabDouble``    |
+-----------------------+
| ``ImageXyzByte``      |
+-----------------------+
| ``ImageXyzUInt16``    |
+-----------------------+
| ``ImageXyzDouble``    |
+-----------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *ImageList*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``Image [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``ImageEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(Image item)``

The method **Add** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``item``    | ``Image``   |               |
+-------------+-------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(Image item)``

The method **Contains** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``item``    | ``Image``   |               |
+-------------+-------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(Image item)``

The method **Remove** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``item``    | ``Image``   |               |
+-------------+-------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(Image item)``

The method **IndexOf** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``item``    | ``Image``   |               |
+-------------+-------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, Image item)``

The method **Insert** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``item``    | ``Image``          |               |
+-------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
