Class *BarcodeDecoder*
----------------------

Configuration for the barcode decoder.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **BarcodeDecoder** implements the following interfaces:

+--------------------------------+
| Interface                      |
+================================+
| ``IEquatableBarcodeDecoder``   |
+--------------------------------+
| ``ISerializable``              |
+--------------------------------+

The class **BarcodeDecoder** contains the following properties:

+-----------------------+-------+-------+-----------------------------------------------------------------+
| Property              | Get   | Set   | Description                                                     |
+=======================+=======+=======+=================================================================+
| ``Code39``            | \*    | \*    | The code\_39 decoder.                                           |
+-----------------------+-------+-------+-----------------------------------------------------------------+
| ``Code93``            | \*    | \*    | The code\_93 decoder.                                           |
+-----------------------+-------+-------+-----------------------------------------------------------------+
| ``Code128``           | \*    | \*    | The code\_128 decoder.                                          |
+-----------------------+-------+-------+-----------------------------------------------------------------+
| ``Ean8``              | \*    | \*    | The ean\_8 decoder.                                             |
+-----------------------+-------+-------+-----------------------------------------------------------------+
| ``Ean13``             | \*    | \*    | The ean\_13 decoder.                                            |
+-----------------------+-------+-------+-----------------------------------------------------------------+
| ``Itf``               | \*    | \*    | The ITF decoder.                                                |
+-----------------------+-------+-------+-----------------------------------------------------------------+
| ``UpcA``              | \*    | \*    | The upc\_a decoder.                                             |
+-----------------------+-------+-------+-----------------------------------------------------------------+
| ``UpcE``              | \*    | \*    | The upc\_e decoder.                                             |
+-----------------------+-------+-------+-----------------------------------------------------------------+
| ``UpcEanExtension``   | \*    | \*    | The upc\_ean\_extension decoder.                                |
+-----------------------+-------+-------+-----------------------------------------------------------------+
| ``Binarizer``         | \*    | \*    | The binarizer mode: global (default for 1D decoder) or local.   |
+-----------------------+-------+-------+-----------------------------------------------------------------+
| ``TryHarder``         | \*    | \*    | Try harder to decode.                                           |
+-----------------------+-------+-------+-----------------------------------------------------------------+

The class **BarcodeDecoder** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

The class **BarcodeDecoder** contains the following enumerations:

+---------------------+------------------------------+
| Enumeration         | Description                  |
+=====================+==============================+
| ``BinarizerMode``   | TODO documentation missing   |
+---------------------+------------------------------+

Description
~~~~~~~~~~~

Various barcode symbologies can be configured as well as modes of decoding.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *BarcodeDecoder*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``BarcodeDecoder()``

Default constructor.

By default, all supported symbologies are enabled. However, try\_harder is disabled.

Constructors
~~~~~~~~~~~~

Constructor *BarcodeDecoder*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``BarcodeDecoder(System.Boolean code39, System.Boolean code93, System.Boolean code128, System.Boolean ean8, System.Boolean ean13, System.Boolean itf, System.Boolean upcA, System.Boolean upcE, System.Boolean upcEanExtension, BarcodeDecoder.BinarizerMode binarizer, System.Boolean tryHarder)``

Constructor.

The constructor has the following parameters:

+-----------------------+------------------------------------+---------------------------------------------------------------------------+
| Parameter             | Type                               | Description                                                               |
+=======================+====================================+===========================================================================+
| ``code39``            | ``System.Boolean``                 | Code 39.                                                                  |
+-----------------------+------------------------------------+---------------------------------------------------------------------------+
| ``code93``            | ``System.Boolean``                 | Code 93.                                                                  |
+-----------------------+------------------------------------+---------------------------------------------------------------------------+
| ``code128``           | ``System.Boolean``                 | Code 128.                                                                 |
+-----------------------+------------------------------------+---------------------------------------------------------------------------+
| ``ean8``              | ``System.Boolean``                 | EAN 8.                                                                    |
+-----------------------+------------------------------------+---------------------------------------------------------------------------+
| ``ean13``             | ``System.Boolean``                 | EAN 13.                                                                   |
+-----------------------+------------------------------------+---------------------------------------------------------------------------+
| ``itf``               | ``System.Boolean``                 | ITF.                                                                      |
+-----------------------+------------------------------------+---------------------------------------------------------------------------+
| ``upcA``              | ``System.Boolean``                 | UPC A.                                                                    |
+-----------------------+------------------------------------+---------------------------------------------------------------------------+
| ``upcE``              | ``System.Boolean``                 | UPC E.                                                                    |
+-----------------------+------------------------------------+---------------------------------------------------------------------------+
| ``upcEanExtension``   | ``System.Boolean``                 | UPC EAN Extension.                                                        |
+-----------------------+------------------------------------+---------------------------------------------------------------------------+
| ``binarizer``         | ``BarcodeDecoder.BinarizerMode``   | The binarizer mode.                                                       |
+-----------------------+------------------------------------+---------------------------------------------------------------------------+
| ``tryHarder``         | ``System.Boolean``                 | If set, tries harder to find the code and usually takes longer as well.   |
+-----------------------+------------------------------------+---------------------------------------------------------------------------+

Every supported symbology can be switched on or off by the respective boolen parameter. In addition, you can make the decoder work harder (and take longer) by setting the try\_harder parameter.

Properties
~~~~~~~~~~

Property *Code39*
^^^^^^^^^^^^^^^^^

``System.Boolean Code39``

The code\_39 decoder.

Property *Code93*
^^^^^^^^^^^^^^^^^

``System.Boolean Code93``

The code\_93 decoder.

Property *Code128*
^^^^^^^^^^^^^^^^^^

``System.Boolean Code128``

The code\_128 decoder.

Property *Ean8*
^^^^^^^^^^^^^^^

``System.Boolean Ean8``

The ean\_8 decoder.

Property *Ean13*
^^^^^^^^^^^^^^^^

``System.Boolean Ean13``

The ean\_13 decoder.

Property *Itf*
^^^^^^^^^^^^^^

``System.Boolean Itf``

The ITF decoder.

Property *UpcA*
^^^^^^^^^^^^^^^

``System.Boolean UpcA``

The upc\_a decoder.

Property *UpcE*
^^^^^^^^^^^^^^^

``System.Boolean UpcE``

The upc\_e decoder.

Property *UpcEanExtension*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean UpcEanExtension``

The upc\_ean\_extension decoder.

Property *Binarizer*
^^^^^^^^^^^^^^^^^^^^

``BarcodeDecoder.BinarizerMode Binarizer``

The binarizer mode: global (default for 1D decoder) or local.

Property *TryHarder*
^^^^^^^^^^^^^^^^^^^^

``System.Boolean TryHarder``

Try harder to decode.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Enumerations
~~~~~~~~~~~~

Enumeration *BinarizerMode*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum BinarizerMode``

TODO documentation missing

The enumeration **BinarizerMode** has the following constants:

+--------------+---------+---------------+
| Name         | Value   | Description   |
+==============+=========+===============+
| ``global``   | ``0``   |               |
+--------------+---------+---------------+
| ``local``    | ``1``   |               |
+--------------+---------+---------------+

::

    enum BinarizerMode
    {
      global = 0,
      local = 1,
    };

TODO documentation missing
