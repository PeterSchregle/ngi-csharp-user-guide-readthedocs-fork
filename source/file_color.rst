Class *Color*
-------------

The color type.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Color** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``IEquatableColor``          |
+------------------------------+
| ``ISerializable``            |
+------------------------------+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **Color** contains the following properties:

+-------------+-------+-------+-----------------------------------------+
| Property    | Get   | Set   | Description                             |
+=============+=======+=======+=========================================+
| ``Red``     | \*    | \*    | The red\_ color component property.     |
+-------------+-------+-------+-----------------------------------------+
| ``Green``   | \*    | \*    | The green\_ color component property.   |
+-------------+-------+-------+-----------------------------------------+
| ``Blue``    | \*    | \*    | The blue\_ color component property.    |
+-------------+-------+-------+-----------------------------------------+

The class **Color** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Color*
^^^^^^^^^^^^^^^^^^^

``Color()``

Default constructor.

This constructor sets the three components to 0. This corresponds to a color that is pure black.

Constructors
~~~~~~~~~~~~

Constructor *Color*
^^^^^^^^^^^^^^^^^^^

``Color(System.Byte gray)``

Standard constructor.

The constructor has the following parameters:

+-------------+-------------------+-----------------------+
| Parameter   | Type              | Description           |
+=============+===================+=======================+
| ``gray``    | ``System.Byte``   | The gray component.   |
+-------------+-------------------+-----------------------+

This constructor initializes the three components with a gray value.

The constructor also serves as a conversion operator from the scalar type T to type color.

Constructor *Color*
^^^^^^^^^^^^^^^^^^^

``Color(System.Byte red, System.Byte green, System.Byte blue)``

Standard constructor.

The constructor has the following parameters:

+-------------+-------------------+------------------------+
| Parameter   | Type              | Description            |
+=============+===================+========================+
| ``red``     | ``System.Byte``   | The red component.     |
+-------------+-------------------+------------------------+
| ``green``   | ``System.Byte``   | The green component.   |
+-------------+-------------------+------------------------+
| ``blue``    | ``System.Byte``   | The blue component.    |
+-------------+-------------------+------------------------+

This constructor initializes the three components with different values.

Properties
~~~~~~~~~~

Property *Red*
^^^^^^^^^^^^^^

``System.Byte Red``

The red\_ color component property.

Property *Green*
^^^^^^^^^^^^^^^^

``System.Byte Green``

The green\_ color component property.

Property *Blue*
^^^^^^^^^^^^^^^

``System.Byte Blue``

The blue\_ color component property.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO no brief description for variant

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO no description for variant
