Class *Index3d*
---------------

A three-dimensional, zero-based index into a buffer.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Index3d** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``IEquatableIndex3d``        |
+------------------------------+
| ``ISerializable``            |
+------------------------------+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **Index3d** contains the following properties:

+------------+-------+-------+----------------------------------+
| Property   | Get   | Set   | Description                      |
+============+=======+=======+==================================+
| ``X``      | \*    | \*    | The horizontal index position.   |
+------------+-------+-------+----------------------------------+
| ``Y``      | \*    | \*    | The vertical index position.     |
+------------+-------+-------+----------------------------------+
| ``Z``      | \*    | \*    | The planar index position.       |
+------------+-------+-------+----------------------------------+

The class **Index3d** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

This is used to specify a position within a buffer. The position is zero- based, i.e. the leftmost horizontal position is 0, the topmost vertical position is 0, and the nearest planar position is 0.

Together with an extent\_3d, this is used to specify a three-dimensional, rectangular subset of a buffer.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Index3d*
^^^^^^^^^^^^^^^^^^^^^

``Index3d()``

Constructs default index\_3d (0, 0, 0).

Constructors
~~~~~~~~~~~~

Constructor *Index3d*
^^^^^^^^^^^^^^^^^^^^^

``Index3d(System.Int32 x)``

Constructs specific index\_3d.

The constructor has the following parameters:

+-------------+--------------------+----------------------------------+
| Parameter   | Type               | Description                      |
+=============+====================+==================================+
| ``x``       | ``System.Int32``   | The horizontal index position.   |
+-------------+--------------------+----------------------------------+

Constructor *Index3d*
^^^^^^^^^^^^^^^^^^^^^

``Index3d(System.Int32 x, System.Int32 y)``

Constructs specific index\_3d from index (conversion).

The constructor has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Constructor *Index3d*
^^^^^^^^^^^^^^^^^^^^^

``Index3d(System.Int32 x, System.Int32 y, System.Int32 z)``

Constructs specific index from point (conversion).

The constructor has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``z``       | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Constructor *Index3d*
^^^^^^^^^^^^^^^^^^^^^

``Index3d(Index size)``

Constructs specific index from point\_3d (conversion).

The constructor has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``size``    | ``Index``   |               |
+-------------+-------------+---------------+

Properties
~~~~~~~~~~

Property *X*
^^^^^^^^^^^^

``System.Int32 X``

The horizontal index position.

Property *Y*
^^^^^^^^^^^^

``System.Int32 Y``

The vertical index position.

Property *Z*
^^^^^^^^^^^^

``System.Int32 Z``

The planar index position.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO no brief description for variant

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO no description for variant
