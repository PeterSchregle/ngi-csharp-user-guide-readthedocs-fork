Class *TriangleEnumerator*
--------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **TriangleEnumerator** implements the following interfaces:

+----------------------------------------------------+
| Interface                                          |
+====================================================+
| ``IEnumerator``                                    |
+----------------------------------------------------+
| ``IEnumeratorTriangleEnumeratorTriangleVariant``   |
+----------------------------------------------------+

The class **TriangleEnumerator** contains the following variant parameters:

+----------------+-----------------------------------------+
| Variant        | Description                             |
+================+=========================================+
| ``Triangle``   | TODO no brief description for variant   |
+----------------+-----------------------------------------+

The class **TriangleEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **TriangleEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Triangle*
^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Triangle** has the following types:

+----------------------+
| Type                 |
+======================+
| ``TriangleInt32``    |
+----------------------+
| ``TriangleDouble``   |
+----------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Triangle Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
