Class *WidgetRectangle*
-----------------------

A widget\_rectangle displays a rectangle on a surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetRectangle** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetRectangle** contains the following properties:

+----------------+-------+-------+-----------------------------------------+
| Property       | Get   | Set   | Description                             |
+================+=======+=======+=========================================+
| ``Position``   | \*    | \*    | The position of the widget.             |
+----------------+-------+-------+-----------------------------------------+
| ``Outline``    | \*    | \*    | The outline of the widget\_rectangle.   |
+----------------+-------+-------+-----------------------------------------+
| ``Fill``       | \*    | \*    | The fill of the widget.                 |
+----------------+-------+-------+-----------------------------------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_rectangle. These settings affect the look and the behavior of the widget\_rectangle.

The widget\_rectangle supports interactivity via the widget\_interactive base class, i.e. it can be selected, move around. In general it can react on mouse and keyboard interactions. This widget supports layout.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetRectangle()``

Default constructor.

The default constructor creates a widget with default settings. By default the rectangle has zero radii at the origin, the outline is one-pixel wide black and the fill is black transparent.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``Rectangle Position``

The position of the widget.

The position of the widget\_rectangle is specified with a rectangle.

Property *Outline*
^^^^^^^^^^^^^^^^^^

``PenByte Outline``

The outline of the widget\_rectangle.

The outline of the widget\_rectangle is specified with a pen<>.

Property *Fill*
^^^^^^^^^^^^^^^

``SolidColorBrushByte Fill``

The fill of the widget.

The fill of the widget\_rectangle is specified with a brush.
