Class *RegionList*
------------------

A list of regions.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **RegionList** implements the following interfaces:

+-------------------+
| Interface         |
+===================+
| ``IListRegion``   |
+-------------------+

The class **RegionList** contains the following properties:

+-------------------+-------+-------+-----------------------------------+
| Property          | Get   | Set   | Description                       |
+===================+=======+=======+===================================+
| ``Bounds``        | \*    |       | The bounds of the region\_list.   |
+-------------------+-------+-------+-----------------------------------+
| ``Count``         | \*    |       |                                   |
+-------------------+-------+-------+-----------------------------------+
| ``IsFixedSize``   | \*    |       |                                   |
+-------------------+-------+-------+-----------------------------------+
| ``IsReadOnly``    | \*    |       |                                   |
+-------------------+-------+-------+-----------------------------------+
| ``[index]``       | \*    | \*    |                                   |
+-------------------+-------+-------+-----------------------------------+

The class **RegionList** contains the following methods:

+-----------------------------+------------------------------------------------------------+
| Method                      | Description                                                |
+=============================+============================================================+
| ``Translate``               | Translate a region\_list.                                  |
+-----------------------------+------------------------------------------------------------+
| ``SetUnion``                | Union of all regions in the region list.                   |
+-----------------------------+------------------------------------------------------------+
| ``SetIntersection``         | Intersection of all regions in the region list.            |
+-----------------------------+------------------------------------------------------------+
| ``MinkowskiSubtraction``    | Minkowski subtraction of all regions in the region list.   |
+-----------------------------+------------------------------------------------------------+
| ``MinkowskiAddition``       | Minkowski addition of all regions in the region list.      |
+-----------------------------+------------------------------------------------------------+
| ``Erosion``                 | Erode all regions in the region list.                      |
+-----------------------------+------------------------------------------------------------+
| ``Dilation``                | Dilate all regions in the region list.                     |
+-----------------------------+------------------------------------------------------------+
| ``Opening``                 | Opening.                                                   |
+-----------------------------+------------------------------------------------------------+
| ``Closing``                 | Closing.                                                   |
+-----------------------------+------------------------------------------------------------+
| ``InnerBoundary``           | Inner boundary.                                            |
+-----------------------------+------------------------------------------------------------+
| ``OuterBoundary``           | Outer boundary.                                            |
+-----------------------------+------------------------------------------------------------+
| ``MorphologicalGradient``   | Morphological gradient.                                    |
+-----------------------------+------------------------------------------------------------+
| ``Downsample``              |                                                            |
+-----------------------------+------------------------------------------------------------+
| ``FillHoles``               | Fill the holes in all regions in the region list.          |
+-----------------------------+------------------------------------------------------------+
| ``GetEnumerator``           |                                                            |
+-----------------------------+------------------------------------------------------------+
| ``Add``                     |                                                            |
+-----------------------------+------------------------------------------------------------+
| ``Clear``                   |                                                            |
+-----------------------------+------------------------------------------------------------+
| ``Contains``                |                                                            |
+-----------------------------+------------------------------------------------------------+
| ``Remove``                  |                                                            |
+-----------------------------+------------------------------------------------------------+
| ``IndexOf``                 |                                                            |
+-----------------------------+------------------------------------------------------------+
| ``Insert``                  |                                                            |
+-----------------------------+------------------------------------------------------------+
| ``RemoveAt``                |                                                            |
+-----------------------------+------------------------------------------------------------+
| ``ToString``                |                                                            |
+-----------------------------+------------------------------------------------------------+

The class **RegionList** contains the following enumerations:

+------------------------------+------------------------------+
| Enumeration                  | Description                  |
+==============================+==============================+
| ``DownsampleRoundingMode``   | TODO documentation missing   |
+------------------------------+------------------------------+

Description
~~~~~~~~~~~

It is a list.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *RegionList*
^^^^^^^^^^^^^^^^^^^^^^^^

``RegionList()``

Properties
~~~~~~~~~~

Property *Bounds*
^^^^^^^^^^^^^^^^^

``BoxInt32 Bounds``

The bounds of the region\_list.

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``Region [index]``

Static Methods
~~~~~~~~~~~~~~

Method *Connection*
^^^^^^^^^^^^^^^^^^^

``RegionList Connection(Region source)``

The method **Connection** has the following parameters:

+--------------+--------------+---------------+
| Parameter    | Type         | Description   |
+==============+==============+===============+
| ``source``   | ``Region``   |               |
+--------------+--------------+---------------+

Method *Connection*
^^^^^^^^^^^^^^^^^^^

``RegionList Connection(Region source, System.Int32 distance)``

The method **Connection** has the following parameters:

+----------------+--------------------+---------------+
| Parameter      | Type               | Description   |
+================+====================+===============+
| ``source``     | ``Region``         |               |
+----------------+--------------------+---------------+
| ``distance``   | ``System.Int32``   |               |
+----------------+--------------------+---------------+

Method *SegmentMultiThreshold*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RegionList SegmentMultiThreshold(ViewLocatorByte source, DataList thresholds)``

Segments a view at multiple thresholds.

The method **SegmentMultiThreshold** has the following parameters:

+------------------+-----------------------+---------------+
| Parameter        | Type                  | Description   |
+==================+=======================+===============+
| ``source``       | ``ViewLocatorByte``   |               |
+------------------+-----------------------+---------------+
| ``thresholds``   | ``DataList``          |               |
+------------------+-----------------------+---------------+

Method *SegmentMultiThreshold*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RegionList SegmentMultiThreshold(ViewLocatorUInt16 source, Uint16List thresholds)``

Segments a view at multiple thresholds.

The method **SegmentMultiThreshold** has the following parameters:

+------------------+-------------------------+---------------+
| Parameter        | Type                    | Description   |
+==================+=========================+===============+
| ``source``       | ``ViewLocatorUInt16``   |               |
+------------------+-------------------------+---------------+
| ``thresholds``   | ``Uint16List``          |               |
+------------------+-------------------------+---------------+

Method *SegmentMultiThreshold*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RegionList SegmentMultiThreshold(ViewLocatorUInt32 source, Uint32List thresholds)``

Segments a view at multiple thresholds.

The method **SegmentMultiThreshold** has the following parameters:

+------------------+-------------------------+---------------+
| Parameter        | Type                    | Description   |
+==================+=========================+===============+
| ``source``       | ``ViewLocatorUInt32``   |               |
+------------------+-------------------------+---------------+
| ``thresholds``   | ``Uint32List``          |               |
+------------------+-------------------------+---------------+

Method *SegmentMultiThreshold*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RegionList SegmentMultiThreshold(ViewLocatorDouble source, DoubleList thresholds)``

Segments a view at multiple thresholds.

The method **SegmentMultiThreshold** has the following parameters:

+------------------+-------------------------+---------------+
| Parameter        | Type                    | Description   |
+==================+=========================+===============+
| ``source``       | ``ViewLocatorDouble``   |               |
+------------------+-------------------------+---------------+
| ``thresholds``   | ``DoubleList``          |               |
+------------------+-------------------------+---------------+

Method *SegmentMultiThreshold*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RegionList SegmentMultiThreshold(View source, object thresholds)``

Segments a view at multiple thresholds.

The method **SegmentMultiThreshold** has the following parameters:

+------------------+--------------+---------------+
| Parameter        | Type         | Description   |
+==================+==============+===============+
| ``source``       | ``View``     |               |
+------------------+--------------+---------------+
| ``thresholds``   | ``object``   |               |
+------------------+--------------+---------------+

Method *Connection*
^^^^^^^^^^^^^^^^^^^

``RegionList Connection(Region source, VectorInt32 distance)``

The method **Connection** has the following parameters:

+----------------+-------------------+---------------+
| Parameter      | Type              | Description   |
+================+===================+===============+
| ``source``     | ``Region``        |               |
+----------------+-------------------+---------------+
| ``distance``   | ``VectorInt32``   |               |
+----------------+-------------------+---------------+

Method *FillHoles*
^^^^^^^^^^^^^^^^^^

``Region FillHoles(Region regionWithHoles)``

Fill the holes in the region.

The method **FillHoles** has the following parameters:

+-----------------------+--------------+---------------+
| Parameter             | Type         | Description   |
+=======================+==============+===============+
| ``regionWithHoles``   | ``Region``   |               |
+-----------------------+--------------+---------------+

Fills the holes of a region.

Returns a new region with the holes filled.

Methods
~~~~~~~

Method *Translate*
^^^^^^^^^^^^^^^^^^

``RegionList Translate(VectorInt32 translation)``

Translate a region\_list.

The method **Translate** has the following parameters:

+-------------------+-------------------+---------------------------+
| Parameter         | Type              | Description               |
+===================+===================+===========================+
| ``translation``   | ``VectorInt32``   | The translation vector.   |
+-------------------+-------------------+---------------------------+

This function returns a translated copy of a region\_list. Each region in the list is translated by the same vector.

The translated region list.

Method *SetUnion*
^^^^^^^^^^^^^^^^^

``Region SetUnion()``

Union of all regions in the region list.

Method *SetIntersection*
^^^^^^^^^^^^^^^^^^^^^^^^

``Region SetIntersection()``

Intersection of all regions in the region list.

Method *MinkowskiSubtraction*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RegionList MinkowskiSubtraction(Region structuringElement)``

Minkowski subtraction of all regions in the region list.

The method **MinkowskiSubtraction** has the following parameters:

+--------------------------+--------------+----------------------------+
| Parameter                | Type         | Description                |
+==========================+==============+============================+
| ``structuringElement``   | ``Region``   | The structuring element.   |
+--------------------------+--------------+----------------------------+

This is done for all regions in the list.

Method *MinkowskiAddition*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``RegionList MinkowskiAddition(Region structuringElement)``

Minkowski addition of all regions in the region list.

The method **MinkowskiAddition** has the following parameters:

+--------------------------+--------------+----------------------------+
| Parameter                | Type         | Description                |
+==========================+==============+============================+
| ``structuringElement``   | ``Region``   | The structuring element.   |
+--------------------------+--------------+----------------------------+

This is done for all regions in the list.

Method *Erosion*
^^^^^^^^^^^^^^^^

``RegionList Erosion(Region structuringElement)``

Erode all regions in the region list.

The method **Erosion** has the following parameters:

+--------------------------+--------------+----------------------------+
| Parameter                | Type         | Description                |
+==========================+==============+============================+
| ``structuringElement``   | ``Region``   | The structuring element.   |
+--------------------------+--------------+----------------------------+

Erosion smoothes the region boundary and decreases its area.

Structuring elements should be centered on the origin.

This is done for all regions in the list.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``RegionList Dilation(Region structuringElement)``

Dilate all regions in the region list.

The method **Dilation** has the following parameters:

+--------------------------+--------------+----------------------------+
| Parameter                | Type         | Description                |
+==========================+==============+============================+
| ``structuringElement``   | ``Region``   | The structuring element.   |
+--------------------------+--------------+----------------------------+

Dilation smoothes the region boundary and increases its area.

Structuring elements should be centered on the origin.

This is done for all regions in the list.

Method *Opening*
^^^^^^^^^^^^^^^^

``RegionList Opening(Region structuringElement)``

Opening.

The method **Opening** has the following parameters:

+--------------------------+--------------+----------------------------+
| Parameter                | Type         | Description                |
+==========================+==============+============================+
| ``structuringElement``   | ``Region``   | The structuring element.   |
+--------------------------+--------------+----------------------------+

Opening is implemented by an erosion followed by a minkowski addition. Structures smaller than the structuring element are removed and the region boundaries are smoothed.

Structuring elements should be centered on the origin.

This is done for all regions in the list.

Method *Closing*
^^^^^^^^^^^^^^^^

``RegionList Closing(Region structuringElement)``

Closing.

The method **Closing** has the following parameters:

+--------------------------+--------------+----------------------------+
| Parameter                | Type         | Description                |
+==========================+==============+============================+
| ``structuringElement``   | ``Region``   | The structuring element.   |
+--------------------------+--------------+----------------------------+

Closing is implemented by a dilation followed by a minkowski subtraction. Gaps and holes smaller than the structuring element are closed and the region boundaries are smoothed.

Structuring elements should be centered on the origin.

This is done for all regions in the list.

Method *InnerBoundary*
^^^^^^^^^^^^^^^^^^^^^^

``RegionList InnerBoundary()``

Inner boundary.

The inner boundary is calculated by taking the difference of the original region minus the eroded region. The inner boundary lies excusively inside the region.

This is done for all regions in the list.

Method *OuterBoundary*
^^^^^^^^^^^^^^^^^^^^^^

``RegionList OuterBoundary()``

Outer boundary.

The outer boundary is calculated by taking the difference of the dilated region minus the original region. The outer boundary lies excusively outside the region.

This is done for all regions in the list.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RegionList MorphologicalGradient(Region structuringElement)``

Morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+--------------+----------------------------+
| Parameter                | Type         | Description                |
+==========================+==============+============================+
| ``structuringElement``   | ``Region``   | The structuring element.   |
+--------------------------+--------------+----------------------------+

The morphological gradient is calculated by taking the difference of the dilated region minus the eroded region.

This is done for all regions in the list.

Method *Downsample*
^^^^^^^^^^^^^^^^^^^

``RegionList Downsample(System.Double xFactor, System.Double yFactor, RegionList.DownsampleRoundingMode roundingMode)``

The method **Downsample** has the following parameters:

+--------------------+-----------------------------------------+---------------------------------------+
| Parameter          | Type                                    | Description                           |
+====================+=========================================+=======================================+
| ``xFactor``        | ``System.Double``                       | The horizontal downsampling factor.   |
+--------------------+-----------------------------------------+---------------------------------------+
| ``yFactor``        | ``System.Double``                       | The vertical downsampling factor.     |
+--------------------+-----------------------------------------+---------------------------------------+
| ``roundingMode``   | ``RegionList.DownsampleRoundingMode``   | The rounding mode                     |
+--------------------+-----------------------------------------+---------------------------------------+

Method *FillHoles*
^^^^^^^^^^^^^^^^^^

``RegionList FillHoles()``

Fill the holes in all regions in the region list.

For all regions in the list, fill the holes of each region.

Returns a new region\_list where all regions have its holes filled.

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``RegionEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(Region item)``

The method **Add** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``item``    | ``Region``   |               |
+-------------+--------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(Region item)``

The method **Contains** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``item``    | ``Region``   |               |
+-------------+--------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(Region item)``

The method **Remove** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``item``    | ``Region``   |               |
+-------------+--------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(Region item)``

The method **IndexOf** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``item``    | ``Region``   |               |
+-------------+--------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, Region item)``

The method **Insert** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``item``    | ``Region``         |               |
+-------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Enumerations
~~~~~~~~~~~~

Enumeration *DownsampleRoundingMode*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum DownsampleRoundingMode``

TODO documentation missing

The enumeration **DownsampleRoundingMode** has the following constants:

+-----------------+---------+---------------+
| Name            | Value   | Description   |
+=================+=========+===============+
| ``round``       | ``0``   |               |
+-----------------+---------+---------------+
| ``castToInt``   | ``1``   |               |
+-----------------+---------+---------------+
| ``shrink``      | ``2``   |               |
+-----------------+---------+---------------+
| ``grow``        | ``3``   |               |
+-----------------+---------+---------------+

::

    enum DownsampleRoundingMode
    {
      round = 0,
      castToInt = 1,
      shrink = 2,
      grow = 3,
    };

TODO documentation missing
