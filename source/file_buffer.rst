Class *Buffer*
--------------

The buffer class is a multidimensional container that arranges elements of a given type in a linear arrangement.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Buffer** implements the following interfaces:

+------------------------------------------+
| Interface                                |
+==========================================+
| ``IEquatableBufferBufferPixelVariant``   |
+------------------------------------------+

The class **Buffer** contains the following variant parameters:

+-------------+-----------------------------------------+
| Variant     | Description                             |
+=============+=========================================+
| ``Pixel``   | TODO no brief description for variant   |
+-------------+-----------------------------------------+

The class **Buffer** contains the following properties:

+-------------------------+-------+-------+---------------+
| Property                | Get   | Set   | Description   |
+=========================+=======+=======+===============+
| ``Width``               | \*    |       |               |
+-------------------------+-------+-------+---------------+
| ``Height``              | \*    |       |               |
+-------------------------+-------+-------+---------------+
| ``Depth``               | \*    |       |               |
+-------------------------+-------+-------+---------------+
| ``Format``              | \*    |       |               |
+-------------------------+-------+-------+---------------+
| ``Data``                | \*    |       |               |
+-------------------------+-------+-------+---------------+
| ``Volume``              | \*    |       |               |
+-------------------------+-------+-------+---------------+
| ``Center``              | \*    |       |               |
+-------------------------+-------+-------+---------------+
| ``Size``                | \*    |       |               |
+-------------------------+-------+-------+---------------+
| ``Begin``               | \*    |       |               |
+-------------------------+-------+-------+---------------+
| ``View``                | \*    |       |               |
+-------------------------+-------+-------+---------------+
| ``MinimalPixelValue``   | \*    |       |               |
+-------------------------+-------+-------+---------------+
| ``MaximalPixelValue``   | \*    |       |               |
+-------------------------+-------+-------+---------------+
| ``ZeroPixelValue``      | \*    |       |               |
+-------------------------+-------+-------+---------------+
| ``OnePixelValue``       | \*    |       |               |
+-------------------------+-------+-------+---------------+
| ``MiddlePixelValue``    | \*    |       |               |
+-------------------------+-------+-------+---------------+

The class **Buffer** contains the following methods:

+-----------------------+---------------+
| Method                | Description   |
+=======================+===============+
| ``Clear``             |               |
+-----------------------+---------------+
| ``Clear``             |               |
+-----------------------+---------------+
| ``ToString``          |               |
+-----------------------+---------------+
| ``GetPixelValue``     |               |
+-----------------------+---------------+
| ``SetPixelValue``     |               |
+-----------------------+---------------+
| ``ParsePixelValue``   |               |
+-----------------------+---------------+

The class **Buffer** contains the following enumerations:

+-------------------+------------------------------+
| Enumeration       | Description                  |
+===================+==============================+
| ``PixelFormat``   | TODO documentation missing   |
+-------------------+------------------------------+

Description
~~~~~~~~~~~

The following schematic shows the logical layout of a buffer object. |image0|

A buffer can accommodate one-dimensional buffer (height == 1 and depth == 1), like a look-up-table, histogram or a profile, a two-dimensional buffer (depth == 1), like an image, or a three-dimensional buffer, like an image stack or an image sequence.

The buffer class provides data access via three-dimensional locators and views.

The storage area of a buffer is guaranteed to be allocated on a cache line boundary. This has been done to assure optimal access. However, the width of a buffer is arbitrary. If you want to guarantee that the width is a multiple of the cache line length, you can allocate the buffer sufficiently wider, and then use a narrower view to cut the width down to the desired width for viewing. The buffer\_base class.

Variants
~~~~~~~~

Variant *Pixel*
^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Pixel** has the following types:

+------------------+
| Type             |
+==================+
| ``Byte``         |
+------------------+
| ``UInt16``       |
+------------------+
| ``UInt32``       |
+------------------+
| ``Double``       |
+------------------+
| ``RgbByte``      |
+------------------+
| ``RgbUInt16``    |
+------------------+
| ``RgbUInt32``    |
+------------------+
| ``RgbDouble``    |
+------------------+
| ``RgbaByte``     |
+------------------+
| ``RgbaUInt16``   |
+------------------+
| ``RgbaUInt32``   |
+------------------+
| ``RgbaDouble``   |
+------------------+
| ``HlsByte``      |
+------------------+
| ``HlsUInt16``    |
+------------------+
| ``HlsDouble``    |
+------------------+
| ``HsiByte``      |
+------------------+
| ``HsiUInt16``    |
+------------------+
| ``HsiDouble``    |
+------------------+
| ``LabByte``      |
+------------------+
| ``LabUInt16``    |
+------------------+
| ``LabDouble``    |
+------------------+
| ``XyzByte``      |
+------------------+
| ``XyzUInt16``    |
+------------------+
| ``XyzDouble``    |
+------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Buffer*
^^^^^^^^^^^^^^^^^^^^

``Buffer()``

Constructs an empty buffer.

Properties
~~~~~~~~~~

Property *Width*
^^^^^^^^^^^^^^^^

``System.Int32 Width``

Property *Height*
^^^^^^^^^^^^^^^^^

``System.Int32 Height``

Property *Depth*
^^^^^^^^^^^^^^^^

``System.Int32 Depth``

Property *Format*
^^^^^^^^^^^^^^^^^

``Buffer.PixelFormat Format``

Property *Data*
^^^^^^^^^^^^^^^

``System.IntPtr Data``

Property *Volume*
^^^^^^^^^^^^^^^^^

``System.Int32 Volume``

Property *Center*
^^^^^^^^^^^^^^^^^

``Index3d Center``

Property *Size*
^^^^^^^^^^^^^^^

``Extent3d Size``

Property *Begin*
^^^^^^^^^^^^^^^^

``Locator Begin``

Property *View*
^^^^^^^^^^^^^^^

``View View``

Property *MinimalPixelValue*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Object MinimalPixelValue``

Property *MaximalPixelValue*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Object MaximalPixelValue``

Property *ZeroPixelValue*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Object ZeroPixelValue``

Property *OnePixelValue*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.Object OnePixelValue``

Property *MiddlePixelValue*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Object MiddlePixelValue``

Methods
~~~~~~~

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Clear*
^^^^^^^^^^^^^^

``void Clear(System.Object value)``

The method **Clear** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``value``   | ``System.Object``   |               |
+-------------+---------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Method *GetPixelValue*
^^^^^^^^^^^^^^^^^^^^^^

``System.Object GetPixelValue(Index3d index)``

The method **GetPixelValue** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``index``   | ``Index3d``   |               |
+-------------+---------------+---------------+

Method *SetPixelValue*
^^^^^^^^^^^^^^^^^^^^^^

``void SetPixelValue(Index3d index, System.Object value)``

The method **SetPixelValue** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``index``   | ``Index3d``         |               |
+-------------+---------------------+---------------+
| ``value``   | ``System.Object``   |               |
+-------------+---------------------+---------------+

Method *ParsePixelValue*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.Object ParsePixelValue(System.String value)``

The method **ParsePixelValue** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``value``   | ``System.String``   |               |
+-------------+---------------------+---------------+

Enumerations
~~~~~~~~~~~~

Enumeration *PixelFormat*
^^^^^^^^^^^^^^^^^^^^^^^^^

``enum PixelFormat``

TODO documentation missing

The enumeration **PixelFormat** has the following constants:

+----------------+----------+---------------+
| Name           | Value    | Description   |
+================+==========+===============+
| ``unknown``    | ``0``    |               |
+----------------+----------+---------------+
| ``gray8u``     | ``1``    |               |
+----------------+----------+---------------+
| ``gray16u``    | ``2``    |               |
+----------------+----------+---------------+
| ``gray32u``    | ``3``    |               |
+----------------+----------+---------------+
| ``gray64f``    | ``4``    |               |
+----------------+----------+---------------+
| ``alpha8u``    | ``5``    |               |
+----------------+----------+---------------+
| ``alpha16u``   | ``6``    |               |
+----------------+----------+---------------+
| ``alpha32u``   | ``7``    |               |
+----------------+----------+---------------+
| ``alpha64f``   | ``8``    |               |
+----------------+----------+---------------+
| ``rgb8u``      | ``9``    |               |
+----------------+----------+---------------+
| ``rgb16u``     | ``10``   |               |
+----------------+----------+---------------+
| ``rgb32u``     | ``11``   |               |
+----------------+----------+---------------+
| ``rgb64f``     | ``12``   |               |
+----------------+----------+---------------+
| ``rgba8u``     | ``13``   |               |
+----------------+----------+---------------+
| ``rgba16u``    | ``14``   |               |
+----------------+----------+---------------+
| ``rgba32u``    | ``15``   |               |
+----------------+----------+---------------+
| ``rgba64f``    | ``16``   |               |
+----------------+----------+---------------+
| ``cmy8u``      | ``17``   |               |
+----------------+----------+---------------+
| ``cmy16u``     | ``18``   |               |
+----------------+----------+---------------+
| ``cmy32u``     | ``19``   |               |
+----------------+----------+---------------+
| ``cmy64f``     | ``20``   |               |
+----------------+----------+---------------+
| ``hls8u``      | ``21``   |               |
+----------------+----------+---------------+
| ``hls16u``     | ``22``   |               |
+----------------+----------+---------------+
| ``hls32u``     | ``23``   |               |
+----------------+----------+---------------+
| ``hls64f``     | ``24``   |               |
+----------------+----------+---------------+
| ``hsi8u``      | ``25``   |               |
+----------------+----------+---------------+
| ``hsi16u``     | ``26``   |               |
+----------------+----------+---------------+
| ``hsi32u``     | ``27``   |               |
+----------------+----------+---------------+
| ``hsi64f``     | ``28``   |               |
+----------------+----------+---------------+
| ``lab8u``      | ``29``   |               |
+----------------+----------+---------------+
| ``lab16u``     | ``30``   |               |
+----------------+----------+---------------+
| ``lab32u``     | ``31``   |               |
+----------------+----------+---------------+
| ``lab64f``     | ``32``   |               |
+----------------+----------+---------------+
| ``luv8u``      | ``33``   |               |
+----------------+----------+---------------+
| ``luv16u``     | ``34``   |               |
+----------------+----------+---------------+
| ``luv32u``     | ``35``   |               |
+----------------+----------+---------------+
| ``luv64f``     | ``36``   |               |
+----------------+----------+---------------+
| ``lchab8u``    | ``37``   |               |
+----------------+----------+---------------+
| ``lchab16u``   | ``38``   |               |
+----------------+----------+---------------+
| ``lchab32u``   | ``39``   |               |
+----------------+----------+---------------+
| ``lchab64f``   | ``40``   |               |
+----------------+----------+---------------+
| ``lchuv8u``    | ``41``   |               |
+----------------+----------+---------------+
| ``lchuv16u``   | ``42``   |               |
+----------------+----------+---------------+
| ``lchuv32u``   | ``43``   |               |
+----------------+----------+---------------+
| ``lchuv64f``   | ``44``   |               |
+----------------+----------+---------------+
| ``xyz8u``      | ``45``   |               |
+----------------+----------+---------------+
| ``xyz16u``     | ``46``   |               |
+----------------+----------+---------------+
| ``xyz32u``     | ``47``   |               |
+----------------+----------+---------------+
| ``xyz64f``     | ``48``   |               |
+----------------+----------+---------------+
| ``xyY8u``      | ``49``   |               |
+----------------+----------+---------------+
| ``xyY16u``     | ``50``   |               |
+----------------+----------+---------------+
| ``xyY32u``     | ``51``   |               |
+----------------+----------+---------------+
| ``xyY64f``     | ``52``   |               |
+----------------+----------+---------------+

::

    enum PixelFormat
    {
      unknown = 0,
      gray8u = 1,
      gray16u = 2,
      gray32u = 3,
      gray64f = 4,
      alpha8u = 5,
      alpha16u = 6,
      alpha32u = 7,
      alpha64f = 8,
      rgb8u = 9,
      rgb16u = 10,
      rgb32u = 11,
      rgb64f = 12,
      rgba8u = 13,
      rgba16u = 14,
      rgba32u = 15,
      rgba64f = 16,
      cmy8u = 17,
      cmy16u = 18,
      cmy32u = 19,
      cmy64f = 20,
      hls8u = 21,
      hls16u = 22,
      hls32u = 23,
      hls64f = 24,
      hsi8u = 25,
      hsi16u = 26,
      hsi32u = 27,
      hsi64f = 28,
      lab8u = 29,
      lab16u = 30,
      lab32u = 31,
      lab64f = 32,
      luv8u = 33,
      luv16u = 34,
      luv32u = 35,
      luv64f = 36,
      lchab8u = 37,
      lchab16u = 38,
      lchab32u = 39,
      lchab64f = 40,
      lchuv8u = 41,
      lchuv16u = 42,
      lchuv32u = 43,
      lchuv64f = 44,
      xyz8u = 45,
      xyz16u = 46,
      xyz32u = 47,
      xyz64f = 48,
      xyY8u = 49,
      xyY16u = 50,
      xyY32u = 51,
      xyY64f = 52,
    };

TODO documentation missing

.. |image0| image:: images/buffer.png

