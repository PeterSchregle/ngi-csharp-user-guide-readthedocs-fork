Class *RectangleEnumerator*
---------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **RectangleEnumerator** implements the following interfaces:

+----------------------------+
| Interface                  |
+============================+
| ``IEnumerator``            |
+----------------------------+
| ``IEnumeratorRectangle``   |
+----------------------------+

The class **RectangleEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **RectangleEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Rectangle Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
