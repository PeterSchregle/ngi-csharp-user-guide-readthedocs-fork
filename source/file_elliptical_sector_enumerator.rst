Class *EllipticalSectorEnumerator*
----------------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **EllipticalSectorEnumerator** implements the following interfaces:

+-----------------------------------+
| Interface                         |
+===================================+
| ``IEnumerator``                   |
+-----------------------------------+
| ``IEnumeratorEllipticalSector``   |
+-----------------------------------+

The class **EllipticalSectorEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **EllipticalSectorEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``EllipticalSector Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
