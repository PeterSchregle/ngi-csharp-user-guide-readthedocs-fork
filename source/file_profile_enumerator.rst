Class *ProfileEnumerator*
-------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **ProfileEnumerator** implements the following interfaces:

+--------------------------------------------------+
| Interface                                        |
+==================================================+
| ``IEnumerator``                                  |
+--------------------------------------------------+
| ``IEnumeratorProfileEnumeratorProfileVariant``   |
+--------------------------------------------------+

The class **ProfileEnumerator** contains the following variant parameters:

+---------------+-----------------------------------------+
| Variant       | Description                             |
+===============+=========================================+
| ``Profile``   | TODO no brief description for variant   |
+---------------+-----------------------------------------+

The class **ProfileEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **ProfileEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Profile*
^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Profile** has the following types:

+-------------------------+
| Type                    |
+=========================+
| ``ProfileByte``         |
+-------------------------+
| ``ProfileUInt16``       |
+-------------------------+
| ``ProfileUInt32``       |
+-------------------------+
| ``ProfileDouble``       |
+-------------------------+
| ``ProfileRgbByte``      |
+-------------------------+
| ``ProfileRgbUInt16``    |
+-------------------------+
| ``ProfileRgbUInt32``    |
+-------------------------+
| ``ProfileRgbDouble``    |
+-------------------------+
| ``ProfileRgbaByte``     |
+-------------------------+
| ``ProfileRgbaUInt16``   |
+-------------------------+
| ``ProfileRgbaUInt32``   |
+-------------------------+
| ``ProfileRgbaDouble``   |
+-------------------------+
| ``ProfileHlsByte``      |
+-------------------------+
| ``ProfileHlsUInt16``    |
+-------------------------+
| ``ProfileHlsDouble``    |
+-------------------------+
| ``ProfileHsiByte``      |
+-------------------------+
| ``ProfileHsiUInt16``    |
+-------------------------+
| ``ProfileHsiDouble``    |
+-------------------------+
| ``ProfileLabByte``      |
+-------------------------+
| ``ProfileLabUInt16``    |
+-------------------------+
| ``ProfileLabDouble``    |
+-------------------------+
| ``ProfileXyzByte``      |
+-------------------------+
| ``ProfileXyzUInt16``    |
+-------------------------+
| ``ProfileXyzDouble``    |
+-------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Profile Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
