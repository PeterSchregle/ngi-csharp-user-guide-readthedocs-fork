Class *WidgetBase*
------------------

The abstract widget\_base type.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetBase** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetBase** contains the following properties:

+-----------------------+-------+-------+--------------------------------------------+
| Property              | Get   | Set   | Description                                |
+=======================+=======+=======+============================================+
| ``Id``                | \*    | \*    | The identification id.                     |
+-----------------------+-------+-------+--------------------------------------------+
| ``Cookie``            | \*    | \*    | The callback cookie.                       |
+-----------------------+-------+-------+--------------------------------------------+
| ``Visible``           | \*    | \*    | Specifies whether the widget is visible.   |
+-----------------------+-------+-------+--------------------------------------------+
| ``Frozen``            | \*    | \*    | Specifies whether the widget is frozen.    |
+-----------------------+-------+-------+--------------------------------------------+
| ``Parent``            | \*    |       | The parent widget\_base, if any.           |
+-----------------------+-------+-------+--------------------------------------------+
| ``Children``          | \*    |       | The children collection.                   |
+-----------------------+-------+-------+--------------------------------------------+
| ``RenderTransform``   | \*    | \*    | The render transform.                      |
+-----------------------+-------+-------+--------------------------------------------+

The class **WidgetBase** contains the following methods:

+--------------------------+----------------------------------------+
| Method                   | Description                            |
+==========================+========================================+
| ``AddWidget``            | Add a widget to the list.              |
+--------------------------+----------------------------------------+
| ``FindWidget``           | Find a widget in the list.             |
+--------------------------+----------------------------------------+
| ``RemoveAllWidgets``     | Remove all child widgets.              |
+--------------------------+----------------------------------------+
| ``RemoveWidget``         | Remove a widget from the child list.   |
+--------------------------+----------------------------------------+
| ``MoveWidgetToFront``    | Move a widget visually to the front.   |
+--------------------------+----------------------------------------+
| ``MoveWidgetForward``    | Move a widget visually forward.        |
+--------------------------+----------------------------------------+
| ``MoveWidgetToBack``     | Move a widget visually to the back.    |
+--------------------------+----------------------------------------+
| ``MoveWidgetBackward``   | Move a widget visually backward.       |
+--------------------------+----------------------------------------+
| ``Layout``               | Layout the widget and its children.    |
+--------------------------+----------------------------------------+
| ``Invalidate``           | Invalidate the widget\_base.           |
+--------------------------+----------------------------------------+
| ``InvalidateLayout``     | Invalidate the layout.                 |
+--------------------------+----------------------------------------+

Description
~~~~~~~~~~~

Widgets are graphical objects that can be used in the NGI graphical system for various purposes, such as visualization and interaction.

This abstract base class provides a few basic facilities that all widgets share by inheriting from it. A widget has an id and a cookie that can be used for identification. It also has a visibility status and it maintains a connection to a parent widget or a parent display.

Properties that affect the visual appearance of the widget cause automatic invalidation of the widget when they are changed. This ensures that the widgets are redrawn properly when they are changed.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetBase*
^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetBase()``

Default constructor.

The default constructor creates a widget\_base with default settings.

Properties
~~~~~~~~~~

Property *Id*
^^^^^^^^^^^^^

``System.Int32 Id``

The identification id.

The id can hold an arbitrary integer for identification purposes.

The id can be used to search for some specific widget in a tree of widgets.

Property *Cookie*
^^^^^^^^^^^^^^^^^

``System.IntPtr Cookie``

The callback cookie.

This cookie can hold arbitrary data in the size of a pointer. This is 32 bit on a 32 bit OS and 64 bit on a 64 bit OS. This is done to provide means to better connect user code with ngi code.

Property *Visible*
^^^^^^^^^^^^^^^^^^

``System.Boolean Visible``

Specifies whether the widget is visible.

This property allows you to show and hide widgets as needed. If a widget is hidden, it cannot be interactively modified, since it is not visible. Also, all of its children are not visible also.

Property *Frozen*
^^^^^^^^^^^^^^^^^

``System.Boolean Frozen``

Specifies whether the widget is frozen.

This property allows you to enable and disable widget invalidation as needed. If a widget is frozen, invalidation is halted.

Property *Parent*
^^^^^^^^^^^^^^^^^

``WidgetBase Parent``

The parent widget\_base, if any.

Property *Children*
^^^^^^^^^^^^^^^^^^^

``WidgetBaseList Children``

The children collection.

Property *RenderTransform*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``AffineMatrix RenderTransform``

The render transform.

Methods
~~~~~~~

Method *AddWidget*
^^^^^^^^^^^^^^^^^^

``void AddWidget(WidgetBase widget)``

Add a widget to the list.

The method **AddWidget** has the following parameters:

+--------------+------------------+--------------------------+
| Parameter    | Type             | Description              |
+==============+==================+==========================+
| ``widget``   | ``WidgetBase``   | Pointer to the widget.   |
+--------------+------------------+--------------------------+

If the display does not manage the ownership of the widget, you must make sure that the widget lives as long as the display. If the widget is deleted earlier, this may cause an invalid access and a crash. Usually you pass the address of a stack allocated widget in this case.

If the display manages the ownership of the widget, it takes care of deleting the widget once it is no longer needed. This also means that your code does not need to delete the widget. Usually you pass the address of a heap allocated widget in this case.

Method *FindWidget*
^^^^^^^^^^^^^^^^^^^

``System.Int32 FindWidget(WidgetBase widget)``

Find a widget in the list.

The method **FindWidget** has the following parameters:

+--------------+------------------+---------------------------------+
| Parameter    | Type             | Description                     |
+==============+==================+=================================+
| ``widget``   | ``WidgetBase``   | Shared pointer to the widget.   |
+--------------+------------------+---------------------------------+

/return The function returns the zero-based index in the list if the widget was found. Otherwise, it returns -1.

Method *RemoveAllWidgets*
^^^^^^^^^^^^^^^^^^^^^^^^^

``void RemoveAllWidgets()``

Remove all child widgets.

You can use this function to remove all child widgets that you have previously added with the add\_widget() method in one shot.

Method *RemoveWidget*
^^^^^^^^^^^^^^^^^^^^^

``void RemoveWidget(WidgetBase widget)``

Remove a widget from the child list.

The method **RemoveWidget** has the following parameters:

+--------------+------------------+---------------------------------+
| Parameter    | Type             | Description                     |
+==============+==================+=================================+
| ``widget``   | ``WidgetBase``   | Shared pointer to the widget.   |
+--------------+------------------+---------------------------------+

Method *MoveWidgetToFront*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void MoveWidgetToFront(WidgetBase widget)``

Move a widget visually to the front.

The method **MoveWidgetToFront** has the following parameters:

+--------------+------------------+---------------------------------+
| Parameter    | Type             | Description                     |
+==============+==================+=================================+
| ``widget``   | ``WidgetBase``   | Shared pointer to the widget.   |
+--------------+------------------+---------------------------------+

The naming of this function is related to the visual appearance: the widget is to appear on top. Actually, in the internal widget list, it is moved to the very back of the list.

If the widget is already at the correct position, nothing will happen.

Method *MoveWidgetForward*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void MoveWidgetForward(WidgetBase widget)``

Move a widget visually forward.

The method **MoveWidgetForward** has the following parameters:

+--------------+------------------+---------------------------------+
| Parameter    | Type             | Description                     |
+==============+==================+=================================+
| ``widget``   | ``WidgetBase``   | Shared pointer to the widget.   |
+--------------+------------------+---------------------------------+

The naming of this function is related to the visual appearance: the widget is to appear nearer to the top. Actually, in the internal widget list, it is moved backward in the list.

Method *MoveWidgetToBack*
^^^^^^^^^^^^^^^^^^^^^^^^^

``void MoveWidgetToBack(WidgetBase widget)``

Move a widget visually to the back.

The method **MoveWidgetToBack** has the following parameters:

+--------------+------------------+---------------------------------+
| Parameter    | Type             | Description                     |
+==============+==================+=================================+
| ``widget``   | ``WidgetBase``   | Shared pointer to the widget.   |
+--------------+------------------+---------------------------------+

The naming of this function is related to the visual appearance: the widget is to appear below all other. Actually, in the internal widget list, it is moved to the very start of the list.

If the widget is already at the correct position, nothing will happen.

Method *MoveWidgetBackward*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void MoveWidgetBackward(WidgetBase widget)``

Move a widget visually backward.

The method **MoveWidgetBackward** has the following parameters:

+--------------+------------------+---------------------------------+
| Parameter    | Type             | Description                     |
+==============+==================+=================================+
| ``widget``   | ``WidgetBase``   | Shared pointer to the widget.   |
+--------------+------------------+---------------------------------+

The naming of this function is related to the visual appearance: the widget is to appear farther to the top. Actually, in the internal widget list, it is moved forward in the list.

Method *Layout*
^^^^^^^^^^^^^^^

``void Layout()``

Layout the widget and its children.

The default implementation recursively calls layout() for all its children in a top-down fashion.

The default implementation does not do any real layout, and thus ignores any sizes and the respective bounding boxes of its children.

Method *Invalidate*
^^^^^^^^^^^^^^^^^^^

``void Invalidate()``

Invalidate the widget\_base.

This is called to invalidate the window, either by user code or implicitly when some properties change that trigger subsequent invalidation.

Invalidation can be disabled by setting the frozen property to true, and it can be enabled by setting the frozen property to false.

Invalidation also only happens if a topmost display is connected to this widget tree and if this widget is visible. Otherwise the invalidation is meaningless and not carried out.

Invalidation is a process that affects the whole widget tree. First, the invalidation request tunnels down the whole tree from the point of origin down to the leaves. When a leave is reached, the invalidation bubbles up again until it reaches the root of the tree.

Finally, when the invalidation request has reached the root of the tree it causes a repaint of the requested area.

Method *InvalidateLayout*
^^^^^^^^^^^^^^^^^^^^^^^^^

``void InvalidateLayout()``

Invalidate the layout.

Invalidation causes a recalculation of the layout plus a redraw of the invalidated area. The layout request needs to bubble up the widget tree in a bottom-up fashion, until it reaches a top display. Once the request has bubbled up to the parent display, a layout process is started from there in a top-down fashion. This ensures that correct layout is processed for the whole widget tree, regardless where the layout is started.

Widget properties are implemented in a way that they trigger relayout, if a property is changed in a way that potentially could affect layout.

A widget bubbles up the layout request to it's immediate parent only if a) the widget is visible, b) the widget is not frozen, c) the parent chain leads up to a parent display and d) a potentially set on\_invalidate\_layout event does not cancel bubbling.

When the layout request has bubbled up to its parent, the layout request is evaluated again in the context of the parent. Only when the layout request has bubbled up to a top display, the actual layout process will continue to work down the widget tree in a top-down fashion.

Events
~~~~~~

Event *Invalidate*
^^^^^^^^^^^^^^^^^^

``System.Boolean Invalidate(WidgetBase widget)``

TODO no brief description for variant

The event **Invalidate** has the following parameters:

+--------------+------------------+---------------+
| Parameter    | Type             | Description   |
+==============+==================+===============+
| ``widget``   | ``WidgetBase``   |               |
+--------------+------------------+---------------+

TODO no description for variant

Event *InvalidateLayout*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean InvalidateLayout(WidgetBase widget)``

TODO no brief description for variant

The event **InvalidateLayout** has the following parameters:

+--------------+------------------+---------------+
| Parameter    | Type             | Description   |
+==============+==================+===============+
| ``widget``   | ``WidgetBase``   |               |
+--------------+------------------+---------------+

TODO no description for variant

Event *Draw*
^^^^^^^^^^^^

``void Draw(WidgetBase widget, GraphicsContext context)``

TODO no brief description for variant

The event **Draw** has the following parameters:

+---------------+-----------------------+---------------+
| Parameter     | Type                  | Description   |
+===============+=======================+===============+
| ``widget``    | ``WidgetBase``        |               |
+---------------+-----------------------+---------------+
| ``context``   | ``GraphicsContext``   |               |
+---------------+-----------------------+---------------+

TODO no description for variant

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO no brief description for variant

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO no description for variant
