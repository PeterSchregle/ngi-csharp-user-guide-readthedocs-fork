Class *ThickLineSegment*
------------------------

A thick\_line\_segment in two-dimensional euclidean space.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **ThickLineSegment** implements the following interfaces:

+-------------------------------------------------------------------+
| Interface                                                         |
+===================================================================+
| ``IEquatableThickLineSegmentThickLineSegmentCoordinateVariant``   |
+-------------------------------------------------------------------+
| ``ISerializable``                                                 |
+-------------------------------------------------------------------+
| ``INotifyPropertyChanged``                                        |
+-------------------------------------------------------------------+

The class **ThickLineSegment** contains the following variant parameters:

+------------------+-----------------------------------------+
| Variant          | Description                             |
+==================+=========================================+
| ``Coordinate``   | TODO no brief description for variant   |
+------------------+-----------------------------------------+

The class **ThickLineSegment** contains the following properties:

+-------------------+-------+-------+-----------------------------------------------------+
| Property          | Get   | Set   | Description                                         |
+===================+=======+=======+=====================================================+
| ``LineSegment``   | \*    | \*    | The line\_segment part of the thick line segment.   |
+-------------------+-------+-------+-----------------------------------------------------+
| ``Thickness``     | \*    | \*    | The thickness part of the thick line segment.       |
+-------------------+-------+-------+-----------------------------------------------------+

The class **ThickLineSegment** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``Move``       | Move line segment.                                      |
+----------------+---------------------------------------------------------+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

The thick\_line\_segment starts at it's origin p and extends in it's positive direction to its finite endpoint at p+d\*e.

A thick\_line\_segment can be seen as a point combined with a direction and an extent in two-dimensional space.

The following operations are implemented: operator== : comparison for equality. operator != : comparison for inequality. operator- : negation, directly implemented. operator+ : addition, implemented through boost::additive operator-= : subtraction, directly implemented. operator- : subtraction, implemented through boost::additive

Variants
~~~~~~~~

Variant *Coordinate*
^^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Coordinate** has the following types:

+--------------+
| Type         |
+==============+
| ``Int32``    |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *ThickLineSegment*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ThickLineSegment()``

Construct a thick\_line\_segment from a line\_segment and thickness.

Constructors
~~~~~~~~~~~~

Constructor *ThickLineSegment*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ThickLineSegment(LineSegment origin, System.Object thickness)``

Construct a thick\_line\_segment from a line\_segment and thickness of a different type.

The constructor has the following parameters:

+-----------------+---------------------+------------------+
| Parameter       | Type                | Description      |
+=================+=====================+==================+
| ``origin``      | ``LineSegment``     |                  |
+-----------------+---------------------+------------------+
| ``thickness``   | ``System.Object``   | The thickness.   |
+-----------------+---------------------+------------------+

Properties
~~~~~~~~~~

Property *LineSegment*
^^^^^^^^^^^^^^^^^^^^^^

``LineSegment LineSegment``

The line\_segment part of the thick line segment.

Property *Thickness*
^^^^^^^^^^^^^^^^^^^^

``System.Object Thickness``

The thickness part of the thick line segment.

Methods
~~~~~~~

Method *Move*
^^^^^^^^^^^^^

``ThickLineSegment Move(Vector movement)``

Move line segment.

The method **Move** has the following parameters:

+----------------+--------------+--------------------+
| Parameter      | Type         | Description        |
+================+==============+====================+
| ``movement``   | ``Vector``   | Movement vector.   |
+----------------+--------------+--------------------+

Moves a line segment by a vector.

The moved line segment.

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO documentation missing

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO documentation missing
