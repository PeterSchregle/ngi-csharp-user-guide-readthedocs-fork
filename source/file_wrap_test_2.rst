Class *WrapTest2*
-----------------

**Namespace:** Ngi

**Module:**

The class **WrapTest2** contains the following properties:

+-----------------+-------+-------+---------------+
| Property        | Get   | Set   | Description   |
+=================+=======+=======+===============+
| ``ValueProp``   | \*    | \*    |               |
+-----------------+-------+-------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WrapTest2*
^^^^^^^^^^^^^^^^^^^^^^^

``WrapTest2()``

Properties
~~~~~~~~~~

Property *ValueProp*
^^^^^^^^^^^^^^^^^^^^

``System.Int32 ValueProp``
