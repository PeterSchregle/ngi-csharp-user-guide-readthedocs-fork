Class *ThickLineSegmentList*
----------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **ThickLineSegmentList** implements the following interfaces:

+--------------------------------------------------------+
| Interface                                              |
+========================================================+
| ``IListThickLineSegmentListThickLineSegmentVariant``   |
+--------------------------------------------------------+

The class **ThickLineSegmentList** contains the following variant parameters:

+------------------------+-----------------------------------------+
| Variant                | Description                             |
+========================+=========================================+
| ``ThickLineSegment``   | TODO no brief description for variant   |
+------------------------+-----------------------------------------+

The class **ThickLineSegmentList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **ThickLineSegmentList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *ThickLineSegment*
^^^^^^^^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **ThickLineSegment** has the following types:

+------------------------------+
| Type                         |
+==============================+
| ``ThickLineSegmentInt32``    |
+------------------------------+
| ``ThickLineSegmentDouble``   |
+------------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *ThickLineSegmentList*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ThickLineSegmentList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``ThickLineSegment [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``ThickLineSegmentEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(ThickLineSegment item)``

The method **Add** has the following parameters:

+-------------+------------------------+---------------+
| Parameter   | Type                   | Description   |
+=============+========================+===============+
| ``item``    | ``ThickLineSegment``   |               |
+-------------+------------------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(ThickLineSegment item)``

The method **Contains** has the following parameters:

+-------------+------------------------+---------------+
| Parameter   | Type                   | Description   |
+=============+========================+===============+
| ``item``    | ``ThickLineSegment``   |               |
+-------------+------------------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(ThickLineSegment item)``

The method **Remove** has the following parameters:

+-------------+------------------------+---------------+
| Parameter   | Type                   | Description   |
+=============+========================+===============+
| ``item``    | ``ThickLineSegment``   |               |
+-------------+------------------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(ThickLineSegment item)``

The method **IndexOf** has the following parameters:

+-------------+------------------------+---------------+
| Parameter   | Type                   | Description   |
+=============+========================+===============+
| ``item``    | ``ThickLineSegment``   |               |
+-------------+------------------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, ThickLineSegment item)``

The method **Insert** has the following parameters:

+-------------+------------------------+---------------+
| Parameter   | Type                   | Description   |
+=============+========================+===============+
| ``index``   | ``System.Int32``       |               |
+-------------+------------------------+---------------+
| ``item``    | ``ThickLineSegment``   |               |
+-------------+------------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
