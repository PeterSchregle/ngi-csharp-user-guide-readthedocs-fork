Class *Quadrilateral*
---------------------

A quadrilateral in two-dimensional euclidean space.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Quadrilateral** implements the following interfaces:

+-------------------------------------------------------------+
| Interface                                                   |
+=============================================================+
| ``IEquatableQuadrilateralQuadrilateralCoordinateVariant``   |
+-------------------------------------------------------------+
| ``ISerializable``                                           |
+-------------------------------------------------------------+
| ``INotifyPropertyChanged``                                  |
+-------------------------------------------------------------+

The class **Quadrilateral** contains the following variant parameters:

+------------------+-----------------------------------------+
| Variant          | Description                             |
+==================+=========================================+
| ``Coordinate``   | TODO no brief description for variant   |
+------------------+-----------------------------------------+

The class **Quadrilateral** contains the following properties:

+-------------------+-------+-------+----------------------------------------------+
| Property          | Get   | Set   | Description                                  |
+===================+=======+=======+==============================================+
| ``P1``            | \*    | \*    | The first point of the quadrilateral.        |
+-------------------+-------+-------+----------------------------------------------+
| ``P2``            | \*    | \*    | The second point of the quadrilateral.       |
+-------------------+-------+-------+----------------------------------------------+
| ``P3``            | \*    | \*    | The third point of the quadrilateral.        |
+-------------------+-------+-------+----------------------------------------------+
| ``P4``            | \*    | \*    | The fourth point of the quadrilateral.       |
+-------------------+-------+-------+----------------------------------------------+
| ``Center``        | \*    |       | The center of the quadrilateral.             |
+-------------------+-------+-------+----------------------------------------------+
| ``BoundingBox``   | \*    |       | Get the bounding box of the quadrilateral.   |
+-------------------+-------+-------+----------------------------------------------+

The class **Quadrilateral** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``Move``       | Move quadrilateral.                                     |
+----------------+---------------------------------------------------------+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

The following operations are implemented: operator == : comparison for equality. operator != : comparison for inequality.

Variants
~~~~~~~~

Variant *Coordinate*
^^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Coordinate** has the following types:

+--------------+
| Type         |
+==============+
| ``Int32``    |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Quadrilateral*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Quadrilateral()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *Quadrilateral*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Quadrilateral(Point p1, Point p2, Point p3, Point p4)``

Construct a quadrilateral from four points.

The constructor has the following parameters:

+-------------+-------------+---------------------+
| Parameter   | Type        | Description         |
+=============+=============+=====================+
| ``p1``      | ``Point``   | The first point.    |
+-------------+-------------+---------------------+
| ``p2``      | ``Point``   | The second point.   |
+-------------+-------------+---------------------+
| ``p3``      | ``Point``   | The third point.    |
+-------------+-------------+---------------------+
| ``p4``      | ``Point``   | The fourth point.   |
+-------------+-------------+---------------------+

Properties
~~~~~~~~~~

Property *P1*
^^^^^^^^^^^^^

``Point P1``

The first point of the quadrilateral.

Property *P2*
^^^^^^^^^^^^^

``Point P2``

The second point of the quadrilateral.

Property *P3*
^^^^^^^^^^^^^

``Point P3``

The third point of the quadrilateral.

Property *P4*
^^^^^^^^^^^^^

``Point P4``

The fourth point of the quadrilateral.

Property *Center*
^^^^^^^^^^^^^^^^^

``PointDouble Center``

The center of the quadrilateral.

Property *BoundingBox*
^^^^^^^^^^^^^^^^^^^^^^

``BoxDouble BoundingBox``

Get the bounding box of the quadrilateral.

The bounding box is an axis aligned box that bounds the quadrilateral. It can optionally take a line thickness into account.

The bounding box of the quadrilateral.

Methods
~~~~~~~

Method *Move*
^^^^^^^^^^^^^

``Quadrilateral Move(Vector movement)``

Move quadrilateral.

The method **Move** has the following parameters:

+----------------+--------------+--------------------+
| Parameter      | Type         | Description        |
+================+==============+====================+
| ``movement``   | ``Vector``   | Movement vector.   |
+----------------+--------------+--------------------+

Moves a quadrilateral by a vector.

The moved quadrilateral.

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO documentation missing

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO documentation missing
