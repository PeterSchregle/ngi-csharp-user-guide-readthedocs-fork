Class *RayEnumerator*
---------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **RayEnumerator** implements the following interfaces:

+------------------------------------------+
| Interface                                |
+==========================================+
| ``IEnumerator``                          |
+------------------------------------------+
| ``IEnumeratorRayEnumeratorRayVariant``   |
+------------------------------------------+

The class **RayEnumerator** contains the following variant parameters:

+-----------+-----------------------------------------+
| Variant   | Description                             |
+===========+=========================================+
| ``Ray``   | TODO no brief description for variant   |
+-----------+-----------------------------------------+

The class **RayEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **RayEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Ray*
^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Ray** has the following types:

+-----------------+
| Type            |
+=================+
| ``RayInt32``    |
+-----------------+
| ``RayDouble``   |
+-----------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Ray Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
