Class *Palette*
---------------

...

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Palette** contains the following variant parameters:

+-------------+-----------------------------------------+
| Variant     | Description                             |
+=============+=========================================+
| ``Pixel``   | TODO no brief description for variant   |
+-------------+-----------------------------------------+

The class **Palette** contains the following methods:

+----------------------------+-----------------------------------------------------------------+
| Method                     | Description                                                     |
+============================+=================================================================+
| ``ExportPalette``          | Exports a palette to a file.                                    |
+----------------------------+-----------------------------------------------------------------+
| ``ConvertToRgbaPalette``   | Converts a palette of any type to a palette with rgba pixels.   |
+----------------------------+-----------------------------------------------------------------+
| ``ConvertToRgbaPalette``   | Converts a palette of any type to a palette with rgba pixels.   |
+----------------------------+-----------------------------------------------------------------+

Description
~~~~~~~~~~~

The base class.

Variants
~~~~~~~~

Variant *Pixel*
^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Pixel** has the following types:

+------------------+
| Type             |
+==================+
| ``Byte``         |
+------------------+
| ``UInt16``       |
+------------------+
| ``UInt32``       |
+------------------+
| ``Double``       |
+------------------+
| ``RgbByte``      |
+------------------+
| ``RgbUInt16``    |
+------------------+
| ``RgbUInt32``    |
+------------------+
| ``RgbDouble``    |
+------------------+
| ``RgbaByte``     |
+------------------+
| ``RgbaUInt16``   |
+------------------+
| ``RgbaUInt32``   |
+------------------+
| ``RgbaDouble``   |
+------------------+
| ``HlsByte``      |
+------------------+
| ``HlsUInt16``    |
+------------------+
| ``HlsDouble``    |
+------------------+
| ``HsiByte``      |
+------------------+
| ``HsiUInt16``    |
+------------------+
| ``HsiDouble``    |
+------------------+
| ``LabByte``      |
+------------------+
| ``LabUInt16``    |
+------------------+
| ``LabDouble``    |
+------------------+
| ``XyzByte``      |
+------------------+
| ``XyzUInt16``    |
+------------------+
| ``XyzDouble``    |
+------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Palette*
^^^^^^^^^^^^^^^^^^^^^

``Palette()``

Constructs an empty palette.

Constructors
~~~~~~~~~~~~

Constructor *Palette*
^^^^^^^^^^^^^^^^^^^^^

``Palette(System.Int32 width, System.Object value)``

Constructs a palette of a specific size with all elements not initialized.

The constructor has the following parameters:

+-------------+---------------------+-----------------------------------------+
| Parameter   | Type                | Description                             |
+=============+=====================+=========================================+
| ``width``   | ``System.Int32``    | The width of the constructed palette.   |
+-------------+---------------------+-----------------------------------------+
| ``value``   | ``System.Object``   |                                         |
+-------------+---------------------+-----------------------------------------+

Constructor *Palette*
^^^^^^^^^^^^^^^^^^^^^

``Palette(System.String name)``

Constructs a palette of a specific size with all elements set to a specific value.

The constructor has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``name``    | ``System.String``   |               |
+-------------+---------------------+---------------+

Static Methods
~~~~~~~~~~~~~~

Method *CreateLinearPalette*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Palette CreateLinearPalette(System.Int32 width, System.Double a, System.Double b)``

Creates a linear palette.

The method **CreateLinearPalette** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``width``   | ``System.Int32``    |               |
+-------------+---------------------+---------------+
| ``a``       | ``System.Double``   |               |
+-------------+---------------------+---------------+
| ``b``       | ``System.Double``   |               |
+-------------+---------------------+---------------+

Method *CreateMinMaxScalingPalette*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Palette CreateMinMaxScalingPalette(System.Int32 width, System.Double minimum, System.Double maximum)``

Creates a palette that scales between min and max.

The method **CreateMinMaxScalingPalette** has the following parameters:

+---------------+---------------------+---------------+
| Parameter     | Type                | Description   |
+===============+=====================+===============+
| ``width``     | ``System.Int32``    |               |
+---------------+---------------------+---------------+
| ``minimum``   | ``System.Double``   |               |
+---------------+---------------------+---------------+
| ``maximum``   | ``System.Double``   |               |
+---------------+---------------------+---------------+

Method *CreateGammaPalette*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Palette CreateGammaPalette(System.Int32 width, System.Double gamma)``

Creates a gamma palette.

The method **CreateGammaPalette** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``width``   | ``System.Int32``    |               |
+-------------+---------------------+---------------+
| ``gamma``   | ``System.Double``   |               |
+-------------+---------------------+---------------+

Method *ImportPalette*
^^^^^^^^^^^^^^^^^^^^^^

``Palette ImportPalette(System.String filename)``

Imports a palette from a file.

The method **ImportPalette** has the following parameters:

+----------------+---------------------+---------------+
| Parameter      | Type                | Description   |
+================+=====================+===============+
| ``filename``   | ``System.String``   |               |
+----------------+---------------------+---------------+

The pathname must be a complete (absolute or relative) path to a file.

The function supports palettes with 256 entries only, in grayscale or rgb unsigned char format only.

The format is raw binary. A grayscale palette must contain 256 bytes, a rgb palette must contain 768 bytes in planar format.

The loaded data in a palette.

Methods
~~~~~~~

Method *ExportPalette*
^^^^^^^^^^^^^^^^^^^^^^

``void ExportPalette(System.String filename)``

Exports a palette to a file.

The method **ExportPalette** has the following parameters:

+----------------+---------------------+-------------------------------------+
| Parameter      | Type                | Description                         |
+================+=====================+=====================================+
| ``filename``   | ``System.String``   | The pathname of the file to save.   |
+----------------+---------------------+-------------------------------------+

The pathname must be a complete (absolute or relative) path to a file.

The function supports palettes with 256 entries only, in grayscale or rgb unsigned char format only.

The format is raw binary. A grayscale palette must contain 256 bytes, a rgb palette must contain 768 bytes in planar format.

Method *ConvertToRgbaPalette*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``PaletteRgbaByte ConvertToRgbaPalette()``

Converts a palette of any type to a palette with rgba pixels.

The function allocates a palette in 32 bit rgba format. It copies the pixels from the source view into the image, performing potential color space conversion on the fly.

This is the catch-all function that can convert all types of color spaces.

Method *ConvertToRgbaPalette*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``PaletteRgbaByte ConvertToRgbaPalette(System.Double offset, System.Double scale)``

Converts a palette of any type to a palette with rgba pixels.

The method **ConvertToRgbaPalette** has the following parameters:

+--------------+---------------------+----------------------------------------------+
| Parameter    | Type                | Description                                  |
+==============+=====================+==============================================+
| ``offset``   | ``System.Double``   | The offset to be added to the pixel value.   |
+--------------+---------------------+----------------------------------------------+
| ``scale``    | ``System.Double``   | The scale factor to be applied,              |
+--------------+---------------------+----------------------------------------------+

The function allocates a palette in 32 bit rgba format. It copies the pixels from the source view into the image, while adding an offset and scaling by scale, performing potential color space conversion on the fly.

This is the catch-all function that can convert all types of color spaces.
