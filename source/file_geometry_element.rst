Class *GeometryElement*
-----------------------

The following operations are implemented: operator== : comparison for equality.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **GeometryElement** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **GeometryElement** contains the following properties:

+-------------------+-------+-------+---------------------------------------------+
| Property          | Get   | Set   | Description                                 |
+===================+=======+=======+=============================================+
| ``ElementType``   | \*    |       |                                             |
+-------------------+-------+-------+---------------------------------------------+
| ``TargetPoint``   | \*    | \*    | The target point of the geometry element.   |
+-------------------+-------+-------+---------------------------------------------+

The class **GeometryElement** contains the following enumerations:

+-----------------+------------------------------+
| Enumeration     | Description                  |
+=================+==============================+
| ``ElementIs``   | TODO documentation missing   |
+-----------------+------------------------------+

Description
~~~~~~~~~~~

operator!= : comparison for inequality. operator- : negation, directly implemented. operator+ : addition, implemented through boost::additive operator-= : subtraction, directly implemented. operator- : subtraction, implemented through boost::additive

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *GeometryElement*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``GeometryElement()``

Default constructor.

Properties
~~~~~~~~~~

Property *ElementType*
^^^^^^^^^^^^^^^^^^^^^^

``GeometryElement.ElementIs ElementType``

Property *TargetPoint*
^^^^^^^^^^^^^^^^^^^^^^

``PointDouble TargetPoint``

The target point of the geometry element.

Enumerations
~~~~~~~~~~~~

Enumeration *ElementIs*
^^^^^^^^^^^^^^^^^^^^^^^

``enum ElementIs``

TODO documentation missing

The enumeration **ElementIs** has the following constants:

+---------------------------------+---------+---------------+
| Name                            | Value   | Description   |
+=================================+=========+===============+
| ``elementIsStart``              | ``1``   |               |
+---------------------------------+---------+---------------+
| ``elementIsALineSegment``       | ``2``   |               |
+---------------------------------+---------+---------------+
| ``elementIsAnArc``              | ``3``   |               |
+---------------------------------+---------+---------------+
| ``elementIsAnEllipticalArc``    | ``4``   |               |
+---------------------------------+---------+---------------+
| ``elementIsAQuadraticBezier``   | ``5``   |               |
+---------------------------------+---------+---------------+
| ``elementIsACubicBezier``       | ``6``   |               |
+---------------------------------+---------+---------------+

::

    enum ElementIs
    {
      elementIsStart = 1,
      elementIsALineSegment = 2,
      elementIsAnArc = 3,
      elementIsAnEllipticalArc = 4,
      elementIsAQuadraticBezier = 5,
      elementIsACubicBezier = 6,
    };

TODO documentation missing

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO no brief description for variant

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO no description for variant
