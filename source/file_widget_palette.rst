Class *WidgetPalette*
---------------------

A widget\_palette displays a palette on a surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetPalette** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetPalette** contains the following variant parameters:

+------------+-----------------------------------------+
| Variant    | Description                             |
+============+=========================================+
| ``View``   | TODO no brief description for variant   |
+------------+-----------------------------------------+

The class **WidgetPalette** contains the following properties:

+-------------------+-------+-------+------------------------------------------+
| Property          | Get   | Set   | Description                              |
+===================+=======+=======+==========================================+
| ``Position``      | \*    | \*    | The position of the widget.              |
+-------------------+-------+-------+------------------------------------------+
| ``Orientation``   | \*    | \*    | The drawing orientation of the widget.   |
+-------------------+-------+-------+------------------------------------------+
| ``Alpha``         | \*    | \*    | The opacity.                             |
+-------------------+-------+-------+------------------------------------------+

The class **WidgetPalette** contains the following enumerations:

+--------------------------+------------------------------+
| Enumeration              | Description                  |
+==========================+==============================+
| ``DrawingOrientation``   | TODO documentation missing   |
+--------------------------+------------------------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_palette. These settings affect the look and the behavior of the widget\_palette.

The widget\_palette supports automatic layout via the widget\_layoutable base class. The size of the widget is determined by the size of the palette view which is displayed.

The widget\_palette supports interactivity via the widget\_interactive base class, i.e. it can be selected, move around. In general it can react on mouse and keyboard interactions. This widget displays a view.

Variants
~~~~~~~~

Variant *View*
^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **View** has the following types:

+-----------------------------+
| Type                        |
+=============================+
| ``ViewLocatorByte``         |
+-----------------------------+
| ``ViewLocatorUInt16``       |
+-----------------------------+
| ``ViewLocatorUInt32``       |
+-----------------------------+
| ``ViewLocatorDouble``       |
+-----------------------------+
| ``ViewLocatorRgbByte``      |
+-----------------------------+
| ``ViewLocatorRgbUInt16``    |
+-----------------------------+
| ``ViewLocatorRgbUInt32``    |
+-----------------------------+
| ``ViewLocatorRgbDouble``    |
+-----------------------------+
| ``ViewLocatorRgbaByte``     |
+-----------------------------+
| ``ViewLocatorRgbaUInt16``   |
+-----------------------------+
| ``ViewLocatorRgbaUInt32``   |
+-----------------------------+
| ``ViewLocatorRgbaDouble``   |
+-----------------------------+
| ``ViewLocatorHlsByte``      |
+-----------------------------+
| ``ViewLocatorHlsUInt16``    |
+-----------------------------+
| ``ViewLocatorHlsDouble``    |
+-----------------------------+
| ``ViewLocatorHsiByte``      |
+-----------------------------+
| ``ViewLocatorHsiUInt16``    |
+-----------------------------+
| ``ViewLocatorHsiDouble``    |
+-----------------------------+
| ``ViewLocatorLabByte``      |
+-----------------------------+
| ``ViewLocatorLabUInt16``    |
+-----------------------------+
| ``ViewLocatorLabDouble``    |
+-----------------------------+
| ``ViewLocatorXyzByte``      |
+-----------------------------+
| ``ViewLocatorXyzUInt16``    |
+-----------------------------+
| ``ViewLocatorXyzDouble``    |
+-----------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetPalette*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetPalette()``

Default constructor.

The default constructor creates a widget with default settings.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``BoxDouble Position``

The position of the widget.

Property *Orientation*
^^^^^^^^^^^^^^^^^^^^^^

``WidgetPalette.DrawingOrientation Orientation``

The drawing orientation of the widget.

Property *Alpha*
^^^^^^^^^^^^^^^^

``System.Double Alpha``

The opacity.

Enumerations
~~~~~~~~~~~~

Enumeration *DrawingOrientation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum DrawingOrientation``

TODO documentation missing

The enumeration **DrawingOrientation** has the following constants:

+-------------------+---------+---------------+
| Name              | Value   | Description   |
+===================+=========+===============+
| ``leftToRight``   | ``0``   |               |
+-------------------+---------+---------------+
| ``rightToLeft``   | ``1``   |               |
+-------------------+---------+---------------+
| ``bottomToTop``   | ``2``   |               |
+-------------------+---------+---------------+
| ``topToBottom``   | ``3``   |               |
+-------------------+---------+---------------+

::

    enum DrawingOrientation
    {
      leftToRight = 0,
      rightToLeft = 1,
      bottomToTop = 2,
      topToBottom = 3,
    };

TODO documentation missing
