Class *CadDrawing*
------------------

A class holding a drawing parsed from a cad file (dxf or geo).

**Namespace:** Ngi

**Module:** CadPdf

The class **CadDrawing** implements the following interfaces:

+----------------------------+
| Interface                  |
+============================+
| ``IEquatableCadDrawing``   |
+----------------------------+

The class **CadDrawing** contains the following properties:

+-------------------------+-------+-------+----------------------------------------+
| Property                | Get   | Set   | Description                            |
+=========================+=======+=======+========================================+
| ``Bounds``              | \*    | \*    | The outer dimensions of the drawing.   |
+-------------------------+-------+-------+----------------------------------------+
| ``Contours``            | \*    | \*    | The list of contours.                  |
+-------------------------+-------+-------+----------------------------------------+
| ``OuterContourIndex``   | \*    | \*    | The index of the outer contour.        |
+-------------------------+-------+-------+----------------------------------------+

The class **CadDrawing** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``Render``     | Render the drawing into an image.                       |
+----------------+---------------------------------------------------------+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

A drawing consists currently of a list of contours and an index that specifies the outer contour (hence, 1 outer, multiple inner contours).

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *CadDrawing*
^^^^^^^^^^^^^^^^^^^^^^^^

``CadDrawing()``

Constructs an empty cad\_drawing.

Properties
~~~~~~~~~~

Property *Bounds*
^^^^^^^^^^^^^^^^^

``BoxDouble Bounds``

The outer dimensions of the drawing.

Property *Contours*
^^^^^^^^^^^^^^^^^^^

``CadContourList Contours``

The list of contours.

Property *OuterContourIndex*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Int32 OuterContourIndex``

The index of the outer contour.

Methods
~~~~~~~

Method *Render*
^^^^^^^^^^^^^^^

``ImageByte Render(System.Double opticalResolution, System.Boolean filled)``

Render the drawing into an image.

The method **Render** has the following parameters:

+--------------+------------+----------------------------------------------------+
| Parameter    | Type       | Description                                        |
+==============+============+====================================================+
| ``opticalRes | ``System.D | The optical resolution of the rendering.This is    |
| olution``    | ouble``    | the size of a pixel in mm. The inverse is the      |
|              |            | number of pixels per mm, which does not need to be |
|              |            | an integral number.                                |
+--------------+------------+----------------------------------------------------+
| ``filled``   | ``System.B | Draw the contours filled (true) or outlined        |
|              | oolean``   | (false).                                           |
+--------------+------------+----------------------------------------------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
