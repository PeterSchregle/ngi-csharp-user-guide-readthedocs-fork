Class *TweeningLinear*
----------------------

Calculating functor that calculates a linear tweening.

**Namespace:** Ngi

**Module:** ImageProcessing

Description
~~~~~~~~~~~

This functor is used for the preset functions.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *TweeningLinear*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TweeningLinear()``

Default Constructor.
