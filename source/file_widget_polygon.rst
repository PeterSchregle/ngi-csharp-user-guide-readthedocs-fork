Class *WidgetPolygon*
---------------------

A widget\_polygon displays a polygon on a surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetPolygon** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetPolygon** contains the following properties:

+----------------+-------+-------+----------------------------------+
| Property       | Get   | Set   | Description                      |
+================+=======+=======+==================================+
| ``Position``   | \*    | \*    | The position of the widget.      |
+----------------+-------+-------+----------------------------------+
| ``Outline``    | \*    | \*    | The outline pen of the widget.   |
+----------------+-------+-------+----------------------------------+
| ``Fill``       | \*    | \*    | The fill brush of the widget.    |
+----------------+-------+-------+----------------------------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_polygon. These settings affect the look and the behavior of the widget\_polygon.

The widget\_polygon supports interactivity via the widget\_interactive base class, i.e. it can be selected, move around. In general it can react on mouse and keyboard interactions. A widget\_polygon has interactive widget capabilities.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetPolygon*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetPolygon()``

Standard constructor.

The standard constructor creates a widget with default settings. By default the polyline contains no points, the outline is one-pixel wide black and the fill is black transparent.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``PolylineDouble Position``

The position of the widget.

Property *Outline*
^^^^^^^^^^^^^^^^^^

``PenByte Outline``

The outline pen of the widget.

Property *Fill*
^^^^^^^^^^^^^^^

``SolidColorBrushByte Fill``

The fill brush of the widget.
