Class *Direction*
-----------------

A direction in two-dimensional euclidean space.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Direction** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``IEquatableDirection``      |
+------------------------------+
| ``ISerializable``            |
+------------------------------+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **Direction** contains the following properties:

+------------+--------+--------+----------------------------------------------------+
| Property   | Get    | Set    | Description                                        |
+============+========+========+====================================================+
| ``Alpha``  | \*     | \*     | The angle of the direction in radians.             |
+------------+--------+--------+----------------------------------------------------+
| ``Angle``  | \*     |        | The angle of the direction.                        |
+------------+--------+--------+----------------------------------------------------+
| ``Abscissa | \*     |        | The abscissa is the projection of the unit vector  |
| ``         |        |        | corresponding to this direction onto the           |
|            |        |        | horizontal x axis.                                 |
+------------+--------+--------+----------------------------------------------------+
| ``Ordinate | \*     |        | The ordinate is the projection of the unit vector  |
| ``         |        |        | corresponding to this direction onto the vertical  |
|            |        |        | y axis.                                            |
+------------+--------+--------+----------------------------------------------------+
| ``Normal`` | \*     |        | The normal direction is the direction under a      |
|            |        |        | right angle to this direction, going               |
|            |        |        | counter-clockwise, implemented by adding pi/2.     |
+------------+--------+--------+----------------------------------------------------+
| ``Opposite | \*     |        | The opposite direction is what it says,            |
| ``         |        |        | implemented by adding pi.                          |
+------------+--------+--------+----------------------------------------------------+
| ``Normaliz | \*     |        | The normalized direction returns a direction in    |
| ed``       |        |        | the same direction, but within the range from zero |
|            |        |        | to 2\*pi radians.                                  |
+------------+--------+--------+----------------------------------------------------+

The class **Direction** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``Add``        | Add direction.                                          |
+----------------+---------------------------------------------------------+
| ``Add``        | Add an angle.                                           |
+----------------+---------------------------------------------------------+
| ``Subtract``   | Subtract direction.                                     |
+----------------+---------------------------------------------------------+
| ``Subtract``   | Add an angle.                                           |
+----------------+---------------------------------------------------------+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

A direction can be seen as a vector of unit-length in two-dimensional space.

It serves to specify directions in two-dimensional space, much like angles.

The following operators are implemented for a direction: operator- : negation, directly implemented. operator+= : addition, directly implemented. operator+ : addition, implemented through boost::additive operator-= : subtraction, directly implemented. operator- : subtraction, implemented through boost::additive

Adding a scalar or another direction to a direction changes its angle.

In addition you can calculate the normal direction, which is a direction perpendicular to this direction (the angle is increased by 90° in the mathematical sense).

A direction is stored as an angle. You can retrieve this angular value, as well as calculate the abscissa and ordinate of a unit vector in the respective direction.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Direction*
^^^^^^^^^^^^^^^^^^^^^^^

``Direction()``

Default constructor.

This constructor initializes the direction pointing in the direction of the positive x-axis, which corresponds to an angle of 0 degrees.

Constructors
~~~~~~~~~~~~

Constructor *Direction*
^^^^^^^^^^^^^^^^^^^^^^^

``Direction(System.Double alpha)``

Construct a direction from an angle.

The constructor has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``alpha``   | ``System.Double``   |               |
+-------------+---------------------+---------------+

Properties
~~~~~~~~~~

Property *Alpha*
^^^^^^^^^^^^^^^^

``System.Double Alpha``

The angle of the direction in radians.

Property *Angle*
^^^^^^^^^^^^^^^^

``Angle Angle``

The angle of the direction.

Property *Abscissa*
^^^^^^^^^^^^^^^^^^^

``System.Double Abscissa``

The abscissa is the projection of the unit vector corresponding to this direction onto the horizontal x axis.

Property *Ordinate*
^^^^^^^^^^^^^^^^^^^

``System.Double Ordinate``

The ordinate is the projection of the unit vector corresponding to this direction onto the vertical y axis.

Property *Normal*
^^^^^^^^^^^^^^^^^

``Direction Normal``

The normal direction is the direction under a right angle to this direction, going counter-clockwise, implemented by adding pi/2.

You may want to normalize the returned direction in order to confine it to the range 0 to 2\*pi.

Property *Opposite*
^^^^^^^^^^^^^^^^^^^

``Direction Opposite``

The opposite direction is what it says, implemented by adding pi.

You may want to normalize the returned direction in order to confine it to the range 0 to 2\*pi.

Property *Normalized*
^^^^^^^^^^^^^^^^^^^^^

``Direction Normalized``

The normalized direction returns a direction in the same direction, but within the range from zero to 2\*pi radians.

Static Methods
~~~~~~~~~~~~~~

Method *Bisector*
^^^^^^^^^^^^^^^^^

``Direction Bisector(Direction a, Direction b)``

Returns bisecting direction.

The method **Bisector** has the following parameters:

+-------------+-----------------+---------------+
| Parameter   | Type            | Description   |
+=============+=================+===============+
| ``a``       | ``Direction``   |               |
+-------------+-----------------+---------------+
| ``b``       | ``Direction``   |               |
+-------------+-----------------+---------------+

Methods
~~~~~~~

Method *Add*
^^^^^^^^^^^^

``Direction Add(Direction direction)``

Add direction.

The method **Add** has the following parameters:

+-----------------+-----------------+------------------------+
| Parameter       | Type            | Description            |
+=================+=================+========================+
| ``direction``   | ``Direction``   | The other direction.   |
+-----------------+-----------------+------------------------+

Method *Add*
^^^^^^^^^^^^

``Direction Add(System.Double angle)``

Add an angle.

The method **Add** has the following parameters:

+-------------+---------------------+------------------------------------------------------------------------------------------------------------+
| Parameter   | Type                | Description                                                                                                |
+=============+=====================+============================================================================================================+
| ``angle``   | ``System.Double``   | The angle in radians. You can use the radians function to convert from degrees to radians, if necessary.   |
+-------------+---------------------+------------------------------------------------------------------------------------------------------------+

Method *Subtract*
^^^^^^^^^^^^^^^^^

``Direction Subtract(Direction direction)``

Subtract direction.

The method **Subtract** has the following parameters:

+-----------------+-----------------+------------------------+
| Parameter       | Type            | Description            |
+=================+=================+========================+
| ``direction``   | ``Direction``   | The other direction.   |
+-----------------+-----------------+------------------------+

Method *Subtract*
^^^^^^^^^^^^^^^^^

``Direction Subtract(System.Double angle)``

Add an angle.

The method **Subtract** has the following parameters:

+-------------+---------------------+------------------------------------------------------------------------------------------------------------+
| Parameter   | Type                | Description                                                                                                |
+=============+=====================+============================================================================================================+
| ``angle``   | ``System.Double``   | The angle in radians. You can use the radians function to convert from degrees to radians, if necessary.   |
+-------------+---------------------+------------------------------------------------------------------------------------------------------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO no brief description for variant

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO no description for variant
