Class *RigidMatrix*
-------------------

A matrix class useful for rigid 2D geometric transformations.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **RigidMatrix** implements the following interfaces:

+-----------------------------+
| Interface                   |
+=============================+
| ``IEquatableRigidMatrix``   |
+-----------------------------+
| ``ISerializable``           |
+-----------------------------+

The class **RigidMatrix** contains the following properties:

+-------------------+-------+-------+----------------------------------------------------+
| Property          | Get   | Set   | Description                                        |
+===================+=======+=======+====================================================+
| ``Phi``           | \*    | \*    | The rotation angle.                                |
+-------------------+-------+-------+----------------------------------------------------+
| ``Translation``   | \*    | \*    | The translation vector of the matrix.              |
+-------------------+-------+-------+----------------------------------------------------+
| ``M11``           | \*    |       | The element m11 (row 1, column 1) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M12``           | \*    |       | The element m12 (row 1, column 2) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M13``           | \*    |       | The element m13 (row 1, column 3) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M21``           | \*    |       | The element m21 (row 2, column 1) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M22``           | \*    |       | The element m22 (row 2, column 2) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M23``           | \*    |       | The element m23 (row 2, column 3) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M31``           | \*    |       | The element m31 (row 3, column 1) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M32``           | \*    |       | The element m32 (row 3, column 2) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M33``           | \*    |       | The element m33 (row 3, column 3) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``[index]``       | \*    |       | The matrix values as a parameterized property.     |
+-------------------+-------+-------+----------------------------------------------------+
| ``Determinant``   | \*    |       | The determinant of the matrix.                     |
+-------------------+-------+-------+----------------------------------------------------+
| ``Trace``         | \*    |       | The trace of the matrix.                           |
+-------------------+-------+-------+----------------------------------------------------+
| ``Inverse``       | \*    |       | The inverse of the matrix.                         |
+-------------------+-------+-------+----------------------------------------------------+

The class **RigidMatrix** contains the following methods:

+-----------------------+--------------------------------------------+
| Method                | Description                                |
+=======================+============================================+
| ``ResetToIdentity``   | Reset the matrix to the identity matrix.   |
+-----------------------+--------------------------------------------+
| ``Rotate``            | Rotate the matrix in place.                |
+-----------------------+--------------------------------------------+
| ``Translate``         | Translate the matrix in place.             |
+-----------------------+--------------------------------------------+

Description
~~~~~~~~~~~

The values in the matrix are arranged like this:

\| \| \| m11 m12 m13 \|

.. raw:: html

   <table rows="6" cols="1">

cos( phi ) -sin( phi ) tx

m21 m22 m23

sin( phi ) cos( phi ) ty

m31 m32 m33

0 0 1

.. raw:: html

   </table>

The size of the matrix is implicitely 3x3. Only the values of the first two rows are stored in the matrix.

The inverse is still an rigid matrix.

The transpose no longer would be an rigid matrix and is therefore not implemented. If you need to calculate the transpose, you need to convert this matrix to a matrix and calculate the transpose there.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *RigidMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^

``RigidMatrix()``

Default constructor.

By default this is the identity matrix.

Constructors
~~~~~~~~~~~~

Constructor *RigidMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^

``RigidMatrix(System.Double phi, VectorDouble translation)``

Constructor.

The constructor has the following parameters:

+-------------------+---------------------+---------------------------+
| Parameter         | Type                | Description               |
+===================+=====================+===========================+
| ``phi``           | ``System.Double``   | The rotation\_angle.      |
+-------------------+---------------------+---------------------------+
| ``translation``   | ``VectorDouble``    | The translation vector.   |
+-------------------+---------------------+---------------------------+

Constructor *RigidMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^

``RigidMatrix(System.Double phi, System.Double dx, System.Double dy)``

Constructor.

The constructor has the following parameters:

+-------------+---------------------+-------------------------------------------------------+
| Parameter   | Type                | Description                                           |
+=============+=====================+=======================================================+
| ``phi``     | ``System.Double``   | The rotation\_angle.                                  |
+-------------+---------------------+-------------------------------------------------------+
| ``dx``      | ``System.Double``   | The horizontal component of the translation vector.   |
+-------------+---------------------+-------------------------------------------------------+
| ``dy``      | ``System.Double``   | The vertical component of the translation vector.     |
+-------------+---------------------+-------------------------------------------------------+

Constructor *RigidMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^

``RigidMatrix(RotationMatrix rhs)``

Copy constructor.

The constructor has the following parameters:

+-------------+----------------------+------------------------+
| Parameter   | Type                 | Description            |
+=============+======================+========================+
| ``rhs``     | ``RotationMatrix``   | The right hand side.   |
+-------------+----------------------+------------------------+

Constructor *RigidMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^

``RigidMatrix(TranslationMatrix rhs)``

Conversion constructor.

The constructor has the following parameters:

+-------------+-------------------------+------------------------+
| Parameter   | Type                    | Description            |
+=============+=========================+========================+
| ``rhs``     | ``TranslationMatrix``   | The right hand side.   |
+-------------+-------------------------+------------------------+

Converts a rotation matrix to an rigid matrix.

Constructor *RigidMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^

``RigidMatrix(Pose rhs)``

Conversion constructor.

The constructor has the following parameters:

+-------------+------------+------------------------+
| Parameter   | Type       | Description            |
+=============+============+========================+
| ``rhs``     | ``Pose``   | The right hand side.   |
+-------------+------------+------------------------+

Converts a translation matrix to an rigid matrix.

Properties
~~~~~~~~~~

Property *Phi*
^^^^^^^^^^^^^^

``System.Double Phi``

The rotation angle.

Property *Translation*
^^^^^^^^^^^^^^^^^^^^^^

``VectorDouble Translation``

The translation vector of the matrix.

Property *M11*
^^^^^^^^^^^^^^

``System.Double M11``

The element m11 (row 1, column 1) of the matrix.

Property *M12*
^^^^^^^^^^^^^^

``System.Double M12``

The element m12 (row 1, column 2) of the matrix.

Property *M13*
^^^^^^^^^^^^^^

``System.Double M13``

The element m13 (row 1, column 3) of the matrix.

Property *M21*
^^^^^^^^^^^^^^

``System.Double M21``

The element m21 (row 2, column 1) of the matrix.

Property *M22*
^^^^^^^^^^^^^^

``System.Double M22``

The element m22 (row 2, column 2) of the matrix.

Property *M23*
^^^^^^^^^^^^^^

``System.Double M23``

The element m23 (row 2, column 3) of the matrix.

Property *M31*
^^^^^^^^^^^^^^

``System.Double M31``

The element m31 (row 3, column 1) of the matrix.

Property *M32*
^^^^^^^^^^^^^^

``System.Double M32``

The element m32 (row 3, column 2) of the matrix.

Property *M33*
^^^^^^^^^^^^^^

``System.Double M33``

The element m33 (row 3, column 3) of the matrix.

Property *[index]*
^^^^^^^^^^^^^^^^^^

``System.Double [index]``

The matrix values as a parameterized property.

This property allows you to retrieve the implicit values also.

Property *Determinant*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double Determinant``

The determinant of the matrix.

Property *Trace*
^^^^^^^^^^^^^^^^

``System.Double Trace``

The trace of the matrix.

Property *Inverse*
^^^^^^^^^^^^^^^^^^

``RigidMatrix Inverse``

The inverse of the matrix.

Static Methods
~~~~~~~~~~~~~~

Method *Multiply*
^^^^^^^^^^^^^^^^^

``RigidMatrix Multiply(RigidMatrix a, RigidMatrix b)``

Multiply two matrices.

The method **Multiply** has the following parameters:

+-------------+-------------------+---------------+
| Parameter   | Type              | Description   |
+=============+===================+===============+
| ``a``       | ``RigidMatrix``   |               |
+-------------+-------------------+---------------+
| ``b``       | ``RigidMatrix``   |               |
+-------------+-------------------+---------------+

The product.

Methods
~~~~~~~

Method *ResetToIdentity*
^^^^^^^^^^^^^^^^^^^^^^^^

``void ResetToIdentity()``

Reset the matrix to the identity matrix.

A reference to this object.

Method *Rotate*
^^^^^^^^^^^^^^^

``void Rotate(System.Double phi)``

Rotate the matrix in place.

The method **Rotate** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``phi``     | ``System.Double``   |               |
+-------------+---------------------+---------------+

Return a reference to the matrix.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``void Translate(VectorDouble translation)``

Translate the matrix in place.

The method **Translate** has the following parameters:

+-------------------+--------------------+---------------------------+
| Parameter         | Type               | Description               |
+===================+====================+===========================+
| ``translation``   | ``VectorDouble``   | The translation vector.   |
+-------------------+--------------------+---------------------------+

Return a reference to the matrix.
