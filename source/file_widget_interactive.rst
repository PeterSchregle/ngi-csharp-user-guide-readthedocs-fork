Class *WidgetInteractive*
-------------------------

The abstract widget\_interactive type.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetInteractive** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetInteractive** contains the following properties:

+----------------------+-------+-------+---------------------------------------------------------------------+
| Property             | Get   | Set   | Description                                                         |
+======================+=======+=======+=====================================================================+
| ``Active``           | \*    | \*    | Specifies whether the widget is active (i.e.                        |
+----------------------+-------+-------+---------------------------------------------------------------------+
| ``Selected``         | \*    | \*    | Specifies whether the widget is selected.                           |
+----------------------+-------+-------+---------------------------------------------------------------------+
| ``Hot``              | \*    | \*    | Specifies whether the widget is hot.                                |
+----------------------+-------+-------+---------------------------------------------------------------------+
| ``Proximity``        | \*    | \*    | The proximity in pixels.                                            |
+----------------------+-------+-------+---------------------------------------------------------------------+
| ``TrackingHandle``   | \*    | \*    | Remembers the handle that is tracked during a tracking operation.   |
+----------------------+-------+-------+---------------------------------------------------------------------+
| ``TrackingPoint``    | \*    | \*    | Remembers the tracking point during a tracking operation.           |
+----------------------+-------+-------+---------------------------------------------------------------------+

Description
~~~~~~~~~~~

This abstract base class provides a few basic facilities that many widgets share by inheriting from it. A widget derived from this class provides interaction with the mouse and the keyboard. An interactive widget has all the base features.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetInteractive*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetInteractive()``

Default constructor.

The default constructor creates a widget with default settings.

Properties
~~~~~~~~~~

Property *Active*
^^^^^^^^^^^^^^^^^

``System.Boolean Active``

Specifies whether the widget is active (i.e.

reacts to user interaction).

If this property is true, the user can interact with the widget, in order to move, resize or zoom it. Interactive zooming is also affected by the interactive\_zoom, interactive\_zoom\_factor, zoom\_reset\_character, zoom\_in\_character and zoom\_out\_character properties.

Property *Selected*
^^^^^^^^^^^^^^^^^^^

``System.Boolean Selected``

Specifies whether the widget is selected.

A widget can be selected and deselected by clicking it with the left mouse button, but only if the widget is active.

The selection status affects behavior.

Property *Hot*
^^^^^^^^^^^^^^

``System.Boolean Hot``

Specifies whether the widget is hot.

A widget is hot when the mouse is positioned over it (hot-tracking), but only if the widget is active.

Often, the hot status is used to modify the widget look.

Property *Proximity*
^^^^^^^^^^^^^^^^^^^^

``System.Int32 Proximity``

The proximity in pixels.

When the mouse is closer than proximity to the widgets hit spots, the hit spot is hit. If this distance is small, hit spots must be pointed at more precisely, while if this distance is big, hit spots are more easily grabbed with the mouse.

Property *TrackingHandle*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Int32 TrackingHandle``

Remembers the handle that is tracked during a tracking operation.

The tracking handle is used internally during a tracking operation.

Property *TrackingPoint*
^^^^^^^^^^^^^^^^^^^^^^^^

``PointDouble TrackingPoint``

Remembers the tracking point during a tracking operation.

The tracking point is used internally during a tracking operation.

Events
~~~~~~

Event *HitTest*
^^^^^^^^^^^^^^^

``System.Boolean HitTest(WidgetInteractive widget, PointDouble position, ref System.Boolean hit)``

TODO no brief description for variant

The event **HitTest** has the following parameters:

+----------------+--------------------------+---------------+
| Parameter      | Type                     | Description   |
+================+==========================+===============+
| ``widget``     | ``WidgetInteractive``    |               |
+----------------+--------------------------+---------------+
| ``position``   | ``PointDouble``          |               |
+----------------+--------------------------+---------------+
| ``hit``        | ``ref System.Boolean``   |               |
+----------------+--------------------------+---------------+

TODO no description for variant

Event *HitSpotHitTest*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean HitSpotHitTest(WidgetInteractive widget, System.Int32 hitSpot)``

TODO no brief description for variant

The event **HitSpotHitTest** has the following parameters:

+---------------+-------------------------+---------------+
| Parameter     | Type                    | Description   |
+===============+=========================+===============+
| ``widget``    | ``WidgetInteractive``   |               |
+---------------+-------------------------+---------------+
| ``hitSpot``   | ``System.Int32``        |               |
+---------------+-------------------------+---------------+

TODO no description for variant

Event *HitSpotHandleChanged*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void HitSpotHandleChanged(WidgetInteractive widget, System.Int32 hitSpot)``

TODO no brief description for variant

The event **HitSpotHandleChanged** has the following parameters:

+---------------+-------------------------+---------------+
| Parameter     | Type                    | Description   |
+===============+=========================+===============+
| ``widget``    | ``WidgetInteractive``   |               |
+---------------+-------------------------+---------------+
| ``hitSpot``   | ``System.Int32``        |               |
+---------------+-------------------------+---------------+

TODO no description for variant

Event *Select*
^^^^^^^^^^^^^^

``System.Boolean Select(WidgetInteractive widget, PointDouble position)``

TODO no brief description for variant

The event **Select** has the following parameters:

+----------------+-------------------------+---------------+
| Parameter      | Type                    | Description   |
+================+=========================+===============+
| ``widget``     | ``WidgetInteractive``   |               |
+----------------+-------------------------+---------------+
| ``position``   | ``PointDouble``         |               |
+----------------+-------------------------+---------------+

TODO no description for variant

Event *StartTrack*
^^^^^^^^^^^^^^^^^^

``System.Boolean StartTrack(WidgetInteractive widget, PointDouble position)``

TODO no brief description for variant

The event **StartTrack** has the following parameters:

+----------------+-------------------------+---------------+
| Parameter      | Type                    | Description   |
+================+=========================+===============+
| ``widget``     | ``WidgetInteractive``   |               |
+----------------+-------------------------+---------------+
| ``position``   | ``PointDouble``         |               |
+----------------+-------------------------+---------------+

TODO no description for variant

Event *Track*
^^^^^^^^^^^^^

``void Track(WidgetInteractive widget, PointDouble position)``

TODO no brief description for variant

The event **Track** has the following parameters:

+----------------+-------------------------+---------------+
| Parameter      | Type                    | Description   |
+================+=========================+===============+
| ``widget``     | ``WidgetInteractive``   |               |
+----------------+-------------------------+---------------+
| ``position``   | ``PointDouble``         |               |
+----------------+-------------------------+---------------+

TODO no description for variant

Event *StopTrack*
^^^^^^^^^^^^^^^^^

``void StopTrack(WidgetInteractive widget, PointDouble position)``

TODO no brief description for variant

The event **StopTrack** has the following parameters:

+----------------+-------------------------+---------------+
| Parameter      | Type                    | Description   |
+================+=========================+===============+
| ``widget``     | ``WidgetInteractive``   |               |
+----------------+-------------------------+---------------+
| ``position``   | ``PointDouble``         |               |
+----------------+-------------------------+---------------+

TODO no description for variant

Event *MouseMove*
^^^^^^^^^^^^^^^^^

``System.Boolean MouseMove(WidgetInteractive widget, PointDouble position, System.UInt32 flags)``

TODO no brief description for variant

The event **MouseMove** has the following parameters:

+----------------+-------------------------+---------------+
| Parameter      | Type                    | Description   |
+================+=========================+===============+
| ``widget``     | ``WidgetInteractive``   |               |
+----------------+-------------------------+---------------+
| ``position``   | ``PointDouble``         |               |
+----------------+-------------------------+---------------+
| ``flags``      | ``System.UInt32``       |               |
+----------------+-------------------------+---------------+

TODO no description for variant

Event *MouseWheel*
^^^^^^^^^^^^^^^^^^

``System.Boolean MouseWheel(WidgetInteractive widget, System.Double distance, PointDouble position, System.UInt32 flags)``

TODO no brief description for variant

The event **MouseWheel** has the following parameters:

+----------------+-------------------------+---------------+
| Parameter      | Type                    | Description   |
+================+=========================+===============+
| ``widget``     | ``WidgetInteractive``   |               |
+----------------+-------------------------+---------------+
| ``distance``   | ``System.Double``       |               |
+----------------+-------------------------+---------------+
| ``position``   | ``PointDouble``         |               |
+----------------+-------------------------+---------------+
| ``flags``      | ``System.UInt32``       |               |
+----------------+-------------------------+---------------+

TODO no description for variant

Event *MouseButtonDown*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean MouseButtonDown(WidgetInteractive widget, System.UInt32 button, PointDouble position, System.UInt32 flags)``

TODO no brief description for variant

The event **MouseButtonDown** has the following parameters:

+----------------+-------------------------+---------------+
| Parameter      | Type                    | Description   |
+================+=========================+===============+
| ``widget``     | ``WidgetInteractive``   |               |
+----------------+-------------------------+---------------+
| ``button``     | ``System.UInt32``       |               |
+----------------+-------------------------+---------------+
| ``position``   | ``PointDouble``         |               |
+----------------+-------------------------+---------------+
| ``flags``      | ``System.UInt32``       |               |
+----------------+-------------------------+---------------+

TODO no description for variant

Event *MouseButtonUp*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean MouseButtonUp(WidgetInteractive widget, System.UInt32 button, PointDouble position, System.UInt32 flags)``

TODO no brief description for variant

The event **MouseButtonUp** has the following parameters:

+----------------+-------------------------+---------------+
| Parameter      | Type                    | Description   |
+================+=========================+===============+
| ``widget``     | ``WidgetInteractive``   |               |
+----------------+-------------------------+---------------+
| ``button``     | ``System.UInt32``       |               |
+----------------+-------------------------+---------------+
| ``position``   | ``PointDouble``         |               |
+----------------+-------------------------+---------------+
| ``flags``      | ``System.UInt32``       |               |
+----------------+-------------------------+---------------+

TODO no description for variant

Event *MouseButtonDoubleClick*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean MouseButtonDoubleClick(WidgetInteractive widget, System.UInt32 button, PointDouble position, System.UInt32 flags)``

TODO no brief description for variant

The event **MouseButtonDoubleClick** has the following parameters:

+----------------+-------------------------+---------------+
| Parameter      | Type                    | Description   |
+================+=========================+===============+
| ``widget``     | ``WidgetInteractive``   |               |
+----------------+-------------------------+---------------+
| ``button``     | ``System.UInt32``       |               |
+----------------+-------------------------+---------------+
| ``position``   | ``PointDouble``         |               |
+----------------+-------------------------+---------------+
| ``flags``      | ``System.UInt32``       |               |
+----------------+-------------------------+---------------+

TODO no description for variant

Event *KeyDown*
^^^^^^^^^^^^^^^

``System.Boolean KeyDown(WidgetInteractive widget, System.UInt32 keyCode)``

TODO no brief description for variant

The event **KeyDown** has the following parameters:

+---------------+-------------------------+---------------+
| Parameter     | Type                    | Description   |
+===============+=========================+===============+
| ``widget``    | ``WidgetInteractive``   |               |
+---------------+-------------------------+---------------+
| ``keyCode``   | ``System.UInt32``       |               |
+---------------+-------------------------+---------------+

TODO no description for variant

Event *KeyUp*
^^^^^^^^^^^^^

``System.Boolean KeyUp(WidgetInteractive widget, System.UInt32 keyCode)``

TODO no brief description for variant

The event **KeyUp** has the following parameters:

+---------------+-------------------------+---------------+
| Parameter     | Type                    | Description   |
+===============+=========================+===============+
| ``widget``    | ``WidgetInteractive``   |               |
+---------------+-------------------------+---------------+
| ``keyCode``   | ``System.UInt32``       |               |
+---------------+-------------------------+---------------+

TODO no description for variant
