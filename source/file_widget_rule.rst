Class *WidgetRule*
------------------

A widget\_rule displays a rule for length measurement on a surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetRule** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetRule** contains the following properties:

+--------------------------+-------+-------+-----------------------------------------------------------------------------------+
| Property                 | Get   | Set   | Description                                                                       |
+==========================+=======+=======+===================================================================================+
| ``Position``             | \*    | \*    | The position of the ruler.                                                        |
+--------------------------+-------+-------+-----------------------------------------------------------------------------------+
| ``Width``                | \*    | \*    | The width of the ruler.                                                           |
+--------------------------+-------+-------+-----------------------------------------------------------------------------------+
| ``Scale``                | \*    | \*    | The scaling of the ruler (in pixel per units).                                    |
+--------------------------+-------+-------+-----------------------------------------------------------------------------------+
| ``TickSpacing``          | \*    | \*    | The tick spacing of the ruler. Ticks that are narrower than this are not drawn.   |
+--------------------------+-------+-------+-----------------------------------------------------------------------------------+
| ``MajorTick``            | \*    | \*    | Major ticks are drawn every major\_tick ticks.                                    |
+--------------------------+-------+-------+-----------------------------------------------------------------------------------+
| ``MinorTick``            | \*    | \*    | Minor ticks are drawn every minor\_tick ticks.                                    |
+--------------------------+-------+-------+-----------------------------------------------------------------------------------+
| ``PointTick``            | \*    | \*    | Point ticks are drawn every point\_tick ticks.                                    |
+--------------------------+-------+-------+-----------------------------------------------------------------------------------+
| ``MajorTickExtent``      | \*    | \*    | Major ticks length.                                                               |
+--------------------------+-------+-------+-----------------------------------------------------------------------------------+
| ``MinorTickExtent``      | \*    | \*    | Minor ticks length.                                                               |
+--------------------------+-------+-------+-----------------------------------------------------------------------------------+
| ``PointTickExtent``      | \*    | \*    | Point ticks length.                                                               |
+--------------------------+-------+-------+-----------------------------------------------------------------------------------+
| ``MajorTickLinePen``     | \*    | \*    | The pen of the major tick lines.                                                  |
+--------------------------+-------+-------+-----------------------------------------------------------------------------------+
| ``MinorTickLinePen``     | \*    | \*    | The pen of the minor tick lines.                                                  |
+--------------------------+-------+-------+-----------------------------------------------------------------------------------+
| ``PointTickLinePen``     | \*    | \*    | The pen of the point tick lines.                                                  |
+--------------------------+-------+-------+-----------------------------------------------------------------------------------+
| ``TickNumberDistance``   | \*    | \*    | Distance of the numbers on the ruler.                                             |
+--------------------------+-------+-------+-----------------------------------------------------------------------------------+
| ``TickNumberFont``       | \*    | \*    | The number text font of the numbers on the ruler.                                 |
+--------------------------+-------+-------+-----------------------------------------------------------------------------------+
| ``TickNumberBrush``      | \*    | \*    | The number text brush of the numbers on the ruler.                                |
+--------------------------+-------+-------+-----------------------------------------------------------------------------------+
| ``Message``              | \*    | \*    | The message.                                                                      |
+--------------------------+-------+-------+-----------------------------------------------------------------------------------+
| ``MessageOrigin``        | \*    | \*    | The message origin.                                                               |
+--------------------------+-------+-------+-----------------------------------------------------------------------------------+
| ``MessageFont``          | \*    | \*    | The message text font.                                                            |
+--------------------------+-------+-------+-----------------------------------------------------------------------------------+
| ``MessageBrush``         | \*    | \*    | The message text brush.                                                           |
+--------------------------+-------+-------+-----------------------------------------------------------------------------------+
| ``BaseLinePen``          | \*    | \*    | The pen of the ruler base line.                                                   |
+--------------------------+-------+-------+-----------------------------------------------------------------------------------+
| ``ReferenceLinePen``     | \*    | \*    | The pen of the ruler reference line.                                              |
+--------------------------+-------+-------+-----------------------------------------------------------------------------------+
| ``ActiveAreaBrush``      | \*    | \*    | The brush of the active area of the ruler.                                        |
+--------------------------+-------+-------+-----------------------------------------------------------------------------------+
| ``Length``               | \*    | \*    | Get the length of the rule in scaled units.                                       |
+--------------------------+-------+-------+-----------------------------------------------------------------------------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_rule. These settings affect the look and the behavior of the widget\_rule.

The widget\_rule supports interactivity via the widget\_interactive base class, i.e. it can be selected, move around. In general it can react on mouse and keyboard interactions. A widget\_rule has interactive widget capabilities.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetRule*
^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetRule()``

Default constructor.

The default constructor creates a widget with default settings.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``LineSegmentDouble Position``

The position of the ruler.

The length in scaled units is calculated as length = position.extent / scale. If the position.extent of the rule is changed - as happens when the rule is interactively moved, the length is calculated appropriately.

See also: scale, length.

Property *Width*
^^^^^^^^^^^^^^^^

``System.Double Width``

The width of the ruler.

Property *Scale*
^^^^^^^^^^^^^^^^

``System.Double Scale``

The scaling of the ruler (in pixel per units).

The length in scaled units is calculated as length = position.extent / scale. If the scale of the ruler is changed, the length is calculated appropriately.

See also: position, length.

Property *TickSpacing*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double TickSpacing``

The tick spacing of the ruler. Ticks that are narrower than this are not drawn.

Property *MajorTick*
^^^^^^^^^^^^^^^^^^^^

``System.Int32 MajorTick``

Major ticks are drawn every major\_tick ticks.

Property *MinorTick*
^^^^^^^^^^^^^^^^^^^^

``System.Int32 MinorTick``

Minor ticks are drawn every minor\_tick ticks.

Property *PointTick*
^^^^^^^^^^^^^^^^^^^^

``System.Int32 PointTick``

Point ticks are drawn every point\_tick ticks.

Property *MajorTickExtent*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double MajorTickExtent``

Major ticks length.

Property *MinorTickExtent*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double MinorTickExtent``

Minor ticks length.

Property *PointTickExtent*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double PointTickExtent``

Point ticks length.

Property *MajorTickLinePen*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``PenByte MajorTickLinePen``

The pen of the major tick lines.

Property *MinorTickLinePen*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``PenByte MinorTickLinePen``

The pen of the minor tick lines.

Property *PointTickLinePen*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``PenByte PointTickLinePen``

The pen of the point tick lines.

Property *TickNumberDistance*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double TickNumberDistance``

Distance of the numbers on the ruler.

Property *TickNumberFont*
^^^^^^^^^^^^^^^^^^^^^^^^^

``Font TickNumberFont``

The number text font of the numbers on the ruler.

Property *TickNumberBrush*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``SolidColorBrushByte TickNumberBrush``

The number text brush of the numbers on the ruler.

Property *Message*
^^^^^^^^^^^^^^^^^^

``System.String Message``

The message.

Property *MessageOrigin*
^^^^^^^^^^^^^^^^^^^^^^^^

``PointDouble MessageOrigin``

The message origin.

Property *MessageFont*
^^^^^^^^^^^^^^^^^^^^^^

``Font MessageFont``

The message text font.

Property *MessageBrush*
^^^^^^^^^^^^^^^^^^^^^^^

``SolidColorBrushByte MessageBrush``

The message text brush.

Property *BaseLinePen*
^^^^^^^^^^^^^^^^^^^^^^

``PenByte BaseLinePen``

The pen of the ruler base line.

Property *ReferenceLinePen*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``PenByte ReferenceLinePen``

The pen of the ruler reference line.

Property *ActiveAreaBrush*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``SolidColorBrushByte ActiveAreaBrush``

The brush of the active area of the ruler.

Property *Length*
^^^^^^^^^^^^^^^^^

``System.Double Length``

Get the length of the rule in scaled units.

The length in scaled units is calculated as length = position.extent / scale. If the length of the ruler is changed - this is the real calibration step, the scale is calculated appropriately (scale = position.extent / length).

See also: scale, position.
