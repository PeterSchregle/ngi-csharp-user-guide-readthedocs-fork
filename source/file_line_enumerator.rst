Class *LineEnumerator*
----------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **LineEnumerator** implements the following interfaces:

+--------------------------------------------+
| Interface                                  |
+============================================+
| ``IEnumerator``                            |
+--------------------------------------------+
| ``IEnumeratorLineEnumeratorLineVariant``   |
+--------------------------------------------+

The class **LineEnumerator** contains the following variant parameters:

+------------+-----------------------------------------+
| Variant    | Description                             |
+============+=========================================+
| ``Line``   | TODO no brief description for variant   |
+------------+-----------------------------------------+

The class **LineEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **LineEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Line*
^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Line** has the following types:

+------------------+
| Type             |
+==================+
| ``LineInt32``    |
+------------------+
| ``LineDouble``   |
+------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Line Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
