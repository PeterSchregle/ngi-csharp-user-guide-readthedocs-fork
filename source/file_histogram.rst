Class *Histogram*
-----------------

...

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Histogram** contains the following variant parameters:

+-------------+-----------------------------------------+
| Variant     | Description                             |
+=============+=========================================+
| ``Pixel``   | TODO no brief description for variant   |
+-------------+-----------------------------------------+

Description
~~~~~~~~~~~

The histogram base class.

Variants
~~~~~~~~

Variant *Pixel*
^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Pixel** has the following types:

+------------------+
| Type             |
+==================+
| ``UInt32``       |
+------------------+
| ``Double``       |
+------------------+
| ``RgbUInt32``    |
+------------------+
| ``RgbDouble``    |
+------------------+
| ``RgbaUInt32``   |
+------------------+
| ``RgbaDouble``   |
+------------------+
| ``HlsDouble``    |
+------------------+
| ``HsiDouble``    |
+------------------+
| ``LabDouble``    |
+------------------+
| ``XyzDouble``    |
+------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Histogram*
^^^^^^^^^^^^^^^^^^^^^^^

``Histogram()``

Constructs an empty histogram.

Constructors
~~~~~~~~~~~~

Constructor *Histogram*
^^^^^^^^^^^^^^^^^^^^^^^

``Histogram(System.Int32 width, System.Int32 height, System.Int32 depth, System.Object value)``

Constructs a histogram of a specific size with all elements not initialized.

The constructor has the following parameters:

+--------------+---------------------+--------------------------------------------+
| Parameter    | Type                | Description                                |
+==============+=====================+============================================+
| ``width``    | ``System.Int32``    | The width of the constructed histogram.    |
+--------------+---------------------+--------------------------------------------+
| ``height``   | ``System.Int32``    | The height of the constructed histogram.   |
+--------------+---------------------+--------------------------------------------+
| ``depth``    | ``System.Int32``    | The depth of the constructed histogram.    |
+--------------+---------------------+--------------------------------------------+
| ``value``    | ``System.Object``   |                                            |
+--------------+---------------------+--------------------------------------------+
