Class *Triangle*
----------------

A triangle in two-dimensional euclidean space.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Triangle** implements the following interfaces:

+---------------------------------------------------+
| Interface                                         |
+===================================================+
| ``IEquatableTriangleTriangleCoordinateVariant``   |
+---------------------------------------------------+
| ``ISerializable``                                 |
+---------------------------------------------------+
| ``INotifyPropertyChanged``                        |
+---------------------------------------------------+

The class **Triangle** contains the following variant parameters:

+------------------+-----------------------------------------+
| Variant          | Description                             |
+==================+=========================================+
| ``Coordinate``   | TODO no brief description for variant   |
+------------------+-----------------------------------------+

The class **Triangle** contains the following properties:

+--------------------+-------+-------+-----------------------------------------+
| Property           | Get   | Set   | Description                             |
+====================+=======+=======+=========================================+
| ``P1``             | \*    | \*    | The first point of the triangle.        |
+--------------------+-------+-------+-----------------------------------------+
| ``P2``             | \*    | \*    | The second point of the triangle.       |
+--------------------+-------+-------+-----------------------------------------+
| ``P3``             | \*    | \*    | The third point of the triangle.        |
+--------------------+-------+-------+-----------------------------------------+
| ``BoundingBox``    | \*    |       | Get the bounding box of the triangle.   |
+--------------------+-------+-------+-----------------------------------------+
| ``Circumcenter``   | \*    |       | The circumcenter of the triangle.       |
+--------------------+-------+-------+-----------------------------------------+

The class **Triangle** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``Move``       | Move triangle.                                          |
+----------------+---------------------------------------------------------+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

The following operations are implemented: operator== : comparison for equality. operator != : comparison for inequality.

Variants
~~~~~~~~

Variant *Coordinate*
^^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Coordinate** has the following types:

+--------------+
| Type         |
+==============+
| ``Int32``    |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Triangle*
^^^^^^^^^^^^^^^^^^^^^^

``Triangle()``

Construct a triangle from three points.

Constructors
~~~~~~~~~~~~

Constructor *Triangle*
^^^^^^^^^^^^^^^^^^^^^^

``Triangle(Point p1, Point p2, Point p3)``

Construct a triangle from three points.

The constructor has the following parameters:

+-------------+-------------+---------------------+
| Parameter   | Type        | Description         |
+=============+=============+=====================+
| ``p1``      | ``Point``   | The first point.    |
+-------------+-------------+---------------------+
| ``p2``      | ``Point``   | The second point.   |
+-------------+-------------+---------------------+
| ``p3``      | ``Point``   | The third point.    |
+-------------+-------------+---------------------+

Properties
~~~~~~~~~~

Property *P1*
^^^^^^^^^^^^^

``Point P1``

The first point of the triangle.

Property *P2*
^^^^^^^^^^^^^

``Point P2``

The second point of the triangle.

Property *P3*
^^^^^^^^^^^^^

``Point P3``

The third point of the triangle.

Property *BoundingBox*
^^^^^^^^^^^^^^^^^^^^^^

``BoxDouble BoundingBox``

Get the bounding box of the triangle.

The bounding box is an axis aligned box that bounds the triangle. It can optionally take a line thickness into account.

The bounding box of the triangle.

Property *Circumcenter*
^^^^^^^^^^^^^^^^^^^^^^^

``PointDouble Circumcenter``

The circumcenter of the triangle.

Methods
~~~~~~~

Method *Move*
^^^^^^^^^^^^^

``Triangle Move(Vector movement)``

Move triangle.

The method **Move** has the following parameters:

+----------------+--------------+--------------------+
| Parameter      | Type         | Description        |
+================+==============+====================+
| ``movement``   | ``Vector``   | Movement vector.   |
+----------------+--------------+--------------------+

Moves a triangle by a vector.

The moved triangle.

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO documentation missing

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO documentation missing
