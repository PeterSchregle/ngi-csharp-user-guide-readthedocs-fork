Class *WrapTest1*
-----------------

**Namespace:** Ngi

**Module:**

The class **WrapTest1** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WrapTest1** contains the following properties:

+------------------+-------+-------+---------------+
| Property         | Get   | Set   | Description   |
+==================+=======+=======+===============+
| ``ObjectProp``   | \*    | \*    |               |
+------------------+-------+-------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WrapTest1*
^^^^^^^^^^^^^^^^^^^^^^^

``WrapTest1()``

Properties
~~~~~~~~~~

Property *ObjectProp*
^^^^^^^^^^^^^^^^^^^^^

``WrapTest2 ObjectProp``

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO no brief description for variant

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO no description for variant
