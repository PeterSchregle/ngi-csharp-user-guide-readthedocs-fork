Class *MvizMatrixcode*
----------------------

A matrixcode result.

**Namespace:** Ngi

**Module:** BarcodeMatrixcode

The class **MvizMatrixcode** implements the following interfaces:

+--------------------------------+
| Interface                      |
+================================+
| ``IEquatableMvizMatrixcode``   |
+--------------------------------+
| ``ISerializable``              |
+--------------------------------+

The class **MvizMatrixcode** contains the following properties:

+------------+--------+--------+----------------------------------------------------+
| Property   | Get    | Set    | Description                                        |
+============+========+========+====================================================+
| ``Text``   | \*     |        | The decoded text of the barcode.                   |
+------------+--------+--------+----------------------------------------------------+
| ``Position | \*     |        | The position of the barcode.                       |
| ``         |        |        |                                                    |
+------------+--------+--------+----------------------------------------------------+
| ``Corners` | \*     |        | The accurate corners of the barcode.               |
| `          |        |        |                                                    |
+------------+--------+--------+----------------------------------------------------+
| ``Symbolog | \*     |        | The symbology of the barcode.                      |
| yId``      |        |        |                                                    |
+------------+--------+--------+----------------------------------------------------+
| ``Symbolog | \*     |        | The symbology of the barcode.                      |
| yName``    |        |        |                                                    |
+------------+--------+--------+----------------------------------------------------+
| ``Polarity | \*     |        | The polarity of the barcode.                       |
| ``         |        |        |                                                    |
+------------+--------+--------+----------------------------------------------------+
| ``View``   | \*     |        | The view of the barcode.                           |
+------------+--------+--------+----------------------------------------------------+
| ``Size``   | \*     |        | The size of the barcode (the number of columns and |
|            |        |        | rows).                                             |
+------------+--------+--------+----------------------------------------------------+
| ``Fnc1``   | \*     |        | This is true, if a valid FNC1 character was found. |
+------------+--------+--------+----------------------------------------------------+
| ``ReaderPr | \*     |        | This is true, if the reader programming control    |
| ogramming` |        |        | character was found in the first position.         |
| `          |        |        |                                                    |
+------------+--------+--------+----------------------------------------------------+
| ``SymbolPo | \*     |        | If Structured Append is in use, gives the position |
| sition``   |        |        | in the sequence.                                   |
+------------+--------+--------+----------------------------------------------------+
| ``SymbolTo | \*     |        | If Structured Append is in use, gives the total    |
| tal``      |        |        | number of symbols in the sequence.                 |
+------------+--------+--------+----------------------------------------------------+
| ``SymbolFi | \*     |        | If Structured Append is in use, first File         |
| leId1``    |        |        | Identification code, in range [1..254].            |
+------------+--------+--------+----------------------------------------------------+
| ``SymbolFi | \*     |        | If Structured Append is in use, second File        |
| leId2``    |        |        | Identification code, in range [1..254].            |
+------------+--------+--------+----------------------------------------------------+
| ``Grades`` | \*     |        | A character string with the symbol quality         |
|            |        |        | gradings in the order: Overall, Symbol Contrast,   |
|            |        |        | Print Growth Horizontal, Print Growth Vertical,    |
|            |        |        | Axial NonUniformity, Grid NonUniformity, Unused    |
|            |        |        | Error, Modulation, Reflectance Margin, Fixed       |
|            |        |        | Pattern Damage.                                    |
+------------+--------+--------+----------------------------------------------------+
| ``SymbolCo | \*     |        | Indicates how different the foreground and         |
| ntrast``   |        |        | background intensities are.                        |
+------------+--------+--------+----------------------------------------------------+
| ``Contrast | \*     |        | Numerical assessment of local losses of contrast.  |
| Uniformity |        |        |                                                    |
| ``         |        |        |                                                    |
+------------+--------+--------+----------------------------------------------------+
| ``PrintGro | \*     |        | A measure of the relative horizontal thickness of  |
| wthHorizon |        |        | the black/white cells, which can be adverse        |
| tal``      |        |        | because of orverinkg/underinking or similar        |
|            |        |        | effects due to the marking process.                |
+------------+--------+--------+----------------------------------------------------+
| ``PrintGro | \*     |        | A measure of the relative vertical thickness of    |
| wthVertica |        |        | the black/white cells, which can be adverse        |
| l``        |        |        | because of orverinkg/underinking or similar        |
|            |        |        | effects due to the marking process.                |
+------------+--------+--------+----------------------------------------------------+
| ``AxialNon | \*     |        | Indicates if the size of the cells differs along   |
| Uniformity |        |        | one axis compared to the other.                    |
| ``         |        |        |                                                    |
+------------+--------+--------+----------------------------------------------------+
| ``GridNonU | \*     |        | Indicates if the cells are aligned on a regular    |
| niformity` |        |        | grid, with no local deformation.                   |
| `          |        |        |                                                    |
+------------+--------+--------+----------------------------------------------------+
| ``UnusedEr | \*     |        | 2D codes have a built-in error decoding            |
| ror``      |        |        | capability, enabled by redundancy in the content.  |
+------------+--------+--------+----------------------------------------------------+

The class **MvizMatrixcode** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

The class **MvizMatrixcode** contains the following enumerations:

+---------------------+------------------------------+
| Enumeration         | Description                  |
+=====================+==============================+
| ``SymbologyType``   | TODO documentation missing   |
+---------------------+------------------------------+
| ``PolarityType``    | TODO documentation missing   |
+---------------------+------------------------------+
| ``ViewType``        | TODO documentation missing   |
+---------------------+------------------------------+
| ``ShapeType``       | TODO documentation missing   |
+---------------------+------------------------------+

Description
~~~~~~~~~~~

A matrixcode result consists of the read matrixcode, the symbology and the location of the matrixcode.

The following operators are implemented for a matrixcode: operator == : comparison for equality. operator != : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *MvizMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MvizMatrixcode()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *MvizMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MvizMatrixcode(System.String text, Rectangle position, QuadrilateralDouble corners, MvizMatrixcode.SymbologyType symbologyId, MvizMatrixcode.PolarityType polarity, MvizMatrixcode.ViewType view, Extent size, System.Boolean fnc1, System.Boolean readerProgramming, System.Int32 symbolPosition, System.Int32 symbolTotal, System.Int32 symbolFileId1, System.Int32 symbolFileId2, System.String grades, System.Double symbolContrast, System.Double contrastUniformity, System.Double printGrowthHorizontal, System.Double printGrowthVertical, System.Double axialNonUniformity, System.Double gridNonUniformity, System.Double unusedError)``

Constructor.

The constructor has the following parameters:

+--------------+------------+----------------------------------------------------+
| Parameter    | Type       | Description                                        |
+==============+============+====================================================+
| ``text``     | ``System.S | The decoded text of the barcode.                   |
|              | tring``    |                                                    |
+--------------+------------+----------------------------------------------------+
| ``position`` | ``Rectangl | The position of the barcode.                       |
|              | e``        |                                                    |
+--------------+------------+----------------------------------------------------+
| ``corners``  | ``Quadrila | The accurate corners of the barcode.               |
|              | teralDoubl |                                                    |
|              | e``        |                                                    |
+--------------+------------+----------------------------------------------------+
| ``symbologyI | ``MvizMatr | The symbology of the barcode.                      |
| d``          | ixcode.Sym |                                                    |
|              | bologyType |                                                    |
|              | ``         |                                                    |
+--------------+------------+----------------------------------------------------+
| ``polarity`` | ``MvizMatr | The polarity of the barcode.                       |
|              | ixcode.Pol |                                                    |
|              | arityType` |                                                    |
|              | `          |                                                    |
+--------------+------------+----------------------------------------------------+
| ``view``     | ``MvizMatr | The view of the barcode.                           |
|              | ixcode.Vie |                                                    |
|              | wType``    |                                                    |
+--------------+------------+----------------------------------------------------+
| ``size``     | ``Extent`` | The size of the barcode (the number of columns and |
|              |            | rows).                                             |
+--------------+------------+----------------------------------------------------+
| ``fnc1``     | ``System.B | The FNC1 flag, true if a valid FNC1 character was  |
|              | oolean``   | found.                                             |
+--------------+------------+----------------------------------------------------+
| ``readerProg | ``System.B | The reader programming flag, true if the reader    |
| ramming``    | oolean``   | programming control character was found in the     |
|              |            | first position.                                    |
+--------------+------------+----------------------------------------------------+
| ``symbolPosi | ``System.I | If Structured Append is in use, gives the position |
| tion``       | nt32``     | in the sequence.                                   |
+--------------+------------+----------------------------------------------------+
| ``symbolTota | ``System.I | If Structured Append is in use, gives the total    |
| l``          | nt32``     | number of symbols in the sequence.                 |
+--------------+------------+----------------------------------------------------+
| ``symbolFile | ``System.I | If Structured Append is in use, first File         |
| Id1``        | nt32``     | Identification code, in range [1..254].            |
+--------------+------------+----------------------------------------------------+
| ``symbolFile | ``System.I | If Structured Append is in use, second File        |
| Id2``        | nt32``     | Identification code, in range [1..254].            |
+--------------+------------+----------------------------------------------------+
| ``grades``   | ``System.S | A character string with the symbol quality         |
|              | tring``    | gradings.                                          |
+--------------+------------+----------------------------------------------------+
| ``symbolCont | ``System.D | Numerical assessment of the contrast strength.     |
| rast``       | ouble``    |                                                    |
+--------------+------------+----------------------------------------------------+
| ``contrastUn | ``System.D | Numerical assessment of local losses of contrast.  |
| iformity``   | ouble``    |                                                    |
+--------------+------------+----------------------------------------------------+
| ``printGrowt | ``System.D | Numerical assessment of the horizontal cell        |
| hHorizontal` | ouble``    | filling.                                           |
| `            |            |                                                    |
+--------------+------------+----------------------------------------------------+
| ``printGrowt | ``System.D | Numerical assessment of the vertical cell filling. |
| hVertical``  | ouble``    |                                                    |
+--------------+------------+----------------------------------------------------+
| ``axialNonUn | ``System.D | Numerical assessment of geometric isotropy.        |
| iformity``   | ouble``    |                                                    |
+--------------+------------+----------------------------------------------------+
| ``gridNonUni | ``System.D | Numerical assessment of geometric distortion.      |
| formity``    | ouble``    |                                                    |
+--------------+------------+----------------------------------------------------+
| ``unusedErro | ``System.D | Numerical assessment of error-freeness.            |
| r``          | ouble``    |                                                    |
+--------------+------------+----------------------------------------------------+

Properties
~~~~~~~~~~

Property *Text*
^^^^^^^^^^^^^^^

``System.String Text``

The decoded text of the barcode.

Property *Position*
^^^^^^^^^^^^^^^^^^^

``Rectangle Position``

The position of the barcode.

Property *Corners*
^^^^^^^^^^^^^^^^^^

``QuadrilateralDouble Corners``

The accurate corners of the barcode.

Property *SymbologyId*
^^^^^^^^^^^^^^^^^^^^^^

``MvizMatrixcode.SymbologyType SymbologyId``

The symbology of the barcode.

Property *SymbologyName*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.String SymbologyName``

The symbology of the barcode.

Property *Polarity*
^^^^^^^^^^^^^^^^^^^

``MvizMatrixcode.PolarityType Polarity``

The polarity of the barcode.

Property *View*
^^^^^^^^^^^^^^^

``MvizMatrixcode.ViewType View``

The view of the barcode.

Property *Size*
^^^^^^^^^^^^^^^

``Extent Size``

The size of the barcode (the number of columns and rows).

Property *Fnc1*
^^^^^^^^^^^^^^^

``System.Boolean Fnc1``

This is true, if a valid FNC1 character was found.

Property *ReaderProgramming*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean ReaderProgramming``

This is true, if the reader programming control character was found in the first position.

Property *SymbolPosition*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Int32 SymbolPosition``

If Structured Append is in use, gives the position in the sequence.

Property *SymbolTotal*
^^^^^^^^^^^^^^^^^^^^^^

``System.Int32 SymbolTotal``

If Structured Append is in use, gives the total number of symbols in the sequence.

Property *SymbolFileId1*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.Int32 SymbolFileId1``

If Structured Append is in use, first File Identification code, in range [1..254].

Property *SymbolFileId2*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.Int32 SymbolFileId2``

If Structured Append is in use, second File Identification code, in range [1..254].

Property *Grades*
^^^^^^^^^^^^^^^^^

``System.String Grades``

A character string with the symbol quality gradings in the order: Overall, Symbol Contrast, Print Growth Horizontal, Print Growth Vertical, Axial NonUniformity, Grid NonUniformity, Unused Error, Modulation, Reflectance Margin, Fixed Pattern Damage.

Property *SymbolContrast*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double SymbolContrast``

Indicates how different the foreground and background intensities are.

The larger the better, as noise, dirt or other degradation can modify these intensities and complicate location/decoding.

Property *ContrastUniformity*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double ContrastUniformity``

Numerical assessment of local losses of contrast.

Property *PrintGrowthHorizontal*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double PrintGrowthHorizontal``

A measure of the relative horizontal thickness of the black/white cells, which can be adverse because of orverinkg/underinking or similar effects due to the marking process.

Property *PrintGrowthVertical*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double PrintGrowthVertical``

A measure of the relative vertical thickness of the black/white cells, which can be adverse because of orverinkg/underinking or similar effects due to the marking process.

Property *AxialNonUniformity*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double AxialNonUniformity``

Indicates if the size of the cells differs along one axis compared to the other.

This is often an indication of a mismatch in marking speeds across and along the substrate.

Property *GridNonUniformity*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double GridNonUniformity``

Indicates if the cells are aligned on a regular grid, with no local deformation.

This is often an indication of a mechanical disturbance in the marking motion.

Property *UnusedError*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double UnusedError``

2D codes have a built-in error decoding capability, enabled by redundancy in the content.

The more error correction capacity is consumed, the more likely a reading failure can arise.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Enumerations
~~~~~~~~~~~~

Enumeration *SymbologyType*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum SymbologyType``

TODO documentation missing

The enumeration **SymbologyType** has the following constants:

+------------------+---------+---------------+
| Name             | Value   | Description   |
+==================+=========+===============+
| ``unknown``      | ``0``   |               |
+------------------+---------+---------------+
| ``dataMatrix``   | ``1``   |               |
+------------------+---------+---------------+
| ``qrCode``       | ``2``   |               |
+------------------+---------+---------------+

::

    enum SymbologyType
    {
      unknown = 0,
      dataMatrix = 1,
      qrCode = 2,
    };

TODO documentation missing

Enumeration *PolarityType*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum PolarityType``

TODO documentation missing

The enumeration **PolarityType** has the following constants:

+--------------------+---------+---------------+
| Name               | Value   | Description   |
+====================+=========+===============+
| ``blackOnWhite``   | ``1``   |               |
+--------------------+---------+---------------+
| ``whiteOnBlack``   | ``2``   |               |
+--------------------+---------+---------------+
| ``autoPolarity``   | ``3``   |               |
+--------------------+---------+---------------+

::

    enum PolarityType
    {
      blackOnWhite = 1,
      whiteOnBlack = 2,
      autoPolarity = 3,
    };

TODO documentation missing

Enumeration *ViewType*
^^^^^^^^^^^^^^^^^^^^^^

``enum ViewType``

TODO documentation missing

The enumeration **ViewType** has the following constants:

+----------------+---------+---------------+
| Name           | Value   | Description   |
+================+=========+===============+
| ``straight``   | ``1``   |               |
+----------------+---------+---------------+
| ``mirrored``   | ``2``   |               |
+----------------+---------+---------------+
| ``autoView``   | ``3``   |               |
+----------------+---------+---------------+

::

    enum ViewType
    {
      straight = 1,
      mirrored = 2,
      autoView = 3,
    };

TODO documentation missing

Enumeration *ShapeType*
^^^^^^^^^^^^^^^^^^^^^^^

``enum ShapeType``

TODO documentation missing

The enumeration **ShapeType** has the following constants:

+-------------------+---------+---------------+
| Name              | Value   | Description   |
+===================+=========+===============+
| ``square``        | ``1``   |               |
+-------------------+---------+---------------+
| ``rectangular``   | ``2``   |               |
+-------------------+---------+---------------+
| ``all``           | ``3``   |               |
+-------------------+---------+---------------+

::

    enum ShapeType
    {
      square = 1,
      rectangular = 2,
      all = 3,
    };

TODO documentation missing
