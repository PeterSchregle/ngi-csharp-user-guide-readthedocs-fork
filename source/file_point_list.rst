Class *PointList*
-----------------

It is a list.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **PointList** implements the following interfaces:

+----------------------------------+
| Interface                        |
+==================================+
| ``IListPointListPointVariant``   |
+----------------------------------+

The class **PointList** contains the following variant parameters:

+-------------+-----------------------------------------+
| Variant     | Description                             |
+=============+=========================================+
| ``Point``   | TODO no brief description for variant   |
+-------------+-----------------------------------------+

The class **PointList** contains the following properties:

+-----------------------------------------+-------+-------+---------------------------------------------------------------------------+
| Property                                | Get   | Set   | Description                                                               |
+=========================================+=======+=======+===========================================================================+
| ``Centroid``                            | \*    |       | Calculate the centroid of the point list.                                 |
+-----------------------------------------+-------+-------+---------------------------------------------------------------------------+
| ``BoundingBox``                         | \*    |       | Calculate the bounding box of the point list.                             |
+-----------------------------------------+-------+-------+---------------------------------------------------------------------------+
| ``ConvexHull``                          | \*    |       | Calculate the convex hull.                                                |
+-----------------------------------------+-------+-------+---------------------------------------------------------------------------+
| ``MinimumWidth``                        | \*    |       | Calculate the minimum width of a convex polygon.                          |
+-----------------------------------------+-------+-------+---------------------------------------------------------------------------+
| ``MaximumWidth``                        | \*    |       | Calculate the maximum width of a convex polygon.                          |
+-----------------------------------------+-------+-------+---------------------------------------------------------------------------+
| ``MinimumAreaBoundingRectangle``        | \*    |       | Calculate the minimum area bounding rectangle of a convex polygon.        |
+-----------------------------------------+-------+-------+---------------------------------------------------------------------------+
| ``MinimumPerimeterBoundingRectangle``   | \*    |       | Calculate the minimum perimeter bounding rectangle of a convex polygon.   |
+-----------------------------------------+-------+-------+---------------------------------------------------------------------------+
| ``MinimumBoundingCircle``               | \*    |       | Calculate the minimum bounding circle of a polygon.                       |
+-----------------------------------------+-------+-------+---------------------------------------------------------------------------+
| ``Count``                               | \*    |       |                                                                           |
+-----------------------------------------+-------+-------+---------------------------------------------------------------------------+
| ``IsFixedSize``                         | \*    |       |                                                                           |
+-----------------------------------------+-------+-------+---------------------------------------------------------------------------+
| ``IsReadOnly``                          | \*    |       |                                                                           |
+-----------------------------------------+-------+-------+---------------------------------------------------------------------------+
| ``[index]``                             | \*    | \*    |                                                                           |
+-----------------------------------------+-------+-------+---------------------------------------------------------------------------+

The class **PointList** contains the following methods:

+---------------------+------------------------+
| Method              | Description            |
+=====================+========================+
| ``Move``            | Move the point list.   |
+---------------------+------------------------+
| ``GetEnumerator``   |                        |
+---------------------+------------------------+
| ``Add``             |                        |
+---------------------+------------------------+
| ``Clear``           |                        |
+---------------------+------------------------+
| ``Contains``        |                        |
+---------------------+------------------------+
| ``Remove``          |                        |
+---------------------+------------------------+
| ``IndexOf``         |                        |
+---------------------+------------------------+
| ``Insert``          |                        |
+---------------------+------------------------+
| ``RemoveAt``        |                        |
+---------------------+------------------------+
| ``ToString``        |                        |
+---------------------+------------------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Point*
^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Point** has the following types:

+-------------------+
| Type              |
+===================+
| ``PointInt32``    |
+-------------------+
| ``PointDouble``   |
+-------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *PointList*
^^^^^^^^^^^^^^^^^^^^^^^

``PointList()``

Properties
~~~~~~~~~~

Property *Centroid*
^^^^^^^^^^^^^^^^^^^

``Point Centroid``

Calculate the centroid of the point list.

The centroid is the arithmetic mean ("average") position of all the points in the list. An empty point list will generate an exception.

Property *BoundingBox*
^^^^^^^^^^^^^^^^^^^^^^

``Box BoundingBox``

Calculate the bounding box of the point list.

The bounding box is an axis aligned box that bounds the point list. An empty point list will generate an exception.

Property *ConvexHull*
^^^^^^^^^^^^^^^^^^^^^

``PointList ConvexHull``

Calculate the convex hull.

The convex hull is a pre-requisite for a number of other algorithms, such as the determination of feret diameters, etc.

The convex hull is returned in the form of a clockwise oriented point list starting with the leftmost point. The term clockwise is chosen with respect to a right-hand coordinate system. Since nGI uses a left hand coordinate system the returned point list is actually oriented counter-clockwise.

This function also handles corner cases. An empty point list will generate an exception. If a point list consisting of one point is passed in, a copy of this point list is returned. If a point list consisting of two or three points is passed in, the returned point list will contain these two or three points, potentially in different order.

Property *MinimumWidth*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Double MinimumWidth``

Calculate the minimum width of a convex polygon.

An empty point list will generate an exception.

Property *MaximumWidth*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Double MaximumWidth``

Calculate the maximum width of a convex polygon.

An empty point list will generate an exception.

Property *MinimumAreaBoundingRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Rectangle MinimumAreaBoundingRectangle``

Calculate the minimum area bounding rectangle of a convex polygon.

A point list with less than 2 points will generate an exception.

Property *MinimumPerimeterBoundingRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Rectangle MinimumPerimeterBoundingRectangle``

Calculate the minimum perimeter bounding rectangle of a convex polygon.

A point list with less than 2 points will generate an exception.

Property *MinimumBoundingCircle*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Circle MinimumBoundingCircle``

Calculate the minimum bounding circle of a polygon.

An empty point list will generate an exception.

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``Point [index]``

Static Methods
~~~~~~~~~~~~~~

Method *IntersectCircleAndLine*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``PointListPointDouble IntersectCircleAndLine(Circle circle, Line line)``

Calculate the intersection between a circle and a line.

The method **IntersectCircleAndLine** has the following parameters:

+--------------+--------------+---------------+
| Parameter    | Type         | Description   |
+==============+==============+===============+
| ``circle``   | ``Circle``   |               |
+--------------+--------------+---------------+
| ``line``     | ``Line``     |               |
+--------------+--------------+---------------+

The intersection may either result in a line\_segment if the line is a secant line, or a point, if the line is a tangent line or be empty if the line does not intersect the circle at all.

The result is returned in the form of list of points that a) contains two points if the line crosses the circle, a) contains one point if the line touches the circle, or c) is empty if the line and the circle do not intersect.

Methods
~~~~~~~

Method *Move*
^^^^^^^^^^^^^

``PointList Move(Vector movement)``

Move the point list.

The method **Move** has the following parameters:

+----------------+--------------+--------------------+
| Parameter      | Type         | Description        |
+================+==============+====================+
| ``movement``   | ``Vector``   | Movement vector.   |
+----------------+--------------+--------------------+

Moves a point\_list by a vector.

The moved point.

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``PointEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(Point item)``

The method **Add** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``item``    | ``Point``   |               |
+-------------+-------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(Point item)``

The method **Contains** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``item``    | ``Point``   |               |
+-------------+-------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(Point item)``

The method **Remove** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``item``    | ``Point``   |               |
+-------------+-------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(Point item)``

The method **IndexOf** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``item``    | ``Point``   |               |
+-------------+-------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, Point item)``

The method **Insert** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``item``    | ``Point``          |               |
+-------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
