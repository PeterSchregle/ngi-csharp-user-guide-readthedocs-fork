Class *Box3dEnumerator*
-----------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Box3dEnumerator** implements the following interfaces:

+----------------------------------------------+
| Interface                                    |
+==============================================+
| ``IEnumerator``                              |
+----------------------------------------------+
| ``IEnumeratorBox3dEnumeratorBox3dVariant``   |
+----------------------------------------------+

The class **Box3dEnumerator** contains the following variant parameters:

+-------------+-----------------------------------------+
| Variant     | Description                             |
+=============+=========================================+
| ``Box3d``   | TODO no brief description for variant   |
+-------------+-----------------------------------------+

The class **Box3dEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **Box3dEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Box3d*
^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Box3d** has the following types:

+-------------------+
| Type              |
+===================+
| ``Box3dInt32``    |
+-------------------+
| ``Box3dDouble``   |
+-------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Box3d Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
