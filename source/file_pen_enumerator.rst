Class *PenEnumerator*
---------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **PenEnumerator** implements the following interfaces:

+------------------------------------------+
| Interface                                |
+==========================================+
| ``IEnumerator``                          |
+------------------------------------------+
| ``IEnumeratorPenEnumeratorPenVariant``   |
+------------------------------------------+

The class **PenEnumerator** contains the following variant parameters:

+-----------+-----------------------------------------+
| Variant   | Description                             |
+===========+=========================================+
| ``Pen``   | TODO no brief description for variant   |
+-----------+-----------------------------------------+

The class **PenEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **PenEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Pen*
^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Pen** has the following types:

+-----------------+
| Type            |
+=================+
| ``PenByte``     |
+-----------------+
| ``PenUInt16``   |
+-----------------+
| ``PenUInt32``   |
+-----------------+
| ``PenDouble``   |
+-----------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Pen Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
