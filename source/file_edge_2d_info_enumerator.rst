Class *Edge2dInfoEnumerator*
----------------------------

**Namespace:** Ngi

**Module:** AnalysisMeasuring

The class **Edge2dInfoEnumerator** implements the following interfaces:

+-----------------------------+
| Interface                   |
+=============================+
| ``IEnumerator``             |
+-----------------------------+
| ``IEnumeratorEdge2dInfo``   |
+-----------------------------+

The class **Edge2dInfoEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **Edge2dInfoEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Edge2dInfo Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
