Class *GeometryElementList*
---------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **GeometryElementList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+

The class **GeometryElementList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *GeometryElementList*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``GeometryElementList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``GeometryElementEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(GeometryElement item)``

The method **Add** has the following parameters:

+-------------+-----------------------+---------------+
| Parameter   | Type                  | Description   |
+=============+=======================+===============+
| ``item``    | ``GeometryElement``   |               |
+-------------+-----------------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(GeometryElement item)``

The method **Contains** has the following parameters:

+-------------+-----------------------+---------------+
| Parameter   | Type                  | Description   |
+=============+=======================+===============+
| ``item``    | ``GeometryElement``   |               |
+-------------+-----------------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(GeometryElement item)``

The method **Remove** has the following parameters:

+-------------+-----------------------+---------------+
| Parameter   | Type                  | Description   |
+=============+=======================+===============+
| ``item``    | ``GeometryElement``   |               |
+-------------+-----------------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(GeometryElement item)``

The method **IndexOf** has the following parameters:

+-------------+-----------------------+---------------+
| Parameter   | Type                  | Description   |
+=============+=======================+===============+
| ``item``    | ``GeometryElement``   |               |
+-------------+-----------------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, GeometryElement item)``

The method **Insert** has the following parameters:

+-------------+-----------------------+---------------+
| Parameter   | Type                  | Description   |
+=============+=======================+===============+
| ``index``   | ``System.Int32``      |               |
+-------------+-----------------------+---------------+
| ``item``    | ``GeometryElement``   |               |
+-------------+-----------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
