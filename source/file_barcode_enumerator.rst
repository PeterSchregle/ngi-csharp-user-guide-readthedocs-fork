Class *BarcodeEnumerator*
-------------------------

**Namespace:** Ngi

**Module:** BarcodeMatrixcode

The class **BarcodeEnumerator** implements the following interfaces:

+--------------------------+
| Interface                |
+==========================+
| ``IEnumerator``          |
+--------------------------+
| ``IEnumeratorBarcode``   |
+--------------------------+

The class **BarcodeEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **BarcodeEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Barcode Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
