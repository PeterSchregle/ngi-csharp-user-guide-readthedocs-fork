Class *CircleEnumerator*
------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **CircleEnumerator** implements the following interfaces:

+-------------------------+
| Interface               |
+=========================+
| ``IEnumerator``         |
+-------------------------+
| ``IEnumeratorCircle``   |
+-------------------------+

The class **CircleEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **CircleEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Circle Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
