Class *View*
------------

A template that provides a random-access view as a basis for a mutable or a constant view.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **View** implements the following interfaces:

+----------------------------------------+
| Interface                              |
+========================================+
| ``IEquatableViewViewLocatorVariant``   |
+----------------------------------------+

The class **View** contains the following variant parameters:

+---------------+-----------------------------------------+
| Variant       | Description                             |
+===============+=========================================+
| ``Locator``   | TODO no brief description for variant   |
+---------------+-----------------------------------------+

The class **View** contains the following properties:

+---------------+-------+-------+-------------------------------------------------------------------+
| Property      | Get   | Set   | Description                                                       |
+===============+=======+=======+===================================================================+
| ``Width``     | \*    |       | The width of the view in pixels.                                  |
+---------------+-------+-------+-------------------------------------------------------------------+
| ``Height``    | \*    |       | The height of the view in pixels.                                 |
+---------------+-------+-------+-------------------------------------------------------------------+
| ``Depth``     | \*    |       | The depth of the view in pixels.                                  |
+---------------+-------+-------+-------------------------------------------------------------------+
| ``Volume``    | \*    |       | The volume of the view.                                           |
+---------------+-------+-------+-------------------------------------------------------------------+
| ``Size``      | \*    |       | The size of the view.                                             |
+---------------+-------+-------+-------------------------------------------------------------------+
| ``IsEmpty``   | \*    |       | Returns true if the view is empty, and false otherwise.           |
+---------------+-------+-------+-------------------------------------------------------------------+
| ``Begin``     | \*    |       | The adapted three-dimensional locator to the begin of the data.   |
+---------------+-------+-------+-------------------------------------------------------------------+

The class **View** contains the following methods:

+------------------------+----------------------------------------------------------------+
| Method                 | Description                                                    |
+========================+================================================================+
| ``Subsample``          | Returns a subsampling view.                                    |
+------------------------+----------------------------------------------------------------+
| ``Reverse``            | Returns a reverse view.                                        |
+------------------------+----------------------------------------------------------------+
| ``DimensionSwap``      | Returns a dimension swap view.                                 |
+------------------------+----------------------------------------------------------------+
| ``Subset``             | Returns a subset of this view.                                 |
+------------------------+----------------------------------------------------------------+
| ``IntersectWithBox``   | Intersects the view with a box in the same coordinate range.   |
+------------------------+----------------------------------------------------------------+
| ``IntersectWithBox``   | Intersects the view with a box in the same coordinate range.   |
+------------------------+----------------------------------------------------------------+
| ``ToString``           | Provide string representation for debugging purposes.          |
+------------------------+----------------------------------------------------------------+

The class **View** contains the following enumerations:

+-----------------------+------------------------------+
| Enumeration           | Description                  |
+=======================+==============================+
| ``DimensionSwapId``   | TODO documentation missing   |
+-----------------------+------------------------------+

Description
~~~~~~~~~~~

A view is a kind of a 'three-dimensional range' that is used within NGI to address buffers or parts of buffers.

.. figure:: images/view.png
   :alt: 

A view consists of a three-dimensional locator that points to the begin of the data, as well as of a width, height and depth to specify the three- dimensional size of the view.

Views are used by algorithms in NGI to access the data which is stored in buffers. This provides an indirection in data access which is one of the reasons of the flexibility of data access in NGI.

A view makes use of a locator, so it provides three-dimensional navigation on top of linear data.

A view has convenience methods that can turn itself into other, related views.

The subset method can cut out a subset of the current view by specifying a three-dimensional coordinate offset and a three-dimensional size.

The dimension\_swap method can swap view dimensions (i.e. swap the horizontal and the vertical dimensions) by adjusting the various pitches in the underlying locator.

The subsample method can subsample in one or more dimensions, and this is also done by adjusting the various pitches in the underlying locator.

The reverse method can reverse the iteration direction in one or more dimensions. Reversing is done by adjusting the position and negating the respective pitches of the underlying iterator.

The subset, dimension\_swap, subsample and reverse methods can be chained together. This means that you could create a view that is a subset, with a subsampling factor, swaps the horizontal and vertical axes and accesses data in a reverse way in certain dimensions.

Variants
~~~~~~~~

Variant *Locator*
^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Locator** has the following types:

+-------------------------+
| Type                    |
+=========================+
| ``LocatorByte``         |
+-------------------------+
| ``LocatorUInt16``       |
+-------------------------+
| ``LocatorUInt32``       |
+-------------------------+
| ``LocatorDouble``       |
+-------------------------+
| ``LocatorRgbByte``      |
+-------------------------+
| ``LocatorRgbUInt16``    |
+-------------------------+
| ``LocatorRgbUInt32``    |
+-------------------------+
| ``LocatorRgbDouble``    |
+-------------------------+
| ``LocatorRgbaByte``     |
+-------------------------+
| ``LocatorRgbaUInt16``   |
+-------------------------+
| ``LocatorRgbaUInt32``   |
+-------------------------+
| ``LocatorRgbaDouble``   |
+-------------------------+
| ``LocatorHlsByte``      |
+-------------------------+
| ``LocatorHlsUInt16``    |
+-------------------------+
| ``LocatorHlsDouble``    |
+-------------------------+
| ``LocatorHsiByte``      |
+-------------------------+
| ``LocatorHsiUInt16``    |
+-------------------------+
| ``LocatorHsiDouble``    |
+-------------------------+
| ``LocatorLabByte``      |
+-------------------------+
| ``LocatorLabUInt16``    |
+-------------------------+
| ``LocatorLabDouble``    |
+-------------------------+
| ``LocatorXyzByte``      |
+-------------------------+
| ``LocatorXyzUInt16``    |
+-------------------------+
| ``LocatorXyzDouble``    |
+-------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *View*
^^^^^^^^^^^^^^^^^^

``View()``

Default-construct a three-dimensional view.

A view constructed with this constructor can only serve to be instantiated. It cannot be used to iterate over some data.

Constructors
~~~~~~~~~~~~

Constructor *View*
^^^^^^^^^^^^^^^^^^

``View(Locator begin, System.Int32 width, System.Int32 height, System.Int32 depth)``

Construct a three-dimensional view from a three-dimensional locator.

The constructor has the following parameters:

+--------------+--------------------+----------------------------------------------------------------------------------------------------------------+
| Parameter    | Type               | Description                                                                                                    |
+==============+====================+================================================================================================================+
| ``begin``    | ``Locator``        | The locator.                                                                                                   |
+--------------+--------------------+----------------------------------------------------------------------------------------------------------------+
| ``width``    | ``System.Int32``   | The width in pixels (!= 0).                                                                                    |
+--------------+--------------------+----------------------------------------------------------------------------------------------------------------+
| ``height``   | ``System.Int32``   | The height in pixels (!= 0). This is 1 by default in order to ease creation of one-dimensional views.          |
+--------------+--------------------+----------------------------------------------------------------------------------------------------------------+
| ``depth``    | ``System.Int32``   | The depth in pixels (!= 0). This is 1 by default in order to ease creation of one- or two-dimensional views.   |
+--------------+--------------------+----------------------------------------------------------------------------------------------------------------+

In addition to the locator, you need to pass three size values.

Properties
~~~~~~~~~~

Property *Width*
^^^^^^^^^^^^^^^^

``System.Int32 Width``

The width of the view in pixels.

Property *Height*
^^^^^^^^^^^^^^^^^

``System.Int32 Height``

The height of the view in pixels.

Property *Depth*
^^^^^^^^^^^^^^^^

``System.Int32 Depth``

The depth of the view in pixels.

Property *Volume*
^^^^^^^^^^^^^^^^^

``System.Int32 Volume``

The volume of the view.

The volume of a view is the product of its width, height and depth. It is therefore the number of pixels in a view. The volume of a view is read only.

Property *Size*
^^^^^^^^^^^^^^^

``Extent3d Size``

The size of the view.

The size of a view is read only.

Property *IsEmpty*
^^^^^^^^^^^^^^^^^^

``System.Boolean IsEmpty``

Returns true if the view is empty, and false otherwise.

A view is empty, if either of its sizes in the horizontal, vertical or planar directions is equal zero.

True if the view is empty, false otherwise.

Property *Begin*
^^^^^^^^^^^^^^^^

``Locator Begin``

The adapted three-dimensional locator to the begin of the data.

Methods
~~~~~~~

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``View Subsample(System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Returns a subsampling view.

The method **Subsample** has the following parameters:

+---------------+--------------------+------------------------------------------------------------------------------------------------------------------+
| Parameter     | Type               | Description                                                                                                      |
+===============+====================+==================================================================================================================+
| ``xFactor``   | ``System.Int32``   | The horizontal subsampling factor (>= 1).                                                                        |
+---------------+--------------------+------------------------------------------------------------------------------------------------------------------+
| ``yFactor``   | ``System.Int32``   | The vertical subsampling factor (>= 1). This defaults to 1 in order to ease one-dimensional subsampling.         |
+---------------+--------------------+------------------------------------------------------------------------------------------------------------------+
| ``zFactor``   | ``System.Int32``   | The planar subsampling factor (>= 1). This defaults to 1 in order to ease one- or two-dimensional subsampling.   |
+---------------+--------------------+------------------------------------------------------------------------------------------------------------------+

This adjusts the sizes according to the subsampling factors, as well as the pitches of the locator, so that the view provides the means for subsampling.

Method *Reverse*
^^^^^^^^^^^^^^^^

``View Reverse(System.Boolean xReverse, System.Boolean yReverse, System.Boolean zReverse)``

Returns a reverse view.

The method **Reverse** has the following parameters:

+----------------+----------------------+-------------------------------------------------------------------------------------------------------+
| Parameter      | Type                 | Description                                                                                           |
+================+======================+=======================================================================================================+
| ``xReverse``   | ``System.Boolean``   | The horizontal reverse flag.                                                                          |
+----------------+----------------------+-------------------------------------------------------------------------------------------------------+
| ``yReverse``   | ``System.Boolean``   | The vertical reverse flag. This defaults to false in order to ease one-dimensional reversing.         |
+----------------+----------------------+-------------------------------------------------------------------------------------------------------+
| ``zReverse``   | ``System.Boolean``   | The planar reverse flag. This defaults to false in order to ease one- or two-dimensional reversing.   |
+----------------+----------------------+-------------------------------------------------------------------------------------------------------+

This adjusts the position of the begin locator according to the reverse flags, as well as the pitches, so that the view is suitable for reverse movement in the specified dimensions.

Method *DimensionSwap*
^^^^^^^^^^^^^^^^^^^^^^

``View DimensionSwap(View.DimensionSwapId swap)``

Returns a dimension swap view.

The method **DimensionSwap** has the following parameters:

+-------------+----------------------------+----------------------------+
| Parameter   | Type                       | Description                |
+=============+============================+============================+
| ``swap``    | ``View.DimensionSwapId``   | The dimension swap code.   |
+-------------+----------------------------+----------------------------+

This adjusts the pitches of the begin locator according to the dimension order, as well as the sizes, so that the view is suitable for movement with swapped dimensions.

Method *Subset*
^^^^^^^^^^^^^^^

``View Subset(System.Int32 x, System.Int32 width, System.Int32 y, System.Int32 height, System.Int32 z, System.Int32 depth)``

Returns a subset of this view.

The method **Subset** has the following parameters:

+--------------+--------------------+------------------------------------------+
| Parameter    | Type               | Description                              |
+==============+====================+==========================================+
| ``x``        | ``System.Int32``   | The horizontal position of the subset.   |
+--------------+--------------------+------------------------------------------+
| ``width``    | ``System.Int32``   | The width of the subset.                 |
+--------------+--------------------+------------------------------------------+
| ``y``        | ``System.Int32``   | The vertical position of the subset.     |
+--------------+--------------------+------------------------------------------+
| ``height``   | ``System.Int32``   | The height of the subset.                |
+--------------+--------------------+------------------------------------------+
| ``z``        | ``System.Int32``   | The planar position of the subset.       |
+--------------+--------------------+------------------------------------------+
| ``depth``    | ``System.Int32``   | The depth of the subset.                 |
+--------------+--------------------+------------------------------------------+

This adjusts the position of the begin locator according to the subset, as well as the sizes, so that the view is suitable for movement within the subset.

Method *IntersectWithBox*
^^^^^^^^^^^^^^^^^^^^^^^^^

``View IntersectWithBox(BoxInt32 clip)``

Intersects the view with a box in the same coordinate range.

The method **IntersectWithBox** has the following parameters:

+-------------+----------------+----------------------------------------------------------+
| Parameter   | Type           | Description                                              |
+=============+================+==========================================================+
| ``clip``    | ``BoxInt32``   | Reference to the box used to intersect with this view.   |
+-------------+----------------+----------------------------------------------------------+

The box coordinates will be used to change the begin and size of the returned view.

Method *IntersectWithBox*
^^^^^^^^^^^^^^^^^^^^^^^^^

``View IntersectWithBox(Box3dInt32 clip)``

Intersects the view with a box in the same coordinate range.

The method **IntersectWithBox** has the following parameters:

+-------------+------------------+--------------------------------------------------------------+
| Parameter   | Type             | Description                                                  |
+=============+==================+==============================================================+
| ``clip``    | ``Box3dInt32``   | Reference to the box\_3d used to intersect with this view.   |
+-------------+------------------+--------------------------------------------------------------+

The box coordinates will be used to change the begin and size of the returned view.

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Enumerations
~~~~~~~~~~~~

Enumeration *DimensionSwapId*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum DimensionSwapId``

TODO documentation missing

The enumeration **DimensionSwapId** has the following constants:

+---------------+---------+---------------+
| Name          | Value   | Description   |
+===============+=========+===============+
| ``xyz2xyz``   | ``1``   |               |
+---------------+---------+---------------+
| ``xyz2xzy``   | ``2``   |               |
+---------------+---------+---------------+
| ``xyz2yxz``   | ``3``   |               |
+---------------+---------+---------------+
| ``xyz2yzx``   | ``4``   |               |
+---------------+---------+---------------+
| ``xyz2zxy``   | ``5``   |               |
+---------------+---------+---------------+
| ``xyz2zyx``   | ``6``   |               |
+---------------+---------+---------------+

::

    enum DimensionSwapId
    {
      xyz2xyz = 1,
      xyz2xzy = 2,
      xyz2yxz = 3,
      xyz2yzx = 4,
      xyz2zxy = 5,
      xyz2zyx = 6,
    };

TODO documentation missing
