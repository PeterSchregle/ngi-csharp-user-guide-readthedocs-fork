Class *Edge1d*
--------------

The edge\_1d as returned by gauging.

**Namespace:** Ngi

**Module:** AnalysisMeasuring

The class **Edge1d** implements the following interfaces:

+------------------------+
| Interface              |
+========================+
| ``IEquatableEdge1d``   |
+------------------------+
| ``ISerializable``      |
+------------------------+

The class **Edge1d** contains the following properties:

+----------------+-------+-------+-------------------------------+
| Property       | Get   | Set   | Description                   |
+================+=======+=======+===============================+
| ``Position``   | \*    |       | The position of the edge.     |
+----------------+-------+-------+-------------------------------+
| ``Strength``   | \*    |       | The strength of the edge.     |
+----------------+-------+-------+-------------------------------+
| ``Grey``       | \*    |       | The grey value of the edge.   |
+----------------+-------+-------+-------------------------------+

The class **Edge1d** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

Edge characteristics are the position, the strength and the grey value.

The following operators are implemented for a edge\_1d: operator == : comparison for equality. operator != : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Edge1d*
^^^^^^^^^^^^^^^^^^^^

``Edge1d()``

Standard constructor.

Constructors
~~~~~~~~~~~~

Constructor *Edge1d*
^^^^^^^^^^^^^^^^^^^^

``Edge1d(System.Double position, System.Double strength, System.Double grey)``

Standard constructor.

The constructor has the following parameters:

+----------------+---------------------+-------------------------------+
| Parameter      | Type                | Description                   |
+================+=====================+===============================+
| ``position``   | ``System.Double``   | The position of the edge.     |
+----------------+---------------------+-------------------------------+
| ``strength``   | ``System.Double``   | The strength of the edge.     |
+----------------+---------------------+-------------------------------+
| ``grey``       | ``System.Double``   | The grey value of the edge.   |
+----------------+---------------------+-------------------------------+

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``System.Double Position``

The position of the edge.

Property *Strength*
^^^^^^^^^^^^^^^^^^^

``System.Double Strength``

The strength of the edge.

Property *Grey*
^^^^^^^^^^^^^^^

``System.Double Grey``

The grey value of the edge.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
