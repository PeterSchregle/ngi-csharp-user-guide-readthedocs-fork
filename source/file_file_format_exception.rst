Class *FileFormatException*
---------------------------

The basic ngi image file format exception type.

**Namespace:** Ngi

**Module:**

The class **FileFormatException** contains the following enumerations:

+--------------------+------------------------------+
| Enumeration        | Description                  |
+====================+==============================+
| ``ExceptionIds``   | TODO documentation missing   |
+--------------------+------------------------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *FileFormatException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``FileFormatException()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *FileFormatException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``FileFormatException(System.String message)``

Construct an exception from a message.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+

Constructor *FileFormatException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``FileFormatException(System.String message, System.UInt32 id)``

Construct an exception from a message and an id.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+
| ``id``        | ``System.UInt32``   | The exception id.        |
+---------------+---------------------+--------------------------+

Enumerations
~~~~~~~~~~~~

Enumeration *ExceptionIds*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum ExceptionIds``

TODO documentation missing

The enumeration **ExceptionIds** has the following constants:

+-------------------------+---------+---------------+
| Name                    | Value   | Description   |
+=========================+=========+===============+
| ``notAnError``          | ``0``   |               |
+-------------------------+---------+---------------+
| ``unknownError``        | ``1``   |               |
+-------------------------+---------+---------------+
| ``couldNotLoad``        | ``2``   |               |
+-------------------------+---------+---------------+
| ``typeMismatch``        | ``3``   |               |
+-------------------------+---------+---------------+
| ``couldNotSave``        | ``4``   |               |
+-------------------------+---------+---------------+
| ``unknownFileFormat``   | ``5``   |               |
+-------------------------+---------+---------------+

::

    enum ExceptionIds
    {
      notAnError = 0,
      unknownError = 1,
      couldNotLoad = 2,
      typeMismatch = 3,
      couldNotSave = 4,
      unknownFileFormat = 5,
    };

TODO documentation missing
