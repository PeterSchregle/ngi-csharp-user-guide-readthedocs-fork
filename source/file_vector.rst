Class *Vector*
--------------

A vector in two-dimensional euclidean space.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Vector** implements the following interfaces:

+-----------------------------------------------+
| Interface                                     |
+===============================================+
| ``IEquatableVectorVectorCoordinateVariant``   |
+-----------------------------------------------+
| ``ISerializable``                             |
+-----------------------------------------------+
| ``INotifyPropertyChanged``                    |
+-----------------------------------------------+

The class **Vector** contains the following variant parameters:

+------------------+-----------------------------------------+
| Variant          | Description                             |
+==================+=========================================+
| ``Coordinate``   | TODO no brief description for variant   |
+------------------+-----------------------------------------+

The class **Vector** contains the following properties:

+---------------------+-------+-------+------------------------------------------------------------+
| Property            | Get   | Set   | Description                                                |
+=====================+=======+=======+============================================================+
| ``Dx``              | \*    | \*    | The horizontal component of the vector.                    |
+---------------------+-------+-------+------------------------------------------------------------+
| ``Dy``              | \*    | \*    | The vertical component of the vector.                      |
+---------------------+-------+-------+------------------------------------------------------------+
| ``Direction``       | \*    |       | The direction of the vector.                               |
+---------------------+-------+-------+------------------------------------------------------------+
| ``Dir``             | \*    |       | The direction of the vector (for backward compatiblity).   |
+---------------------+-------+-------+------------------------------------------------------------+
| ``Angle``           | \*    |       | The angle of the vector.                                   |
+---------------------+-------+-------+------------------------------------------------------------+
| ``Normal``          | \*    |       | The normal direction.                                      |
+---------------------+-------+-------+------------------------------------------------------------+
| ``SquaredLength``   | \*    |       | Get the squared length.                                    |
+---------------------+-------+-------+------------------------------------------------------------+
| ``Length``          | \*    |       | Get the length.                                            |
+---------------------+-------+-------+------------------------------------------------------------+

The class **Vector** contains the following methods:

+------------------+---------------------------------------------------------+
| Method           | Description                                             |
+==================+=========================================================+
| ``DotProduct``   | Calculate the dot-product of two vectors.               |
+------------------+---------------------------------------------------------+
| ``AngleTo``      | Calculate the angle between two vectors.                |
+------------------+---------------------------------------------------------+
| ``Rotate``       | Rotate a vector.                                        |
+------------------+---------------------------------------------------------+
| ``ToString``     | Provide string representation for debugging purposes.   |
+------------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

A vector can be seen as a vector in two-dimensional space.

The following operations are implemented: operator== : comparison for equality. operator != : comparison for inequality. operator- : negation, directly implemented. operator+= : addition, directly implemented. operator+ : addition, implemented through boost::additive operator-= : subtraction, directly implemented. operator- : subtraction, implemented through boost::additive operator\ *= : multiplication, directly implemented. operator* : multiplication, implemented through boost::multiplicative operator/= : division, directly implemented. operator/ : division, implemented through boost::multiplicative

Variants
~~~~~~~~

Variant *Coordinate*
^^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Coordinate** has the following types:

+--------------+
| Type         |
+==============+
| ``Int32``    |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Vector*
^^^^^^^^^^^^^^^^^^^^

``Vector()``

Default constructor.

Constructs a vector with unit length pointing in the direction of the positive x-axis.

Constructors
~~~~~~~~~~~~

Constructor *Vector*
^^^^^^^^^^^^^^^^^^^^

``Vector(System.Object dx, System.Object dy)``

Standard constructor.

The constructor has the following parameters:

+-------------+---------------------+-------------------------+
| Parameter   | Type                | Description             |
+=============+=====================+=========================+
| ``dx``      | ``System.Object``   | The horizontal width.   |
+-------------+---------------------+-------------------------+
| ``dy``      | ``System.Object``   | The vertical height.    |
+-------------+---------------------+-------------------------+

Constructs a vector with specific coordinates.

Constructor *Vector*
^^^^^^^^^^^^^^^^^^^^

``Vector(Direction direction)``

Standard converting constructor.

The constructor has the following parameters:

+-----------------+-----------------+---------------+
| Parameter       | Type            | Description   |
+=================+=================+===============+
| ``direction``   | ``Direction``   |               |
+-----------------+-----------------+---------------+

Properties
~~~~~~~~~~

Property *Dx*
^^^^^^^^^^^^^

``System.Object Dx``

The horizontal component of the vector.

Property *Dy*
^^^^^^^^^^^^^

``System.Object Dy``

The vertical component of the vector.

Property *Direction*
^^^^^^^^^^^^^^^^^^^^

``Direction Direction``

The direction of the vector.

The zero vector is direction-less, so you should check for this before you attempt to call this function.

Property *Dir*
^^^^^^^^^^^^^^

``Direction Dir``

The direction of the vector (for backward compatiblity).

The zero vector is direction-less, so you should check for this before you attempt to call this function.

Property *Angle*
^^^^^^^^^^^^^^^^

``Angle Angle``

The angle of the vector.

Property *Normal*
^^^^^^^^^^^^^^^^^

``Direction Normal``

The normal direction.

The zero vector can not be normalized, so you should check for this before you attempt to call this function.

Property *SquaredLength*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double SquaredLength``

Get the squared length.

This function calculates the length of the vector.

Property *Length*
^^^^^^^^^^^^^^^^^

``System.Double Length``

Get the length.

This function calculates the length of the vector.

Methods
~~~~~~~

Method *DotProduct*
^^^^^^^^^^^^^^^^^^^

``System.Object DotProduct(Vector rhs)``

Calculate the dot-product of two vectors.

The method **DotProduct** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``rhs``     | ``Vector``   |               |
+-------------+--------------+---------------+

Method *AngleTo*
^^^^^^^^^^^^^^^^

``System.Double AngleTo(Vector rhs)``

Calculate the angle between two vectors.

The method **AngleTo** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``rhs``     | ``Vector``   |               |
+-------------+--------------+---------------+

Method *Rotate*
^^^^^^^^^^^^^^^

``VectorDouble Rotate(System.Double angle)``

Rotate a vector.

The method **Rotate** has the following parameters:

+-------------+---------------------+----------------------------------+
| Parameter   | Type                | Description                      |
+=============+=====================+==================================+
| ``angle``   | ``System.Double``   | The rotation angle in radians.   |
+-------------+---------------------+----------------------------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO documentation missing

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO documentation missing
