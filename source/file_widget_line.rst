Class *WidgetLine*
------------------

A widget\_line displays a line on a surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetLine** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetLine** contains the following properties:

+----------------+-------+-------+-------------------------------+
| Property       | Get   | Set   | Description                   |
+================+=======+=======+===============================+
| ``Position``   | \*    | \*    | The position of the widget.   |
+----------------+-------+-------+-------------------------------+
| ``Pen``        | \*    | \*    | The pen of the widget.        |
+----------------+-------+-------+-------------------------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_line. These settings affect the look and the behavior of the widget\_line.

The widget\_line supports interactivity via the widget\_interactive base class, i.e. it can be selected, move around. In general it can react on mouse and keyboard interactions. A widget\_line has interactive widget capabilities.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetLine*
^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetLine()``

Default constructor.

The default constructor creates a widget\_line with default settings.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``LineDouble Position``

The position of the widget.

Property *Pen*
^^^^^^^^^^^^^^

``PenByte Pen``

The pen of the widget.
