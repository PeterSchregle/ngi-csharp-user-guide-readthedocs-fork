Class *CadParser*
-----------------

**Namespace:** Ngi

**Module:** CadPdf

Description
~~~~~~~~~~~

Static Methods
~~~~~~~~~~~~~~

Method *ParseDxfFile*
^^^^^^^^^^^^^^^^^^^^^

``CadDrawing ParseDxfFile(System.String fileName, System.String layer)``

Opens and parses a DXF file.

The method **ParseDxfFile** has the following parameters:

+----------------+---------------------+---------------+
| Parameter      | Type                | Description   |
+================+=====================+===============+
| ``fileName``   | ``System.String``   |               |
+----------------+---------------------+---------------+
| ``layer``      | ``System.String``   |               |
+----------------+---------------------+---------------+

The function parses the DXF files and converts all the contained LINE, ARC and CIRCLE primitives into nGI geometric counterparts. If the geometries build closed contours, they are filled. The geometries of the passed layer are used.

A cad\_drawing, containing the geometries.

Method *ParseGeoFile*
^^^^^^^^^^^^^^^^^^^^^

``CadDrawing ParseGeoFile(System.String fileName)``

Opens and parses a GEO file.

The method **ParseGeoFile** has the following parameters:

+----------------+---------------------+---------------+
| Parameter      | Type                | Description   |
+================+=====================+===============+
| ``fileName``   | ``System.String``   |               |
+----------------+---------------------+---------------+

The function parses the GEO files and converts all the contained LINE, ARC and CIRCLE primitives into nGI geometric counterparts. If the geometries build closed contours, they are filled.

A cad\_drawing, containing the geometries.
