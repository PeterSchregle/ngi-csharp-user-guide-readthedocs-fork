Class *Pen*
-----------

A drawing pen.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Pen** implements the following interfaces:

+--------------------------------------+
| Interface                            |
+======================================+
| ``IEquatablePenPenPrimaryVariant``   |
+--------------------------------------+
| ``ISerializable``                    |
+--------------------------------------+
| ``INotifyPropertyChanged``           |
+--------------------------------------+

The class **Pen** contains the following variant parameters:

+---------------+-----------------------------------------+
| Variant       | Description                             |
+===============+=========================================+
| ``Primary``   | TODO no brief description for variant   |
+---------------+-----------------------------------------+

The class **Pen** contains the following properties:

+-------------------------+-------+-------+-----------------------------------+
| Property                | Get   | Set   | Description                       |
+=========================+=======+=======+===================================+
| ``Brush``               | \*    | \*    | The color of the pen.             |
+-------------------------+-------+-------+-----------------------------------+
| ``Width``               | \*    | \*    | The width of the pen.             |
+-------------------------+-------+-------+-----------------------------------+
| ``DashStyle``           | \*    | \*    | The dash style of the pen.        |
+-------------------------+-------+-------+-----------------------------------+
| ``DashOffset``          | \*    | \*    | The dash offset of the pen.       |
+-------------------------+-------+-------+-----------------------------------+
| ``StartCapStyle``       | \*    | \*    | The start cap style of the pen.   |
+-------------------------+-------+-------+-----------------------------------+
| ``EndCapStyle``         | \*    | \*    | The end cap style of the pen.     |
+-------------------------+-------+-------+-----------------------------------+
| ``DashCapStyle``        | \*    | \*    | The dash cap style of the pen.    |
+-------------------------+-------+-------+-----------------------------------+
| ``LineJoinStyle``       | \*    | \*    | The line join style of the pen.   |
+-------------------------+-------+-------+-----------------------------------+
| ``MiterLimit``          | \*    | \*    | The miter limit of the pen.       |
+-------------------------+-------+-------+-----------------------------------+
| ``StartLineEndStyle``   | \*    | \*    | The start line end style.         |
+-------------------------+-------+-------+-----------------------------------+
| ``StartLineEndSize``    | \*    | \*    | The start line end size.          |
+-------------------------+-------+-------+-----------------------------------+
| ``StopLineEndStyle``    | \*    | \*    | The stop line end style.          |
+-------------------------+-------+-------+-----------------------------------+
| ``StopLineEndSize``     | \*    | \*    | The stop line end size.           |
+-------------------------+-------+-------+-----------------------------------+

The class **Pen** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

The class **Pen** contains the following enumerations:

+-------------------------+------------------------------+
| Enumeration             | Description                  |
+=========================+==============================+
| ``DashStyleType``       | TODO documentation missing   |
+-------------------------+------------------------------+
| ``CapStyleType``        | TODO documentation missing   |
+-------------------------+------------------------------+
| ``LineJoinStyleType``   | TODO documentation missing   |
+-------------------------+------------------------------+
| ``LineEndStyleType``    | TODO documentation missing   |
+-------------------------+------------------------------+

Description
~~~~~~~~~~~

A drawing pen has a brush, a width, a dash style, a dash offset, a cap style for start, end and dash caps, a line join style and a miter limit; It is used to draw outlines.

If the pen is used to draw rays and line\_segments, it also can be used to specify additional line ends at the begin and end.

The width of a pen is scaled with the transform if the width is positive. The width is not scaled with the transform when specified as a negative number. You can use a zero width to specify the narrowest possible pen.

The following operators are implemented for a pen: operator == : comparison for equality. operator != : comparison for inequality.

Variants
~~~~~~~~

Variant *Primary*
^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Primary** has the following types:

+--------------+
| Type         |
+==============+
| ``Byte``     |
+--------------+
| ``UInt16``   |
+--------------+
| ``UInt32``   |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Pen*
^^^^^^^^^^^^^^^^^

``Pen()``

Standard constructor.

Constructors
~~~~~~~~~~~~

Constructor *Pen*
^^^^^^^^^^^^^^^^^

``Pen(SolidColorBrush brush, System.Double width, Pen.DashStyleType dashStyle, System.Double dashOffset, Pen.CapStyleType startCapStyle, Pen.CapStyleType endCapStyle, Pen.CapStyleType dashCapStyle, Pen.LineJoinStyleType lineJoinStyle, System.Double miterLimit, Pen.LineEndStyleType startLineEndStyle, System.Double startLineEndSize, Pen.LineEndStyleType stopLineEndStyle, System.Double stopLineEndSize)``

Standard constructor.

The constructor has the following parameters:

+-------------------------+-----------------------------+-------------------------------------------------+
| Parameter               | Type                        | Description                                     |
+=========================+=============================+=================================================+
| ``brush``               | ``SolidColorBrush``         |                                                 |
+-------------------------+-----------------------------+-------------------------------------------------+
| ``width``               | ``System.Double``           | A width (defaults to 1.0).                      |
+-------------------------+-----------------------------+-------------------------------------------------+
| ``dashStyle``           | ``Pen.DashStyleType``       | Dash style (defaults to solid).                 |
+-------------------------+-----------------------------+-------------------------------------------------+
| ``dashOffset``          | ``System.Double``           | The dash offset (defaults to 0.0).              |
+-------------------------+-----------------------------+-------------------------------------------------+
| ``startCapStyle``       | ``Pen.CapStyleType``        | Start cap style (defaults to square\_cap).      |
+-------------------------+-----------------------------+-------------------------------------------------+
| ``endCapStyle``         | ``Pen.CapStyleType``        | End cap style (defaults to square\_cap).        |
+-------------------------+-----------------------------+-------------------------------------------------+
| ``dashCapStyle``        | ``Pen.CapStyleType``        | Dash cap style (defaults to square\_cap).       |
+-------------------------+-----------------------------+-------------------------------------------------+
| ``lineJoinStyle``       | ``Pen.LineJoinStyleType``   | Line join style (defaults to miter\_join).      |
+-------------------------+-----------------------------+-------------------------------------------------+
| ``miterLimit``          | ``System.Double``           | The miter limit (defaults to 0.0).              |
+-------------------------+-----------------------------+-------------------------------------------------+
| ``startLineEndStyle``   | ``Pen.LineEndStyleType``    | The start line end (defaults to normal\_end).   |
+-------------------------+-----------------------------+-------------------------------------------------+
| ``startLineEndSize``    | ``System.Double``           | The start line end size (defaults to 0.0).      |
+-------------------------+-----------------------------+-------------------------------------------------+
| ``stopLineEndStyle``    | ``Pen.LineEndStyleType``    | The stop line end (defaults to normal\_end).    |
+-------------------------+-----------------------------+-------------------------------------------------+
| ``stopLineEndSize``     | ``System.Double``           | The stop line end size (defaults to 0.0).       |
+-------------------------+-----------------------------+-------------------------------------------------+

Properties
~~~~~~~~~~

Property *Brush*
^^^^^^^^^^^^^^^^

``SolidColorBrush Brush``

The color of the pen.

Property *Width*
^^^^^^^^^^^^^^^^

``System.Double Width``

The width of the pen.

Property *DashStyle*
^^^^^^^^^^^^^^^^^^^^

``Pen.DashStyleType DashStyle``

The dash style of the pen.

The type is int instead of dash\_style\_type to facilitate automatic casts.

Property *DashOffset*
^^^^^^^^^^^^^^^^^^^^^

``System.Double DashOffset``

The dash offset of the pen.

A value that specifies an offset in the dash sequence. A positive dash offset value shifts the dash pattern, in units of stroke width, toward the start of the stroked geometry. A negative dash offset value shifts the dash pattern, in units of stroke width, toward the end of the stroked geometry.

Property *StartCapStyle*
^^^^^^^^^^^^^^^^^^^^^^^^

``Pen.CapStyleType StartCapStyle``

The start cap style of the pen.

The type is int instead of cap\_style\_type to facilitate automatic casts.

Property *EndCapStyle*
^^^^^^^^^^^^^^^^^^^^^^

``Pen.CapStyleType EndCapStyle``

The end cap style of the pen.

The type is int instead of cap\_style\_type to facilitate automatic casts.

Property *DashCapStyle*
^^^^^^^^^^^^^^^^^^^^^^^

``Pen.CapStyleType DashCapStyle``

The dash cap style of the pen.

The type is int instead of cap\_style\_type to facilitate automatic casts.

Property *LineJoinStyle*
^^^^^^^^^^^^^^^^^^^^^^^^

``Pen.LineJoinStyleType LineJoinStyle``

The line join style of the pen.

The type is int instead of line\_join\_style\_type to facilitate automatic casts.

Property *MiterLimit*
^^^^^^^^^^^^^^^^^^^^^

``System.Double MiterLimit``

The miter limit of the pen.

This value is always treated as if it were larger or equal to 1.0.

Property *StartLineEndStyle*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Pen.LineEndStyleType StartLineEndStyle``

The start line end style.

The type is int instead of line\_end\_style\_type to facilitate automatic casts.

Property *StartLineEndSize*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double StartLineEndSize``

The start line end size.

Property *StopLineEndStyle*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Pen.LineEndStyleType StopLineEndStyle``

The stop line end style.

The type is int instead of line\_end\_style\_type to facilitate automatic casts.

Property *StopLineEndSize*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double StopLineEndSize``

The stop line end size.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Enumerations
~~~~~~~~~~~~

Enumeration *DashStyleType*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum DashStyleType``

TODO documentation missing

The enumeration **DashStyleType** has the following constants:

+---------------------+---------+---------------+
| Name                | Value   | Description   |
+=====================+=========+===============+
| ``solid``           | ``0``   |               |
+---------------------+---------+---------------+
| ``dashed``          | ``1``   |               |
+---------------------+---------+---------------+
| ``dotted``          | ``2``   |               |
+---------------------+---------+---------------+
| ``dashdotted``      | ``3``   |               |
+---------------------+---------+---------------+
| ``dashdotdotted``   | ``4``   |               |
+---------------------+---------+---------------+

::

    enum DashStyleType
    {
      solid = 0,
      dashed = 1,
      dotted = 2,
      dashdotted = 3,
      dashdotdotted = 4,
    };

TODO documentation missing

Enumeration *CapStyleType*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum CapStyleType``

TODO documentation missing

The enumeration **CapStyleType** has the following constants:

+-------------------+---------+---------------+
| Name              | Value   | Description   |
+===================+=========+===============+
| ``flatCap``       | ``0``   |               |
+-------------------+---------+---------------+
| ``squareCap``     | ``1``   |               |
+-------------------+---------+---------------+
| ``roundCap``      | ``2``   |               |
+-------------------+---------+---------------+
| ``triangleCap``   | ``3``   |               |
+-------------------+---------+---------------+

::

    enum CapStyleType
    {
      flatCap = 0,
      squareCap = 1,
      roundCap = 2,
      triangleCap = 3,
    };

TODO documentation missing

Enumeration *LineJoinStyleType*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum LineJoinStyleType``

TODO documentation missing

The enumeration **LineJoinStyleType** has the following constants:

+------------------------+---------+---------------+
| Name                   | Value   | Description   |
+========================+=========+===============+
| ``miterJoin``          | ``0``   |               |
+------------------------+---------+---------------+
| ``bevelJoin``          | ``1``   |               |
+------------------------+---------+---------------+
| ``roundJoin``          | ``2``   |               |
+------------------------+---------+---------------+
| ``miterOrBevelJoin``   | ``3``   |               |
+------------------------+---------+---------------+

::

    enum LineJoinStyleType
    {
      miterJoin = 0,
      bevelJoin = 1,
      roundJoin = 2,
      miterOrBevelJoin = 3,
    };

TODO documentation missing

Enumeration *LineEndStyleType*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum LineEndStyleType``

TODO documentation missing

The enumeration **LineEndStyleType** has the following constants:

+-----------------------------+----------+---------------+
| Name                        | Value    | Description   |
+=============================+==========+===============+
| ``normalEnd``               | ``0``    |               |
+-----------------------------+----------+---------------+
| ``filledDiskEnd``           | ``1``    |               |
+-----------------------------+----------+---------------+
| ``filledSquareEnd``         | ``2``    |               |
+-----------------------------+----------+---------------+
| ``filledDiamondEnd``        | ``3``    |               |
+-----------------------------+----------+---------------+
| ``filledArrowOutEnd``       | ``4``    |               |
+-----------------------------+----------+---------------+
| ``filledArrowInEnd``        | ``5``    |               |
+-----------------------------+----------+---------------+
| ``barEnd``                  | ``6``    |               |
+-----------------------------+----------+---------------+
| ``backslashEnd``            | ``7``    |               |
+-----------------------------+----------+---------------+
| ``slashEnd``                | ``8``    |               |
+-----------------------------+----------+---------------+
| ``arrowOutEnd``             | ``9``    |               |
+-----------------------------+----------+---------------+
| ``arrowInEnd``              | ``10``   |               |
+-----------------------------+----------+---------------+
| ``filledSlimArrowOutEnd``   | ``11``   |               |
+-----------------------------+----------+---------------+
| ``filledSlimArrowInEnd``    | ``12``   |               |
+-----------------------------+----------+---------------+
| ``slimArrowOutEnd``         | ``13``   |               |
+-----------------------------+----------+---------------+
| ``slimArrowInEnd``          | ``14``   |               |
+-----------------------------+----------+---------------+

::

    enum LineEndStyleType
    {
      normalEnd = 0,
      filledDiskEnd = 1,
      filledSquareEnd = 2,
      filledDiamondEnd = 3,
      filledArrowOutEnd = 4,
      filledArrowInEnd = 5,
      barEnd = 6,
      backslashEnd = 7,
      slashEnd = 8,
      arrowOutEnd = 9,
      arrowInEnd = 10,
      filledSlimArrowOutEnd = 11,
      filledSlimArrowInEnd = 12,
      slimArrowOutEnd = 13,
      slimArrowInEnd = 14,
    };

TODO documentation missing

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO documentation missing

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO documentation missing
