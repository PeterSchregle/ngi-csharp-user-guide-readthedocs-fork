Class *TeighaLayer*
-------------------

A class holding a layer parsed from a dxf file.

**Namespace:** Ngi

**Module:** CadPdf

The class **TeighaLayer** implements the following interfaces:

+-----------------------------+
| Interface                   |
+=============================+
| ``IEquatableTeighaLayer``   |
+-----------------------------+

The class **TeighaLayer** contains the following properties:

+-------------------+-------+-------+--------------------------+
| Property          | Get   | Set   | Description              |
+===================+=======+=======+==========================+
| ``Name``          | \*    | \*    | The name of the layer.   |
+-------------------+-------+-------+--------------------------+
| ``Description``   | \*    | \*    | The name of the layer.   |
+-------------------+-------+-------+--------------------------+
| ``Pen``           | \*    | \*    | The pen of the layer.    |
+-------------------+-------+-------+--------------------------+
| ``Contours``      | \*    | \*    | The list of contours.    |
+-------------------+-------+-------+--------------------------+

The class **TeighaLayer** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``Render``     | Render a part of the layer into an image.               |
+----------------+---------------------------------------------------------+
| ``Render``     | Render a part of the layer into an image.               |
+----------------+---------------------------------------------------------+
| ``Render``     | Render the whole layer into an image.                   |
+----------------+---------------------------------------------------------+
| ``Render``     | Render the whole layer into an image.                   |
+----------------+---------------------------------------------------------+
| ``Render``     | Render a part of the layer into an image.               |
+----------------+---------------------------------------------------------+
| ``Render``     | Render a part of the layer into an image.               |
+----------------+---------------------------------------------------------+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

A consists of a list of contours.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *TeighaLayer*
^^^^^^^^^^^^^^^^^^^^^^^^^

``TeighaLayer()``

Constructs an empty teigha\_layer.

Properties
~~~~~~~~~~

Property *Name*
^^^^^^^^^^^^^^^

``System.String Name``

The name of the layer.

Property *Description*
^^^^^^^^^^^^^^^^^^^^^^

``System.String Description``

The name of the layer.

Property *Pen*
^^^^^^^^^^^^^^

``PenByte Pen``

The pen of the layer.

Property *Contours*
^^^^^^^^^^^^^^^^^^^

``CadContourList Contours``

The list of contours.

Methods
~~~~~~~

Method *Render*
^^^^^^^^^^^^^^^

``ImageRgbByte Render(System.Double opticalResolution, System.Boolean filled, RgbByte background)``

Render a part of the layer into an image.

The method **Render** has the following parameters:

+--------------+------------+----------------------------------------------------+
| Parameter    | Type       | Description                                        |
+==============+============+====================================================+
| ``opticalRes | ``System.D | The optical resolution of the rendering.This is    |
| olution``    | ouble``    | the size of a pixel in mm. The inverse is the      |
|              |            | number of pixels per mm, which does not need to be |
|              |            | an integral number.                                |
+--------------+------------+----------------------------------------------------+
| ``filled``   | ``System.B | Draw the contours filled (true) or outlined        |
|              | oolean``   | (false).                                           |
+--------------+------------+----------------------------------------------------+
| ``background | ``RgbByte` | The background color.                              |
| ``           | `          |                                                    |
+--------------+------------+----------------------------------------------------+

The part of the image that should be rendered is given by a box. The box coordinates are in the coordinate system of the CAD drawing.

The coordinate system of the CAD drawing is a right-handed coordinate system, where the positive x-axis extends to the right and the positive y-axis extends to the top.

The coordinate system in an image is a left-handed coordinate system, where the positive x-axis extends to the right and the positive y-axis extends to the bottom.

The function calculates an internal coordinate transformation so that the rendering in the image is in the same orientation as in the CAD drawing (its y coordinates have been flipped upside down).

Method *Render*
^^^^^^^^^^^^^^^

``ImageRgbByte Render(System.Double opticalResolution, System.Boolean filled, RgbByte foreground, RgbByte background)``

Render a part of the layer into an image.

The method **Render** has the following parameters:

+--------------+------------+----------------------------------------------------+
| Parameter    | Type       | Description                                        |
+==============+============+====================================================+
| ``opticalRes | ``System.D | The optical resolution of the rendering.This is    |
| olution``    | ouble``    | the size of a pixel in the units of the cad        |
|              |            | contours, whatever it is.                          |
+--------------+------------+----------------------------------------------------+
| ``filled``   | ``System.B | Draw the contours filled (true) or outlined        |
|              | oolean``   | (false).                                           |
+--------------+------------+----------------------------------------------------+
| ``foreground | ``RgbByte` | The foreground color.                              |
| ``           | `          |                                                    |
+--------------+------------+----------------------------------------------------+
| ``background | ``RgbByte` | The background color.                              |
| ``           | `          |                                                    |
+--------------+------------+----------------------------------------------------+

The part of the image that should be rendered is given by a box. The box coordinates are in the coordinate system of the CAD drawing.

The coordinate system of the CAD drawing is a right-handed coordinate system, where the positive x-axis extends to the right and the positive y-axis extends to the top.

The coordinate system in an image is a left-handed coordinate system, where the positive x-axis extends to the right and the positive y-axis extends to the bottom.

The function calculates an internal coordinate transformation so that the rendering in the image is in the same orientation as in the CAD drawing (its y coordinates have been flipped upside down).

Method *Render*
^^^^^^^^^^^^^^^

``ImageRgbByte Render(BoxDouble part, System.Double opticalResolution, System.Boolean filled, RgbByte background)``

Render the whole layer into an image.

The method **Render** has the following parameters:

+--------------+------------+----------------------------------------------------+
| Parameter    | Type       | Description                                        |
+==============+============+====================================================+
| ``part``     | ``BoxDoubl |                                                    |
|              | e``        |                                                    |
+--------------+------------+----------------------------------------------------+
| ``opticalRes | ``System.D | The optical resolution of the rendering.This is    |
| olution``    | ouble``    | the size of a pixel in mm. The inverse is the      |
|              |            | number of pixels per mm, which does not need to be |
|              |            | an integral number.                                |
+--------------+------------+----------------------------------------------------+
| ``filled``   | ``System.B | Draw the contours filled (true) or outlined        |
|              | oolean``   | (false).                                           |
+--------------+------------+----------------------------------------------------+
| ``background | ``RgbByte` | The background color.                              |
| ``           | `          |                                                    |
+--------------+------------+----------------------------------------------------+

The coordinate system of the CAD drawing is a right-handed coordinate system, where the positive x-axis extends to the right and the positive y-axis extends to the top.

The coordinate system in an image is a left-handed coordinate system, where the positive x-axis extends to the right and the positive y-axis extends to the bottom.

The function calculates an internal coordinate transformation so that the rendering in the image is in the same orientation as in the CAD drawing (its y coordinates have been flipped upside down).

Method *Render*
^^^^^^^^^^^^^^^

``ImageRgbByte Render(BoxDouble part, System.Double opticalResolution, System.Boolean filled, RgbByte foreground, RgbByte background)``

Render the whole layer into an image.

The method **Render** has the following parameters:

+--------------+------------+----------------------------------------------------+
| Parameter    | Type       | Description                                        |
+==============+============+====================================================+
| ``part``     | ``BoxDoubl |                                                    |
|              | e``        |                                                    |
+--------------+------------+----------------------------------------------------+
| ``opticalRes | ``System.D | The optical resolution of the rendering.This is    |
| olution``    | ouble``    | the size of a pixel in mm. The inverse is the      |
|              |            | number of pixels per mm, which does not need to be |
|              |            | an integral number.                                |
+--------------+------------+----------------------------------------------------+
| ``filled``   | ``System.B | Draw the contours filled (true) or outlined        |
|              | oolean``   | (false).                                           |
+--------------+------------+----------------------------------------------------+
| ``foreground | ``RgbByte` | The foreground color.                              |
| ``           | `          |                                                    |
+--------------+------------+----------------------------------------------------+
| ``background | ``RgbByte` | The background color.                              |
| ``           | `          |                                                    |
+--------------+------------+----------------------------------------------------+

The coordinate system of the CAD drawing is a right-handed coordinate system, where the positive x-axis extends to the right and the positive y-axis extends to the top.

The coordinate system in an image is a left-handed coordinate system, where the positive x-axis extends to the right and the positive y-axis extends to the bottom.

The function calculates an internal coordinate transformation so that the rendering in the image is in the same orientation as in the CAD drawing (its y coordinates have been flipped upside down).

Method *Render*
^^^^^^^^^^^^^^^

``ImageRgbByte Render(AffineMatrix transformation, VectorInt32 size, System.Boolean filled, RgbByte background)``

Render a part of the layer into an image.

The method **Render** has the following parameters:

+----------------------+----------------------+--------------------------------------------------------+
| Parameter            | Type                 | Description                                            |
+======================+======================+========================================================+
| ``transformation``   | ``AffineMatrix``     | The transformation matrix.                             |
+----------------------+----------------------+--------------------------------------------------------+
| ``size``             | ``VectorInt32``      | The size of the image that should be rendered.         |
+----------------------+----------------------+--------------------------------------------------------+
| ``filled``           | ``System.Boolean``   | Draw the contours filled (true) or outlined (false).   |
+----------------------+----------------------+--------------------------------------------------------+
| ``background``       | ``RgbByte``          | The background color.                                  |
+----------------------+----------------------+--------------------------------------------------------+

The part of the drawing to be rendered is given by a transformation matrix and a size.

Method *Render*
^^^^^^^^^^^^^^^

``ImageRgbByte Render(AffineMatrix transformation, VectorInt32 size, System.Boolean filled, RgbByte foreground, RgbByte background)``

Render a part of the layer into an image.

The method **Render** has the following parameters:

+----------------------+----------------------+--------------------------------------------------------+
| Parameter            | Type                 | Description                                            |
+======================+======================+========================================================+
| ``transformation``   | ``AffineMatrix``     | The transformation matrix.                             |
+----------------------+----------------------+--------------------------------------------------------+
| ``size``             | ``VectorInt32``      | The size of the image that should be rendered.         |
+----------------------+----------------------+--------------------------------------------------------+
| ``filled``           | ``System.Boolean``   | Draw the contours filled (true) or outlined (false).   |
+----------------------+----------------------+--------------------------------------------------------+
| ``foreground``       | ``RgbByte``          | The foreground color.                                  |
+----------------------+----------------------+--------------------------------------------------------+
| ``background``       | ``RgbByte``          | The background color.                                  |
+----------------------+----------------------+--------------------------------------------------------+

The part of the drawing to be rendered is given by a transformation matrix and a size.

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
