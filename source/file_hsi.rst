Class *Hsi*
-----------

The hsi color type.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Hsi** contains the following variant parameters:

+-----------------+-----------------------------------------+
| Variant         | Description                             |
+=================+=========================================+
| ``Component``   | TODO no brief description for variant   |
+-----------------+-----------------------------------------+

The class **Hsi** contains the following properties:

+------------------+-------+-------+----------------------------------------------+
| Property         | Get   | Set   | Description                                  |
+==================+=======+=======+==============================================+
| ``Hue``          | \*    |       | The hue\_ color component property.          |
+------------------+-------+-------+----------------------------------------------+
| ``Saturation``   | \*    |       | The saturation\_ color component property.   |
+------------------+-------+-------+----------------------------------------------+
| ``Intensity``    | \*    |       | The intensity\_ color component property.    |
+------------------+-------+-------+----------------------------------------------+

The class **Hsi** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

The hsi color type template allows to be instantiated for the basic integer and floating point types.

Physically, the color primitives are in intensity - saturation - hue order, to match the most obvious bitmap organization. Logically, however, the color primitives are in hue - saturation - intensity order. The logical order is reflected in the parameter order in methods.

This class does not provide an implicit conversion operator to the hsia type, but the other way round: the hsia type provides a conversion from the hsi type, so that buffers of this type can be conveniently displayed.

Variants
~~~~~~~~

Variant *Component*
^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Component** has the following types:

+--------------+
| Type         |
+==============+
| ``Int8``     |
+--------------+
| ``Byte``     |
+--------------+
| ``Int16``    |
+--------------+
| ``UInt16``   |
+--------------+
| ``Int32``    |
+--------------+
| ``UInt32``   |
+--------------+
| ``Int64``    |
+--------------+
| ``UInt64``   |
+--------------+
| ``Single``   |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Hsi*
^^^^^^^^^^^^^^^^^

``Hsi()``

Default constructor.

This constructor sets the three components to 0. This corresponds to a color that is pure black.

Constructors
~~~~~~~~~~~~

Constructor *Hsi*
^^^^^^^^^^^^^^^^^

``Hsi(System.Object value)``

Standard constructor.

The constructor has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``value``   | ``System.Object``   |               |
+-------------+---------------------+---------------+

This constructor initializes the three components with a gray value.

The constructor also serves as a conversion operator from the scalar type T to type hsi<T>.

Constructor *Hsi*
^^^^^^^^^^^^^^^^^

``Hsi(System.Object hue, System.Object saturation, System.Object intensity)``

Standard constructor.

The constructor has the following parameters:

+------------------+---------------------+-----------------------------+
| Parameter        | Type                | Description                 |
+==================+=====================+=============================+
| ``hue``          | ``System.Object``   | The hue component.          |
+------------------+---------------------+-----------------------------+
| ``saturation``   | ``System.Object``   | The saturation component.   |
+------------------+---------------------+-----------------------------+
| ``intensity``    | ``System.Object``   | The intensity component.    |
+------------------+---------------------+-----------------------------+

This constructor initializes the three components with different values.

Properties
~~~~~~~~~~

Property *Hue*
^^^^^^^^^^^^^^

``System.Object Hue``

The hue\_ color component property.

Property *Saturation*
^^^^^^^^^^^^^^^^^^^^^

``System.Object Saturation``

The saturation\_ color component property.

Property *Intensity*
^^^^^^^^^^^^^^^^^^^^

``System.Object Intensity``

The intensity\_ color component property.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
