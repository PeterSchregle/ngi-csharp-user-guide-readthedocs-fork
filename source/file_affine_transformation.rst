Class *AffineTransformation*
----------------------------

Affine transformation of geometric primitives.

**Namespace:** Ngi

**Module:** ImageProcessing

Description
~~~~~~~~~~~

This class provides static methods to transform geometric primitives via an affine transformation.

The class can transform these geometric primitives: point, vector, direction, line\_segment, ray, line, box, rectangle, circle, ellipse, arc elliptical\_arc, triangle, quadrilateral, polyline, quadratic\_bezier and geometry.

Some primitives retain their nature, such as a point, which is transformed into another point by an affine transformation.

Some primitives change their nature, such as a circle, which is transformed into an ellipse by an affine transformation.

Some primitives can have an integer coordinate type on input, but the resulting types are always of floating point coordinate type. This is because the transformation cannot retain the integer nature in general. To illustrate, a point<int> type is transformed and the result is a point<double> type.

Static Methods
~~~~~~~~~~~~~~

Method *Transform*
^^^^^^^^^^^^^^^^^^

``VectorDouble Transform(VectorDouble vector, AffineMatrix matrix)``

Transform a vector.

The method **Transform** has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``vector``   | ``VectorDouble``   |               |
+--------------+--------------------+---------------+
| ``matrix``   | ``AffineMatrix``   |               |
+--------------+--------------------+---------------+

A vector transformed with an affine transformation results in another vector. The affine transformation is carried out with a matrix multiplication. Returns the transformed vector.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``PointDouble Transform(PointDouble point, AffineMatrix matrix)``

Transform a point.

The method **Transform** has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``point``    | ``PointDouble``    |               |
+--------------+--------------------+---------------+
| ``matrix``   | ``AffineMatrix``   |               |
+--------------+--------------------+---------------+

A point transformed with an affine transformation results in another point. The affine transformation is carried out with a matrix multiplication. Returns the transformed point.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Direction Transform(Direction direction, AffineMatrix matrix)``

Transform a direction.

The method **Transform** has the following parameters:

+-----------------+--------------------+---------------+
| Parameter       | Type               | Description   |
+=================+====================+===============+
| ``direction``   | ``Direction``      |               |
+-----------------+--------------------+---------------+
| ``matrix``      | ``AffineMatrix``   |               |
+-----------------+--------------------+---------------+

A direction transformed with an affine transformation results in another direction.

The affine transformation is carried out with a matrix multiplication.

Returns the transformed direction.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``LineSegmentDouble Transform(LineSegmentDouble lineSegment, AffineMatrix matrix)``

Transform a line segment.

The method **Transform** has the following parameters:

+-------------------+-------------------------+---------------+
| Parameter         | Type                    | Description   |
+===================+=========================+===============+
| ``lineSegment``   | ``LineSegmentDouble``   |               |
+-------------------+-------------------------+---------------+
| ``matrix``        | ``AffineMatrix``        |               |
+-------------------+-------------------------+---------------+

A line segment transformed with an affine transformation results in another line segment.

The affine transformation is carried out with a matrix multiplication.

Returns the transformed line segment.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``RayDouble Transform(RayDouble ray, AffineMatrix matrix)``

Transform a ray.

The method **Transform** has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``ray``      | ``RayDouble``      |               |
+--------------+--------------------+---------------+
| ``matrix``   | ``AffineMatrix``   |               |
+--------------+--------------------+---------------+

A ray transformed with an affine transformation results in another ray.

The affine transformation is carried out with a matrix multiplication.

Returns the transformed ray.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``LineDouble Transform(LineDouble line, AffineMatrix matrix)``

Transform a line.

The method **Transform** has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``line``     | ``LineDouble``     |               |
+--------------+--------------------+---------------+
| ``matrix``   | ``AffineMatrix``   |               |
+--------------+--------------------+---------------+

A line transformed with an affine transformation results in another line.

The affine transformation is carried out with a matrix multiplication.

Returns the transformed line.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``QuadrilateralDouble Transform(BoxDouble box, AffineMatrix matrix)``

Transform a box.

The method **Transform** has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``box``      | ``BoxDouble``      |               |
+--------------+--------------------+---------------+
| ``matrix``   | ``AffineMatrix``   |               |
+--------------+--------------------+---------------+

A box transformed with an affine transformation results in a quadrilateral. The affine transformation is carried out with a matrix multiplication. Returns the transformed box in the form of a quadrilateral.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``QuadrilateralDouble Transform(Rectangle rectangle, AffineMatrix matrix)``

Transform a rectangle.

The method **Transform** has the following parameters:

+-----------------+--------------------+---------------+
| Parameter       | Type               | Description   |
+=================+====================+===============+
| ``rectangle``   | ``Rectangle``      |               |
+-----------------+--------------------+---------------+
| ``matrix``      | ``AffineMatrix``   |               |
+-----------------+--------------------+---------------+

A rectangle transformed with an affine transformation results in a quadrilateral. The affine transformation is carried out with a matrix multiplication. Returns the transformed rectangle in the form of a quadrilateral.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Ellipse Transform(Circle circle, AffineMatrix matrix)``

Transform a circle.

The method **Transform** has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``circle``   | ``Circle``         |               |
+--------------+--------------------+---------------+
| ``matrix``   | ``AffineMatrix``   |               |
+--------------+--------------------+---------------+

A circle transformed with an affine transformation results in an ellipse. The affine transformation is carried out with a matrix multiplication. Returns the transformed circle in the form of an ellipse.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Ellipse Transform(Ellipse ellipse, AffineMatrix matrix)``

Transform an ellipse.

The method **Transform** has the following parameters:

+---------------+--------------------+---------------+
| Parameter     | Type               | Description   |
+===============+====================+===============+
| ``ellipse``   | ``Ellipse``        |               |
+---------------+--------------------+---------------+
| ``matrix``    | ``AffineMatrix``   |               |
+---------------+--------------------+---------------+

An ellipse transformed with an affine transformation results in another ellipse.

The affine transformation is carried out with a matrix multiplication.

Returns the transformed ellipse.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``EllipticalArc Transform(Arc arc, AffineMatrix matrix)``

Transform an arc.

The method **Transform** has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``arc``      | ``Arc``            |               |
+--------------+--------------------+---------------+
| ``matrix``   | ``AffineMatrix``   |               |
+--------------+--------------------+---------------+

An arc transformed with an affine transformation results in an elliptical arc.

The affine transformation is carried out with a matrix multiplication.

Returns the transformed elliptical arc.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``EllipticalArc Transform(EllipticalArc ellipticalArc, AffineMatrix matrix)``

Transform an elliptical arc.

The method **Transform** has the following parameters:

+---------------------+---------------------+---------------+
| Parameter           | Type                | Description   |
+=====================+=====================+===============+
| ``ellipticalArc``   | ``EllipticalArc``   |               |
+---------------------+---------------------+---------------+
| ``matrix``          | ``AffineMatrix``    |               |
+---------------------+---------------------+---------------+

An elliptical arc transformed with an affine transformation results in another elliptical arc

The affine transformation is carried out with a matrix multiplication.

Returns the transformed elliptical arc.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``TriangleDouble Transform(TriangleDouble triangle, AffineMatrix matrix)``

Transform a triangle.

The method **Transform** has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``triangle``   | ``TriangleDouble``   |               |
+----------------+----------------------+---------------+
| ``matrix``     | ``AffineMatrix``     |               |
+----------------+----------------------+---------------+

A triangle transformed with an affine transformation results in another triangle.

The affine transformation is carried out with a matrix multiplication.

Returns the transformed triangle.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``QuadrilateralDouble Transform(QuadrilateralDouble quadrilateral, AffineMatrix matrix)``

Transform a quadrilateral.

The method **Transform** has the following parameters:

+---------------------+---------------------------+---------------+
| Parameter           | Type                      | Description   |
+=====================+===========================+===============+
| ``quadrilateral``   | ``QuadrilateralDouble``   |               |
+---------------------+---------------------------+---------------+
| ``matrix``          | ``AffineMatrix``          |               |
+---------------------+---------------------------+---------------+

A quadrilateral transformed with an affine transformation results in another quadrilateral.

The affine transformation is carried out with a matrix multiplication.

Returns the transformed quadrilateral.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``PolylineDouble Transform(PolylineDouble polyline, AffineMatrix matrix)``

Transform a polyline.

The method **Transform** has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``polyline``   | ``PolylineDouble``   |               |
+----------------+----------------------+---------------+
| ``matrix``     | ``AffineMatrix``     |               |
+----------------+----------------------+---------------+

A polyline transformed with an affine transformation results in another polyline.

The affine transformation is carried out with a matrix multiplication.

Returns the transformed polyline.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``QuadraticBezier Transform(QuadraticBezier bezier, AffineMatrix matrix)``

Transform a quadratic bezier curve.

The method **Transform** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``bezier``   | ``QuadraticBezier``   |               |
+--------------+-----------------------+---------------+
| ``matrix``   | ``AffineMatrix``      |               |
+--------------+-----------------------+---------------+

A quadratic bezier curve transformed with an affine transformation results in another quadratic bezier curve.

The affine transformation is carried out with a matrix multiplication.

Returns the transformed quadratic bezier curve.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``CubicBezier Transform(CubicBezier bezier, AffineMatrix matrix)``

Transform a cubic bezier curve.

The method **Transform** has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``bezier``   | ``CubicBezier``    |               |
+--------------+--------------------+---------------+
| ``matrix``   | ``AffineMatrix``   |               |
+--------------+--------------------+---------------+

A cubic bezier curve transformed with an affine transformation results in another cubic bezier curve.

The affine transformation is carried out with a matrix multiplication.

Returns the transformed cubic bezier curve.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Geometry Transform(Geometry geometry, AffineMatrix matrix)``

Transform a geometry.

The method **Transform** has the following parameters:

+----------------+--------------------+---------------+
| Parameter      | Type               | Description   |
+================+====================+===============+
| ``geometry``   | ``Geometry``       |               |
+----------------+--------------------+---------------+
| ``matrix``     | ``AffineMatrix``   |               |
+----------------+--------------------+---------------+

A geometry transformed with an affine transformation results in another geometry.

The affine transformation is carried out by affine transformation of each geometry element.

Returns the transformed geometry.
