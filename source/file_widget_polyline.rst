Class *WidgetPolyline*
----------------------

A widget\_polyline displays a curve on a surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetPolyline** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetPolyline** contains the following variant parameters:

+------------+-----------------------------------------+
| Variant    | Description                             |
+============+=========================================+
| ``View``   | TODO no brief description for variant   |
+------------+-----------------------------------------+

The class **WidgetPolyline** contains the following properties:

+-----------------------------+-------+-------+--------------------------------------------------------------------------+
| Property                    | Get   | Set   | Description                                                              |
+=============================+=======+=======+==========================================================================+
| ``Position``                | \*    | \*    | The position of the widget.                                              |
+-----------------------------+-------+-------+--------------------------------------------------------------------------+
| ``Outlines``                | \*    | \*    | The outline pen.                                                         |
+-----------------------------+-------+-------+--------------------------------------------------------------------------+
| ``Fills``                   | \*    | \*    | The fill brush.                                                          |
+-----------------------------+-------+-------+--------------------------------------------------------------------------+
| ``Direction``               | \*    | \*    | The drawing direction of the widget.                                     |
+-----------------------------+-------+-------+--------------------------------------------------------------------------+
| ``Minimum``                 | \*    | \*    | The minimum.                                                             |
+-----------------------------+-------+-------+--------------------------------------------------------------------------+
| ``AutoscaleMinimum``        | \*    | \*    | Determine the minimum automatically.                                     |
+-----------------------------+-------+-------+--------------------------------------------------------------------------+
| ``AutoscaleMinimumValue``   | \*    |       | Find the minimum for scaling by inspecting the view data.                |
+-----------------------------+-------+-------+--------------------------------------------------------------------------+
| ``ScalingMinimum``          | \*    |       | Returns the minimum used for scaling.                                    |
+-----------------------------+-------+-------+--------------------------------------------------------------------------+
| ``Maximum``                 | \*    | \*    | The maximum.                                                             |
+-----------------------------+-------+-------+--------------------------------------------------------------------------+
| ``AutoscaleMaximum``        | \*    | \*    | Determine the maximum automatically.                                     |
+-----------------------------+-------+-------+--------------------------------------------------------------------------+
| ``AutoscaleMaximumValue``   | \*    |       | Find the maximum for scaling by inspecting the view data.                |
+-----------------------------+-------+-------+--------------------------------------------------------------------------+
| ``ScalingMaximum``          | \*    |       | Returns the maximum used for scaling.                                    |
+-----------------------------+-------+-------+--------------------------------------------------------------------------+
| ``Reverse``                 | \*    | \*    | Reverse drawing order.                                                   |
+-----------------------------+-------+-------+--------------------------------------------------------------------------+
| ``Logscale``                | \*    | \*    | Linear or logarithmic scaling.                                           |
+-----------------------------+-------+-------+--------------------------------------------------------------------------+
| ``Baseline``                | \*    | \*    | The base line.                                                           |
+-----------------------------+-------+-------+--------------------------------------------------------------------------+
| ``ClosedOutline``           | \*    | \*    | This flag determines whether the outline will be drawn closed or open.   |
+-----------------------------+-------+-------+--------------------------------------------------------------------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_polyline. These settings affect the look and the behavior of the widget\_polyline.

The widget\_polyline supports automatic layout via the widget\_layoutable base class. The size of the widget is determined by the size of the view which is displayed.

The widget\_polyline supports interactivity via the widget\_interactive base class, i.e. it can be selected, move around. In general it can react on mouse and keyboard interactions. This widget displays a view.

Variants
~~~~~~~~

Variant *View*
^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **View** has the following types:

+-----------------------------+
| Type                        |
+=============================+
| ``ViewLocatorByte``         |
+-----------------------------+
| ``ViewLocatorUInt16``       |
+-----------------------------+
| ``ViewLocatorUInt32``       |
+-----------------------------+
| ``ViewLocatorDouble``       |
+-----------------------------+
| ``ViewLocatorRgbByte``      |
+-----------------------------+
| ``ViewLocatorRgbUInt16``    |
+-----------------------------+
| ``ViewLocatorRgbUInt32``    |
+-----------------------------+
| ``ViewLocatorRgbDouble``    |
+-----------------------------+
| ``ViewLocatorRgbaByte``     |
+-----------------------------+
| ``ViewLocatorRgbaUInt16``   |
+-----------------------------+
| ``ViewLocatorRgbaUInt32``   |
+-----------------------------+
| ``ViewLocatorRgbaDouble``   |
+-----------------------------+
| ``ViewLocatorHlsByte``      |
+-----------------------------+
| ``ViewLocatorHlsUInt16``    |
+-----------------------------+
| ``ViewLocatorHlsDouble``    |
+-----------------------------+
| ``ViewLocatorHsiByte``      |
+-----------------------------+
| ``ViewLocatorHsiUInt16``    |
+-----------------------------+
| ``ViewLocatorHsiDouble``    |
+-----------------------------+
| ``ViewLocatorLabByte``      |
+-----------------------------+
| ``ViewLocatorLabUInt16``    |
+-----------------------------+
| ``ViewLocatorLabDouble``    |
+-----------------------------+
| ``ViewLocatorXyzByte``      |
+-----------------------------+
| ``ViewLocatorXyzUInt16``    |
+-----------------------------+
| ``ViewLocatorXyzDouble``    |
+-----------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetPolyline*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetPolyline()``

Default constructor.

The default constructor creates a widget with default settings. By default the pen is one-pixel wide black and the brush is black half transparent.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``BoxDouble Position``

The position of the widget.

Property *Outlines*
^^^^^^^^^^^^^^^^^^^

``PenListPenByte Outlines``

The outline pen.

Property *Fills*
^^^^^^^^^^^^^^^^

``BrushListSolidColorBrushByte Fills``

The fill brush.

Property *Direction*
^^^^^^^^^^^^^^^^^^^^

``System.Int32 Direction``

The drawing direction of the widget.

Property *Minimum*
^^^^^^^^^^^^^^^^^^

``System.Double Minimum``

The minimum.

Property *AutoscaleMinimum*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean AutoscaleMinimum``

Determine the minimum automatically.

Property *AutoscaleMinimumValue*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double AutoscaleMinimumValue``

Find the minimum for scaling by inspecting the view data.

This returns the minimum scalar value used for scaling. The same function is used when the autoscale property is set to true. However, when autoscale is used, the minimum and maximum properties remain unchanged. This function exists so that you can implement on-demand autoscaling. In such a scenario, you would call the get\_scaling\_minimum function to set the minimum property.

Property *ScalingMinimum*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double ScalingMinimum``

Returns the minimum used for scaling.

If autoscale == true, the function uses get\_autoscale\_minimum() to find the minimum and returns this value. If autoscale == false, the function returns the value of the minimum property.

Property *Maximum*
^^^^^^^^^^^^^^^^^^

``System.Double Maximum``

The maximum.

Property *AutoscaleMaximum*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean AutoscaleMaximum``

Determine the maximum automatically.

Property *AutoscaleMaximumValue*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double AutoscaleMaximumValue``

Find the maximum for scaling by inspecting the view data.

This returns the maximum scalar value used for scaling. The same function is used when the autoscale property is set to true. However, when autoscale is used, the minimum and maximum properties remain unchanged. This function exists so that you can implement on-demand autoscaling. In such a scenario, you would call the get\_scaling\_maximum function to set the maximum property.

Property *ScalingMaximum*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double ScalingMaximum``

Returns the maximum used for scaling.

If autoscale == true, the function uses get\_autoscale\_maximum() to find the maximum and returns this value. If autoscale == false, the function returns the value of the maxnimum property.

Property *Reverse*
^^^^^^^^^^^^^^^^^^

``System.Boolean Reverse``

Reverse drawing order.

Property *Logscale*
^^^^^^^^^^^^^^^^^^^

``System.Boolean Logscale``

Linear or logarithmic scaling.

Property *Baseline*
^^^^^^^^^^^^^^^^^^^

``System.Double Baseline``

The base line.

Property *ClosedOutline*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean ClosedOutline``

This flag determines whether the outline will be drawn closed or open.
