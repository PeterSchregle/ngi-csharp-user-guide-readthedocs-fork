Class *XcoderAlgorithms*
------------------------

Encoder/decoder functions.

**Namespace:** Ngi

**Module:** ImageProcessing

Description
~~~~~~~~~~~

The class contains functions for Base64 encoding and decoding.

Static Methods
~~~~~~~~~~~~~~

Method *Base64Encode*
^^^^^^^^^^^^^^^^^^^^^

``DataList Base64Encode(DataList data)``

Encodes a block of data with the Base64 encoding scheme.

The method **Base64Encode** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``data``    | ``DataList``   |               |
+-------------+----------------+---------------+

/returns The Base64 encoded block of data.

Method *Base64Decode*
^^^^^^^^^^^^^^^^^^^^^

``DataList Base64Decode(DataList data)``

Decodes a block of data with the Base64 encoding scheme.

The method **Base64Decode** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``data``    | ``DataList``   |               |
+-------------+----------------+---------------+

/returns The Base64 decoded block of data.
