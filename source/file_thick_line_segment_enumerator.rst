Class *ThickLineSegmentEnumerator*
----------------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **ThickLineSegmentEnumerator** implements the following interfaces:

+--------------------------------------------------------------------+
| Interface                                                          |
+====================================================================+
| ``IEnumerator``                                                    |
+--------------------------------------------------------------------+
| ``IEnumeratorThickLineSegmentEnumeratorThickLineSegmentVariant``   |
+--------------------------------------------------------------------+

The class **ThickLineSegmentEnumerator** contains the following variant parameters:

+------------------------+-----------------------------------------+
| Variant                | Description                             |
+========================+=========================================+
| ``ThickLineSegment``   | TODO no brief description for variant   |
+------------------------+-----------------------------------------+

The class **ThickLineSegmentEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **ThickLineSegmentEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *ThickLineSegment*
^^^^^^^^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **ThickLineSegment** has the following types:

+------------------------------+
| Type                         |
+==============================+
| ``ThickLineSegmentInt32``    |
+------------------------------+
| ``ThickLineSegmentDouble``   |
+------------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``ThickLineSegment Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
