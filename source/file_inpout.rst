Class *Inpout*
--------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Inpout** contains the following methods:

+-----------------+--------------------------------+
| Method          | Description                    |
+=================+================================+
| ``ReadPort``    | Read from the given IO port.   |
+-----------------+--------------------------------+
| ``WritePort``   | Write to the given IO port.    |
+-----------------+--------------------------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Inpout*
^^^^^^^^^^^^^^^^^^^^

``Inpout()``

Constructors
~~~~~~~~~~~~

Constructor *Inpout*
^^^^^^^^^^^^^^^^^^^^

``Inpout(System.String moduleFileName)``

Construct the inpout object.

The constructor has the following parameters:

+----------------------+---------------------+---------------+
| Parameter            | Type                | Description   |
+======================+=====================+===============+
| ``moduleFileName``   | ``System.String``   |               |
+----------------------+---------------------+---------------+

This loads the InpOut dynamic library and finds the addresses of the read (Inp32) and write (Out32) functions. Despite the names of the functions, they also exist in 64 bit forms.

Static Methods
~~~~~~~~~~~~~~

Method *GetdefaultModuleFileName*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.String GetdefaultModuleFileName()``

Returns the default module file name of the Inpout module.

The default module name is "InpOutx64.dll" when compiled for 64bit, and "InpOut32.dll" otherwise. It does not include a path.

Methods
~~~~~~~

Method *ReadPort*
^^^^^^^^^^^^^^^^^

``System.Int16 ReadPort(System.Int16 address)``

Read from the given IO port.

The method **ReadPort** has the following parameters:

+---------------+--------------------+-------------------------------+
| Parameter     | Type               | Description                   |
+===============+====================+===============================+
| ``address``   | ``System.Int16``   | The address of the IO port.   |
+---------------+--------------------+-------------------------------+

/return The data read from the IO port.

Method *WritePort*
^^^^^^^^^^^^^^^^^^

``void WritePort(System.Int16 address, System.Int16 data)``

Write to the given IO port.

The method **WritePort** has the following parameters:

+---------------+--------------------+-------------------------------------+
| Parameter     | Type               | Description                         |
+===============+====================+=====================================+
| ``address``   | ``System.Int16``   | The address of the IO port.         |
+---------------+--------------------+-------------------------------------+
| ``data``      | ``System.Int16``   | The data to write to the IO port.   |
+---------------+--------------------+-------------------------------------+
