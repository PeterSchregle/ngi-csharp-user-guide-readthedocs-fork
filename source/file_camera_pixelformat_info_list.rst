Class *CameraPixelformatInfoList*
---------------------------------

**Namespace:** Ngi

**Module:** Camera

The class **CameraPixelformatInfoList** implements the following interfaces:

+----------------------------------+
| Interface                        |
+==================================+
| ``IListCameraPixelformatInfo``   |
+----------------------------------+

The class **CameraPixelformatInfoList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **CameraPixelformatInfoList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *CameraPixelformatInfoList*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``CameraPixelformatInfoList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``CameraPixelformatInfo [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``CameraPixelformatInfoEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(CameraPixelformatInfo item)``

The method **Add** has the following parameters:

+-------------+-----------------------------+---------------+
| Parameter   | Type                        | Description   |
+=============+=============================+===============+
| ``item``    | ``CameraPixelformatInfo``   |               |
+-------------+-----------------------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(CameraPixelformatInfo item)``

The method **Contains** has the following parameters:

+-------------+-----------------------------+---------------+
| Parameter   | Type                        | Description   |
+=============+=============================+===============+
| ``item``    | ``CameraPixelformatInfo``   |               |
+-------------+-----------------------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(CameraPixelformatInfo item)``

The method **Remove** has the following parameters:

+-------------+-----------------------------+---------------+
| Parameter   | Type                        | Description   |
+=============+=============================+===============+
| ``item``    | ``CameraPixelformatInfo``   |               |
+-------------+-----------------------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(CameraPixelformatInfo item)``

The method **IndexOf** has the following parameters:

+-------------+-----------------------------+---------------+
| Parameter   | Type                        | Description   |
+=============+=============================+===============+
| ``item``    | ``CameraPixelformatInfo``   |               |
+-------------+-----------------------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, CameraPixelformatInfo item)``

The method **Insert** has the following parameters:

+-------------+-----------------------------+---------------+
| Parameter   | Type                        | Description   |
+=============+=============================+===============+
| ``index``   | ``System.Int32``            |               |
+-------------+-----------------------------+---------------+
| ``item``    | ``CameraPixelformatInfo``   |               |
+-------------+-----------------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
