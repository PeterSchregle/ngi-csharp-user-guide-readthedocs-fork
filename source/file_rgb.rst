Class *Rgb*
-----------

The RGB color type.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Rgb** contains the following variant parameters:

+-----------------+-----------------------------------------+
| Variant         | Description                             |
+=================+=========================================+
| ``Component``   | TODO no brief description for variant   |
+-----------------+-----------------------------------------+

The class **Rgb** contains the following properties:

+---------------+-------+-------+-----------------------------------------+
| Property      | Get   | Set   | Description                             |
+===============+=======+=======+=========================================+
| ``Red``       | \*    | \*    | The red\_ color component property.     |
+---------------+-------+-------+-----------------------------------------+
| ``Green``     | \*    | \*    | The green\_ color component property.   |
+---------------+-------+-------+-----------------------------------------+
| ``Blue``      | \*    | \*    | The blue\_ color component property.    |
+---------------+-------+-------+-----------------------------------------+
| ``Minimum``   | \*    |       | The minimum color component.            |
+---------------+-------+-------+-----------------------------------------+
| ``Maximum``   | \*    |       | The maximum color component.            |
+---------------+-------+-------+-----------------------------------------+
| ``Gray``      | \*    |       | The color converted to gray.            |
+---------------+-------+-------+-----------------------------------------+

The class **Rgb** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

The RGB color type template allows to be instantiated for the basic integer and floating point types.

Physically, the color primitives are in blue - green - red order, to match the most obvious bitmap organization. Logically, however, the color primitives are in red - green - blue order. The logical order is reflected in the parameter order in methods.

This class does not provide an implicit conversion operator to the rgba type, but the other way round: the rgba type provides a conversion from the rgb type, so that buffers of this type can be conveniently displayed.

Variants
~~~~~~~~

Variant *Component*
^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Component** has the following types:

+--------------+
| Type         |
+==============+
| ``Int8``     |
+--------------+
| ``Byte``     |
+--------------+
| ``Int16``    |
+--------------+
| ``UInt16``   |
+--------------+
| ``Int32``    |
+--------------+
| ``UInt32``   |
+--------------+
| ``Int64``    |
+--------------+
| ``UInt64``   |
+--------------+
| ``Single``   |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Rgb*
^^^^^^^^^^^^^^^^^

``Rgb()``

Default constructor.

This constructor sets the three components to 0. This corresponds to a color that is pure black.

Constructors
~~~~~~~~~~~~

Constructor *Rgb*
^^^^^^^^^^^^^^^^^

``Rgb(System.Object gray)``

Standard constructor.

The constructor has the following parameters:

+-------------+---------------------+-----------------------+
| Parameter   | Type                | Description           |
+=============+=====================+=======================+
| ``gray``    | ``System.Object``   | The gray component.   |
+-------------+---------------------+-----------------------+

This constructor initializes the three components with a gray value.

The constructor also serves as a conversion operator from the scalar type T to type rgb<T>.

Constructor *Rgb*
^^^^^^^^^^^^^^^^^

``Rgb(System.Object red, System.Object green, System.Object blue)``

Standard constructor.

The constructor has the following parameters:

+-------------+---------------------+------------------------+
| Parameter   | Type                | Description            |
+=============+=====================+========================+
| ``red``     | ``System.Object``   | The red component.     |
+-------------+---------------------+------------------------+
| ``green``   | ``System.Object``   | The green component.   |
+-------------+---------------------+------------------------+
| ``blue``    | ``System.Object``   | The blue component.    |
+-------------+---------------------+------------------------+

This constructor initializes the three components with different values.

Properties
~~~~~~~~~~

Property *Red*
^^^^^^^^^^^^^^

``System.Object Red``

The red\_ color component property.

Property *Green*
^^^^^^^^^^^^^^^^

``System.Object Green``

The green\_ color component property.

Property *Blue*
^^^^^^^^^^^^^^^

``System.Object Blue``

The blue\_ color component property.

Property *Minimum*
^^^^^^^^^^^^^^^^^^

``System.Object Minimum``

The minimum color component.

Property *Maximum*
^^^^^^^^^^^^^^^^^^

``System.Object Maximum``

The maximum color component.

Property *Gray*
^^^^^^^^^^^^^^^

``System.Object Gray``

The color converted to gray.

Static Methods
~~~~~~~~~~~~~~

Method *FromString*
^^^^^^^^^^^^^^^^^^^

``Rgb FromString(System.String value)``

Parse a value from a string.

The method **FromString** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``value``   | ``System.String``   |               |
+-------------+---------------------+---------------+

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
