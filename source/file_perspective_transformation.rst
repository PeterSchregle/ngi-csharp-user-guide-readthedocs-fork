Class *PerspectiveTransformation*
---------------------------------

Perspective transformation of geometric primitives.

**Namespace:** Ngi

**Module:** ImageProcessing

Description
~~~~~~~~~~~

This class provides static methods to transform geometric primitives via a perspective transformation.

The class can transform these geometric primitives: point, vector, direction, line\_segment, ray, line, box, rectangle, circle, ellipse, arc elliptical\_arc, triangle, quadrilateral, polyline, quadratic\_bezier and geometry.

Some primitives retain their nature, such as a point, which is transformed into another point by a perspective transformation.

Some primitives change their nature, such as a circle, which is transformed into an ellipse by an perspective transformation.

Some primitives can have an integer coordinate type on input, but the resulting types are always of floating point coordinate type. This is because the transformation cannot retain the integer nature in general. To illustrate, a point<int> type is transformed and the result is a point<double> type.

Static Methods
~~~~~~~~~~~~~~

Method *Transform*
^^^^^^^^^^^^^^^^^^

``PointDouble Transform(PointDouble point, PerspectiveMatrix matrix)``

Transform a point.

The method **Transform** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``point``    | ``PointDouble``         |               |
+--------------+-------------------------+---------------+
| ``matrix``   | ``PerspectiveMatrix``   |               |
+--------------+-------------------------+---------------+

A point transformed with an perspective transformation results in another point.

The perspective transformation is carried out with a matrix multiplication.

Returns the transformed point.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``LineSegmentDouble Transform(LineSegmentDouble lineSegment, PerspectiveMatrix matrix)``

Transform a line segment.

The method **Transform** has the following parameters:

+-------------------+-------------------------+---------------+
| Parameter         | Type                    | Description   |
+===================+=========================+===============+
| ``lineSegment``   | ``LineSegmentDouble``   |               |
+-------------------+-------------------------+---------------+
| ``matrix``        | ``PerspectiveMatrix``   |               |
+-------------------+-------------------------+---------------+

A line segment transformed with an perspective transformation results in another line segment.

The perspective transformation is carried out with a matrix multiplication.

Returns the transformed line segment.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``QuadrilateralDouble Transform(BoxDouble box, PerspectiveMatrix matrix)``

Transform a box.

The method **Transform** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``box``      | ``BoxDouble``           |               |
+--------------+-------------------------+---------------+
| ``matrix``   | ``PerspectiveMatrix``   |               |
+--------------+-------------------------+---------------+

A box transformed with an perspective transformation results in a quadrilateral.

The perspective transformation is carried out with a matrix multiplication.

Returns the transformed box in the form of a quadrilateral.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``TriangleDouble Transform(TriangleDouble triangle, PerspectiveMatrix matrix)``

Transform a triangle.

The method **Transform** has the following parameters:

+----------------+-------------------------+---------------+
| Parameter      | Type                    | Description   |
+================+=========================+===============+
| ``triangle``   | ``TriangleDouble``      |               |
+----------------+-------------------------+---------------+
| ``matrix``     | ``PerspectiveMatrix``   |               |
+----------------+-------------------------+---------------+

A triangle transformed with an perspective transformation results in another triangle.

The perspective transformation is carried out with a matrix multiplication.

Returns the transformed triangle.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``QuadrilateralDouble Transform(QuadrilateralDouble quadrilateral, PerspectiveMatrix matrix)``

Transform a quadrilateral.

The method **Transform** has the following parameters:

+---------------------+---------------------------+---------------+
| Parameter           | Type                      | Description   |
+=====================+===========================+===============+
| ``quadrilateral``   | ``QuadrilateralDouble``   |               |
+---------------------+---------------------------+---------------+
| ``matrix``          | ``PerspectiveMatrix``     |               |
+---------------------+---------------------------+---------------+

A quadrilateral transformed with an perspective transformation results in another quadrilateral.

The perspective transformation is carried out with a matrix multiplication.

Returns the transformed quadrilateral.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``PolylineDouble Transform(PolylineDouble polyline, PerspectiveMatrix matrix)``

Transform a polyline.

The method **Transform** has the following parameters:

+----------------+-------------------------+---------------+
| Parameter      | Type                    | Description   |
+================+=========================+===============+
| ``polyline``   | ``PolylineDouble``      |               |
+----------------+-------------------------+---------------+
| ``matrix``     | ``PerspectiveMatrix``   |               |
+----------------+-------------------------+---------------+

A polyline transformed with an perspective transformation results in another polyline.

The perspective transformation is carried out with a matrix multiplication.

Returns the transformed polyline.
