Class *WidgetCircle*
--------------------

A widget\_circle displays a circle on a surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetCircle** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetCircle** contains the following properties:

+----------------+-------+-------+---------------------------------------+
| Property       | Get   | Set   | Description                           |
+================+=======+=======+=======================================+
| ``Position``   | \*    | \*    | The position of the widget\_circle.   |
+----------------+-------+-------+---------------------------------------+
| ``Outline``    | \*    | \*    | The outline of the widget\_circle.    |
+----------------+-------+-------+---------------------------------------+
| ``Fill``       | \*    | \*    | The fill of the widget\_circle.       |
+----------------+-------+-------+---------------------------------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_circle. These settings affect the look and the behavior of the widget\_circle.

The widget\_circle supports interactivity via the widget\_interactive base class, i.e. it can be selected, move around. In general it can react on mouse and keyboard interactions. This widget supports layout.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetCircle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetCircle()``

Default constructor.

The default constructor creates a widget with default settings. By default the circle is of zero radius at the origin, the outline is one-pixel wide black and the fill is black transparent.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``Circle Position``

The position of the widget\_circle.

The position of a widget\_circle is specified with a circle.

Property *Outline*
^^^^^^^^^^^^^^^^^^

``PenByte Outline``

The outline of the widget\_circle.

The outline of the widget\_circle is specified with a pen<>.

Property *Fill*
^^^^^^^^^^^^^^^

``SolidColorBrushByte Fill``

The fill of the widget\_circle.

The fill of the widget\_circle is specified with a brush.
