Class *Edge1dInfo*
------------------

The edge\_1d\_info as returned by gauging.

**Namespace:** Ngi

**Module:** AnalysisMeasuring

The class **Edge1dInfo** implements the following interfaces:

+----------------------------+
| Interface                  |
+============================+
| ``IEquatableEdge1dInfo``   |
+----------------------------+
| ``ISerializable``          |
+----------------------------+

The class **Edge1dInfo** contains the following properties:

+----------------+-------+-------+------------------------------------+
| Property       | Get   | Set   | Description                        |
+================+=======+=======+====================================+
| ``Edges``      | \*    |       | The edges.                         |
+----------------+-------+-------+------------------------------------+
| ``Profile``    | \*    |       | The smoothed grey value profile.   |
+----------------+-------+-------+------------------------------------+
| ``Gradient``   | \*    |       | The gradient profile.              |
+----------------+-------+-------+------------------------------------+

The class **Edge1dInfo** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

This class combines an edge\_1d\_list with a smoothed profile and a gradient profile. This enables you to look at the data that is used for edge detection and understand the effects of the sigma and threshold parameters.

The following operators are implemented for a edge\_1d\_info: operator == : comparison for equality. operator != : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Edge1dInfo*
^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo()``

Standard constructor.

Constructors
~~~~~~~~~~~~

Constructor *Edge1dInfo*
^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo(Edge1dList edges, ProfileDouble profile, ProfileDouble gradient)``

Standard constructor.

The constructor has the following parameters:

+----------------+---------------------+-----------------------------------+
| Parameter      | Type                | Description                       |
+================+=====================+===================================+
| ``edges``      | ``Edge1dList``      | The edges.                        |
+----------------+---------------------+-----------------------------------+
| ``profile``    | ``ProfileDouble``   | The smothed grey value profile.   |
+----------------+---------------------+-----------------------------------+
| ``gradient``   | ``ProfileDouble``   | The gradient profile.             |
+----------------+---------------------+-----------------------------------+

Properties
~~~~~~~~~~

Property *Edges*
^^^^^^^^^^^^^^^^

``Edge1dList Edges``

The edges.

Property *Profile*
^^^^^^^^^^^^^^^^^^

``ProfileDouble Profile``

The smoothed grey value profile.

Property *Gradient*
^^^^^^^^^^^^^^^^^^^

``ProfileDouble Gradient``

The gradient profile.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
