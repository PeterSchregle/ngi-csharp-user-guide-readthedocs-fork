Class *EllipticalArcEnumerator*
-------------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **EllipticalArcEnumerator** implements the following interfaces:

+--------------------------------+
| Interface                      |
+================================+
| ``IEnumerator``                |
+--------------------------------+
| ``IEnumeratorEllipticalArc``   |
+--------------------------------+

The class **EllipticalArcEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **EllipticalArcEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``EllipticalArc Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
