Class *MatrixcodeDecoder*
-------------------------

Configuration for the matrixcode decoder.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **MatrixcodeDecoder** implements the following interfaces:

+-----------------------------------+
| Interface                         |
+===================================+
| ``ISerializable``                 |
+-----------------------------------+
| ``IEquatableMatrixcodeDecoder``   |
+-----------------------------------+

The class **MatrixcodeDecoder** contains the following properties:

+------------------+-------+-------+-----------------------------------------------------------------+
| Property         | Get   | Set   | Description                                                     |
+==================+=======+=======+=================================================================+
| ``Aztec``        | \*    | \*    | The Aztec decoder.                                              |
+------------------+-------+-------+-----------------------------------------------------------------+
| ``DataMatrix``   | \*    | \*    | The data\_matrix decoder.                                       |
+------------------+-------+-------+-----------------------------------------------------------------+
| ``Pdf417``       | \*    | \*    | The pdf\_417 decoder.                                           |
+------------------+-------+-------+-----------------------------------------------------------------+
| ``QrCode``       | \*    | \*    | The qr\_code decoder.                                           |
+------------------+-------+-------+-----------------------------------------------------------------+
| ``Binarizer``    | \*    | \*    | The binarizer mode: global or local (default for 2D decoder).   |
+------------------+-------+-------+-----------------------------------------------------------------+
| ``TryHarder``    | \*    | \*    | Try harder to decode.                                           |
+------------------+-------+-------+-----------------------------------------------------------------+

The class **MatrixcodeDecoder** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

The class **MatrixcodeDecoder** contains the following enumerations:

+---------------------+------------------------------+
| Enumeration         | Description                  |
+=====================+==============================+
| ``BinarizerMode``   | TODO documentation missing   |
+---------------------+------------------------------+

Description
~~~~~~~~~~~

Various matrixcode symbologies can be configured as well as modes of decoding.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *MatrixcodeDecoder*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeDecoder()``

Default constructor.

By default, all supported symbologies are enabled. However, try\_harder is disabled.

Constructors
~~~~~~~~~~~~

Constructor *MatrixcodeDecoder*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeDecoder(System.Boolean aztec, System.Boolean dataMatrix, System.Boolean pdf417, System.Boolean qrCode, MatrixcodeDecoder.BinarizerMode binarizer, System.Boolean tryHarder)``

Constructor.

The constructor has the following parameters:

+------------------+---------------------------------------+---------------------------------------------------------------------------+
| Parameter        | Type                                  | Description                                                               |
+==================+=======================================+===========================================================================+
| ``aztec``        | ``System.Boolean``                    | Aztec code.                                                               |
+------------------+---------------------------------------+---------------------------------------------------------------------------+
| ``dataMatrix``   | ``System.Boolean``                    | Data Matrix.                                                              |
+------------------+---------------------------------------+---------------------------------------------------------------------------+
| ``pdf417``       | ``System.Boolean``                    | PDF 417.                                                                  |
+------------------+---------------------------------------+---------------------------------------------------------------------------+
| ``qrCode``       | ``System.Boolean``                    | QR Code.                                                                  |
+------------------+---------------------------------------+---------------------------------------------------------------------------+
| ``binarizer``    | ``MatrixcodeDecoder.BinarizerMode``   | The binarizer mode.                                                       |
+------------------+---------------------------------------+---------------------------------------------------------------------------+
| ``tryHarder``    | ``System.Boolean``                    | If set, tries harder to find the code and usually takes longer as well.   |
+------------------+---------------------------------------+---------------------------------------------------------------------------+

Every supported symbology can be switched on or off by the respective boolen parameter. In addition, you can make the decoder work harder (and take longer) by setting the try\_harder parameter.

Properties
~~~~~~~~~~

Property *Aztec*
^^^^^^^^^^^^^^^^

``System.Boolean Aztec``

The Aztec decoder.

Property *DataMatrix*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean DataMatrix``

The data\_matrix decoder.

Property *Pdf417*
^^^^^^^^^^^^^^^^^

``System.Boolean Pdf417``

The pdf\_417 decoder.

Property *QrCode*
^^^^^^^^^^^^^^^^^

``System.Boolean QrCode``

The qr\_code decoder.

Property *Binarizer*
^^^^^^^^^^^^^^^^^^^^

``MatrixcodeDecoder.BinarizerMode Binarizer``

The binarizer mode: global or local (default for 2D decoder).

Property *TryHarder*
^^^^^^^^^^^^^^^^^^^^

``System.Boolean TryHarder``

Try harder to decode.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Enumerations
~~~~~~~~~~~~

Enumeration *BinarizerMode*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum BinarizerMode``

TODO documentation missing

The enumeration **BinarizerMode** has the following constants:

+--------------+---------+---------------+
| Name         | Value   | Description   |
+==============+=========+===============+
| ``global``   | ``0``   |               |
+--------------+---------+---------------+
| ``local``    | ``1``   |               |
+--------------+---------+---------------+

::

    enum BinarizerMode
    {
      global = 0,
      local = 1,
    };

TODO documentation missing
