Class *OcrCharacterInfoList*
----------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **OcrCharacterInfoList** implements the following interfaces:

+-----------------------------+
| Interface                   |
+=============================+
| ``IListOcrCharacterInfo``   |
+-----------------------------+

The class **OcrCharacterInfoList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **OcrCharacterInfoList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *OcrCharacterInfoList*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``OcrCharacterInfoList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``OcrCharacterInfo [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``OcrCharacterInfoEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(OcrCharacterInfo item)``

The method **Add** has the following parameters:

+-------------+------------------------+---------------+
| Parameter   | Type                   | Description   |
+=============+========================+===============+
| ``item``    | ``OcrCharacterInfo``   |               |
+-------------+------------------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(OcrCharacterInfo item)``

The method **Contains** has the following parameters:

+-------------+------------------------+---------------+
| Parameter   | Type                   | Description   |
+=============+========================+===============+
| ``item``    | ``OcrCharacterInfo``   |               |
+-------------+------------------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(OcrCharacterInfo item)``

The method **Remove** has the following parameters:

+-------------+------------------------+---------------+
| Parameter   | Type                   | Description   |
+=============+========================+===============+
| ``item``    | ``OcrCharacterInfo``   |               |
+-------------+------------------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(OcrCharacterInfo item)``

The method **IndexOf** has the following parameters:

+-------------+------------------------+---------------+
| Parameter   | Type                   | Description   |
+=============+========================+===============+
| ``item``    | ``OcrCharacterInfo``   |               |
+-------------+------------------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, OcrCharacterInfo item)``

The method **Insert** has the following parameters:

+-------------+------------------------+---------------+
| Parameter   | Type                   | Description   |
+=============+========================+===============+
| ``index``   | ``System.Int32``       |               |
+-------------+------------------------+---------------+
| ``item``    | ``OcrCharacterInfo``   |               |
+-------------+------------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
