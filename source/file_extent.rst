Class *Extent*
--------------

A two-dimensional extent.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Extent** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``IEquatableExtent``         |
+------------------------------+
| ``ISerializable``            |
+------------------------------+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **Extent** contains the following properties:

+--------------+-------+-------+---------------+
| Property     | Get   | Set   | Description   |
+==============+=======+=======+===============+
| ``Width``    | \*    | \*    | The width.    |
+--------------+-------+-------+---------------+
| ``Height``   | \*    | \*    | The height.   |
+--------------+-------+-------+---------------+

The class **Extent** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

This is used to specify the size of a two-dimensional, rectangular area within a buffer.

Together with an index, this is used to specify a three-dimensional, rectangular subset of a buffer.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Extent*
^^^^^^^^^^^^^^^^^^^^

``Extent()``

Constructs default extent (1, 1).

Constructors
~~~~~~~~~~~~

Constructor *Extent*
^^^^^^^^^^^^^^^^^^^^

``Extent(System.Int32 width)``

Constructs specific extent.

The constructor has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``width``   | ``System.Int32``   | The width.    |
+-------------+--------------------+---------------+

Constructor *Extent*
^^^^^^^^^^^^^^^^^^^^

``Extent(System.Int32 width, System.Int32 height)``

Constructs specific extent from vector (conversion).

The constructor has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``width``    | ``System.Int32``   |               |
+--------------+--------------------+---------------+
| ``height``   | ``System.Int32``   |               |
+--------------+--------------------+---------------+

Properties
~~~~~~~~~~

Property *Width*
^^^^^^^^^^^^^^^^

``System.Int32 Width``

The width.

Property *Height*
^^^^^^^^^^^^^^^^^

``System.Int32 Height``

The height.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO no brief description for variant

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO no description for variant
