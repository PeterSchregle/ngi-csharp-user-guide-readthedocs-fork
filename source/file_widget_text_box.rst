Class *WidgetTextBox*
---------------------

A widget\_text\_box is a rectangle that can be set and/or interactively moved on a drawing surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetTextBox** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetTextBox** contains the following properties:

+------------------+-------+-------+---------------+
| Property         | Get   | Set   | Description   |
+==================+=======+=======+===============+
| ``TextWidget``   | \*    | \*    |               |
+------------------+-------+-------+---------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_text\_box. These settings affect the look and the behavior of the widget\_text\_box.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetTextBox*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetTextBox()``

Default constructor.

The default constructor creates a widget with default settings.

Properties
~~~~~~~~~~

Property *TextWidget*
^^^^^^^^^^^^^^^^^^^^^

``WidgetText TextWidget``
