Class *WidgetThickLineSegment*
------------------------------

A widget\_thick\_line\_segment displays a line segment on a surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetThickLineSegment** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetThickLineSegment** contains the following properties:

+----------------+-------+-------+----------------------------------------------------+
| Property       | Get   | Set   | Description                                        |
+================+=======+=======+====================================================+
| ``Position``   | \*    | \*    | The position of the widget.                        |
+----------------+-------+-------+----------------------------------------------------+
| ``Outline``    | \*    | \*    | The outline of the widget\_thick\_line\_segment.   |
+----------------+-------+-------+----------------------------------------------------+
| ``Fill``       | \*    | \*    | The fill of the widget\_thick\_line\_segment.      |
+----------------+-------+-------+----------------------------------------------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_thick\_line\_segment. These settings affect the look and the behavior of the widget\_thick\_line\_segment.

The widget\_thick\_line\_segment supports interactivity via the widget\_interactive base class, i.e. it can be selected, move around. In general it can react on mouse and keyboard interactions. This widget supports layout.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetThickLineSegment*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetThickLineSegment()``

Default constructor.

The default constructor creates a widget with default settings.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``ThickLineSegmentDouble Position``

The position of the widget.

Property *Outline*
^^^^^^^^^^^^^^^^^^

``PenByte Outline``

The outline of the widget\_thick\_line\_segment.

The outline of the widget\_thick\_line\_segment is specified with a pen<>.

Property *Fill*
^^^^^^^^^^^^^^^

``SolidColorBrushByte Fill``

The fill of the widget\_thick\_line\_segment.

The fill of the widget\_thick\_line\_segment is specified with a brush.
