Class *ComErrorException*
-------------------------

The COM (Component Object Model) error exception type.

**Namespace:** Ngi

**Module:**

Description
~~~~~~~~~~~

The HRESULT is stored in the exception id\_ member.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *ComErrorException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ComErrorException()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *ComErrorException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ComErrorException(System.Int32 hresult)``

Construct an exception from a HRESULT value.

The constructor has the following parameters:

+---------------+--------------------+-----------------------+
| Parameter     | Type               | Description           |
+===============+====================+=======================+
| ``hresult``   | ``System.Int32``   | The COM error code.   |
+---------------+--------------------+-----------------------+
