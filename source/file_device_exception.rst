Class *DeviceException*
-----------------------

The basic ngi device exception type.

**Namespace:** Ngi

**Module:**

The class **DeviceException** contains the following enumerations:

+--------------------+------------------------------+
| Enumeration        | Description                  |
+====================+==============================+
| ``ExceptionIds``   | TODO documentation missing   |
+--------------------+------------------------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *DeviceException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``DeviceException()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *DeviceException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``DeviceException(System.String message)``

Construct an exception from a message.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+

Constructor *DeviceException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``DeviceException(System.String message, System.UInt32 id)``

Construct an exception from a message and an id.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+
| ``id``        | ``System.UInt32``   | The exception id.        |
+---------------+---------------------+--------------------------+

Enumerations
~~~~~~~~~~~~

Enumeration *ExceptionIds*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum ExceptionIds``

TODO documentation missing

The enumeration **ExceptionIds** has the following constants:

+-----------------------------+---------+---------------+
| Name                        | Value   | Description   |
+=============================+=========+===============+
| ``notAnError``              | ``0``   |               |
+-----------------------------+---------+---------------+
| ``unknownError``            | ``1``   |               |
+-----------------------------+---------+---------------+
| ``deviceIsOpen``            | ``2``   |               |
+-----------------------------+---------+---------------+
| ``deviceIsNotOpen``         | ``3``   |               |
+-----------------------------+---------+---------------+
| ``invalidPropertyAspect``   | ``4``   |               |
+-----------------------------+---------+---------------+
| ``invalidNumberOfFrames``   | ``5``   |               |
+-----------------------------+---------+---------------+
| ``invalidFrameIndex``       | ``6``   |               |
+-----------------------------+---------+---------------+
| ``featureNotSupported``     | ``7``   |               |
+-----------------------------+---------+---------------+
| ``ringbufferIsLocked``      | ``8``   |               |
+-----------------------------+---------+---------------+

::

    enum ExceptionIds
    {
      notAnError = 0,
      unknownError = 1,
      deviceIsOpen = 2,
      deviceIsNotOpen = 3,
      invalidPropertyAspect = 4,
      invalidNumberOfFrames = 5,
      invalidFrameIndex = 6,
      featureNotSupported = 7,
      ringbufferIsLocked = 8,
    };

TODO documentation missing
