Class *Lab*
-----------

The lab color type.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Lab** contains the following variant parameters:

+-----------------+-----------------------------------------+
| Variant         | Description                             |
+=================+=========================================+
| ``Component``   | TODO no brief description for variant   |
+-----------------+-----------------------------------------+

The class **Lab** contains the following properties:

+------------+-------+-------+-------------------------------------+
| Property   | Get   | Set   | Description                         |
+============+=======+=======+=====================================+
| ``L``      | \*    |       | The l\_ color component property.   |
+------------+-------+-------+-------------------------------------+
| ``A``      | \*    |       | The a\_ color component property.   |
+------------+-------+-------+-------------------------------------+
| ``B``      | \*    |       | The b\_ color component property.   |
+------------+-------+-------+-------------------------------------+

The class **Lab** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

The lab color type template allows to be instantiated for the basic integer and floating point types.

Physically, the color primitives are in b - a - l order, to match the most obvious bitmap organization. Logically, however, the color primitives are in l - a - b order. The logical order is reflected in the parameter order in methods.

This class does not provide an implicit conversion operator to the laba type, but the other way round: the laba type provides a conversion from the lab type, so that buffers of this type can be conveniently displayed.

Variants
~~~~~~~~

Variant *Component*
^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Component** has the following types:

+--------------+
| Type         |
+==============+
| ``Int8``     |
+--------------+
| ``Byte``     |
+--------------+
| ``Int16``    |
+--------------+
| ``UInt16``   |
+--------------+
| ``Int32``    |
+--------------+
| ``UInt32``   |
+--------------+
| ``Int64``    |
+--------------+
| ``UInt64``   |
+--------------+
| ``Single``   |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Lab*
^^^^^^^^^^^^^^^^^

``Lab()``

Default constructor.

This constructor sets the three components to 0. This corresponds to a color that is pure black.

Constructors
~~~~~~~~~~~~

Constructor *Lab*
^^^^^^^^^^^^^^^^^

``Lab(System.Object value)``

Standard constructor.

The constructor has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``value``   | ``System.Object``   |               |
+-------------+---------------------+---------------+

This constructor initializes the three components with a gray value.

The constructor also serves as a conversion operator from the scalar type T to type lab<T>.

Constructor *Lab*
^^^^^^^^^^^^^^^^^

``Lab(System.Object l, System.Object a, System.Object b)``

Standard constructor.

The constructor has the following parameters:

+-------------+---------------------+--------------------+
| Parameter   | Type                | Description        |
+=============+=====================+====================+
| ``l``       | ``System.Object``   | The l component.   |
+-------------+---------------------+--------------------+
| ``a``       | ``System.Object``   | The a component.   |
+-------------+---------------------+--------------------+
| ``b``       | ``System.Object``   | The b component.   |
+-------------+---------------------+--------------------+

This constructor initializes the three components with different values.

Properties
~~~~~~~~~~

Property *L*
^^^^^^^^^^^^

``System.Object L``

The l\_ color component property.

Property *A*
^^^^^^^^^^^^

``System.Object A``

The a\_ color component property.

Property *B*
^^^^^^^^^^^^

``System.Object B``

The b\_ color component property.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
