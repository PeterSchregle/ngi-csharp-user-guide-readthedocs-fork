Class *WidgetOrthogonalGrid*
----------------------------

A widget\_orthogonal\_grid is a widget displaying a grid of crossing horizontal and vertical lines.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetOrthogonalGrid** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetOrthogonalGrid** contains the following properties:

+----------------+-------+-------+------------------------------------------+
| Property       | Get   | Set   | Description                              |
+================+=======+=======+==========================================+
| ``Position``   | \*    | \*    | The position (location) of the widget.   |
+----------------+-------+-------+------------------------------------------+
| ``Spacing``    | \*    | \*    | The spacing of the widget.               |
+----------------+-------+-------+------------------------------------------+
| ``Pen``        | \*    | \*    | The pen of the widget.                   |
+----------------+-------+-------+------------------------------------------+

Description
~~~~~~~~~~~

It can be positioned on some surface. It provides interaction, but tracking is not implemented. A widget\_orthogonal\_grid has interactive widget capabilities.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetOrthogonalGrid*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetOrthogonalGrid()``

Default constructor.

The default constructor creates a widget with default settings.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``PointDouble Position``

The position (location) of the widget.

Property *Spacing*
^^^^^^^^^^^^^^^^^^

``VectorDouble Spacing``

The spacing of the widget.

Property *Pen*
^^^^^^^^^^^^^^

``PenByte Pen``

The pen of the widget.
