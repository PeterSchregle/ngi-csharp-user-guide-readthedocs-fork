Class *HarmonicMeanFunctor*
---------------------------

This function object can be used to calculate the harmonic mean of a sequence of values.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **HarmonicMeanFunctor** contains the following variant parameters:

+------------+-----------------------------------------+
| Variant    | Description                             |
+============+=========================================+
| ``Type``   | TODO no brief description for variant   |
+------------+-----------------------------------------+

The class **HarmonicMeanFunctor** contains the following properties:

+---------------------+-------+-------+------------------------------+
| Property            | Get   | Set   | Description                  |
+=====================+=======+=======+==============================+
| ``Count``           | \*    |       | The count of the elements.   |
+---------------------+-------+-------+------------------------------+
| ``ReciprocalSum``   | \*    |       | The sum of the elements.     |
+---------------------+-------+-------+------------------------------+
| ``HarmonicMean``    | \*    |       | The harmonic mean.           |
+---------------------+-------+-------+------------------------------+

The class **HarmonicMeanFunctor** contains the following methods:

+--------------------------+-----------------------------+
| Method                   | Description                 |
+==========================+=============================+
| ``ProcessNextElement``   | Process the next element.   |
+--------------------------+-----------------------------+

Description
~~~~~~~~~~~

Usually the function object is used in algorithms such as for\_each. You can use it both for algorithms in the STL as well as for algorithms in ngi.

A nice description of the harmonic mean can be found at http://en.wikipedia.org/wiki/Harmonic\_mean.

Variants
~~~~~~~~

Variant *Type*
^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Type** has the following types:

+------------------+
| Type             |
+==================+
| ``Byte``         |
+------------------+
| ``UInt16``       |
+------------------+
| ``UInt32``       |
+------------------+
| ``Double``       |
+------------------+
| ``RgbByte``      |
+------------------+
| ``RgbUInt16``    |
+------------------+
| ``RgbUInt32``    |
+------------------+
| ``RgbDouble``    |
+------------------+
| ``RgbaByte``     |
+------------------+
| ``RgbaUInt16``   |
+------------------+
| ``RgbaUInt32``   |
+------------------+
| ``RgbaDouble``   |
+------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *HarmonicMeanFunctor*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HarmonicMeanFunctor()``

Construct the functor.

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

The count of the elements.

Property *ReciprocalSum*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.Object ReciprocalSum``

The sum of the elements.

Property *HarmonicMean*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Object HarmonicMean``

The harmonic mean.

Methods
~~~~~~~

Method *ProcessNextElement*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ProcessNextElement(System.Object element)``

Process the next element.

The method **ProcessNextElement** has the following parameters:

+---------------+---------------------+----------------------+
| Parameter     | Type                | Description          |
+===============+=====================+======================+
| ``element``   | ``System.Object``   | The element value.   |
+---------------+---------------------+----------------------+
