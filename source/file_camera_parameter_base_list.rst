Class *CameraParameterBaseList*
-------------------------------

**Namespace:** Ngi

**Module:** Camera

The class **CameraParameterBaseList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+

The class **CameraParameterBaseList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *CameraParameterBaseList*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``CameraParameterBaseList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``CameraParameterBaseEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(CameraParameterBase item)``

The method **Add** has the following parameters:

+-------------+---------------------------+---------------+
| Parameter   | Type                      | Description   |
+=============+===========================+===============+
| ``item``    | ``CameraParameterBase``   |               |
+-------------+---------------------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(CameraParameterBase item)``

The method **Contains** has the following parameters:

+-------------+---------------------------+---------------+
| Parameter   | Type                      | Description   |
+=============+===========================+===============+
| ``item``    | ``CameraParameterBase``   |               |
+-------------+---------------------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(CameraParameterBase item)``

The method **Remove** has the following parameters:

+-------------+---------------------------+---------------+
| Parameter   | Type                      | Description   |
+=============+===========================+===============+
| ``item``    | ``CameraParameterBase``   |               |
+-------------+---------------------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(CameraParameterBase item)``

The method **IndexOf** has the following parameters:

+-------------+---------------------------+---------------+
| Parameter   | Type                      | Description   |
+=============+===========================+===============+
| ``item``    | ``CameraParameterBase``   |               |
+-------------+---------------------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, CameraParameterBase item)``

The method **Insert** has the following parameters:

+-------------+---------------------------+---------------+
| Parameter   | Type                      | Description   |
+=============+===========================+===============+
| ``index``   | ``System.Int32``          |               |
+-------------+---------------------------+---------------+
| ``item``    | ``CameraParameterBase``   |               |
+-------------+---------------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
