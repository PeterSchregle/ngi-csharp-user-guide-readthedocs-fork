Class *TweeningSinusoidal1*
---------------------------

Calculating functor that calculates a sinusoidal tweening.

**Namespace:** Ngi

**Module:** ImageProcessing

Description
~~~~~~~~~~~

The function used for the tweening is the part of the sinus curve between 0 and pi/2.

This function starts with a slope of 1 and ends with a slope of 0.

This functor is used for the preset functions.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *TweeningSinusoidal1*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TweeningSinusoidal1()``

Default Constructor.
