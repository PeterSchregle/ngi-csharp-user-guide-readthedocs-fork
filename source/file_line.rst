Class *Line*
------------

A directed infinite line in two-dimensional euclidean space.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Line** implements the following interfaces:

+-------------------------------------------+
| Interface                                 |
+===========================================+
| ``IEquatableLineLineCoordinateVariant``   |
+-------------------------------------------+
| ``ISerializable``                         |
+-------------------------------------------+
| ``INotifyPropertyChanged``                |
+-------------------------------------------+

The class **Line** contains the following variant parameters:

+------------------+-----------------------------------------+
| Variant          | Description                             |
+==================+=========================================+
| ``Coordinate``   | TODO no brief description for variant   |
+------------------+-----------------------------------------+

The class **Line** contains the following properties:

+-----------------+-------+-------+-----------------------------------------------------------+
| Property        | Get   | Set   | Description                                               |
+=================+=======+=======+===========================================================+
| ``Origin``      | \*    | \*    | The starting point of the line.                           |
+-----------------+-------+-------+-----------------------------------------------------------+
| ``Direction``   | \*    | \*    |                                                           |
+-----------------+-------+-------+-----------------------------------------------------------+
| ``Dir``         | \*    |       | The direction of the line (for backward compatibility).   |
+-----------------+-------+-------+-----------------------------------------------------------+
| ``Normal``      | \*    |       | Get the normal.                                           |
+-----------------+-------+-------+-----------------------------------------------------------+
| ``Slope``       | \*    |       | The slope of the line.                                    |
+-----------------+-------+-------+-----------------------------------------------------------+
| ``Intercept``   | \*    |       |                                                           |
+-----------------+-------+-------+-----------------------------------------------------------+
| ``Pose``        | \*    |       | The pose of the line.                                     |
+-----------------+-------+-------+-----------------------------------------------------------+

The class **Line** contains the following methods:

+---------------------------+---------------------------------------------------------------+
| Method                    | Description                                                   |
+===========================+===============================================================+
| ``Move``                  | Move line.                                                    |
+---------------------------+---------------------------------------------------------------+
| ``GetPointOrientation``   | Returns the orientation of the point relative to this line.   |
+---------------------------+---------------------------------------------------------------+
| ``ToString``              | Provide string representation for debugging purposes.         |
+---------------------------+---------------------------------------------------------------+

Description
~~~~~~~~~~~

The line starts at its origin and extends in both its positive and negative direction into infinity but with a distinguished direction.

A line can be seen as a point combined with a direction vector in two-dimensional space. The direction vector is normalized to quadrants I and IV, i.e. the direction ranges from -pi/2 to +pi/2 (-90° to 90°). This is done to reduce ambiguity (a line with a direction of 30° would be the same as a line with a direction of 210°).

The following operations are implemented: operator == : comparison for equality. operator != : comparison for inequality. operator- : negation, directly implemented. operator+ : addition, implemented through boost::additive operator-= : subtraction, directly implemented. operator- : subtraction, implemented through boost::additive

Variants
~~~~~~~~

Variant *Coordinate*
^^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Coordinate** has the following types:

+--------------+
| Type         |
+==============+
| ``Int32``    |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Line*
^^^^^^^^^^^^^^^^^^

``Line()``

Default constructor.

By default this is a horizontal line through the origin.

Constructors
~~~~~~~~~~~~

Constructor *Line*
^^^^^^^^^^^^^^^^^^

``Line(Point origin, Direction direction)``

Construct a line from a point and a direction.

The constructor has the following parameters:

+-----------------+-----------------+------------------+
| Parameter       | Type            | Description      |
+=================+=================+==================+
| ``origin``      | ``Point``       | The origin.      |
+-----------------+-----------------+------------------+
| ``direction``   | ``Direction``   | The direction.   |
+-----------------+-----------------+------------------+

Constructor *Line*
^^^^^^^^^^^^^^^^^^

``Line(Point origin, Point other)``

Construct a line from two points.

The constructor has the following parameters:

+--------------+-------------+--------------------+
| Parameter    | Type        | Description        |
+==============+=============+====================+
| ``origin``   | ``Point``   | The origin.        |
+--------------+-------------+--------------------+
| ``other``    | ``Point``   | The other point.   |
+--------------+-------------+--------------------+

Constructor *Line*
^^^^^^^^^^^^^^^^^^

``Line(System.Double slope, System.Double intercept)``

Construct a line from slope and intercept values.

The constructor has the following parameters:

+-----------------+---------------------+---------------+
| Parameter       | Type                | Description   |
+=================+=====================+===============+
| ``slope``       | ``System.Double``   |               |
+-----------------+---------------------+---------------+
| ``intercept``   | ``System.Double``   |               |
+-----------------+---------------------+---------------+

The mathematical formula for a line is y = ax + b. A is the slope, b is the intercept. This constructor builds the line from the slope and intercept values a and b.

Properties
~~~~~~~~~~

Property *Origin*
^^^^^^^^^^^^^^^^^

``Point Origin``

The starting point of the line.

Property *Direction*
^^^^^^^^^^^^^^^^^^^^

``Direction Direction``

Property *Dir*
^^^^^^^^^^^^^^

``Direction Dir``

The direction of the line (for backward compatibility).

Property *Normal*
^^^^^^^^^^^^^^^^^

``Direction Normal``

Get the normal.

This function provides read-access only.

Property *Slope*
^^^^^^^^^^^^^^^^

``System.Double Slope``

The slope of the line.

Property *Intercept*
^^^^^^^^^^^^^^^^^^^^

``System.Double Intercept``

Property *Pose*
^^^^^^^^^^^^^^^

``Pose Pose``

The pose of the line.

Static Methods
~~~~~~~~~~~~~~

Method *Intersect*
^^^^^^^^^^^^^^^^^^

``PointDouble Intersect(Line a, Line b)``

Intersect two lines and return the intersecting point, if any.

The method **Intersect** has the following parameters:

+-------------+------------+---------------+
| Parameter   | Type       | Description   |
+=============+============+===============+
| ``a``       | ``Line``   |               |
+-------------+------------+---------------+
| ``b``       | ``Line``   |               |
+-------------+------------+---------------+

If the lines intersect, the intersecting point is returned. If the lines are parallel, there is no intersection and an exception is thrown. If the lines are collinear, there is an infinite number of intersections and an exception is thrown.

Methods
~~~~~~~

Method *Move*
^^^^^^^^^^^^^

``Line Move(Vector movement)``

Move line.

The method **Move** has the following parameters:

+----------------+--------------+--------------------+
| Parameter      | Type         | Description        |
+================+==============+====================+
| ``movement``   | ``Vector``   | Movement vector.   |
+----------------+--------------+--------------------+

Moves a line by a vector.

The moved line.

Method *GetPointOrientation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double GetPointOrientation(Point pt)``

Returns the orientation of the point relative to this line.

The method **GetPointOrientation** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``pt``      | ``Point``   |               |
+-------------+-------------+---------------+

Returns negative values if the point is to the left, positive values if the point is to the right and zero if the point is on the line. Left and right are used in the sense as defined in a right handed coordinate system; in reality nGI uses a left handed coordinate system and therefore left and right are reversed.

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO documentation missing

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO documentation missing
