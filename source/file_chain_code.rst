Class *ChainCode*
-----------------

Class that provides a chain code representation.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **ChainCode** implements the following interfaces:

+---------------------------+
| Interface                 |
+===========================+
| ``IEquatableChainCode``   |
+---------------------------+

The class **ChainCode** contains the following properties:

+---------------------+-------+-------+--------------------------------------------------------------+
| Property            | Get   | Set   | Description                                                  |
+=====================+=======+=======+==============================================================+
| ``Origin``          | \*    | \*    | The starting point.                                          |
+---------------------+-------+-------+--------------------------------------------------------------+
| ``FreemanCodes``    | \*    | \*    | The list of freeman codes.                                   |
+---------------------+-------+-------+--------------------------------------------------------------+
| ``Freeman``         | \*    |       | The list of freeman\_codes in string form.                   |
+---------------------+-------+-------+--------------------------------------------------------------+
| ``ContourLength``   | \*    |       | Returns the length of a contour defined with a chain code.   |
+---------------------+-------+-------+--------------------------------------------------------------+
| ``Points``          | \*    |       | The chain code in the form of a point\_list.                 |
+---------------------+-------+-------+--------------------------------------------------------------+

The class **ChainCode** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

A chain code consists of a starting point as well as a sequence of freeman\_code values to specify a contour.

Here is an example of an object and its corresponding chain code.

+++++++++++ 9 \| \| \| \| \| \| \| \| \| \| \| +++++++++++ 8 \| \| \| x \| x \| x \| x \| \| x \| \| \| +++++++++++ 7 \| \| x \| x \| x \| x \| x \| x \| x \| x \| \| +++++++++++ 6 \| \| x \| x \| x \| x \| x \| x \| x \| x \| \| +++++++++++ 5 \| \| x \| x \| x \| x \| x \| x \| x \| \| \| +++++++++++ 4 \| \| x \| x \| x \| x \| x \| x \| x \| x \| \| +++++++++++ 3 \| \| x \| x \| x \| x \| x \| x \| x \| x \| \| +++++++++++ 2 \| \| x \| x \| x \| x \| x \| x \| x \| \| \| +++++++++++ 1 \| \| x \| x \| x \| x \| \| \| \| \| \| +++++++++++ 0 \| \| \| \| \| \| \| \| \| \| \| +++++++++++ 0 1 2 3 4 5 6 7 8 9

The starting pixel is at the lower left corner at the location (1,1).

The eight-connected chain code of the object is 000100123123534445666666. The eight-connected chain code corresponds to the following contour:

+++++++++++ 9 \| \| \| \| \| \| \| \| \| \| \| +++++++++++ 8 \| \| \| x \| x \| x \| x \| \| x \| \| \| +++++++++++ 7 \| \| x \| \| \| \| \| x \| \| x \| \| +++++++++++ 6 \| \| x \| \| \| \| \| \| \| x \| \| +++++++++++ 5 \| \| x \| \| \| \| \| \| x \| \| \| +++++++++++ 4 \| \| x \| \| \| \| \| \| \| x \| \| +++++++++++ 3 \| \| x \| \| \| \| \| \| \| x \| \| +++++++++++ 2 \| \| x \| \| \| \| x \| x \| x \| \| \| +++++++++++ 1 \| \| x \| x \| x \| x \| \| \| \| \| \| +++++++++++ 0 \| \| \| \| \| \| \| \| \| \| \| +++++++++++ 0 1 2 3 4 5 6 7 8 9

The four-connected chain code is 00020002024220242644244464666666. The four-connected chain code corresponds to the following contour:

+++++++++++ 9 \| \| \| \| \| \| \| \| \| \| \| +++++++++++ 8 \| \| \| x \| x \| x \| x \| \| x \| \| \| +++++++++++ 7 \| \| x \| x \| \| \| x \| x \| x \| x \| \| +++++++++++ 6 \| \| x \| \| \| \| \| \| x \| x \| \| +++++++++++ 5 \| \| x \| \| \| \| \| \| x \| \| \| +++++++++++ 4 \| \| x \| \| \| \| \| \| x \| x \| \| +++++++++++ 3 \| \| x \| \| \| \| \| \| x \| x \| \| +++++++++++ 2 \| \| x \| \| \| x \| x \| x \| x \| \| \| +++++++++++ 1 \| \| x \| x \| x \| x \| \| \| \| \| \| +++++++++++ 0 \| \| \| \| \| \| \| \| \| \| \| +++++++++++ 0 1 2 3 4 5 6 7 8 9

Chain codes are the basis for algorithms that can detect certain object characteristics, such as the perimeter, etc.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *ChainCode*
^^^^^^^^^^^^^^^^^^^^^^^

``ChainCode()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *ChainCode*
^^^^^^^^^^^^^^^^^^^^^^^

``ChainCode(PointInt32 origin)``

Constructor.

The constructor has the following parameters:

+--------------+------------------+-----------------------+
| Parameter    | Type             | Description           |
+==============+==================+=======================+
| ``origin``   | ``PointInt32``   | The starting point.   |
+--------------+------------------+-----------------------+

Constructor *ChainCode*
^^^^^^^^^^^^^^^^^^^^^^^

``ChainCode(PointInt32 orgin, FreemanCodeList freemanCodes)``

Constructor.

The constructor has the following parameters:

+--------------------+-----------------------+------------------------------+
| Parameter          | Type                  | Description                  |
+====================+=======================+==============================+
| ``orgin``          | ``PointInt32``        |                              |
+--------------------+-----------------------+------------------------------+
| ``freemanCodes``   | ``FreemanCodeList``   | The list of freeman codes.   |
+--------------------+-----------------------+------------------------------+

Properties
~~~~~~~~~~

Property *Origin*
^^^^^^^^^^^^^^^^^

``PointInt32 Origin``

The starting point.

Property *FreemanCodes*
^^^^^^^^^^^^^^^^^^^^^^^

``FreemanCodeList FreemanCodes``

The list of freeman codes.

Property *Freeman*
^^^^^^^^^^^^^^^^^^

``System.String Freeman``

The list of freeman\_codes in string form.

Property *ContourLength*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double ContourLength``

Returns the length of a contour defined with a chain code.

The length is calculated by weighing diagonal steps with a length of sqrt(2) and orthogonal steps with a length of 1. Usually this exaggerates the length so that the returned length is likely a bit too long.

Property *Points*
^^^^^^^^^^^^^^^^^

``PointListPointInt32 Points``

The chain code in the form of a point\_list.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
