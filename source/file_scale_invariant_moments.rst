Class *ScaleInvariantMoments*
-----------------------------

Class to hold scale\_invariant\_moments of the second and third order.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **ScaleInvariantMoments** implements the following interfaces:

+---------------------------------------+
| Interface                             |
+=======================================+
| ``IEquatableScaleInvariantMoments``   |
+---------------------------------------+
| ``ISerializable``                     |
+---------------------------------------+

The class **ScaleInvariantMoments** contains the following properties:

+-------------+-------+-------+------------------------------------------------------------+
| Property    | Get   | Set   | Description                                                |
+=============+=======+=======+============================================================+
| ``Eta20``   | \*    |       | The first order horizontal scale invariant moment.         |
+-------------+-------+-------+------------------------------------------------------------+
| ``Eta11``   | \*    |       | The first order mixed scale invariant moment.              |
+-------------+-------+-------+------------------------------------------------------------+
| ``Eta02``   | \*    |       | The first order vertical scale invariant moment.           |
+-------------+-------+-------+------------------------------------------------------------+
| ``Eta30``   | \*    |       | The third order horizontal scale invariant moment.         |
+-------------+-------+-------+------------------------------------------------------------+
| ``Eta21``   | \*    |       | The third order horizontal mixed scale invariant moment.   |
+-------------+-------+-------+------------------------------------------------------------+
| ``Eta12``   | \*    |       | The third order vertical mixed scale invariant moment.     |
+-------------+-------+-------+------------------------------------------------------------+
| ``Eta03``   | \*    |       | The third order vertical scale invariant moment.           |
+-------------+-------+-------+------------------------------------------------------------+

The class **ScaleInvariantMoments** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

For a nice and short treatment of moments see Chaur-Chin Chen, Improved Moment Invariants for Shape Discrimination [1992].

Moments are a sort of weighted averages of the image pixels' intensities (the pixel intensities may be set to 1 in case of binary moments). They are useful to describe segmented objects. Simple properties of objects found via moments include area (or total intensity), its centroid, and information about its orientation.

The scale\_invariant\_moments are invariant with respect to translation and scaling, but not rotation.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *ScaleInvariantMoments*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ScaleInvariantMoments()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *ScaleInvariantMoments*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ScaleInvariantMoments(CentralMoments mu)``

Standard constructor.

The constructor has the following parameters:

+-------------+----------------------+--------------------+
| Parameter   | Type                 | Description        |
+=============+======================+====================+
| ``mu``      | ``CentralMoments``   | Central moments.   |
+-------------+----------------------+--------------------+

Initializes the scale\_invariant\_moments.

Properties
~~~~~~~~~~

Property *Eta20*
^^^^^^^^^^^^^^^^

``System.Double Eta20``

The first order horizontal scale invariant moment.

Property *Eta11*
^^^^^^^^^^^^^^^^

``System.Double Eta11``

The first order mixed scale invariant moment.

Property *Eta02*
^^^^^^^^^^^^^^^^

``System.Double Eta02``

The first order vertical scale invariant moment.

Property *Eta30*
^^^^^^^^^^^^^^^^

``System.Double Eta30``

The third order horizontal scale invariant moment.

Property *Eta21*
^^^^^^^^^^^^^^^^

``System.Double Eta21``

The third order horizontal mixed scale invariant moment.

Property *Eta12*
^^^^^^^^^^^^^^^^

``System.Double Eta12``

The third order vertical mixed scale invariant moment.

Property *Eta03*
^^^^^^^^^^^^^^^^

``System.Double Eta03``

The third order vertical scale invariant moment.

Static Methods
~~~~~~~~~~~~~~

Method *CalculateEta20*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateEta20(CentralMoments mu)``

Calculate eta20 from central moments.

The method **CalculateEta20** has the following parameters:

+-------------+----------------------+---------------+
| Parameter   | Type                 | Description   |
+=============+======================+===============+
| ``mu``      | ``CentralMoments``   |               |
+-------------+----------------------+---------------+

eta20 is is a second order scale invariant moment.

The eta20 scale invariant moment.

Method *CalculateEta11*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateEta11(CentralMoments mu)``

Calculate eta11 from central moments.

The method **CalculateEta11** has the following parameters:

+-------------+----------------------+---------------+
| Parameter   | Type                 | Description   |
+=============+======================+===============+
| ``mu``      | ``CentralMoments``   |               |
+-------------+----------------------+---------------+

eta11 is is a second order scale invariant moment.

The eta11 scale invariant moment.

Method *CalculateEta02*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateEta02(CentralMoments mu)``

Calculate eta02 from central moments.

The method **CalculateEta02** has the following parameters:

+-------------+----------------------+---------------+
| Parameter   | Type                 | Description   |
+=============+======================+===============+
| ``mu``      | ``CentralMoments``   |               |
+-------------+----------------------+---------------+

eta02 is is a second order scale invariant moment.

The eta02 scale invariant moment.

Method *CalculateEta30*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateEta30(CentralMoments mu)``

Calculate eta30 from central moments.

The method **CalculateEta30** has the following parameters:

+-------------+----------------------+---------------+
| Parameter   | Type                 | Description   |
+=============+======================+===============+
| ``mu``      | ``CentralMoments``   |               |
+-------------+----------------------+---------------+

eta30 is is a third order scale invariant moment.

The eta30 scale invariant moment.

Method *CalculateEta21*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateEta21(CentralMoments mu)``

Calculate eta21 from central moments.

The method **CalculateEta21** has the following parameters:

+-------------+----------------------+---------------+
| Parameter   | Type                 | Description   |
+=============+======================+===============+
| ``mu``      | ``CentralMoments``   |               |
+-------------+----------------------+---------------+

eta21 is is a third order scale invariant moment.

The eta21 scale invariant moment.

Method *CalculateEta12*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateEta12(CentralMoments mu)``

Calculate eta12 from central moments.

The method **CalculateEta12** has the following parameters:

+-------------+----------------------+---------------+
| Parameter   | Type                 | Description   |
+=============+======================+===============+
| ``mu``      | ``CentralMoments``   |               |
+-------------+----------------------+---------------+

eta12 is is a third order scale invariant moment.

The eta12 scale invariant moment.

Method *CalculateEta03*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateEta03(CentralMoments mu)``

Calculate eta03 from central moments.

The method **CalculateEta03** has the following parameters:

+-------------+----------------------+---------------+
| Parameter   | Type                 | Description   |
+=============+======================+===============+
| ``mu``      | ``CentralMoments``   |               |
+-------------+----------------------+---------------+

eta03 is is a third order scale invariant moment.

The eta03 scale invariant moment.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
