Class *ImageFileInfo*
---------------------

The image\_file\_info class allows you to get information about a picture stored in a file, such as its size and type of data.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **ImageFileInfo** contains the following properties:

+----------------------+-------+-------+-------------------------------------------------------------+
| Property             | Get   | Set   | Description                                                 |
+======================+=======+=======+=============================================================+
| ``Pathname``         | \*    |       | The pathname of the file.                                   |
+----------------------+-------+-------+-------------------------------------------------------------+
| ``Width``            | \*    |       | Width of the image contained in the file.                   |
+----------------------+-------+-------+-------------------------------------------------------------+
| ``Height``           | \*    |       | Height of the image contained in the file.                  |
+----------------------+-------+-------+-------------------------------------------------------------+
| ``Depth``            | \*    |       | Depth of the image contained in the file.                   |
+----------------------+-------+-------+-------------------------------------------------------------+
| ``Size``             | \*    |       | Size of the image contained in the file.                    |
+----------------------+-------+-------+-------------------------------------------------------------+
| ``Volume``           | \*    |       | Volume of the image contained in the file.                  |
+----------------------+-------+-------+-------------------------------------------------------------+
| ``ChannelCount``     | \*    |       | Get the number of color channels contained in the file.     |
+----------------------+-------+-------+-------------------------------------------------------------+
| ``BitsPerChannel``   | \*    |       | Get the number of bits per channel contained in the file.   |
+----------------------+-------+-------+-------------------------------------------------------------+
| ``HasPalette``       | \*    |       | Determines if the file contains a color palette.            |
+----------------------+-------+-------+-------------------------------------------------------------+

The class **ImageFileInfo** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *ImageFileInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageFileInfo()``

Constructs an empty image\_file\_info object.

Constructors
~~~~~~~~~~~~

Constructor *ImageFileInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageFileInfo(System.String pathname)``

Constructs a image\_file\_info object from a file.

The constructor has the following parameters:

+----------------+---------------------+----------------------------------------+
| Parameter      | Type                | Description                            |
+================+=====================+========================================+
| ``pathname``   | ``System.String``   | The pathname of the file to inspect.   |
+----------------+---------------------+----------------------------------------+

The filename can either be a complete (absolute or relative) path to a file, or it can be a partial filename.

If a complete path to a file is given, the function tries to load the respective file. If a partial filename is given, the function appends it to the path fragment stored in the NGI\_IMAGE\_PATH environment variable. It then tries to load the file addressed by this combined path.

Constructor *ImageFileInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageFileInfo(System.IntPtr memory, System.Int32 size)``

Constructs a image\_file\_info object from memory.

The constructor has the following parameters:

+--------------+---------------------+----------------------------------------+
| Parameter    | Type                | Description                            |
+==============+=====================+========================================+
| ``memory``   | ``System.IntPtr``   | Pointer to memory.                     |
+--------------+---------------------+----------------------------------------+
| ``size``     | ``System.Int32``    | Size (in bytes) of the memory block.   |
+--------------+---------------------+----------------------------------------+

Constructor *ImageFileInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageFileInfo(DataList data)``

Constructs a image\_file\_info object from a data\_list.

The constructor has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``data``    | ``DataList``   | The data..    |
+-------------+----------------+---------------+

Properties
~~~~~~~~~~

Property *Pathname*
^^^^^^^^^^^^^^^^^^^

``System.String Pathname``

The pathname of the file.

Property *Width*
^^^^^^^^^^^^^^^^

``System.Int32 Width``

Width of the image contained in the file.

The width is constant and can be read only. The function returns 0 if the file format is not understood.

Property *Height*
^^^^^^^^^^^^^^^^^

``System.Int32 Height``

Height of the image contained in the file.

The height is constant and can be read only. The function returns 0 if the file format is not understood.

Property *Depth*
^^^^^^^^^^^^^^^^

``System.Int32 Depth``

Depth of the image contained in the file.

The depth is constant and can be read only. The function returns 0 if the file format is not understood.

Property *Size*
^^^^^^^^^^^^^^^

``Extent3d Size``

Size of the image contained in the file.

The size is constant and can be read only. The function returns a zero extent\_3d if the file format is not understood.

Property *Volume*
^^^^^^^^^^^^^^^^^

``System.Int32 Volume``

Volume of the image contained in the file.

The volume is constant and can be read only. The function returns a zero volume if the file format is not understood.

Property *ChannelCount*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Int32 ChannelCount``

Get the number of color channels contained in the file.

The channel count is constant and can be read only. The function returns 0 if the file format is not understood.

Property *BitsPerChannel*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Int32 BitsPerChannel``

Get the number of bits per channel contained in the file.

The number of bits is constant and can be read only. The function returns 0 if the file format is not understood.

Property *HasPalette*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean HasPalette``

Determines if the file contains a color palette.

True if the file contains a color palette, false otherwise.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
