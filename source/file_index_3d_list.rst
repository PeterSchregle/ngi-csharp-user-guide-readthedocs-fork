Class *Index3dList*
-------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Index3dList** implements the following interfaces:

+--------------------+
| Interface          |
+====================+
| ``IListIndex3d``   |
+--------------------+

The class **Index3dList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **Index3dList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Index3dList*
^^^^^^^^^^^^^^^^^^^^^^^^^

``Index3dList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``Index3d [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``Index3dEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(Index3d item)``

The method **Add** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``item``    | ``Index3d``   |               |
+-------------+---------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(Index3d item)``

The method **Contains** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``item``    | ``Index3d``   |               |
+-------------+---------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(Index3d item)``

The method **Remove** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``item``    | ``Index3d``   |               |
+-------------+---------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(Index3d item)``

The method **IndexOf** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``item``    | ``Index3d``   |               |
+-------------+---------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, Index3d item)``

The method **Insert** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``item``    | ``Index3d``        |               |
+-------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
