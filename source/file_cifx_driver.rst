Class *CifxDriver*
------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **CifxDriver** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Version``   | \*    |       |               |
+---------------+-------+-------+---------------+
| ``Boards``    | \*    |       |               |
+---------------+-------+-------+---------------+

The class **CifxDriver** contains the following methods:

+-------------------+---------------+
| Method            | Description   |
+===================+===============+
| ``OpenChannel``   |               |
+-------------------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *CifxDriver*
^^^^^^^^^^^^^^^^^^^^^^^^

``CifxDriver()``

Properties
~~~~~~~~~~

Property *Version*
^^^^^^^^^^^^^^^^^^

``System.String Version``

Property *Boards*
^^^^^^^^^^^^^^^^^

``StringList Boards``

Static Methods
~~~~~~~~~~~~~~

Method *IsAvailable*
^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsAvailable()``

Methods
~~~~~~~

Method *OpenChannel*
^^^^^^^^^^^^^^^^^^^^

``CifxChannel OpenChannel(System.String board, System.UInt32 channelIdx)``

The method **OpenChannel** has the following parameters:

+------------------+---------------------+---------------+
| Parameter        | Type                | Description   |
+==================+=====================+===============+
| ``board``        | ``System.String``   |               |
+------------------+---------------------+---------------+
| ``channelIdx``   | ``System.UInt32``   |               |
+------------------+---------------------+---------------+
