Class *TeighaParser*
--------------------

Parse DXF/DWG file and create a drawing.

**Namespace:** Ngi

**Module:** CadPdf

Description
~~~~~~~~~~~

The drawing consists of a box specifying its dimensions, and a list of contours.

This has been superseded by teigha\_parser2 and teigha\_drawing2. This version is kept for compatibility reasons.

Static Methods
~~~~~~~~~~~~~~

Method *ParseDxfFile*
^^^^^^^^^^^^^^^^^^^^^

``TeighaDrawing ParseDxfFile(System.String fileName, System.String layer)``

Opens and parses a DXF file.

The method **ParseDxfFile** has the following parameters:

+----------------+---------------------+---------------+
| Parameter      | Type                | Description   |
+================+=====================+===============+
| ``fileName``   | ``System.String``   |               |
+----------------+---------------------+---------------+
| ``layer``      | ``System.String``   |               |
+----------------+---------------------+---------------+

A teigha\_drawing, containing the geometries.
