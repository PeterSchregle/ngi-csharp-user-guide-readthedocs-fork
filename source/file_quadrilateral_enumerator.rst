Class *QuadrilateralEnumerator*
-------------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **QuadrilateralEnumerator** implements the following interfaces:

+--------------------------------------------------------------+
| Interface                                                    |
+==============================================================+
| ``IEnumerator``                                              |
+--------------------------------------------------------------+
| ``IEnumeratorQuadrilateralEnumeratorQuadrilateralVariant``   |
+--------------------------------------------------------------+

The class **QuadrilateralEnumerator** contains the following variant parameters:

+---------------------+-----------------------------------------+
| Variant             | Description                             |
+=====================+=========================================+
| ``Quadrilateral``   | TODO no brief description for variant   |
+---------------------+-----------------------------------------+

The class **QuadrilateralEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **QuadrilateralEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Quadrilateral*
^^^^^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Quadrilateral** has the following types:

+---------------------------+
| Type                      |
+===========================+
| ``QuadrilateralInt32``    |
+---------------------------+
| ``QuadrilateralDouble``   |
+---------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Quadrilateral Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
