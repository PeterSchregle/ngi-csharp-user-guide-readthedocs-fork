Class *CameraParameterIenumEntry*
---------------------------------

**Namespace:** Ngi

**Module:** Camera

The class **CameraParameterIenumEntry** contains the following properties:

+--------------------+-------+-------+---------------+
| Property           | Get   | Set   | Description   |
+====================+=======+=======+===============+
| ``Value``          | \*    |       |               |
+--------------------+-------+-------+---------------+
| ``Symbolic``       | \*    |       |               |
+--------------------+-------+-------+---------------+
| ``NumericValue``   | \*    |       |               |
+--------------------+-------+-------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Value*
^^^^^^^^^^^^^^^^

``System.Int64 Value``

Property *Symbolic*
^^^^^^^^^^^^^^^^^^^

``System.String Symbolic``

Property *NumericValue*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Double NumericValue``
