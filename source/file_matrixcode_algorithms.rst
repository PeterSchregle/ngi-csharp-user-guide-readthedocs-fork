Class *MatrixcodeAlgorithms*
----------------------------

Wraps free-standing functions for matrixcode decoding.

**Namespace:** Ngi

**Module:** BarcodeMatrixcode

Description
~~~~~~~~~~~

The purpose of this class is to make the functions available for the .NET wrapper.

Static Methods
~~~~~~~~~~~~~~

Method *MvizDecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MvizMatrixcode MvizDecodeMatrixcode(ViewLocatorByte source, MvizMatrixcodeDecoder configuration)``

The method **MvizDecodeMatrixcode** has the following parameters:

+---------------------+-----------------------------+---------------+
| Parameter           | Type                        | Description   |
+=====================+=============================+===============+
| ``source``          | ``ViewLocatorByte``         |               |
+---------------------+-----------------------------+---------------+
| ``configuration``   | ``MvizMatrixcodeDecoder``   |               |
+---------------------+-----------------------------+---------------+

Method *MvizDecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MvizMatrixcode MvizDecodeMatrixcode(ViewLocatorUInt16 source, MvizMatrixcodeDecoder configuration)``

The method **MvizDecodeMatrixcode** has the following parameters:

+---------------------+-----------------------------+---------------+
| Parameter           | Type                        | Description   |
+=====================+=============================+===============+
| ``source``          | ``ViewLocatorUInt16``       |               |
+---------------------+-----------------------------+---------------+
| ``configuration``   | ``MvizMatrixcodeDecoder``   |               |
+---------------------+-----------------------------+---------------+

Method *MvizDecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MvizMatrixcode MvizDecodeMatrixcode(ViewLocatorUInt32 source, MvizMatrixcodeDecoder configuration)``

The method **MvizDecodeMatrixcode** has the following parameters:

+---------------------+-----------------------------+---------------+
| Parameter           | Type                        | Description   |
+=====================+=============================+===============+
| ``source``          | ``ViewLocatorUInt32``       |               |
+---------------------+-----------------------------+---------------+
| ``configuration``   | ``MvizMatrixcodeDecoder``   |               |
+---------------------+-----------------------------+---------------+

Method *MvizDecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MvizMatrixcode MvizDecodeMatrixcode(ViewLocatorRgbByte source, MvizMatrixcodeDecoder configuration)``

The method **MvizDecodeMatrixcode** has the following parameters:

+---------------------+-----------------------------+---------------+
| Parameter           | Type                        | Description   |
+=====================+=============================+===============+
| ``source``          | ``ViewLocatorRgbByte``      |               |
+---------------------+-----------------------------+---------------+
| ``configuration``   | ``MvizMatrixcodeDecoder``   |               |
+---------------------+-----------------------------+---------------+

Method *MvizDecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MvizMatrixcode MvizDecodeMatrixcode(ViewLocatorRgbaByte source, MvizMatrixcodeDecoder configuration)``

The method **MvizDecodeMatrixcode** has the following parameters:

+---------------------+-----------------------------+---------------+
| Parameter           | Type                        | Description   |
+=====================+=============================+===============+
| ``source``          | ``ViewLocatorRgbaByte``     |               |
+---------------------+-----------------------------+---------------+
| ``configuration``   | ``MvizMatrixcodeDecoder``   |               |
+---------------------+-----------------------------+---------------+

Method *MvizDecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MvizMatrixcode MvizDecodeMatrixcode(View source, MvizMatrixcodeDecoder configuration)``

The method **MvizDecodeMatrixcode** has the following parameters:

+---------------------+-----------------------------+---------------+
| Parameter           | Type                        | Description   |
+=====================+=============================+===============+
| ``source``          | ``View``                    |               |
+---------------------+-----------------------------+---------------+
| ``configuration``   | ``MvizMatrixcodeDecoder``   |               |
+---------------------+-----------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(ViewLocatorByte source, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+-------------------------+---------------+
| Parameter           | Type                    | Description   |
+=====================+=========================+===============+
| ``source``          | ``ViewLocatorByte``     |               |
+---------------------+-------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``   |               |
+---------------------+-------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(ViewLocatorUInt16 source, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+-------------------------+---------------+
| Parameter           | Type                    | Description   |
+=====================+=========================+===============+
| ``source``          | ``ViewLocatorUInt16``   |               |
+---------------------+-------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``   |               |
+---------------------+-------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(ViewLocatorUInt32 source, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+-------------------------+---------------+
| Parameter           | Type                    | Description   |
+=====================+=========================+===============+
| ``source``          | ``ViewLocatorUInt32``   |               |
+---------------------+-------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``   |               |
+---------------------+-------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(ViewLocatorDouble source, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+-------------------------+---------------+
| Parameter           | Type                    | Description   |
+=====================+=========================+===============+
| ``source``          | ``ViewLocatorDouble``   |               |
+---------------------+-------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``   |               |
+---------------------+-------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(ViewLocatorRgbByte source, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+--------------------------+---------------+
| Parameter           | Type                     | Description   |
+=====================+==========================+===============+
| ``source``          | ``ViewLocatorRgbByte``   |               |
+---------------------+--------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``    |               |
+---------------------+--------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(ViewLocatorRgbUInt16 source, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+----------------------------+---------------+
| Parameter           | Type                       | Description   |
+=====================+============================+===============+
| ``source``          | ``ViewLocatorRgbUInt16``   |               |
+---------------------+----------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``      |               |
+---------------------+----------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(ViewLocatorRgbUInt32 source, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+----------------------------+---------------+
| Parameter           | Type                       | Description   |
+=====================+============================+===============+
| ``source``          | ``ViewLocatorRgbUInt32``   |               |
+---------------------+----------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``      |               |
+---------------------+----------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(ViewLocatorRgbDouble source, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+----------------------------+---------------+
| Parameter           | Type                       | Description   |
+=====================+============================+===============+
| ``source``          | ``ViewLocatorRgbDouble``   |               |
+---------------------+----------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``      |               |
+---------------------+----------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(ViewLocatorRgbaByte source, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+---------------------------+---------------+
| Parameter           | Type                      | Description   |
+=====================+===========================+===============+
| ``source``          | ``ViewLocatorRgbaByte``   |               |
+---------------------+---------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``     |               |
+---------------------+---------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(ViewLocatorRgbaUInt16 source, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+-----------------------------+---------------+
| Parameter           | Type                        | Description   |
+=====================+=============================+===============+
| ``source``          | ``ViewLocatorRgbaUInt16``   |               |
+---------------------+-----------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``       |               |
+---------------------+-----------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(ViewLocatorRgbaUInt32 source, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+-----------------------------+---------------+
| Parameter           | Type                        | Description   |
+=====================+=============================+===============+
| ``source``          | ``ViewLocatorRgbaUInt32``   |               |
+---------------------+-----------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``       |               |
+---------------------+-----------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(ViewLocatorRgbaDouble source, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+-----------------------------+---------------+
| Parameter           | Type                        | Description   |
+=====================+=============================+===============+
| ``source``          | ``ViewLocatorRgbaDouble``   |               |
+---------------------+-----------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``       |               |
+---------------------+-----------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(View source, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+-------------------------+---------------+
| Parameter           | Type                    | Description   |
+=====================+=========================+===============+
| ``source``          | ``View``                |               |
+---------------------+-------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``   |               |
+---------------------+-------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(ViewLocatorByte source, Region region, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+-------------------------+---------------+
| Parameter           | Type                    | Description   |
+=====================+=========================+===============+
| ``source``          | ``ViewLocatorByte``     |               |
+---------------------+-------------------------+---------------+
| ``region``          | ``Region``              |               |
+---------------------+-------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``   |               |
+---------------------+-------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(ViewLocatorUInt16 source, Region region, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+-------------------------+---------------+
| Parameter           | Type                    | Description   |
+=====================+=========================+===============+
| ``source``          | ``ViewLocatorUInt16``   |               |
+---------------------+-------------------------+---------------+
| ``region``          | ``Region``              |               |
+---------------------+-------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``   |               |
+---------------------+-------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(ViewLocatorUInt32 source, Region region, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+-------------------------+---------------+
| Parameter           | Type                    | Description   |
+=====================+=========================+===============+
| ``source``          | ``ViewLocatorUInt32``   |               |
+---------------------+-------------------------+---------------+
| ``region``          | ``Region``              |               |
+---------------------+-------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``   |               |
+---------------------+-------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(ViewLocatorDouble source, Region region, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+-------------------------+---------------+
| Parameter           | Type                    | Description   |
+=====================+=========================+===============+
| ``source``          | ``ViewLocatorDouble``   |               |
+---------------------+-------------------------+---------------+
| ``region``          | ``Region``              |               |
+---------------------+-------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``   |               |
+---------------------+-------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(ViewLocatorRgbByte source, Region region, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+--------------------------+---------------+
| Parameter           | Type                     | Description   |
+=====================+==========================+===============+
| ``source``          | ``ViewLocatorRgbByte``   |               |
+---------------------+--------------------------+---------------+
| ``region``          | ``Region``               |               |
+---------------------+--------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``    |               |
+---------------------+--------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(ViewLocatorRgbUInt16 source, Region region, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+----------------------------+---------------+
| Parameter           | Type                       | Description   |
+=====================+============================+===============+
| ``source``          | ``ViewLocatorRgbUInt16``   |               |
+---------------------+----------------------------+---------------+
| ``region``          | ``Region``                 |               |
+---------------------+----------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``      |               |
+---------------------+----------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(ViewLocatorRgbUInt32 source, Region region, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+----------------------------+---------------+
| Parameter           | Type                       | Description   |
+=====================+============================+===============+
| ``source``          | ``ViewLocatorRgbUInt32``   |               |
+---------------------+----------------------------+---------------+
| ``region``          | ``Region``                 |               |
+---------------------+----------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``      |               |
+---------------------+----------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(ViewLocatorRgbDouble source, Region region, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+----------------------------+---------------+
| Parameter           | Type                       | Description   |
+=====================+============================+===============+
| ``source``          | ``ViewLocatorRgbDouble``   |               |
+---------------------+----------------------------+---------------+
| ``region``          | ``Region``                 |               |
+---------------------+----------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``      |               |
+---------------------+----------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(ViewLocatorRgbaByte source, Region region, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+---------------------------+---------------+
| Parameter           | Type                      | Description   |
+=====================+===========================+===============+
| ``source``          | ``ViewLocatorRgbaByte``   |               |
+---------------------+---------------------------+---------------+
| ``region``          | ``Region``                |               |
+---------------------+---------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``     |               |
+---------------------+---------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(ViewLocatorRgbaUInt16 source, Region region, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+-----------------------------+---------------+
| Parameter           | Type                        | Description   |
+=====================+=============================+===============+
| ``source``          | ``ViewLocatorRgbaUInt16``   |               |
+---------------------+-----------------------------+---------------+
| ``region``          | ``Region``                  |               |
+---------------------+-----------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``       |               |
+---------------------+-----------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(ViewLocatorRgbaUInt32 source, Region region, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+-----------------------------+---------------+
| Parameter           | Type                        | Description   |
+=====================+=============================+===============+
| ``source``          | ``ViewLocatorRgbaUInt32``   |               |
+---------------------+-----------------------------+---------------+
| ``region``          | ``Region``                  |               |
+---------------------+-----------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``       |               |
+---------------------+-----------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(ViewLocatorRgbaDouble source, Region region, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+-----------------------------+---------------+
| Parameter           | Type                        | Description   |
+=====================+=============================+===============+
| ``source``          | ``ViewLocatorRgbaDouble``   |               |
+---------------------+-----------------------------+---------------+
| ``region``          | ``Region``                  |               |
+---------------------+-----------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``       |               |
+---------------------+-----------------------------+---------------+

Method *DecodeMatrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixcodeList DecodeMatrixcode(View source, Region region, MatrixcodeDecoder configuration)``

The method **DecodeMatrixcode** has the following parameters:

+---------------------+-------------------------+---------------+
| Parameter           | Type                    | Description   |
+=====================+=========================+===============+
| ``source``          | ``View``                |               |
+---------------------+-------------------------+---------------+
| ``region``          | ``Region``              |               |
+---------------------+-------------------------+---------------+
| ``configuration``   | ``MatrixcodeDecoder``   |               |
+---------------------+-------------------------+---------------+
