Class *OcrCharacter*
--------------------

The ocr\_character as returned by the CharReader.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **OcrCharacter** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``IEquatableOcrCharacter``   |
+------------------------------+
| ``ISerializable``            |
+------------------------------+

The class **OcrCharacter** contains the following properties:

+-----------------+-------+-------+--------------------------------------------------------------------------------------------------------------------+
| Property        | Get   | Set   | Description                                                                                                        |
+=================+=======+=======+====================================================================================================================+
| ``Box``         | \*    |       | The bounding box.                                                                                                  |
+-----------------+-------+-------+--------------------------------------------------------------------------------------------------------------------+
| ``Character``   | \*    |       | The character.                                                                                                     |
+-----------------+-------+-------+--------------------------------------------------------------------------------------------------------------------+
| ``Score``       | \*    |       | The character score.                                                                                               |
+-----------------+-------+-------+--------------------------------------------------------------------------------------------------------------------+
| ``Valid``       | \*    |       | The valid field indicates if the characters are considered reliable in the sense of the minimum score criterion.   |
+-----------------+-------+-------+--------------------------------------------------------------------------------------------------------------------+

The class **OcrCharacter** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

Character characteristics are the bounding box, the character, the score quality and the reliability in the sense of the minimum score.

The following operators are implemented for a ocr\_character: operator == : comparison for equality. operator != : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *OcrCharacter*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``OcrCharacter()``

Standard constructor.

Constructors
~~~~~~~~~~~~

Constructor *OcrCharacter*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``OcrCharacter(BoxDouble box, System.String character, System.Double score, System.Boolean valid)``

Standard constructor.

The constructor has the following parameters:

+-----------------+----------------------+-------------------------------------------------------------------------------------------------------------------+
| Parameter       | Type                 | Description                                                                                                       |
+=================+======================+===================================================================================================================+
| ``box``         | ``BoxDouble``        | The bounding box.                                                                                                 |
+-----------------+----------------------+-------------------------------------------------------------------------------------------------------------------+
| ``character``   | ``System.String``    | The character.                                                                                                    |
+-----------------+----------------------+-------------------------------------------------------------------------------------------------------------------+
| ``score``       | ``System.Double``    | The character score                                                                                               |
+-----------------+----------------------+-------------------------------------------------------------------------------------------------------------------+
| ``valid``       | ``System.Boolean``   | The valid field indicates if the characters are considered reliable in the sense of the minimum score criterion   |
+-----------------+----------------------+-------------------------------------------------------------------------------------------------------------------+

Properties
~~~~~~~~~~

Property *Box*
^^^^^^^^^^^^^^

``BoxDouble Box``

The bounding box.

Property *Character*
^^^^^^^^^^^^^^^^^^^^

``System.String Character``

The character.

Property *Score*
^^^^^^^^^^^^^^^^

``System.Double Score``

The character score.

Property *Valid*
^^^^^^^^^^^^^^^^

``System.Boolean Valid``

The valid field indicates if the characters are considered reliable in the sense of the minimum score criterion.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
