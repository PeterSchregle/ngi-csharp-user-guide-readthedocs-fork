Class *Number*
--------------

The.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Number** implements the following interfaces:

+-----------------------------------------+
| Interface                               |
+=========================================+
| ``IEquatableNumberNumberTypeVariant``   |
+-----------------------------------------+
| ``ISerializable``                       |
+-----------------------------------------+

The class **Number** contains the following variant parameters:

+------------+-----------------------------------------+
| Variant    | Description                             |
+============+=========================================+
| ``Type``   | TODO no brief description for variant   |
+------------+-----------------------------------------+

The class **Number** contains the following properties:

+--------------------+-------+-------+---------------------------------------------------------------------------------+
| Property           | Get   | Set   | Description                                                                     |
+====================+=======+=======+=================================================================================+
| ``Value``          | \*    | \*    | The value.                                                                      |
+--------------------+-------+-------+---------------------------------------------------------------------------------+
| ``MinimalValue``   | \*    |       | Returns an exemplar of a value set to the minimal value of the type.            |
+--------------------+-------+-------+---------------------------------------------------------------------------------+
| ``MaximalValue``   | \*    |       | Returns an exemplar of a value set to the maximal value of the type.            |
+--------------------+-------+-------+---------------------------------------------------------------------------------+
| ``ZeroValue``      | \*    |       | Returns an exemplar of a value set to zero.                                     |
+--------------------+-------+-------+---------------------------------------------------------------------------------+
| ``OneValue``       | \*    |       | Returns an exemplar of a value set to one.                                      |
+--------------------+-------+-------+---------------------------------------------------------------------------------+
| ``MiddleValue``    | \*    |       | Returns an exemplar of a value set to the middle between minimum and maximum.   |
+--------------------+-------+-------+---------------------------------------------------------------------------------+

The class **Number** contains the following methods:

+------------------+---------------------------------------------------------+
| Method           | Description                                             |
+==================+=========================================================+
| ``ToString``     | Provide string representation for debugging purposes.   |
+------------------+---------------------------------------------------------+
| ``ParseValue``   | Parse a value from a string.                            |
+------------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Type*
^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Type** has the following types:

+--------------+
| Type         |
+==============+
| ``Byte``     |
+--------------+
| ``UInt16``   |
+--------------+
| ``UInt32``   |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Number*
^^^^^^^^^^^^^^^^^^^^

``Number()``

Constructs a number set to zero.

Constructors
~~~~~~~~~~~~

Constructor *Number*
^^^^^^^^^^^^^^^^^^^^

``Number(System.Object value)``

Constructs a number that is not initialized.

The constructor has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``value``   | ``System.Object``   |               |
+-------------+---------------------+---------------+

Properties
~~~~~~~~~~

Property *Value*
^^^^^^^^^^^^^^^^

``System.Object Value``

The value.

Property *MinimalValue*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Object MinimalValue``

Returns an exemplar of a value set to the minimal value of the type.

Property *MaximalValue*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Object MaximalValue``

Returns an exemplar of a value set to the maximal value of the type.

Property *ZeroValue*
^^^^^^^^^^^^^^^^^^^^

``System.Object ZeroValue``

Returns an exemplar of a value set to zero.

Property *OneValue*
^^^^^^^^^^^^^^^^^^^

``System.Object OneValue``

Returns an exemplar of a value set to one.

Property *MiddleValue*
^^^^^^^^^^^^^^^^^^^^^^

``System.Object MiddleValue``

Returns an exemplar of a value set to the middle between minimum and maximum.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Method *ParseValue*
^^^^^^^^^^^^^^^^^^^

``System.Object ParseValue(System.String value)``

Parse a value from a string.

The method **ParseValue** has the following parameters:

+-------------+---------------------+------------------------+
| Parameter   | Type                | Description            |
+=============+=====================+========================+
| ``value``   | ``System.String``   | The string to parse.   |
+-------------+---------------------+------------------------+
