Class *TweeningPower4*
----------------------

Calculating functor that calculates a power tweening.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **TweeningPower4** contains the following properties:

+-------------+-------+-------+---------------------+
| Property    | Get   | Set   | Description         |
+=============+=======+=======+=====================+
| ``Power``   | \*    |       | The kernel width.   |
+-------------+-------+-------+---------------------+

Description
~~~~~~~~~~~

The function used for the tweening is a curve built from tweening\_power\_1 on the left half and tweening\_power\_2 on the right half.

This function starts and ends with a slope of 0 and has a slope of 1 in the middle.

This functor is used for the preset functions.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *TweeningPower4*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TweeningPower4()``

Default Constructor.

Constructors
~~~~~~~~~~~~

Constructor *TweeningPower4*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TweeningPower4(System.Double power)``

Constructor.

The constructor has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``power``   | ``System.Double``   | The power.    |
+-------------+---------------------+---------------+

Properties
~~~~~~~~~~

Property *Power*
^^^^^^^^^^^^^^^^

``System.Double Power``

The kernel width.
