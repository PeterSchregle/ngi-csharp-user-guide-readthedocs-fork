Class *Mathematics*
-------------------

Mathematics functions.

**Namespace:** Ngi

**Module:** ImageProcessing

Description
~~~~~~~~~~~

The class contains a group of mathematics functions.

Static Methods
~~~~~~~~~~~~~~

Method *E*
^^^^^^^^^^

``System.Double E()``

Returns the constant e.

The constant e.

Method *Pi*
^^^^^^^^^^^

``System.Double Pi()``

Returns the constant pi.

The constant pi.

Method *Abs*
^^^^^^^^^^^^

``System.Double Abs(NumberByte x)``

Calculates the absolute value of a number.

The method **Abs** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The absolute value of a number.

Method *Abs*
^^^^^^^^^^^^

``System.Double Abs(NumberUInt16 x)``

Calculates the absolute value of a number.

The method **Abs** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The absolute value of a number.

Method *Abs*
^^^^^^^^^^^^

``System.Double Abs(NumberUInt32 x)``

Calculates the absolute value of a number.

The method **Abs** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The absolute value of a number.

Method *Abs*
^^^^^^^^^^^^

``System.Double Abs(NumberDouble x)``

Calculates the absolute value of a number.

The method **Abs** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The absolute value of a number.

Method *Abs*
^^^^^^^^^^^^

``System.Double Abs(Number x)``

Calculates the absolute value of a number.

The method **Abs** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+

The absolute value of a number.

Method *Sign*
^^^^^^^^^^^^^

``System.Double Sign(NumberByte x)``

Returns a number indicating the sign of a number.

The method **Sign** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The function returns -1.0 if the number is smaller than 0, 0 if the number equals 0 and 1.0 if the number is bigger than 0.A number indicating the sign of a number.

Method *Sign*
^^^^^^^^^^^^^

``System.Double Sign(NumberUInt16 x)``

Returns a number indicating the sign of a number.

The method **Sign** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The function returns -1.0 if the number is smaller than 0, 0 if the number equals 0 and 1.0 if the number is bigger than 0.A number indicating the sign of a number.

Method *Sign*
^^^^^^^^^^^^^

``System.Double Sign(NumberUInt32 x)``

Returns a number indicating the sign of a number.

The method **Sign** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The function returns -1.0 if the number is smaller than 0, 0 if the number equals 0 and 1.0 if the number is bigger than 0.A number indicating the sign of a number.

Method *Sign*
^^^^^^^^^^^^^

``System.Double Sign(NumberDouble x)``

Returns a number indicating the sign of a number.

The method **Sign** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The function returns -1.0 if the number is smaller than 0, 0 if the number equals 0 and 1.0 if the number is bigger than 0.A number indicating the sign of a number.

Method *Sign*
^^^^^^^^^^^^^

``System.Double Sign(Number x)``

Returns a number indicating the sign of a number.

The method **Sign** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+

The function returns -1.0 if the number is smaller than 0, 0 if the number equals 0 and 1.0 if the number is bigger than 0.A number indicating the sign of a number.

Method *Ceiling*
^^^^^^^^^^^^^^^^

``System.Double Ceiling(NumberByte x)``

Returns the smallest integer greater than or equal to the specified number.

The method **Ceiling** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The smallest integer greater than or equal to the specified number.

Method *Ceiling*
^^^^^^^^^^^^^^^^

``System.Double Ceiling(NumberUInt16 x)``

Returns the smallest integer greater than or equal to the specified number.

The method **Ceiling** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The smallest integer greater than or equal to the specified number.

Method *Ceiling*
^^^^^^^^^^^^^^^^

``System.Double Ceiling(NumberUInt32 x)``

Returns the smallest integer greater than or equal to the specified number.

The method **Ceiling** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The smallest integer greater than or equal to the specified number.

Method *Ceiling*
^^^^^^^^^^^^^^^^

``System.Double Ceiling(NumberDouble x)``

Returns the smallest integer greater than or equal to the specified number.

The method **Ceiling** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The smallest integer greater than or equal to the specified number.

Method *Ceiling*
^^^^^^^^^^^^^^^^

``System.Double Ceiling(Number x)``

Returns the smallest integer greater than or equal to the specified number.

The method **Ceiling** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+

The smallest integer greater than or equal to the specified number.

Method *Floor*
^^^^^^^^^^^^^^

``System.Double Floor(NumberByte x)``

Returns the largest integer less than or equal to the specified number.

The method **Floor** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The largest integer less than or equal to the specified number.

Method *Floor*
^^^^^^^^^^^^^^

``System.Double Floor(NumberUInt16 x)``

Returns the largest integer less than or equal to the specified number.

The method **Floor** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The largest integer less than or equal to the specified number.

Method *Floor*
^^^^^^^^^^^^^^

``System.Double Floor(NumberUInt32 x)``

Returns the largest integer less than or equal to the specified number.

The method **Floor** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The largest integer less than or equal to the specified number.

Method *Floor*
^^^^^^^^^^^^^^

``System.Double Floor(NumberDouble x)``

Returns the largest integer less than or equal to the specified number.

The method **Floor** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The largest integer less than or equal to the specified number.

Method *Floor*
^^^^^^^^^^^^^^

``System.Double Floor(Number x)``

Returns the largest integer less than or equal to the specified number.

The method **Floor** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+

The largest integer less than or equal to the specified number.

Method *Sin*
^^^^^^^^^^^^

``System.Double Sin(NumberByte x)``

Returns the sine of the specified angle.

The method **Sin** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The sine of the specified angle.

Method *Sin*
^^^^^^^^^^^^

``System.Double Sin(NumberUInt16 x)``

Returns the sine of the specified angle.

The method **Sin** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The sine of the specified angle.

Method *Sin*
^^^^^^^^^^^^

``System.Double Sin(NumberUInt32 x)``

Returns the sine of the specified angle.

The method **Sin** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The sine of the specified angle.

Method *Sin*
^^^^^^^^^^^^

``System.Double Sin(NumberDouble x)``

Returns the sine of the specified angle.

The method **Sin** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The sine of the specified angle.

Method *Sin*
^^^^^^^^^^^^

``System.Double Sin(Number x)``

Returns the sine of the specified angle.

The method **Sin** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+

The sine of the specified angle.

Method *Cos*
^^^^^^^^^^^^

``System.Double Cos(NumberByte x)``

Returns the cosine of the specified angle.

The method **Cos** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The cosine of the specified angle.

Method *Cos*
^^^^^^^^^^^^

``System.Double Cos(NumberUInt16 x)``

Returns the cosine of the specified angle.

The method **Cos** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The cosine of the specified angle.

Method *Cos*
^^^^^^^^^^^^

``System.Double Cos(NumberUInt32 x)``

Returns the cosine of the specified angle.

The method **Cos** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The cosine of the specified angle.

Method *Cos*
^^^^^^^^^^^^

``System.Double Cos(NumberDouble x)``

Returns the cosine of the specified angle.

The method **Cos** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The cosine of the specified angle.

Method *Cos*
^^^^^^^^^^^^

``System.Double Cos(Number x)``

Returns the cosine of the specified angle.

The method **Cos** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+

The cosine of the specified angle.

Method *Tan*
^^^^^^^^^^^^

``System.Double Tan(NumberByte x)``

Returns the tangent of the specified angle.

The method **Tan** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The tangent of the specified angle.

Method *Tan*
^^^^^^^^^^^^

``System.Double Tan(NumberUInt16 x)``

Returns the tangent of the specified angle.

The method **Tan** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The tangent of the specified angle.

Method *Tan*
^^^^^^^^^^^^

``System.Double Tan(NumberUInt32 x)``

Returns the tangent of the specified angle.

The method **Tan** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The tangent of the specified angle.

Method *Tan*
^^^^^^^^^^^^

``System.Double Tan(NumberDouble x)``

Returns the tangent of the specified angle.

The method **Tan** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The tangent of the specified angle.

Method *Tan*
^^^^^^^^^^^^

``System.Double Tan(Number x)``

Returns the tangent of the specified angle.

The method **Tan** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+

The tangent of the specified angle.

Method *Asin*
^^^^^^^^^^^^^

``System.Double Asin(NumberByte x)``

Returns the angle whose sine is the specified number.

The method **Asin** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The angle whose sine is the specified number.

Method *Asin*
^^^^^^^^^^^^^

``System.Double Asin(NumberUInt16 x)``

Returns the angle whose sine is the specified number.

The method **Asin** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The angle whose sine is the specified number.

Method *Asin*
^^^^^^^^^^^^^

``System.Double Asin(NumberUInt32 x)``

Returns the angle whose sine is the specified number.

The method **Asin** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The angle whose sine is the specified number.

Method *Asin*
^^^^^^^^^^^^^

``System.Double Asin(NumberDouble x)``

Returns the angle whose sine is the specified number.

The method **Asin** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The angle whose sine is the specified number.

Method *Asin*
^^^^^^^^^^^^^

``System.Double Asin(Number x)``

Returns the angle whose sine is the specified number.

The method **Asin** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+

The angle whose sine is the specified number.

Method *Acos*
^^^^^^^^^^^^^

``System.Double Acos(NumberByte x)``

Returns the angle whose cosine is the specified number.

The method **Acos** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The angle whose cosine is the specified number.

Method *Acos*
^^^^^^^^^^^^^

``System.Double Acos(NumberUInt16 x)``

Returns the angle whose cosine is the specified number.

The method **Acos** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The angle whose cosine is the specified number.

Method *Acos*
^^^^^^^^^^^^^

``System.Double Acos(NumberUInt32 x)``

Returns the angle whose cosine is the specified number.

The method **Acos** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The angle whose cosine is the specified number.

Method *Acos*
^^^^^^^^^^^^^

``System.Double Acos(NumberDouble x)``

Returns the angle whose cosine is the specified number.

The method **Acos** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The angle whose cosine is the specified number.

Method *Acos*
^^^^^^^^^^^^^

``System.Double Acos(Number x)``

Returns the angle whose cosine is the specified number.

The method **Acos** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+

The angle whose cosine is the specified number.

Method *Atan*
^^^^^^^^^^^^^

``System.Double Atan(NumberByte x)``

Returns the angle whose tangent is the specified number.

The method **Atan** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The angle whose tangent is the specified number.

Method *Atan*
^^^^^^^^^^^^^

``System.Double Atan(NumberUInt16 x)``

Returns the angle whose tangent is the specified number.

The method **Atan** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The angle whose tangent is the specified number.

Method *Atan*
^^^^^^^^^^^^^

``System.Double Atan(NumberUInt32 x)``

Returns the angle whose tangent is the specified number.

The method **Atan** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The angle whose tangent is the specified number.

Method *Atan*
^^^^^^^^^^^^^

``System.Double Atan(NumberDouble x)``

Returns the angle whose tangent is the specified number.

The method **Atan** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The angle whose tangent is the specified number.

Method *Atan*
^^^^^^^^^^^^^

``System.Double Atan(Number x)``

Returns the angle whose tangent is the specified number.

The method **Atan** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+

The angle whose tangent is the specified number.

Method *Sinh*
^^^^^^^^^^^^^

``System.Double Sinh(NumberByte x)``

Returns the hyperbolic sine of the specified angle.

The method **Sinh** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The hyperbolic sine of the specified angle.

Method *Sinh*
^^^^^^^^^^^^^

``System.Double Sinh(NumberUInt16 x)``

Returns the hyperbolic sine of the specified angle.

The method **Sinh** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The hyperbolic sine of the specified angle.

Method *Sinh*
^^^^^^^^^^^^^

``System.Double Sinh(NumberUInt32 x)``

Returns the hyperbolic sine of the specified angle.

The method **Sinh** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The hyperbolic sine of the specified angle.

Method *Sinh*
^^^^^^^^^^^^^

``System.Double Sinh(NumberDouble x)``

Returns the hyperbolic sine of the specified angle.

The method **Sinh** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The hyperbolic sine of the specified angle.

Method *Sinh*
^^^^^^^^^^^^^

``System.Double Sinh(Number x)``

Returns the hyperbolic sine of the specified angle.

The method **Sinh** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+

The hyperbolic sine of the specified angle.

Method *Cosh*
^^^^^^^^^^^^^

``System.Double Cosh(NumberByte x)``

Returns the hyperbolic cosine of the specified angle.

The method **Cosh** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The hyperbolic cosine of the specified angle.

Method *Cosh*
^^^^^^^^^^^^^

``System.Double Cosh(NumberUInt16 x)``

Returns the hyperbolic cosine of the specified angle.

The method **Cosh** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The hyperbolic cosine of the specified angle.

Method *Cosh*
^^^^^^^^^^^^^

``System.Double Cosh(NumberUInt32 x)``

Returns the hyperbolic cosine of the specified angle.

The method **Cosh** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The hyperbolic cosine of the specified angle.

Method *Cosh*
^^^^^^^^^^^^^

``System.Double Cosh(NumberDouble x)``

Returns the hyperbolic cosine of the specified angle.

The method **Cosh** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The hyperbolic cosine of the specified angle.

Method *Cosh*
^^^^^^^^^^^^^

``System.Double Cosh(Number x)``

Returns the hyperbolic cosine of the specified angle.

The method **Cosh** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+

The hyperbolic cosine of the specified angle.

Method *Tanh*
^^^^^^^^^^^^^

``System.Double Tanh(NumberByte x)``

Returns the hyperbolic tangent of the specified angle.

The method **Tanh** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The hyperbolic tangent of the specified angle.

Method *Tanh*
^^^^^^^^^^^^^

``System.Double Tanh(NumberUInt16 x)``

Returns the hyperbolic tangent of the specified angle.

The method **Tanh** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The hyperbolic tangent of the specified angle.

Method *Tanh*
^^^^^^^^^^^^^

``System.Double Tanh(NumberUInt32 x)``

Returns the hyperbolic tangent of the specified angle.

The method **Tanh** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The hyperbolic tangent of the specified angle.

Method *Tanh*
^^^^^^^^^^^^^

``System.Double Tanh(NumberDouble x)``

Returns the hyperbolic tangent of the specified angle.

The method **Tanh** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The hyperbolic tangent of the specified angle.

Method *Tanh*
^^^^^^^^^^^^^

``System.Double Tanh(Number x)``

Returns the hyperbolic tangent of the specified angle.

The method **Tanh** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+

The hyperbolic tangent of the specified angle.

Method *Exp*
^^^^^^^^^^^^

``System.Double Exp(NumberByte x)``

Returns Returns e raised to the specified power.

The method **Exp** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

e raised to the specified power.

Method *Exp*
^^^^^^^^^^^^

``System.Double Exp(NumberUInt16 x)``

Returns Returns e raised to the specified power.

The method **Exp** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

e raised to the specified power.

Method *Exp*
^^^^^^^^^^^^

``System.Double Exp(NumberUInt32 x)``

Returns Returns e raised to the specified power.

The method **Exp** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

e raised to the specified power.

Method *Exp*
^^^^^^^^^^^^

``System.Double Exp(NumberDouble x)``

Returns Returns e raised to the specified power.

The method **Exp** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

e raised to the specified power.

Method *Exp*
^^^^^^^^^^^^

``System.Double Exp(Number x)``

Returns Returns e raised to the specified power.

The method **Exp** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+

e raised to the specified power.

Method *Log*
^^^^^^^^^^^^

``System.Double Log(NumberByte x)``

Returns the natural (base e) logarithm of a specified number.

The method **Log** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The natural (base e) logarithm of a specified number.

Method *Log*
^^^^^^^^^^^^

``System.Double Log(NumberUInt16 x)``

Returns the natural (base e) logarithm of a specified number.

The method **Log** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The natural (base e) logarithm of a specified number.

Method *Log*
^^^^^^^^^^^^

``System.Double Log(NumberUInt32 x)``

Returns the natural (base e) logarithm of a specified number.

The method **Log** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The natural (base e) logarithm of a specified number.

Method *Log*
^^^^^^^^^^^^

``System.Double Log(NumberDouble x)``

Returns the natural (base e) logarithm of a specified number.

The method **Log** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The natural (base e) logarithm of a specified number.

Method *Log*
^^^^^^^^^^^^

``System.Double Log(Number x)``

Returns the natural (base e) logarithm of a specified number.

The method **Log** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+

The natural (base e) logarithm of a specified number.

Method *Log10*
^^^^^^^^^^^^^^

``System.Double Log10(NumberByte x)``

Returns the natural (base 10) logarithm of a specified number.

The method **Log10** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The natural (base 10) logarithm of a specified number.

Method *Log10*
^^^^^^^^^^^^^^

``System.Double Log10(NumberUInt16 x)``

Returns the natural (base 10) logarithm of a specified number.

The method **Log10** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The natural (base 10) logarithm of a specified number.

Method *Log10*
^^^^^^^^^^^^^^

``System.Double Log10(NumberUInt32 x)``

Returns the natural (base 10) logarithm of a specified number.

The method **Log10** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The natural (base 10) logarithm of a specified number.

Method *Log10*
^^^^^^^^^^^^^^

``System.Double Log10(NumberDouble x)``

Returns the natural (base 10) logarithm of a specified number.

The method **Log10** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The natural (base 10) logarithm of a specified number.

Method *Log10*
^^^^^^^^^^^^^^

``System.Double Log10(Number x)``

Returns the natural (base 10) logarithm of a specified number.

The method **Log10** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+

The natural (base 10) logarithm of a specified number.

Method *Sqrt*
^^^^^^^^^^^^^

``System.Double Sqrt(NumberByte x)``

Returns the square root of a specified number.

The method **Sqrt** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The square root of a specified number.

Method *Sqrt*
^^^^^^^^^^^^^

``System.Double Sqrt(NumberUInt16 x)``

Returns the square root of a specified number.

The method **Sqrt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The square root of a specified number.

Method *Sqrt*
^^^^^^^^^^^^^

``System.Double Sqrt(NumberUInt32 x)``

Returns the square root of a specified number.

The method **Sqrt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The square root of a specified number.

Method *Sqrt*
^^^^^^^^^^^^^

``System.Double Sqrt(NumberDouble x)``

Returns the square root of a specified number.

The method **Sqrt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The square root of a specified number.

Method *Sqrt*
^^^^^^^^^^^^^

``System.Double Sqrt(Number x)``

Returns the square root of a specified number.

The method **Sqrt** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+

The square root of a specified number.

Method *Truncate*
^^^^^^^^^^^^^^^^^

``System.Double Truncate(NumberByte x)``

Calculates the integral part of a number.

The method **Truncate** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The integral part of a number.

Method *Truncate*
^^^^^^^^^^^^^^^^^

``System.Double Truncate(NumberUInt16 x)``

Calculates the integral part of a number.

The method **Truncate** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The integral part of a number.

Method *Truncate*
^^^^^^^^^^^^^^^^^

``System.Double Truncate(NumberUInt32 x)``

Calculates the integral part of a number.

The method **Truncate** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The integral part of a number.

Method *Truncate*
^^^^^^^^^^^^^^^^^

``System.Double Truncate(NumberDouble x)``

Calculates the integral part of a number.

The method **Truncate** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The integral part of a number.

Method *Truncate*
^^^^^^^^^^^^^^^^^

``System.Double Truncate(Number x)``

Calculates the integral part of a number.

The method **Truncate** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+

The integral part of a number.

Method *Fraction*
^^^^^^^^^^^^^^^^^

``System.Double Fraction(NumberByte x)``

Calculates the fractional part of a specified number.

The method **Fraction** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The fractional part of a specified number.

Method *Fraction*
^^^^^^^^^^^^^^^^^

``System.Double Fraction(NumberUInt16 x)``

Calculates the fractional part of a specified number.

The method **Fraction** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The fractional part of a specified number.

Method *Fraction*
^^^^^^^^^^^^^^^^^

``System.Double Fraction(NumberUInt32 x)``

Calculates the fractional part of a specified number.

The method **Fraction** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The fractional part of a specified number.

Method *Fraction*
^^^^^^^^^^^^^^^^^

``System.Double Fraction(NumberDouble x)``

Calculates the fractional part of a specified number.

The method **Fraction** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The fractional part of a specified number.

Method *Fraction*
^^^^^^^^^^^^^^^^^

``System.Double Fraction(Number x)``

Calculates the fractional part of a specified number.

The method **Fraction** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+

The fractional part of a specified number.

Method *DegreesToRadians*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double DegreesToRadians(NumberByte x)``

Convert an angle from degrees to radians.

The method **DegreesToRadians** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

An angle in degrees is converted to radians by multiplying it with pi/180.

The angle in radians.

Method *DegreesToRadians*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double DegreesToRadians(NumberUInt16 x)``

Convert an angle from degrees to radians.

The method **DegreesToRadians** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

An angle in degrees is converted to radians by multiplying it with pi/180.

The angle in radians.

Method *DegreesToRadians*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double DegreesToRadians(NumberUInt32 x)``

Convert an angle from degrees to radians.

The method **DegreesToRadians** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

An angle in degrees is converted to radians by multiplying it with pi/180.

The angle in radians.

Method *DegreesToRadians*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double DegreesToRadians(NumberDouble x)``

Convert an angle from degrees to radians.

The method **DegreesToRadians** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

An angle in degrees is converted to radians by multiplying it with pi/180.

The angle in radians.

Method *DegreesToRadians*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double DegreesToRadians(Number x)``

Convert an angle from degrees to radians.

The method **DegreesToRadians** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+

An angle in degrees is converted to radians by multiplying it with pi/180.

The angle in radians.

Method *RadiansToDegrees*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double RadiansToDegrees(NumberByte x)``

Convert an angle from radians to degrees.

The method **RadiansToDegrees** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

An angle in radians is converted to degrees by multiplying it with 180/pi.

The angle in degrees.

Method *RadiansToDegrees*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double RadiansToDegrees(NumberUInt16 x)``

Convert an angle from radians to degrees.

The method **RadiansToDegrees** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

An angle in radians is converted to degrees by multiplying it with 180/pi.

The angle in degrees.

Method *RadiansToDegrees*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double RadiansToDegrees(NumberUInt32 x)``

Convert an angle from radians to degrees.

The method **RadiansToDegrees** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

An angle in radians is converted to degrees by multiplying it with 180/pi.

The angle in degrees.

Method *RadiansToDegrees*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double RadiansToDegrees(NumberDouble x)``

Convert an angle from radians to degrees.

The method **RadiansToDegrees** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

An angle in radians is converted to degrees by multiplying it with 180/pi.

The angle in degrees.

Method *RadiansToDegrees*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double RadiansToDegrees(Number x)``

Convert an angle from radians to degrees.

The method **RadiansToDegrees** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+

An angle in radians is converted to degrees by multiplying it with 180/pi.

The angle in degrees.

Method *Round*
^^^^^^^^^^^^^^

``System.Double Round(NumberByte x)``

Round a number to an integer.

The method **Round** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The rounded number.

Method *Round*
^^^^^^^^^^^^^^

``System.Double Round(NumberUInt16 x)``

Round a number to an integer.

The method **Round** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The rounded number.

Method *Round*
^^^^^^^^^^^^^^

``System.Double Round(NumberUInt32 x)``

Round a number to an integer.

The method **Round** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The rounded number.

Method *Round*
^^^^^^^^^^^^^^

``System.Double Round(NumberDouble x)``

Round a number to an integer.

The method **Round** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The rounded number.

Method *Round*
^^^^^^^^^^^^^^

``System.Double Round(Number x)``

Round a number to an integer.

The method **Round** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+

The rounded number.

Method *Min*
^^^^^^^^^^^^

``System.Double Min(NumberByte x, NumberByte y)``

Returns the smaller of two numbers.

The method **Min** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+
| ``y``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The smaller of two numbers.

Method *Min*
^^^^^^^^^^^^

``System.Double Min(NumberUInt16 x, NumberUInt16 y)``

Returns the smaller of two numbers.

The method **Min** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The smaller of two numbers.

Method *Min*
^^^^^^^^^^^^

``System.Double Min(NumberUInt32 x, NumberUInt32 y)``

Returns the smaller of two numbers.

The method **Min** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The smaller of two numbers.

Method *Min*
^^^^^^^^^^^^

``System.Double Min(NumberDouble x, NumberDouble y)``

Returns the smaller of two numbers.

The method **Min** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The smaller of two numbers.

Method *Min*
^^^^^^^^^^^^

``System.Double Min(Number x, Number y)``

Returns the smaller of two numbers.

The method **Min** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+
| ``y``       | ``Number``   |               |
+-------------+--------------+---------------+

The smaller of two numbers.

Method *Max*
^^^^^^^^^^^^

``System.Double Max(NumberByte x, NumberByte y)``

Returns the bigger of two numbers.

The method **Max** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+
| ``y``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The bigger of two numbers.

Method *Max*
^^^^^^^^^^^^

``System.Double Max(NumberUInt16 x, NumberUInt16 y)``

Returns the bigger of two numbers.

The method **Max** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The bigger of two numbers.

Method *Max*
^^^^^^^^^^^^

``System.Double Max(NumberUInt32 x, NumberUInt32 y)``

Returns the bigger of two numbers.

The method **Max** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The bigger of two numbers.

Method *Max*
^^^^^^^^^^^^

``System.Double Max(NumberDouble x, NumberDouble y)``

Returns the bigger of two numbers.

The method **Max** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The bigger of two numbers.

Method *Max*
^^^^^^^^^^^^

``System.Double Max(Number x, Number y)``

Returns the bigger of two numbers.

The method **Max** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+
| ``y``       | ``Number``   |               |
+-------------+--------------+---------------+

The bigger of two numbers.

Method *Add*
^^^^^^^^^^^^

``System.Double Add(NumberByte x, NumberByte y)``

Adds two numbers.

The method **Add** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+
| ``y``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The sum of two numbers.

Method *Add*
^^^^^^^^^^^^

``System.Double Add(NumberUInt16 x, NumberUInt16 y)``

Adds two numbers.

The method **Add** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The sum of two numbers.

Method *Add*
^^^^^^^^^^^^

``System.Double Add(NumberUInt32 x, NumberUInt32 y)``

Adds two numbers.

The method **Add** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The sum of two numbers.

Method *Add*
^^^^^^^^^^^^

``System.Double Add(NumberDouble x, NumberDouble y)``

Adds two numbers.

The method **Add** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The sum of two numbers.

Method *Add*
^^^^^^^^^^^^

``System.Double Add(Number x, Number y)``

Adds two numbers.

The method **Add** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+
| ``y``       | ``Number``   |               |
+-------------+--------------+---------------+

The sum of two numbers.

Method *Subtract*
^^^^^^^^^^^^^^^^^

``System.Double Subtract(NumberByte x, NumberByte y)``

Subtracts second number from first number.

The method **Subtract** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+
| ``y``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The difference of two numbers.

Method *Subtract*
^^^^^^^^^^^^^^^^^

``System.Double Subtract(NumberUInt16 x, NumberUInt16 y)``

Subtracts second number from first number.

The method **Subtract** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The difference of two numbers.

Method *Subtract*
^^^^^^^^^^^^^^^^^

``System.Double Subtract(NumberUInt32 x, NumberUInt32 y)``

Subtracts second number from first number.

The method **Subtract** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The difference of two numbers.

Method *Subtract*
^^^^^^^^^^^^^^^^^

``System.Double Subtract(NumberDouble x, NumberDouble y)``

Subtracts second number from first number.

The method **Subtract** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The difference of two numbers.

Method *Subtract*
^^^^^^^^^^^^^^^^^

``System.Double Subtract(Number x, Number y)``

Subtracts second number from first number.

The method **Subtract** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+
| ``y``       | ``Number``   |               |
+-------------+--------------+---------------+

The difference of two numbers.

Method *Multiply*
^^^^^^^^^^^^^^^^^

``System.Double Multiply(NumberByte x, NumberByte y)``

Multiplies two numbers.

The method **Multiply** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+
| ``y``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The product of two numbers.

Method *Multiply*
^^^^^^^^^^^^^^^^^

``System.Double Multiply(NumberUInt16 x, NumberUInt16 y)``

Multiplies two numbers.

The method **Multiply** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The product of two numbers.

Method *Multiply*
^^^^^^^^^^^^^^^^^

``System.Double Multiply(NumberUInt32 x, NumberUInt32 y)``

Multiplies two numbers.

The method **Multiply** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The product of two numbers.

Method *Multiply*
^^^^^^^^^^^^^^^^^

``System.Double Multiply(NumberDouble x, NumberDouble y)``

Multiplies two numbers.

The method **Multiply** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The product of two numbers.

Method *Multiply*
^^^^^^^^^^^^^^^^^

``System.Double Multiply(Number x, Number y)``

Multiplies two numbers.

The method **Multiply** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+
| ``y``       | ``Number``   |               |
+-------------+--------------+---------------+

The product of two numbers.

Method *Divide*
^^^^^^^^^^^^^^^

``System.Double Divide(NumberByte x, NumberByte y)``

Divides first number through second number.

The method **Divide** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+
| ``y``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The quotient of two numbers.

Method *Divide*
^^^^^^^^^^^^^^^

``System.Double Divide(NumberUInt16 x, NumberUInt16 y)``

Divides first number through second number.

The method **Divide** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The quotient of two numbers.

Method *Divide*
^^^^^^^^^^^^^^^

``System.Double Divide(NumberUInt32 x, NumberUInt32 y)``

Divides first number through second number.

The method **Divide** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The quotient of two numbers.

Method *Divide*
^^^^^^^^^^^^^^^

``System.Double Divide(NumberDouble x, NumberDouble y)``

Divides first number through second number.

The method **Divide** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The quotient of two numbers.

Method *Divide*
^^^^^^^^^^^^^^^

``System.Double Divide(Number x, Number y)``

Divides first number through second number.

The method **Divide** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+
| ``y``       | ``Number``   |               |
+-------------+--------------+---------------+

The quotient of two numbers.

Method *Pow*
^^^^^^^^^^^^

``System.Double Pow(NumberByte x, NumberByte y)``

Returns a specified number raised to the specified power.

The method **Pow** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+
| ``y``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

A specified number raised to the specified power.

Method *Pow*
^^^^^^^^^^^^

``System.Double Pow(NumberUInt16 x, NumberUInt16 y)``

Returns a specified number raised to the specified power.

The method **Pow** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

A specified number raised to the specified power.

Method *Pow*
^^^^^^^^^^^^

``System.Double Pow(NumberUInt32 x, NumberUInt32 y)``

Returns a specified number raised to the specified power.

The method **Pow** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

A specified number raised to the specified power.

Method *Pow*
^^^^^^^^^^^^

``System.Double Pow(NumberDouble x, NumberDouble y)``

Returns a specified number raised to the specified power.

The method **Pow** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

A specified number raised to the specified power.

Method *Pow*
^^^^^^^^^^^^

``System.Double Pow(Number x, Number y)``

Returns a specified number raised to the specified power.

The method **Pow** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+
| ``y``       | ``Number``   |               |
+-------------+--------------+---------------+

A specified number raised to the specified power.

Method *Atan2*
^^^^^^^^^^^^^^

``System.Double Atan2(NumberByte x, NumberByte y)``

Returns the angle whose tangent is the quotient of two specified numbers.

The method **Atan2** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``x``       | ``NumberByte``   |               |
+-------------+------------------+---------------+
| ``y``       | ``NumberByte``   |               |
+-------------+------------------+---------------+

The angle whose tangent is the quotient of two specified numbers.

Method *Atan2*
^^^^^^^^^^^^^^

``System.Double Atan2(NumberUInt16 x, NumberUInt16 y)``

Returns the angle whose tangent is the quotient of two specified numbers.

The method **Atan2** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``NumberUInt16``   |               |
+-------------+--------------------+---------------+

The angle whose tangent is the quotient of two specified numbers.

Method *Atan2*
^^^^^^^^^^^^^^

``System.Double Atan2(NumberUInt32 x, NumberUInt32 y)``

Returns the angle whose tangent is the quotient of two specified numbers.

The method **Atan2** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``NumberUInt32``   |               |
+-------------+--------------------+---------------+

The angle whose tangent is the quotient of two specified numbers.

Method *Atan2*
^^^^^^^^^^^^^^

``System.Double Atan2(NumberDouble x, NumberDouble y)``

Returns the angle whose tangent is the quotient of two specified numbers.

The method **Atan2** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``NumberDouble``   |               |
+-------------+--------------------+---------------+

The angle whose tangent is the quotient of two specified numbers.

Method *Atan2*
^^^^^^^^^^^^^^

``System.Double Atan2(Number x, Number y)``

Returns the angle whose tangent is the quotient of two specified numbers.

The method **Atan2** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``x``       | ``Number``   |               |
+-------------+--------------+---------------+
| ``y``       | ``Number``   |               |
+-------------+--------------+---------------+

The angle whose tangent is the quotient of two specified numbers.
