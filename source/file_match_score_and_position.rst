Class *MatchScoreAndPosition*
-----------------------------

A class holding a match score and the associated match position and angle.

**Namespace:** Ngi

**Module:** TemplateMatching

The class **MatchScoreAndPosition** implements the following interfaces:

+---------------------------------------+
| Interface                             |
+=======================================+
| ``IEquatableMatchScoreAndPosition``   |
+---------------------------------------+
| ``ISerializable``                     |
+---------------------------------------+
| ``INotifyPropertyChanged``            |
+---------------------------------------+

The class **MatchScoreAndPosition** contains the following properties:

+----------------+-------+-------+-----------------------+
| Property       | Get   | Set   | Description           |
+================+=======+=======+=======================+
| ``Score``      | \*    | \*    | The match score.      |
+----------------+-------+-------+-----------------------+
| ``Position``   | \*    | \*    | The match position.   |
+----------------+-------+-------+-----------------------+
| ``Angle``      | \*    | \*    | The match angle.      |
+----------------+-------+-------+-----------------------+

The class **MatchScoreAndPosition** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

A match score is a normalized score on a scale from 0.0 to 1.0, where 1.0 specifies a perfect match and 0.0 is no match at all.

A match position is specified with a point<double> in 2D euclidean space.

A match angle is specified in radians with an double between 0 and 2\*pi.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *MatchScoreAndPosition*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPosition()``

Constructs a match\_score\_and\_position at origin (0, 0).

Constructors
~~~~~~~~~~~~

Constructor *MatchScoreAndPosition*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPosition(System.Double score, PointDouble position, System.Double angle)``

Constructs a match\_score\_and\_position with specific values.

The constructor has the following parameters:

+----------------+---------------------+-----------------+
| Parameter      | Type                | Description     |
+================+=====================+=================+
| ``score``      | ``System.Double``   | The score.      |
+----------------+---------------------+-----------------+
| ``position``   | ``PointDouble``     | The position.   |
+----------------+---------------------+-----------------+
| ``angle``      | ``System.Double``   | The angle.      |
+----------------+---------------------+-----------------+

Properties
~~~~~~~~~~

Property *Score*
^^^^^^^^^^^^^^^^

``System.Double Score``

The match score.

Property *Position*
^^^^^^^^^^^^^^^^^^^

``PointDouble Position``

The match position.

Property *Angle*
^^^^^^^^^^^^^^^^

``System.Double Angle``

The match angle.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO no brief description for variant

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO no description for variant
