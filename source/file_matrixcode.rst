Class *Matrixcode*
------------------

A matrixcode result.

**Namespace:** Ngi

**Module:** BarcodeMatrixcode

The class **Matrixcode** implements the following interfaces:

+----------------------------+
| Interface                  |
+============================+
| ``IEquatableMatrixcode``   |
+----------------------------+
| ``ISerializable``          |
+----------------------------+

The class **Matrixcode** contains the following properties:

+---------------------+-------+-------+---------------------------------------+
| Property            | Get   | Set   | Description                           |
+=====================+=======+=======+=======================================+
| ``Text``            | \*    |       | The decoded text of the matrixcode.   |
+---------------------+-------+-------+---------------------------------------+
| ``Position``        | \*    |       | The position of the matrixcode.       |
+---------------------+-------+-------+---------------------------------------+
| ``SymbologyId``     | \*    |       | The symbology of the matrixcode.      |
+---------------------+-------+-------+---------------------------------------+
| ``SymbologyName``   | \*    |       | The symbology of the matrixcode.      |
+---------------------+-------+-------+---------------------------------------+

The class **Matrixcode** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

The class **Matrixcode** contains the following enumerations:

+---------------------+------------------------------+
| Enumeration         | Description                  |
+=====================+==============================+
| ``SymbologyType``   | TODO documentation missing   |
+---------------------+------------------------------+

Description
~~~~~~~~~~~

A matrixcode result consists of the read matrixcode, the symbology and the location of the matrixcode.

The following operators are implemented for a matrixcode: operator == : comparison for equality. operator != : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Matrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^

``Matrixcode()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *Matrixcode*
^^^^^^^^^^^^^^^^^^^^^^^^

``Matrixcode(System.String text, PointListPointDouble position, Matrixcode.SymbologyType symbologyId)``

Constructor.

The constructor has the following parameters:

+-------------------+--------------------------------+---------------------------------------+
| Parameter         | Type                           | Description                           |
+===================+================================+=======================================+
| ``text``          | ``System.String``              | The decoded text of the matrixcode.   |
+-------------------+--------------------------------+---------------------------------------+
| ``position``      | ``PointListPointDouble``       | The position of the matrixcode.       |
+-------------------+--------------------------------+---------------------------------------+
| ``symbologyId``   | ``Matrixcode.SymbologyType``   | The symbology of the matrixcode.      |
+-------------------+--------------------------------+---------------------------------------+

Properties
~~~~~~~~~~

Property *Text*
^^^^^^^^^^^^^^^

``System.String Text``

The decoded text of the matrixcode.

Property *Position*
^^^^^^^^^^^^^^^^^^^

``PointListPointDouble Position``

The position of the matrixcode.

Property *SymbologyId*
^^^^^^^^^^^^^^^^^^^^^^

``Matrixcode.SymbologyType SymbologyId``

The symbology of the matrixcode.

Property *SymbologyName*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.String SymbologyName``

The symbology of the matrixcode.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Enumerations
~~~~~~~~~~~~

Enumeration *SymbologyType*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum SymbologyType``

TODO documentation missing

The enumeration **SymbologyType** has the following constants:

+------------------+----------+---------------+
| Name             | Value    | Description   |
+==================+==========+===============+
| ``unknown``      | ``0``    |               |
+------------------+----------+---------------+
| ``aztec``        | ``1``    |               |
+------------------+----------+---------------+
| ``dataMatrix``   | ``8``    |               |
+------------------+----------+---------------+
| ``pdf417``       | ``13``   |               |
+------------------+----------+---------------+
| ``qrCode``       | ``14``   |               |
+------------------+----------+---------------+

::

    enum SymbologyType
    {
      unknown = 0,
      aztec = 1,
      dataMatrix = 8,
      pdf417 = 13,
      qrCode = 14,
    };

TODO documentation missing
