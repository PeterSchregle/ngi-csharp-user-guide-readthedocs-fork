Class *IoResource*
------------------

The base class for files, serial ports and sockets.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **IoResource** contains the following properties:

+--------------+-------+-------+---------------------------------+
| Property     | Get   | Set   | Description                     |
+==============+=======+=======+=================================+
| ``IsOpen``   | \*    |       | True, if the reource is open.   |
+--------------+-------+-------+---------------------------------+

The class **IoResource** contains the following methods:

+-------------+------------------------------------+
| Method      | Description                        |
+=============+====================================+
| ``Open``    | Open the io\_resource.             |
+-------------+------------------------------------+
| ``Close``   | Close the io\_resource.            |
+-------------+------------------------------------+
| ``Read``    | Read data from the io\_resource.   |
+-------------+------------------------------------+
| ``Write``   | Write data to the io\_resource.    |
+-------------+------------------------------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *IsOpen*
^^^^^^^^^^^^^^^^^

``System.Boolean IsOpen``

True, if the reource is open.

Methods
~~~~~~~

Method *Open*
^^^^^^^^^^^^^

``void Open()``

Open the io\_resource.

Method *Close*
^^^^^^^^^^^^^^

``void Close()``

Close the io\_resource.

Method *Read*
^^^^^^^^^^^^^

``DataList Read(System.Int32 numberOfBytesToRead)``

Read data from the io\_resource.

The method **Read** has the following parameters:

+---------------------------+--------------------+----------------------------------------+
| Parameter                 | Type               | Description                            |
+===========================+====================+========================================+
| ``numberOfBytesToRead``   | ``System.Int32``   | The maximum number of bytes to read.   |
+---------------------------+--------------------+----------------------------------------+

The data.

Method *Write*
^^^^^^^^^^^^^^

``System.Int32 Write(DataList data)``

Write data to the io\_resource.

The method **Write** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``data``    | ``DataList``   | The data.     |
+-------------+----------------+---------------+
