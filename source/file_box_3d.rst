Class *Box3d*
-------------

A box in three-dimensional euclidean space.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Box3d** implements the following interfaces:

+---------------------------------------------+
| Interface                                   |
+=============================================+
| ``IEquatableBox3dBox3dCoordinateVariant``   |
+---------------------------------------------+
| ``ISerializable``                           |
+---------------------------------------------+
| ``INotifyPropertyChanged``                  |
+---------------------------------------------+

The class **Box3d** contains the following variant parameters:

+------------------+-----------------------------------------+
| Variant          | Description                             |
+==================+=========================================+
| ``Coordinate``   | TODO no brief description for variant   |
+------------------+-----------------------------------------+

The class **Box3d** contains the following properties:

+--------------+-------+-------+--------------------------+
| Property     | Get   | Set   | Description              |
+==============+=======+=======+==========================+
| ``Origin``   | \*    | \*    | The origin of the box.   |
+--------------+-------+-------+--------------------------+
| ``Size``     | \*    | \*    | The size of the box.     |
+--------------+-------+-------+--------------------------+

Description
~~~~~~~~~~~

The box is axis- parallel.

Variants
~~~~~~~~

Variant *Coordinate*
^^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Coordinate** has the following types:

+--------------+
| Type         |
+==============+
| ``Int32``    |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Box3d*
^^^^^^^^^^^^^^^^^^^

``Box3d()``

Standard constructor.

Construct a box from a point and a size.

Constructors
~~~~~~~~~~~~

Constructor *Box3d*
^^^^^^^^^^^^^^^^^^^

``Box3d(Point3d origin, Vector3d size)``

Standard constructor.

The constructor has the following parameters:

+--------------+----------------+---------------+
| Parameter    | Type           | Description   |
+==============+================+===============+
| ``origin``   | ``Point3d``    |               |
+--------------+----------------+---------------+
| ``size``     | ``Vector3d``   |               |
+--------------+----------------+---------------+

Construct a box from two points.

Constructor *Box3d*
^^^^^^^^^^^^^^^^^^^

``Box3d(Point3d first, Point3d second)``

Standard constructor.

The constructor has the following parameters:

+--------------+---------------+---------------+
| Parameter    | Type          | Description   |
+==============+===============+===============+
| ``first``    | ``Point3d``   |               |
+--------------+---------------+---------------+
| ``second``   | ``Point3d``   |               |
+--------------+---------------+---------------+

Construct a box from two point coordinates and two size values.

Constructor *Box3d*
^^^^^^^^^^^^^^^^^^^

``Box3d(System.Object x, System.Object y, System.Object z, System.Object dx, System.Object dy, System.Object dz)``

Standard converting constructor.

The constructor has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``x``       | ``System.Object``   |               |
+-------------+---------------------+---------------+
| ``y``       | ``System.Object``   |               |
+-------------+---------------------+---------------+
| ``z``       | ``System.Object``   |               |
+-------------+---------------------+---------------+
| ``dx``      | ``System.Object``   |               |
+-------------+---------------------+---------------+
| ``dy``      | ``System.Object``   |               |
+-------------+---------------------+---------------+
| ``dz``      | ``System.Object``   |               |
+-------------+---------------------+---------------+

Construct a box from a point and a size.

Properties
~~~~~~~~~~

Property *Origin*
^^^^^^^^^^^^^^^^^

``Point3d Origin``

The origin of the box.

Property *Size*
^^^^^^^^^^^^^^^

``Vector3d Size``

The size of the box.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO documentation missing

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO documentation missing
