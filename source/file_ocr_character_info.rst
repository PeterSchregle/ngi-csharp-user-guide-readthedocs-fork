Class *OcrCharacterInfo*
------------------------

The ocr\_character\_info as returned by the CharReader.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **OcrCharacterInfo** implements the following interfaces:

+----------------------------------+
| Interface                        |
+==================================+
| ``IEquatableOcrCharacterInfo``   |
+----------------------------------+
| ``ISerializable``                |
+----------------------------------+

The class **OcrCharacterInfo** contains the following properties:

+--------------------+-------+-------+----------------------+
| Property           | Get   | Set   | Description          |
+====================+=======+=======+======================+
| ``Characters``     | \*    |       | The characters.      |
+--------------------+-------+-------+----------------------+
| ``Text``           | \*    |       | The text.            |
+--------------------+-------+-------+----------------------+
| ``AverageScore``   | \*    |       | The average score.   |
+--------------------+-------+-------+----------------------+

The class **OcrCharacterInfo** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

This class combines an ocr\_character\_list with the global ocr text and the average score.

The following operators are implemented for a ocr\_character\_info: operator == : comparison for equality. operator != : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *OcrCharacterInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``OcrCharacterInfo()``

Standard constructor.

Constructors
~~~~~~~~~~~~

Constructor *OcrCharacterInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``OcrCharacterInfo(OcrCharacterList characters, System.String text, System.Double averageScore)``

Standard constructor.

The constructor has the following parameters:

+--------------------+------------------------+----------------------+
| Parameter          | Type                   | Description          |
+====================+========================+======================+
| ``characters``     | ``OcrCharacterList``   | The characters.      |
+--------------------+------------------------+----------------------+
| ``text``           | ``System.String``      |                      |
+--------------------+------------------------+----------------------+
| ``averageScore``   | ``System.Double``      | The average score.   |
+--------------------+------------------------+----------------------+

Properties
~~~~~~~~~~

Property *Characters*
^^^^^^^^^^^^^^^^^^^^^

``OcrCharacterList Characters``

The characters.

Property *Text*
^^^^^^^^^^^^^^^

``System.String Text``

The text.

Property *AverageScore*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Double AverageScore``

The average score.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
