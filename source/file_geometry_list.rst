Class *GeometryList*
--------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **GeometryList** implements the following interfaces:

+---------------------+
| Interface           |
+=====================+
| ``IListGeometry``   |
+---------------------+

The class **GeometryList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **GeometryList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *GeometryList*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``GeometryList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``Geometry [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``GeometryEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(Geometry item)``

The method **Add** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``item``    | ``Geometry``   |               |
+-------------+----------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(Geometry item)``

The method **Contains** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``item``    | ``Geometry``   |               |
+-------------+----------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(Geometry item)``

The method **Remove** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``item``    | ``Geometry``   |               |
+-------------+----------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(Geometry item)``

The method **IndexOf** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``item``    | ``Geometry``   |               |
+-------------+----------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, Geometry item)``

The method **Insert** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``item``    | ``Geometry``       |               |
+-------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
