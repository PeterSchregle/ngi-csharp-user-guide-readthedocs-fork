Class *SplitAndCombineAlgorithms*
---------------------------------

Image segmentation functions.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **SplitAndCombineAlgorithms** contains the following enumerations:

+-----------------------------+------------------------------+
| Enumeration                 | Description                  |
+=============================+==============================+
| ``SplitCombineDirection``   | TODO documentation missing   |
+-----------------------------+------------------------------+

Description
~~~~~~~~~~~

The class contains functions to split and combine images in the horizontal, vertical or planar directions.

Static Methods
~~~~~~~~~~~~~~

Method *Split*
^^^^^^^^^^^^^^

``void Split(ViewLocatorByte source, ViewLocatorByte first, ViewLocatorByte second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``ViewLocatorByte``                                   |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``ViewLocatorByte``                                   |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``ViewLocatorByte``                                   |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Split*
^^^^^^^^^^^^^^

``void Split(ViewLocatorUInt16 source, ViewLocatorUInt16 first, ViewLocatorUInt16 second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``ViewLocatorUInt16``                                 |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``ViewLocatorUInt16``                                 |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``ViewLocatorUInt16``                                 |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Split*
^^^^^^^^^^^^^^

``void Split(ViewLocatorUInt32 source, ViewLocatorUInt32 first, ViewLocatorUInt32 second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``ViewLocatorUInt32``                                 |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``ViewLocatorUInt32``                                 |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``ViewLocatorUInt32``                                 |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Split*
^^^^^^^^^^^^^^

``void Split(ViewLocatorDouble source, ViewLocatorDouble first, ViewLocatorDouble second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``ViewLocatorDouble``                                 |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``ViewLocatorDouble``                                 |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``ViewLocatorDouble``                                 |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Split*
^^^^^^^^^^^^^^

``void Split(ViewLocatorRgbByte source, ViewLocatorRgbByte first, ViewLocatorRgbByte second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``ViewLocatorRgbByte``                                |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``ViewLocatorRgbByte``                                |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``ViewLocatorRgbByte``                                |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Split*
^^^^^^^^^^^^^^

``void Split(ViewLocatorRgbUInt16 source, ViewLocatorRgbUInt16 first, ViewLocatorRgbUInt16 second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``ViewLocatorRgbUInt16``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``ViewLocatorRgbUInt16``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``ViewLocatorRgbUInt16``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Split*
^^^^^^^^^^^^^^

``void Split(ViewLocatorRgbUInt32 source, ViewLocatorRgbUInt32 first, ViewLocatorRgbUInt32 second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``ViewLocatorRgbUInt32``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``ViewLocatorRgbUInt32``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``ViewLocatorRgbUInt32``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Split*
^^^^^^^^^^^^^^

``void Split(ViewLocatorRgbDouble source, ViewLocatorRgbDouble first, ViewLocatorRgbDouble second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``ViewLocatorRgbDouble``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``ViewLocatorRgbDouble``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``ViewLocatorRgbDouble``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Split*
^^^^^^^^^^^^^^

``void Split(ViewLocatorRgbaByte source, ViewLocatorRgbaByte first, ViewLocatorRgbaByte second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``ViewLocatorRgbaByte``                               |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``ViewLocatorRgbaByte``                               |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``ViewLocatorRgbaByte``                               |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Split*
^^^^^^^^^^^^^^

``void Split(ViewLocatorRgbaUInt16 source, ViewLocatorRgbaUInt16 first, ViewLocatorRgbaUInt16 second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``ViewLocatorRgbaUInt16``                             |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``ViewLocatorRgbaUInt16``                             |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``ViewLocatorRgbaUInt16``                             |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Split*
^^^^^^^^^^^^^^

``void Split(ViewLocatorRgbaUInt32 source, ViewLocatorRgbaUInt32 first, ViewLocatorRgbaUInt32 second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``ViewLocatorRgbaUInt32``                             |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``ViewLocatorRgbaUInt32``                             |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``ViewLocatorRgbaUInt32``                             |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Split*
^^^^^^^^^^^^^^

``void Split(ViewLocatorRgbaDouble source, ViewLocatorRgbaDouble first, ViewLocatorRgbaDouble second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``ViewLocatorRgbaDouble``                             |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``ViewLocatorRgbaDouble``                             |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``ViewLocatorRgbaDouble``                             |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Split*
^^^^^^^^^^^^^^

``void Split(ViewLocatorHlsByte source, ViewLocatorHlsByte first, ViewLocatorHlsByte second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``ViewLocatorHlsByte``                                |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``ViewLocatorHlsByte``                                |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``ViewLocatorHlsByte``                                |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Split*
^^^^^^^^^^^^^^

``void Split(ViewLocatorHlsUInt16 source, ViewLocatorHlsUInt16 first, ViewLocatorHlsUInt16 second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``ViewLocatorHlsUInt16``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``ViewLocatorHlsUInt16``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``ViewLocatorHlsUInt16``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Split*
^^^^^^^^^^^^^^

``void Split(ViewLocatorHlsDouble source, ViewLocatorHlsDouble first, ViewLocatorHlsDouble second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``ViewLocatorHlsDouble``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``ViewLocatorHlsDouble``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``ViewLocatorHlsDouble``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Split*
^^^^^^^^^^^^^^

``void Split(ViewLocatorHsiByte source, ViewLocatorHsiByte first, ViewLocatorHsiByte second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``ViewLocatorHsiByte``                                |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``ViewLocatorHsiByte``                                |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``ViewLocatorHsiByte``                                |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Split*
^^^^^^^^^^^^^^

``void Split(ViewLocatorHsiUInt16 source, ViewLocatorHsiUInt16 first, ViewLocatorHsiUInt16 second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``ViewLocatorHsiUInt16``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``ViewLocatorHsiUInt16``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``ViewLocatorHsiUInt16``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Split*
^^^^^^^^^^^^^^

``void Split(ViewLocatorHsiDouble source, ViewLocatorHsiDouble first, ViewLocatorHsiDouble second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``ViewLocatorHsiDouble``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``ViewLocatorHsiDouble``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``ViewLocatorHsiDouble``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Split*
^^^^^^^^^^^^^^

``void Split(ViewLocatorLabByte source, ViewLocatorLabByte first, ViewLocatorLabByte second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``ViewLocatorLabByte``                                |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``ViewLocatorLabByte``                                |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``ViewLocatorLabByte``                                |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Split*
^^^^^^^^^^^^^^

``void Split(ViewLocatorLabUInt16 source, ViewLocatorLabUInt16 first, ViewLocatorLabUInt16 second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``ViewLocatorLabUInt16``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``ViewLocatorLabUInt16``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``ViewLocatorLabUInt16``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Split*
^^^^^^^^^^^^^^

``void Split(ViewLocatorLabDouble source, ViewLocatorLabDouble first, ViewLocatorLabDouble second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``ViewLocatorLabDouble``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``ViewLocatorLabDouble``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``ViewLocatorLabDouble``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Split*
^^^^^^^^^^^^^^

``void Split(ViewLocatorXyzByte source, ViewLocatorXyzByte first, ViewLocatorXyzByte second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``ViewLocatorXyzByte``                                |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``ViewLocatorXyzByte``                                |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``ViewLocatorXyzByte``                                |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Split*
^^^^^^^^^^^^^^

``void Split(ViewLocatorXyzUInt16 source, ViewLocatorXyzUInt16 first, ViewLocatorXyzUInt16 second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``ViewLocatorXyzUInt16``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``ViewLocatorXyzUInt16``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``ViewLocatorXyzUInt16``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Split*
^^^^^^^^^^^^^^

``void Split(ViewLocatorXyzDouble source, ViewLocatorXyzDouble first, ViewLocatorXyzDouble second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``ViewLocatorXyzDouble``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``ViewLocatorXyzDouble``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``ViewLocatorXyzDouble``                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Split*
^^^^^^^^^^^^^^

``void Split(View source, View first, View second, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Split** has the following parameters:

+-----------------+-------------------------------------------------------+---------------+
| Parameter       | Type                                                  | Description   |
+=================+=======================================================+===============+
| ``source``      | ``View``                                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``first``       | ``View``                                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``second``      | ``View``                                              |               |
+-----------------+-------------------------------------------------------+---------------+
| ``direction``   | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-----------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(ViewLocatorByte first, ViewLocatorByte second, ViewLocatorByte destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``ViewLocatorByte``                                   |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``ViewLocatorByte``                                   |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``ViewLocatorByte``                                   |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(ViewLocatorUInt16 first, ViewLocatorUInt16 second, ViewLocatorUInt16 destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``ViewLocatorUInt16``                                 |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``ViewLocatorUInt16``                                 |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``ViewLocatorUInt16``                                 |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(ViewLocatorUInt32 first, ViewLocatorUInt32 second, ViewLocatorUInt32 destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``ViewLocatorUInt32``                                 |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``ViewLocatorUInt32``                                 |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``ViewLocatorUInt32``                                 |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(ViewLocatorDouble first, ViewLocatorDouble second, ViewLocatorDouble destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``ViewLocatorDouble``                                 |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``ViewLocatorDouble``                                 |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``ViewLocatorDouble``                                 |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(ViewLocatorRgbByte first, ViewLocatorRgbByte second, ViewLocatorRgbByte destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``ViewLocatorRgbByte``                                |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``ViewLocatorRgbByte``                                |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``ViewLocatorRgbByte``                                |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(ViewLocatorRgbUInt16 first, ViewLocatorRgbUInt16 second, ViewLocatorRgbUInt16 destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``ViewLocatorRgbUInt16``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``ViewLocatorRgbUInt16``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``ViewLocatorRgbUInt16``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(ViewLocatorRgbUInt32 first, ViewLocatorRgbUInt32 second, ViewLocatorRgbUInt32 destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``ViewLocatorRgbUInt32``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``ViewLocatorRgbUInt32``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``ViewLocatorRgbUInt32``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(ViewLocatorRgbDouble first, ViewLocatorRgbDouble second, ViewLocatorRgbDouble destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``ViewLocatorRgbDouble``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``ViewLocatorRgbDouble``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``ViewLocatorRgbDouble``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(ViewLocatorRgbaByte first, ViewLocatorRgbaByte second, ViewLocatorRgbaByte destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``ViewLocatorRgbaByte``                               |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``ViewLocatorRgbaByte``                               |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``ViewLocatorRgbaByte``                               |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(ViewLocatorRgbaUInt16 first, ViewLocatorRgbaUInt16 second, ViewLocatorRgbaUInt16 destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``ViewLocatorRgbaUInt16``                             |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``ViewLocatorRgbaUInt16``                             |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``ViewLocatorRgbaUInt16``                             |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(ViewLocatorRgbaUInt32 first, ViewLocatorRgbaUInt32 second, ViewLocatorRgbaUInt32 destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``ViewLocatorRgbaUInt32``                             |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``ViewLocatorRgbaUInt32``                             |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``ViewLocatorRgbaUInt32``                             |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(ViewLocatorRgbaDouble first, ViewLocatorRgbaDouble second, ViewLocatorRgbaDouble destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``ViewLocatorRgbaDouble``                             |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``ViewLocatorRgbaDouble``                             |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``ViewLocatorRgbaDouble``                             |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(ViewLocatorHlsByte first, ViewLocatorHlsByte second, ViewLocatorHlsByte destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``ViewLocatorHlsByte``                                |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``ViewLocatorHlsByte``                                |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``ViewLocatorHlsByte``                                |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(ViewLocatorHlsUInt16 first, ViewLocatorHlsUInt16 second, ViewLocatorHlsUInt16 destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``ViewLocatorHlsUInt16``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``ViewLocatorHlsUInt16``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``ViewLocatorHlsUInt16``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(ViewLocatorHlsDouble first, ViewLocatorHlsDouble second, ViewLocatorHlsDouble destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``ViewLocatorHlsDouble``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``ViewLocatorHlsDouble``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``ViewLocatorHlsDouble``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(ViewLocatorHsiByte first, ViewLocatorHsiByte second, ViewLocatorHsiByte destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``ViewLocatorHsiByte``                                |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``ViewLocatorHsiByte``                                |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``ViewLocatorHsiByte``                                |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(ViewLocatorHsiUInt16 first, ViewLocatorHsiUInt16 second, ViewLocatorHsiUInt16 destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``ViewLocatorHsiUInt16``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``ViewLocatorHsiUInt16``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``ViewLocatorHsiUInt16``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(ViewLocatorHsiDouble first, ViewLocatorHsiDouble second, ViewLocatorHsiDouble destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``ViewLocatorHsiDouble``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``ViewLocatorHsiDouble``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``ViewLocatorHsiDouble``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(ViewLocatorLabByte first, ViewLocatorLabByte second, ViewLocatorLabByte destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``ViewLocatorLabByte``                                |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``ViewLocatorLabByte``                                |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``ViewLocatorLabByte``                                |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(ViewLocatorLabUInt16 first, ViewLocatorLabUInt16 second, ViewLocatorLabUInt16 destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``ViewLocatorLabUInt16``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``ViewLocatorLabUInt16``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``ViewLocatorLabUInt16``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(ViewLocatorLabDouble first, ViewLocatorLabDouble second, ViewLocatorLabDouble destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``ViewLocatorLabDouble``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``ViewLocatorLabDouble``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``ViewLocatorLabDouble``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(ViewLocatorXyzByte first, ViewLocatorXyzByte second, ViewLocatorXyzByte destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``ViewLocatorXyzByte``                                |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``ViewLocatorXyzByte``                                |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``ViewLocatorXyzByte``                                |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(ViewLocatorXyzUInt16 first, ViewLocatorXyzUInt16 second, ViewLocatorXyzUInt16 destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``ViewLocatorXyzUInt16``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``ViewLocatorXyzUInt16``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``ViewLocatorXyzUInt16``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(ViewLocatorXyzDouble first, ViewLocatorXyzDouble second, ViewLocatorXyzDouble destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``ViewLocatorXyzDouble``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``ViewLocatorXyzDouble``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``ViewLocatorXyzDouble``                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *Combine*
^^^^^^^^^^^^^^^^

``void Combine(View first, View second, View destination, SplitAndCombineAlgorithms.SplitCombineDirection direction)``

Extract the specified channel from the source.

The method **Combine** has the following parameters:

+-------------------+-------------------------------------------------------+---------------+
| Parameter         | Type                                                  | Description   |
+===================+=======================================================+===============+
| ``first``         | ``View``                                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``second``        | ``View``                                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``destination``   | ``View``                                              |               |
+-------------------+-------------------------------------------------------+---------------+
| ``direction``     | ``SplitAndCombineAlgorithms.SplitCombineDirection``   |               |
+-------------------+-------------------------------------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Enumerations
~~~~~~~~~~~~

Enumeration *SplitCombineDirection*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum SplitCombineDirection``

TODO documentation missing

The enumeration **SplitCombineDirection** has the following constants:

+------------------+---------+---------------+
| Name             | Value   | Description   |
+==================+=========+===============+
| ``horizontal``   | ``0``   |               |
+------------------+---------+---------------+
| ``vertical``     | ``1``   |               |
+------------------+---------+---------------+
| ``planar``       | ``2``   |               |
+------------------+---------+---------------+

::

    enum SplitCombineDirection
    {
      horizontal = 0,
      vertical = 1,
      planar = 2,
    };

TODO documentation missing
