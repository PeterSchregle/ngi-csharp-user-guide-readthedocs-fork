Class *SectorEnumerator*
------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **SectorEnumerator** implements the following interfaces:

+-------------------------+
| Interface               |
+=========================+
| ``IEnumerator``         |
+-------------------------+
| ``IEnumeratorSector``   |
+-------------------------+

The class **SectorEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **SectorEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Sector Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
