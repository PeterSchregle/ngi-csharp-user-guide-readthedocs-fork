Class *StereoAlgorithms*
------------------------

Radiometric stereo functions.

**Namespace:** Ngi

**Module:** ImageProcessing

Description
~~~~~~~~~~~

The class contains functions for radiometric stereo.

Static Methods
~~~~~~~~~~~~~~

Method *RadiometricStereo*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``RadiometricStereoResult RadiometricStereo(ImageListImageByte images, Vector3dListVector3dDouble illuminationDirections, System.Double reflectionThreshold)``

Perform a radiometric stereo on a set of images.

The method **RadiometricStereo** has the following parameters:

+------------------------------+----------------------------------+---------------+
| Parameter                    | Type                             | Description   |
+==============================+==================================+===============+
| ``images``                   | ``ImageListImageByte``           |               |
+------------------------------+----------------------------------+---------------+
| ``illuminationDirections``   | ``Vector3dListVector3dDouble``   |               |
+------------------------------+----------------------------------+---------------+
| ``reflectionThreshold``      | ``System.Double``                |               |
+------------------------------+----------------------------------+---------------+

Method *RadiometricStereo*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``RadiometricStereoResult RadiometricStereo(ImageListImageUInt16 images, Vector3dListVector3dDouble illuminationDirections, System.Double reflectionThreshold)``

Perform a radiometric stereo on a set of images.

The method **RadiometricStereo** has the following parameters:

+------------------------------+----------------------------------+---------------+
| Parameter                    | Type                             | Description   |
+==============================+==================================+===============+
| ``images``                   | ``ImageListImageUInt16``         |               |
+------------------------------+----------------------------------+---------------+
| ``illuminationDirections``   | ``Vector3dListVector3dDouble``   |               |
+------------------------------+----------------------------------+---------------+
| ``reflectionThreshold``      | ``System.Double``                |               |
+------------------------------+----------------------------------+---------------+

Method *RadiometricStereo*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``RadiometricStereoResult RadiometricStereo(ImageListImageUInt32 images, Vector3dListVector3dDouble illuminationDirections, System.Double reflectionThreshold)``

Perform a radiometric stereo on a set of images.

The method **RadiometricStereo** has the following parameters:

+------------------------------+----------------------------------+---------------+
| Parameter                    | Type                             | Description   |
+==============================+==================================+===============+
| ``images``                   | ``ImageListImageUInt32``         |               |
+------------------------------+----------------------------------+---------------+
| ``illuminationDirections``   | ``Vector3dListVector3dDouble``   |               |
+------------------------------+----------------------------------+---------------+
| ``reflectionThreshold``      | ``System.Double``                |               |
+------------------------------+----------------------------------+---------------+

Method *RadiometricStereo*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``RadiometricStereoResult RadiometricStereo(ImageListImageDouble images, Vector3dListVector3dDouble illuminationDirections, System.Double reflectionThreshold)``

Perform a radiometric stereo on a set of images.

The method **RadiometricStereo** has the following parameters:

+------------------------------+----------------------------------+---------------+
| Parameter                    | Type                             | Description   |
+==============================+==================================+===============+
| ``images``                   | ``ImageListImageDouble``         |               |
+------------------------------+----------------------------------+---------------+
| ``illuminationDirections``   | ``Vector3dListVector3dDouble``   |               |
+------------------------------+----------------------------------+---------------+
| ``reflectionThreshold``      | ``System.Double``                |               |
+------------------------------+----------------------------------+---------------+

Method *RadiometricStereo*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``RadiometricStereoResult RadiometricStereo(ImageList images, Vector3dListVector3dDouble illuminationDirections, System.Double reflectionThreshold)``

Perform a radiometric stereo on a set of images.

The method **RadiometricStereo** has the following parameters:

+------------------------------+----------------------------------+---------------+
| Parameter                    | Type                             | Description   |
+==============================+==================================+===============+
| ``images``                   | ``ImageList``                    |               |
+------------------------------+----------------------------------+---------------+
| ``illuminationDirections``   | ``Vector3dListVector3dDouble``   |               |
+------------------------------+----------------------------------+---------------+
| ``reflectionThreshold``      | ``System.Double``                |               |
+------------------------------+----------------------------------+---------------+

Method *GaussianCurvatureFromGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble GaussianCurvatureFromGradient(ImageDouble gradientX, ImageDouble gradientY)``

The method **GaussianCurvatureFromGradient** has the following parameters:

+-----------------+-------------------+---------------+
| Parameter       | Type              | Description   |
+=================+===================+===============+
| ``gradientX``   | ``ImageDouble``   |               |
+-----------------+-------------------+---------------+
| ``gradientY``   | ``ImageDouble``   |               |
+-----------------+-------------------+---------------+

Method *MeanCurvatureFromGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble MeanCurvatureFromGradient(ImageDouble gradientX, ImageDouble gradientY)``

The method **MeanCurvatureFromGradient** has the following parameters:

+-----------------+-------------------+---------------+
| Parameter       | Type              | Description   |
+=================+===================+===============+
| ``gradientX``   | ``ImageDouble``   |               |
+-----------------+-------------------+---------------+
| ``gradientY``   | ``ImageDouble``   |               |
+-----------------+-------------------+---------------+
