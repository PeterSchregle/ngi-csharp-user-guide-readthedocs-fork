Class *WidgetView*
------------------

A widget\_view displays the contents of a view on a surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetView** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetView** contains the following variant parameters:

+------------+-----------------------------------------+
| Variant    | Description                             |
+============+=========================================+
| ``View``   | TODO no brief description for variant   |
+------------+-----------------------------------------+

The class **WidgetView** contains the following properties:

+------------+-------+-------+---------------------------+
| Property   | Get   | Set   | Description               |
+============+=======+=======+===========================+
| ``View``   | \*    | \*    | The view of the widget.   |
+------------+-------+-------+---------------------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_view. These settings affect the look and the behavior of the widget\_view.

The widget\_view supports automatic layout via the widget\_layoutable base class. The size of the widget is determined by the size of the view which is displayed.

The widget\_view supports interactivity via the widget\_interactive base class, i.e. it can be selected, move around. In general it can react on mouse and keyboard interactions. This widget supports layout.

Variants
~~~~~~~~

Variant *View*
^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **View** has the following types:

+-----------------------------+
| Type                        |
+=============================+
| ``ViewLocatorByte``         |
+-----------------------------+
| ``ViewLocatorUInt16``       |
+-----------------------------+
| ``ViewLocatorUInt32``       |
+-----------------------------+
| ``ViewLocatorDouble``       |
+-----------------------------+
| ``ViewLocatorRgbByte``      |
+-----------------------------+
| ``ViewLocatorRgbUInt16``    |
+-----------------------------+
| ``ViewLocatorRgbUInt32``    |
+-----------------------------+
| ``ViewLocatorRgbDouble``    |
+-----------------------------+
| ``ViewLocatorRgbaByte``     |
+-----------------------------+
| ``ViewLocatorRgbaUInt16``   |
+-----------------------------+
| ``ViewLocatorRgbaUInt32``   |
+-----------------------------+
| ``ViewLocatorRgbaDouble``   |
+-----------------------------+
| ``ViewLocatorHlsByte``      |
+-----------------------------+
| ``ViewLocatorHlsUInt16``    |
+-----------------------------+
| ``ViewLocatorHlsDouble``    |
+-----------------------------+
| ``ViewLocatorHsiByte``      |
+-----------------------------+
| ``ViewLocatorHsiUInt16``    |
+-----------------------------+
| ``ViewLocatorHsiDouble``    |
+-----------------------------+
| ``ViewLocatorLabByte``      |
+-----------------------------+
| ``ViewLocatorLabUInt16``    |
+-----------------------------+
| ``ViewLocatorLabDouble``    |
+-----------------------------+
| ``ViewLocatorXyzByte``      |
+-----------------------------+
| ``ViewLocatorXyzUInt16``    |
+-----------------------------+
| ``ViewLocatorXyzDouble``    |
+-----------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetView*
^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetView()``

Default constructor.

The default constructor creates a widget with default settings.

Properties
~~~~~~~~~~

Property *View*
^^^^^^^^^^^^^^^

``View View``

The view of the widget.
