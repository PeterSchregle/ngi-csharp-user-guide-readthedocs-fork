Class *Ray*
-----------

An infinite ray in two-dimensional euclidean space.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Ray** implements the following interfaces:

+-----------------------------------------+
| Interface                               |
+=========================================+
| ``IEquatableRayRayCoordinateVariant``   |
+-----------------------------------------+
| ``ISerializable``                       |
+-----------------------------------------+
| ``INotifyPropertyChanged``              |
+-----------------------------------------+

The class **Ray** contains the following variant parameters:

+------------------+-----------------------------------------+
| Variant          | Description                             |
+==================+=========================================+
| ``Coordinate``   | TODO no brief description for variant   |
+------------------+-----------------------------------------+

The class **Ray** contains the following properties:

+----------------------+-------+-------+----------------------------------------------------------+
| Property             | Get   | Set   | Description                                              |
+======================+=======+=======+==========================================================+
| ``Origin``           | \*    | \*    | The starting point of the ray.                           |
+----------------------+-------+-------+----------------------------------------------------------+
| ``Direction``        | \*    | \*    | The direction of the ray.                                |
+----------------------+-------+-------+----------------------------------------------------------+
| ``Dir``              | \*    |       | The direction of the ray (for backward compatibility).   |
+----------------------+-------+-------+----------------------------------------------------------+
| ``StartDirection``   | \*    |       | Get the start direction.                                 |
+----------------------+-------+-------+----------------------------------------------------------+

The class **Ray** contains the following methods:

+---------------------------+---------------------------------------------------------------+
| Method                    | Description                                                   |
+===========================+===============================================================+
| ``GetPointOrientation``   | Returns the orientation of the point relative to this line.   |
+---------------------------+---------------------------------------------------------------+
| ``Move``                  | Move ray.                                                     |
+---------------------------+---------------------------------------------------------------+
| ``ToString``              | Provide string representation for debugging purposes.         |
+---------------------------+---------------------------------------------------------------+

Description
~~~~~~~~~~~

The ray starts at it's origin and extends in it's positive direction into infinity.

A ray can be seen as a point combined with a direction vector in two-dimensional space.

The following operations are implemented: operator == : comparison for equality. operator != : comparison for inequality. operator- : negation, directly implemented. operator+ : addition, implemented through boost::additive operator-= : subtraction, directly implemented. operator- : subtraction, implemented through boost::additive

Variants
~~~~~~~~

Variant *Coordinate*
^^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Coordinate** has the following types:

+--------------+
| Type         |
+==============+
| ``Int32``    |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Ray*
^^^^^^^^^^^^^^^^^

``Ray()``

Construct a ray from a point and a direction vector.

If no parameters are passed, this constructs a horizontal ray starting at the origin and extending to the right in direction of the positive x-axis.

Constructors
~~~~~~~~~~~~

Constructor *Ray*
^^^^^^^^^^^^^^^^^

``Ray(Point origin, Direction direction)``

Construct a ray from a point and a direction vector of a different type.

The constructor has the following parameters:

+-----------------+-----------------+-----------------------------+
| Parameter       | Type            | Description                 |
+=================+=================+=============================+
| ``origin``      | ``Point``       | The origin of the ray.      |
+-----------------+-----------------+-----------------------------+
| ``direction``   | ``Direction``   | The direction of the ray.   |
+-----------------+-----------------+-----------------------------+

Properties
~~~~~~~~~~

Property *Origin*
^^^^^^^^^^^^^^^^^

``Point Origin``

The starting point of the ray.

Property *Direction*
^^^^^^^^^^^^^^^^^^^^

``Direction Direction``

The direction of the ray.

Property *Dir*
^^^^^^^^^^^^^^

``Direction Dir``

The direction of the ray (for backward compatibility).

Property *StartDirection*
^^^^^^^^^^^^^^^^^^^^^^^^^

``Direction StartDirection``

Get the start direction.

The start direction is opposite to the direction.

Methods
~~~~~~~

Method *GetPointOrientation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double GetPointOrientation(Point pt)``

Returns the orientation of the point relative to this line.

The method **GetPointOrientation** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``pt``      | ``Point``   |               |
+-------------+-------------+---------------+

Returns negative values if the point is to the left, positive values if the point is to the right and zero if the point is on the line. Left and right are used in the sense as defined in a right handed coordinate system; in reality nGI uses a left handed coordinate system and therefore left and right are reversed.

Method *Move*
^^^^^^^^^^^^^

``Ray Move(Vector movement)``

Move ray.

The method **Move** has the following parameters:

+----------------+--------------+--------------------+
| Parameter      | Type         | Description        |
+================+==============+====================+
| ``movement``   | ``Vector``   | Movement vector.   |
+----------------+--------------+--------------------+

Moves a ray by a vector.

The moved ray.

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO documentation missing

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO documentation missing
