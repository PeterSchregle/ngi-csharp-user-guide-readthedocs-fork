Class *BrushList*
-----------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **BrushList** implements the following interfaces:

+--------------------------------------------+
| Interface                                  |
+============================================+
| ``IListBrushListSolidColorBrushVariant``   |
+--------------------------------------------+

The class **BrushList** contains the following variant parameters:

+-----------------------+-----------------------------------------+
| Variant               | Description                             |
+=======================+=========================================+
| ``SolidColorBrush``   | TODO no brief description for variant   |
+-----------------------+-----------------------------------------+

The class **BrushList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **BrushList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *SolidColorBrush*
^^^^^^^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **SolidColorBrush** has the following types:

+-----------------------------+
| Type                        |
+=============================+
| ``SolidColorBrushByte``     |
+-----------------------------+
| ``SolidColorBrushUInt16``   |
+-----------------------------+
| ``SolidColorBrushUInt32``   |
+-----------------------------+
| ``SolidColorBrushDouble``   |
+-----------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *BrushList*
^^^^^^^^^^^^^^^^^^^^^^^

``BrushList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``SolidColorBrush [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``BrushEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(SolidColorBrush item)``

The method **Add** has the following parameters:

+-------------+-----------------------+---------------+
| Parameter   | Type                  | Description   |
+=============+=======================+===============+
| ``item``    | ``SolidColorBrush``   |               |
+-------------+-----------------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(SolidColorBrush item)``

The method **Contains** has the following parameters:

+-------------+-----------------------+---------------+
| Parameter   | Type                  | Description   |
+=============+=======================+===============+
| ``item``    | ``SolidColorBrush``   |               |
+-------------+-----------------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(SolidColorBrush item)``

The method **Remove** has the following parameters:

+-------------+-----------------------+---------------+
| Parameter   | Type                  | Description   |
+=============+=======================+===============+
| ``item``    | ``SolidColorBrush``   |               |
+-------------+-----------------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(SolidColorBrush item)``

The method **IndexOf** has the following parameters:

+-------------+-----------------------+---------------+
| Parameter   | Type                  | Description   |
+=============+=======================+===============+
| ``item``    | ``SolidColorBrush``   |               |
+-------------+-----------------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, SolidColorBrush item)``

The method **Insert** has the following parameters:

+-------------+-----------------------+---------------+
| Parameter   | Type                  | Description   |
+=============+=======================+===============+
| ``index``   | ``System.Int32``      |               |
+-------------+-----------------------+---------------+
| ``item``    | ``SolidColorBrush``   |               |
+-------------+-----------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
