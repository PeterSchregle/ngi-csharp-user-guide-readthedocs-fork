Class *WidgetRay*
-----------------

A widget\_ray displays a ray on a surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetRay** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetRay** contains the following properties:

+----------------+-------+-------+-------------------------------+
| Property       | Get   | Set   | Description                   |
+================+=======+=======+===============================+
| ``Position``   | \*    | \*    | The position of the widget.   |
+----------------+-------+-------+-------------------------------+
| ``Pen``        | \*    | \*    | The pen of the widget.        |
+----------------+-------+-------+-------------------------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_ray. These settings affect the look and the behavior of the widget\_ray.

The widget\_ray supports interactivity via the widget\_interactive base class, i.e. it can be selected, move around. In general it can react on mouse and keyboard interactions. A widget\_ray has interactive widget capabilities.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetRay*
^^^^^^^^^^^^^^^^^^^^^^^

``WidgetRay()``

Default constructor.

The default constructor creates a widget\_ray with default settings. By default the ray is located at point 0,0 and points to the right, and the pen is one-pixel wide black.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``RayDouble Position``

The position of the widget.

Property *Pen*
^^^^^^^^^^^^^^

``PenByte Pen``

The pen of the widget.
