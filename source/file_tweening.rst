Class *Tweening*
----------------

The base class for tweening functors.

**Namespace:** Ngi

**Module:** ImageProcessing

Description
~~~~~~~~~~~

All tweening functors must derive from this class.
