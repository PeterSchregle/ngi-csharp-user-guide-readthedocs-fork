Class *LicenseException*
------------------------

Exception class for license exceptions.

**Namespace:** Ngi

**Module:**

Description
~~~~~~~~~~~

License exceptions are used in the wrapper generator, when functions from modules are called, which are not licensed. The license check is made in the static constructor of the related C# classes.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *LicenseException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``LicenseException()``

Constructor.

Constructors
~~~~~~~~~~~~

Constructor *LicenseException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``LicenseException(System.String message)``

Constructor.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+
