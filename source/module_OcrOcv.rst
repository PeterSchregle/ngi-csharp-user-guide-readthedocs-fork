Module OcrOcv
=============

The module **OcrOcv** contains the following static classes:

+-----------+--------------------------------------------------+
| Class     | Description                                      |
+===========+==================================================+
| ``Ocr``   | OCR (optical character recognition) functions.   |
+-----------+--------------------------------------------------+

The module **OcrOcv** contains the following classes:

+-----------------+---------------+
| Class           | Description   |
+=================+===============+
| ``OcrReader``   |               |
+-----------------+---------------+
