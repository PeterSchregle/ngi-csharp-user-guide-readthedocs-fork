Class *WidgetMouse*
-------------------

A widget\_mouse provides interaction with the mouse on a surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetMouse** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetMouse** contains the following properties:

+----------------+-------+-------+-------------------------------+
| Property       | Get   | Set   | Description                   |
+================+=======+=======+===============================+
| ``Position``   | \*    | \*    | The position of the widget.   |
+----------------+-------+-------+-------------------------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_mouse. These settings affect the look and the behavior of the widget\_mouse.

The widget\_mouse supports interactivity via the widget\_interactive base class, i.e. it can be selected, move around. In general it can react on mouse and keyboard interactions. A widget\_mouse has interactive widget capabilities.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetMouse*
^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetMouse()``

Default constructor.

The default constructor creates a widget with default settings. By default the position is at zero and the outline is one-pixel wide black.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``PointDouble Position``

The position of the widget.
