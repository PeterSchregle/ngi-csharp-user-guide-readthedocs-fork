Class *RotationMatrix*
----------------------

A matrix useful for affine 2D rotation.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **RotationMatrix** implements the following interfaces:

+--------------------------------+
| Interface                      |
+================================+
| ``IEquatableRotationMatrix``   |
+--------------------------------+
| ``ISerializable``              |
+--------------------------------+

The class **RotationMatrix** contains the following properties:

+-------------------+-------+-------+----------------------------------------------------+
| Property          | Get   | Set   | Description                                        |
+===================+=======+=======+====================================================+
| ``Phi``           | \*    | \*    | The rotation angle.                                |
+-------------------+-------+-------+----------------------------------------------------+
| ``M11``           | \*    |       | The element m11 (row 1, column 1) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M12``           | \*    |       | The element m12 (row 1, column 2) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M13``           | \*    |       | The element m13 (row 1, column 3) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M21``           | \*    |       | The element m21 (row 2, column 1) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M22``           | \*    |       | The element m22 (row 2, column 2) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M23``           | \*    |       | The element m23 (row 2, column 3) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M31``           | \*    |       | The element m31 (row 3, column 1) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M32``           | \*    |       | The element m32 (row 3, column 2) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M33``           | \*    |       | The element m33 (row 3, column 3) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``[index]``       | \*    |       | The matrix values as a parameterized property.     |
+-------------------+-------+-------+----------------------------------------------------+
| ``Determinant``   | \*    |       | The determinant of the matrix.                     |
+-------------------+-------+-------+----------------------------------------------------+
| ``Trace``         | \*    |       | The trace of the matrix.                           |
+-------------------+-------+-------+----------------------------------------------------+
| ``Inverse``       | \*    |       | The inverse of the matrix.                         |
+-------------------+-------+-------+----------------------------------------------------+
| ``Transpose``     | \*    |       | The transpose of the matrix.                       |
+-------------------+-------+-------+----------------------------------------------------+

The class **RotationMatrix** contains the following methods:

+-----------------------+--------------------------------------------+
| Method                | Description                                |
+=======================+============================================+
| ``ResetToIdentity``   | Reset the matrix to the identity matrix.   |
+-----------------------+--------------------------------------------+
| ``Rotate``            | Rotate the matrix in place.                |
+-----------------------+--------------------------------------------+

Description
~~~~~~~~~~~

The values in the matrix are arranged like this:

\| \| \| m11 m12 m13 \|

.. raw:: html

   <table rows="6" cols="1">

cos( phi ) -sin( phi ) 0

m21 m22 m23

sin( phi ) cos( phi ) 0

m31 m32 m33

0 0 1

.. raw:: html

   </table>

All values are not stored within this object but implicit.

The determinant of such a matrix is always 1.

The trace of such a matrix is 2 \* cos(phi) + 1.

The size is implicitely 3x3 and related properties are not implemented.

The inverse can be quickly calculated by negating the rotation angle, and the result is still a rotation matrix.

The transpose of a rotation matrix is the same as its inverse and the result is still a rotation matrix.

This storage assumes row-vectors and post-multiplication for transformations.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *RotationMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RotationMatrix()``

Default constructor.

By default this is the identity matrix.

Constructors
~~~~~~~~~~~~

Constructor *RotationMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RotationMatrix(System.Double phi)``

Constructor.

The constructor has the following parameters:

+-------------+---------------------+------------------------+
| Parameter   | Type                | Description            |
+=============+=====================+========================+
| ``phi``     | ``System.Double``   | The rotation\_angle.   |
+-------------+---------------------+------------------------+

Constructs a rotation matrix that rotates by the given angle.

Properties
~~~~~~~~~~

Property *Phi*
^^^^^^^^^^^^^^

``System.Double Phi``

The rotation angle.

Property *M11*
^^^^^^^^^^^^^^

``System.Double M11``

The element m11 (row 1, column 1) of the matrix.

Property *M12*
^^^^^^^^^^^^^^

``System.Double M12``

The element m12 (row 1, column 2) of the matrix.

Property *M13*
^^^^^^^^^^^^^^

``System.Double M13``

The element m13 (row 1, column 3) of the matrix.

Property *M21*
^^^^^^^^^^^^^^

``System.Double M21``

The element m21 (row 2, column 1) of the matrix.

Property *M22*
^^^^^^^^^^^^^^

``System.Double M22``

The element m22 (row 2, column 2) of the matrix.

Property *M23*
^^^^^^^^^^^^^^

``System.Double M23``

The element m23 (row 2, column 3) of the matrix.

Property *M31*
^^^^^^^^^^^^^^

``System.Double M31``

The element m31 (row 3, column 1) of the matrix.

Property *M32*
^^^^^^^^^^^^^^

``System.Double M32``

The element m32 (row 3, column 2) of the matrix.

Property *M33*
^^^^^^^^^^^^^^

``System.Double M33``

The element m33 (row 3, column 3) of the matrix.

Property *[index]*
^^^^^^^^^^^^^^^^^^

``System.Double [index]``

The matrix values as a parameterized property.

This property allows you to retrieve the implicit values also.

Property *Determinant*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double Determinant``

The determinant of the matrix.

Property *Trace*
^^^^^^^^^^^^^^^^

``System.Double Trace``

The trace of the matrix.

Property *Inverse*
^^^^^^^^^^^^^^^^^^

``RotationMatrix Inverse``

The inverse of the matrix.

Property *Transpose*
^^^^^^^^^^^^^^^^^^^^

``RotationMatrix Transpose``

The transpose of the matrix.

Static Methods
~~~~~~~~~~~~~~

Method *Multiply*
^^^^^^^^^^^^^^^^^

``RotationMatrix Multiply(RotationMatrix a, RotationMatrix b)``

Multiply two matrices.

The method **Multiply** has the following parameters:

+-------------+----------------------+---------------+
| Parameter   | Type                 | Description   |
+=============+======================+===============+
| ``a``       | ``RotationMatrix``   |               |
+-------------+----------------------+---------------+
| ``b``       | ``RotationMatrix``   |               |
+-------------+----------------------+---------------+

The product.

Methods
~~~~~~~

Method *ResetToIdentity*
^^^^^^^^^^^^^^^^^^^^^^^^

``void ResetToIdentity()``

Reset the matrix to the identity matrix.

The identity matrix has the following form. \| 1 0 0 \| \| 0 1 0 \| \| 0 0 1 \|A reference to this object.

Method *Rotate*
^^^^^^^^^^^^^^^

``void Rotate(System.Double phi)``

Rotate the matrix in place.

The method **Rotate** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``phi``     | ``System.Double``   |               |
+-------------+---------------------+---------------+

Return a reference to the matrix.
