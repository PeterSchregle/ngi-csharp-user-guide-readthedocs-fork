Class *Point*
-------------

A point in two-dimensional euclidean space.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Point** implements the following interfaces:

+---------------------------------------------+
| Interface                                   |
+=============================================+
| ``IEquatablePointPointCoordinateVariant``   |
+---------------------------------------------+
| ``ISerializable``                           |
+---------------------------------------------+
| ``INotifyPropertyChanged``                  |
+---------------------------------------------+

The class **Point** contains the following variant parameters:

+------------------+--------------------------------------------+
| Variant          | Description                                |
+==================+============================================+
| ``Coordinate``   | The coordinate type of the point object.   |
+------------------+--------------------------------------------+

The class **Point** contains the following properties:

+------------+-------+-------+-------------------------------------------+
| Property   | Get   | Set   | Description                               |
+============+=======+=======+===========================================+
| ``X``      | \*    | \*    | The horizontal coordinate of the point.   |
+------------+-------+-------+-------------------------------------------+
| ``Y``      | \*    | \*    | The vertical coordinate of the point.     |
+------------+-------+-------+-------------------------------------------+

The class **Point** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``Move``       | Move point.                                             |
+----------------+---------------------------------------------------------+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

A point can be seen as a vector rooted at the origin in two-dimensional space.

The following operations are implemented: operator== : comparison for equality. operator != : comparison for inequality. operator- : negation, directly implemented. operator+= : addition, directly implemented. operator+ : addition, implemented through boost::additive operator-= : subtraction, directly implemented. operator- : subtraction, implemented through boost::additive operator\ *= : multiplication, directly implemented. operator* : multiplication, implemented through boost::multiplicative operator/= : division, directly implemented. operator/ : division, implemented through boost::multiplicative

Variants
~~~~~~~~

Variant *Coordinate*
^^^^^^^^^^^^^^^^^^^^

The coordinate type of the point object.

The variant parameter **Coordinate** has the following types:

+--------------+
| Type         |
+==============+
| ``Int32``    |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

Coordinates of points can be of different types.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Point*
^^^^^^^^^^^^^^^^^^^

``Point()``

Constructs a point at origin (0, 0).

Constructors
~~~~~~~~~~~~

Constructor *Point*
^^^^^^^^^^^^^^^^^^^

``Point(System.Object x, System.Object y)``

Constructs a point with specific coordinates.

The constructor has the following parameters:

+-------------+---------------------+------------------------------+
| Parameter   | Type                | Description                  |
+=============+=====================+==============================+
| ``x``       | ``System.Object``   | The horizontal coordinate.   |
+-------------+---------------------+------------------------------+
| ``y``       | ``System.Object``   | The vertical coordinate.     |
+-------------+---------------------+------------------------------+

Properties
~~~~~~~~~~

Property *X*
^^^^^^^^^^^^

``System.Object X``

The horizontal coordinate of the point.

Property *Y*
^^^^^^^^^^^^

``System.Object Y``

The vertical coordinate of the point.

Static Methods
~~~~~~~~~~~~~~

Method *VectorBetweenPoints*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Vector VectorBetweenPoints(Point from, Point to)``

Calculates the vector between two points.

The method **VectorBetweenPoints** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``from``    | ``Point``   |               |
+-------------+-------------+---------------+
| ``to``      | ``Point``   |               |
+-------------+-------------+---------------+

Method *Distance*
^^^^^^^^^^^^^^^^^

``System.Double Distance(Point a, Point b)``

Calculates the distance between two points.

The method **Distance** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``a``       | ``Point``   |               |
+-------------+-------------+---------------+
| ``b``       | ``Point``   |               |
+-------------+-------------+---------------+

Method *DistanceSquared*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double DistanceSquared(Point a, Point b)``

Calculates the squared distance between two points.

The method **DistanceSquared** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``a``       | ``Point``   |               |
+-------------+-------------+---------------+
| ``b``       | ``Point``   |               |
+-------------+-------------+---------------+

Methods
~~~~~~~

Method *Move*
^^^^^^^^^^^^^

``Point Move(Vector movement)``

Move point.

The method **Move** has the following parameters:

+----------------+--------------+--------------------+
| Parameter      | Type         | Description        |
+================+==============+====================+
| ``movement``   | ``Vector``   | Movement vector.   |
+----------------+--------------+--------------------+

Moves a point by a vector.

The moved point.

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO documentation missing

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO documentation missing
