Class *QuadrilateralList*
-------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **QuadrilateralList** implements the following interfaces:

+--------------------------------------------------+
| Interface                                        |
+==================================================+
| ``IListQuadrilateralListQuadrilateralVariant``   |
+--------------------------------------------------+

The class **QuadrilateralList** contains the following variant parameters:

+---------------------+-----------------------------------------+
| Variant             | Description                             |
+=====================+=========================================+
| ``Quadrilateral``   | TODO no brief description for variant   |
+---------------------+-----------------------------------------+

The class **QuadrilateralList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **QuadrilateralList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Quadrilateral*
^^^^^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Quadrilateral** has the following types:

+---------------------------+
| Type                      |
+===========================+
| ``QuadrilateralInt32``    |
+---------------------------+
| ``QuadrilateralDouble``   |
+---------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *QuadrilateralList*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``QuadrilateralList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``Quadrilateral [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``QuadrilateralEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(Quadrilateral item)``

The method **Add** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``item``    | ``Quadrilateral``   |               |
+-------------+---------------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(Quadrilateral item)``

The method **Contains** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``item``    | ``Quadrilateral``   |               |
+-------------+---------------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(Quadrilateral item)``

The method **Remove** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``item``    | ``Quadrilateral``   |               |
+-------------+---------------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(Quadrilateral item)``

The method **IndexOf** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``item``    | ``Quadrilateral``   |               |
+-------------+---------------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, Quadrilateral item)``

The method **Insert** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``index``   | ``System.Int32``    |               |
+-------------+---------------------+---------------+
| ``item``    | ``Quadrilateral``   |               |
+-------------+---------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
