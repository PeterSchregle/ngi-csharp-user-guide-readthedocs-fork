Class *Region*
--------------

A region.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Region** implements the following interfaces:

+------------------------+
| Interface              |
+========================+
| ``IEquatableRegion``   |
+------------------------+

The class **Region** contains the following properties:

+----------------------------+-------+-------+---------------+
| Property                   | Get   | Set   | Description   |
+============================+=======+=======+===============+
| ``Chords``                 | \*    |       |               |
+----------------------------+-------+-------+---------------+
| ``IsComplement``           | \*    | \*    |               |
+----------------------------+-------+-------+---------------+
| ``IsEmpty``                | \*    |       |               |
+----------------------------+-------+-------+---------------+
| ``FirstPoint``             | \*    |       |               |
+----------------------------+-------+-------+---------------+
| ``Area``                   | \*    |       |               |
+----------------------------+-------+-------+---------------+
| ``Left``                   | \*    |       |               |
+----------------------------+-------+-------+---------------+
| ``Right``                  | \*    |       |               |
+----------------------------+-------+-------+---------------+
| ``Top``                    | \*    |       |               |
+----------------------------+-------+-------+---------------+
| ``Bottom``                 | \*    |       |               |
+----------------------------+-------+-------+---------------+
| ``Bounds``                 | \*    |       |               |
+----------------------------+-------+-------+---------------+
| ``Perimeter``              | \*    |       |               |
+----------------------------+-------+-------+---------------+
| ``Moments``                | \*    |       |               |
+----------------------------+-------+-------+---------------+
| ``VectorizedBoundaries``   | \*    |       |               |
+----------------------------+-------+-------+---------------+
| ``ConvexHull``             | \*    |       |               |
+----------------------------+-------+-------+---------------+

The class **Region** contains the following methods:

+-----------------------------+---------------------------------------------------------+
| Method                      | Description                                             |
+=============================+=========================================================+
| ``GetAreaCalibrated``       | The area of the region in calibrated units.             |
+-----------------------------+---------------------------------------------------------+
| ``GetBoundsCalibrated``     | The area of the region in calibrated units.             |
+-----------------------------+---------------------------------------------------------+
| ``ToString``                | Provide string representation for debugging purposes.   |
+-----------------------------+---------------------------------------------------------+
| ``MinkowskiSubtraction``    | Minkowski subtraction.                                  |
+-----------------------------+---------------------------------------------------------+
| ``MinkowskiAddition``       | Minkowski addition.                                     |
+-----------------------------+---------------------------------------------------------+
| ``Erosion``                 | Erode a region with a structuring element.              |
+-----------------------------+---------------------------------------------------------+
| ``Dilation``                | Dilate a region with a structuring element.             |
+-----------------------------+---------------------------------------------------------+
| ``Opening``                 | Open a region with a structuring element.               |
+-----------------------------+---------------------------------------------------------+
| ``Closing``                 | Close a region with a structuring element.              |
+-----------------------------+---------------------------------------------------------+
| ``InnerBoundary``           | Inner boundary.                                         |
+-----------------------------+---------------------------------------------------------+
| ``OuterBoundary``           | Outer boundary.                                         |
+-----------------------------+---------------------------------------------------------+
| ``MorphologicalGradient``   | Morphological gradient.                                 |
+-----------------------------+---------------------------------------------------------+
| ``Translate``               | Translate a region.                                     |
+-----------------------------+---------------------------------------------------------+
| ``Center``                  | Centers a region on the origin.                         |
+-----------------------------+---------------------------------------------------------+
| ``Transpose``               | Transpose a region.                                     |
+-----------------------------+---------------------------------------------------------+
| ``Complement``              | Complement of a region.                                 |
+-----------------------------+---------------------------------------------------------+
| ``Downsample``              | Downsample a region.                                    |
+-----------------------------+---------------------------------------------------------+
| ``ToPolygonList``           |                                                         |
+-----------------------------+---------------------------------------------------------+
| ``ToPointList``             |                                                         |
+-----------------------------+---------------------------------------------------------+

The class **Region** contains the following enumerations:

+------------------------------+------------------------------+
| Enumeration                  | Description                  |
+==============================+==============================+
| ``DownsampleRoundingMode``   | TODO documentation missing   |
+------------------------------+------------------------------+

Description
~~~~~~~~~~~

A region consists of a list of chords. The chords are sorted in the horizontal and vertical directions.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Region*
^^^^^^^^^^^^^^^^^^^^

``Region()``

Construct a default region.

The region is empty after construction.

Constructors
~~~~~~~~~~~~

Constructor *Region*
^^^^^^^^^^^^^^^^^^^^

``Region(Chord chord)``

Construct a region with one chord.

The constructor has the following parameters:

+-------------+-------------+----------------------+
| Parameter   | Type        | Description          |
+=============+=============+======================+
| ``chord``   | ``Chord``   | The initial chord.   |
+-------------+-------------+----------------------+

The region contains one item after construction.

Constructor *Region*
^^^^^^^^^^^^^^^^^^^^

``Region(ChainCode code)``

Construct a region from a chain\_code.

The constructor has the following parameters:

+-------------+-----------------+--------------------+
| Parameter   | Type            | Description        |
+=============+=================+====================+
| ``code``    | ``ChainCode``   | The chain\_code.   |
+-------------+-----------------+--------------------+

Any pixel in the chain\_code that is reached by following the chain code will become part of the region.

Constructor *Region*
^^^^^^^^^^^^^^^^^^^^

``Region(PointListPointInt32 points)``

Construct a region from a list of points.

The constructor has the following parameters:

+--------------+---------------------------+-----------------------+
| Parameter    | Type                      | Description           |
+==============+===========================+=======================+
| ``points``   | ``PointListPointInt32``   | The list of points.   |
+--------------+---------------------------+-----------------------+

Any point in the point list will become a pixel that is part of the region.

Properties
~~~~~~~~~~

Property *Chords*
^^^^^^^^^^^^^^^^^

``ChordList Chords``

Property *IsComplement*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsComplement``

Property *IsEmpty*
^^^^^^^^^^^^^^^^^^

``System.Boolean IsEmpty``

Property *FirstPoint*
^^^^^^^^^^^^^^^^^^^^^

``PointInt32 FirstPoint``

Property *Area*
^^^^^^^^^^^^^^^

``System.Int32 Area``

Property *Left*
^^^^^^^^^^^^^^^

``System.Int32 Left``

Property *Right*
^^^^^^^^^^^^^^^^

``System.Int32 Right``

Property *Top*
^^^^^^^^^^^^^^

``System.Int32 Top``

Property *Bottom*
^^^^^^^^^^^^^^^^^

``System.Int32 Bottom``

Property *Bounds*
^^^^^^^^^^^^^^^^^

``BoxDouble Bounds``

Property *Perimeter*
^^^^^^^^^^^^^^^^^^^^

``System.Double Perimeter``

Property *Moments*
^^^^^^^^^^^^^^^^^^

``Moments Moments``

Property *VectorizedBoundaries*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``PolylineListPolylineDouble VectorizedBoundaries``

Property *ConvexHull*
^^^^^^^^^^^^^^^^^^^^^

``PolylineDouble ConvexHull``

Static Methods
~~~~~~~~~~~~~~

Method *SetUnion*
^^^^^^^^^^^^^^^^^

``Region SetUnion(Region a, Region b)``

Union of two regions.

The method **SetUnion** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``a``       | ``Region``   |               |
+-------------+--------------+---------------+
| ``b``       | ``Region``   |               |
+-------------+--------------+---------------+

This function supports inverted regions and uses DeMorgan's rules to eliminate the complement.

Method *SetIntersection*
^^^^^^^^^^^^^^^^^^^^^^^^

``Region SetIntersection(Region a, Region b)``

Intersection of two regions.

The method **SetIntersection** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``a``       | ``Region``   |               |
+-------------+--------------+---------------+
| ``b``       | ``Region``   |               |
+-------------+--------------+---------------+

This function supports inverted regions and uses DeMorgan's rules to eliminate the complement.

Method *SetDifference*
^^^^^^^^^^^^^^^^^^^^^^

``Region SetDifference(Region a, Region b)``

Difference of two regions.

The method **SetDifference** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``a``       | ``Region``   |               |
+-------------+--------------+---------------+
| ``b``       | ``Region``   |               |
+-------------+--------------+---------------+

This function supports inverted regions and uses DeMorgan's rules to eliminate the complement.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(ViewLocatorByte source, System.Byte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+-----------------------+---------------+
| Parameter       | Type                  | Description   |
+=================+=======================+===============+
| ``source``      | ``ViewLocatorByte``   |               |
+-----------------+-----------------------+---------------+
| ``threshold``   | ``System.Byte``       |               |
+-----------------+-----------------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(ViewLocatorUInt16 source, System.UInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt16``   |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.UInt16``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(ViewLocatorUInt32 source, System.UInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt32``   |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.UInt32``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(ViewLocatorDouble source, System.Double threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorDouble``   |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.Double``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(ViewLocatorRgbByte source, RgbByte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+--------------------------+---------------+
| Parameter       | Type                     | Description   |
+=================+==========================+===============+
| ``source``      | ``ViewLocatorRgbByte``   |               |
+-----------------+--------------------------+---------------+
| ``threshold``   | ``RgbByte``              |               |
+-----------------+--------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(ViewLocatorRgbUInt16 source, RgbUInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbUInt16``   |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbUInt16``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(ViewLocatorRgbUInt32 source, RgbUInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbUInt32``   |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbUInt32``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(ViewLocatorRgbDouble source, RgbDouble threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbDouble``   |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbDouble``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(ViewLocatorRgbaByte source, RgbaByte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+---------------------------+---------------+
| Parameter       | Type                      | Description   |
+=================+===========================+===============+
| ``source``      | ``ViewLocatorRgbaByte``   |               |
+-----------------+---------------------------+---------------+
| ``threshold``   | ``RgbaByte``              |               |
+-----------------+---------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(ViewLocatorRgbaUInt16 source, RgbaUInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaUInt16``   |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaUInt16``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(ViewLocatorRgbaUInt32 source, RgbaUInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaUInt32``   |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaUInt32``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(ViewLocatorRgbaDouble source, RgbaDouble threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaDouble``   |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaDouble``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(View source, object threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+--------------+---------------+
| Parameter       | Type         | Description   |
+=================+==============+===============+
| ``source``      | ``View``     |               |
+-----------------+--------------+---------------+
| ``threshold``   | ``object``   |               |
+-----------------+--------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(ViewLocatorByte source, System.Byte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+-----------------------+---------------+
| Parameter       | Type                  | Description   |
+=================+=======================+===============+
| ``source``      | ``ViewLocatorByte``   |               |
+-----------------+-----------------------+---------------+
| ``threshold``   | ``System.Byte``       |               |
+-----------------+-----------------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(ViewLocatorUInt16 source, System.UInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt16``   |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.UInt16``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(ViewLocatorUInt32 source, System.UInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt32``   |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.UInt32``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(ViewLocatorDouble source, System.Double threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorDouble``   |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.Double``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(ViewLocatorRgbByte source, RgbByte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+--------------------------+---------------+
| Parameter       | Type                     | Description   |
+=================+==========================+===============+
| ``source``      | ``ViewLocatorRgbByte``   |               |
+-----------------+--------------------------+---------------+
| ``threshold``   | ``RgbByte``              |               |
+-----------------+--------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(ViewLocatorRgbUInt16 source, RgbUInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbUInt16``   |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbUInt16``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(ViewLocatorRgbUInt32 source, RgbUInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbUInt32``   |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbUInt32``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(ViewLocatorRgbDouble source, RgbDouble threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbDouble``   |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbDouble``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(ViewLocatorRgbaByte source, RgbaByte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+---------------------------+---------------+
| Parameter       | Type                      | Description   |
+=================+===========================+===============+
| ``source``      | ``ViewLocatorRgbaByte``   |               |
+-----------------+---------------------------+---------------+
| ``threshold``   | ``RgbaByte``              |               |
+-----------------+---------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(ViewLocatorRgbaUInt16 source, RgbaUInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaUInt16``   |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaUInt16``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(ViewLocatorRgbaUInt32 source, RgbaUInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaUInt32``   |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaUInt32``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(ViewLocatorRgbaDouble source, RgbaDouble threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaDouble``   |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaDouble``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(View source, object threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+--------------+---------------+
| Parameter       | Type         | Description   |
+=================+==============+===============+
| ``source``      | ``View``     |               |
+-----------------+--------------+---------------+
| ``threshold``   | ``object``   |               |
+-----------------+--------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(ViewLocatorByte source, System.Byte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+-----------------------+---------------+
| Parameter       | Type                  | Description   |
+=================+=======================+===============+
| ``source``      | ``ViewLocatorByte``   |               |
+-----------------+-----------------------+---------------+
| ``threshold``   | ``System.Byte``       |               |
+-----------------+-----------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(ViewLocatorUInt16 source, System.UInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt16``   |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.UInt16``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(ViewLocatorUInt32 source, System.UInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt32``   |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.UInt32``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(ViewLocatorDouble source, System.Double threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorDouble``   |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.Double``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(ViewLocatorRgbByte source, RgbByte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+--------------------------+---------------+
| Parameter       | Type                     | Description   |
+=================+==========================+===============+
| ``source``      | ``ViewLocatorRgbByte``   |               |
+-----------------+--------------------------+---------------+
| ``threshold``   | ``RgbByte``              |               |
+-----------------+--------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(ViewLocatorRgbUInt16 source, RgbUInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbUInt16``   |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbUInt16``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(ViewLocatorRgbUInt32 source, RgbUInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbUInt32``   |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbUInt32``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(ViewLocatorRgbDouble source, RgbDouble threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbDouble``   |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbDouble``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(ViewLocatorRgbaByte source, RgbaByte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+---------------------------+---------------+
| Parameter       | Type                      | Description   |
+=================+===========================+===============+
| ``source``      | ``ViewLocatorRgbaByte``   |               |
+-----------------+---------------------------+---------------+
| ``threshold``   | ``RgbaByte``              |               |
+-----------------+---------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(ViewLocatorRgbaUInt16 source, RgbaUInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaUInt16``   |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaUInt16``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(ViewLocatorRgbaUInt32 source, RgbaUInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaUInt32``   |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaUInt32``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(ViewLocatorRgbaDouble source, RgbaDouble threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaDouble``   |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaDouble``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(View source, object threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+--------------+---------------+
| Parameter       | Type         | Description   |
+=================+==============+===============+
| ``source``      | ``View``     |               |
+-----------------+--------------+---------------+
| ``threshold``   | ``object``   |               |
+-----------------+--------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(ViewLocatorByte source, System.Byte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+-----------------------+---------------+
| Parameter       | Type                  | Description   |
+=================+=======================+===============+
| ``source``      | ``ViewLocatorByte``   |               |
+-----------------+-----------------------+---------------+
| ``threshold``   | ``System.Byte``       |               |
+-----------------+-----------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(ViewLocatorUInt16 source, System.UInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt16``   |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.UInt16``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(ViewLocatorUInt32 source, System.UInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt32``   |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.UInt32``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(ViewLocatorDouble source, System.Double threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorDouble``   |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.Double``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(ViewLocatorRgbByte source, RgbByte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+--------------------------+---------------+
| Parameter       | Type                     | Description   |
+=================+==========================+===============+
| ``source``      | ``ViewLocatorRgbByte``   |               |
+-----------------+--------------------------+---------------+
| ``threshold``   | ``RgbByte``              |               |
+-----------------+--------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(ViewLocatorRgbUInt16 source, RgbUInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbUInt16``   |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbUInt16``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(ViewLocatorRgbUInt32 source, RgbUInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbUInt32``   |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbUInt32``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(ViewLocatorRgbDouble source, RgbDouble threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbDouble``   |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbDouble``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(ViewLocatorRgbaByte source, RgbaByte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+---------------------------+---------------+
| Parameter       | Type                      | Description   |
+=================+===========================+===============+
| ``source``      | ``ViewLocatorRgbaByte``   |               |
+-----------------+---------------------------+---------------+
| ``threshold``   | ``RgbaByte``              |               |
+-----------------+---------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(ViewLocatorRgbaUInt16 source, RgbaUInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaUInt16``   |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaUInt16``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(ViewLocatorRgbaUInt32 source, RgbaUInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaUInt32``   |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaUInt32``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(ViewLocatorRgbaDouble source, RgbaDouble threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaDouble``   |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaDouble``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(View source, object threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+--------------+---------------+
| Parameter       | Type         | Description   |
+=================+==============+===============+
| ``source``      | ``View``     |               |
+-----------------+--------------+---------------+
| ``threshold``   | ``object``   |               |
+-----------------+--------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(ViewLocatorByte source, System.Byte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+-----------------------+---------------+
| Parameter       | Type                  | Description   |
+=================+=======================+===============+
| ``source``      | ``ViewLocatorByte``   |               |
+-----------------+-----------------------+---------------+
| ``threshold``   | ``System.Byte``       |               |
+-----------------+-----------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(ViewLocatorUInt16 source, System.UInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt16``   |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.UInt16``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(ViewLocatorUInt32 source, System.UInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt32``   |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.UInt32``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(ViewLocatorDouble source, System.Double threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorDouble``   |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.Double``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(ViewLocatorRgbByte source, RgbByte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+--------------------------+---------------+
| Parameter       | Type                     | Description   |
+=================+==========================+===============+
| ``source``      | ``ViewLocatorRgbByte``   |               |
+-----------------+--------------------------+---------------+
| ``threshold``   | ``RgbByte``              |               |
+-----------------+--------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(ViewLocatorRgbUInt16 source, RgbUInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbUInt16``   |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbUInt16``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(ViewLocatorRgbUInt32 source, RgbUInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbUInt32``   |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbUInt32``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(ViewLocatorRgbDouble source, RgbDouble threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbDouble``   |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbDouble``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(ViewLocatorRgbaByte source, RgbaByte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+---------------------------+---------------+
| Parameter       | Type                      | Description   |
+=================+===========================+===============+
| ``source``      | ``ViewLocatorRgbaByte``   |               |
+-----------------+---------------------------+---------------+
| ``threshold``   | ``RgbaByte``              |               |
+-----------------+---------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(ViewLocatorRgbaUInt16 source, RgbaUInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaUInt16``   |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaUInt16``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(ViewLocatorRgbaUInt32 source, RgbaUInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaUInt32``   |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaUInt32``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(ViewLocatorRgbaDouble source, RgbaDouble threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaDouble``   |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaDouble``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(View source, object threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+--------------+---------------+
| Parameter       | Type         | Description   |
+=================+==============+===============+
| ``source``      | ``View``     |               |
+-----------------+--------------+---------------+
| ``threshold``   | ``object``   |               |
+-----------------+--------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(ViewLocatorByte source, System.Byte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+-----------------------+---------------+
| Parameter       | Type                  | Description   |
+=================+=======================+===============+
| ``source``      | ``ViewLocatorByte``   |               |
+-----------------+-----------------------+---------------+
| ``threshold``   | ``System.Byte``       |               |
+-----------------+-----------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(ViewLocatorUInt16 source, System.UInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt16``   |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.UInt16``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(ViewLocatorUInt32 source, System.UInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt32``   |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.UInt32``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(ViewLocatorDouble source, System.Double threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorDouble``   |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.Double``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(ViewLocatorRgbByte source, RgbByte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+--------------------------+---------------+
| Parameter       | Type                     | Description   |
+=================+==========================+===============+
| ``source``      | ``ViewLocatorRgbByte``   |               |
+-----------------+--------------------------+---------------+
| ``threshold``   | ``RgbByte``              |               |
+-----------------+--------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(ViewLocatorRgbUInt16 source, RgbUInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbUInt16``   |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbUInt16``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(ViewLocatorRgbUInt32 source, RgbUInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbUInt32``   |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbUInt32``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(ViewLocatorRgbDouble source, RgbDouble threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbDouble``   |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbDouble``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(ViewLocatorRgbaByte source, RgbaByte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+---------------------------+---------------+
| Parameter       | Type                      | Description   |
+=================+===========================+===============+
| ``source``      | ``ViewLocatorRgbaByte``   |               |
+-----------------+---------------------------+---------------+
| ``threshold``   | ``RgbaByte``              |               |
+-----------------+---------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(ViewLocatorRgbaUInt16 source, RgbaUInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaUInt16``   |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaUInt16``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(ViewLocatorRgbaUInt32 source, RgbaUInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaUInt32``   |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaUInt32``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(ViewLocatorRgbaDouble source, RgbaDouble threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaDouble``   |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaDouble``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(View source, object threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+--------------+---------------+
| Parameter       | Type         | Description   |
+=================+==============+===============+
| ``source``      | ``View``     |               |
+-----------------+--------------+---------------+
| ``threshold``   | ``object``   |               |
+-----------------+--------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(ViewLocatorByte source, System.Byte lowerThreshold, System.Byte upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+-----------------------+---------------+
| Parameter            | Type                  | Description   |
+======================+=======================+===============+
| ``source``           | ``ViewLocatorByte``   |               |
+----------------------+-----------------------+---------------+
| ``lowerThreshold``   | ``System.Byte``       |               |
+----------------------+-----------------------+---------------+
| ``upperThreshold``   | ``System.Byte``       |               |
+----------------------+-----------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(ViewLocatorUInt16 source, System.UInt16 lowerThreshold, System.UInt16 upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+-------------------------+---------------+
| Parameter            | Type                    | Description   |
+======================+=========================+===============+
| ``source``           | ``ViewLocatorUInt16``   |               |
+----------------------+-------------------------+---------------+
| ``lowerThreshold``   | ``System.UInt16``       |               |
+----------------------+-------------------------+---------------+
| ``upperThreshold``   | ``System.UInt16``       |               |
+----------------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(ViewLocatorUInt32 source, System.UInt32 lowerThreshold, System.UInt32 upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+-------------------------+---------------+
| Parameter            | Type                    | Description   |
+======================+=========================+===============+
| ``source``           | ``ViewLocatorUInt32``   |               |
+----------------------+-------------------------+---------------+
| ``lowerThreshold``   | ``System.UInt32``       |               |
+----------------------+-------------------------+---------------+
| ``upperThreshold``   | ``System.UInt32``       |               |
+----------------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(ViewLocatorDouble source, System.Double lowerThreshold, System.Double upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+-------------------------+---------------+
| Parameter            | Type                    | Description   |
+======================+=========================+===============+
| ``source``           | ``ViewLocatorDouble``   |               |
+----------------------+-------------------------+---------------+
| ``lowerThreshold``   | ``System.Double``       |               |
+----------------------+-------------------------+---------------+
| ``upperThreshold``   | ``System.Double``       |               |
+----------------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(ViewLocatorRgbByte source, RgbByte lowerThreshold, RgbByte upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+--------------------------+---------------+
| Parameter            | Type                     | Description   |
+======================+==========================+===============+
| ``source``           | ``ViewLocatorRgbByte``   |               |
+----------------------+--------------------------+---------------+
| ``lowerThreshold``   | ``RgbByte``              |               |
+----------------------+--------------------------+---------------+
| ``upperThreshold``   | ``RgbByte``              |               |
+----------------------+--------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(ViewLocatorRgbUInt16 source, RgbUInt16 lowerThreshold, RgbUInt16 upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+----------------------------+---------------+
| Parameter            | Type                       | Description   |
+======================+============================+===============+
| ``source``           | ``ViewLocatorRgbUInt16``   |               |
+----------------------+----------------------------+---------------+
| ``lowerThreshold``   | ``RgbUInt16``              |               |
+----------------------+----------------------------+---------------+
| ``upperThreshold``   | ``RgbUInt16``              |               |
+----------------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(ViewLocatorRgbUInt32 source, RgbUInt32 lowerThreshold, RgbUInt32 upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+----------------------------+---------------+
| Parameter            | Type                       | Description   |
+======================+============================+===============+
| ``source``           | ``ViewLocatorRgbUInt32``   |               |
+----------------------+----------------------------+---------------+
| ``lowerThreshold``   | ``RgbUInt32``              |               |
+----------------------+----------------------------+---------------+
| ``upperThreshold``   | ``RgbUInt32``              |               |
+----------------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(ViewLocatorRgbDouble source, RgbDouble lowerThreshold, RgbDouble upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+----------------------------+---------------+
| Parameter            | Type                       | Description   |
+======================+============================+===============+
| ``source``           | ``ViewLocatorRgbDouble``   |               |
+----------------------+----------------------------+---------------+
| ``lowerThreshold``   | ``RgbDouble``              |               |
+----------------------+----------------------------+---------------+
| ``upperThreshold``   | ``RgbDouble``              |               |
+----------------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(ViewLocatorRgbaByte source, RgbaByte lowerThreshold, RgbaByte upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+---------------------------+---------------+
| Parameter            | Type                      | Description   |
+======================+===========================+===============+
| ``source``           | ``ViewLocatorRgbaByte``   |               |
+----------------------+---------------------------+---------------+
| ``lowerThreshold``   | ``RgbaByte``              |               |
+----------------------+---------------------------+---------------+
| ``upperThreshold``   | ``RgbaByte``              |               |
+----------------------+---------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(ViewLocatorRgbaUInt16 source, RgbaUInt16 lowerThreshold, RgbaUInt16 upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+-----------------------------+---------------+
| Parameter            | Type                        | Description   |
+======================+=============================+===============+
| ``source``           | ``ViewLocatorRgbaUInt16``   |               |
+----------------------+-----------------------------+---------------+
| ``lowerThreshold``   | ``RgbaUInt16``              |               |
+----------------------+-----------------------------+---------------+
| ``upperThreshold``   | ``RgbaUInt16``              |               |
+----------------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(ViewLocatorRgbaUInt32 source, RgbaUInt32 lowerThreshold, RgbaUInt32 upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+-----------------------------+---------------+
| Parameter            | Type                        | Description   |
+======================+=============================+===============+
| ``source``           | ``ViewLocatorRgbaUInt32``   |               |
+----------------------+-----------------------------+---------------+
| ``lowerThreshold``   | ``RgbaUInt32``              |               |
+----------------------+-----------------------------+---------------+
| ``upperThreshold``   | ``RgbaUInt32``              |               |
+----------------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(ViewLocatorRgbaDouble source, RgbaDouble lowerThreshold, RgbaDouble upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+-----------------------------+---------------+
| Parameter            | Type                        | Description   |
+======================+=============================+===============+
| ``source``           | ``ViewLocatorRgbaDouble``   |               |
+----------------------+-----------------------------+---------------+
| ``lowerThreshold``   | ``RgbaDouble``              |               |
+----------------------+-----------------------------+---------------+
| ``upperThreshold``   | ``RgbaDouble``              |               |
+----------------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(View source, object lowerThreshold, object upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+--------------+---------------+
| Parameter            | Type         | Description   |
+======================+==============+===============+
| ``source``           | ``View``     |               |
+----------------------+--------------+---------------+
| ``lowerThreshold``   | ``object``   |               |
+----------------------+--------------+---------------+
| ``upperThreshold``   | ``object``   |               |
+----------------------+--------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(ViewLocatorByte source, System.Byte lowerThreshold, System.Byte upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+-----------------------+---------------+
| Parameter            | Type                  | Description   |
+======================+=======================+===============+
| ``source``           | ``ViewLocatorByte``   |               |
+----------------------+-----------------------+---------------+
| ``lowerThreshold``   | ``System.Byte``       |               |
+----------------------+-----------------------+---------------+
| ``upperThreshold``   | ``System.Byte``       |               |
+----------------------+-----------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(ViewLocatorUInt16 source, System.UInt16 lowerThreshold, System.UInt16 upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+-------------------------+---------------+
| Parameter            | Type                    | Description   |
+======================+=========================+===============+
| ``source``           | ``ViewLocatorUInt16``   |               |
+----------------------+-------------------------+---------------+
| ``lowerThreshold``   | ``System.UInt16``       |               |
+----------------------+-------------------------+---------------+
| ``upperThreshold``   | ``System.UInt16``       |               |
+----------------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(ViewLocatorUInt32 source, System.UInt32 lowerThreshold, System.UInt32 upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+-------------------------+---------------+
| Parameter            | Type                    | Description   |
+======================+=========================+===============+
| ``source``           | ``ViewLocatorUInt32``   |               |
+----------------------+-------------------------+---------------+
| ``lowerThreshold``   | ``System.UInt32``       |               |
+----------------------+-------------------------+---------------+
| ``upperThreshold``   | ``System.UInt32``       |               |
+----------------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(ViewLocatorDouble source, System.Double lowerThreshold, System.Double upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+-------------------------+---------------+
| Parameter            | Type                    | Description   |
+======================+=========================+===============+
| ``source``           | ``ViewLocatorDouble``   |               |
+----------------------+-------------------------+---------------+
| ``lowerThreshold``   | ``System.Double``       |               |
+----------------------+-------------------------+---------------+
| ``upperThreshold``   | ``System.Double``       |               |
+----------------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(ViewLocatorRgbByte source, RgbByte lowerThreshold, RgbByte upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+--------------------------+---------------+
| Parameter            | Type                     | Description   |
+======================+==========================+===============+
| ``source``           | ``ViewLocatorRgbByte``   |               |
+----------------------+--------------------------+---------------+
| ``lowerThreshold``   | ``RgbByte``              |               |
+----------------------+--------------------------+---------------+
| ``upperThreshold``   | ``RgbByte``              |               |
+----------------------+--------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(ViewLocatorRgbUInt16 source, RgbUInt16 lowerThreshold, RgbUInt16 upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+----------------------------+---------------+
| Parameter            | Type                       | Description   |
+======================+============================+===============+
| ``source``           | ``ViewLocatorRgbUInt16``   |               |
+----------------------+----------------------------+---------------+
| ``lowerThreshold``   | ``RgbUInt16``              |               |
+----------------------+----------------------------+---------------+
| ``upperThreshold``   | ``RgbUInt16``              |               |
+----------------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(ViewLocatorRgbUInt32 source, RgbUInt32 lowerThreshold, RgbUInt32 upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+----------------------------+---------------+
| Parameter            | Type                       | Description   |
+======================+============================+===============+
| ``source``           | ``ViewLocatorRgbUInt32``   |               |
+----------------------+----------------------------+---------------+
| ``lowerThreshold``   | ``RgbUInt32``              |               |
+----------------------+----------------------------+---------------+
| ``upperThreshold``   | ``RgbUInt32``              |               |
+----------------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(ViewLocatorRgbDouble source, RgbDouble lowerThreshold, RgbDouble upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+----------------------------+---------------+
| Parameter            | Type                       | Description   |
+======================+============================+===============+
| ``source``           | ``ViewLocatorRgbDouble``   |               |
+----------------------+----------------------------+---------------+
| ``lowerThreshold``   | ``RgbDouble``              |               |
+----------------------+----------------------------+---------------+
| ``upperThreshold``   | ``RgbDouble``              |               |
+----------------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(ViewLocatorRgbaByte source, RgbaByte lowerThreshold, RgbaByte upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+---------------------------+---------------+
| Parameter            | Type                      | Description   |
+======================+===========================+===============+
| ``source``           | ``ViewLocatorRgbaByte``   |               |
+----------------------+---------------------------+---------------+
| ``lowerThreshold``   | ``RgbaByte``              |               |
+----------------------+---------------------------+---------------+
| ``upperThreshold``   | ``RgbaByte``              |               |
+----------------------+---------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(ViewLocatorRgbaUInt16 source, RgbaUInt16 lowerThreshold, RgbaUInt16 upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+-----------------------------+---------------+
| Parameter            | Type                        | Description   |
+======================+=============================+===============+
| ``source``           | ``ViewLocatorRgbaUInt16``   |               |
+----------------------+-----------------------------+---------------+
| ``lowerThreshold``   | ``RgbaUInt16``              |               |
+----------------------+-----------------------------+---------------+
| ``upperThreshold``   | ``RgbaUInt16``              |               |
+----------------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(ViewLocatorRgbaUInt32 source, RgbaUInt32 lowerThreshold, RgbaUInt32 upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+-----------------------------+---------------+
| Parameter            | Type                        | Description   |
+======================+=============================+===============+
| ``source``           | ``ViewLocatorRgbaUInt32``   |               |
+----------------------+-----------------------------+---------------+
| ``lowerThreshold``   | ``RgbaUInt32``              |               |
+----------------------+-----------------------------+---------------+
| ``upperThreshold``   | ``RgbaUInt32``              |               |
+----------------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(ViewLocatorRgbaDouble source, RgbaDouble lowerThreshold, RgbaDouble upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+-----------------------------+---------------+
| Parameter            | Type                        | Description   |
+======================+=============================+===============+
| ``source``           | ``ViewLocatorRgbaDouble``   |               |
+----------------------+-----------------------------+---------------+
| ``lowerThreshold``   | ``RgbaDouble``              |               |
+----------------------+-----------------------------+---------------+
| ``upperThreshold``   | ``RgbaDouble``              |               |
+----------------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(View source, object lowerThreshold, object upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+--------------+---------------+
| Parameter            | Type         | Description   |
+======================+==============+===============+
| ``source``           | ``View``     |               |
+----------------------+--------------+---------------+
| ``lowerThreshold``   | ``object``   |               |
+----------------------+--------------+---------------+
| ``upperThreshold``   | ``object``   |               |
+----------------------+--------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(ViewLocatorByte source, Region region, System.Byte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+-----------------------+---------------+
| Parameter       | Type                  | Description   |
+=================+=======================+===============+
| ``source``      | ``ViewLocatorByte``   |               |
+-----------------+-----------------------+---------------+
| ``region``      | ``Region``            |               |
+-----------------+-----------------------+---------------+
| ``threshold``   | ``System.Byte``       |               |
+-----------------+-----------------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(ViewLocatorUInt16 source, Region region, System.UInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt16``   |               |
+-----------------+-------------------------+---------------+
| ``region``      | ``Region``              |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.UInt16``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(ViewLocatorUInt32 source, Region region, System.UInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt32``   |               |
+-----------------+-------------------------+---------------+
| ``region``      | ``Region``              |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.UInt32``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(ViewLocatorDouble source, Region region, System.Double threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorDouble``   |               |
+-----------------+-------------------------+---------------+
| ``region``      | ``Region``              |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.Double``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(ViewLocatorRgbByte source, Region region, RgbByte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+--------------------------+---------------+
| Parameter       | Type                     | Description   |
+=================+==========================+===============+
| ``source``      | ``ViewLocatorRgbByte``   |               |
+-----------------+--------------------------+---------------+
| ``region``      | ``Region``               |               |
+-----------------+--------------------------+---------------+
| ``threshold``   | ``RgbByte``              |               |
+-----------------+--------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(ViewLocatorRgbUInt16 source, Region region, RgbUInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbUInt16``   |               |
+-----------------+----------------------------+---------------+
| ``region``      | ``Region``                 |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbUInt16``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(ViewLocatorRgbUInt32 source, Region region, RgbUInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbUInt32``   |               |
+-----------------+----------------------------+---------------+
| ``region``      | ``Region``                 |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbUInt32``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(ViewLocatorRgbDouble source, Region region, RgbDouble threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbDouble``   |               |
+-----------------+----------------------------+---------------+
| ``region``      | ``Region``                 |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbDouble``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(ViewLocatorRgbaByte source, Region region, RgbaByte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+---------------------------+---------------+
| Parameter       | Type                      | Description   |
+=================+===========================+===============+
| ``source``      | ``ViewLocatorRgbaByte``   |               |
+-----------------+---------------------------+---------------+
| ``region``      | ``Region``                |               |
+-----------------+---------------------------+---------------+
| ``threshold``   | ``RgbaByte``              |               |
+-----------------+---------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(ViewLocatorRgbaUInt16 source, Region region, RgbaUInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaUInt16``   |               |
+-----------------+-----------------------------+---------------+
| ``region``      | ``Region``                  |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaUInt16``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(ViewLocatorRgbaUInt32 source, Region region, RgbaUInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaUInt32``   |               |
+-----------------+-----------------------------+---------------+
| ``region``      | ``Region``                  |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaUInt32``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(ViewLocatorRgbaDouble source, Region region, RgbaDouble threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaDouble``   |               |
+-----------------+-----------------------------+---------------+
| ``region``      | ``Region``                  |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaDouble``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdEqual(View source, Region region, object threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdEqual** has the following parameters:

+-----------------+--------------+---------------+
| Parameter       | Type         | Description   |
+=================+==============+===============+
| ``source``      | ``View``     |               |
+-----------------+--------------+---------------+
| ``region``      | ``Region``   |               |
+-----------------+--------------+---------------+
| ``threshold``   | ``object``   |               |
+-----------------+--------------+---------------+

It takes a source view and returns the region that contains all pixels which are equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(ViewLocatorByte source, Region region, System.Byte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+-----------------------+---------------+
| Parameter       | Type                  | Description   |
+=================+=======================+===============+
| ``source``      | ``ViewLocatorByte``   |               |
+-----------------+-----------------------+---------------+
| ``region``      | ``Region``            |               |
+-----------------+-----------------------+---------------+
| ``threshold``   | ``System.Byte``       |               |
+-----------------+-----------------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(ViewLocatorUInt16 source, Region region, System.UInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt16``   |               |
+-----------------+-------------------------+---------------+
| ``region``      | ``Region``              |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.UInt16``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(ViewLocatorUInt32 source, Region region, System.UInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt32``   |               |
+-----------------+-------------------------+---------------+
| ``region``      | ``Region``              |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.UInt32``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(ViewLocatorDouble source, Region region, System.Double threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorDouble``   |               |
+-----------------+-------------------------+---------------+
| ``region``      | ``Region``              |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.Double``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(ViewLocatorRgbByte source, Region region, RgbByte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+--------------------------+---------------+
| Parameter       | Type                     | Description   |
+=================+==========================+===============+
| ``source``      | ``ViewLocatorRgbByte``   |               |
+-----------------+--------------------------+---------------+
| ``region``      | ``Region``               |               |
+-----------------+--------------------------+---------------+
| ``threshold``   | ``RgbByte``              |               |
+-----------------+--------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(ViewLocatorRgbUInt16 source, Region region, RgbUInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbUInt16``   |               |
+-----------------+----------------------------+---------------+
| ``region``      | ``Region``                 |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbUInt16``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(ViewLocatorRgbUInt32 source, Region region, RgbUInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbUInt32``   |               |
+-----------------+----------------------------+---------------+
| ``region``      | ``Region``                 |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbUInt32``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(ViewLocatorRgbDouble source, Region region, RgbDouble threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbDouble``   |               |
+-----------------+----------------------------+---------------+
| ``region``      | ``Region``                 |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbDouble``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(ViewLocatorRgbaByte source, Region region, RgbaByte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+---------------------------+---------------+
| Parameter       | Type                      | Description   |
+=================+===========================+===============+
| ``source``      | ``ViewLocatorRgbaByte``   |               |
+-----------------+---------------------------+---------------+
| ``region``      | ``Region``                |               |
+-----------------+---------------------------+---------------+
| ``threshold``   | ``RgbaByte``              |               |
+-----------------+---------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(ViewLocatorRgbaUInt16 source, Region region, RgbaUInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaUInt16``   |               |
+-----------------+-----------------------------+---------------+
| ``region``      | ``Region``                  |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaUInt16``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(ViewLocatorRgbaUInt32 source, Region region, RgbaUInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaUInt32``   |               |
+-----------------+-----------------------------+---------------+
| ``region``      | ``Region``                  |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaUInt32``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(ViewLocatorRgbaDouble source, Region region, RgbaDouble threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaDouble``   |               |
+-----------------+-----------------------------+---------------+
| ``region``      | ``Region``                  |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaDouble``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotEqual(View source, Region region, object threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdNotEqual** has the following parameters:

+-----------------+--------------+---------------+
| Parameter       | Type         | Description   |
+=================+==============+===============+
| ``source``      | ``View``     |               |
+-----------------+--------------+---------------+
| ``region``      | ``Region``   |               |
+-----------------+--------------+---------------+
| ``threshold``   | ``object``   |               |
+-----------------+--------------+---------------+

It takes a source view and returns the region that contains all pixels which are not equal to the threshold. This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(ViewLocatorByte source, Region region, System.Byte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+-----------------------+---------------+
| Parameter       | Type                  | Description   |
+=================+=======================+===============+
| ``source``      | ``ViewLocatorByte``   |               |
+-----------------+-----------------------+---------------+
| ``region``      | ``Region``            |               |
+-----------------+-----------------------+---------------+
| ``threshold``   | ``System.Byte``       |               |
+-----------------+-----------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(ViewLocatorUInt16 source, Region region, System.UInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt16``   |               |
+-----------------+-------------------------+---------------+
| ``region``      | ``Region``              |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.UInt16``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(ViewLocatorUInt32 source, Region region, System.UInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt32``   |               |
+-----------------+-------------------------+---------------+
| ``region``      | ``Region``              |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.UInt32``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(ViewLocatorDouble source, Region region, System.Double threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorDouble``   |               |
+-----------------+-------------------------+---------------+
| ``region``      | ``Region``              |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.Double``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(ViewLocatorRgbByte source, Region region, RgbByte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+--------------------------+---------------+
| Parameter       | Type                     | Description   |
+=================+==========================+===============+
| ``source``      | ``ViewLocatorRgbByte``   |               |
+-----------------+--------------------------+---------------+
| ``region``      | ``Region``               |               |
+-----------------+--------------------------+---------------+
| ``threshold``   | ``RgbByte``              |               |
+-----------------+--------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(ViewLocatorRgbUInt16 source, Region region, RgbUInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbUInt16``   |               |
+-----------------+----------------------------+---------------+
| ``region``      | ``Region``                 |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbUInt16``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(ViewLocatorRgbUInt32 source, Region region, RgbUInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbUInt32``   |               |
+-----------------+----------------------------+---------------+
| ``region``      | ``Region``                 |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbUInt32``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(ViewLocatorRgbDouble source, Region region, RgbDouble threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbDouble``   |               |
+-----------------+----------------------------+---------------+
| ``region``      | ``Region``                 |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbDouble``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(ViewLocatorRgbaByte source, Region region, RgbaByte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+---------------------------+---------------+
| Parameter       | Type                      | Description   |
+=================+===========================+===============+
| ``source``      | ``ViewLocatorRgbaByte``   |               |
+-----------------+---------------------------+---------------+
| ``region``      | ``Region``                |               |
+-----------------+---------------------------+---------------+
| ``threshold``   | ``RgbaByte``              |               |
+-----------------+---------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(ViewLocatorRgbaUInt16 source, Region region, RgbaUInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaUInt16``   |               |
+-----------------+-----------------------------+---------------+
| ``region``      | ``Region``                  |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaUInt16``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(ViewLocatorRgbaUInt32 source, Region region, RgbaUInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaUInt32``   |               |
+-----------------+-----------------------------+---------------+
| ``region``      | ``Region``                  |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaUInt32``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(ViewLocatorRgbaDouble source, Region region, RgbaDouble threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaDouble``   |               |
+-----------------+-----------------------------+---------------+
| ``region``      | ``Region``                  |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaDouble``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBigger*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBigger(View source, Region region, object threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBigger** has the following parameters:

+-----------------+--------------+---------------+
| Parameter       | Type         | Description   |
+=================+==============+===============+
| ``source``      | ``View``     |               |
+-----------------+--------------+---------------+
| ``region``      | ``Region``   |               |
+-----------------+--------------+---------------+
| ``threshold``   | ``object``   |               |
+-----------------+--------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(ViewLocatorByte source, Region region, System.Byte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+-----------------------+---------------+
| Parameter       | Type                  | Description   |
+=================+=======================+===============+
| ``source``      | ``ViewLocatorByte``   |               |
+-----------------+-----------------------+---------------+
| ``region``      | ``Region``            |               |
+-----------------+-----------------------+---------------+
| ``threshold``   | ``System.Byte``       |               |
+-----------------+-----------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(ViewLocatorUInt16 source, Region region, System.UInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt16``   |               |
+-----------------+-------------------------+---------------+
| ``region``      | ``Region``              |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.UInt16``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(ViewLocatorUInt32 source, Region region, System.UInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt32``   |               |
+-----------------+-------------------------+---------------+
| ``region``      | ``Region``              |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.UInt32``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(ViewLocatorDouble source, Region region, System.Double threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorDouble``   |               |
+-----------------+-------------------------+---------------+
| ``region``      | ``Region``              |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.Double``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(ViewLocatorRgbByte source, Region region, RgbByte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+--------------------------+---------------+
| Parameter       | Type                     | Description   |
+=================+==========================+===============+
| ``source``      | ``ViewLocatorRgbByte``   |               |
+-----------------+--------------------------+---------------+
| ``region``      | ``Region``               |               |
+-----------------+--------------------------+---------------+
| ``threshold``   | ``RgbByte``              |               |
+-----------------+--------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(ViewLocatorRgbUInt16 source, Region region, RgbUInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbUInt16``   |               |
+-----------------+----------------------------+---------------+
| ``region``      | ``Region``                 |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbUInt16``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(ViewLocatorRgbUInt32 source, Region region, RgbUInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbUInt32``   |               |
+-----------------+----------------------------+---------------+
| ``region``      | ``Region``                 |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbUInt32``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(ViewLocatorRgbDouble source, Region region, RgbDouble threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbDouble``   |               |
+-----------------+----------------------------+---------------+
| ``region``      | ``Region``                 |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbDouble``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(ViewLocatorRgbaByte source, Region region, RgbaByte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+---------------------------+---------------+
| Parameter       | Type                      | Description   |
+=================+===========================+===============+
| ``source``      | ``ViewLocatorRgbaByte``   |               |
+-----------------+---------------------------+---------------+
| ``region``      | ``Region``                |               |
+-----------------+---------------------------+---------------+
| ``threshold``   | ``RgbaByte``              |               |
+-----------------+---------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(ViewLocatorRgbaUInt16 source, Region region, RgbaUInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaUInt16``   |               |
+-----------------+-----------------------------+---------------+
| ``region``      | ``Region``                  |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaUInt16``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(ViewLocatorRgbaUInt32 source, Region region, RgbaUInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaUInt32``   |               |
+-----------------+-----------------------------+---------------+
| ``region``      | ``Region``                  |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaUInt32``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(ViewLocatorRgbaDouble source, Region region, RgbaDouble threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaDouble``   |               |
+-----------------+-----------------------------+---------------+
| ``region``      | ``Region``                  |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaDouble``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBiggerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBiggerOrEqual(View source, Region region, object threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdBiggerOrEqual** has the following parameters:

+-----------------+--------------+---------------+
| Parameter       | Type         | Description   |
+=================+==============+===============+
| ``source``      | ``View``     |               |
+-----------------+--------------+---------------+
| ``region``      | ``Region``   |               |
+-----------------+--------------+---------------+
| ``threshold``   | ``object``   |               |
+-----------------+--------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(ViewLocatorByte source, Region region, System.Byte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+-----------------------+---------------+
| Parameter       | Type                  | Description   |
+=================+=======================+===============+
| ``source``      | ``ViewLocatorByte``   |               |
+-----------------+-----------------------+---------------+
| ``region``      | ``Region``            |               |
+-----------------+-----------------------+---------------+
| ``threshold``   | ``System.Byte``       |               |
+-----------------+-----------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(ViewLocatorUInt16 source, Region region, System.UInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt16``   |               |
+-----------------+-------------------------+---------------+
| ``region``      | ``Region``              |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.UInt16``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(ViewLocatorUInt32 source, Region region, System.UInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt32``   |               |
+-----------------+-------------------------+---------------+
| ``region``      | ``Region``              |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.UInt32``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(ViewLocatorDouble source, Region region, System.Double threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorDouble``   |               |
+-----------------+-------------------------+---------------+
| ``region``      | ``Region``              |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.Double``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(ViewLocatorRgbByte source, Region region, RgbByte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+--------------------------+---------------+
| Parameter       | Type                     | Description   |
+=================+==========================+===============+
| ``source``      | ``ViewLocatorRgbByte``   |               |
+-----------------+--------------------------+---------------+
| ``region``      | ``Region``               |               |
+-----------------+--------------------------+---------------+
| ``threshold``   | ``RgbByte``              |               |
+-----------------+--------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(ViewLocatorRgbUInt16 source, Region region, RgbUInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbUInt16``   |               |
+-----------------+----------------------------+---------------+
| ``region``      | ``Region``                 |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbUInt16``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(ViewLocatorRgbUInt32 source, Region region, RgbUInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbUInt32``   |               |
+-----------------+----------------------------+---------------+
| ``region``      | ``Region``                 |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbUInt32``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(ViewLocatorRgbDouble source, Region region, RgbDouble threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbDouble``   |               |
+-----------------+----------------------------+---------------+
| ``region``      | ``Region``                 |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbDouble``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(ViewLocatorRgbaByte source, Region region, RgbaByte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+---------------------------+---------------+
| Parameter       | Type                      | Description   |
+=================+===========================+===============+
| ``source``      | ``ViewLocatorRgbaByte``   |               |
+-----------------+---------------------------+---------------+
| ``region``      | ``Region``                |               |
+-----------------+---------------------------+---------------+
| ``threshold``   | ``RgbaByte``              |               |
+-----------------+---------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(ViewLocatorRgbaUInt16 source, Region region, RgbaUInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaUInt16``   |               |
+-----------------+-----------------------------+---------------+
| ``region``      | ``Region``                  |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaUInt16``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(ViewLocatorRgbaUInt32 source, Region region, RgbaUInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaUInt32``   |               |
+-----------------+-----------------------------+---------------+
| ``region``      | ``Region``                  |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaUInt32``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(ViewLocatorRgbaDouble source, Region region, RgbaDouble threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaDouble``   |               |
+-----------------+-----------------------------+---------------+
| ``region``      | ``Region``                  |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaDouble``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmaller*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmaller(View source, Region region, object threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmaller** has the following parameters:

+-----------------+--------------+---------------+
| Parameter       | Type         | Description   |
+=================+==============+===============+
| ``source``      | ``View``     |               |
+-----------------+--------------+---------------+
| ``region``      | ``Region``   |               |
+-----------------+--------------+---------------+
| ``threshold``   | ``object``   |               |
+-----------------+--------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (non inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(ViewLocatorByte source, Region region, System.Byte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+-----------------------+---------------+
| Parameter       | Type                  | Description   |
+=================+=======================+===============+
| ``source``      | ``ViewLocatorByte``   |               |
+-----------------+-----------------------+---------------+
| ``region``      | ``Region``            |               |
+-----------------+-----------------------+---------------+
| ``threshold``   | ``System.Byte``       |               |
+-----------------+-----------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(ViewLocatorUInt16 source, Region region, System.UInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt16``   |               |
+-----------------+-------------------------+---------------+
| ``region``      | ``Region``              |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.UInt16``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(ViewLocatorUInt32 source, Region region, System.UInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt32``   |               |
+-----------------+-------------------------+---------------+
| ``region``      | ``Region``              |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.UInt32``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(ViewLocatorDouble source, Region region, System.Double threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorDouble``   |               |
+-----------------+-------------------------+---------------+
| ``region``      | ``Region``              |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.Double``       |               |
+-----------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(ViewLocatorRgbByte source, Region region, RgbByte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+--------------------------+---------------+
| Parameter       | Type                     | Description   |
+=================+==========================+===============+
| ``source``      | ``ViewLocatorRgbByte``   |               |
+-----------------+--------------------------+---------------+
| ``region``      | ``Region``               |               |
+-----------------+--------------------------+---------------+
| ``threshold``   | ``RgbByte``              |               |
+-----------------+--------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(ViewLocatorRgbUInt16 source, Region region, RgbUInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbUInt16``   |               |
+-----------------+----------------------------+---------------+
| ``region``      | ``Region``                 |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbUInt16``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(ViewLocatorRgbUInt32 source, Region region, RgbUInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbUInt32``   |               |
+-----------------+----------------------------+---------------+
| ``region``      | ``Region``                 |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbUInt32``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(ViewLocatorRgbDouble source, Region region, RgbDouble threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``source``      | ``ViewLocatorRgbDouble``   |               |
+-----------------+----------------------------+---------------+
| ``region``      | ``Region``                 |               |
+-----------------+----------------------------+---------------+
| ``threshold``   | ``RgbDouble``              |               |
+-----------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(ViewLocatorRgbaByte source, Region region, RgbaByte threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+---------------------------+---------------+
| Parameter       | Type                      | Description   |
+=================+===========================+===============+
| ``source``      | ``ViewLocatorRgbaByte``   |               |
+-----------------+---------------------------+---------------+
| ``region``      | ``Region``                |               |
+-----------------+---------------------------+---------------+
| ``threshold``   | ``RgbaByte``              |               |
+-----------------+---------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(ViewLocatorRgbaUInt16 source, Region region, RgbaUInt16 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaUInt16``   |               |
+-----------------+-----------------------------+---------------+
| ``region``      | ``Region``                  |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaUInt16``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(ViewLocatorRgbaUInt32 source, Region region, RgbaUInt32 threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaUInt32``   |               |
+-----------------+-----------------------------+---------------+
| ``region``      | ``Region``                  |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaUInt32``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(ViewLocatorRgbaDouble source, Region region, RgbaDouble threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``source``      | ``ViewLocatorRgbaDouble``   |               |
+-----------------+-----------------------------+---------------+
| ``region``      | ``Region``                  |               |
+-----------------+-----------------------------+---------------+
| ``threshold``   | ``RgbaDouble``              |               |
+-----------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdSmallerOrEqual*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdSmallerOrEqual(View source, Region region, object threshold)``

This function segments a view using a threshold.

The method **SegmentThresholdSmallerOrEqual** has the following parameters:

+-----------------+--------------+---------------+
| Parameter       | Type         | Description   |
+=================+==============+===============+
| ``source``      | ``View``     |               |
+-----------------+--------------+---------------+
| ``region``      | ``Region``   |               |
+-----------------+--------------+---------------+
| ``threshold``   | ``object``   |               |
+-----------------+--------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the threshold (inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(ViewLocatorByte source, Region region, System.Byte lowerThreshold, System.Byte upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+-----------------------+---------------+
| Parameter            | Type                  | Description   |
+======================+=======================+===============+
| ``source``           | ``ViewLocatorByte``   |               |
+----------------------+-----------------------+---------------+
| ``region``           | ``Region``            |               |
+----------------------+-----------------------+---------------+
| ``lowerThreshold``   | ``System.Byte``       |               |
+----------------------+-----------------------+---------------+
| ``upperThreshold``   | ``System.Byte``       |               |
+----------------------+-----------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(ViewLocatorUInt16 source, Region region, System.UInt16 lowerThreshold, System.UInt16 upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+-------------------------+---------------+
| Parameter            | Type                    | Description   |
+======================+=========================+===============+
| ``source``           | ``ViewLocatorUInt16``   |               |
+----------------------+-------------------------+---------------+
| ``region``           | ``Region``              |               |
+----------------------+-------------------------+---------------+
| ``lowerThreshold``   | ``System.UInt16``       |               |
+----------------------+-------------------------+---------------+
| ``upperThreshold``   | ``System.UInt16``       |               |
+----------------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(ViewLocatorUInt32 source, Region region, System.UInt32 lowerThreshold, System.UInt32 upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+-------------------------+---------------+
| Parameter            | Type                    | Description   |
+======================+=========================+===============+
| ``source``           | ``ViewLocatorUInt32``   |               |
+----------------------+-------------------------+---------------+
| ``region``           | ``Region``              |               |
+----------------------+-------------------------+---------------+
| ``lowerThreshold``   | ``System.UInt32``       |               |
+----------------------+-------------------------+---------------+
| ``upperThreshold``   | ``System.UInt32``       |               |
+----------------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(ViewLocatorDouble source, Region region, System.Double lowerThreshold, System.Double upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+-------------------------+---------------+
| Parameter            | Type                    | Description   |
+======================+=========================+===============+
| ``source``           | ``ViewLocatorDouble``   |               |
+----------------------+-------------------------+---------------+
| ``region``           | ``Region``              |               |
+----------------------+-------------------------+---------------+
| ``lowerThreshold``   | ``System.Double``       |               |
+----------------------+-------------------------+---------------+
| ``upperThreshold``   | ``System.Double``       |               |
+----------------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(ViewLocatorRgbByte source, Region region, RgbByte lowerThreshold, RgbByte upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+--------------------------+---------------+
| Parameter            | Type                     | Description   |
+======================+==========================+===============+
| ``source``           | ``ViewLocatorRgbByte``   |               |
+----------------------+--------------------------+---------------+
| ``region``           | ``Region``               |               |
+----------------------+--------------------------+---------------+
| ``lowerThreshold``   | ``RgbByte``              |               |
+----------------------+--------------------------+---------------+
| ``upperThreshold``   | ``RgbByte``              |               |
+----------------------+--------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(ViewLocatorRgbUInt16 source, Region region, RgbUInt16 lowerThreshold, RgbUInt16 upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+----------------------------+---------------+
| Parameter            | Type                       | Description   |
+======================+============================+===============+
| ``source``           | ``ViewLocatorRgbUInt16``   |               |
+----------------------+----------------------------+---------------+
| ``region``           | ``Region``                 |               |
+----------------------+----------------------------+---------------+
| ``lowerThreshold``   | ``RgbUInt16``              |               |
+----------------------+----------------------------+---------------+
| ``upperThreshold``   | ``RgbUInt16``              |               |
+----------------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(ViewLocatorRgbUInt32 source, Region region, RgbUInt32 lowerThreshold, RgbUInt32 upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+----------------------------+---------------+
| Parameter            | Type                       | Description   |
+======================+============================+===============+
| ``source``           | ``ViewLocatorRgbUInt32``   |               |
+----------------------+----------------------------+---------------+
| ``region``           | ``Region``                 |               |
+----------------------+----------------------------+---------------+
| ``lowerThreshold``   | ``RgbUInt32``              |               |
+----------------------+----------------------------+---------------+
| ``upperThreshold``   | ``RgbUInt32``              |               |
+----------------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(ViewLocatorRgbDouble source, Region region, RgbDouble lowerThreshold, RgbDouble upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+----------------------------+---------------+
| Parameter            | Type                       | Description   |
+======================+============================+===============+
| ``source``           | ``ViewLocatorRgbDouble``   |               |
+----------------------+----------------------------+---------------+
| ``region``           | ``Region``                 |               |
+----------------------+----------------------------+---------------+
| ``lowerThreshold``   | ``RgbDouble``              |               |
+----------------------+----------------------------+---------------+
| ``upperThreshold``   | ``RgbDouble``              |               |
+----------------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(ViewLocatorRgbaByte source, Region region, RgbaByte lowerThreshold, RgbaByte upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+---------------------------+---------------+
| Parameter            | Type                      | Description   |
+======================+===========================+===============+
| ``source``           | ``ViewLocatorRgbaByte``   |               |
+----------------------+---------------------------+---------------+
| ``region``           | ``Region``                |               |
+----------------------+---------------------------+---------------+
| ``lowerThreshold``   | ``RgbaByte``              |               |
+----------------------+---------------------------+---------------+
| ``upperThreshold``   | ``RgbaByte``              |               |
+----------------------+---------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(ViewLocatorRgbaUInt16 source, Region region, RgbaUInt16 lowerThreshold, RgbaUInt16 upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+-----------------------------+---------------+
| Parameter            | Type                        | Description   |
+======================+=============================+===============+
| ``source``           | ``ViewLocatorRgbaUInt16``   |               |
+----------------------+-----------------------------+---------------+
| ``region``           | ``Region``                  |               |
+----------------------+-----------------------------+---------------+
| ``lowerThreshold``   | ``RgbaUInt16``              |               |
+----------------------+-----------------------------+---------------+
| ``upperThreshold``   | ``RgbaUInt16``              |               |
+----------------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(ViewLocatorRgbaUInt32 source, Region region, RgbaUInt32 lowerThreshold, RgbaUInt32 upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+-----------------------------+---------------+
| Parameter            | Type                        | Description   |
+======================+=============================+===============+
| ``source``           | ``ViewLocatorRgbaUInt32``   |               |
+----------------------+-----------------------------+---------------+
| ``region``           | ``Region``                  |               |
+----------------------+-----------------------------+---------------+
| ``lowerThreshold``   | ``RgbaUInt32``              |               |
+----------------------+-----------------------------+---------------+
| ``upperThreshold``   | ``RgbaUInt32``              |               |
+----------------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(ViewLocatorRgbaDouble source, Region region, RgbaDouble lowerThreshold, RgbaDouble upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+-----------------------------+---------------+
| Parameter            | Type                        | Description   |
+======================+=============================+===============+
| ``source``           | ``ViewLocatorRgbaDouble``   |               |
+----------------------+-----------------------------+---------------+
| ``region``           | ``Region``                  |               |
+----------------------+-----------------------------+---------------+
| ``lowerThreshold``   | ``RgbaDouble``              |               |
+----------------------+-----------------------------+---------------+
| ``upperThreshold``   | ``RgbaDouble``              |               |
+----------------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdBetween(View source, Region region, object lowerThreshold, object upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdBetween** has the following parameters:

+----------------------+--------------+---------------+
| Parameter            | Type         | Description   |
+======================+==============+===============+
| ``source``           | ``View``     |               |
+----------------------+--------------+---------------+
| ``region``           | ``Region``   |               |
+----------------------+--------------+---------------+
| ``lowerThreshold``   | ``object``   |               |
+----------------------+--------------+---------------+
| ``upperThreshold``   | ``object``   |               |
+----------------------+--------------+---------------+

It takes a source view and returns the region that contains all pixels which are bigger than the lower threshold while smaller than the upper threshold (both thresholds inclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(ViewLocatorByte source, Region region, System.Byte lowerThreshold, System.Byte upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+-----------------------+---------------+
| Parameter            | Type                  | Description   |
+======================+=======================+===============+
| ``source``           | ``ViewLocatorByte``   |               |
+----------------------+-----------------------+---------------+
| ``region``           | ``Region``            |               |
+----------------------+-----------------------+---------------+
| ``lowerThreshold``   | ``System.Byte``       |               |
+----------------------+-----------------------+---------------+
| ``upperThreshold``   | ``System.Byte``       |               |
+----------------------+-----------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(ViewLocatorUInt16 source, Region region, System.UInt16 lowerThreshold, System.UInt16 upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+-------------------------+---------------+
| Parameter            | Type                    | Description   |
+======================+=========================+===============+
| ``source``           | ``ViewLocatorUInt16``   |               |
+----------------------+-------------------------+---------------+
| ``region``           | ``Region``              |               |
+----------------------+-------------------------+---------------+
| ``lowerThreshold``   | ``System.UInt16``       |               |
+----------------------+-------------------------+---------------+
| ``upperThreshold``   | ``System.UInt16``       |               |
+----------------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(ViewLocatorUInt32 source, Region region, System.UInt32 lowerThreshold, System.UInt32 upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+-------------------------+---------------+
| Parameter            | Type                    | Description   |
+======================+=========================+===============+
| ``source``           | ``ViewLocatorUInt32``   |               |
+----------------------+-------------------------+---------------+
| ``region``           | ``Region``              |               |
+----------------------+-------------------------+---------------+
| ``lowerThreshold``   | ``System.UInt32``       |               |
+----------------------+-------------------------+---------------+
| ``upperThreshold``   | ``System.UInt32``       |               |
+----------------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(ViewLocatorDouble source, Region region, System.Double lowerThreshold, System.Double upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+-------------------------+---------------+
| Parameter            | Type                    | Description   |
+======================+=========================+===============+
| ``source``           | ``ViewLocatorDouble``   |               |
+----------------------+-------------------------+---------------+
| ``region``           | ``Region``              |               |
+----------------------+-------------------------+---------------+
| ``lowerThreshold``   | ``System.Double``       |               |
+----------------------+-------------------------+---------------+
| ``upperThreshold``   | ``System.Double``       |               |
+----------------------+-------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(ViewLocatorRgbByte source, Region region, RgbByte lowerThreshold, RgbByte upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+--------------------------+---------------+
| Parameter            | Type                     | Description   |
+======================+==========================+===============+
| ``source``           | ``ViewLocatorRgbByte``   |               |
+----------------------+--------------------------+---------------+
| ``region``           | ``Region``               |               |
+----------------------+--------------------------+---------------+
| ``lowerThreshold``   | ``RgbByte``              |               |
+----------------------+--------------------------+---------------+
| ``upperThreshold``   | ``RgbByte``              |               |
+----------------------+--------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(ViewLocatorRgbUInt16 source, Region region, RgbUInt16 lowerThreshold, RgbUInt16 upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+----------------------------+---------------+
| Parameter            | Type                       | Description   |
+======================+============================+===============+
| ``source``           | ``ViewLocatorRgbUInt16``   |               |
+----------------------+----------------------------+---------------+
| ``region``           | ``Region``                 |               |
+----------------------+----------------------------+---------------+
| ``lowerThreshold``   | ``RgbUInt16``              |               |
+----------------------+----------------------------+---------------+
| ``upperThreshold``   | ``RgbUInt16``              |               |
+----------------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(ViewLocatorRgbUInt32 source, Region region, RgbUInt32 lowerThreshold, RgbUInt32 upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+----------------------------+---------------+
| Parameter            | Type                       | Description   |
+======================+============================+===============+
| ``source``           | ``ViewLocatorRgbUInt32``   |               |
+----------------------+----------------------------+---------------+
| ``region``           | ``Region``                 |               |
+----------------------+----------------------------+---------------+
| ``lowerThreshold``   | ``RgbUInt32``              |               |
+----------------------+----------------------------+---------------+
| ``upperThreshold``   | ``RgbUInt32``              |               |
+----------------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(ViewLocatorRgbDouble source, Region region, RgbDouble lowerThreshold, RgbDouble upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+----------------------------+---------------+
| Parameter            | Type                       | Description   |
+======================+============================+===============+
| ``source``           | ``ViewLocatorRgbDouble``   |               |
+----------------------+----------------------------+---------------+
| ``region``           | ``Region``                 |               |
+----------------------+----------------------------+---------------+
| ``lowerThreshold``   | ``RgbDouble``              |               |
+----------------------+----------------------------+---------------+
| ``upperThreshold``   | ``RgbDouble``              |               |
+----------------------+----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(ViewLocatorRgbaByte source, Region region, RgbaByte lowerThreshold, RgbaByte upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+---------------------------+---------------+
| Parameter            | Type                      | Description   |
+======================+===========================+===============+
| ``source``           | ``ViewLocatorRgbaByte``   |               |
+----------------------+---------------------------+---------------+
| ``region``           | ``Region``                |               |
+----------------------+---------------------------+---------------+
| ``lowerThreshold``   | ``RgbaByte``              |               |
+----------------------+---------------------------+---------------+
| ``upperThreshold``   | ``RgbaByte``              |               |
+----------------------+---------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(ViewLocatorRgbaUInt16 source, Region region, RgbaUInt16 lowerThreshold, RgbaUInt16 upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+-----------------------------+---------------+
| Parameter            | Type                        | Description   |
+======================+=============================+===============+
| ``source``           | ``ViewLocatorRgbaUInt16``   |               |
+----------------------+-----------------------------+---------------+
| ``region``           | ``Region``                  |               |
+----------------------+-----------------------------+---------------+
| ``lowerThreshold``   | ``RgbaUInt16``              |               |
+----------------------+-----------------------------+---------------+
| ``upperThreshold``   | ``RgbaUInt16``              |               |
+----------------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(ViewLocatorRgbaUInt32 source, Region region, RgbaUInt32 lowerThreshold, RgbaUInt32 upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+-----------------------------+---------------+
| Parameter            | Type                        | Description   |
+======================+=============================+===============+
| ``source``           | ``ViewLocatorRgbaUInt32``   |               |
+----------------------+-----------------------------+---------------+
| ``region``           | ``Region``                  |               |
+----------------------+-----------------------------+---------------+
| ``lowerThreshold``   | ``RgbaUInt32``              |               |
+----------------------+-----------------------------+---------------+
| ``upperThreshold``   | ``RgbaUInt32``              |               |
+----------------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(ViewLocatorRgbaDouble source, Region region, RgbaDouble lowerThreshold, RgbaDouble upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+-----------------------------+---------------+
| Parameter            | Type                        | Description   |
+======================+=============================+===============+
| ``source``           | ``ViewLocatorRgbaDouble``   |               |
+----------------------+-----------------------------+---------------+
| ``region``           | ``Region``                  |               |
+----------------------+-----------------------------+---------------+
| ``lowerThreshold``   | ``RgbaDouble``              |               |
+----------------------+-----------------------------+---------------+
| ``upperThreshold``   | ``RgbaDouble``              |               |
+----------------------+-----------------------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *SegmentThresholdNotBetween*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region SegmentThresholdNotBetween(View source, Region region, object lowerThreshold, object upperThreshold)``

This function segments a view using two thresholds.

The method **SegmentThresholdNotBetween** has the following parameters:

+----------------------+--------------+---------------+
| Parameter            | Type         | Description   |
+======================+==============+===============+
| ``source``           | ``View``     |               |
+----------------------+--------------+---------------+
| ``region``           | ``Region``   |               |
+----------------------+--------------+---------------+
| ``lowerThreshold``   | ``object``   |               |
+----------------------+--------------+---------------+
| ``upperThreshold``   | ``object``   |               |
+----------------------+--------------+---------------+

It takes a source view and returns the region that contains all pixels which are smaller than the lower threshold or bigger than the upper threshold (both thresholds exclusive). This is likely a region consisting of multiple objects. Use the region\_list::connection function to separate objects into multiple disconnected regions.

A region.

Method *FromView*
^^^^^^^^^^^^^^^^^

``Region FromView(ViewLocatorByte view)``

This function converts a view to a region.

The method **FromView** has the following parameters:

+-------------+-----------------------+---------------+
| Parameter   | Type                  | Description   |
+=============+=======================+===============+
| ``view``    | ``ViewLocatorByte``   |               |
+-------------+-----------------------+---------------+

The region consists of the whole view.

A region.

Method *FromView*
^^^^^^^^^^^^^^^^^

``Region FromView(ViewLocatorUInt16 view)``

This function converts a view to a region.

The method **FromView** has the following parameters:

+-------------+-------------------------+---------------+
| Parameter   | Type                    | Description   |
+=============+=========================+===============+
| ``view``    | ``ViewLocatorUInt16``   |               |
+-------------+-------------------------+---------------+

The region consists of the whole view.

A region.

Method *FromView*
^^^^^^^^^^^^^^^^^

``Region FromView(ViewLocatorUInt32 view)``

This function converts a view to a region.

The method **FromView** has the following parameters:

+-------------+-------------------------+---------------+
| Parameter   | Type                    | Description   |
+=============+=========================+===============+
| ``view``    | ``ViewLocatorUInt32``   |               |
+-------------+-------------------------+---------------+

The region consists of the whole view.

A region.

Method *FromView*
^^^^^^^^^^^^^^^^^

``Region FromView(ViewLocatorDouble view)``

This function converts a view to a region.

The method **FromView** has the following parameters:

+-------------+-------------------------+---------------+
| Parameter   | Type                    | Description   |
+=============+=========================+===============+
| ``view``    | ``ViewLocatorDouble``   |               |
+-------------+-------------------------+---------------+

The region consists of the whole view.

A region.

Method *FromView*
^^^^^^^^^^^^^^^^^

``Region FromView(ViewLocatorRgbByte view)``

This function converts a view to a region.

The method **FromView** has the following parameters:

+-------------+--------------------------+---------------+
| Parameter   | Type                     | Description   |
+=============+==========================+===============+
| ``view``    | ``ViewLocatorRgbByte``   |               |
+-------------+--------------------------+---------------+

The region consists of the whole view.

A region.

Method *FromView*
^^^^^^^^^^^^^^^^^

``Region FromView(ViewLocatorRgbUInt16 view)``

This function converts a view to a region.

The method **FromView** has the following parameters:

+-------------+----------------------------+---------------+
| Parameter   | Type                       | Description   |
+=============+============================+===============+
| ``view``    | ``ViewLocatorRgbUInt16``   |               |
+-------------+----------------------------+---------------+

The region consists of the whole view.

A region.

Method *FromView*
^^^^^^^^^^^^^^^^^

``Region FromView(ViewLocatorRgbUInt32 view)``

This function converts a view to a region.

The method **FromView** has the following parameters:

+-------------+----------------------------+---------------+
| Parameter   | Type                       | Description   |
+=============+============================+===============+
| ``view``    | ``ViewLocatorRgbUInt32``   |               |
+-------------+----------------------------+---------------+

The region consists of the whole view.

A region.

Method *FromView*
^^^^^^^^^^^^^^^^^

``Region FromView(ViewLocatorRgbDouble view)``

This function converts a view to a region.

The method **FromView** has the following parameters:

+-------------+----------------------------+---------------+
| Parameter   | Type                       | Description   |
+=============+============================+===============+
| ``view``    | ``ViewLocatorRgbDouble``   |               |
+-------------+----------------------------+---------------+

The region consists of the whole view.

A region.

Method *FromView*
^^^^^^^^^^^^^^^^^

``Region FromView(ViewLocatorRgbaByte view)``

This function converts a view to a region.

The method **FromView** has the following parameters:

+-------------+---------------------------+---------------+
| Parameter   | Type                      | Description   |
+=============+===========================+===============+
| ``view``    | ``ViewLocatorRgbaByte``   |               |
+-------------+---------------------------+---------------+

The region consists of the whole view.

A region.

Method *FromView*
^^^^^^^^^^^^^^^^^

``Region FromView(ViewLocatorRgbaUInt16 view)``

This function converts a view to a region.

The method **FromView** has the following parameters:

+-------------+-----------------------------+---------------+
| Parameter   | Type                        | Description   |
+=============+=============================+===============+
| ``view``    | ``ViewLocatorRgbaUInt16``   |               |
+-------------+-----------------------------+---------------+

The region consists of the whole view.

A region.

Method *FromView*
^^^^^^^^^^^^^^^^^

``Region FromView(ViewLocatorRgbaUInt32 view)``

This function converts a view to a region.

The method **FromView** has the following parameters:

+-------------+-----------------------------+---------------+
| Parameter   | Type                        | Description   |
+=============+=============================+===============+
| ``view``    | ``ViewLocatorRgbaUInt32``   |               |
+-------------+-----------------------------+---------------+

The region consists of the whole view.

A region.

Method *FromView*
^^^^^^^^^^^^^^^^^

``Region FromView(ViewLocatorRgbaDouble view)``

This function converts a view to a region.

The method **FromView** has the following parameters:

+-------------+-----------------------------+---------------+
| Parameter   | Type                        | Description   |
+=============+=============================+===============+
| ``view``    | ``ViewLocatorRgbaDouble``   |               |
+-------------+-----------------------------+---------------+

The region consists of the whole view.

A region.

Method *FromView*
^^^^^^^^^^^^^^^^^

``Region FromView(View view)``

This function converts a view to a region.

The method **FromView** has the following parameters:

+-------------+------------+---------------+
| Parameter   | Type       | Description   |
+=============+============+===============+
| ``view``    | ``View``   |               |
+-------------+------------+---------------+

The region consists of the whole view.

A region.

Method *FromPoint*
^^^^^^^^^^^^^^^^^^

``Region FromPoint(PointInt32 point)``

This function converts a point to a region.

The method **FromPoint** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``point``   | ``PointInt32``   |               |
+-------------+------------------+---------------+

It takes a point and returns the corresponding region. The region encompasses just the point.

The point coordinates are with respect to the middle of a pixel, whereas the integer region coordinates are at the pixel bounds. Thus, there is an offset of 0.5 in horizontal and vertical directions to go from geometric to region coordinates.

A region.

Method *FromPoint*
^^^^^^^^^^^^^^^^^^

``Region FromPoint(PointDouble point)``

This function converts a point to a region.

The method **FromPoint** has the following parameters:

+-------------+-------------------+---------------+
| Parameter   | Type              | Description   |
+=============+===================+===============+
| ``point``   | ``PointDouble``   |               |
+-------------+-------------------+---------------+

It takes a point and returns the corresponding region. The region encompasses just the point.

The point coordinates are with respect to the middle of a pixel, whereas the integer region coordinates are at the pixel bounds. Thus, there is an offset of 0.5 in horizontal and vertical directions to go from geometric to region coordinates.

A region.

Method *FromPoint*
^^^^^^^^^^^^^^^^^^

``Region FromPoint(Point point)``

This function converts a point to a region.

The method **FromPoint** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``point``   | ``Point``   |               |
+-------------+-------------+---------------+

It takes a point and returns the corresponding region. The region encompasses just the point.

The point coordinates are with respect to the middle of a pixel, whereas the integer region coordinates are at the pixel bounds. Thus, there is an offset of 0.5 in horizontal and vertical directions to go from geometric to region coordinates.

A region.

Method *FromLineSegment*
^^^^^^^^^^^^^^^^^^^^^^^^

``Region FromLineSegment(LineSegmentInt32 lineSegment)``

This function converts a line\_segment to a region.

The method **FromLineSegment** has the following parameters:

+-------------------+------------------------+---------------+
| Parameter         | Type                   | Description   |
+===================+========================+===============+
| ``lineSegment``   | ``LineSegmentInt32``   |               |
+-------------------+------------------------+---------------+

It takes a line\_segment and returns the corresponding region. The region encompasses just the line\_segment.

The line\_segment coordinates are with respect to the middle of a pixel, whereas the integer region coordinates are at the pixel bounds. Thus, there is an offset of 0.5 in horizontal and vertical directions to go from geometric to region coordinates.

A region.

Method *FromLineSegment*
^^^^^^^^^^^^^^^^^^^^^^^^

``Region FromLineSegment(LineSegmentDouble lineSegment)``

This function converts a line\_segment to a region.

The method **FromLineSegment** has the following parameters:

+-------------------+-------------------------+---------------+
| Parameter         | Type                    | Description   |
+===================+=========================+===============+
| ``lineSegment``   | ``LineSegmentDouble``   |               |
+-------------------+-------------------------+---------------+

It takes a line\_segment and returns the corresponding region. The region encompasses just the line\_segment.

The line\_segment coordinates are with respect to the middle of a pixel, whereas the integer region coordinates are at the pixel bounds. Thus, there is an offset of 0.5 in horizontal and vertical directions to go from geometric to region coordinates.

A region.

Method *FromLineSegment*
^^^^^^^^^^^^^^^^^^^^^^^^

``Region FromLineSegment(LineSegment lineSegment)``

This function converts a line\_segment to a region.

The method **FromLineSegment** has the following parameters:

+-------------------+-------------------+---------------+
| Parameter         | Type              | Description   |
+===================+===================+===============+
| ``lineSegment``   | ``LineSegment``   |               |
+-------------------+-------------------+---------------+

It takes a line\_segment and returns the corresponding region. The region encompasses just the line\_segment.

The line\_segment coordinates are with respect to the middle of a pixel, whereas the integer region coordinates are at the pixel bounds. Thus, there is an offset of 0.5 in horizontal and vertical directions to go from geometric to region coordinates.

A region.

Method *FromBox*
^^^^^^^^^^^^^^^^

``Region FromBox(BoxInt32 box)``

This function converts a box to a region.

The method **FromBox** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``box``     | ``BoxInt32``   |               |
+-------------+----------------+---------------+

It takes a box and returns the corresponding region. The region consists of all pixels inside the box.

The box coordinates are with respect to the middle of a pixel, whereas the integer region coordinates are at the pixel bounds. Thus, there is an offset of 0.5 in horizontal and vertical directions to go from geometric to region coordinates.

A region, which may be empty.

Method *FromBox*
^^^^^^^^^^^^^^^^

``Region FromBox(BoxDouble box)``

This function converts a box to a region.

The method **FromBox** has the following parameters:

+-------------+-----------------+---------------+
| Parameter   | Type            | Description   |
+=============+=================+===============+
| ``box``     | ``BoxDouble``   |               |
+-------------+-----------------+---------------+

It takes a box and returns the corresponding region. The region consists of all pixels inside the box.

The box coordinates are with respect to the middle of a pixel, whereas the integer region coordinates are at the pixel bounds. Thus, there is an offset of 0.5 in horizontal and vertical directions to go from geometric to region coordinates.

A region, which may be empty.

Method *FromBox*
^^^^^^^^^^^^^^^^

``Region FromBox(Box box)``

This function converts a box to a region.

The method **FromBox** has the following parameters:

+-------------+-----------+---------------+
| Parameter   | Type      | Description   |
+=============+===========+===============+
| ``box``     | ``Box``   |               |
+-------------+-----------+---------------+

It takes a box and returns the corresponding region. The region consists of all pixels inside the box.

The box coordinates are with respect to the middle of a pixel, whereas the integer region coordinates are at the pixel bounds. Thus, there is an offset of 0.5 in horizontal and vertical directions to go from geometric to region coordinates.

A region, which may be empty.

Method *FromPolygon*
^^^^^^^^^^^^^^^^^^^^

``Region FromPolygon(PolylineInt32 polygon)``

This function converts a polygon to a region.

The method **FromPolygon** has the following parameters:

+---------------+---------------------+---------------+
| Parameter     | Type                | Description   |
+===============+=====================+===============+
| ``polygon``   | ``PolylineInt32``   |               |
+---------------+---------------------+---------------+

It takes a polygon and returns the corresponding region. The region consists of all pixels inside the polygon.

The polygon coordinates are with respect to the middle of a pixel, whereas the integer region coordinates are at the pixel bounds. Thus, there is an offset of 0.5 in horizontal and vertical directions to go from geometric to region coordinates.

A region.

Method *FromPolygon*
^^^^^^^^^^^^^^^^^^^^

``Region FromPolygon(PolylineDouble polygon)``

This function converts a polygon to a region.

The method **FromPolygon** has the following parameters:

+---------------+----------------------+---------------+
| Parameter     | Type                 | Description   |
+===============+======================+===============+
| ``polygon``   | ``PolylineDouble``   |               |
+---------------+----------------------+---------------+

It takes a polygon and returns the corresponding region. The region consists of all pixels inside the polygon.

The polygon coordinates are with respect to the middle of a pixel, whereas the integer region coordinates are at the pixel bounds. Thus, there is an offset of 0.5 in horizontal and vertical directions to go from geometric to region coordinates.

A region.

Method *FromPolygon*
^^^^^^^^^^^^^^^^^^^^

``Region FromPolygon(Polyline polygon)``

This function converts a polygon to a region.

The method **FromPolygon** has the following parameters:

+---------------+----------------+---------------+
| Parameter     | Type           | Description   |
+===============+================+===============+
| ``polygon``   | ``Polyline``   |               |
+---------------+----------------+---------------+

It takes a polygon and returns the corresponding region. The region consists of all pixels inside the polygon.

The polygon coordinates are with respect to the middle of a pixel, whereas the integer region coordinates are at the pixel bounds. Thus, there is an offset of 0.5 in horizontal and vertical directions to go from geometric to region coordinates.

A region.

Method *FromRectangle*
^^^^^^^^^^^^^^^^^^^^^^

``Region FromRectangle(Rectangle rectangle)``

This function converts a rectangle to a region.

The method **FromRectangle** has the following parameters:

+-----------------+-----------------+---------------+
| Parameter       | Type            | Description   |
+=================+=================+===============+
| ``rectangle``   | ``Rectangle``   |               |
+-----------------+-----------------+---------------+

It takes a rectangle and returns the corresponding region. The region consists of all pixels inside the polygon.

The rectangle coordinates are with respect to the middle of a pixel, whereas the integer region coordinates are at the pixel bounds. Thus, there is an offset of 0.5 in horizontal and vertical directions to go from geometric to region coordinates.

A region.

Method *FromTriangle*
^^^^^^^^^^^^^^^^^^^^^

``Region FromTriangle(TriangleInt32 triangle)``

This function converts a triangle to a region.

The method **FromTriangle** has the following parameters:

+----------------+---------------------+---------------+
| Parameter      | Type                | Description   |
+================+=====================+===============+
| ``triangle``   | ``TriangleInt32``   |               |
+----------------+---------------------+---------------+

It takes a triangle and returns the corresponding region. The region consists of all pixels inside the polygon.

The triangle coordinates are with respect to the middle of a pixel, whereas the integer region coordinates are at the pixel bounds. Thus, there is an offset of 0.5 in horizontal and vertical directions to go from geometric to region coordinates.

A region.

Method *FromTriangle*
^^^^^^^^^^^^^^^^^^^^^

``Region FromTriangle(TriangleDouble triangle)``

This function converts a triangle to a region.

The method **FromTriangle** has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``triangle``   | ``TriangleDouble``   |               |
+----------------+----------------------+---------------+

It takes a triangle and returns the corresponding region. The region consists of all pixels inside the polygon.

The triangle coordinates are with respect to the middle of a pixel, whereas the integer region coordinates are at the pixel bounds. Thus, there is an offset of 0.5 in horizontal and vertical directions to go from geometric to region coordinates.

A region.

Method *FromTriangle*
^^^^^^^^^^^^^^^^^^^^^

``Region FromTriangle(Triangle triangle)``

This function converts a triangle to a region.

The method **FromTriangle** has the following parameters:

+----------------+----------------+---------------+
| Parameter      | Type           | Description   |
+================+================+===============+
| ``triangle``   | ``Triangle``   |               |
+----------------+----------------+---------------+

It takes a triangle and returns the corresponding region. The region consists of all pixels inside the polygon.

The triangle coordinates are with respect to the middle of a pixel, whereas the integer region coordinates are at the pixel bounds. Thus, there is an offset of 0.5 in horizontal and vertical directions to go from geometric to region coordinates.

A region.

Method *FromQuadrilateral*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region FromQuadrilateral(QuadrilateralInt32 quadrilateral)``

This function converts a quadrilateral to a region.

The method **FromQuadrilateral** has the following parameters:

+---------------------+--------------------------+---------------+
| Parameter           | Type                     | Description   |
+=====================+==========================+===============+
| ``quadrilateral``   | ``QuadrilateralInt32``   |               |
+---------------------+--------------------------+---------------+

It takes a quadrilateral and returns the corresponding region. The region consists of all pixels inside the polygon.

The quadrilateral coordinates are with respect to the middle of a pixel, whereas the integer region coordinates are at the pixel bounds. Thus, there is an offset of 0.5 in horizontal and vertical directions to go from geometric to region coordinates.

A region.

Method *FromQuadrilateral*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region FromQuadrilateral(QuadrilateralDouble quadrilateral)``

This function converts a quadrilateral to a region.

The method **FromQuadrilateral** has the following parameters:

+---------------------+---------------------------+---------------+
| Parameter           | Type                      | Description   |
+=====================+===========================+===============+
| ``quadrilateral``   | ``QuadrilateralDouble``   |               |
+---------------------+---------------------------+---------------+

It takes a quadrilateral and returns the corresponding region. The region consists of all pixels inside the polygon.

The quadrilateral coordinates are with respect to the middle of a pixel, whereas the integer region coordinates are at the pixel bounds. Thus, there is an offset of 0.5 in horizontal and vertical directions to go from geometric to region coordinates.

A region.

Method *FromQuadrilateral*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region FromQuadrilateral(Quadrilateral quadrilateral)``

This function converts a quadrilateral to a region.

The method **FromQuadrilateral** has the following parameters:

+---------------------+---------------------+---------------+
| Parameter           | Type                | Description   |
+=====================+=====================+===============+
| ``quadrilateral``   | ``Quadrilateral``   |               |
+---------------------+---------------------+---------------+

It takes a quadrilateral and returns the corresponding region. The region consists of all pixels inside the polygon.

The quadrilateral coordinates are with respect to the middle of a pixel, whereas the integer region coordinates are at the pixel bounds. Thus, there is an offset of 0.5 in horizontal and vertical directions to go from geometric to region coordinates.

A region.

Method *FromCircle*
^^^^^^^^^^^^^^^^^^^

``Region FromCircle(Circle circle)``

This function converts a circle to a region.

The method **FromCircle** has the following parameters:

+--------------+--------------+---------------+
| Parameter    | Type         | Description   |
+==============+==============+===============+
| ``circle``   | ``Circle``   |               |
+--------------+--------------+---------------+

It takes a circle and returns the corresponding region. The region consists of all pixels inside the circle.

The circle coordinates are with respect to the middle of a pixel, whereas the integer region coordinates are at the pixel bounds. Thus, there is an offset of 0.5 in horizontal and vertical directions to go from geometric to region coordinates.

A region.

Method *FromEllipse*
^^^^^^^^^^^^^^^^^^^^

``Region FromEllipse(Ellipse ellipse)``

This function converts an ellipse to a region.

The method **FromEllipse** has the following parameters:

+---------------+---------------+---------------+
| Parameter     | Type          | Description   |
+===============+===============+===============+
| ``ellipse``   | ``Ellipse``   |               |
+---------------+---------------+---------------+

It takes an ellipse and returns the corresponding region. The region consists of all pixels inside the ellipse.

The ellipse coordinates are with respect to the middle of a pixel, whereas the integer region coordinates are at the pixel bounds. Thus, there is an offset of 0.5 in horizontal and vertical directions to go from geometric to region coordinates.

A region.

Method *CreateRectangularRegion*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region CreateRectangularRegion(VectorDouble size, Direction angle)``

Create a rectangular region centered on the origin.

The method **CreateRectangularRegion** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``size``    | ``VectorDouble``   |               |
+-------------+--------------------+---------------+
| ``angle``   | ``Direction``      |               |
+-------------+--------------------+---------------+

Regions created with this function can be used as structuring elements in morphological functions.

Method *CreateCircularRegion*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region CreateCircularRegion(System.Double diameter)``

Create a circular region centered on the origin.

The method **CreateCircularRegion** has the following parameters:

+----------------+---------------------+---------------+
| Parameter      | Type                | Description   |
+================+=====================+===============+
| ``diameter``   | ``System.Double``   |               |
+----------------+---------------------+---------------+

Regions created with this function can be used as structuring elements in morphological functions.

Method *CreateEllipticalRegion*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region CreateEllipticalRegion(VectorDouble diameter, Direction angle)``

Create an elliptical region centered on the origin.

The method **CreateEllipticalRegion** has the following parameters:

+----------------+--------------------+---------------+
| Parameter      | Type               | Description   |
+================+====================+===============+
| ``diameter``   | ``VectorDouble``   |               |
+----------------+--------------------+---------------+
| ``angle``      | ``Direction``      |               |
+----------------+--------------------+---------------+

Regions created with this function can be used as structuring elements in morphological functions.

Methods
~~~~~~~

Method *GetAreaCalibrated*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double GetAreaCalibrated(System.Double squaredScale)``

The area of the region in calibrated units.

The method **GetAreaCalibrated** has the following parameters:

+--------------------+---------------------+-------------------------------+
| Parameter          | Type                | Description                   |
+====================+=====================+===============================+
| ``squaredScale``   | ``System.Double``   | The squared scaling factor.   |
+--------------------+---------------------+-------------------------------+

Method *GetBoundsCalibrated*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``BoxDouble GetBoundsCalibrated(VectorDouble origin, System.Double scale)``

The area of the region in calibrated units.

The method **GetBoundsCalibrated** has the following parameters:

+--------------+---------------------+-----------------------+
| Parameter    | Type                | Description           |
+==============+=====================+=======================+
| ``origin``   | ``VectorDouble``    | The origin.           |
+--------------+---------------------+-----------------------+
| ``scale``    | ``System.Double``   | The scaling factor.   |
+--------------+---------------------+-----------------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Method *MinkowskiSubtraction*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region MinkowskiSubtraction(Region structuringElement)``

Minkowski subtraction.

The method **MinkowskiSubtraction** has the following parameters:

+--------------------------+--------------+----------------------------+
| Parameter                | Type         | Description                |
+==========================+==============+============================+
| ``structuringElement``   | ``Region``   | The structuring element.   |
+--------------------------+--------------+----------------------------+

This function supports inverted regions and uses DeMorgan's rules to eliminate the complement.

Method *MinkowskiAddition*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region MinkowskiAddition(Region structuringElement)``

Minkowski addition.

The method **MinkowskiAddition** has the following parameters:

+--------------------------+--------------+----------------------------+
| Parameter                | Type         | Description                |
+==========================+==============+============================+
| ``structuringElement``   | ``Region``   | The structuring element.   |
+--------------------------+--------------+----------------------------+

This function supports inverted regions and uses DeMorgan's rules to eliminate the complement.

Method *Erosion*
^^^^^^^^^^^^^^^^

``Region Erosion(Region structuringElement)``

Erode a region with a structuring element.

The method **Erosion** has the following parameters:

+--------------------------+--------------+----------------------------+
| Parameter                | Type         | Description                |
+==========================+==============+============================+
| ``structuringElement``   | ``Region``   | The structuring element.   |
+--------------------------+--------------+----------------------------+

Erosion smoothes the region boundary and decreases its area. Erosion may split connected regions in parts. Such regions logically remain one region. If you need to split them, you should use connected components.

Structuring elements should be centered on the origin.

This function supports inverted regions and uses DeMorgan's rules to eliminate the complement.

The eroded region.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``Region Dilation(Region structuringElement)``

Dilate a region with a structuring element.

The method **Dilation** has the following parameters:

+--------------------------+--------------+----------------------------+
| Parameter                | Type         | Description                |
+==========================+==============+============================+
| ``structuringElement``   | ``Region``   | The structuring element.   |
+--------------------------+--------------+----------------------------+

Dilation smoothes the region boundary and increases its area. Dilation may merge disconnected parts of regions.

Structuring elements should be centered on the origin.

This function supports inverted regions and uses DeMorgan's rules to eliminate the complement.

The dilated region.

Method *Opening*
^^^^^^^^^^^^^^^^

``Region Opening(Region structuringElement)``

Open a region with a structuring element.

The method **Opening** has the following parameters:

+--------------------------+--------------+----------------------------+
| Parameter                | Type         | Description                |
+==========================+==============+============================+
| ``structuringElement``   | ``Region``   | The structuring element.   |
+--------------------------+--------------+----------------------------+

Opening is implemented by an erosion followed by a minkowski addition. Structures smaller than the structuring element are removed and the region boundaries are smoothed.

Structuring elements should be centered on the origin.

The opened region.

Method *Closing*
^^^^^^^^^^^^^^^^

``Region Closing(Region structuringElement)``

Close a region with a structuring element.

The method **Closing** has the following parameters:

+--------------------------+--------------+----------------------------+
| Parameter                | Type         | Description                |
+==========================+==============+============================+
| ``structuringElement``   | ``Region``   | The structuring element.   |
+--------------------------+--------------+----------------------------+

Closing is implemented by a dilation followed by a minkowski subtraction. Gaps and holes smaller than the structuring element are closed and the region boundaries are smoothed.

Structuring elements should be centered on the origin.

The closed region.

Method *InnerBoundary*
^^^^^^^^^^^^^^^^^^^^^^

``Region InnerBoundary()``

Inner boundary.

The inner boundary is calculated by taking the difference of the original region minus the eroded region. The inner boundary lies exclusively inside the region.

The inner boundary as a region.

Method *OuterBoundary*
^^^^^^^^^^^^^^^^^^^^^^

``Region OuterBoundary()``

Outer boundary.

The outer boundary is calculated by taking the difference of the dilated region minus the original region. The outer boundary lies exclusively outside the region.

The outer boundary as a region.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Region MorphologicalGradient(Region structuringElement)``

Morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+--------------+----------------------------+
| Parameter                | Type         | Description                |
+==========================+==============+============================+
| ``structuringElement``   | ``Region``   | The structuring element.   |
+--------------------------+--------------+----------------------------+

The morphological gradient is calculated by taking the difference of the dilated region minus the eroded region.

The morphological gradient as a region.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``Region Translate(VectorInt32 translation)``

Translate a region.

The method **Translate** has the following parameters:

+-------------------+-------------------+---------------+
| Parameter         | Type              | Description   |
+===================+===================+===============+
| ``translation``   | ``VectorInt32``   |               |
+-------------------+-------------------+---------------+

This function returns a translated copy of a region.

Method *Center*
^^^^^^^^^^^^^^^

``Region Center()``

Centers a region on the origin.

This function returns a centered copy of a region.

Method *Transpose*
^^^^^^^^^^^^^^^^^^

``Region Transpose()``

Transpose a region.

This function returns a transposed copy of a region.

Method *Complement*
^^^^^^^^^^^^^^^^^^^

``Region Complement()``

Complement of a region.

This function calculates the set-theoretic complement of a region.

Method *Downsample*
^^^^^^^^^^^^^^^^^^^

``Region Downsample(System.Double xFactor, System.Double yFactor, Region.DownsampleRoundingMode roundingMode)``

Downsample a region.

The method **Downsample** has the following parameters:

+--------------------+-------------------------------------+---------------------------------------+
| Parameter          | Type                                | Description                           |
+====================+=====================================+=======================================+
| ``xFactor``        | ``System.Double``                   | The horizontal downsampling factor.   |
+--------------------+-------------------------------------+---------------------------------------+
| ``yFactor``        | ``System.Double``                   | The vertical downsampling factor.     |
+--------------------+-------------------------------------+---------------------------------------+
| ``roundingMode``   | ``Region.DownsampleRoundingMode``   | The rounding mode                     |
+--------------------+-------------------------------------+---------------------------------------+

The function returns a new region which is downsampled from this region.

/return The downsampled region.

Method *ToPolygonList*
^^^^^^^^^^^^^^^^^^^^^^

``PolylineListPolylineDouble ToPolygonList()``

Method *ToPointList*
^^^^^^^^^^^^^^^^^^^^

``PointListPointDouble ToPointList()``

Enumerations
~~~~~~~~~~~~

Enumeration *DownsampleRoundingMode*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum DownsampleRoundingMode``

TODO documentation missing

The enumeration **DownsampleRoundingMode** has the following constants:

+-----------------+---------+---------------+
| Name            | Value   | Description   |
+=================+=========+===============+
| ``round``       | ``0``   |               |
+-----------------+---------+---------------+
| ``castToInt``   | ``1``   |               |
+-----------------+---------+---------------+
| ``shrink``      | ``2``   |               |
+-----------------+---------+---------------+
| ``grow``        | ``3``   |               |
+-----------------+---------+---------------+

::

    enum DownsampleRoundingMode
    {
      round = 0,
      castToInt = 1,
      shrink = 2,
      grow = 3,
    };

TODO documentation missing
