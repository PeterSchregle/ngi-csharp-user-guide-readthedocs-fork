Class *TweeningBinary*
----------------------

Calculating functor that calculates a binary tweening.

**Namespace:** Ngi

**Module:** ImageProcessing

Description
~~~~~~~~~~~

This functor is used for the preset functions.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *TweeningBinary*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TweeningBinary()``

Default Constructor.
