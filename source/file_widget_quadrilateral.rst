Class *WidgetQuadrilateral*
---------------------------

A widget\_quadrilateral displays a quadrilateral on a surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetQuadrilateral** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetQuadrilateral** contains the following properties:

+----------------+-------+-------+----------------------------------------------+
| Property       | Get   | Set   | Description                                  |
+================+=======+=======+==============================================+
| ``Position``   | \*    | \*    | The position of the widget\_quadrilateral.   |
+----------------+-------+-------+----------------------------------------------+
| ``Outline``    | \*    | \*    | The outline of the widget\_quadrilateral.    |
+----------------+-------+-------+----------------------------------------------+
| ``Fill``       | \*    | \*    | The fill of the widget\_quadrilateral.       |
+----------------+-------+-------+----------------------------------------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_quadrilateral. These settings affect the look and the behavior of the widget\_quadrilateral.

The widget\_quadrilateral supports interactivity via the widget\_interactive base class, i.e. it can be selected, move around. In general it can react on mouse and keyboard interactions. A widget\_quadrilateral has interactive widget capabilities.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetQuadrilateral*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetQuadrilateral()``

Default constructor.

The default constructor creates a widget with default settings. By default the points are all at the origin, the outline is one-pixel wide black and the fill is black transparent.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``QuadrilateralDouble Position``

The position of the widget\_quadrilateral.

The position of a widget\_quadrilateral is specified with a quadrilateral<>.

Property *Outline*
^^^^^^^^^^^^^^^^^^

``PenByte Outline``

The outline of the widget\_quadrilateral.

The outline of the widget\_quadrilateral is specified with a pen<>.

Property *Fill*
^^^^^^^^^^^^^^^

``SolidColorBrushByte Fill``

The fill of the widget\_quadrilateral.

The fill of the widget\_quadrilateral is specified with a brush.
