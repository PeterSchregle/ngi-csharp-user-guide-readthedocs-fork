Class *Vector3dList*
--------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Vector3dList** implements the following interfaces:

+----------------------------------------+
| Interface                              |
+========================================+
| ``IListVector3dListVector3dVariant``   |
+----------------------------------------+

The class **Vector3dList** contains the following variant parameters:

+----------------+-----------------------------------------+
| Variant        | Description                             |
+================+=========================================+
| ``Vector3d``   | TODO no brief description for variant   |
+----------------+-----------------------------------------+

The class **Vector3dList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **Vector3dList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Vector3d*
^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Vector3d** has the following types:

+----------------------+
| Type                 |
+======================+
| ``Vector3dInt32``    |
+----------------------+
| ``Vector3dDouble``   |
+----------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Vector3dList*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``Vector3dList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``Vector3d [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``Vector3dEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(Vector3d item)``

The method **Add** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``item``    | ``Vector3d``   |               |
+-------------+----------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(Vector3d item)``

The method **Contains** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``item``    | ``Vector3d``   |               |
+-------------+----------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(Vector3d item)``

The method **Remove** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``item``    | ``Vector3d``   |               |
+-------------+----------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(Vector3d item)``

The method **IndexOf** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``item``    | ``Vector3d``   |               |
+-------------+----------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, Vector3d item)``

The method **Insert** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``item``    | ``Vector3d``       |               |
+-------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
