Class *Camera*
--------------

Camera to acquire images.

**Namespace:** Ngi

**Module:** Camera

The class **Camera** contains the following properties:

+-----------------------+-------+-------+------------------------------------------------------------------------+
| Property              | Get   | Set   | Description                                                            |
+=======================+=======+=======+========================================================================+
| ``IsOpen``            | \*    |       | Is the camera open ?                                                   |
+-----------------------+-------+-------+------------------------------------------------------------------------+
| ``Parameters``        | \*    |       |                                                                        |
+-----------------------+-------+-------+------------------------------------------------------------------------+
| ``Pixelformats``      | \*    |       | Pixelformats of the camera that are compatible with the buffer type.   |
+-----------------------+-------+-------+------------------------------------------------------------------------+
| ``PixelformatInfo``   | \*    | \*    | Get the current pixelformat of the camera.                             |
+-----------------------+-------+-------+------------------------------------------------------------------------+
| ``Aoi``               | \*    |       | The size of the acquired image.                                        |
+-----------------------+-------+-------+------------------------------------------------------------------------+

The class **Camera** contains the following methods:

+------------------------+--------------------------------------------------------+
| Method                 | Description                                            |
+========================+========================================================+
| ``Open``               | Open a specific camera as described by camera\_info.   |
+------------------------+--------------------------------------------------------+
| ``Open``               | Open a specific camera as described by camera\_info.   |
+------------------------+--------------------------------------------------------+
| ``Close``              |                                                        |
+------------------------+--------------------------------------------------------+
| ``StartAcquisition``   | Start the acquisition.                                 |
+------------------------+--------------------------------------------------------+
| ``StopAcquisition``    | Stop the acquisition.                                  |
+------------------------+--------------------------------------------------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Camera*
^^^^^^^^^^^^^^^^^^^^

``Camera()``

Properties
~~~~~~~~~~

Property *IsOpen*
^^^^^^^^^^^^^^^^^

``System.Boolean IsOpen``

Is the camera open ?

Property *Parameters*
^^^^^^^^^^^^^^^^^^^^^

``CameraParameterBaseList Parameters``

Property *Pixelformats*
^^^^^^^^^^^^^^^^^^^^^^^

``CameraPixelformatInfoList Pixelformats``

Pixelformats of the camera that are compatible with the buffer type.

Camera must be open.

Property *PixelformatInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``CameraPixelformatInfo PixelformatInfo``

Get the current pixelformat of the camera.

Property *Aoi*
^^^^^^^^^^^^^^

``BoxInt32 Aoi``

The size of the acquired image.

Only the size of the box is relevant.

Methods
~~~~~~~

Method *Open*
^^^^^^^^^^^^^

``void Open(CameraInfo cameraInfo)``

Open a specific camera as described by camera\_info.

The method **Open** has the following parameters:

+------------------+------------------+---------------------------------+
| Parameter        | Type             | Description                     |
+==================+==================+=================================+
| ``cameraInfo``   | ``CameraInfo``   | Specifies the camera to open.   |
+------------------+------------------+---------------------------------+

Method *Open*
^^^^^^^^^^^^^

``void Open(CameraInfo cameraInfo, CameraPixelformatInfo pixelformat)``

Open a specific camera as described by camera\_info.

The method **Open** has the following parameters:

+-------------------+-----------------------------+-----------------------------------------+
| Parameter         | Type                        | Description                             |
+===================+=============================+=========================================+
| ``cameraInfo``    | ``CameraInfo``              | Specifies the camera to open.           |
+-------------------+-----------------------------+-----------------------------------------+
| ``pixelformat``   | ``CameraPixelformatInfo``   | Specifies the pixelformat to be used.   |
+-------------------+-----------------------------+-----------------------------------------+

May fail if not compatible with image\_type

Method *Close*
^^^^^^^^^^^^^^

``void Close()``

Method *StartAcquisition*
^^^^^^^^^^^^^^^^^^^^^^^^^

``void StartAcquisition(System.Int32 numberOfFrames)``

Start the acquisition.

The method **StartAcquisition** has the following parameters:

+----------------------+--------------------+---------------------------------------------+
| Parameter            | Type               | Description                                 |
+======================+====================+=============================================+
| ``numberOfFrames``   | ``System.Int32``   | Number of frames to acquire. 0 to acquire   |
+----------------------+--------------------+---------------------------------------------+

stop\_acquisition has always to be called, even if a finite number of frames is acquired.

Method *StopAcquisition*
^^^^^^^^^^^^^^^^^^^^^^^^

``void StopAcquisition()``

Stop the acquisition.

Always call stop\_acquisition when done.

Events
~~~~~~

Event *CameraBuffer*
^^^^^^^^^^^^^^^^^^^^

``void CameraBuffer(CameraBuffer cameraBuffer)``

TODO no brief description for variant

The event **CameraBuffer** has the following parameters:

+--------------------+--------------------+---------------+
| Parameter          | Type               | Description   |
+====================+====================+===============+
| ``cameraBuffer``   | ``CameraBuffer``   |               |
+--------------------+--------------------+---------------+

TODO no description for variant
