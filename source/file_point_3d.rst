Class *Point3d*
---------------

A point in three-dimensional euclidean space.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Point3d** implements the following interfaces:

+-------------------------------------------------+
| Interface                                       |
+=================================================+
| ``IEquatablePoint3dPoint3dCoordinateVariant``   |
+-------------------------------------------------+
| ``ISerializable``                               |
+-------------------------------------------------+
| ``INotifyPropertyChanged``                      |
+-------------------------------------------------+

The class **Point3d** contains the following variant parameters:

+------------------+-----------------------------------------+
| Variant          | Description                             |
+==================+=========================================+
| ``Coordinate``   | TODO no brief description for variant   |
+------------------+-----------------------------------------+

The class **Point3d** contains the following properties:

+------------+-------+-------+-------------------------------------------+
| Property   | Get   | Set   | Description                               |
+============+=======+=======+===========================================+
| ``X``      | \*    | \*    | The horizontal coordinate of the point.   |
+------------+-------+-------+-------------------------------------------+
| ``Y``      | \*    | \*    | The vertical coordinate of the point.     |
+------------+-------+-------+-------------------------------------------+
| ``Z``      | \*    | \*    | The planar coordinate of the point.       |
+------------+-------+-------+-------------------------------------------+

The class **Point3d** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

A point\_3d can be seen as a vector rooted at the origin in three-dimensional space.

The following operations are implemented: operator== : comparison for equality. operator != : comparison for inequality. operator- : negation, directly implemented. operator+= : addition, directly implemented. operator+ : addition, implemented through boost::additive operator-= : subtraction, directly implemented. operator- : subtraction, implemented through boost::additive operator\ *= : multiplication, directly implemented. operator* : multiplication, implemented through boost::multiplicative operator/= : division, directly implemented. operator/ : division, implemented through boost::multiplicative

Variants
~~~~~~~~

Variant *Coordinate*
^^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Coordinate** has the following types:

+--------------+
| Type         |
+==============+
| ``Int32``    |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Point3d*
^^^^^^^^^^^^^^^^^^^^^

``Point3d()``

Constructs a point\_3d with specific coordinates.

If no parameters are passed, a point with zero coordinates is constructed.

Constructors
~~~~~~~~~~~~

Constructor *Point3d*
^^^^^^^^^^^^^^^^^^^^^

``Point3d(System.Object x, System.Object y, System.Object z)``

Constructs a point\_3d with specific coordinates of different type.

The constructor has the following parameters:

+-------------+---------------------+------------------------------+
| Parameter   | Type                | Description                  |
+=============+=====================+==============================+
| ``x``       | ``System.Object``   | The horizontal coordinate.   |
+-------------+---------------------+------------------------------+
| ``y``       | ``System.Object``   | The vertical coordinate.     |
+-------------+---------------------+------------------------------+
| ``z``       | ``System.Object``   | The planar coordinate.       |
+-------------+---------------------+------------------------------+

Properties
~~~~~~~~~~

Property *X*
^^^^^^^^^^^^

``System.Object X``

The horizontal coordinate of the point.

Property *Y*
^^^^^^^^^^^^

``System.Object Y``

The vertical coordinate of the point.

Property *Z*
^^^^^^^^^^^^

``System.Object Z``

The planar coordinate of the point.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO documentation missing

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO documentation missing
