Class *Box*
-----------

A box in two-dimensional euclidean space.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Box** implements the following interfaces:

+-----------------------------------------+
| Interface                               |
+=========================================+
| ``IEquatableBoxBoxCoordinateVariant``   |
+-----------------------------------------+
| ``ISerializable``                       |
+-----------------------------------------+
| ``INotifyPropertyChanged``              |
+-----------------------------------------+

The class **Box** contains the following variant parameters:

+------------------+---------------------------+
| Variant          | Description               |
+==================+===========================+
| ``Coordinate``   | The coordinate variant.   |
+------------------+---------------------------+

The class **Box** contains the following properties:

+-------------------+-------+-------+----------------------------------------------+
| Property          | Get   | Set   | Description                                  |
+===================+=======+=======+==============================================+
| ``Origin``        | \*    | \*    | The origin of the box.                       |
+-------------------+-------+-------+----------------------------------------------+
| ``Size``          | \*    | \*    | The size of the box.                         |
+-------------------+-------+-------+----------------------------------------------+
| ``Left``          | \*    |       | The left coordinate of the box.              |
+-------------------+-------+-------+----------------------------------------------+
| ``Top``           | \*    |       | The top coordinate of the box.               |
+-------------------+-------+-------+----------------------------------------------+
| ``Right``         | \*    |       | The right coordinate of the box.             |
+-------------------+-------+-------+----------------------------------------------+
| ``Bottom``        | \*    |       | The bottom coordinate of the box.            |
+-------------------+-------+-------+----------------------------------------------+
| ``Width``         | \*    |       | The width of the box.                        |
+-------------------+-------+-------+----------------------------------------------+
| ``Height``        | \*    |       | The height of the box.                       |
+-------------------+-------+-------+----------------------------------------------+
| ``TopLeft``       | \*    |       | The top left point of the box.               |
+-------------------+-------+-------+----------------------------------------------+
| ``TopRight``      | \*    |       | The top right point of the box.              |
+-------------------+-------+-------+----------------------------------------------+
| ``BottomLeft``    | \*    |       | The bottom left point of the box.            |
+-------------------+-------+-------+----------------------------------------------+
| ``BottomRight``   | \*    |       | The bottom right point of the box.           |
+-------------------+-------+-------+----------------------------------------------+
| ``Center``        | \*    |       | The center point of the box.                 |
+-------------------+-------+-------+----------------------------------------------+
| ``IsEmpty``       | \*    |       | True if the box is empty, false otherwise.   |
+-------------------+-------+-------+----------------------------------------------+
| ``Area``          | \*    |       | The area of the box.                         |
+-------------------+-------+-------+----------------------------------------------+
| ``Perimeter``     | \*    |       | The perimeter of the box.                    |
+-------------------+-------+-------+----------------------------------------------+

The class **Box** contains the following methods:

+--------------------+---------------------------------------------------------+
| Method             | Description                                             |
+====================+=========================================================+
| ``Offset``         | Offset the box.                                         |
+--------------------+---------------------------------------------------------+
| ``Inflate``        | Inflate the box.                                        |
+--------------------+---------------------------------------------------------+
| ``Inflate``        | Inflate the box.                                        |
+--------------------+---------------------------------------------------------+
| ``Unite``          | Calculate the union of this and another box.            |
+--------------------+---------------------------------------------------------+
| ``Intersect``      | Calculate the intersection of this and another box.     |
+--------------------+---------------------------------------------------------+
| ``Move``           | Move box.                                               |
+--------------------+---------------------------------------------------------+
| ``GetNormalize``   |                                                         |
+--------------------+---------------------------------------------------------+
| ``Zoom``           | Zoom the box.                                           |
+--------------------+---------------------------------------------------------+
| ``Zoom``           | Zoom the box.                                           |
+--------------------+---------------------------------------------------------+
| ``Contains``       | Test if point is inside the box.                        |
+--------------------+---------------------------------------------------------+
| ``ToString``       | Provide string representation for debugging purposes.   |
+--------------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

The box is axis- parallel.

The following operations are implemented: operator+= : addition, directly implemented. operator+ : addition, implemented through boost::additive operator-= : subtraction, directly implemented. operator- : subtraction, implemented through boost::additive

Variants
~~~~~~~~

Variant *Coordinate*
^^^^^^^^^^^^^^^^^^^^

The coordinate variant.

The variant parameter **Coordinate** has the following types:

+--------------+
| Type         |
+==============+
| ``Int32``    |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

The box can have either integer or floating point coordinates.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Box*
^^^^^^^^^^^^^^^^^

``Box()``

Default constructor.

Default construct a box at origin zero (0, 0) and size zero (0 x 0).

Constructors
~~~~~~~~~~~~

Constructor *Box*
^^^^^^^^^^^^^^^^^

``Box(Point origin, Vector size)``

Standard constructor.

The constructor has the following parameters:

+--------------+--------------+---------------+
| Parameter    | Type         | Description   |
+==============+==============+===============+
| ``origin``   | ``Point``    | The origin.   |
+--------------+--------------+---------------+
| ``size``     | ``Vector``   | The vector.   |
+--------------+--------------+---------------+

Construct a box from a point and a size.

Constructor *Box*
^^^^^^^^^^^^^^^^^

``Box(Point first, Point second)``

Standard constructor.

The constructor has the following parameters:

+--------------+-------------+---------------------+
| Parameter    | Type        | Description         |
+==============+=============+=====================+
| ``first``    | ``Point``   | The first point.    |
+--------------+-------------+---------------------+
| ``second``   | ``Point``   | The second point.   |
+--------------+-------------+---------------------+

Construct a box from two points.

Constructor *Box*
^^^^^^^^^^^^^^^^^

``Box(System.Object left, System.Object top, System.Object right, System.Object bottom)``

Standard constructor.

The constructor has the following parameters:

+--------------+---------------------+--------------------------+
| Parameter    | Type                | Description              |
+==============+=====================+==========================+
| ``left``     | ``System.Object``   | The left coordinate.     |
+--------------+---------------------+--------------------------+
| ``top``      | ``System.Object``   | The top coordinate.      |
+--------------+---------------------+--------------------------+
| ``right``    | ``System.Object``   | The right coordinate.    |
+--------------+---------------------+--------------------------+
| ``bottom``   | ``System.Object``   | The bottom coordinate.   |
+--------------+---------------------+--------------------------+

Construct a box from four point coordinates.

Properties
~~~~~~~~~~

Property *Origin*
^^^^^^^^^^^^^^^^^

``Point Origin``

The origin of the box.

Property *Size*
^^^^^^^^^^^^^^^

``Vector Size``

The size of the box.

Property *Left*
^^^^^^^^^^^^^^^

``System.Object Left``

The left coordinate of the box.

Property *Top*
^^^^^^^^^^^^^^

``System.Object Top``

The top coordinate of the box.

Property *Right*
^^^^^^^^^^^^^^^^

``System.Object Right``

The right coordinate of the box.

Property *Bottom*
^^^^^^^^^^^^^^^^^

``System.Object Bottom``

The bottom coordinate of the box.

Property *Width*
^^^^^^^^^^^^^^^^

``System.Object Width``

The width of the box.

Property *Height*
^^^^^^^^^^^^^^^^^

``System.Object Height``

The height of the box.

Property *TopLeft*
^^^^^^^^^^^^^^^^^^

``Point TopLeft``

The top left point of the box.

Property *TopRight*
^^^^^^^^^^^^^^^^^^^

``Point TopRight``

The top right point of the box.

Property *BottomLeft*
^^^^^^^^^^^^^^^^^^^^^

``Point BottomLeft``

The bottom left point of the box.

Property *BottomRight*
^^^^^^^^^^^^^^^^^^^^^^

``Point BottomRight``

The bottom right point of the box.

Property *Center*
^^^^^^^^^^^^^^^^^

``Point Center``

The center point of the box.

Property *IsEmpty*
^^^^^^^^^^^^^^^^^^

``System.Boolean IsEmpty``

True if the box is empty, false otherwise.

Property *Area*
^^^^^^^^^^^^^^^

``System.Double Area``

The area of the box.

Property *Perimeter*
^^^^^^^^^^^^^^^^^^^^

``System.Double Perimeter``

The perimeter of the box.

Static Methods
~~~~~~~~~~~~~~

Method *Inflate*
^^^^^^^^^^^^^^^^

``Box Inflate(Box box, Thickness thickness)``

Inflate the box.

The method **Inflate** has the following parameters:

+-----------------+-----------------+--------------------------------+
| Parameter       | Type            | Description                    |
+=================+=================+================================+
| ``box``         | ``Box``         |                                |
+-----------------+-----------------+--------------------------------+
| ``thickness``   | ``Thickness``   | The size to inflate the box.   |
+-----------------+-----------------+--------------------------------+

You can use positive size values to inflate and negative values to deflate the box.

As a result of deflation, the box may no longer be normalized after the operation. A normalized box is one where left < right and bottom < top.

The inflated box.

Method *Unite*
^^^^^^^^^^^^^^

``Box Unite(Box a, Box b)``

Calculate the union of two boxes.

The method **Unite** has the following parameters:

+-------------+-----------+---------------+
| Parameter   | Type      | Description   |
+=============+===========+===============+
| ``a``       | ``Box``   |               |
+-------------+-----------+---------------+
| ``b``       | ``Box``   |               |
+-------------+-----------+---------------+

Method *Intersect*
^^^^^^^^^^^^^^^^^^

``Box Intersect(Box a, Box b)``

Calculate the intersection of two boxes.

The method **Intersect** has the following parameters:

+-------------+-----------+---------------+
| Parameter   | Type      | Description   |
+=============+===========+===============+
| ``a``       | ``Box``   |               |
+-------------+-----------+---------------+
| ``b``       | ``Box``   |               |
+-------------+-----------+---------------+

Methods
~~~~~~~

Method *Offset*
^^^^^^^^^^^^^^^

``void Offset(Vector offset)``

Offset the box.

The method **Offset** has the following parameters:

+--------------+--------------+-------------------------------+
| Parameter    | Type         | Description                   |
+==============+==============+===============================+
| ``offset``   | ``Vector``   | The size to offset the box.   |
+--------------+--------------+-------------------------------+

You can use positive size values to move to the right/bottom or negative size values to move to the left/top.

Method *Inflate*
^^^^^^^^^^^^^^^^

``void Inflate(Thickness thickness)``

Inflate the box.

The method **Inflate** has the following parameters:

+-----------------+-----------------+--------------------------------+
| Parameter       | Type            | Description                    |
+=================+=================+================================+
| ``thickness``   | ``Thickness``   | The size to inflate the box.   |
+-----------------+-----------------+--------------------------------+

You can use positive thickness values to inflate and negative values to deflate the box.

As a result of deflation, the box may no longer be normalized after the operation. A normalized box is one where left < right and bottom < top.

Method *Inflate*
^^^^^^^^^^^^^^^^

``void Inflate(Vector size)``

Inflate the box.

The method **Inflate** has the following parameters:

+-------------+--------------+--------------------------------+
| Parameter   | Type         | Description                    |
+=============+==============+================================+
| ``size``    | ``Vector``   | The size to inflate the box.   |
+-------------+--------------+--------------------------------+

You can use positive size values to inflate and negative values to deflate the box.

As a result of deflation, the box may no longer be normalized after the operation. A normalized box is one where left < right and bottom < top.

Method *Unite*
^^^^^^^^^^^^^^

``void Unite(Box other)``

Calculate the union of this and another box.

The method **Unite** has the following parameters:

+-------------+-----------+------------------+
| Parameter   | Type      | Description      |
+=============+===========+==================+
| ``other``   | ``Box``   | The other box.   |
+-------------+-----------+------------------+

Method *Intersect*
^^^^^^^^^^^^^^^^^^

``void Intersect(Box other)``

Calculate the intersection of this and another box.

The method **Intersect** has the following parameters:

+-------------+-----------+------------------+
| Parameter   | Type      | Description      |
+=============+===========+==================+
| ``other``   | ``Box``   | The other box.   |
+-------------+-----------+------------------+

Method *Move*
^^^^^^^^^^^^^

``Box Move(Vector movement)``

Move box.

The method **Move** has the following parameters:

+----------------+--------------+--------------------+
| Parameter      | Type         | Description        |
+================+==============+====================+
| ``movement``   | ``Vector``   | Movement vector.   |
+----------------+--------------+--------------------+

Moves a box by a vector.

The moved box.

Method *GetNormalize*
^^^^^^^^^^^^^^^^^^^^^

``Box GetNormalize()``

Method *Zoom*
^^^^^^^^^^^^^

``Box Zoom(VectorInt32 zoom)``

Zoom the box.

The method **Zoom** has the following parameters:

+-------------+-------------------+--------------------+
| Parameter   | Type              | Description        |
+=============+===================+====================+
| ``zoom``    | ``VectorInt32``   | The zoom factor.   |
+-------------+-------------------+--------------------+

Method *Zoom*
^^^^^^^^^^^^^

``Box Zoom(VectorDouble zoom)``

Zoom the box.

The method **Zoom** has the following parameters:

+-------------+--------------------+--------------------+
| Parameter   | Type               | Description        |
+=============+====================+====================+
| ``zoom``    | ``VectorDouble``   | The zoom factor.   |
+-------------+--------------------+--------------------+

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(Point point)``

Test if point is inside the box.

The method **Contains** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``point``   | ``Point``   |               |
+-------------+-------------+---------------+

If the point lies on the box outline it is still considered "inside" and the function return true in this case.

Return true if point is inside the box otherwise return false.

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO documentation missing

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO documentation missing
