Class *VectorEnumerator*
------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **VectorEnumerator** implements the following interfaces:

+------------------------------------------------+
| Interface                                      |
+================================================+
| ``IEnumerator``                                |
+------------------------------------------------+
| ``IEnumeratorVectorEnumeratorVectorVariant``   |
+------------------------------------------------+

The class **VectorEnumerator** contains the following variant parameters:

+--------------+-----------------------------------------+
| Variant      | Description                             |
+==============+=========================================+
| ``Vector``   | TODO no brief description for variant   |
+--------------+-----------------------------------------+

The class **VectorEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **VectorEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Vector*
^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Vector** has the following types:

+--------------------+
| Type               |
+====================+
| ``VectorInt32``    |
+--------------------+
| ``VectorDouble``   |
+--------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Vector Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
