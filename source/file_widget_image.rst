Class *WidgetImage*
-------------------

A widget\_image displays an image on a surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetImage** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetImage** contains the following variant parameters:

+------------+-----------------------------------------+
| Variant    | Description                             |
+============+=========================================+
| ``View``   | TODO no brief description for variant   |
+------------+-----------------------------------------+

The class **WidgetImage** contains the following properties:

+----------------+-------+-------+-----------------------------------+
| Property       | Get   | Set   | Description                       |
+================+=======+=======+===================================+
| ``Position``   | \*    | \*    | The position of the widget.       |
+----------------+-------+-------+-----------------------------------+
| ``Palette``    | \*    | \*    | The palette view of the widget.   |
+----------------+-------+-------+-----------------------------------+
| ``Minimum``    | \*    | \*    | The minimum for scaling.          |
+----------------+-------+-------+-----------------------------------+
| ``Maximum``    | \*    | \*    | The maximum for scaling.          |
+----------------+-------+-------+-----------------------------------+
| ``Alpha``      | \*    | \*    | The opacity.                      |
+----------------+-------+-------+-----------------------------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_image. These settings affect the look and the behavior of the widget\_image.

The widget\_image supports automatic layout via the widget\_layoutable base class. The size of the widget is determined by the size of the image view which is displayed.

The widget\_image supports interactivity via the widget\_interactive base class, i.e. it can be selected, move around. In general it can react on mouse and keyboard interactions. This widget displays a view.

Variants
~~~~~~~~

Variant *View*
^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **View** has the following types:

+-----------------------------+
| Type                        |
+=============================+
| ``ViewLocatorByte``         |
+-----------------------------+
| ``ViewLocatorUInt16``       |
+-----------------------------+
| ``ViewLocatorUInt32``       |
+-----------------------------+
| ``ViewLocatorDouble``       |
+-----------------------------+
| ``ViewLocatorRgbByte``      |
+-----------------------------+
| ``ViewLocatorRgbUInt16``    |
+-----------------------------+
| ``ViewLocatorRgbUInt32``    |
+-----------------------------+
| ``ViewLocatorRgbDouble``    |
+-----------------------------+
| ``ViewLocatorRgbaByte``     |
+-----------------------------+
| ``ViewLocatorRgbaUInt16``   |
+-----------------------------+
| ``ViewLocatorRgbaUInt32``   |
+-----------------------------+
| ``ViewLocatorRgbaDouble``   |
+-----------------------------+
| ``ViewLocatorHlsByte``      |
+-----------------------------+
| ``ViewLocatorHlsUInt16``    |
+-----------------------------+
| ``ViewLocatorHlsDouble``    |
+-----------------------------+
| ``ViewLocatorHsiByte``      |
+-----------------------------+
| ``ViewLocatorHsiUInt16``    |
+-----------------------------+
| ``ViewLocatorHsiDouble``    |
+-----------------------------+
| ``ViewLocatorLabByte``      |
+-----------------------------+
| ``ViewLocatorLabUInt16``    |
+-----------------------------+
| ``ViewLocatorLabDouble``    |
+-----------------------------+
| ``ViewLocatorXyzByte``      |
+-----------------------------+
| ``ViewLocatorXyzUInt16``    |
+-----------------------------+
| ``ViewLocatorXyzDouble``    |
+-----------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetImage*
^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetImage()``

Default constructor.

The default constructor creates a widget with default settings.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``BoxDouble Position``

The position of the widget.

Property *Palette*
^^^^^^^^^^^^^^^^^^

``View Palette``

The palette view of the widget.

Property *Minimum*
^^^^^^^^^^^^^^^^^^

``System.Double Minimum``

The minimum for scaling.

Property *Maximum*
^^^^^^^^^^^^^^^^^^

``System.Double Maximum``

The maximum for scaling.

Property *Alpha*
^^^^^^^^^^^^^^^^

``System.Double Alpha``

The opacity.
