Class *Statistic*
-----------------

Statistic functions.

**Namespace:** Ngi

**Module:** ImageProcessing

Description
~~~~~~~~~~~

The class contains functions to calculate profiles, histograms and other statistic information from images.

Static Methods
~~~~~~~~~~~~~~

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileByte HorizontalProfile(ViewLocatorByte source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileUInt16 HorizontalProfile(ViewLocatorUInt16 source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileUInt32 HorizontalProfile(ViewLocatorUInt32 source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileDouble HorizontalProfile(ViewLocatorDouble source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileRgbByte HorizontalProfile(ViewLocatorRgbByte source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileRgbUInt16 HorizontalProfile(ViewLocatorRgbUInt16 source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileRgbUInt32 HorizontalProfile(ViewLocatorRgbUInt32 source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileRgbDouble HorizontalProfile(ViewLocatorRgbDouble source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileRgbaByte HorizontalProfile(ViewLocatorRgbaByte source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileRgbaUInt16 HorizontalProfile(ViewLocatorRgbaUInt16 source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileRgbaUInt32 HorizontalProfile(ViewLocatorRgbaUInt32 source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileRgbaDouble HorizontalProfile(ViewLocatorRgbaDouble source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileHlsByte HorizontalProfile(ViewLocatorHlsByte source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileHlsUInt16 HorizontalProfile(ViewLocatorHlsUInt16 source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileHlsDouble HorizontalProfile(ViewLocatorHlsDouble source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileHsiByte HorizontalProfile(ViewLocatorHsiByte source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileHsiUInt16 HorizontalProfile(ViewLocatorHsiUInt16 source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileHsiDouble HorizontalProfile(ViewLocatorHsiDouble source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileLabByte HorizontalProfile(ViewLocatorLabByte source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileLabUInt16 HorizontalProfile(ViewLocatorLabUInt16 source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileLabDouble HorizontalProfile(ViewLocatorLabDouble source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileXyzByte HorizontalProfile(ViewLocatorXyzByte source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileXyzUInt16 HorizontalProfile(ViewLocatorXyzUInt16 source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileXyzDouble HorizontalProfile(ViewLocatorXyzDouble source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *HorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``Profile HorizontalProfile(View source)``

Calculates an accumulated profile along the horizontal direction.

The method **HorizontalProfile** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

The algorithm progresses from left to right through the source view and averages the values per column. The result is an accumulated horizontal profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the horizontal profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the horizontal profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileByte VerticalProfile(ViewLocatorByte source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileUInt16 VerticalProfile(ViewLocatorUInt16 source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileUInt32 VerticalProfile(ViewLocatorUInt32 source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileDouble VerticalProfile(ViewLocatorDouble source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileRgbByte VerticalProfile(ViewLocatorRgbByte source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileRgbUInt16 VerticalProfile(ViewLocatorRgbUInt16 source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileRgbUInt32 VerticalProfile(ViewLocatorRgbUInt32 source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileRgbDouble VerticalProfile(ViewLocatorRgbDouble source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileRgbaByte VerticalProfile(ViewLocatorRgbaByte source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileRgbaUInt16 VerticalProfile(ViewLocatorRgbaUInt16 source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileRgbaUInt32 VerticalProfile(ViewLocatorRgbaUInt32 source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileRgbaDouble VerticalProfile(ViewLocatorRgbaDouble source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileHlsByte VerticalProfile(ViewLocatorHlsByte source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileHlsUInt16 VerticalProfile(ViewLocatorHlsUInt16 source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileHlsDouble VerticalProfile(ViewLocatorHlsDouble source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileHsiByte VerticalProfile(ViewLocatorHsiByte source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileHsiUInt16 VerticalProfile(ViewLocatorHsiUInt16 source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileHsiDouble VerticalProfile(ViewLocatorHsiDouble source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileLabByte VerticalProfile(ViewLocatorLabByte source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileLabUInt16 VerticalProfile(ViewLocatorLabUInt16 source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileLabDouble VerticalProfile(ViewLocatorLabDouble source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileXyzByte VerticalProfile(ViewLocatorXyzByte source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileXyzUInt16 VerticalProfile(ViewLocatorXyzUInt16 source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileXyzDouble VerticalProfile(ViewLocatorXyzDouble source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *VerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^

``Profile VerticalProfile(View source)``

Calculates an accumulated profile along the vertical direction.

The method **VerticalProfile** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

The algorithm progresses from top to bottom through the source view and averages the values across per row. The result is an accumulated vertical profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the height of the source view. The height of this buffer\_base will match the depth of the source view.

If the source view has several planes in z-direction, this means that the vertical profile will be calculated per plane, and the results will be put in a view line per line, which will result in a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileByte PlanarProfile(ViewLocatorByte source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileUInt16 PlanarProfile(ViewLocatorUInt16 source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileUInt32 PlanarProfile(ViewLocatorUInt32 source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileDouble PlanarProfile(ViewLocatorDouble source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileRgbByte PlanarProfile(ViewLocatorRgbByte source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileRgbUInt16 PlanarProfile(ViewLocatorRgbUInt16 source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileRgbUInt32 PlanarProfile(ViewLocatorRgbUInt32 source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileRgbDouble PlanarProfile(ViewLocatorRgbDouble source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileRgbaByte PlanarProfile(ViewLocatorRgbaByte source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileRgbaUInt16 PlanarProfile(ViewLocatorRgbaUInt16 source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileRgbaUInt32 PlanarProfile(ViewLocatorRgbaUInt32 source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileRgbaDouble PlanarProfile(ViewLocatorRgbaDouble source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileHlsByte PlanarProfile(ViewLocatorHlsByte source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileHlsUInt16 PlanarProfile(ViewLocatorHlsUInt16 source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileHlsDouble PlanarProfile(ViewLocatorHlsDouble source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileHsiByte PlanarProfile(ViewLocatorHsiByte source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileHsiUInt16 PlanarProfile(ViewLocatorHsiUInt16 source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileHsiDouble PlanarProfile(ViewLocatorHsiDouble source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileLabByte PlanarProfile(ViewLocatorLabByte source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileLabUInt16 PlanarProfile(ViewLocatorLabUInt16 source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileLabDouble PlanarProfile(ViewLocatorLabDouble source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileXyzByte PlanarProfile(ViewLocatorXyzByte source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileXyzUInt16 PlanarProfile(ViewLocatorXyzUInt16 source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileXyzDouble PlanarProfile(ViewLocatorXyzDouble source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *PlanarProfile*
^^^^^^^^^^^^^^^^^^^^^^

``Profile PlanarProfile(View source)``

Calculates an accumulated profile along the planar direction.

The method **PlanarProfile** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

The algorithm progresses from front to back through the source view and averages the values across the planes. The result is an accumulated planar profile.

The profile is returned in the form of a buffer\_base. The type of the buffer\_base has the same underlying type as the source.

The width of this buffer\_base will match the width of the source view. The height of this buffer\_base will match the height of the source view.

If the source view has several rows and columns, the result of the planar profile will be a two-dimensional view.

A buffer\_base containing the vertical profile.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramDouble CalculateHistogram(ViewLocatorByte source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramDouble CalculateHistogram(ViewLocatorUInt16 source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramDouble CalculateHistogram(ViewLocatorUInt32 source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramDouble CalculateHistogram(ViewLocatorDouble source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramRgbDouble CalculateHistogram(ViewLocatorRgbByte source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramRgbDouble CalculateHistogram(ViewLocatorRgbUInt16 source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramRgbDouble CalculateHistogram(ViewLocatorRgbUInt32 source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramRgbDouble CalculateHistogram(ViewLocatorRgbDouble source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramRgbaDouble CalculateHistogram(ViewLocatorRgbaByte source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramRgbaDouble CalculateHistogram(ViewLocatorRgbaUInt16 source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramRgbaDouble CalculateHistogram(ViewLocatorRgbaUInt32 source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramRgbaDouble CalculateHistogram(ViewLocatorRgbaDouble source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramHlsDouble CalculateHistogram(ViewLocatorHlsByte source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramHlsDouble CalculateHistogram(ViewLocatorHlsUInt16 source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramHlsDouble CalculateHistogram(ViewLocatorHlsDouble source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramHsiDouble CalculateHistogram(ViewLocatorHsiByte source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramHsiDouble CalculateHistogram(ViewLocatorHsiUInt16 source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramHsiDouble CalculateHistogram(ViewLocatorHsiDouble source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramLabDouble CalculateHistogram(ViewLocatorLabByte source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramLabDouble CalculateHistogram(ViewLocatorLabUInt16 source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramLabDouble CalculateHistogram(ViewLocatorLabDouble source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramXyzDouble CalculateHistogram(ViewLocatorXyzByte source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramXyzDouble CalculateHistogram(ViewLocatorXyzUInt16 source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramXyzDouble CalculateHistogram(ViewLocatorXyzDouble source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Histogram CalculateHistogram(View source)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels.

The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramDouble CalculateHistogram(ViewLocatorByte source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``region``   | ``Region``            |               |
+--------------+-----------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramDouble CalculateHistogram(ViewLocatorUInt16 source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramDouble CalculateHistogram(ViewLocatorUInt32 source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramDouble CalculateHistogram(ViewLocatorDouble source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramRgbDouble CalculateHistogram(ViewLocatorRgbByte source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramRgbDouble CalculateHistogram(ViewLocatorRgbUInt16 source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramRgbDouble CalculateHistogram(ViewLocatorRgbUInt32 source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramRgbDouble CalculateHistogram(ViewLocatorRgbDouble source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramRgbaDouble CalculateHistogram(ViewLocatorRgbaByte source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``region``   | ``Region``                |               |
+--------------+---------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramRgbaDouble CalculateHistogram(ViewLocatorRgbaUInt16 source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramRgbaDouble CalculateHistogram(ViewLocatorRgbaUInt32 source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramRgbaDouble CalculateHistogram(ViewLocatorRgbaDouble source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramHlsDouble CalculateHistogram(ViewLocatorHlsByte source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramHlsDouble CalculateHistogram(ViewLocatorHlsUInt16 source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramHlsDouble CalculateHistogram(ViewLocatorHlsDouble source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramHsiDouble CalculateHistogram(ViewLocatorHsiByte source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramHsiDouble CalculateHistogram(ViewLocatorHsiUInt16 source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramHsiDouble CalculateHistogram(ViewLocatorHsiDouble source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramLabDouble CalculateHistogram(ViewLocatorLabByte source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramLabDouble CalculateHistogram(ViewLocatorLabUInt16 source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramLabDouble CalculateHistogram(ViewLocatorLabDouble source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramXyzDouble CalculateHistogram(ViewLocatorXyzByte source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramXyzDouble CalculateHistogram(ViewLocatorXyzUInt16 source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramXyzDouble CalculateHistogram(ViewLocatorXyzDouble source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Histogram CalculateHistogram(View source, Region region)``

Computes the histogram.

The method **CalculateHistogram** has the following parameters:

+--------------+--------------+---------------+
| Parameter    | Type         | Description   |
+==============+==============+===============+
| ``source``   | ``View``     |               |
+--------------+--------------+---------------+
| ``region``   | ``Region``   |               |
+--------------+--------------+---------------+

The function takes the values from the source view and computes the histogram.

The histogram is returned in the form of a buffer\_base<int>.

The number of bins of the histogram defaults to an automatically calculated value from the data type of the source parameter.

The values calculated are pixel-sums that are not normalized to the total amount of pixels. The region parameter constrains the function to the region inside of the view only. The histogram buffer\_base.

Method *Minimum*
^^^^^^^^^^^^^^^^

``System.Byte Minimum(ViewLocatorByte source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``System.UInt16 Minimum(ViewLocatorUInt16 source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``System.UInt32 Minimum(ViewLocatorUInt32 source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``System.Double Minimum(ViewLocatorDouble source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``RgbByte Minimum(ViewLocatorRgbByte source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``RgbUInt16 Minimum(ViewLocatorRgbUInt16 source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``RgbUInt32 Minimum(ViewLocatorRgbUInt32 source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``RgbDouble Minimum(ViewLocatorRgbDouble source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``RgbaByte Minimum(ViewLocatorRgbaByte source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``RgbaUInt16 Minimum(ViewLocatorRgbaUInt16 source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``RgbaUInt32 Minimum(ViewLocatorRgbaUInt32 source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``RgbaDouble Minimum(ViewLocatorRgbaDouble source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``HlsByte Minimum(ViewLocatorHlsByte source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``HlsUInt16 Minimum(ViewLocatorHlsUInt16 source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``HlsDouble Minimum(ViewLocatorHlsDouble source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``HsiByte Minimum(ViewLocatorHsiByte source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``HsiUInt16 Minimum(ViewLocatorHsiUInt16 source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``HsiDouble Minimum(ViewLocatorHsiDouble source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``LabByte Minimum(ViewLocatorLabByte source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``LabUInt16 Minimum(ViewLocatorLabUInt16 source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``LabDouble Minimum(ViewLocatorLabDouble source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``XyzByte Minimum(ViewLocatorXyzByte source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``XyzUInt16 Minimum(ViewLocatorXyzUInt16 source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``XyzDouble Minimum(ViewLocatorXyzDouble source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``object Minimum(View source)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``System.Byte Minimum(ViewLocatorByte source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``region``   | ``Region``            |               |
+--------------+-----------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``System.UInt16 Minimum(ViewLocatorUInt16 source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``System.UInt32 Minimum(ViewLocatorUInt32 source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``System.Double Minimum(ViewLocatorDouble source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``RgbByte Minimum(ViewLocatorRgbByte source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``RgbUInt16 Minimum(ViewLocatorRgbUInt16 source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``RgbUInt32 Minimum(ViewLocatorRgbUInt32 source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``RgbDouble Minimum(ViewLocatorRgbDouble source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``RgbaByte Minimum(ViewLocatorRgbaByte source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``region``   | ``Region``                |               |
+--------------+---------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``RgbaUInt16 Minimum(ViewLocatorRgbaUInt16 source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``RgbaUInt32 Minimum(ViewLocatorRgbaUInt32 source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``RgbaDouble Minimum(ViewLocatorRgbaDouble source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``HlsByte Minimum(ViewLocatorHlsByte source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``HlsUInt16 Minimum(ViewLocatorHlsUInt16 source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``HlsDouble Minimum(ViewLocatorHlsDouble source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``HsiByte Minimum(ViewLocatorHsiByte source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``HsiUInt16 Minimum(ViewLocatorHsiUInt16 source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``HsiDouble Minimum(ViewLocatorHsiDouble source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``LabByte Minimum(ViewLocatorLabByte source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``LabUInt16 Minimum(ViewLocatorLabUInt16 source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``LabDouble Minimum(ViewLocatorLabDouble source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``XyzByte Minimum(ViewLocatorXyzByte source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``XyzUInt16 Minimum(ViewLocatorXyzUInt16 source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``XyzDouble Minimum(ViewLocatorXyzDouble source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *Minimum*
^^^^^^^^^^^^^^^^

``object Minimum(View source, Region region)``

Computes the minimum.

The method **Minimum** has the following parameters:

+--------------+--------------+---------------+
| Parameter    | Type         | Description   |
+==============+==============+===============+
| ``source``   | ``View``     |               |
+--------------+--------------+---------------+
| ``region``   | ``Region``   |               |
+--------------+--------------+---------------+

The minimum is the minimum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The minimum.

Method *MinimumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double MinimumFromHistogram(ViewLocatorUInt32 source)``

Computes the minimum given a histogram.

The method **MinimumFromHistogram** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

The minimum.

Method *MinimumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double MinimumFromHistogram(ViewLocatorDouble source)``

Computes the minimum given a histogram.

The method **MinimumFromHistogram** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

The minimum.

Method *MinimumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble MinimumFromHistogram(ViewLocatorRgbDouble source)``

Computes the minimum given a histogram.

The method **MinimumFromHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

The minimum.

Method *MinimumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble MinimumFromHistogram(ViewLocatorRgbaDouble source)``

Computes the minimum given a histogram.

The method **MinimumFromHistogram** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

The minimum.

Method *MinimumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble MinimumFromHistogram(ViewLocatorHlsDouble source)``

Computes the minimum given a histogram.

The method **MinimumFromHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

The minimum.

Method *MinimumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble MinimumFromHistogram(ViewLocatorHsiDouble source)``

Computes the minimum given a histogram.

The method **MinimumFromHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

The minimum.

Method *MinimumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble MinimumFromHistogram(ViewLocatorLabDouble source)``

Computes the minimum given a histogram.

The method **MinimumFromHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

The minimum.

Method *MinimumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble MinimumFromHistogram(ViewLocatorXyzDouble source)``

Computes the minimum given a histogram.

The method **MinimumFromHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

The minimum.

Method *MinimumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Object MinimumFromHistogram(View source)``

Computes the minimum given a histogram.

The method **MinimumFromHistogram** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

The minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Byte ChannelMinimum(ViewLocatorByte source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``System.UInt16 ChannelMinimum(ViewLocatorUInt16 source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``System.UInt32 ChannelMinimum(ViewLocatorUInt32 source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Double ChannelMinimum(ViewLocatorDouble source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbByte ChannelMinimum(ViewLocatorRgbByte source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbUInt16 ChannelMinimum(ViewLocatorRgbUInt16 source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbUInt32 ChannelMinimum(ViewLocatorRgbUInt32 source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble ChannelMinimum(ViewLocatorRgbDouble source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbaByte ChannelMinimum(ViewLocatorRgbaByte source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbaUInt16 ChannelMinimum(ViewLocatorRgbaUInt16 source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbaUInt32 ChannelMinimum(ViewLocatorRgbaUInt32 source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble ChannelMinimum(ViewLocatorRgbaDouble source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``HlsByte ChannelMinimum(ViewLocatorHlsByte source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``HlsUInt16 ChannelMinimum(ViewLocatorHlsUInt16 source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble ChannelMinimum(ViewLocatorHlsDouble source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``HsiByte ChannelMinimum(ViewLocatorHsiByte source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``HsiUInt16 ChannelMinimum(ViewLocatorHsiUInt16 source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble ChannelMinimum(ViewLocatorHsiDouble source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``LabByte ChannelMinimum(ViewLocatorLabByte source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``LabUInt16 ChannelMinimum(ViewLocatorLabUInt16 source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble ChannelMinimum(ViewLocatorLabDouble source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``XyzByte ChannelMinimum(ViewLocatorXyzByte source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``XyzUInt16 ChannelMinimum(ViewLocatorXyzUInt16 source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble ChannelMinimum(ViewLocatorXyzDouble source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``object ChannelMinimum(View source)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Byte ChannelMinimum(ViewLocatorByte source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``region``   | ``Region``            |               |
+--------------+-----------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``System.UInt16 ChannelMinimum(ViewLocatorUInt16 source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``System.UInt32 ChannelMinimum(ViewLocatorUInt32 source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Double ChannelMinimum(ViewLocatorDouble source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbByte ChannelMinimum(ViewLocatorRgbByte source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbUInt16 ChannelMinimum(ViewLocatorRgbUInt16 source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbUInt32 ChannelMinimum(ViewLocatorRgbUInt32 source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble ChannelMinimum(ViewLocatorRgbDouble source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbaByte ChannelMinimum(ViewLocatorRgbaByte source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``region``   | ``Region``                |               |
+--------------+---------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbaUInt16 ChannelMinimum(ViewLocatorRgbaUInt16 source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbaUInt32 ChannelMinimum(ViewLocatorRgbaUInt32 source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble ChannelMinimum(ViewLocatorRgbaDouble source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``HlsByte ChannelMinimum(ViewLocatorHlsByte source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``HlsUInt16 ChannelMinimum(ViewLocatorHlsUInt16 source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble ChannelMinimum(ViewLocatorHlsDouble source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``HsiByte ChannelMinimum(ViewLocatorHsiByte source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``HsiUInt16 ChannelMinimum(ViewLocatorHsiUInt16 source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble ChannelMinimum(ViewLocatorHsiDouble source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``LabByte ChannelMinimum(ViewLocatorLabByte source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``LabUInt16 ChannelMinimum(ViewLocatorLabUInt16 source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble ChannelMinimum(ViewLocatorLabDouble source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``XyzByte ChannelMinimum(ViewLocatorXyzByte source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``XyzUInt16 ChannelMinimum(ViewLocatorXyzUInt16 source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble ChannelMinimum(ViewLocatorXyzDouble source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *ChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^

``object ChannelMinimum(View source, Region region)``

Computes the channel-wise minimum.

The method **ChannelMinimum** has the following parameters:

+--------------+--------------+---------------+
| Parameter    | Type         | Description   |
+==============+==============+===============+
| ``source``   | ``View``     |               |
+--------------+--------------+---------------+
| ``region``   | ``Region``   |               |
+--------------+--------------+---------------+

The channel\_minimum is the channel-wise minimum value in a view. For single channel types this is the same as the minimum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise minimum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``System.Byte Maximum(ViewLocatorByte source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``System.UInt16 Maximum(ViewLocatorUInt16 source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``System.UInt32 Maximum(ViewLocatorUInt32 source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``System.Double Maximum(ViewLocatorDouble source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``RgbByte Maximum(ViewLocatorRgbByte source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``RgbUInt16 Maximum(ViewLocatorRgbUInt16 source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``RgbUInt32 Maximum(ViewLocatorRgbUInt32 source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``RgbDouble Maximum(ViewLocatorRgbDouble source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``RgbaByte Maximum(ViewLocatorRgbaByte source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``RgbaUInt16 Maximum(ViewLocatorRgbaUInt16 source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``RgbaUInt32 Maximum(ViewLocatorRgbaUInt32 source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``RgbaDouble Maximum(ViewLocatorRgbaDouble source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``HlsByte Maximum(ViewLocatorHlsByte source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``HlsUInt16 Maximum(ViewLocatorHlsUInt16 source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``HlsDouble Maximum(ViewLocatorHlsDouble source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``HsiByte Maximum(ViewLocatorHsiByte source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``HsiUInt16 Maximum(ViewLocatorHsiUInt16 source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``HsiDouble Maximum(ViewLocatorHsiDouble source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``LabByte Maximum(ViewLocatorLabByte source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``LabUInt16 Maximum(ViewLocatorLabUInt16 source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``LabDouble Maximum(ViewLocatorLabDouble source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``XyzByte Maximum(ViewLocatorXyzByte source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``XyzUInt16 Maximum(ViewLocatorXyzUInt16 source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``XyzDouble Maximum(ViewLocatorXyzDouble source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``object Maximum(View source)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``System.Byte Maximum(ViewLocatorByte source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``region``   | ``Region``            |               |
+--------------+-----------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``System.UInt16 Maximum(ViewLocatorUInt16 source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``System.UInt32 Maximum(ViewLocatorUInt32 source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``System.Double Maximum(ViewLocatorDouble source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``RgbByte Maximum(ViewLocatorRgbByte source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``RgbUInt16 Maximum(ViewLocatorRgbUInt16 source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``RgbUInt32 Maximum(ViewLocatorRgbUInt32 source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``RgbDouble Maximum(ViewLocatorRgbDouble source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``RgbaByte Maximum(ViewLocatorRgbaByte source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``region``   | ``Region``                |               |
+--------------+---------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``RgbaUInt16 Maximum(ViewLocatorRgbaUInt16 source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``RgbaUInt32 Maximum(ViewLocatorRgbaUInt32 source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``RgbaDouble Maximum(ViewLocatorRgbaDouble source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``HlsByte Maximum(ViewLocatorHlsByte source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``HlsUInt16 Maximum(ViewLocatorHlsUInt16 source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``HlsDouble Maximum(ViewLocatorHlsDouble source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``HsiByte Maximum(ViewLocatorHsiByte source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``HsiUInt16 Maximum(ViewLocatorHsiUInt16 source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``HsiDouble Maximum(ViewLocatorHsiDouble source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``LabByte Maximum(ViewLocatorLabByte source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``LabUInt16 Maximum(ViewLocatorLabUInt16 source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``LabDouble Maximum(ViewLocatorLabDouble source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``XyzByte Maximum(ViewLocatorXyzByte source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``XyzUInt16 Maximum(ViewLocatorXyzUInt16 source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``XyzDouble Maximum(ViewLocatorXyzDouble source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *Maximum*
^^^^^^^^^^^^^^^^

``object Maximum(View source, Region region)``

Computes the maximum.

The method **Maximum** has the following parameters:

+--------------+--------------+---------------+
| Parameter    | Type         | Description   |
+==============+==============+===============+
| ``source``   | ``View``     |               |
+--------------+--------------+---------------+
| ``region``   | ``Region``   |               |
+--------------+--------------+---------------+

The maximum is the maximum value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The maximum.

Method *MaximumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double MaximumFromHistogram(ViewLocatorUInt32 source)``

Computes the maximum given a histogram.

The method **MaximumFromHistogram** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

The maximum.

Method *MaximumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double MaximumFromHistogram(ViewLocatorDouble source)``

Computes the maximum given a histogram.

The method **MaximumFromHistogram** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

The maximum.

Method *MaximumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble MaximumFromHistogram(ViewLocatorRgbDouble source)``

Computes the maximum given a histogram.

The method **MaximumFromHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

The maximum.

Method *MaximumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble MaximumFromHistogram(ViewLocatorRgbaDouble source)``

Computes the maximum given a histogram.

The method **MaximumFromHistogram** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

The maximum.

Method *MaximumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble MaximumFromHistogram(ViewLocatorHlsDouble source)``

Computes the maximum given a histogram.

The method **MaximumFromHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

The maximum.

Method *MaximumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble MaximumFromHistogram(ViewLocatorHsiDouble source)``

Computes the maximum given a histogram.

The method **MaximumFromHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

The maximum.

Method *MaximumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble MaximumFromHistogram(ViewLocatorLabDouble source)``

Computes the maximum given a histogram.

The method **MaximumFromHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

The maximum.

Method *MaximumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble MaximumFromHistogram(ViewLocatorXyzDouble source)``

Computes the maximum given a histogram.

The method **MaximumFromHistogram** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

The maximum.

Method *MaximumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Object MaximumFromHistogram(View source)``

Computes the maximum given a histogram.

The method **MaximumFromHistogram** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

The maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Byte ChannelMaximum(ViewLocatorByte source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``System.UInt16 ChannelMaximum(ViewLocatorUInt16 source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``System.UInt32 ChannelMaximum(ViewLocatorUInt32 source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Double ChannelMaximum(ViewLocatorDouble source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbByte ChannelMaximum(ViewLocatorRgbByte source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbUInt16 ChannelMaximum(ViewLocatorRgbUInt16 source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbUInt32 ChannelMaximum(ViewLocatorRgbUInt32 source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble ChannelMaximum(ViewLocatorRgbDouble source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbaByte ChannelMaximum(ViewLocatorRgbaByte source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbaUInt16 ChannelMaximum(ViewLocatorRgbaUInt16 source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbaUInt32 ChannelMaximum(ViewLocatorRgbaUInt32 source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble ChannelMaximum(ViewLocatorRgbaDouble source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``HlsByte ChannelMaximum(ViewLocatorHlsByte source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``HlsUInt16 ChannelMaximum(ViewLocatorHlsUInt16 source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble ChannelMaximum(ViewLocatorHlsDouble source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``HsiByte ChannelMaximum(ViewLocatorHsiByte source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``HsiUInt16 ChannelMaximum(ViewLocatorHsiUInt16 source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble ChannelMaximum(ViewLocatorHsiDouble source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``LabByte ChannelMaximum(ViewLocatorLabByte source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``LabUInt16 ChannelMaximum(ViewLocatorLabUInt16 source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble ChannelMaximum(ViewLocatorLabDouble source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``XyzByte ChannelMaximum(ViewLocatorXyzByte source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``XyzUInt16 ChannelMaximum(ViewLocatorXyzUInt16 source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble ChannelMaximum(ViewLocatorXyzDouble source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``object ChannelMaximum(View source)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Byte ChannelMaximum(ViewLocatorByte source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``region``   | ``Region``            |               |
+--------------+-----------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``System.UInt16 ChannelMaximum(ViewLocatorUInt16 source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``System.UInt32 ChannelMaximum(ViewLocatorUInt32 source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Double ChannelMaximum(ViewLocatorDouble source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbByte ChannelMaximum(ViewLocatorRgbByte source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbUInt16 ChannelMaximum(ViewLocatorRgbUInt16 source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbUInt32 ChannelMaximum(ViewLocatorRgbUInt32 source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble ChannelMaximum(ViewLocatorRgbDouble source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbaByte ChannelMaximum(ViewLocatorRgbaByte source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``region``   | ``Region``                |               |
+--------------+---------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbaUInt16 ChannelMaximum(ViewLocatorRgbaUInt16 source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbaUInt32 ChannelMaximum(ViewLocatorRgbaUInt32 source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble ChannelMaximum(ViewLocatorRgbaDouble source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``HlsByte ChannelMaximum(ViewLocatorHlsByte source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``HlsUInt16 ChannelMaximum(ViewLocatorHlsUInt16 source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble ChannelMaximum(ViewLocatorHlsDouble source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``HsiByte ChannelMaximum(ViewLocatorHsiByte source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``HsiUInt16 ChannelMaximum(ViewLocatorHsiUInt16 source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble ChannelMaximum(ViewLocatorHsiDouble source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``LabByte ChannelMaximum(ViewLocatorLabByte source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``LabUInt16 ChannelMaximum(ViewLocatorLabUInt16 source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble ChannelMaximum(ViewLocatorLabDouble source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``XyzByte ChannelMaximum(ViewLocatorXyzByte source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``XyzUInt16 ChannelMaximum(ViewLocatorXyzUInt16 source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble ChannelMaximum(ViewLocatorXyzDouble source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *ChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^

``object ChannelMaximum(View source, Region region)``

Computes the channel-wise maximum.

The method **ChannelMaximum** has the following parameters:

+--------------+--------------+---------------+
| Parameter    | Type         | Description   |
+==============+==============+===============+
| ``source``   | ``View``     |               |
+--------------+--------------+---------------+
| ``region``   | ``Region``   |               |
+--------------+--------------+---------------+

The channel\_maximum is the channel-wise maximum value in a view. For single channel types this is the same as the maximum, but for multiple channel types there are separate minima calculated for each channel.

The region parameter constrains the function to the region inside of the view only.

The channel-wise maximum.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``System.Double Quantile(ViewLocatorByte source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``quant``    | ``System.Double``     |               |
+--------------+-----------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``System.Double Quantile(ViewLocatorUInt16 source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``quant``    | ``System.Double``       |               |
+--------------+-------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``System.Double Quantile(ViewLocatorUInt32 source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``quant``    | ``System.Double``       |               |
+--------------+-------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``System.Double Quantile(ViewLocatorDouble source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``quant``    | ``System.Double``       |               |
+--------------+-------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``RgbDouble Quantile(ViewLocatorRgbByte source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``quant``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``RgbDouble Quantile(ViewLocatorRgbUInt16 source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``quant``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``RgbDouble Quantile(ViewLocatorRgbUInt32 source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``quant``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``RgbDouble Quantile(ViewLocatorRgbDouble source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``quant``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``RgbaDouble Quantile(ViewLocatorRgbaByte source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``quant``    | ``System.Double``         |               |
+--------------+---------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``RgbaDouble Quantile(ViewLocatorRgbaUInt16 source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``quant``    | ``System.Double``           |               |
+--------------+-----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``RgbaDouble Quantile(ViewLocatorRgbaUInt32 source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``quant``    | ``System.Double``           |               |
+--------------+-----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``RgbaDouble Quantile(ViewLocatorRgbaDouble source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``quant``    | ``System.Double``           |               |
+--------------+-----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``HlsDouble Quantile(ViewLocatorHlsByte source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``quant``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``HlsDouble Quantile(ViewLocatorHlsUInt16 source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``quant``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``HlsDouble Quantile(ViewLocatorHlsDouble source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``quant``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``HsiDouble Quantile(ViewLocatorHsiByte source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``quant``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``HsiDouble Quantile(ViewLocatorHsiUInt16 source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``quant``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``HsiDouble Quantile(ViewLocatorHsiDouble source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``quant``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``LabDouble Quantile(ViewLocatorLabByte source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``quant``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``LabDouble Quantile(ViewLocatorLabUInt16 source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``quant``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``LabDouble Quantile(ViewLocatorLabDouble source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``quant``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``XyzDouble Quantile(ViewLocatorXyzByte source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``quant``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``XyzDouble Quantile(ViewLocatorXyzUInt16 source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``quant``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``XyzDouble Quantile(ViewLocatorXyzDouble source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``quant``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``System.Object Quantile(View source, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+---------------------+---------------+
| Parameter    | Type                | Description   |
+==============+=====================+===============+
| ``source``   | ``View``            |               |
+--------------+---------------------+---------------+
| ``quant``    | ``System.Double``   |               |
+--------------+---------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.

The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``System.Double Quantile(ViewLocatorByte source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``region``   | ``Region``            |               |
+--------------+-----------------------+---------------+
| ``quant``    | ``System.Double``     |               |
+--------------+-----------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``System.Double Quantile(ViewLocatorUInt16 source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``quant``    | ``System.Double``       |               |
+--------------+-------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``System.Double Quantile(ViewLocatorUInt32 source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``quant``    | ``System.Double``       |               |
+--------------+-------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``System.Double Quantile(ViewLocatorDouble source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``quant``    | ``System.Double``       |               |
+--------------+-------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``RgbDouble Quantile(ViewLocatorRgbByte source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``quant``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``RgbDouble Quantile(ViewLocatorRgbUInt16 source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``quant``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``RgbDouble Quantile(ViewLocatorRgbUInt32 source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``quant``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``RgbDouble Quantile(ViewLocatorRgbDouble source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``quant``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``RgbaDouble Quantile(ViewLocatorRgbaByte source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``region``   | ``Region``                |               |
+--------------+---------------------------+---------------+
| ``quant``    | ``System.Double``         |               |
+--------------+---------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``RgbaDouble Quantile(ViewLocatorRgbaUInt16 source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``quant``    | ``System.Double``           |               |
+--------------+-----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``RgbaDouble Quantile(ViewLocatorRgbaUInt32 source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``quant``    | ``System.Double``           |               |
+--------------+-----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``RgbaDouble Quantile(ViewLocatorRgbaDouble source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``quant``    | ``System.Double``           |               |
+--------------+-----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``HlsDouble Quantile(ViewLocatorHlsByte source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``quant``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``HlsDouble Quantile(ViewLocatorHlsUInt16 source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``quant``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``HlsDouble Quantile(ViewLocatorHlsDouble source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``quant``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``HsiDouble Quantile(ViewLocatorHsiByte source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``quant``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``HsiDouble Quantile(ViewLocatorHsiUInt16 source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``quant``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``HsiDouble Quantile(ViewLocatorHsiDouble source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``quant``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``LabDouble Quantile(ViewLocatorLabByte source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``quant``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``LabDouble Quantile(ViewLocatorLabUInt16 source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``quant``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``LabDouble Quantile(ViewLocatorLabDouble source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``quant``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``XyzDouble Quantile(ViewLocatorXyzByte source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``quant``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``XyzDouble Quantile(ViewLocatorXyzUInt16 source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``quant``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``XyzDouble Quantile(ViewLocatorXyzDouble source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``quant``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *Quantile*
^^^^^^^^^^^^^^^^^

``System.Object Quantile(View source, Region region, System.Double quant)``

Computes the specified quantile.

The method **Quantile** has the following parameters:

+--------------+---------------------+---------------+
| Parameter    | Type                | Description   |
+==============+=====================+===============+
| ``source``   | ``View``            |               |
+--------------+---------------------+---------------+
| ``region``   | ``Region``          |               |
+--------------+---------------------+---------------+
| ``quant``    | ``System.Double``   |               |
+--------------+---------------------+---------------+

The quantile is the quantile value in a view. The value\_type of the view must implement operator <, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The specified quantile.

Method *QuantileFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double QuantileFromHistogram(ViewLocatorUInt32 histogram, System.Double quant)``

Computes the specified quantile given a histogram.

The method **QuantileFromHistogram** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``histogram``   | ``ViewLocatorUInt32``   |               |
+-----------------+-------------------------+---------------+
| ``quant``       | ``System.Double``       |               |
+-----------------+-------------------------+---------------+

The given data is interpreted as being a histogram, and the quantile is computed with respect to the buffer\_base that was used to compute the histogram.

If the view contains valid histogram data, suitable results can always be found and returned. However, if the view contains entries that are all zero, correct results cannot be determined. In this case the returned value will be -1 cast to type T.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The specified quantile, or -1 cast to T if the passed histogram was not valid.

Method *QuantileFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double QuantileFromHistogram(ViewLocatorDouble histogram, System.Double quant)``

Computes the specified quantile given a histogram.

The method **QuantileFromHistogram** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``histogram``   | ``ViewLocatorDouble``   |               |
+-----------------+-------------------------+---------------+
| ``quant``       | ``System.Double``       |               |
+-----------------+-------------------------+---------------+

The given data is interpreted as being a histogram, and the quantile is computed with respect to the buffer\_base that was used to compute the histogram.

If the view contains valid histogram data, suitable results can always be found and returned. However, if the view contains entries that are all zero, correct results cannot be determined. In this case the returned value will be -1 cast to type T.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The specified quantile, or -1 cast to T if the passed histogram was not valid.

Method *QuantileFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble QuantileFromHistogram(ViewLocatorRgbDouble histogram, System.Double quant)``

Computes the specified quantile given a histogram.

The method **QuantileFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorRgbDouble``   |               |
+-----------------+----------------------------+---------------+
| ``quant``       | ``System.Double``          |               |
+-----------------+----------------------------+---------------+

The given data is interpreted as being a histogram, and the quantile is computed with respect to the buffer\_base that was used to compute the histogram.

If the view contains valid histogram data, suitable results can always be found and returned. However, if the view contains entries that are all zero, correct results cannot be determined. In this case the returned value will be -1 cast to type T.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The specified quantile, or -1 cast to T if the passed histogram was not valid.

Method *QuantileFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble QuantileFromHistogram(ViewLocatorRgbaDouble histogram, System.Double quant)``

Computes the specified quantile given a histogram.

The method **QuantileFromHistogram** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``histogram``   | ``ViewLocatorRgbaDouble``   |               |
+-----------------+-----------------------------+---------------+
| ``quant``       | ``System.Double``           |               |
+-----------------+-----------------------------+---------------+

The given data is interpreted as being a histogram, and the quantile is computed with respect to the buffer\_base that was used to compute the histogram.

If the view contains valid histogram data, suitable results can always be found and returned. However, if the view contains entries that are all zero, correct results cannot be determined. In this case the returned value will be -1 cast to type T.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The specified quantile, or -1 cast to T if the passed histogram was not valid.

Method *QuantileFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble QuantileFromHistogram(ViewLocatorHlsDouble histogram, System.Double quant)``

Computes the specified quantile given a histogram.

The method **QuantileFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorHlsDouble``   |               |
+-----------------+----------------------------+---------------+
| ``quant``       | ``System.Double``          |               |
+-----------------+----------------------------+---------------+

The given data is interpreted as being a histogram, and the quantile is computed with respect to the buffer\_base that was used to compute the histogram.

If the view contains valid histogram data, suitable results can always be found and returned. However, if the view contains entries that are all zero, correct results cannot be determined. In this case the returned value will be -1 cast to type T.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The specified quantile, or -1 cast to T if the passed histogram was not valid.

Method *QuantileFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble QuantileFromHistogram(ViewLocatorHsiDouble histogram, System.Double quant)``

Computes the specified quantile given a histogram.

The method **QuantileFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorHsiDouble``   |               |
+-----------------+----------------------------+---------------+
| ``quant``       | ``System.Double``          |               |
+-----------------+----------------------------+---------------+

The given data is interpreted as being a histogram, and the quantile is computed with respect to the buffer\_base that was used to compute the histogram.

If the view contains valid histogram data, suitable results can always be found and returned. However, if the view contains entries that are all zero, correct results cannot be determined. In this case the returned value will be -1 cast to type T.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The specified quantile, or -1 cast to T if the passed histogram was not valid.

Method *QuantileFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble QuantileFromHistogram(ViewLocatorLabDouble histogram, System.Double quant)``

Computes the specified quantile given a histogram.

The method **QuantileFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorLabDouble``   |               |
+-----------------+----------------------------+---------------+
| ``quant``       | ``System.Double``          |               |
+-----------------+----------------------------+---------------+

The given data is interpreted as being a histogram, and the quantile is computed with respect to the buffer\_base that was used to compute the histogram.

If the view contains valid histogram data, suitable results can always be found and returned. However, if the view contains entries that are all zero, correct results cannot be determined. In this case the returned value will be -1 cast to type T.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The specified quantile, or -1 cast to T if the passed histogram was not valid.

Method *QuantileFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble QuantileFromHistogram(ViewLocatorXyzDouble histogram, System.Double quant)``

Computes the specified quantile given a histogram.

The method **QuantileFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorXyzDouble``   |               |
+-----------------+----------------------------+---------------+
| ``quant``       | ``System.Double``          |               |
+-----------------+----------------------------+---------------+

The given data is interpreted as being a histogram, and the quantile is computed with respect to the buffer\_base that was used to compute the histogram.

If the view contains valid histogram data, suitable results can always be found and returned. However, if the view contains entries that are all zero, correct results cannot be determined. In this case the returned value will be -1 cast to type T.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The specified quantile, or -1 cast to T if the passed histogram was not valid.

Method *QuantileFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Object QuantileFromHistogram(View histogram, System.Double quant)``

Computes the specified quantile given a histogram.

The method **QuantileFromHistogram** has the following parameters:

+-----------------+---------------------+---------------+
| Parameter       | Type                | Description   |
+=================+=====================+===============+
| ``histogram``   | ``View``            |               |
+-----------------+---------------------+---------------+
| ``quant``       | ``System.Double``   |               |
+-----------------+---------------------+---------------+

The given data is interpreted as being a histogram, and the quantile is computed with respect to the buffer\_base that was used to compute the histogram.

If the view contains valid histogram data, suitable results can always be found and returned. However, if the view contains entries that are all zero, correct results cannot be determined. In this case the returned value will be -1 cast to type T.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The specified quantile, or -1 cast to T if the passed histogram was not valid.

Method *Sum*
^^^^^^^^^^^^

``System.Double Sum(ViewLocatorByte source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``System.Double Sum(ViewLocatorUInt16 source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``System.Double Sum(ViewLocatorUInt32 source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``System.Double Sum(ViewLocatorDouble source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``RgbDouble Sum(ViewLocatorRgbByte source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``RgbDouble Sum(ViewLocatorRgbUInt16 source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``RgbDouble Sum(ViewLocatorRgbUInt32 source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``RgbDouble Sum(ViewLocatorRgbDouble source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``RgbaDouble Sum(ViewLocatorRgbaByte source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``RgbaDouble Sum(ViewLocatorRgbaUInt16 source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``RgbaDouble Sum(ViewLocatorRgbaUInt32 source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``RgbaDouble Sum(ViewLocatorRgbaDouble source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``HlsDouble Sum(ViewLocatorHlsByte source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``HlsDouble Sum(ViewLocatorHlsUInt16 source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``HlsDouble Sum(ViewLocatorHlsDouble source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``HsiDouble Sum(ViewLocatorHsiByte source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``HsiDouble Sum(ViewLocatorHsiUInt16 source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``HsiDouble Sum(ViewLocatorHsiDouble source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``LabDouble Sum(ViewLocatorLabByte source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``LabDouble Sum(ViewLocatorLabUInt16 source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``LabDouble Sum(ViewLocatorLabDouble source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``XyzDouble Sum(ViewLocatorXyzByte source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``XyzDouble Sum(ViewLocatorXyzUInt16 source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``XyzDouble Sum(ViewLocatorXyzDouble source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``System.Object Sum(View source)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.

The sum.

Method *Sum*
^^^^^^^^^^^^

``System.Double Sum(ViewLocatorByte source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``region``   | ``Region``            |               |
+--------------+-----------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *Sum*
^^^^^^^^^^^^

``System.Double Sum(ViewLocatorUInt16 source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *Sum*
^^^^^^^^^^^^

``System.Double Sum(ViewLocatorUInt32 source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *Sum*
^^^^^^^^^^^^

``System.Double Sum(ViewLocatorDouble source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *Sum*
^^^^^^^^^^^^

``RgbDouble Sum(ViewLocatorRgbByte source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *Sum*
^^^^^^^^^^^^

``RgbDouble Sum(ViewLocatorRgbUInt16 source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *Sum*
^^^^^^^^^^^^

``RgbDouble Sum(ViewLocatorRgbUInt32 source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *Sum*
^^^^^^^^^^^^

``RgbDouble Sum(ViewLocatorRgbDouble source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *Sum*
^^^^^^^^^^^^

``RgbaDouble Sum(ViewLocatorRgbaByte source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``region``   | ``Region``                |               |
+--------------+---------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *Sum*
^^^^^^^^^^^^

``RgbaDouble Sum(ViewLocatorRgbaUInt16 source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *Sum*
^^^^^^^^^^^^

``RgbaDouble Sum(ViewLocatorRgbaUInt32 source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *Sum*
^^^^^^^^^^^^

``RgbaDouble Sum(ViewLocatorRgbaDouble source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *Sum*
^^^^^^^^^^^^

``HlsDouble Sum(ViewLocatorHlsByte source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *Sum*
^^^^^^^^^^^^

``HlsDouble Sum(ViewLocatorHlsUInt16 source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *Sum*
^^^^^^^^^^^^

``HlsDouble Sum(ViewLocatorHlsDouble source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *Sum*
^^^^^^^^^^^^

``HsiDouble Sum(ViewLocatorHsiByte source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *Sum*
^^^^^^^^^^^^

``HsiDouble Sum(ViewLocatorHsiUInt16 source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *Sum*
^^^^^^^^^^^^

``HsiDouble Sum(ViewLocatorHsiDouble source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *Sum*
^^^^^^^^^^^^

``LabDouble Sum(ViewLocatorLabByte source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *Sum*
^^^^^^^^^^^^

``LabDouble Sum(ViewLocatorLabUInt16 source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *Sum*
^^^^^^^^^^^^

``LabDouble Sum(ViewLocatorLabDouble source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *Sum*
^^^^^^^^^^^^

``XyzDouble Sum(ViewLocatorXyzByte source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *Sum*
^^^^^^^^^^^^

``XyzDouble Sum(ViewLocatorXyzUInt16 source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *Sum*
^^^^^^^^^^^^

``XyzDouble Sum(ViewLocatorXyzDouble source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *Sum*
^^^^^^^^^^^^

``System.Object Sum(View source, Region region)``

Computes the sum.

The method **Sum** has the following parameters:

+--------------+--------------+---------------+
| Parameter    | Type         | Description   |
+==============+==============+===============+
| ``source``   | ``View``     |               |
+--------------+--------------+---------------+
| ``region``   | ``Region``   |               |
+--------------+--------------+---------------+

Computes the sum of all values in the view. The value\_type of the view must implement operator+, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum.

Method *SumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double SumFromHistogram(ViewLocatorUInt32 histogram)``

Computes the sum given a histogram.

The method **SumFromHistogram** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``histogram``   | ``ViewLocatorUInt32``   |               |
+-----------------+-------------------------+---------------+

The given data view is interpreted as being a histogram, and the sum returned is the sum of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The sum.

Method *SumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double SumFromHistogram(ViewLocatorDouble histogram)``

Computes the sum given a histogram.

The method **SumFromHistogram** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``histogram``   | ``ViewLocatorDouble``   |               |
+-----------------+-------------------------+---------------+

The given data view is interpreted as being a histogram, and the sum returned is the sum of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The sum.

Method *SumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble SumFromHistogram(ViewLocatorRgbDouble histogram)``

Computes the sum given a histogram.

The method **SumFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorRgbDouble``   |               |
+-----------------+----------------------------+---------------+

The given data view is interpreted as being a histogram, and the sum returned is the sum of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The sum.

Method *SumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble SumFromHistogram(ViewLocatorRgbaDouble histogram)``

Computes the sum given a histogram.

The method **SumFromHistogram** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``histogram``   | ``ViewLocatorRgbaDouble``   |               |
+-----------------+-----------------------------+---------------+

The given data view is interpreted as being a histogram, and the sum returned is the sum of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The sum.

Method *SumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble SumFromHistogram(ViewLocatorHlsDouble histogram)``

Computes the sum given a histogram.

The method **SumFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorHlsDouble``   |               |
+-----------------+----------------------------+---------------+

The given data view is interpreted as being a histogram, and the sum returned is the sum of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The sum.

Method *SumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble SumFromHistogram(ViewLocatorHsiDouble histogram)``

Computes the sum given a histogram.

The method **SumFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorHsiDouble``   |               |
+-----------------+----------------------------+---------------+

The given data view is interpreted as being a histogram, and the sum returned is the sum of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The sum.

Method *SumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble SumFromHistogram(ViewLocatorLabDouble histogram)``

Computes the sum given a histogram.

The method **SumFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorLabDouble``   |               |
+-----------------+----------------------------+---------------+

The given data view is interpreted as being a histogram, and the sum returned is the sum of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The sum.

Method *SumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble SumFromHistogram(ViewLocatorXyzDouble histogram)``

Computes the sum given a histogram.

The method **SumFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorXyzDouble``   |               |
+-----------------+----------------------------+---------------+

The given data view is interpreted as being a histogram, and the sum returned is the sum of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The sum.

Method *SumFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Object SumFromHistogram(View histogram)``

Computes the sum given a histogram.

The method **SumFromHistogram** has the following parameters:

+-----------------+------------+---------------+
| Parameter       | Type       | Description   |
+=================+============+===============+
| ``histogram``   | ``View``   |               |
+-----------------+------------+---------------+

The given data view is interpreted as being a histogram, and the sum returned is the sum of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The sum.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``System.Double SumOfSquares(ViewLocatorByte source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``System.Double SumOfSquares(ViewLocatorUInt16 source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``System.Double SumOfSquares(ViewLocatorUInt32 source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``System.Double SumOfSquares(ViewLocatorDouble source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``RgbDouble SumOfSquares(ViewLocatorRgbByte source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``RgbDouble SumOfSquares(ViewLocatorRgbUInt16 source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``RgbDouble SumOfSquares(ViewLocatorRgbUInt32 source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``RgbDouble SumOfSquares(ViewLocatorRgbDouble source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble SumOfSquares(ViewLocatorRgbaByte source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble SumOfSquares(ViewLocatorRgbaUInt16 source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble SumOfSquares(ViewLocatorRgbaUInt32 source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble SumOfSquares(ViewLocatorRgbaDouble source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``HlsDouble SumOfSquares(ViewLocatorHlsByte source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``HlsDouble SumOfSquares(ViewLocatorHlsUInt16 source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``HlsDouble SumOfSquares(ViewLocatorHlsDouble source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``HsiDouble SumOfSquares(ViewLocatorHsiByte source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``HsiDouble SumOfSquares(ViewLocatorHsiUInt16 source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``HsiDouble SumOfSquares(ViewLocatorHsiDouble source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``LabDouble SumOfSquares(ViewLocatorLabByte source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``LabDouble SumOfSquares(ViewLocatorLabUInt16 source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``LabDouble SumOfSquares(ViewLocatorLabDouble source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``XyzDouble SumOfSquares(ViewLocatorXyzByte source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``XyzDouble SumOfSquares(ViewLocatorXyzUInt16 source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``XyzDouble SumOfSquares(ViewLocatorXyzDouble source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``System.Object SumOfSquares(View source)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.

The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``System.Double SumOfSquares(ViewLocatorByte source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``region``   | ``Region``            |               |
+--------------+-----------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``System.Double SumOfSquares(ViewLocatorUInt16 source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``System.Double SumOfSquares(ViewLocatorUInt32 source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``System.Double SumOfSquares(ViewLocatorDouble source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``RgbDouble SumOfSquares(ViewLocatorRgbByte source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``RgbDouble SumOfSquares(ViewLocatorRgbUInt16 source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``RgbDouble SumOfSquares(ViewLocatorRgbUInt32 source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``RgbDouble SumOfSquares(ViewLocatorRgbDouble source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble SumOfSquares(ViewLocatorRgbaByte source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``region``   | ``Region``                |               |
+--------------+---------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble SumOfSquares(ViewLocatorRgbaUInt16 source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble SumOfSquares(ViewLocatorRgbaUInt32 source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble SumOfSquares(ViewLocatorRgbaDouble source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``HlsDouble SumOfSquares(ViewLocatorHlsByte source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``HlsDouble SumOfSquares(ViewLocatorHlsUInt16 source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``HlsDouble SumOfSquares(ViewLocatorHlsDouble source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``HsiDouble SumOfSquares(ViewLocatorHsiByte source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``HsiDouble SumOfSquares(ViewLocatorHsiUInt16 source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``HsiDouble SumOfSquares(ViewLocatorHsiDouble source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``LabDouble SumOfSquares(ViewLocatorLabByte source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``LabDouble SumOfSquares(ViewLocatorLabUInt16 source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``LabDouble SumOfSquares(ViewLocatorLabDouble source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``XyzDouble SumOfSquares(ViewLocatorXyzByte source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``XyzDouble SumOfSquares(ViewLocatorXyzUInt16 source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``XyzDouble SumOfSquares(ViewLocatorXyzDouble source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquares*
^^^^^^^^^^^^^^^^^^^^^

``System.Object SumOfSquares(View source, Region region)``

Computes the sum of squares.

The method **SumOfSquares** has the following parameters:

+--------------+--------------+---------------+
| Parameter    | Type         | Description   |
+==============+==============+===============+
| ``source``   | ``View``     |               |
+--------------+--------------+---------------+
| ``region``   | ``Region``   |               |
+--------------+--------------+---------------+

Computes the sum of all squared values in the view. The value\_type of the view must implement ngi::numeric\_traits<>::scalar\_product<>() as well as operator +(), otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The sum of the squares.

Method *SumOfSquaresFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double SumOfSquaresFromHistogram(ViewLocatorUInt32 histogram)``

Computes the sum of squares given a histogram.

The method **SumOfSquaresFromHistogram** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``histogram``   | ``ViewLocatorUInt32``   |               |
+-----------------+-------------------------+---------------+

The given data view is interpreted as being a histogram, and the sum of squares returned is the sum of the squares of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The sum of the squares.

Method *SumOfSquaresFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double SumOfSquaresFromHistogram(ViewLocatorDouble histogram)``

Computes the sum of squares given a histogram.

The method **SumOfSquaresFromHistogram** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``histogram``   | ``ViewLocatorDouble``   |               |
+-----------------+-------------------------+---------------+

The given data view is interpreted as being a histogram, and the sum of squares returned is the sum of the squares of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The sum of the squares.

Method *SumOfSquaresFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble SumOfSquaresFromHistogram(ViewLocatorRgbDouble histogram)``

Computes the sum of squares given a histogram.

The method **SumOfSquaresFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorRgbDouble``   |               |
+-----------------+----------------------------+---------------+

The given data view is interpreted as being a histogram, and the sum of squares returned is the sum of the squares of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The sum of the squares.

Method *SumOfSquaresFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble SumOfSquaresFromHistogram(ViewLocatorRgbaDouble histogram)``

Computes the sum of squares given a histogram.

The method **SumOfSquaresFromHistogram** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``histogram``   | ``ViewLocatorRgbaDouble``   |               |
+-----------------+-----------------------------+---------------+

The given data view is interpreted as being a histogram, and the sum of squares returned is the sum of the squares of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The sum of the squares.

Method *SumOfSquaresFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble SumOfSquaresFromHistogram(ViewLocatorHlsDouble histogram)``

Computes the sum of squares given a histogram.

The method **SumOfSquaresFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorHlsDouble``   |               |
+-----------------+----------------------------+---------------+

The given data view is interpreted as being a histogram, and the sum of squares returned is the sum of the squares of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The sum of the squares.

Method *SumOfSquaresFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble SumOfSquaresFromHistogram(ViewLocatorHsiDouble histogram)``

Computes the sum of squares given a histogram.

The method **SumOfSquaresFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorHsiDouble``   |               |
+-----------------+----------------------------+---------------+

The given data view is interpreted as being a histogram, and the sum of squares returned is the sum of the squares of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The sum of the squares.

Method *SumOfSquaresFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble SumOfSquaresFromHistogram(ViewLocatorLabDouble histogram)``

Computes the sum of squares given a histogram.

The method **SumOfSquaresFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorLabDouble``   |               |
+-----------------+----------------------------+---------------+

The given data view is interpreted as being a histogram, and the sum of squares returned is the sum of the squares of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The sum of the squares.

Method *SumOfSquaresFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble SumOfSquaresFromHistogram(ViewLocatorXyzDouble histogram)``

Computes the sum of squares given a histogram.

The method **SumOfSquaresFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorXyzDouble``   |               |
+-----------------+----------------------------+---------------+

The given data view is interpreted as being a histogram, and the sum of squares returned is the sum of the squares of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The sum of the squares.

Method *SumOfSquaresFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Object SumOfSquaresFromHistogram(View histogram)``

Computes the sum of squares given a histogram.

The method **SumOfSquaresFromHistogram** has the following parameters:

+-----------------+------------+---------------+
| Parameter       | Type       | Description   |
+=================+============+===============+
| ``histogram``   | ``View``   |               |
+-----------------+------------+---------------+

The given data view is interpreted as being a histogram, and the sum of squares returned is the sum of the squares of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The sum of the squares.

Method *Average*
^^^^^^^^^^^^^^^^

``System.Double Average(ViewLocatorByte source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

The average is calculated according to the following formula: |image0|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``System.Double Average(ViewLocatorUInt16 source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

The average is calculated according to the following formula: |image1|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``System.Double Average(ViewLocatorUInt32 source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

The average is calculated according to the following formula: |image2|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``System.Double Average(ViewLocatorDouble source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

The average is calculated according to the following formula: |image3|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``RgbDouble Average(ViewLocatorRgbByte source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

The average is calculated according to the following formula: |image4|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``RgbDouble Average(ViewLocatorRgbUInt16 source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

The average is calculated according to the following formula: |image5|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``RgbDouble Average(ViewLocatorRgbUInt32 source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

The average is calculated according to the following formula: |image6|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``RgbDouble Average(ViewLocatorRgbDouble source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

The average is calculated according to the following formula: |image7|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``RgbaDouble Average(ViewLocatorRgbaByte source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+

The average is calculated according to the following formula: |image8|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``RgbaDouble Average(ViewLocatorRgbaUInt16 source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+

The average is calculated according to the following formula: |image9|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``RgbaDouble Average(ViewLocatorRgbaUInt32 source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+

The average is calculated according to the following formula: |image10|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``RgbaDouble Average(ViewLocatorRgbaDouble source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

The average is calculated according to the following formula: |image11|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``HlsDouble Average(ViewLocatorHlsByte source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

The average is calculated according to the following formula: |image12|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``HlsDouble Average(ViewLocatorHlsUInt16 source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

The average is calculated according to the following formula: |image13|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``HlsDouble Average(ViewLocatorHlsDouble source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

The average is calculated according to the following formula: |image14|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``HsiDouble Average(ViewLocatorHsiByte source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

The average is calculated according to the following formula: |image15|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``HsiDouble Average(ViewLocatorHsiUInt16 source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

The average is calculated according to the following formula: |image16|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``HsiDouble Average(ViewLocatorHsiDouble source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

The average is calculated according to the following formula: |image17|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``LabDouble Average(ViewLocatorLabByte source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

The average is calculated according to the following formula: |image18|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``LabDouble Average(ViewLocatorLabUInt16 source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

The average is calculated according to the following formula: |image19|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``LabDouble Average(ViewLocatorLabDouble source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

The average is calculated according to the following formula: |image20|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``XyzDouble Average(ViewLocatorXyzByte source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

The average is calculated according to the following formula: |image21|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``XyzDouble Average(ViewLocatorXyzUInt16 source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

The average is calculated according to the following formula: |image22|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``XyzDouble Average(ViewLocatorXyzDouble source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

The average is calculated according to the following formula: |image23|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``System.Object Average(View source)``

Computes the average.

The method **Average** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

The average is calculated according to the following formula: |image24|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``System.Double Average(ViewLocatorByte source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``region``   | ``Region``            |               |
+--------------+-----------------------+---------------+

The average is calculated according to the following formula: |image25|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``System.Double Average(ViewLocatorUInt16 source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The average is calculated according to the following formula: |image26|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``System.Double Average(ViewLocatorUInt32 source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The average is calculated according to the following formula: |image27|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``System.Double Average(ViewLocatorDouble source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The average is calculated according to the following formula: |image28|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``RgbDouble Average(ViewLocatorRgbByte source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The average is calculated according to the following formula: |image29|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``RgbDouble Average(ViewLocatorRgbUInt16 source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The average is calculated according to the following formula: |image30|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``RgbDouble Average(ViewLocatorRgbUInt32 source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The average is calculated according to the following formula: |image31|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``RgbDouble Average(ViewLocatorRgbDouble source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The average is calculated according to the following formula: |image32|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``RgbaDouble Average(ViewLocatorRgbaByte source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``region``   | ``Region``                |               |
+--------------+---------------------------+---------------+

The average is calculated according to the following formula: |image33|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``RgbaDouble Average(ViewLocatorRgbaUInt16 source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The average is calculated according to the following formula: |image34|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``RgbaDouble Average(ViewLocatorRgbaUInt32 source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The average is calculated according to the following formula: |image35|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``RgbaDouble Average(ViewLocatorRgbaDouble source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The average is calculated according to the following formula: |image36|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``HlsDouble Average(ViewLocatorHlsByte source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The average is calculated according to the following formula: |image37|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``HlsDouble Average(ViewLocatorHlsUInt16 source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The average is calculated according to the following formula: |image38|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``HlsDouble Average(ViewLocatorHlsDouble source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The average is calculated according to the following formula: |image39|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``HsiDouble Average(ViewLocatorHsiByte source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The average is calculated according to the following formula: |image40|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``HsiDouble Average(ViewLocatorHsiUInt16 source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The average is calculated according to the following formula: |image41|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``HsiDouble Average(ViewLocatorHsiDouble source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The average is calculated according to the following formula: |image42|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``LabDouble Average(ViewLocatorLabByte source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The average is calculated according to the following formula: |image43|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``LabDouble Average(ViewLocatorLabUInt16 source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The average is calculated according to the following formula: |image44|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``LabDouble Average(ViewLocatorLabDouble source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The average is calculated according to the following formula: |image45|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``XyzDouble Average(ViewLocatorXyzByte source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The average is calculated according to the following formula: |image46|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``XyzDouble Average(ViewLocatorXyzUInt16 source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The average is calculated according to the following formula: |image47|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``XyzDouble Average(ViewLocatorXyzDouble source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The average is calculated according to the following formula: |image48|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *Average*
^^^^^^^^^^^^^^^^

``System.Object Average(View source, Region region)``

Computes the average.

The method **Average** has the following parameters:

+--------------+--------------+---------------+
| Parameter    | Type         | Description   |
+==============+==============+===============+
| ``source``   | ``View``     |               |
+--------------+--------------+---------------+
| ``region``   | ``Region``   |               |
+--------------+--------------+---------------+

The average is calculated according to the following formula: |image49|

The value\_type of the view must implement operator+ and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.More information about the average can be found at http://en.wikipedia.org/wiki/Average and at http://mathworld.wolfram.com/ArithmeticMean.html. For multi-channel types (such as color), the average is calculated channel-wise.

The average.

Method *AverageFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double AverageFromHistogram(ViewLocatorUInt32 histogram)``

Computes the average given a histogram.

The method **AverageFromHistogram** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``histogram``   | ``ViewLocatorUInt32``   |               |
+-----------------+-------------------------+---------------+

The given data range is interpreted as being a histogram, and the average returned is the average of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The average.

Method *AverageFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double AverageFromHistogram(ViewLocatorDouble histogram)``

Computes the average given a histogram.

The method **AverageFromHistogram** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``histogram``   | ``ViewLocatorDouble``   |               |
+-----------------+-------------------------+---------------+

The given data range is interpreted as being a histogram, and the average returned is the average of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The average.

Method *AverageFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble AverageFromHistogram(ViewLocatorRgbDouble histogram)``

Computes the average given a histogram.

The method **AverageFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorRgbDouble``   |               |
+-----------------+----------------------------+---------------+

The given data range is interpreted as being a histogram, and the average returned is the average of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The average.

Method *AverageFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble AverageFromHistogram(ViewLocatorRgbaDouble histogram)``

Computes the average given a histogram.

The method **AverageFromHistogram** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``histogram``   | ``ViewLocatorRgbaDouble``   |               |
+-----------------+-----------------------------+---------------+

The given data range is interpreted as being a histogram, and the average returned is the average of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The average.

Method *AverageFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble AverageFromHistogram(ViewLocatorHlsDouble histogram)``

Computes the average given a histogram.

The method **AverageFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorHlsDouble``   |               |
+-----------------+----------------------------+---------------+

The given data range is interpreted as being a histogram, and the average returned is the average of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The average.

Method *AverageFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble AverageFromHistogram(ViewLocatorHsiDouble histogram)``

Computes the average given a histogram.

The method **AverageFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorHsiDouble``   |               |
+-----------------+----------------------------+---------------+

The given data range is interpreted as being a histogram, and the average returned is the average of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The average.

Method *AverageFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble AverageFromHistogram(ViewLocatorLabDouble histogram)``

Computes the average given a histogram.

The method **AverageFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorLabDouble``   |               |
+-----------------+----------------------------+---------------+

The given data range is interpreted as being a histogram, and the average returned is the average of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The average.

Method *AverageFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble AverageFromHistogram(ViewLocatorXyzDouble histogram)``

Computes the average given a histogram.

The method **AverageFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorXyzDouble``   |               |
+-----------------+----------------------------+---------------+

The given data range is interpreted as being a histogram, and the average returned is the average of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The average.

Method *AverageFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Object AverageFromHistogram(View histogram)``

Computes the average given a histogram.

The method **AverageFromHistogram** has the following parameters:

+-----------------+------------+---------------+
| Parameter       | Type       | Description   |
+=================+============+===============+
| ``histogram``   | ``View``   |               |
+-----------------+------------+---------------+

The given data range is interpreted as being a histogram, and the average returned is the average of the original data, not the histogram data.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The average.

Method *Variance*
^^^^^^^^^^^^^^^^^

``System.Double Variance(ViewLocatorByte source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``System.Double Variance(ViewLocatorUInt16 source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``System.Double Variance(ViewLocatorUInt32 source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``System.Double Variance(ViewLocatorDouble source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``RgbDouble Variance(ViewLocatorRgbByte source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``RgbDouble Variance(ViewLocatorRgbUInt16 source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``RgbDouble Variance(ViewLocatorRgbUInt32 source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``RgbDouble Variance(ViewLocatorRgbDouble source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``RgbaDouble Variance(ViewLocatorRgbaByte source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``RgbaDouble Variance(ViewLocatorRgbaUInt16 source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``RgbaDouble Variance(ViewLocatorRgbaUInt32 source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``RgbaDouble Variance(ViewLocatorRgbaDouble source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``HlsDouble Variance(ViewLocatorHlsByte source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``HlsDouble Variance(ViewLocatorHlsUInt16 source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``HlsDouble Variance(ViewLocatorHlsDouble source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``HsiDouble Variance(ViewLocatorHsiByte source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``HsiDouble Variance(ViewLocatorHsiUInt16 source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``HsiDouble Variance(ViewLocatorHsiDouble source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``LabDouble Variance(ViewLocatorLabByte source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``LabDouble Variance(ViewLocatorLabUInt16 source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``LabDouble Variance(ViewLocatorLabDouble source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``XyzDouble Variance(ViewLocatorXyzByte source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``XyzDouble Variance(ViewLocatorXyzUInt16 source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``XyzDouble Variance(ViewLocatorXyzDouble source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``System.Object Variance(View source)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``System.Double Variance(ViewLocatorByte source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``region``   | ``Region``            |               |
+--------------+-----------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``System.Double Variance(ViewLocatorUInt16 source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``System.Double Variance(ViewLocatorUInt32 source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``System.Double Variance(ViewLocatorDouble source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``RgbDouble Variance(ViewLocatorRgbByte source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``RgbDouble Variance(ViewLocatorRgbUInt16 source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``RgbDouble Variance(ViewLocatorRgbUInt32 source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``RgbDouble Variance(ViewLocatorRgbDouble source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``RgbaDouble Variance(ViewLocatorRgbaByte source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``region``   | ``Region``                |               |
+--------------+---------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``RgbaDouble Variance(ViewLocatorRgbaUInt16 source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``RgbaDouble Variance(ViewLocatorRgbaUInt32 source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``RgbaDouble Variance(ViewLocatorRgbaDouble source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``HlsDouble Variance(ViewLocatorHlsByte source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``HlsDouble Variance(ViewLocatorHlsUInt16 source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``HlsDouble Variance(ViewLocatorHlsDouble source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``HsiDouble Variance(ViewLocatorHsiByte source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``HsiDouble Variance(ViewLocatorHsiUInt16 source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``HsiDouble Variance(ViewLocatorHsiDouble source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``LabDouble Variance(ViewLocatorLabByte source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``LabDouble Variance(ViewLocatorLabUInt16 source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``LabDouble Variance(ViewLocatorLabDouble source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``XyzDouble Variance(ViewLocatorXyzByte source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``XyzDouble Variance(ViewLocatorXyzUInt16 source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``XyzDouble Variance(ViewLocatorXyzDouble source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *Variance*
^^^^^^^^^^^^^^^^^

``System.Object Variance(View source, Region region)``

Computes the variance.

The method **Variance** has the following parameters:

+--------------+--------------+---------------+
| Parameter    | Type         | Description   |
+==============+==============+===============+
| ``source``   | ``View``     |               |
+--------------+--------------+---------------+
| ``region``   | ``Region``   |               |
+--------------+--------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The variance.

Method *VarianceFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double VarianceFromHistogram(ViewLocatorUInt32 histogram)``

Computes the variance from a histogram.

The method **VarianceFromHistogram** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``histogram``   | ``ViewLocatorUInt32``   |               |
+-----------------+-------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The variance.

Method *VarianceFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double VarianceFromHistogram(ViewLocatorDouble histogram)``

Computes the variance from a histogram.

The method **VarianceFromHistogram** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``histogram``   | ``ViewLocatorDouble``   |               |
+-----------------+-------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The variance.

Method *VarianceFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble VarianceFromHistogram(ViewLocatorRgbDouble histogram)``

Computes the variance from a histogram.

The method **VarianceFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorRgbDouble``   |               |
+-----------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The variance.

Method *VarianceFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble VarianceFromHistogram(ViewLocatorRgbaDouble histogram)``

Computes the variance from a histogram.

The method **VarianceFromHistogram** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``histogram``   | ``ViewLocatorRgbaDouble``   |               |
+-----------------+-----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The variance.

Method *VarianceFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble VarianceFromHistogram(ViewLocatorHlsDouble histogram)``

Computes the variance from a histogram.

The method **VarianceFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorHlsDouble``   |               |
+-----------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The variance.

Method *VarianceFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble VarianceFromHistogram(ViewLocatorHsiDouble histogram)``

Computes the variance from a histogram.

The method **VarianceFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorHsiDouble``   |               |
+-----------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The variance.

Method *VarianceFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble VarianceFromHistogram(ViewLocatorLabDouble histogram)``

Computes the variance from a histogram.

The method **VarianceFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorLabDouble``   |               |
+-----------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The variance.

Method *VarianceFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble VarianceFromHistogram(ViewLocatorXyzDouble histogram)``

Computes the variance from a histogram.

The method **VarianceFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorXyzDouble``   |               |
+-----------------+----------------------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The variance.

Method *VarianceFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Object VarianceFromHistogram(View histogram)``

Computes the variance from a histogram.

The method **VarianceFromHistogram** has the following parameters:

+-----------------+------------+---------------+
| Parameter       | Type       | Description   |
+=================+============+===============+
| ``histogram``   | ``View``   |               |
+-----------------+------------+---------------+

The variance is a measure of statistical dispersion, averaging the squared distance of values from the mean value. The variance is a measure of how much a statistical distribution is spread out.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The variance.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double StandardDeviation(ViewLocatorByte source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double StandardDeviation(ViewLocatorUInt16 source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double StandardDeviation(ViewLocatorUInt32 source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double StandardDeviation(ViewLocatorDouble source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble StandardDeviation(ViewLocatorRgbByte source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble StandardDeviation(ViewLocatorRgbUInt16 source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble StandardDeviation(ViewLocatorRgbUInt32 source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble StandardDeviation(ViewLocatorRgbDouble source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble StandardDeviation(ViewLocatorRgbaByte source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble StandardDeviation(ViewLocatorRgbaUInt16 source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble StandardDeviation(ViewLocatorRgbaUInt32 source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble StandardDeviation(ViewLocatorRgbaDouble source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble StandardDeviation(ViewLocatorHlsByte source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble StandardDeviation(ViewLocatorHlsUInt16 source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble StandardDeviation(ViewLocatorHlsDouble source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble StandardDeviation(ViewLocatorHsiByte source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble StandardDeviation(ViewLocatorHsiUInt16 source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble StandardDeviation(ViewLocatorHsiDouble source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble StandardDeviation(ViewLocatorLabByte source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble StandardDeviation(ViewLocatorLabUInt16 source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble StandardDeviation(ViewLocatorLabDouble source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble StandardDeviation(ViewLocatorXyzByte source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble StandardDeviation(ViewLocatorXyzUInt16 source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble StandardDeviation(ViewLocatorXyzDouble source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Object StandardDeviation(View source)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double StandardDeviation(ViewLocatorByte source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``region``   | ``Region``            |               |
+--------------+-----------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double StandardDeviation(ViewLocatorUInt16 source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double StandardDeviation(ViewLocatorUInt32 source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double StandardDeviation(ViewLocatorDouble source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble StandardDeviation(ViewLocatorRgbByte source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble StandardDeviation(ViewLocatorRgbUInt16 source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble StandardDeviation(ViewLocatorRgbUInt32 source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble StandardDeviation(ViewLocatorRgbDouble source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble StandardDeviation(ViewLocatorRgbaByte source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``region``   | ``Region``                |               |
+--------------+---------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble StandardDeviation(ViewLocatorRgbaUInt16 source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble StandardDeviation(ViewLocatorRgbaUInt32 source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble StandardDeviation(ViewLocatorRgbaDouble source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble StandardDeviation(ViewLocatorHlsByte source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble StandardDeviation(ViewLocatorHlsUInt16 source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble StandardDeviation(ViewLocatorHlsDouble source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble StandardDeviation(ViewLocatorHsiByte source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble StandardDeviation(ViewLocatorHsiUInt16 source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble StandardDeviation(ViewLocatorHsiDouble source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble StandardDeviation(ViewLocatorLabByte source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble StandardDeviation(ViewLocatorLabUInt16 source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble StandardDeviation(ViewLocatorLabDouble source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble StandardDeviation(ViewLocatorXyzByte source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble StandardDeviation(ViewLocatorXyzUInt16 source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble StandardDeviation(ViewLocatorXyzDouble source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Object StandardDeviation(View source, Region region)``

Computes the standard deviation.

The method **StandardDeviation** has the following parameters:

+--------------+--------------+---------------+
| Parameter    | Type         | Description   |
+==============+==============+===============+
| ``source``   | ``View``     |               |
+--------------+--------------+---------------+
| ``region``   | ``Region``   |               |
+--------------+--------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The standard deviation.

Method *StandardDeviationFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double StandardDeviationFromHistogram(ViewLocatorUInt32 histogram)``

Computes the standard deviation from a histogram.

The method **StandardDeviationFromHistogram** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``histogram``   | ``ViewLocatorUInt32``   |               |
+-----------------+-------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The standard deviation.

Method *StandardDeviationFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double StandardDeviationFromHistogram(ViewLocatorDouble histogram)``

Computes the standard deviation from a histogram.

The method **StandardDeviationFromHistogram** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``histogram``   | ``ViewLocatorDouble``   |               |
+-----------------+-------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The standard deviation.

Method *StandardDeviationFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble StandardDeviationFromHistogram(ViewLocatorRgbDouble histogram)``

Computes the standard deviation from a histogram.

The method **StandardDeviationFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorRgbDouble``   |               |
+-----------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The standard deviation.

Method *StandardDeviationFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble StandardDeviationFromHistogram(ViewLocatorRgbaDouble histogram)``

Computes the standard deviation from a histogram.

The method **StandardDeviationFromHistogram** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``histogram``   | ``ViewLocatorRgbaDouble``   |               |
+-----------------+-----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The standard deviation.

Method *StandardDeviationFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble StandardDeviationFromHistogram(ViewLocatorHlsDouble histogram)``

Computes the standard deviation from a histogram.

The method **StandardDeviationFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorHlsDouble``   |               |
+-----------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The standard deviation.

Method *StandardDeviationFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble StandardDeviationFromHistogram(ViewLocatorHsiDouble histogram)``

Computes the standard deviation from a histogram.

The method **StandardDeviationFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorHsiDouble``   |               |
+-----------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The standard deviation.

Method *StandardDeviationFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble StandardDeviationFromHistogram(ViewLocatorLabDouble histogram)``

Computes the standard deviation from a histogram.

The method **StandardDeviationFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorLabDouble``   |               |
+-----------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The standard deviation.

Method *StandardDeviationFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble StandardDeviationFromHistogram(ViewLocatorXyzDouble histogram)``

Computes the standard deviation from a histogram.

The method **StandardDeviationFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorXyzDouble``   |               |
+-----------------+----------------------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The standard deviation.

Method *StandardDeviationFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Object StandardDeviationFromHistogram(View histogram)``

Computes the standard deviation from a histogram.

The method **StandardDeviationFromHistogram** has the following parameters:

+-----------------+------------+---------------+
| Parameter       | Type       | Description   |
+=================+============+===============+
| ``histogram``   | ``View``   |               |
+-----------------+------------+---------------+

The standard deviation is a measure of statistical dispersion. It is the square root of the variance.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The standard deviation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double CoefficientOfVariation(ViewLocatorByte source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double CoefficientOfVariation(ViewLocatorUInt16 source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double CoefficientOfVariation(ViewLocatorUInt32 source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double CoefficientOfVariation(ViewLocatorDouble source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble CoefficientOfVariation(ViewLocatorRgbByte source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble CoefficientOfVariation(ViewLocatorRgbUInt16 source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble CoefficientOfVariation(ViewLocatorRgbUInt32 source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble CoefficientOfVariation(ViewLocatorRgbDouble source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble CoefficientOfVariation(ViewLocatorRgbaByte source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble CoefficientOfVariation(ViewLocatorRgbaUInt16 source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble CoefficientOfVariation(ViewLocatorRgbaUInt32 source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble CoefficientOfVariation(ViewLocatorRgbaDouble source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble CoefficientOfVariation(ViewLocatorHlsByte source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble CoefficientOfVariation(ViewLocatorHlsUInt16 source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble CoefficientOfVariation(ViewLocatorHlsDouble source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble CoefficientOfVariation(ViewLocatorHsiByte source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble CoefficientOfVariation(ViewLocatorHsiUInt16 source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble CoefficientOfVariation(ViewLocatorHsiDouble source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble CoefficientOfVariation(ViewLocatorLabByte source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble CoefficientOfVariation(ViewLocatorLabUInt16 source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble CoefficientOfVariation(ViewLocatorLabDouble source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble CoefficientOfVariation(ViewLocatorXyzByte source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble CoefficientOfVariation(ViewLocatorXyzUInt16 source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble CoefficientOfVariation(ViewLocatorXyzDouble source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Object CoefficientOfVariation(View source)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double CoefficientOfVariation(ViewLocatorByte source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``region``   | ``Region``            |               |
+--------------+-----------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double CoefficientOfVariation(ViewLocatorUInt16 source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double CoefficientOfVariation(ViewLocatorUInt32 source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double CoefficientOfVariation(ViewLocatorDouble source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``region``   | ``Region``              |               |
+--------------+-------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble CoefficientOfVariation(ViewLocatorRgbByte source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble CoefficientOfVariation(ViewLocatorRgbUInt16 source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble CoefficientOfVariation(ViewLocatorRgbUInt32 source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble CoefficientOfVariation(ViewLocatorRgbDouble source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble CoefficientOfVariation(ViewLocatorRgbaByte source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``region``   | ``Region``                |               |
+--------------+---------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble CoefficientOfVariation(ViewLocatorRgbaUInt16 source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble CoefficientOfVariation(ViewLocatorRgbaUInt32 source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble CoefficientOfVariation(ViewLocatorRgbaDouble source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``region``   | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble CoefficientOfVariation(ViewLocatorHlsByte source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble CoefficientOfVariation(ViewLocatorHlsUInt16 source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble CoefficientOfVariation(ViewLocatorHlsDouble source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble CoefficientOfVariation(ViewLocatorHsiByte source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble CoefficientOfVariation(ViewLocatorHsiUInt16 source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble CoefficientOfVariation(ViewLocatorHsiDouble source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble CoefficientOfVariation(ViewLocatorLabByte source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble CoefficientOfVariation(ViewLocatorLabUInt16 source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble CoefficientOfVariation(ViewLocatorLabDouble source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble CoefficientOfVariation(ViewLocatorXyzByte source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``region``   | ``Region``               |               |
+--------------+--------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble CoefficientOfVariation(ViewLocatorXyzUInt16 source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble CoefficientOfVariation(ViewLocatorXyzDouble source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``region``   | ``Region``                 |               |
+--------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Object CoefficientOfVariation(View source, Region region)``

Computes the coefficient of variation.

The method **CoefficientOfVariation** has the following parameters:

+--------------+--------------+---------------+
| Parameter    | Type         | Description   |
+==============+==============+===============+
| ``source``   | ``View``     |               |
+--------------+--------------+---------------+
| ``region``   | ``Region``   |               |
+--------------+--------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.The region parameter constrains the function to the region inside of the view only.The coefficient of variation.

Method *CoefficientOfVariationFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double CoefficientOfVariationFromHistogram(ViewLocatorUInt32 histogram)``

Computes the coefficient of variation from a histogram.

The method **CoefficientOfVariationFromHistogram** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``histogram``   | ``ViewLocatorUInt32``   |               |
+-----------------+-------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The coefficient of variation.

Method *CoefficientOfVariationFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double CoefficientOfVariationFromHistogram(ViewLocatorDouble histogram)``

Computes the coefficient of variation from a histogram.

The method **CoefficientOfVariationFromHistogram** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``histogram``   | ``ViewLocatorDouble``   |               |
+-----------------+-------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The coefficient of variation.

Method *CoefficientOfVariationFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbDouble CoefficientOfVariationFromHistogram(ViewLocatorRgbDouble histogram)``

Computes the coefficient of variation from a histogram.

The method **CoefficientOfVariationFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorRgbDouble``   |               |
+-----------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The coefficient of variation.

Method *CoefficientOfVariationFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RgbaDouble CoefficientOfVariationFromHistogram(ViewLocatorRgbaDouble histogram)``

Computes the coefficient of variation from a histogram.

The method **CoefficientOfVariationFromHistogram** has the following parameters:

+-----------------+-----------------------------+---------------+
| Parameter       | Type                        | Description   |
+=================+=============================+===============+
| ``histogram``   | ``ViewLocatorRgbaDouble``   |               |
+-----------------+-----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The coefficient of variation.

Method *CoefficientOfVariationFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HlsDouble CoefficientOfVariationFromHistogram(ViewLocatorHlsDouble histogram)``

Computes the coefficient of variation from a histogram.

The method **CoefficientOfVariationFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorHlsDouble``   |               |
+-----------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The coefficient of variation.

Method *CoefficientOfVariationFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HsiDouble CoefficientOfVariationFromHistogram(ViewLocatorHsiDouble histogram)``

Computes the coefficient of variation from a histogram.

The method **CoefficientOfVariationFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorHsiDouble``   |               |
+-----------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The coefficient of variation.

Method *CoefficientOfVariationFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``LabDouble CoefficientOfVariationFromHistogram(ViewLocatorLabDouble histogram)``

Computes the coefficient of variation from a histogram.

The method **CoefficientOfVariationFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorLabDouble``   |               |
+-----------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The coefficient of variation.

Method *CoefficientOfVariationFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``XyzDouble CoefficientOfVariationFromHistogram(ViewLocatorXyzDouble histogram)``

Computes the coefficient of variation from a histogram.

The method **CoefficientOfVariationFromHistogram** has the following parameters:

+-----------------+----------------------------+---------------+
| Parameter       | Type                       | Description   |
+=================+============================+===============+
| ``histogram``   | ``ViewLocatorXyzDouble``   |               |
+-----------------+----------------------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The coefficient of variation.

Method *CoefficientOfVariationFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Object CoefficientOfVariationFromHistogram(View histogram)``

Computes the coefficient of variation from a histogram.

The method **CoefficientOfVariationFromHistogram** has the following parameters:

+-----------------+------------+---------------+
| Parameter       | Type       | Description   |
+=================+============+===============+
| ``histogram``   | ``View``   |               |
+-----------------+------------+---------------+

The coefficient of variation is also known as relative standard deviation. The variance of a dispersion with a high average is usually bigger than the variance of a dispersion with a low average. Since the variance and the standard deviation are not normalized, usually one cannot reason whether a variance is low or high. The coefficient of variation provides such a normalization. It is defined as the ratio of the standard deviation and the average.

The function internally calls average to calculate the average if the average parameter is set to zero (the default). If you have calculated the average already, you can pass it with the average parameter.

The coefficient of variation.

Method *Covariance*
^^^^^^^^^^^^^^^^^^^

``System.Double Covariance(ViewLocatorByte source1, ViewLocatorByte source2)``

Computes the covariance of the two ranges.

The method **Covariance** has the following parameters:

+---------------+-----------------------+---------------+
| Parameter     | Type                  | Description   |
+===============+=======================+===============+
| ``source1``   | ``ViewLocatorByte``   |               |
+---------------+-----------------------+---------------+
| ``source2``   | ``ViewLocatorByte``   |               |
+---------------+-----------------------+---------------+

The covariance is a measure of how much two different signals vary, or

.. raw:: html

   <itemizedlist>

in other words - the strength of the correlation between the two signals.

.. raw:: html

   </itemizedlist>

The covariance is calculated according to the following formula: |image50|\ |image51|

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the covariance can be found at http://en.wikipedia.org/wiki/Covariance and at http://mathworld.wolfram.com/Covariance.html.

The covariance.

Method *Covariance*
^^^^^^^^^^^^^^^^^^^

``System.Double Covariance(ViewLocatorUInt16 source1, ViewLocatorUInt16 source2)``

Computes the covariance of the two ranges.

The method **Covariance** has the following parameters:

+---------------+-------------------------+---------------+
| Parameter     | Type                    | Description   |
+===============+=========================+===============+
| ``source1``   | ``ViewLocatorUInt16``   |               |
+---------------+-------------------------+---------------+
| ``source2``   | ``ViewLocatorUInt16``   |               |
+---------------+-------------------------+---------------+

The covariance is a measure of how much two different signals vary, or

.. raw:: html

   <itemizedlist>

in other words - the strength of the correlation between the two signals.

.. raw:: html

   </itemizedlist>

The covariance is calculated according to the following formula: |image52|\ |image53|

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the covariance can be found at http://en.wikipedia.org/wiki/Covariance and at http://mathworld.wolfram.com/Covariance.html.

The covariance.

Method *Covariance*
^^^^^^^^^^^^^^^^^^^

``System.Double Covariance(ViewLocatorUInt32 source1, ViewLocatorUInt32 source2)``

Computes the covariance of the two ranges.

The method **Covariance** has the following parameters:

+---------------+-------------------------+---------------+
| Parameter     | Type                    | Description   |
+===============+=========================+===============+
| ``source1``   | ``ViewLocatorUInt32``   |               |
+---------------+-------------------------+---------------+
| ``source2``   | ``ViewLocatorUInt32``   |               |
+---------------+-------------------------+---------------+

The covariance is a measure of how much two different signals vary, or

.. raw:: html

   <itemizedlist>

in other words - the strength of the correlation between the two signals.

.. raw:: html

   </itemizedlist>

The covariance is calculated according to the following formula: |image54|\ |image55|

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the covariance can be found at http://en.wikipedia.org/wiki/Covariance and at http://mathworld.wolfram.com/Covariance.html.

The covariance.

Method *Covariance*
^^^^^^^^^^^^^^^^^^^

``System.Double Covariance(ViewLocatorDouble source1, ViewLocatorDouble source2)``

Computes the covariance of the two ranges.

The method **Covariance** has the following parameters:

+---------------+-------------------------+---------------+
| Parameter     | Type                    | Description   |
+===============+=========================+===============+
| ``source1``   | ``ViewLocatorDouble``   |               |
+---------------+-------------------------+---------------+
| ``source2``   | ``ViewLocatorDouble``   |               |
+---------------+-------------------------+---------------+

The covariance is a measure of how much two different signals vary, or

.. raw:: html

   <itemizedlist>

in other words - the strength of the correlation between the two signals.

.. raw:: html

   </itemizedlist>

The covariance is calculated according to the following formula: |image56|\ |image57|

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the covariance can be found at http://en.wikipedia.org/wiki/Covariance and at http://mathworld.wolfram.com/Covariance.html.

The covariance.

Method *Covariance*
^^^^^^^^^^^^^^^^^^^

``System.Double Covariance(View source1, View source2)``

Computes the covariance of the two ranges.

The method **Covariance** has the following parameters:

+---------------+------------+---------------+
| Parameter     | Type       | Description   |
+===============+============+===============+
| ``source1``   | ``View``   |               |
+---------------+------------+---------------+
| ``source2``   | ``View``   |               |
+---------------+------------+---------------+

The covariance is a measure of how much two different signals vary, or

.. raw:: html

   <itemizedlist>

in other words - the strength of the correlation between the two signals.

.. raw:: html

   </itemizedlist>

The covariance is calculated according to the following formula: |image58|\ |image59|

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

More information about the covariance can be found at http://en.wikipedia.org/wiki/Covariance and at http://mathworld.wolfram.com/Covariance.html.

The covariance.

Method *SquaredCrossCorrelation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double SquaredCrossCorrelation(ViewLocatorByte source1, ViewLocatorByte source2)``

Computes the normalized squared cross correlation.

The method **SquaredCrossCorrelation** has the following parameters:

+---------------+-----------------------+---------------+
| Parameter     | Type                  | Description   |
+===============+=======================+===============+
| ``source1``   | ``ViewLocatorByte``   |               |
+---------------+-----------------------+---------------+
| ``source2``   | ``ViewLocatorByte``   |               |
+---------------+-----------------------+---------------+

The normalized cross correlation is calculated according to the following formula:

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The squared cross-correlation is faster to calculate, since it avoids the application of a square root.

The sqaured cross correlation coefficient.

Method *SquaredCrossCorrelation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double SquaredCrossCorrelation(ViewLocatorUInt16 source1, ViewLocatorUInt16 source2)``

Computes the normalized squared cross correlation.

The method **SquaredCrossCorrelation** has the following parameters:

+---------------+-------------------------+---------------+
| Parameter     | Type                    | Description   |
+===============+=========================+===============+
| ``source1``   | ``ViewLocatorUInt16``   |               |
+---------------+-------------------------+---------------+
| ``source2``   | ``ViewLocatorUInt16``   |               |
+---------------+-------------------------+---------------+

The normalized cross correlation is calculated according to the following formula:

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The squared cross-correlation is faster to calculate, since it avoids the application of a square root.

The sqaured cross correlation coefficient.

Method *SquaredCrossCorrelation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double SquaredCrossCorrelation(ViewLocatorUInt32 source1, ViewLocatorUInt32 source2)``

Computes the normalized squared cross correlation.

The method **SquaredCrossCorrelation** has the following parameters:

+---------------+-------------------------+---------------+
| Parameter     | Type                    | Description   |
+===============+=========================+===============+
| ``source1``   | ``ViewLocatorUInt32``   |               |
+---------------+-------------------------+---------------+
| ``source2``   | ``ViewLocatorUInt32``   |               |
+---------------+-------------------------+---------------+

The normalized cross correlation is calculated according to the following formula:

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The squared cross-correlation is faster to calculate, since it avoids the application of a square root.

The sqaured cross correlation coefficient.

Method *SquaredCrossCorrelation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double SquaredCrossCorrelation(ViewLocatorDouble source1, ViewLocatorDouble source2)``

Computes the normalized squared cross correlation.

The method **SquaredCrossCorrelation** has the following parameters:

+---------------+-------------------------+---------------+
| Parameter     | Type                    | Description   |
+===============+=========================+===============+
| ``source1``   | ``ViewLocatorDouble``   |               |
+---------------+-------------------------+---------------+
| ``source2``   | ``ViewLocatorDouble``   |               |
+---------------+-------------------------+---------------+

The normalized cross correlation is calculated according to the following formula:

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The squared cross-correlation is faster to calculate, since it avoids the application of a square root.

The sqaured cross correlation coefficient.

Method *SquaredCrossCorrelation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double SquaredCrossCorrelation(View source1, View source2)``

Computes the normalized squared cross correlation.

The method **SquaredCrossCorrelation** has the following parameters:

+---------------+------------+---------------+
| Parameter     | Type       | Description   |
+===============+============+===============+
| ``source1``   | ``View``   |               |
+---------------+------------+---------------+
| ``source2``   | ``View``   |               |
+---------------+------------+---------------+

The normalized cross correlation is calculated according to the following formula:

The value\_type of the view must implement operator+, operator- and operator/ with a scalar, otherwise this function will cause a compile error.

The squared cross-correlation is faster to calculate, since it avoids the application of a square root.

The sqaured cross correlation coefficient.

Method *CrossCorrelation*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double CrossCorrelation(ViewLocatorByte source1, ViewLocatorByte source2)``

Computes the normalized cross correlation.

The method **CrossCorrelation** has the following parameters:

+---------------+-----------------------+---------------+
| Parameter     | Type                  | Description   |
+===============+=======================+===============+
| ``source1``   | ``ViewLocatorByte``   |               |
+---------------+-----------------------+---------------+
| ``source2``   | ``ViewLocatorByte``   |               |
+---------------+-----------------------+---------------+

The normalized cross correlation is calculated according to the following formula:

The cross correlation coefficient.

Method *CrossCorrelation*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double CrossCorrelation(ViewLocatorUInt16 source1, ViewLocatorUInt16 source2)``

Computes the normalized cross correlation.

The method **CrossCorrelation** has the following parameters:

+---------------+-------------------------+---------------+
| Parameter     | Type                    | Description   |
+===============+=========================+===============+
| ``source1``   | ``ViewLocatorUInt16``   |               |
+---------------+-------------------------+---------------+
| ``source2``   | ``ViewLocatorUInt16``   |               |
+---------------+-------------------------+---------------+

The normalized cross correlation is calculated according to the following formula:

The cross correlation coefficient.

Method *CrossCorrelation*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double CrossCorrelation(ViewLocatorUInt32 source1, ViewLocatorUInt32 source2)``

Computes the normalized cross correlation.

The method **CrossCorrelation** has the following parameters:

+---------------+-------------------------+---------------+
| Parameter     | Type                    | Description   |
+===============+=========================+===============+
| ``source1``   | ``ViewLocatorUInt32``   |               |
+---------------+-------------------------+---------------+
| ``source2``   | ``ViewLocatorUInt32``   |               |
+---------------+-------------------------+---------------+

The normalized cross correlation is calculated according to the following formula:

The cross correlation coefficient.

Method *CrossCorrelation*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double CrossCorrelation(ViewLocatorDouble source1, ViewLocatorDouble source2)``

Computes the normalized cross correlation.

The method **CrossCorrelation** has the following parameters:

+---------------+-------------------------+---------------+
| Parameter     | Type                    | Description   |
+===============+=========================+===============+
| ``source1``   | ``ViewLocatorDouble``   |               |
+---------------+-------------------------+---------------+
| ``source2``   | ``ViewLocatorDouble``   |               |
+---------------+-------------------------+---------------+

The normalized cross correlation is calculated according to the following formula:

The cross correlation coefficient.

Method *CrossCorrelation*
^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double CrossCorrelation(View source1, View source2)``

Computes the normalized cross correlation.

The method **CrossCorrelation** has the following parameters:

+---------------+------------+---------------+
| Parameter     | Type       | Description   |
+===============+============+===============+
| ``source1``   | ``View``   |               |
+---------------+------------+---------------+
| ``source2``   | ``View``   |               |
+---------------+------------+---------------+

The normalized cross correlation is calculated according to the following formula:

The cross correlation coefficient.

Method *OtsuThreshold*
^^^^^^^^^^^^^^^^^^^^^^

``System.Byte OtsuThreshold(ViewLocatorByte source)``

Calculates an optimal threshold.

The method **OtsuThreshold** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

This overload calculates an optimal threshold from image data using the method described by Otsu.

Internally it first calculates a histogram from the image view, and then calculates the optimal threshold from this histogram.

An optimal threshold calculated from image data.

Method *OtsuThreshold*
^^^^^^^^^^^^^^^^^^^^^^

``System.UInt16 OtsuThreshold(ViewLocatorUInt16 source)``

Calculates an optimal threshold.

The method **OtsuThreshold** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

This overload calculates an optimal threshold from image data using the method described by Otsu.

Internally it first calculates a histogram from the image view, and then calculates the optimal threshold from this histogram.

An optimal threshold calculated from image data.

Method *OtsuThreshold*
^^^^^^^^^^^^^^^^^^^^^^

``System.UInt32 OtsuThreshold(ViewLocatorUInt32 source)``

Calculates an optimal threshold.

The method **OtsuThreshold** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

This overload calculates an optimal threshold from image data using the method described by Otsu.

Internally it first calculates a histogram from the image view, and then calculates the optimal threshold from this histogram.

An optimal threshold calculated from image data.

Method *OtsuThreshold*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double OtsuThreshold(ViewLocatorDouble source)``

Calculates an optimal threshold.

The method **OtsuThreshold** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

This overload calculates an optimal threshold from image data using the method described by Otsu.

Internally it first calculates a histogram from the image view, and then calculates the optimal threshold from this histogram.

An optimal threshold calculated from image data.

Method *OtsuThreshold*
^^^^^^^^^^^^^^^^^^^^^^

``object OtsuThreshold(View source)``

Calculates an optimal threshold.

The method **OtsuThreshold** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

This overload calculates an optimal threshold from image data using the method described by Otsu.

Internally it first calculates a histogram from the image view, and then calculates the optimal threshold from this histogram.

An optimal threshold calculated from image data.

Method *OtsuThresholdFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.UInt32 OtsuThresholdFromHistogram(ViewLocatorUInt32 histogram)``

Computes the specified quantile given a histogram.

The method **OtsuThresholdFromHistogram** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``histogram``   | ``ViewLocatorUInt32``   |               |
+-----------------+-------------------------+---------------+

The given data is interpreted as being a histogram, and the quantile is computed with respect to the buffer\_base that was used to compute the histogram.

If the view contains valid histogram data, suitable results can always be found and returned. However, if the view contains entries that are all zero, correct results cannot be determined. In this case the returned value will be -1 cast to type T.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The specified quantile, or -1 cast to T if the passed histogram was not valid.

Method *OtsuThresholdFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double OtsuThresholdFromHistogram(ViewLocatorDouble histogram)``

Computes the specified quantile given a histogram.

The method **OtsuThresholdFromHistogram** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``histogram``   | ``ViewLocatorDouble``   |               |
+-----------------+-------------------------+---------------+

The given data is interpreted as being a histogram, and the quantile is computed with respect to the buffer\_base that was used to compute the histogram.

If the view contains valid histogram data, suitable results can always be found and returned. However, if the view contains entries that are all zero, correct results cannot be determined. In this case the returned value will be -1 cast to type T.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The specified quantile, or -1 cast to T if the passed histogram was not valid.

Method *OtsuThresholdFromHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``object OtsuThresholdFromHistogram(View histogram)``

Computes the specified quantile given a histogram.

The method **OtsuThresholdFromHistogram** has the following parameters:

+-----------------+------------+---------------+
| Parameter       | Type       | Description   |
+=================+============+===============+
| ``histogram``   | ``View``   |               |
+-----------------+------------+---------------+

The given data is interpreted as being a histogram, and the quantile is computed with respect to the buffer\_base that was used to compute the histogram.

If the view contains valid histogram data, suitable results can always be found and returned. However, if the view contains entries that are all zero, correct results cannot be determined. In this case the returned value will be -1 cast to type T.

This histogram view must be one-dimensional (if its extent in the y and z dimensions is bigger than 1, only the first plane and line of data is used).

The specified quantile, or -1 cast to T if the passed histogram was not valid.

.. |image0| image:: images/average.png
.. |image1| image:: images/average.png
.. |image2| image:: images/average.png
.. |image3| image:: images/average.png
.. |image4| image:: images/average.png
.. |image5| image:: images/average.png
.. |image6| image:: images/average.png
.. |image7| image:: images/average.png
.. |image8| image:: images/average.png
.. |image9| image:: images/average.png
.. |image10| image:: images/average.png
.. |image11| image:: images/average.png
.. |image12| image:: images/average.png
.. |image13| image:: images/average.png
.. |image14| image:: images/average.png
.. |image15| image:: images/average.png
.. |image16| image:: images/average.png
.. |image17| image:: images/average.png
.. |image18| image:: images/average.png
.. |image19| image:: images/average.png
.. |image20| image:: images/average.png
.. |image21| image:: images/average.png
.. |image22| image:: images/average.png
.. |image23| image:: images/average.png
.. |image24| image:: images/average.png
.. |image25| image:: images/average.png
.. |image26| image:: images/average.png
.. |image27| image:: images/average.png
.. |image28| image:: images/average.png
.. |image29| image:: images/average.png
.. |image30| image:: images/average.png
.. |image31| image:: images/average.png
.. |image32| image:: images/average.png
.. |image33| image:: images/average.png
.. |image34| image:: images/average.png
.. |image35| image:: images/average.png
.. |image36| image:: images/average.png
.. |image37| image:: images/average.png
.. |image38| image:: images/average.png
.. |image39| image:: images/average.png
.. |image40| image:: images/average.png
.. |image41| image:: images/average.png
.. |image42| image:: images/average.png
.. |image43| image:: images/average.png
.. |image44| image:: images/average.png
.. |image45| image:: images/average.png
.. |image46| image:: images/average.png
.. |image47| image:: images/average.png
.. |image48| image:: images/average.png
.. |image49| image:: images/average.png
.. |image50| image:: images/covariance_0.png
.. |image51| image:: images/covariance_1.png
.. |image52| image:: images/covariance_0.png
.. |image53| image:: images/covariance_1.png
.. |image54| image:: images/covariance_0.png
.. |image55| image:: images/covariance_1.png
.. |image56| image:: images/covariance_0.png
.. |image57| image:: images/covariance_1.png
.. |image58| image:: images/covariance_0.png
.. |image59| image:: images/covariance_1.png

