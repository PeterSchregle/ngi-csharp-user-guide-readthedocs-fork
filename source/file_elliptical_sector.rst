Class *EllipticalSector*
------------------------

The geometric elliptical\_sector primitive.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **EllipticalSector** implements the following interfaces:

+----------------------------------+
| Interface                        |
+==================================+
| ``IEquatableEllipticalSector``   |
+----------------------------------+
| ``ISerializable``                |
+----------------------------------+
| ``INotifyPropertyChanged``       |
+----------------------------------+

The class **EllipticalSector** contains the following properties:

+-------------------+-------+-------+---------------------------------------------------+
| Property          | Get   | Set   | Description                                       |
+===================+=======+=======+===================================================+
| ``Ring``          | \*    | \*    | The center of the elliptical\_sector.             |
+-------------------+-------+-------+---------------------------------------------------+
| ``StartAngle``    | \*    | \*    | The start angle of the elliptical\_sector.        |
+-------------------+-------+-------+---------------------------------------------------+
| ``EndAngle``      | \*    | \*    | The end angle of the elliptical\_sector.          |
+-------------------+-------+-------+---------------------------------------------------+
| ``InnerArc``      | \*    |       | The inner arc of the elliptical\_sector.          |
+-------------------+-------+-------+---------------------------------------------------+
| ``OuterArc``      | \*    |       | The outer arc of the elliptical\_sector.          |
+-------------------+-------+-------+---------------------------------------------------+
| ``BoundingBox``   | \*    |       | Get the bounding box of the elliptical\_sector.   |
+-------------------+-------+-------+---------------------------------------------------+

The class **EllipticalSector** contains the following methods:

+------------------------+----------------------------------------------------------------------------------------------------------------------+
| Method                 | Description                                                                                                          |
+========================+======================================================================================================================+
| ``Normalize``          | A normalized elliptical\_sector has positive radii, and the outer radius is bigger or equal than the inner radius.   |
+------------------------+----------------------------------------------------------------------------------------------------------------------+
| ``NormalizeInPlace``   | A normalized elliptical\_sector has positive radii, and the outer radius is bigger or equal than the inner radius.   |
+------------------------+----------------------------------------------------------------------------------------------------------------------+
| ``ToString``           | Provide string representation for debugging purposes.                                                                |
+------------------------+----------------------------------------------------------------------------------------------------------------------+

Description
~~~~~~~~~~~

A elliptical\_sector can be seen as two arcs with the same center, starting and ending angles, but different radii.

The following operations are implemented: operator == : comparison for equality. operator != : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *EllipticalSector*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``EllipticalSector()``

Default constructor.

The default constructor builds a elliptical\_sector with its center at the origin and zero inner and outer radii.

Constructors
~~~~~~~~~~~~

Constructor *EllipticalSector*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``EllipticalSector(Ring ring, System.Double startAngle, System.Double endAngle)``

Standard constructor.

The constructor has the following parameters:

+------------------+---------------------+--------------------+
| Parameter        | Type                | Description        |
+==================+=====================+====================+
| ``ring``         | ``Ring``            | The ring.          |
+------------------+---------------------+--------------------+
| ``startAngle``   | ``System.Double``   | The start angle.   |
+------------------+---------------------+--------------------+
| ``endAngle``     | ``System.Double``   | The end angle.     |
+------------------+---------------------+--------------------+

Construct a elliptical\_sector from its ring and the starting and ending angles.

Properties
~~~~~~~~~~

Property *Ring*
^^^^^^^^^^^^^^^

``Ring Ring``

The center of the elliptical\_sector.

Property *StartAngle*
^^^^^^^^^^^^^^^^^^^^^

``System.Double StartAngle``

The start angle of the elliptical\_sector.

Property *EndAngle*
^^^^^^^^^^^^^^^^^^^

``System.Double EndAngle``

The end angle of the elliptical\_sector.

Property *InnerArc*
^^^^^^^^^^^^^^^^^^^

``Arc InnerArc``

The inner arc of the elliptical\_sector.

The outer arc starts at the end angle and ends at the start angle. The outer\_arc, end\_line\_segment, inner\_arc and start\_line\_segment can be used in this order to build up a geometry.

Property *OuterArc*
^^^^^^^^^^^^^^^^^^^

``Arc OuterArc``

The outer arc of the elliptical\_sector.

The outer arc starts at the start angle and ends at the end angle. The outer\_arc, end\_line\_segment, inner\_arc and start\_line\_segment can be used in this order to build up a geometry.

Property *BoundingBox*
^^^^^^^^^^^^^^^^^^^^^^

``BoxDouble BoundingBox``

Get the bounding box of the elliptical\_sector.

The bounding box is an axis aligned box that bounds the elliptical\_sector.

The bounding box of the elliptical\_sector.

Methods
~~~~~~~

Method *Normalize*
^^^^^^^^^^^^^^^^^^

``EllipticalSector Normalize()``

A normalized elliptical\_sector has positive radii, and the outer radius is bigger or equal than the inner radius.

This method checks these conditions and returns a normalized variant of the elliptical\_sector.

Method *NormalizeInPlace*
^^^^^^^^^^^^^^^^^^^^^^^^^

``void NormalizeInPlace()``

A normalized elliptical\_sector has positive radii, and the outer radius is bigger or equal than the inner radius.

This method checks these conditions and returns a normalized variant of the elliptical\_sector. It modifies the elliptical\_sector in place.

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO no brief description for variant

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO no description for variant
