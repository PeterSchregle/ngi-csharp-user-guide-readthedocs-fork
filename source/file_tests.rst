Class *Tests*
-------------

Wraps free-standing functions.

**Namespace:** Ngi

**Module:**

Description
~~~~~~~~~~~

The purpose of this class is to make the functions available for the .NET wrapper.

Static Methods
~~~~~~~~~~~~~~

Method *CauseNativeException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double CauseNativeException()``

Method *CauseDivByZeroException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double CauseDivByZeroException(System.Int32 dividend)``

The method **CauseDivByZeroException** has the following parameters:

+----------------+--------------------+---------------+
| Parameter      | Type               | Description   |
+================+====================+===============+
| ``dividend``   | ``System.Int32``   |               |
+----------------+--------------------+---------------+
