Class *Image*
-------------

...

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Image** contains the following variant parameters:

+-------------+-----------------------------------------+
| Variant     | Description                             |
+=============+=========================================+
| ``Pixel``   | TODO no brief description for variant   |
+-------------+-----------------------------------------+

The class **Image** contains the following methods:

+--------------------------+---------------------------------------------------------------+
| Method                   | Description                                                   |
+==========================+===============================================================+
| ``ExportImage``          | Exports an image to a file.                                   |
+--------------------------+---------------------------------------------------------------+
| ``ExportImage``          | Exports an image to memory.                                   |
+--------------------------+---------------------------------------------------------------+
| ``ExportImageDl``        | Exports an image to a data\_list.                             |
+--------------------------+---------------------------------------------------------------+
| ``ConvertToRgbaImage``   | Converts an image of any type to an image with rgba pixels.   |
+--------------------------+---------------------------------------------------------------+
| ``ConvertToRgbaImage``   | Converts an image of any type to an image with rgba pixels.   |
+--------------------------+---------------------------------------------------------------+
| ``MapOverPalette``       | Maps an image over a palette.                                 |
+--------------------------+---------------------------------------------------------------+
| ``MapOverPalette``       | Maps an image over a palette.                                 |
+--------------------------+---------------------------------------------------------------+
| ``MapOverPalette``       |                                                               |
+--------------------------+---------------------------------------------------------------+
| ``MapOverPalette``       |                                                               |
+--------------------------+---------------------------------------------------------------+
| ``MapOverPalette``       |                                                               |
+--------------------------+---------------------------------------------------------------+
| ``MapOverPalette``       |                                                               |
+--------------------------+---------------------------------------------------------------+
| ``MapOverPalette``       |                                                               |
+--------------------------+---------------------------------------------------------------+
| ``MapOverPalette``       |                                                               |
+--------------------------+---------------------------------------------------------------+
| ``MapOverPalette``       |                                                               |
+--------------------------+---------------------------------------------------------------+
| ``MapOverPalette``       |                                                               |
+--------------------------+---------------------------------------------------------------+
| ``MapOverPalette``       |                                                               |
+--------------------------+---------------------------------------------------------------+
| ``MapOverPalette``       |                                                               |
+--------------------------+---------------------------------------------------------------+
| ``MapOverPalette``       |                                                               |
+--------------------------+---------------------------------------------------------------+
| ``MapOverPalette``       |                                                               |
+--------------------------+---------------------------------------------------------------+
| ``MapOverPalette``       |                                                               |
+--------------------------+---------------------------------------------------------------+
| ``MapOverPalette``       |                                                               |
+--------------------------+---------------------------------------------------------------+
| ``MapOverPalette``       |                                                               |
+--------------------------+---------------------------------------------------------------+
| ``MapOverPalette``       |                                                               |
+--------------------------+---------------------------------------------------------------+
| ``MapOverPalette``       |                                                               |
+--------------------------+---------------------------------------------------------------+
| ``MapOverPalette``       |                                                               |
+--------------------------+---------------------------------------------------------------+
| ``MapOverPalette``       |                                                               |
+--------------------------+---------------------------------------------------------------+
| ``MapOverPalette``       |                                                               |
+--------------------------+---------------------------------------------------------------+
| ``MapOverPalette``       |                                                               |
+--------------------------+---------------------------------------------------------------+
| ``MapOverPalette``       |                                                               |
+--------------------------+---------------------------------------------------------------+
| ``Crop``                 | Crops an image.                                               |
+--------------------------+---------------------------------------------------------------+
| ``Crop``                 | Crops an image.                                               |
+--------------------------+---------------------------------------------------------------+

Description
~~~~~~~~~~~

The base class.

Variants
~~~~~~~~

Variant *Pixel*
^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Pixel** has the following types:

+------------------+
| Type             |
+==================+
| ``Byte``         |
+------------------+
| ``UInt16``       |
+------------------+
| ``UInt32``       |
+------------------+
| ``Double``       |
+------------------+
| ``RgbByte``      |
+------------------+
| ``RgbUInt16``    |
+------------------+
| ``RgbUInt32``    |
+------------------+
| ``RgbDouble``    |
+------------------+
| ``RgbaByte``     |
+------------------+
| ``RgbaUInt16``   |
+------------------+
| ``RgbaUInt32``   |
+------------------+
| ``RgbaDouble``   |
+------------------+
| ``HlsByte``      |
+------------------+
| ``HlsUInt16``    |
+------------------+
| ``HlsDouble``    |
+------------------+
| ``HsiByte``      |
+------------------+
| ``HsiUInt16``    |
+------------------+
| ``HsiDouble``    |
+------------------+
| ``LabByte``      |
+------------------+
| ``LabUInt16``    |
+------------------+
| ``LabDouble``    |
+------------------+
| ``XyzByte``      |
+------------------+
| ``XyzUInt16``    |
+------------------+
| ``XyzDouble``    |
+------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Image*
^^^^^^^^^^^^^^^^^^^

``Image()``

Constructs an empty image.

Constructors
~~~~~~~~~~~~

Constructor *Image*
^^^^^^^^^^^^^^^^^^^

``Image(Extent3d size, System.Object value)``

Constructs an image of a specific size with all elements not initialized.

The constructor has the following parameters:

+-------------+---------------------+--------------------------------------+
| Parameter   | Type                | Description                          |
+=============+=====================+======================================+
| ``size``    | ``Extent3d``        | The size of the constructed image.   |
+-------------+---------------------+--------------------------------------+
| ``value``   | ``System.Object``   |                                      |
+-------------+---------------------+--------------------------------------+

Constructor *Image*
^^^^^^^^^^^^^^^^^^^

``Image(System.Int32 width, System.Int32 height, System.Int32 depth, System.Object value)``

Constructs an image of a specific size with all elements not initialized.

The constructor has the following parameters:

+--------------+---------------------+----------------------------------------+
| Parameter    | Type                | Description                            |
+==============+=====================+========================================+
| ``width``    | ``System.Int32``    | The width of the constructed image.    |
+--------------+---------------------+----------------------------------------+
| ``height``   | ``System.Int32``    | The height of the constructed image.   |
+--------------+---------------------+----------------------------------------+
| ``depth``    | ``System.Int32``    | The depth of the constructed image.    |
+--------------+---------------------+----------------------------------------+
| ``value``    | ``System.Object``   |                                        |
+--------------+---------------------+----------------------------------------+

Static Methods
~~~~~~~~~~~~~~

Method *ImportImage*
^^^^^^^^^^^^^^^^^^^^

``Image ImportImage(System.String filename)``

Imports an image from a file.

The method **ImportImage** has the following parameters:

+----------------+---------------------+---------------+
| Parameter      | Type                | Description   |
+================+=====================+===============+
| ``filename``   | ``System.String``   |               |
+----------------+---------------------+---------------+

This function loads an image using FreeImage and then copies it into a data structure of type image.

The filename can either be a complete (absolute or relative) path to a file, or it can be a partial filename.

If a complete path to a file is given, the function tries to load the respective file. If a partial filename is given, the function appends it to the path fragment stored in the NGI\_IMAGE\_PATH environment variable. It then tries to load the file addressed by this combined path.

If the type of the image does not fit the type of the data in the file, the function attempts to do a conversion.

The loaded data in a image.

Method *ImportImage*
^^^^^^^^^^^^^^^^^^^^

``Image ImportImage(System.IntPtr memory, System.Int32 size)``

Imports an image from memory.

The method **ImportImage** has the following parameters:

+--------------+---------------------+---------------+
| Parameter    | Type                | Description   |
+==============+=====================+===============+
| ``memory``   | ``System.IntPtr``   |               |
+--------------+---------------------+---------------+
| ``size``     | ``System.Int32``    |               |
+--------------+---------------------+---------------+

This function loads an image using FreeImage and then copies it into a data structure of type image.

If the type of the image does not fit the type of the data in memory, the function attempts to do a conversion.

The loaded data in a image.

Method *ImportImage*
^^^^^^^^^^^^^^^^^^^^

``Image ImportImage(DataList data)``

Imports an image from a data\_list.

The method **ImportImage** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``data``    | ``DataList``   |               |
+-------------+----------------+---------------+

This function loads an image using FreeImage and then copies it into a data structure of type image.

If the type of the image does not fit the type of the data in memory, the function attempts to do a conversion.

The loaded data in a image.

Method *DistanceTransform*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``Image DistanceTransform(Region obj, Extent3d size)``

Preset an image with a distance transform given a region.

The method **DistanceTransform** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``obj``     | ``Region``     |               |
+-------------+----------------+---------------+
| ``size``    | ``Extent3d``   |               |
+-------------+----------------+---------------+

Method *DistanceTransform*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``Image DistanceTransform(Region obj, Extent3d size, System.Int32 maxDistance)``

Preset an image with a distance transform given a region.

The method **DistanceTransform** has the following parameters:

+-------------------+--------------------+---------------+
| Parameter         | Type               | Description   |
+===================+====================+===============+
| ``obj``           | ``Region``         |               |
+-------------------+--------------------+---------------+
| ``size``          | ``Extent3d``       |               |
+-------------------+--------------------+---------------+
| ``maxDistance``   | ``System.Int32``   |               |
+-------------------+--------------------+---------------+

The distance computation can be constrained to a certain maximum distance for efficiency.

Method *DistanceTransform*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``Image DistanceTransform(Region obj)``

Preset an image with a distance transform given a region.

The method **DistanceTransform** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``obj``     | ``Region``   |               |
+-------------+--------------+---------------+

This function is depreated, since it

.. raw:: html

   <itemizedlist>

provides no way to specify the size of the resulting image

translates the region

.. raw:: html

   </itemizedlist>

You should use the version with the size parameter.

Method *DistanceTransform*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``Image DistanceTransform(Region obj, System.Int32 maxDistance)``

Preset an image with a distance transform given a region.

The method **DistanceTransform** has the following parameters:

+-------------------+--------------------+---------------+
| Parameter         | Type               | Description   |
+===================+====================+===============+
| ``obj``           | ``Region``         |               |
+-------------------+--------------------+---------------+
| ``maxDistance``   | ``System.Int32``   |               |
+-------------------+--------------------+---------------+

The distance computation can be constrained to a certain maximum distance for efficiency

This function is depreated, since it

.. raw:: html

   <itemizedlist>

provides no way to specify the size of the resulting image

translates the region

.. raw:: html

   </itemizedlist>

You should use the version with the size parameter.

Methods
~~~~~~~

Method *ExportImage*
^^^^^^^^^^^^^^^^^^^^

``void ExportImage(System.String filename)``

Exports an image to a file.

The method **ExportImage** has the following parameters:

+----------------+---------------------+-------------------------------------+
| Parameter      | Type                | Description                         |
+================+=====================+=====================================+
| ``filename``   | ``System.String``   | The pathname of the file to save.   |
+----------------+---------------------+-------------------------------------+

The pathname must be a complete (absolute or relative) path to a file.

This function copies a data structure of type image into a structure that FreeImage understands, and then saves it to a file.

The type of the file is determined by examining the extension of the Pathname.

This function only saves the first plane.

Method *ExportImage*
^^^^^^^^^^^^^^^^^^^^

``System.Int32 ExportImage(System.IntPtr memory, System.String format)``

Exports an image to memory.

The method **ExportImage** has the following parameters:

+--------------+---------------------+---------------------+
| Parameter    | Type                | Description         |
+==============+=====================+=====================+
| ``memory``   | ``System.IntPtr``   | The memory block.   |
+--------------+---------------------+---------------------+
| ``format``   | ``System.String``   | The format.         |
+--------------+---------------------+---------------------+

The memory block must be allocated in the proper size before calling this function with a non-null memory parameter. In order to find out the proper size, call the function with a null value for memory.

This function copies a data structure of type BUFFER into a structure that FreeImage understands, and then saves it to memory.

The format is determined by giving a file extension.

This function only saves the first plane.

Method *ExportImageDl*
^^^^^^^^^^^^^^^^^^^^^^

``DataList ExportImageDl(System.String format)``

Exports an image to a data\_list.

The method **ExportImageDl** has the following parameters:

+--------------+---------------------+---------------+
| Parameter    | Type                | Description   |
+==============+=====================+===============+
| ``format``   | ``System.String``   | The format.   |
+--------------+---------------------+---------------+

This function copies a data structure of type BUFFER into a structure that FreeImage understands, and then saves it to a data\_list.

The format is determined by giving a file extension.

This function only saves the first plane.

/return The data\_list.

Method *ConvertToRgbaImage*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte ConvertToRgbaImage()``

Converts an image of any type to an image with rgba pixels.

The function allocates an image in 32 bit rgba format. It copies the pixels from the source view into the image, performing potential color space conversion on the fly.

This is the catch-all function that can convert all types of color spaces.

Method *ConvertToRgbaImage*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte ConvertToRgbaImage(System.Double offset, System.Double scale)``

Converts an image of any type to an image with rgba pixels.

The method **ConvertToRgbaImage** has the following parameters:

+--------------+---------------------+----------------------------------------------+
| Parameter    | Type                | Description                                  |
+==============+=====================+==============================================+
| ``offset``   | ``System.Double``   | The offset to be added to the pixel value.   |
+--------------+---------------------+----------------------------------------------+
| ``scale``    | ``System.Double``   | The scale factor to be applied,              |
+--------------+---------------------+----------------------------------------------+

The function allocates an image in 32 bit rgba format. It copies the pixels from the source view into the image, while adding an offset and scaling by scale, performing potential color space conversion on the fly.

This is the catch-all function that can convert all types of color spaces.

Method *MapOverPalette*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte MapOverPalette(ViewLocatorByte palette)``

Maps an image over a palette.

The method **MapOverPalette** has the following parameters:

+---------------+-----------------------+---------------------+
| Parameter     | Type                  | Description         |
+===============+=======================+=====================+
| ``palette``   | ``ViewLocatorByte``   | The palette view.   |
+---------------+-----------------------+---------------------+

The function works with unsigned char and unsigned short types only. It is not implemented for unsigned int and double types, since the mapping palettes would need to be ridicoulously big.

If the palette has one channel only, this same channel is used to map every source channel. If the palette has multiple channels, this must correspond with the source channels, and every palette channel is used to map the respective source channel.

The algorithm supports parallel execution on multiple cores.

Method *MapOverPalette*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 MapOverPalette(ViewLocatorUInt16 palette)``

Maps an image over a palette.

The method **MapOverPalette** has the following parameters:

+---------------+-------------------------+---------------------+
| Parameter     | Type                    | Description         |
+===============+=========================+=====================+
| ``palette``   | ``ViewLocatorUInt16``   | The palette view.   |
+---------------+-------------------------+---------------------+

The region parameter constrains the function to the region inside of the view only.

The function works with unsigned char and unsigned short types only. It is not implemented for unsigned int and double types, since the mapping palettes would need to be ridicoulously big.

If the palette has one channel only, this same channel is used to map every source channel. If the palette has multiple channels, this must correspond with the source channels, and every palette channel is used to map the respective source channel.

The algorithm supports parallel execution on multiple cores.

Method *MapOverPalette*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 MapOverPalette(ViewLocatorUInt32 palette)``

The method **MapOverPalette** has the following parameters:

+---------------+-------------------------+---------------+
| Parameter     | Type                    | Description   |
+===============+=========================+===============+
| ``palette``   | ``ViewLocatorUInt32``   |               |
+---------------+-------------------------+---------------+

Method *MapOverPalette*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble MapOverPalette(ViewLocatorDouble palette)``

The method **MapOverPalette** has the following parameters:

+---------------+-------------------------+---------------+
| Parameter     | Type                    | Description   |
+===============+=========================+===============+
| ``palette``   | ``ViewLocatorDouble``   |               |
+---------------+-------------------------+---------------+

Method *MapOverPalette*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte MapOverPalette(ViewLocatorRgbByte palette)``

The method **MapOverPalette** has the following parameters:

+---------------+--------------------------+---------------+
| Parameter     | Type                     | Description   |
+===============+==========================+===============+
| ``palette``   | ``ViewLocatorRgbByte``   |               |
+---------------+--------------------------+---------------+

Method *MapOverPalette*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 MapOverPalette(ViewLocatorRgbUInt16 palette)``

The method **MapOverPalette** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``palette``   | ``ViewLocatorRgbUInt16``   |               |
+---------------+----------------------------+---------------+

Method *MapOverPalette*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 MapOverPalette(ViewLocatorRgbUInt32 palette)``

The method **MapOverPalette** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``palette``   | ``ViewLocatorRgbUInt32``   |               |
+---------------+----------------------------+---------------+

Method *MapOverPalette*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble MapOverPalette(ViewLocatorRgbDouble palette)``

The method **MapOverPalette** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``palette``   | ``ViewLocatorRgbDouble``   |               |
+---------------+----------------------------+---------------+

Method *MapOverPalette*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte MapOverPalette(ViewLocatorRgbaByte palette)``

The method **MapOverPalette** has the following parameters:

+---------------+---------------------------+---------------+
| Parameter     | Type                      | Description   |
+===============+===========================+===============+
| ``palette``   | ``ViewLocatorRgbaByte``   |               |
+---------------+---------------------------+---------------+

Method *MapOverPalette*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 MapOverPalette(ViewLocatorRgbaUInt16 palette)``

The method **MapOverPalette** has the following parameters:

+---------------+-----------------------------+---------------+
| Parameter     | Type                        | Description   |
+===============+=============================+===============+
| ``palette``   | ``ViewLocatorRgbaUInt16``   |               |
+---------------+-----------------------------+---------------+

Method *MapOverPalette*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 MapOverPalette(ViewLocatorRgbaUInt32 palette)``

The method **MapOverPalette** has the following parameters:

+---------------+-----------------------------+---------------+
| Parameter     | Type                        | Description   |
+===============+=============================+===============+
| ``palette``   | ``ViewLocatorRgbaUInt32``   |               |
+---------------+-----------------------------+---------------+

Method *MapOverPalette*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble MapOverPalette(ViewLocatorRgbaDouble palette)``

The method **MapOverPalette** has the following parameters:

+---------------+-----------------------------+---------------+
| Parameter     | Type                        | Description   |
+===============+=============================+===============+
| ``palette``   | ``ViewLocatorRgbaDouble``   |               |
+---------------+-----------------------------+---------------+

Method *MapOverPalette*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte MapOverPalette(Region region, ViewLocatorByte palette)``

The method **MapOverPalette** has the following parameters:

+---------------+-----------------------+---------------+
| Parameter     | Type                  | Description   |
+===============+=======================+===============+
| ``region``    | ``Region``            |               |
+---------------+-----------------------+---------------+
| ``palette``   | ``ViewLocatorByte``   |               |
+---------------+-----------------------+---------------+

Method *MapOverPalette*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 MapOverPalette(Region region, ViewLocatorUInt16 palette)``

The method **MapOverPalette** has the following parameters:

+---------------+-------------------------+---------------+
| Parameter     | Type                    | Description   |
+===============+=========================+===============+
| ``region``    | ``Region``              |               |
+---------------+-------------------------+---------------+
| ``palette``   | ``ViewLocatorUInt16``   |               |
+---------------+-------------------------+---------------+

Method *MapOverPalette*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 MapOverPalette(Region region, ViewLocatorUInt32 palette)``

The method **MapOverPalette** has the following parameters:

+---------------+-------------------------+---------------+
| Parameter     | Type                    | Description   |
+===============+=========================+===============+
| ``region``    | ``Region``              |               |
+---------------+-------------------------+---------------+
| ``palette``   | ``ViewLocatorUInt32``   |               |
+---------------+-------------------------+---------------+

Method *MapOverPalette*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble MapOverPalette(Region region, ViewLocatorDouble palette)``

The method **MapOverPalette** has the following parameters:

+---------------+-------------------------+---------------+
| Parameter     | Type                    | Description   |
+===============+=========================+===============+
| ``region``    | ``Region``              |               |
+---------------+-------------------------+---------------+
| ``palette``   | ``ViewLocatorDouble``   |               |
+---------------+-------------------------+---------------+

Method *MapOverPalette*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte MapOverPalette(Region region, ViewLocatorRgbByte palette)``

The method **MapOverPalette** has the following parameters:

+---------------+--------------------------+---------------+
| Parameter     | Type                     | Description   |
+===============+==========================+===============+
| ``region``    | ``Region``               |               |
+---------------+--------------------------+---------------+
| ``palette``   | ``ViewLocatorRgbByte``   |               |
+---------------+--------------------------+---------------+

Method *MapOverPalette*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 MapOverPalette(Region region, ViewLocatorRgbUInt16 palette)``

The method **MapOverPalette** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``region``    | ``Region``                 |               |
+---------------+----------------------------+---------------+
| ``palette``   | ``ViewLocatorRgbUInt16``   |               |
+---------------+----------------------------+---------------+

Method *MapOverPalette*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 MapOverPalette(Region region, ViewLocatorRgbUInt32 palette)``

The method **MapOverPalette** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``region``    | ``Region``                 |               |
+---------------+----------------------------+---------------+
| ``palette``   | ``ViewLocatorRgbUInt32``   |               |
+---------------+----------------------------+---------------+

Method *MapOverPalette*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble MapOverPalette(Region region, ViewLocatorRgbDouble palette)``

The method **MapOverPalette** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``region``    | ``Region``                 |               |
+---------------+----------------------------+---------------+
| ``palette``   | ``ViewLocatorRgbDouble``   |               |
+---------------+----------------------------+---------------+

Method *MapOverPalette*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte MapOverPalette(Region region, ViewLocatorRgbaByte palette)``

The method **MapOverPalette** has the following parameters:

+---------------+---------------------------+---------------+
| Parameter     | Type                      | Description   |
+===============+===========================+===============+
| ``region``    | ``Region``                |               |
+---------------+---------------------------+---------------+
| ``palette``   | ``ViewLocatorRgbaByte``   |               |
+---------------+---------------------------+---------------+

Method *MapOverPalette*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 MapOverPalette(Region region, ViewLocatorRgbaUInt16 palette)``

The method **MapOverPalette** has the following parameters:

+---------------+-----------------------------+---------------+
| Parameter     | Type                        | Description   |
+===============+=============================+===============+
| ``region``    | ``Region``                  |               |
+---------------+-----------------------------+---------------+
| ``palette``   | ``ViewLocatorRgbaUInt16``   |               |
+---------------+-----------------------------+---------------+

Method *MapOverPalette*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 MapOverPalette(Region region, ViewLocatorRgbaUInt32 palette)``

The method **MapOverPalette** has the following parameters:

+---------------+-----------------------------+---------------+
| Parameter     | Type                        | Description   |
+===============+=============================+===============+
| ``region``    | ``Region``                  |               |
+---------------+-----------------------------+---------------+
| ``palette``   | ``ViewLocatorRgbaUInt32``   |               |
+---------------+-----------------------------+---------------+

Method *MapOverPalette*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble MapOverPalette(Region region, ViewLocatorRgbaDouble palette)``

The method **MapOverPalette** has the following parameters:

+---------------+-----------------------------+---------------+
| Parameter     | Type                        | Description   |
+===============+=============================+===============+
| ``region``    | ``Region``                  |               |
+---------------+-----------------------------+---------------+
| ``palette``   | ``ViewLocatorRgbaDouble``   |               |
+---------------+-----------------------------+---------------+

Method *Crop*
^^^^^^^^^^^^^

``Image Crop(BoxDouble aoi)``

Crops an image.

The method **Crop** has the following parameters:

+-------------+-----------------+---------------------------------------+
| Parameter   | Type            | Description                           |
+=============+=================+=======================================+
| ``aoi``     | ``BoxDouble``   | The rectangular region of interest.   |
+-------------+-----------------+---------------------------------------+

Crops an image to a box. The size of the resulting image is determined by the bounds of the box.

Method *Crop*
^^^^^^^^^^^^^

``Image Crop(Region aoi, System.Object background)``

Crops an image.

The method **Crop** has the following parameters:

+------------------+---------------------+-------------------------------------------------------+
| Parameter        | Type                | Description                                           |
+==================+=====================+=======================================================+
| ``aoi``          | ``Region``          | The area of interest.                                 |
+------------------+---------------------+-------------------------------------------------------+
| ``background``   | ``System.Object``   | The background color to be used outside the region.   |
+------------------+---------------------+-------------------------------------------------------+

Crops an image to an area of interest. The size of the resulting image is determined by the bounds of the area of interest.

If the area of interest is empty, the function defaults to copy the whole image.
