Class *Edge2dInfo*
------------------

The edge\_2d\_info as returned by gauging.

**Namespace:** Ngi

**Module:** AnalysisMeasuring

The class **Edge2dInfo** implements the following interfaces:

+----------------------------+
| Interface                  |
+============================+
| ``IEquatableEdge2dInfo``   |
+----------------------------+
| ``ISerializable``          |
+----------------------------+

The class **Edge2dInfo** contains the following properties:

+-----------------+-------+-------+------------------------------------------------+
| Property        | Get   | Set   | Description                                    |
+=================+=======+=======+================================================+
| ``Edges``       | \*    |       | The edges.                                     |
+-----------------+-------+-------+------------------------------------------------+
| ``Profile``     | \*    |       | The smoothed grey value profile.               |
+-----------------+-------+-------+------------------------------------------------+
| ``Gradient``    | \*    |       | The gradient profile.                          |
+-----------------+-------+-------+------------------------------------------------+
| ``Rectangle``   | \*    |       | The rectangle used to calculate the profile.   |
+-----------------+-------+-------+------------------------------------------------+

The class **Edge2dInfo** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

This class combines an edge\_2d\_list with a smoothed profile and a gradient profile. This enables you to look at the data that is used for edge detection and understand the effects of the sigma and threshold parameters.

The following operators are implemented for a edge\_2d\_info: operator == : comparison for equality. operator != : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Edge2dInfo*
^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfo()``

Standard constructor.

Constructors
~~~~~~~~~~~~

Constructor *Edge2dInfo*
^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfo(Edge2dList edges, ProfileDouble profile, ProfileDouble gradient, Rectangle rectangle)``

Standard constructor.

The constructor has the following parameters:

+-----------------+---------------------+------------------------------------------------+
| Parameter       | Type                | Description                                    |
+=================+=====================+================================================+
| ``edges``       | ``Edge2dList``      | The edges.                                     |
+-----------------+---------------------+------------------------------------------------+
| ``profile``     | ``ProfileDouble``   | The smothed grey value profile.                |
+-----------------+---------------------+------------------------------------------------+
| ``gradient``    | ``ProfileDouble``   | The gradient profile.                          |
+-----------------+---------------------+------------------------------------------------+
| ``rectangle``   | ``Rectangle``       | The rectangle used to calculate the profile.   |
+-----------------+---------------------+------------------------------------------------+

Properties
~~~~~~~~~~

Property *Edges*
^^^^^^^^^^^^^^^^

``Edge2dList Edges``

The edges.

Property *Profile*
^^^^^^^^^^^^^^^^^^

``ProfileDouble Profile``

The smoothed grey value profile.

Property *Gradient*
^^^^^^^^^^^^^^^^^^^

``ProfileDouble Gradient``

The gradient profile.

Property *Rectangle*
^^^^^^^^^^^^^^^^^^^^

``Rectangle Rectangle``

The rectangle used to calculate the profile.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
