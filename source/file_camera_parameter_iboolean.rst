Class *CameraParameterIboolean*
-------------------------------

**Namespace:** Ngi

**Module:** Camera

The class **CameraParameterIboolean** contains the following properties:

+-------------+-------+-------+---------------+
| Property    | Get   | Set   | Description   |
+=============+=======+=======+===============+
| ``Value``   | \*    | \*    |               |
+-------------+-------+-------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Value*
^^^^^^^^^^^^^^^^

``System.Boolean Value``
