Class *WidgetPoint*
-------------------

A widget\_point displays a point on a surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetPoint** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetPoint** contains the following properties:

+----------------+-------+-------+-------------------------------+
| Property       | Get   | Set   | Description                   |
+================+=======+=======+===============================+
| ``Position``   | \*    | \*    | The position of the widget.   |
+----------------+-------+-------+-------------------------------+
| ``Pen``        | \*    | \*    | The pen of the widget.        |
+----------------+-------+-------+-------------------------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_point. These settings affect the look and the behavior of the widget\_point.

The widget\_point supports interactivity via the widget\_interactive base class, i.e. it can be selected, move around. In general it can react on mouse and keyboard interactions. A widget\_point has interactive widget capabilities.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetPoint*
^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetPoint()``

Default constructor.

The default constructor creates a widget with default settings. By default the position is at zero and the outline is one-pixel wide black.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``PointDouble Position``

The position of the widget.

Property *Pen*
^^^^^^^^^^^^^^

``PenByte Pen``

The pen of the widget.
