Class *RotationTransformation*
------------------------------

Rotation transformation of geometric primitives.

**Namespace:** Ngi

**Module:** ImageProcessing

Description
~~~~~~~~~~~

This class provides static methods to transform geometric primitives via a rotation transformation.

The class can transform these geometric primitives: point, vector, direction, line\_segment, ray, line, box, rectangle, circle, ellipse, arc elliptical\_arc, triangle, quadrilateral, polyline, quadratic\_bezier and geometry.

All primitives retain their nature, when transformed by a rotation transform.

Some primitives can have an integer coordinate type on input, but the resulting types are always of floating point coordinate type. This is because the transformation cannot retain the integer nature in general. To illustrate, a point<int> type is transformed and the result is a point<double> type.

Static Methods
~~~~~~~~~~~~~~

Method *Transform*
^^^^^^^^^^^^^^^^^^

``VectorDouble Transform(VectorDouble vector, RotationMatrix rotation)``

Transform a vector.

The method **Transform** has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``vector``     | ``VectorDouble``     |               |
+----------------+----------------------+---------------+
| ``rotation``   | ``RotationMatrix``   |               |
+----------------+----------------------+---------------+

A vector transformed with a rotation transform results in another vector.

The rotation transformation is carried out with a matrix multiplication.

Returns the transformed vector.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``PointDouble Transform(PointDouble point, RotationMatrix rotation)``

Transform a point.

The method **Transform** has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``point``      | ``PointDouble``      |               |
+----------------+----------------------+---------------+
| ``rotation``   | ``RotationMatrix``   |               |
+----------------+----------------------+---------------+

A point transformed with a rotation transform results in another point.

The rotation transformation is carried out with a matrix multiplication.

Returns the transformed vector.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Direction Transform(Direction direction, RotationMatrix rotation)``

Transform a direction.

The method **Transform** has the following parameters:

+-----------------+----------------------+---------------+
| Parameter       | Type                 | Description   |
+=================+======================+===============+
| ``direction``   | ``Direction``        |               |
+-----------------+----------------------+---------------+
| ``rotation``    | ``RotationMatrix``   |               |
+-----------------+----------------------+---------------+

A direction transformed with a rotation transform results in another direction.

The rotation transformation is carried out with a matrix multiplication.

Returns the transformed direction.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``LineSegmentDouble Transform(LineSegmentDouble lineSegment, RotationMatrix rotation)``

A line segment transformed with a rotation transform results in another line segment.

The method **Transform** has the following parameters:

+-------------------+-------------------------+---------------+
| Parameter         | Type                    | Description   |
+===================+=========================+===============+
| ``lineSegment``   | ``LineSegmentDouble``   |               |
+-------------------+-------------------------+---------------+
| ``rotation``      | ``RotationMatrix``      |               |
+-------------------+-------------------------+---------------+

The rotation transformation is carried out with a matrix multiplication.

Returns the transformed line segment.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``RayDouble Transform(RayDouble ray, RotationMatrix rotation)``

Transform a ray.

The method **Transform** has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``ray``        | ``RayDouble``        |               |
+----------------+----------------------+---------------+
| ``rotation``   | ``RotationMatrix``   |               |
+----------------+----------------------+---------------+

A ray transformed with a rotation transform results in another ray.

The rotation transformation is carried out with a matrix multiplication.

Returns the transformed ray.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``LineDouble Transform(LineDouble line, RotationMatrix rotation)``

Transform a line.

The method **Transform** has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``line``       | ``LineDouble``       |               |
+----------------+----------------------+---------------+
| ``rotation``   | ``RotationMatrix``   |               |
+----------------+----------------------+---------------+

A line transformed with a rotation transform results in another line.

The rotation transformation is carried out with a matrix multiplication.

Returns the transformed line.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Rectangle Transform(BoxDouble box, RotationMatrix rotation)``

Transform a box.

The method **Transform** has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``box``        | ``BoxDouble``        |               |
+----------------+----------------------+---------------+
| ``rotation``   | ``RotationMatrix``   |               |
+----------------+----------------------+---------------+

A box transformed with a rotation transform results i a rectangle.

The rotation transformation is carried out with a matrix multiplication.

Returns the transformed box in the form of a rectangle.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Rectangle Transform(Rectangle box, RotationMatrix rotation)``

Transform a rectangle.

The method **Transform** has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``box``        | ``Rectangle``        |               |
+----------------+----------------------+---------------+
| ``rotation``   | ``RotationMatrix``   |               |
+----------------+----------------------+---------------+

A rectangle transformed with a rotation transform results in another rectangle.

The rotation transformation is carried out with a matrix multiplication.

Returns the transformed box in the form of a rectangle.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Circle Transform(Circle circle, RotationMatrix rotation)``

Transform a circle.

The method **Transform** has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``circle``     | ``Circle``           |               |
+----------------+----------------------+---------------+
| ``rotation``   | ``RotationMatrix``   |               |
+----------------+----------------------+---------------+

A circle transformed with a rotation transform results in another circle.

The rotation transformation is carried out with a matrix multiplication.

Returns the transformed circle in the form of an ellipse.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Ellipse Transform(Ellipse ellipse, RotationMatrix rotation)``

Transform an ellipse.

The method **Transform** has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``ellipse``    | ``Ellipse``          |               |
+----------------+----------------------+---------------+
| ``rotation``   | ``RotationMatrix``   |               |
+----------------+----------------------+---------------+

An ellipse transformed with a rotation transform results in another ellipse.

The rotation transformation is carried out with a matrix multiplication.

Returns the transformed ellipse.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Arc Transform(Arc arc, RotationMatrix rotation)``

Transform an arc.

The method **Transform** has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``arc``        | ``Arc``              |               |
+----------------+----------------------+---------------+
| ``rotation``   | ``RotationMatrix``   |               |
+----------------+----------------------+---------------+

An arc transformed with a rotation transform results in another arc.

The rotation transformation is carried out with a matrix multiplication.

Returns the transformed arc.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``EllipticalArc Transform(EllipticalArc ellipticalArc, RotationMatrix rotation)``

Transform an elliptical arc.

The method **Transform** has the following parameters:

+---------------------+----------------------+---------------+
| Parameter           | Type                 | Description   |
+=====================+======================+===============+
| ``ellipticalArc``   | ``EllipticalArc``    |               |
+---------------------+----------------------+---------------+
| ``rotation``        | ``RotationMatrix``   |               |
+---------------------+----------------------+---------------+

An elliptical arc transformed with a rotation transform results in another elliptical arc

The rotation transformation is carried out with a matrix multiplication.

Returns the transformed elliptical arc.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``TriangleDouble Transform(TriangleDouble triangle, RotationMatrix rotation)``

Transform a triangle.

The method **Transform** has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``triangle``   | ``TriangleDouble``   |               |
+----------------+----------------------+---------------+
| ``rotation``   | ``RotationMatrix``   |               |
+----------------+----------------------+---------------+

A triangle transformed with a rotation transform results in another triangle.

The rotation transformation is carried out with a matrix multiplication.

Returns the transformed triangle.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``QuadrilateralDouble Transform(QuadrilateralDouble quadrilateral, RotationMatrix rotation)``

Transform a quadrilateral.

The method **Transform** has the following parameters:

+---------------------+---------------------------+---------------+
| Parameter           | Type                      | Description   |
+=====================+===========================+===============+
| ``quadrilateral``   | ``QuadrilateralDouble``   |               |
+---------------------+---------------------------+---------------+
| ``rotation``        | ``RotationMatrix``        |               |
+---------------------+---------------------------+---------------+

A quadrilateral transformed with a rotation transform results in another quadrilateral.

The rotation transformation is carried out with a matrix multiplication.

Returns the transformed quadrilateral.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``PolylineDouble Transform(PolylineDouble polyline, RotationMatrix rotation)``

Transform a polyline.

The method **Transform** has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``polyline``   | ``PolylineDouble``   |               |
+----------------+----------------------+---------------+
| ``rotation``   | ``RotationMatrix``   |               |
+----------------+----------------------+---------------+

A polyline transformed with a rotation transform results in another polyline.

The rotation transformation is carried out with a matrix multiplication.

Returns the transformed polyline.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``QuadraticBezier Transform(QuadraticBezier bezier, RotationMatrix rotation)``

Transform a quadratic bezier curve.

The method **Transform** has the following parameters:

+----------------+-----------------------+---------------+
| Parameter      | Type                  | Description   |
+================+=======================+===============+
| ``bezier``     | ``QuadraticBezier``   |               |
+----------------+-----------------------+---------------+
| ``rotation``   | ``RotationMatrix``    |               |
+----------------+-----------------------+---------------+

A quadratic bezier curve transformed with a rotation transform results in another quadratic bezier curve.

The rotation transformation is carried out with a matrix multiplication.

Returns the transformed quadratic bezier curve.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``CubicBezier Transform(CubicBezier bezier, RotationMatrix rotation)``

Transform a cubic bezier curve.

The method **Transform** has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``bezier``     | ``CubicBezier``      |               |
+----------------+----------------------+---------------+
| ``rotation``   | ``RotationMatrix``   |               |
+----------------+----------------------+---------------+

A cubic bezier curve transformed with a rotation transform results in another cubic bezier curve.

The rotation transformation is carried out with a matrix multiplication.

Returns the transformed cubic bezier curve.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Geometry Transform(Geometry geometry, RotationMatrix rotation)``

Transform a geometry.

The method **Transform** has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``geometry``   | ``Geometry``         |               |
+----------------+----------------------+---------------+
| ``rotation``   | ``RotationMatrix``   |               |
+----------------+----------------------+---------------+

A geometry transformed with a rotation transform results in another geometry.

The rotation transformation is carried out with a matrix multiplication.

Returns the transformed geometry.
