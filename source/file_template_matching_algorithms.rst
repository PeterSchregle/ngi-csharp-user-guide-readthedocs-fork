Class *TemplateMatchingAlgorithms*
----------------------------------

Wraps free-standing functions for template matching.

**Namespace:** Ngi

**Module:** TemplateMatching

The class **TemplateMatchingAlgorithms** contains the following enumerations:

+------------------------+------------------------------+
| Enumeration            | Description                  |
+========================+==============================+
| ``SimilarityMetric``   | TODO documentation missing   |
+------------------------+------------------------------+

Description
~~~~~~~~~~~

The purpose of this class is to make the functions available for the .NET wrapper.

Static Methods
~~~~~~~~~~~~~~

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(ViewLocatorByte source, ViewLocatorByte model, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity metric.

The method **TemplateMatching** has the following parameters:

+--------------+---------------------------------------------------+---------------+
| Parameter    | Type                                              | Description   |
+==============+===================================================+===============+
| ``source``   | ``ViewLocatorByte``                               |               |
+--------------+---------------------------------------------------+---------------+
| ``model``    | ``ViewLocatorByte``                               |               |
+--------------+---------------------------------------------------+---------------+
| ``metric``   | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+--------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(ViewLocatorUInt16 source, ViewLocatorUInt16 model, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity metric.

The method **TemplateMatching** has the following parameters:

+--------------+---------------------------------------------------+---------------+
| Parameter    | Type                                              | Description   |
+==============+===================================================+===============+
| ``source``   | ``ViewLocatorUInt16``                             |               |
+--------------+---------------------------------------------------+---------------+
| ``model``    | ``ViewLocatorUInt16``                             |               |
+--------------+---------------------------------------------------+---------------+
| ``metric``   | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+--------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(ViewLocatorUInt32 source, ViewLocatorUInt32 model, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity metric.

The method **TemplateMatching** has the following parameters:

+--------------+---------------------------------------------------+---------------+
| Parameter    | Type                                              | Description   |
+==============+===================================================+===============+
| ``source``   | ``ViewLocatorUInt32``                             |               |
+--------------+---------------------------------------------------+---------------+
| ``model``    | ``ViewLocatorUInt32``                             |               |
+--------------+---------------------------------------------------+---------------+
| ``metric``   | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+--------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(ViewLocatorDouble source, ViewLocatorDouble model, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity metric.

The method **TemplateMatching** has the following parameters:

+--------------+---------------------------------------------------+---------------+
| Parameter    | Type                                              | Description   |
+==============+===================================================+===============+
| ``source``   | ``ViewLocatorDouble``                             |               |
+--------------+---------------------------------------------------+---------------+
| ``model``    | ``ViewLocatorDouble``                             |               |
+--------------+---------------------------------------------------+---------------+
| ``metric``   | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+--------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(ViewLocatorRgbByte source, ViewLocatorRgbByte model, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity metric.

The method **TemplateMatching** has the following parameters:

+--------------+---------------------------------------------------+---------------+
| Parameter    | Type                                              | Description   |
+==============+===================================================+===============+
| ``source``   | ``ViewLocatorRgbByte``                            |               |
+--------------+---------------------------------------------------+---------------+
| ``model``    | ``ViewLocatorRgbByte``                            |               |
+--------------+---------------------------------------------------+---------------+
| ``metric``   | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+--------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(ViewLocatorRgbUInt16 source, ViewLocatorRgbUInt16 model, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity metric.

The method **TemplateMatching** has the following parameters:

+--------------+---------------------------------------------------+---------------+
| Parameter    | Type                                              | Description   |
+==============+===================================================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``                          |               |
+--------------+---------------------------------------------------+---------------+
| ``model``    | ``ViewLocatorRgbUInt16``                          |               |
+--------------+---------------------------------------------------+---------------+
| ``metric``   | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+--------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(ViewLocatorRgbUInt32 source, ViewLocatorRgbUInt32 model, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity metric.

The method **TemplateMatching** has the following parameters:

+--------------+---------------------------------------------------+---------------+
| Parameter    | Type                                              | Description   |
+==============+===================================================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``                          |               |
+--------------+---------------------------------------------------+---------------+
| ``model``    | ``ViewLocatorRgbUInt32``                          |               |
+--------------+---------------------------------------------------+---------------+
| ``metric``   | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+--------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(ViewLocatorRgbDouble source, ViewLocatorRgbDouble model, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity metric.

The method **TemplateMatching** has the following parameters:

+--------------+---------------------------------------------------+---------------+
| Parameter    | Type                                              | Description   |
+==============+===================================================+===============+
| ``source``   | ``ViewLocatorRgbDouble``                          |               |
+--------------+---------------------------------------------------+---------------+
| ``model``    | ``ViewLocatorRgbDouble``                          |               |
+--------------+---------------------------------------------------+---------------+
| ``metric``   | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+--------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(ViewLocatorRgbaByte source, ViewLocatorRgbaByte model, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity metric.

The method **TemplateMatching** has the following parameters:

+--------------+---------------------------------------------------+---------------+
| Parameter    | Type                                              | Description   |
+==============+===================================================+===============+
| ``source``   | ``ViewLocatorRgbaByte``                           |               |
+--------------+---------------------------------------------------+---------------+
| ``model``    | ``ViewLocatorRgbaByte``                           |               |
+--------------+---------------------------------------------------+---------------+
| ``metric``   | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+--------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(ViewLocatorRgbaUInt16 source, ViewLocatorRgbaUInt16 model, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity metric.

The method **TemplateMatching** has the following parameters:

+--------------+---------------------------------------------------+---------------+
| Parameter    | Type                                              | Description   |
+==============+===================================================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``                         |               |
+--------------+---------------------------------------------------+---------------+
| ``model``    | ``ViewLocatorRgbaUInt16``                         |               |
+--------------+---------------------------------------------------+---------------+
| ``metric``   | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+--------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(ViewLocatorRgbaUInt32 source, ViewLocatorRgbaUInt32 model, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity metric.

The method **TemplateMatching** has the following parameters:

+--------------+---------------------------------------------------+---------------+
| Parameter    | Type                                              | Description   |
+==============+===================================================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``                         |               |
+--------------+---------------------------------------------------+---------------+
| ``model``    | ``ViewLocatorRgbaUInt32``                         |               |
+--------------+---------------------------------------------------+---------------+
| ``metric``   | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+--------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(ViewLocatorRgbaDouble source, ViewLocatorRgbaDouble model, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity metric.

The method **TemplateMatching** has the following parameters:

+--------------+---------------------------------------------------+---------------+
| Parameter    | Type                                              | Description   |
+==============+===================================================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``                         |               |
+--------------+---------------------------------------------------+---------------+
| ``model``    | ``ViewLocatorRgbaDouble``                         |               |
+--------------+---------------------------------------------------+---------------+
| ``metric``   | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+--------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(View source, View model, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity metric.

The method **TemplateMatching** has the following parameters:

+--------------+---------------------------------------------------+---------------+
| Parameter    | Type                                              | Description   |
+==============+===================================================+===============+
| ``source``   | ``View``                                          |               |
+--------------+---------------------------------------------------+---------------+
| ``model``    | ``View``                                          |               |
+--------------+---------------------------------------------------+---------------+
| ``metric``   | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+--------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(ViewLocatorByte source, ViewLocatorByte model, Region modelMask, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity measure.

The method **TemplateMatching** has the following parameters:

+-----------------+---------------------------------------------------+---------------+
| Parameter       | Type                                              | Description   |
+=================+===================================================+===============+
| ``source``      | ``ViewLocatorByte``                               |               |
+-----------------+---------------------------------------------------+---------------+
| ``model``       | ``ViewLocatorByte``                               |               |
+-----------------+---------------------------------------------------+---------------+
| ``modelMask``   | ``Region``                                        |               |
+-----------------+---------------------------------------------------+---------------+
| ``metric``      | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(ViewLocatorUInt16 source, ViewLocatorUInt16 model, Region modelMask, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity measure.

The method **TemplateMatching** has the following parameters:

+-----------------+---------------------------------------------------+---------------+
| Parameter       | Type                                              | Description   |
+=================+===================================================+===============+
| ``source``      | ``ViewLocatorUInt16``                             |               |
+-----------------+---------------------------------------------------+---------------+
| ``model``       | ``ViewLocatorUInt16``                             |               |
+-----------------+---------------------------------------------------+---------------+
| ``modelMask``   | ``Region``                                        |               |
+-----------------+---------------------------------------------------+---------------+
| ``metric``      | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(ViewLocatorUInt32 source, ViewLocatorUInt32 model, Region modelMask, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity measure.

The method **TemplateMatching** has the following parameters:

+-----------------+---------------------------------------------------+---------------+
| Parameter       | Type                                              | Description   |
+=================+===================================================+===============+
| ``source``      | ``ViewLocatorUInt32``                             |               |
+-----------------+---------------------------------------------------+---------------+
| ``model``       | ``ViewLocatorUInt32``                             |               |
+-----------------+---------------------------------------------------+---------------+
| ``modelMask``   | ``Region``                                        |               |
+-----------------+---------------------------------------------------+---------------+
| ``metric``      | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(ViewLocatorDouble source, ViewLocatorDouble model, Region modelMask, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity measure.

The method **TemplateMatching** has the following parameters:

+-----------------+---------------------------------------------------+---------------+
| Parameter       | Type                                              | Description   |
+=================+===================================================+===============+
| ``source``      | ``ViewLocatorDouble``                             |               |
+-----------------+---------------------------------------------------+---------------+
| ``model``       | ``ViewLocatorDouble``                             |               |
+-----------------+---------------------------------------------------+---------------+
| ``modelMask``   | ``Region``                                        |               |
+-----------------+---------------------------------------------------+---------------+
| ``metric``      | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(ViewLocatorRgbByte source, ViewLocatorRgbByte model, Region modelMask, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity measure.

The method **TemplateMatching** has the following parameters:

+-----------------+---------------------------------------------------+---------------+
| Parameter       | Type                                              | Description   |
+=================+===================================================+===============+
| ``source``      | ``ViewLocatorRgbByte``                            |               |
+-----------------+---------------------------------------------------+---------------+
| ``model``       | ``ViewLocatorRgbByte``                            |               |
+-----------------+---------------------------------------------------+---------------+
| ``modelMask``   | ``Region``                                        |               |
+-----------------+---------------------------------------------------+---------------+
| ``metric``      | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(ViewLocatorRgbUInt16 source, ViewLocatorRgbUInt16 model, Region modelMask, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity measure.

The method **TemplateMatching** has the following parameters:

+-----------------+---------------------------------------------------+---------------+
| Parameter       | Type                                              | Description   |
+=================+===================================================+===============+
| ``source``      | ``ViewLocatorRgbUInt16``                          |               |
+-----------------+---------------------------------------------------+---------------+
| ``model``       | ``ViewLocatorRgbUInt16``                          |               |
+-----------------+---------------------------------------------------+---------------+
| ``modelMask``   | ``Region``                                        |               |
+-----------------+---------------------------------------------------+---------------+
| ``metric``      | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(ViewLocatorRgbUInt32 source, ViewLocatorRgbUInt32 model, Region modelMask, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity measure.

The method **TemplateMatching** has the following parameters:

+-----------------+---------------------------------------------------+---------------+
| Parameter       | Type                                              | Description   |
+=================+===================================================+===============+
| ``source``      | ``ViewLocatorRgbUInt32``                          |               |
+-----------------+---------------------------------------------------+---------------+
| ``model``       | ``ViewLocatorRgbUInt32``                          |               |
+-----------------+---------------------------------------------------+---------------+
| ``modelMask``   | ``Region``                                        |               |
+-----------------+---------------------------------------------------+---------------+
| ``metric``      | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(ViewLocatorRgbDouble source, ViewLocatorRgbDouble model, Region modelMask, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity measure.

The method **TemplateMatching** has the following parameters:

+-----------------+---------------------------------------------------+---------------+
| Parameter       | Type                                              | Description   |
+=================+===================================================+===============+
| ``source``      | ``ViewLocatorRgbDouble``                          |               |
+-----------------+---------------------------------------------------+---------------+
| ``model``       | ``ViewLocatorRgbDouble``                          |               |
+-----------------+---------------------------------------------------+---------------+
| ``modelMask``   | ``Region``                                        |               |
+-----------------+---------------------------------------------------+---------------+
| ``metric``      | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(ViewLocatorRgbaByte source, ViewLocatorRgbaByte model, Region modelMask, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity measure.

The method **TemplateMatching** has the following parameters:

+-----------------+---------------------------------------------------+---------------+
| Parameter       | Type                                              | Description   |
+=================+===================================================+===============+
| ``source``      | ``ViewLocatorRgbaByte``                           |               |
+-----------------+---------------------------------------------------+---------------+
| ``model``       | ``ViewLocatorRgbaByte``                           |               |
+-----------------+---------------------------------------------------+---------------+
| ``modelMask``   | ``Region``                                        |               |
+-----------------+---------------------------------------------------+---------------+
| ``metric``      | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(ViewLocatorRgbaUInt16 source, ViewLocatorRgbaUInt16 model, Region modelMask, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity measure.

The method **TemplateMatching** has the following parameters:

+-----------------+---------------------------------------------------+---------------+
| Parameter       | Type                                              | Description   |
+=================+===================================================+===============+
| ``source``      | ``ViewLocatorRgbaUInt16``                         |               |
+-----------------+---------------------------------------------------+---------------+
| ``model``       | ``ViewLocatorRgbaUInt16``                         |               |
+-----------------+---------------------------------------------------+---------------+
| ``modelMask``   | ``Region``                                        |               |
+-----------------+---------------------------------------------------+---------------+
| ``metric``      | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(ViewLocatorRgbaUInt32 source, ViewLocatorRgbaUInt32 model, Region modelMask, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity measure.

The method **TemplateMatching** has the following parameters:

+-----------------+---------------------------------------------------+---------------+
| Parameter       | Type                                              | Description   |
+=================+===================================================+===============+
| ``source``      | ``ViewLocatorRgbaUInt32``                         |               |
+-----------------+---------------------------------------------------+---------------+
| ``model``       | ``ViewLocatorRgbaUInt32``                         |               |
+-----------------+---------------------------------------------------+---------------+
| ``modelMask``   | ``Region``                                        |               |
+-----------------+---------------------------------------------------+---------------+
| ``metric``      | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(ViewLocatorRgbaDouble source, ViewLocatorRgbaDouble model, Region modelMask, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity measure.

The method **TemplateMatching** has the following parameters:

+-----------------+---------------------------------------------------+---------------+
| Parameter       | Type                                              | Description   |
+=================+===================================================+===============+
| ``source``      | ``ViewLocatorRgbaDouble``                         |               |
+-----------------+---------------------------------------------------+---------------+
| ``model``       | ``ViewLocatorRgbaDouble``                         |               |
+-----------------+---------------------------------------------------+---------------+
| ``modelMask``   | ``Region``                                        |               |
+-----------------+---------------------------------------------------+---------------+
| ``metric``      | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *TemplateMatching*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte TemplateMatching(View source, View model, Region modelMask, TemplateMatchingAlgorithms.SimilarityMetric metric)``

Computes the template matching score using a given similarity measure.

The method **TemplateMatching** has the following parameters:

+-----------------+---------------------------------------------------+---------------+
| Parameter       | Type                                              | Description   |
+=================+===================================================+===============+
| ``source``      | ``View``                                          |               |
+-----------------+---------------------------------------------------+---------------+
| ``model``       | ``View``                                          |               |
+-----------------+---------------------------------------------------+---------------+
| ``modelMask``   | ``Region``                                        |               |
+-----------------+---------------------------------------------------+---------------+
| ``metric``      | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------+---------------------------------------------------+---------------+

A template defined by a view and a region is compared with a source view at each pixel location to produce a matching score in a destination view.

The similarity metric can be defined as one of the members of the enumeration similarity\_metric.

If the template has even width or height, it is zero-padded to uneven dimensions and centered.

The source and destination views must have the same size.

Returns a mono image showing the match score.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModelByte CreateTemplateSearchModel(ViewLocatorByte model, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+-----------------------+---------------+
| Parameter              | Type                  | Description   |
+========================+=======================+===============+
| ``model``              | ``ViewLocatorByte``   |               |
+------------------------+-----------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``      |               |
+------------------------+-----------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModelUInt16 CreateTemplateSearchModel(ViewLocatorUInt16 model, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+-------------------------+---------------+
| Parameter              | Type                    | Description   |
+========================+=========================+===============+
| ``model``              | ``ViewLocatorUInt16``   |               |
+------------------------+-------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``        |               |
+------------------------+-------------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModelUInt32 CreateTemplateSearchModel(ViewLocatorUInt32 model, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+-------------------------+---------------+
| Parameter              | Type                    | Description   |
+========================+=========================+===============+
| ``model``              | ``ViewLocatorUInt32``   |               |
+------------------------+-------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``        |               |
+------------------------+-------------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModelDouble CreateTemplateSearchModel(ViewLocatorDouble model, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+-------------------------+---------------+
| Parameter              | Type                    | Description   |
+========================+=========================+===============+
| ``model``              | ``ViewLocatorDouble``   |               |
+------------------------+-------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``        |               |
+------------------------+-------------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModelRgbByte CreateTemplateSearchModel(ViewLocatorRgbByte model, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+--------------------------+---------------+
| Parameter              | Type                     | Description   |
+========================+==========================+===============+
| ``model``              | ``ViewLocatorRgbByte``   |               |
+------------------------+--------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``         |               |
+------------------------+--------------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModelRgbUInt16 CreateTemplateSearchModel(ViewLocatorRgbUInt16 model, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+----------------------------+---------------+
| Parameter              | Type                       | Description   |
+========================+============================+===============+
| ``model``              | ``ViewLocatorRgbUInt16``   |               |
+------------------------+----------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``           |               |
+------------------------+----------------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModelRgbUInt32 CreateTemplateSearchModel(ViewLocatorRgbUInt32 model, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+----------------------------+---------------+
| Parameter              | Type                       | Description   |
+========================+============================+===============+
| ``model``              | ``ViewLocatorRgbUInt32``   |               |
+------------------------+----------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``           |               |
+------------------------+----------------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModelRgbDouble CreateTemplateSearchModel(ViewLocatorRgbDouble model, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+----------------------------+---------------+
| Parameter              | Type                       | Description   |
+========================+============================+===============+
| ``model``              | ``ViewLocatorRgbDouble``   |               |
+------------------------+----------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``           |               |
+------------------------+----------------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModelRgbaByte CreateTemplateSearchModel(ViewLocatorRgbaByte model, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+---------------------------+---------------+
| Parameter              | Type                      | Description   |
+========================+===========================+===============+
| ``model``              | ``ViewLocatorRgbaByte``   |               |
+------------------------+---------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``          |               |
+------------------------+---------------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModelRgbaUInt16 CreateTemplateSearchModel(ViewLocatorRgbaUInt16 model, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+-----------------------------+---------------+
| Parameter              | Type                        | Description   |
+========================+=============================+===============+
| ``model``              | ``ViewLocatorRgbaUInt16``   |               |
+------------------------+-----------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``            |               |
+------------------------+-----------------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModelRgbaUInt32 CreateTemplateSearchModel(ViewLocatorRgbaUInt32 model, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+-----------------------------+---------------+
| Parameter              | Type                        | Description   |
+========================+=============================+===============+
| ``model``              | ``ViewLocatorRgbaUInt32``   |               |
+------------------------+-----------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``            |               |
+------------------------+-----------------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModelRgbaDouble CreateTemplateSearchModel(ViewLocatorRgbaDouble model, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+-----------------------------+---------------+
| Parameter              | Type                        | Description   |
+========================+=============================+===============+
| ``model``              | ``ViewLocatorRgbaDouble``   |               |
+------------------------+-----------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``            |               |
+------------------------+-----------------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModel CreateTemplateSearchModel(View model, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+--------------------+---------------+
| Parameter              | Type               | Description   |
+========================+====================+===============+
| ``model``              | ``View``           |               |
+------------------------+--------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``   |               |
+------------------------+--------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModelByte CreateTemplateSearchModel(ViewLocatorByte model, Region modelMask, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+-----------------------+---------------+
| Parameter              | Type                  | Description   |
+========================+=======================+===============+
| ``model``              | ``ViewLocatorByte``   |               |
+------------------------+-----------------------+---------------+
| ``modelMask``          | ``Region``            |               |
+------------------------+-----------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``      |               |
+------------------------+-----------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModelUInt16 CreateTemplateSearchModel(ViewLocatorUInt16 model, Region modelMask, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+-------------------------+---------------+
| Parameter              | Type                    | Description   |
+========================+=========================+===============+
| ``model``              | ``ViewLocatorUInt16``   |               |
+------------------------+-------------------------+---------------+
| ``modelMask``          | ``Region``              |               |
+------------------------+-------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``        |               |
+------------------------+-------------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModelUInt32 CreateTemplateSearchModel(ViewLocatorUInt32 model, Region modelMask, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+-------------------------+---------------+
| Parameter              | Type                    | Description   |
+========================+=========================+===============+
| ``model``              | ``ViewLocatorUInt32``   |               |
+------------------------+-------------------------+---------------+
| ``modelMask``          | ``Region``              |               |
+------------------------+-------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``        |               |
+------------------------+-------------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModelDouble CreateTemplateSearchModel(ViewLocatorDouble model, Region modelMask, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+-------------------------+---------------+
| Parameter              | Type                    | Description   |
+========================+=========================+===============+
| ``model``              | ``ViewLocatorDouble``   |               |
+------------------------+-------------------------+---------------+
| ``modelMask``          | ``Region``              |               |
+------------------------+-------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``        |               |
+------------------------+-------------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModelRgbByte CreateTemplateSearchModel(ViewLocatorRgbByte model, Region modelMask, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+--------------------------+---------------+
| Parameter              | Type                     | Description   |
+========================+==========================+===============+
| ``model``              | ``ViewLocatorRgbByte``   |               |
+------------------------+--------------------------+---------------+
| ``modelMask``          | ``Region``               |               |
+------------------------+--------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``         |               |
+------------------------+--------------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModelRgbUInt16 CreateTemplateSearchModel(ViewLocatorRgbUInt16 model, Region modelMask, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+----------------------------+---------------+
| Parameter              | Type                       | Description   |
+========================+============================+===============+
| ``model``              | ``ViewLocatorRgbUInt16``   |               |
+------------------------+----------------------------+---------------+
| ``modelMask``          | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``           |               |
+------------------------+----------------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModelRgbUInt32 CreateTemplateSearchModel(ViewLocatorRgbUInt32 model, Region modelMask, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+----------------------------+---------------+
| Parameter              | Type                       | Description   |
+========================+============================+===============+
| ``model``              | ``ViewLocatorRgbUInt32``   |               |
+------------------------+----------------------------+---------------+
| ``modelMask``          | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``           |               |
+------------------------+----------------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModelRgbDouble CreateTemplateSearchModel(ViewLocatorRgbDouble model, Region modelMask, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+----------------------------+---------------+
| Parameter              | Type                       | Description   |
+========================+============================+===============+
| ``model``              | ``ViewLocatorRgbDouble``   |               |
+------------------------+----------------------------+---------------+
| ``modelMask``          | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``           |               |
+------------------------+----------------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModelRgbaByte CreateTemplateSearchModel(ViewLocatorRgbaByte model, Region modelMask, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+---------------------------+---------------+
| Parameter              | Type                      | Description   |
+========================+===========================+===============+
| ``model``              | ``ViewLocatorRgbaByte``   |               |
+------------------------+---------------------------+---------------+
| ``modelMask``          | ``Region``                |               |
+------------------------+---------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``          |               |
+------------------------+---------------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModelRgbaUInt16 CreateTemplateSearchModel(ViewLocatorRgbaUInt16 model, Region modelMask, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+-----------------------------+---------------+
| Parameter              | Type                        | Description   |
+========================+=============================+===============+
| ``model``              | ``ViewLocatorRgbaUInt16``   |               |
+------------------------+-----------------------------+---------------+
| ``modelMask``          | ``Region``                  |               |
+------------------------+-----------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``            |               |
+------------------------+-----------------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModelRgbaUInt32 CreateTemplateSearchModel(ViewLocatorRgbaUInt32 model, Region modelMask, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+-----------------------------+---------------+
| Parameter              | Type                        | Description   |
+========================+=============================+===============+
| ``model``              | ``ViewLocatorRgbaUInt32``   |               |
+------------------------+-----------------------------+---------------+
| ``modelMask``          | ``Region``                  |               |
+------------------------+-----------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``            |               |
+------------------------+-----------------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModelRgbaDouble CreateTemplateSearchModel(ViewLocatorRgbaDouble model, Region modelMask, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+-----------------------------+---------------+
| Parameter              | Type                        | Description   |
+========================+=============================+===============+
| ``model``              | ``ViewLocatorRgbaDouble``   |               |
+------------------------+-----------------------------+---------------+
| ``modelMask``          | ``Region``                  |               |
+------------------------+-----------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``            |               |
+------------------------+-----------------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *CreateTemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModel CreateTemplateSearchModel(View model, Region modelMask, System.Int32 minimalModelSize)``

Creates a model for subsequent template searching.

The method **CreateTemplateSearchModel** has the following parameters:

+------------------------+--------------------+---------------+
| Parameter              | Type               | Description   |
+========================+====================+===============+
| ``model``              | ``View``           |               |
+------------------------+--------------------+---------------+
| ``modelMask``          | ``Region``         |               |
+------------------------+--------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``   |               |
+------------------------+--------------------+---------------+

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

The actual search is performed in a pyramid approach. The number of necessary pyramid levels is computed from the given minimal size of the model.

The preprocessed model can then be used in the model\_search function.

/return A preprocessed search model.

Method *ModelSearch*
^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList ModelSearch(ViewLocatorByte source, TemplateSearchModelByte templateSearchModel, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches for template matches in a view.

The method **ModelSearch** has the following parameters:

+---------------------------+---------------------------------------------------+---------------+
| Parameter                 | Type                                              | Description   |
+===========================+===================================================+===============+
| ``source``                | ``ViewLocatorByte``                               |               |
+---------------------------+---------------------------------------------------+---------------+
| ``templateSearchModel``   | ``TemplateSearchModelByte``                       |               |
+---------------------------+---------------------------------------------------+---------------+
| ``threshold``             | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``metric``                | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+---------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``       | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``    | ``System.Int32``                                  |               |
+---------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *ModelSearch*
^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList ModelSearch(ViewLocatorUInt16 source, TemplateSearchModelUInt16 templateSearchModel, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches for template matches in a view.

The method **ModelSearch** has the following parameters:

+---------------------------+---------------------------------------------------+---------------+
| Parameter                 | Type                                              | Description   |
+===========================+===================================================+===============+
| ``source``                | ``ViewLocatorUInt16``                             |               |
+---------------------------+---------------------------------------------------+---------------+
| ``templateSearchModel``   | ``TemplateSearchModelUInt16``                     |               |
+---------------------------+---------------------------------------------------+---------------+
| ``threshold``             | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``metric``                | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+---------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``       | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``    | ``System.Int32``                                  |               |
+---------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *ModelSearch*
^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList ModelSearch(ViewLocatorUInt32 source, TemplateSearchModelUInt32 templateSearchModel, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches for template matches in a view.

The method **ModelSearch** has the following parameters:

+---------------------------+---------------------------------------------------+---------------+
| Parameter                 | Type                                              | Description   |
+===========================+===================================================+===============+
| ``source``                | ``ViewLocatorUInt32``                             |               |
+---------------------------+---------------------------------------------------+---------------+
| ``templateSearchModel``   | ``TemplateSearchModelUInt32``                     |               |
+---------------------------+---------------------------------------------------+---------------+
| ``threshold``             | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``metric``                | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+---------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``       | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``    | ``System.Int32``                                  |               |
+---------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *ModelSearch*
^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList ModelSearch(ViewLocatorDouble source, TemplateSearchModelDouble templateSearchModel, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches for template matches in a view.

The method **ModelSearch** has the following parameters:

+---------------------------+---------------------------------------------------+---------------+
| Parameter                 | Type                                              | Description   |
+===========================+===================================================+===============+
| ``source``                | ``ViewLocatorDouble``                             |               |
+---------------------------+---------------------------------------------------+---------------+
| ``templateSearchModel``   | ``TemplateSearchModelDouble``                     |               |
+---------------------------+---------------------------------------------------+---------------+
| ``threshold``             | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``metric``                | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+---------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``       | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``    | ``System.Int32``                                  |               |
+---------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *ModelSearch*
^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList ModelSearch(ViewLocatorRgbByte source, TemplateSearchModelRgbByte templateSearchModel, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches for template matches in a view.

The method **ModelSearch** has the following parameters:

+---------------------------+---------------------------------------------------+---------------+
| Parameter                 | Type                                              | Description   |
+===========================+===================================================+===============+
| ``source``                | ``ViewLocatorRgbByte``                            |               |
+---------------------------+---------------------------------------------------+---------------+
| ``templateSearchModel``   | ``TemplateSearchModelRgbByte``                    |               |
+---------------------------+---------------------------------------------------+---------------+
| ``threshold``             | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``metric``                | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+---------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``       | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``    | ``System.Int32``                                  |               |
+---------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *ModelSearch*
^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList ModelSearch(ViewLocatorRgbUInt16 source, TemplateSearchModelRgbUInt16 templateSearchModel, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches for template matches in a view.

The method **ModelSearch** has the following parameters:

+---------------------------+---------------------------------------------------+---------------+
| Parameter                 | Type                                              | Description   |
+===========================+===================================================+===============+
| ``source``                | ``ViewLocatorRgbUInt16``                          |               |
+---------------------------+---------------------------------------------------+---------------+
| ``templateSearchModel``   | ``TemplateSearchModelRgbUInt16``                  |               |
+---------------------------+---------------------------------------------------+---------------+
| ``threshold``             | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``metric``                | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+---------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``       | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``    | ``System.Int32``                                  |               |
+---------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *ModelSearch*
^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList ModelSearch(ViewLocatorRgbUInt32 source, TemplateSearchModelRgbUInt32 templateSearchModel, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches for template matches in a view.

The method **ModelSearch** has the following parameters:

+---------------------------+---------------------------------------------------+---------------+
| Parameter                 | Type                                              | Description   |
+===========================+===================================================+===============+
| ``source``                | ``ViewLocatorRgbUInt32``                          |               |
+---------------------------+---------------------------------------------------+---------------+
| ``templateSearchModel``   | ``TemplateSearchModelRgbUInt32``                  |               |
+---------------------------+---------------------------------------------------+---------------+
| ``threshold``             | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``metric``                | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+---------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``       | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``    | ``System.Int32``                                  |               |
+---------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *ModelSearch*
^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList ModelSearch(ViewLocatorRgbDouble source, TemplateSearchModelRgbDouble templateSearchModel, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches for template matches in a view.

The method **ModelSearch** has the following parameters:

+---------------------------+---------------------------------------------------+---------------+
| Parameter                 | Type                                              | Description   |
+===========================+===================================================+===============+
| ``source``                | ``ViewLocatorRgbDouble``                          |               |
+---------------------------+---------------------------------------------------+---------------+
| ``templateSearchModel``   | ``TemplateSearchModelRgbDouble``                  |               |
+---------------------------+---------------------------------------------------+---------------+
| ``threshold``             | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``metric``                | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+---------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``       | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``    | ``System.Int32``                                  |               |
+---------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *ModelSearch*
^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList ModelSearch(ViewLocatorRgbaByte source, TemplateSearchModelRgbaByte templateSearchModel, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches for template matches in a view.

The method **ModelSearch** has the following parameters:

+---------------------------+---------------------------------------------------+---------------+
| Parameter                 | Type                                              | Description   |
+===========================+===================================================+===============+
| ``source``                | ``ViewLocatorRgbaByte``                           |               |
+---------------------------+---------------------------------------------------+---------------+
| ``templateSearchModel``   | ``TemplateSearchModelRgbaByte``                   |               |
+---------------------------+---------------------------------------------------+---------------+
| ``threshold``             | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``metric``                | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+---------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``       | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``    | ``System.Int32``                                  |               |
+---------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *ModelSearch*
^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList ModelSearch(ViewLocatorRgbaUInt16 source, TemplateSearchModelRgbaUInt16 templateSearchModel, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches for template matches in a view.

The method **ModelSearch** has the following parameters:

+---------------------------+---------------------------------------------------+---------------+
| Parameter                 | Type                                              | Description   |
+===========================+===================================================+===============+
| ``source``                | ``ViewLocatorRgbaUInt16``                         |               |
+---------------------------+---------------------------------------------------+---------------+
| ``templateSearchModel``   | ``TemplateSearchModelRgbaUInt16``                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``threshold``             | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``metric``                | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+---------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``       | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``    | ``System.Int32``                                  |               |
+---------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *ModelSearch*
^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList ModelSearch(ViewLocatorRgbaUInt32 source, TemplateSearchModelRgbaUInt32 templateSearchModel, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches for template matches in a view.

The method **ModelSearch** has the following parameters:

+---------------------------+---------------------------------------------------+---------------+
| Parameter                 | Type                                              | Description   |
+===========================+===================================================+===============+
| ``source``                | ``ViewLocatorRgbaUInt32``                         |               |
+---------------------------+---------------------------------------------------+---------------+
| ``templateSearchModel``   | ``TemplateSearchModelRgbaUInt32``                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``threshold``             | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``metric``                | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+---------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``       | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``    | ``System.Int32``                                  |               |
+---------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *ModelSearch*
^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList ModelSearch(ViewLocatorRgbaDouble source, TemplateSearchModelRgbaDouble templateSearchModel, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches for template matches in a view.

The method **ModelSearch** has the following parameters:

+---------------------------+---------------------------------------------------+---------------+
| Parameter                 | Type                                              | Description   |
+===========================+===================================================+===============+
| ``source``                | ``ViewLocatorRgbaDouble``                         |               |
+---------------------------+---------------------------------------------------+---------------+
| ``templateSearchModel``   | ``TemplateSearchModelRgbaDouble``                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``threshold``             | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``metric``                | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+---------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``       | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``    | ``System.Int32``                                  |               |
+---------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *ModelSearch*
^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList ModelSearch(View source, TemplateSearchModel templateSearchModel, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches for template matches in a view.

The method **ModelSearch** has the following parameters:

+---------------------------+---------------------------------------------------+---------------+
| Parameter                 | Type                                              | Description   |
+===========================+===================================================+===============+
| ``source``                | ``View``                                          |               |
+---------------------------+---------------------------------------------------+---------------+
| ``templateSearchModel``   | ``TemplateSearchModel``                           |               |
+---------------------------+---------------------------------------------------+---------------+
| ``threshold``             | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``metric``                | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+---------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``       | ``System.Double``                                 |               |
+---------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``    | ``System.Int32``                                  |               |
+---------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(ViewLocatorByte source, ViewLocatorByte model, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

Searches for template matches in a view.

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``ViewLocatorByte``                               |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``ViewLocatorByte``                               |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(ViewLocatorUInt16 source, ViewLocatorUInt16 model, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

Searches for template matches in a view.

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``ViewLocatorUInt16``                             |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``ViewLocatorUInt16``                             |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(ViewLocatorUInt32 source, ViewLocatorUInt32 model, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

Searches for template matches in a view.

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``ViewLocatorUInt32``                             |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``ViewLocatorUInt32``                             |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(ViewLocatorDouble source, ViewLocatorDouble model, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

Searches for template matches in a view.

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``ViewLocatorDouble``                             |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``ViewLocatorDouble``                             |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(ViewLocatorRgbByte source, ViewLocatorRgbByte model, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

Searches for template matches in a view.

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``ViewLocatorRgbByte``                            |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``ViewLocatorRgbByte``                            |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(ViewLocatorRgbUInt16 source, ViewLocatorRgbUInt16 model, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

Searches for template matches in a view.

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``ViewLocatorRgbUInt16``                          |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``ViewLocatorRgbUInt16``                          |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(ViewLocatorRgbUInt32 source, ViewLocatorRgbUInt32 model, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

Searches for template matches in a view.

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``ViewLocatorRgbUInt32``                          |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``ViewLocatorRgbUInt32``                          |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(ViewLocatorRgbDouble source, ViewLocatorRgbDouble model, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

Searches for template matches in a view.

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``ViewLocatorRgbDouble``                          |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``ViewLocatorRgbDouble``                          |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(ViewLocatorRgbaByte source, ViewLocatorRgbaByte model, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

Searches for template matches in a view.

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``ViewLocatorRgbaByte``                           |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``ViewLocatorRgbaByte``                           |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(ViewLocatorRgbaUInt16 source, ViewLocatorRgbaUInt16 model, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

Searches for template matches in a view.

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``ViewLocatorRgbaUInt16``                         |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``ViewLocatorRgbaUInt16``                         |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(ViewLocatorRgbaUInt32 source, ViewLocatorRgbaUInt32 model, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

Searches for template matches in a view.

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``ViewLocatorRgbaUInt32``                         |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``ViewLocatorRgbaUInt32``                         |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(ViewLocatorRgbaDouble source, ViewLocatorRgbaDouble model, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

Searches for template matches in a view.

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``ViewLocatorRgbaDouble``                         |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``ViewLocatorRgbaDouble``                         |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(View source, View model, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

Searches for template matches in a view.

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``View``                                          |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``View``                                          |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function). While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(ViewLocatorByte source, ViewLocatorByte model, Region modelMask, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function).

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``ViewLocatorByte``                               |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``ViewLocatorByte``                               |               |
+------------------------+---------------------------------------------------+---------------+
| ``modelMask``          | ``Region``                                        |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(ViewLocatorUInt16 source, ViewLocatorUInt16 model, Region modelMask, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function).

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``ViewLocatorUInt16``                             |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``ViewLocatorUInt16``                             |               |
+------------------------+---------------------------------------------------+---------------+
| ``modelMask``          | ``Region``                                        |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(ViewLocatorUInt32 source, ViewLocatorUInt32 model, Region modelMask, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function).

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``ViewLocatorUInt32``                             |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``ViewLocatorUInt32``                             |               |
+------------------------+---------------------------------------------------+---------------+
| ``modelMask``          | ``Region``                                        |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(ViewLocatorDouble source, ViewLocatorDouble model, Region modelMask, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function).

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``ViewLocatorDouble``                             |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``ViewLocatorDouble``                             |               |
+------------------------+---------------------------------------------------+---------------+
| ``modelMask``          | ``Region``                                        |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(ViewLocatorRgbByte source, ViewLocatorRgbByte model, Region modelMask, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function).

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``ViewLocatorRgbByte``                            |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``ViewLocatorRgbByte``                            |               |
+------------------------+---------------------------------------------------+---------------+
| ``modelMask``          | ``Region``                                        |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(ViewLocatorRgbUInt16 source, ViewLocatorRgbUInt16 model, Region modelMask, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function).

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``ViewLocatorRgbUInt16``                          |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``ViewLocatorRgbUInt16``                          |               |
+------------------------+---------------------------------------------------+---------------+
| ``modelMask``          | ``Region``                                        |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(ViewLocatorRgbUInt32 source, ViewLocatorRgbUInt32 model, Region modelMask, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function).

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``ViewLocatorRgbUInt32``                          |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``ViewLocatorRgbUInt32``                          |               |
+------------------------+---------------------------------------------------+---------------+
| ``modelMask``          | ``Region``                                        |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(ViewLocatorRgbDouble source, ViewLocatorRgbDouble model, Region modelMask, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function).

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``ViewLocatorRgbDouble``                          |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``ViewLocatorRgbDouble``                          |               |
+------------------------+---------------------------------------------------+---------------+
| ``modelMask``          | ``Region``                                        |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(ViewLocatorRgbaByte source, ViewLocatorRgbaByte model, Region modelMask, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function).

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``ViewLocatorRgbaByte``                           |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``ViewLocatorRgbaByte``                           |               |
+------------------------+---------------------------------------------------+---------------+
| ``modelMask``          | ``Region``                                        |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(ViewLocatorRgbaUInt16 source, ViewLocatorRgbaUInt16 model, Region modelMask, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function).

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``ViewLocatorRgbaUInt16``                         |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``ViewLocatorRgbaUInt16``                         |               |
+------------------------+---------------------------------------------------+---------------+
| ``modelMask``          | ``Region``                                        |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(ViewLocatorRgbaUInt32 source, ViewLocatorRgbaUInt32 model, Region modelMask, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function).

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``ViewLocatorRgbaUInt32``                         |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``ViewLocatorRgbaUInt32``                         |               |
+------------------------+---------------------------------------------------+---------------+
| ``modelMask``          | ``Region``                                        |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(ViewLocatorRgbaDouble source, ViewLocatorRgbaDouble model, Region modelMask, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function).

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``ViewLocatorRgbaDouble``                         |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``ViewLocatorRgbaDouble``                         |               |
+------------------------+---------------------------------------------------+---------------+
| ``modelMask``          | ``Region``                                        |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearch*
^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearch(View source, View model, Region modelMask, System.Double threshold, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Double thresholdAdjust)``

This function combines model creation (as done with the create\_template\_search\_model function) and searching (as done with the model\_search function).

The method **TemplateSearch** has the following parameters:

+------------------------+---------------------------------------------------+---------------+
| Parameter              | Type                                              | Description   |
+========================+===================================================+===============+
| ``source``             | ``View``                                          |               |
+------------------------+---------------------------------------------------+---------------+
| ``model``              | ``View``                                          |               |
+------------------------+---------------------------------------------------+---------------+
| ``modelMask``          | ``Region``                                        |               |
+------------------------+---------------------------------------------------+---------------+
| ``threshold``          | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+
| ``metric``             | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``   | ``System.Int32``                                  |               |
+------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``    | ``System.Double``                                 |               |
+------------------------+---------------------------------------------------+---------------+

While the function is easier to call, it may be suboptimal from a perfomance point of view, if the use case is to search for the template multiple times.

/return A list of positions and match scores.

Method *TemplateSearchWithRotationOnDemand*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearchWithRotationOnDemand(ViewLocatorByte source, ViewLocatorByte model, Region modelMask, System.Double threshold, System.Double minAngle, System.Double maxAngle, System.Double angularResolution, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Int32 minimalNumberOfOrientations, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches (with rotation) for template matches in a view with a constrained search space and computes only necessary rotations on demand.

The method **TemplateSearchWithRotationOnDemand** has the following parameters:

+-----------------------------------+---------------------------------------------------+---------------+
| Parameter                         | Type                                              | Description   |
+===================================+===================================================+===============+
| ``source``                        | ``ViewLocatorByte``                               |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``model``                         | ``ViewLocatorByte``                               |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``modelMask``                     | ``Region``                                        |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``threshold``                     | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``maxAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``angularResolution``             | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``metric``                        | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``              | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalNumberOfOrientations``   | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``               | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``            | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+

A template defined by a view and a region is matched with a source view for translations and rotations constrained to certain ranges. The translation range is defined by a box and the angle range by a minimum maximum pair. The method searches for local maximum matches that are stronger than a given threshold.

The search is performed using a pyramid approach. The number pyramid levels is computed from the given minimal size of the model, the minimal number of orientations and the translation and angle ranges.

/return Returns a list of match scores, pixel positions and matching angles.

Method *TemplateSearchWithRotationOnDemand*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearchWithRotationOnDemand(ViewLocatorUInt16 source, ViewLocatorUInt16 model, Region modelMask, System.Double threshold, System.Double minAngle, System.Double maxAngle, System.Double angularResolution, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Int32 minimalNumberOfOrientations, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches (with rotation) for template matches in a view with a constrained search space and computes only necessary rotations on demand.

The method **TemplateSearchWithRotationOnDemand** has the following parameters:

+-----------------------------------+---------------------------------------------------+---------------+
| Parameter                         | Type                                              | Description   |
+===================================+===================================================+===============+
| ``source``                        | ``ViewLocatorUInt16``                             |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``model``                         | ``ViewLocatorUInt16``                             |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``modelMask``                     | ``Region``                                        |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``threshold``                     | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``maxAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``angularResolution``             | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``metric``                        | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``              | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalNumberOfOrientations``   | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``               | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``            | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+

A template defined by a view and a region is matched with a source view for translations and rotations constrained to certain ranges. The translation range is defined by a box and the angle range by a minimum maximum pair. The method searches for local maximum matches that are stronger than a given threshold.

The search is performed using a pyramid approach. The number pyramid levels is computed from the given minimal size of the model, the minimal number of orientations and the translation and angle ranges.

/return Returns a list of match scores, pixel positions and matching angles.

Method *TemplateSearchWithRotationOnDemand*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearchWithRotationOnDemand(ViewLocatorUInt32 source, ViewLocatorUInt32 model, Region modelMask, System.Double threshold, System.Double minAngle, System.Double maxAngle, System.Double angularResolution, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Int32 minimalNumberOfOrientations, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches (with rotation) for template matches in a view with a constrained search space and computes only necessary rotations on demand.

The method **TemplateSearchWithRotationOnDemand** has the following parameters:

+-----------------------------------+---------------------------------------------------+---------------+
| Parameter                         | Type                                              | Description   |
+===================================+===================================================+===============+
| ``source``                        | ``ViewLocatorUInt32``                             |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``model``                         | ``ViewLocatorUInt32``                             |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``modelMask``                     | ``Region``                                        |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``threshold``                     | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``maxAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``angularResolution``             | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``metric``                        | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``              | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalNumberOfOrientations``   | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``               | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``            | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+

A template defined by a view and a region is matched with a source view for translations and rotations constrained to certain ranges. The translation range is defined by a box and the angle range by a minimum maximum pair. The method searches for local maximum matches that are stronger than a given threshold.

The search is performed using a pyramid approach. The number pyramid levels is computed from the given minimal size of the model, the minimal number of orientations and the translation and angle ranges.

/return Returns a list of match scores, pixel positions and matching angles.

Method *TemplateSearchWithRotationOnDemand*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearchWithRotationOnDemand(ViewLocatorDouble source, ViewLocatorDouble model, Region modelMask, System.Double threshold, System.Double minAngle, System.Double maxAngle, System.Double angularResolution, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Int32 minimalNumberOfOrientations, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches (with rotation) for template matches in a view with a constrained search space and computes only necessary rotations on demand.

The method **TemplateSearchWithRotationOnDemand** has the following parameters:

+-----------------------------------+---------------------------------------------------+---------------+
| Parameter                         | Type                                              | Description   |
+===================================+===================================================+===============+
| ``source``                        | ``ViewLocatorDouble``                             |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``model``                         | ``ViewLocatorDouble``                             |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``modelMask``                     | ``Region``                                        |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``threshold``                     | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``maxAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``angularResolution``             | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``metric``                        | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``              | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalNumberOfOrientations``   | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``               | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``            | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+

A template defined by a view and a region is matched with a source view for translations and rotations constrained to certain ranges. The translation range is defined by a box and the angle range by a minimum maximum pair. The method searches for local maximum matches that are stronger than a given threshold.

The search is performed using a pyramid approach. The number pyramid levels is computed from the given minimal size of the model, the minimal number of orientations and the translation and angle ranges.

/return Returns a list of match scores, pixel positions and matching angles.

Method *TemplateSearchWithRotationOnDemand*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearchWithRotationOnDemand(ViewLocatorRgbByte source, ViewLocatorRgbByte model, Region modelMask, System.Double threshold, System.Double minAngle, System.Double maxAngle, System.Double angularResolution, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Int32 minimalNumberOfOrientations, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches (with rotation) for template matches in a view with a constrained search space and computes only necessary rotations on demand.

The method **TemplateSearchWithRotationOnDemand** has the following parameters:

+-----------------------------------+---------------------------------------------------+---------------+
| Parameter                         | Type                                              | Description   |
+===================================+===================================================+===============+
| ``source``                        | ``ViewLocatorRgbByte``                            |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``model``                         | ``ViewLocatorRgbByte``                            |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``modelMask``                     | ``Region``                                        |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``threshold``                     | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``maxAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``angularResolution``             | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``metric``                        | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``              | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalNumberOfOrientations``   | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``               | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``            | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+

A template defined by a view and a region is matched with a source view for translations and rotations constrained to certain ranges. The translation range is defined by a box and the angle range by a minimum maximum pair. The method searches for local maximum matches that are stronger than a given threshold.

The search is performed using a pyramid approach. The number pyramid levels is computed from the given minimal size of the model, the minimal number of orientations and the translation and angle ranges.

/return Returns a list of match scores, pixel positions and matching angles.

Method *TemplateSearchWithRotationOnDemand*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearchWithRotationOnDemand(ViewLocatorRgbUInt16 source, ViewLocatorRgbUInt16 model, Region modelMask, System.Double threshold, System.Double minAngle, System.Double maxAngle, System.Double angularResolution, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Int32 minimalNumberOfOrientations, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches (with rotation) for template matches in a view with a constrained search space and computes only necessary rotations on demand.

The method **TemplateSearchWithRotationOnDemand** has the following parameters:

+-----------------------------------+---------------------------------------------------+---------------+
| Parameter                         | Type                                              | Description   |
+===================================+===================================================+===============+
| ``source``                        | ``ViewLocatorRgbUInt16``                          |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``model``                         | ``ViewLocatorRgbUInt16``                          |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``modelMask``                     | ``Region``                                        |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``threshold``                     | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``maxAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``angularResolution``             | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``metric``                        | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``              | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalNumberOfOrientations``   | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``               | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``            | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+

A template defined by a view and a region is matched with a source view for translations and rotations constrained to certain ranges. The translation range is defined by a box and the angle range by a minimum maximum pair. The method searches for local maximum matches that are stronger than a given threshold.

The search is performed using a pyramid approach. The number pyramid levels is computed from the given minimal size of the model, the minimal number of orientations and the translation and angle ranges.

/return Returns a list of match scores, pixel positions and matching angles.

Method *TemplateSearchWithRotationOnDemand*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearchWithRotationOnDemand(ViewLocatorRgbUInt32 source, ViewLocatorRgbUInt32 model, Region modelMask, System.Double threshold, System.Double minAngle, System.Double maxAngle, System.Double angularResolution, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Int32 minimalNumberOfOrientations, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches (with rotation) for template matches in a view with a constrained search space and computes only necessary rotations on demand.

The method **TemplateSearchWithRotationOnDemand** has the following parameters:

+-----------------------------------+---------------------------------------------------+---------------+
| Parameter                         | Type                                              | Description   |
+===================================+===================================================+===============+
| ``source``                        | ``ViewLocatorRgbUInt32``                          |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``model``                         | ``ViewLocatorRgbUInt32``                          |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``modelMask``                     | ``Region``                                        |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``threshold``                     | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``maxAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``angularResolution``             | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``metric``                        | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``              | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalNumberOfOrientations``   | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``               | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``            | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+

A template defined by a view and a region is matched with a source view for translations and rotations constrained to certain ranges. The translation range is defined by a box and the angle range by a minimum maximum pair. The method searches for local maximum matches that are stronger than a given threshold.

The search is performed using a pyramid approach. The number pyramid levels is computed from the given minimal size of the model, the minimal number of orientations and the translation and angle ranges.

/return Returns a list of match scores, pixel positions and matching angles.

Method *TemplateSearchWithRotationOnDemand*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearchWithRotationOnDemand(ViewLocatorRgbDouble source, ViewLocatorRgbDouble model, Region modelMask, System.Double threshold, System.Double minAngle, System.Double maxAngle, System.Double angularResolution, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Int32 minimalNumberOfOrientations, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches (with rotation) for template matches in a view with a constrained search space and computes only necessary rotations on demand.

The method **TemplateSearchWithRotationOnDemand** has the following parameters:

+-----------------------------------+---------------------------------------------------+---------------+
| Parameter                         | Type                                              | Description   |
+===================================+===================================================+===============+
| ``source``                        | ``ViewLocatorRgbDouble``                          |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``model``                         | ``ViewLocatorRgbDouble``                          |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``modelMask``                     | ``Region``                                        |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``threshold``                     | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``maxAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``angularResolution``             | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``metric``                        | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``              | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalNumberOfOrientations``   | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``               | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``            | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+

A template defined by a view and a region is matched with a source view for translations and rotations constrained to certain ranges. The translation range is defined by a box and the angle range by a minimum maximum pair. The method searches for local maximum matches that are stronger than a given threshold.

The search is performed using a pyramid approach. The number pyramid levels is computed from the given minimal size of the model, the minimal number of orientations and the translation and angle ranges.

/return Returns a list of match scores, pixel positions and matching angles.

Method *TemplateSearchWithRotationOnDemand*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearchWithRotationOnDemand(ViewLocatorRgbaByte source, ViewLocatorRgbaByte model, Region modelMask, System.Double threshold, System.Double minAngle, System.Double maxAngle, System.Double angularResolution, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Int32 minimalNumberOfOrientations, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches (with rotation) for template matches in a view with a constrained search space and computes only necessary rotations on demand.

The method **TemplateSearchWithRotationOnDemand** has the following parameters:

+-----------------------------------+---------------------------------------------------+---------------+
| Parameter                         | Type                                              | Description   |
+===================================+===================================================+===============+
| ``source``                        | ``ViewLocatorRgbaByte``                           |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``model``                         | ``ViewLocatorRgbaByte``                           |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``modelMask``                     | ``Region``                                        |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``threshold``                     | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``maxAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``angularResolution``             | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``metric``                        | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``              | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalNumberOfOrientations``   | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``               | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``            | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+

A template defined by a view and a region is matched with a source view for translations and rotations constrained to certain ranges. The translation range is defined by a box and the angle range by a minimum maximum pair. The method searches for local maximum matches that are stronger than a given threshold.

The search is performed using a pyramid approach. The number pyramid levels is computed from the given minimal size of the model, the minimal number of orientations and the translation and angle ranges.

/return Returns a list of match scores, pixel positions and matching angles.

Method *TemplateSearchWithRotationOnDemand*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearchWithRotationOnDemand(ViewLocatorRgbaUInt16 source, ViewLocatorRgbaUInt16 model, Region modelMask, System.Double threshold, System.Double minAngle, System.Double maxAngle, System.Double angularResolution, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Int32 minimalNumberOfOrientations, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches (with rotation) for template matches in a view with a constrained search space and computes only necessary rotations on demand.

The method **TemplateSearchWithRotationOnDemand** has the following parameters:

+-----------------------------------+---------------------------------------------------+---------------+
| Parameter                         | Type                                              | Description   |
+===================================+===================================================+===============+
| ``source``                        | ``ViewLocatorRgbaUInt16``                         |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``model``                         | ``ViewLocatorRgbaUInt16``                         |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``modelMask``                     | ``Region``                                        |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``threshold``                     | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``maxAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``angularResolution``             | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``metric``                        | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``              | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalNumberOfOrientations``   | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``               | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``            | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+

A template defined by a view and a region is matched with a source view for translations and rotations constrained to certain ranges. The translation range is defined by a box and the angle range by a minimum maximum pair. The method searches for local maximum matches that are stronger than a given threshold.

The search is performed using a pyramid approach. The number pyramid levels is computed from the given minimal size of the model, the minimal number of orientations and the translation and angle ranges.

/return Returns a list of match scores, pixel positions and matching angles.

Method *TemplateSearchWithRotationOnDemand*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearchWithRotationOnDemand(ViewLocatorRgbaUInt32 source, ViewLocatorRgbaUInt32 model, Region modelMask, System.Double threshold, System.Double minAngle, System.Double maxAngle, System.Double angularResolution, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Int32 minimalNumberOfOrientations, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches (with rotation) for template matches in a view with a constrained search space and computes only necessary rotations on demand.

The method **TemplateSearchWithRotationOnDemand** has the following parameters:

+-----------------------------------+---------------------------------------------------+---------------+
| Parameter                         | Type                                              | Description   |
+===================================+===================================================+===============+
| ``source``                        | ``ViewLocatorRgbaUInt32``                         |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``model``                         | ``ViewLocatorRgbaUInt32``                         |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``modelMask``                     | ``Region``                                        |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``threshold``                     | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``maxAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``angularResolution``             | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``metric``                        | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``              | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalNumberOfOrientations``   | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``               | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``            | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+

A template defined by a view and a region is matched with a source view for translations and rotations constrained to certain ranges. The translation range is defined by a box and the angle range by a minimum maximum pair. The method searches for local maximum matches that are stronger than a given threshold.

The search is performed using a pyramid approach. The number pyramid levels is computed from the given minimal size of the model, the minimal number of orientations and the translation and angle ranges.

/return Returns a list of match scores, pixel positions and matching angles.

Method *TemplateSearchWithRotationOnDemand*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearchWithRotationOnDemand(ViewLocatorRgbaDouble source, ViewLocatorRgbaDouble model, Region modelMask, System.Double threshold, System.Double minAngle, System.Double maxAngle, System.Double angularResolution, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Int32 minimalNumberOfOrientations, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches (with rotation) for template matches in a view with a constrained search space and computes only necessary rotations on demand.

The method **TemplateSearchWithRotationOnDemand** has the following parameters:

+-----------------------------------+---------------------------------------------------+---------------+
| Parameter                         | Type                                              | Description   |
+===================================+===================================================+===============+
| ``source``                        | ``ViewLocatorRgbaDouble``                         |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``model``                         | ``ViewLocatorRgbaDouble``                         |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``modelMask``                     | ``Region``                                        |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``threshold``                     | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``maxAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``angularResolution``             | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``metric``                        | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``              | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalNumberOfOrientations``   | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``               | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``            | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+

A template defined by a view and a region is matched with a source view for translations and rotations constrained to certain ranges. The translation range is defined by a box and the angle range by a minimum maximum pair. The method searches for local maximum matches that are stronger than a given threshold.

The search is performed using a pyramid approach. The number pyramid levels is computed from the given minimal size of the model, the minimal number of orientations and the translation and angle ranges.

/return Returns a list of match scores, pixel positions and matching angles.

Method *TemplateSearchWithRotationOnDemand*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList TemplateSearchWithRotationOnDemand(View source, View model, Region modelMask, System.Double threshold, System.Double minAngle, System.Double maxAngle, System.Double angularResolution, TemplateMatchingAlgorithms.SimilarityMetric metric, System.Int32 minimalModelSize, System.Int32 minimalNumberOfOrientations, System.Double thresholdAdjust, System.Int32 stopAtPyramidLevel)``

Searches (with rotation) for template matches in a view with a constrained search space and computes only necessary rotations on demand.

The method **TemplateSearchWithRotationOnDemand** has the following parameters:

+-----------------------------------+---------------------------------------------------+---------------+
| Parameter                         | Type                                              | Description   |
+===================================+===================================================+===============+
| ``source``                        | ``View``                                          |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``model``                         | ``View``                                          |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``modelMask``                     | ``Region``                                        |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``threshold``                     | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``maxAngle``                      | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``angularResolution``             | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``metric``                        | ``TemplateMatchingAlgorithms.SimilarityMetric``   |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalModelSize``              | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``minimalNumberOfOrientations``   | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``thresholdAdjust``               | ``System.Double``                                 |               |
+-----------------------------------+---------------------------------------------------+---------------+
| ``stopAtPyramidLevel``            | ``System.Int32``                                  |               |
+-----------------------------------+---------------------------------------------------+---------------+

A template defined by a view and a region is matched with a source view for translations and rotations constrained to certain ranges. The translation range is defined by a box and the angle range by a minimum maximum pair. The method searches for local maximum matches that are stronger than a given threshold.

The search is performed using a pyramid approach. The number pyramid levels is computed from the given minimal size of the model, the minimal number of orientations and the translation and angle ranges.

/return Returns a list of match scores, pixel positions and matching angles.

Enumerations
~~~~~~~~~~~~

Enumeration *SimilarityMetric*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum SimilarityMetric``

TODO documentation missing

The enumeration **SimilarityMetric** has the following constants:

+------------+---------+---------------+
| Name       | Value   | Description   |
+============+=========+===============+
| ``sad``    | ``0``   |               |
+------------+---------+---------------+
| ``ncc``    | ``2``   |               |
+------------+---------+---------------+
| ``nccr``   | ``3``   |               |
+------------+---------+---------------+
| ``vsc``    | ``6``   |               |
+------------+---------+---------------+
| ``vscr``   | ``7``   |               |
+------------+---------+---------------+
| ``geo``    | ``9``   |               |
+------------+---------+---------------+
| ``geor``   | ``8``   |               |
+------------+---------+---------------+

::

    enum SimilarityMetric
    {
      sad = 0,
      ncc = 2,
      nccr = 3,
      vsc = 6,
      vscr = 7,
      geo = 9,
      geor = 8,
    };

TODO documentation missing
