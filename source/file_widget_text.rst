Class *WidgetText*
------------------

A widget\_text is usually used as a child of some parent widget in order to render text within the bounds of this parent.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetText** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetText** contains the following properties:

+-------------------------------+-------+-------+----------------------------------+
| Property                      | Get   | Set   | Description                      |
+===============================+=======+=======+==================================+
| ``Text``                      | \*    | \*    |                                  |
+-------------------------------+-------+-------+----------------------------------+
| ``TextFont``                  | \*    | \*    |                                  |
+-------------------------------+-------+-------+----------------------------------+
| ``TextBrush``                 | \*    | \*    | The text brush.                  |
+-------------------------------+-------+-------+----------------------------------+
| ``HorizontalTextAlignment``   | \*    | \*    | The horizontal text alignment.   |
+-------------------------------+-------+-------+----------------------------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_text. These settings affect the look and the behavior of the widget\_text.

Among the settings you can make are the text, font, the text\_brush, as well as the horizontal\_text\_alignment.

The text widget supports multi-line text by inserting ":raw-latex:`\n`" characters in the text.

The horizontal\_text\_alignment affects the alignment of multiline text. This widget supports layout.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetText*
^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetText()``

Default constructor.

The default constructor creates a widget with default settings. By default the text is empty, so you will not see anything, unless you put some text into the widget. The text brush is opaque black by default, and the horizontal and vertical alignment are both set to center.

Properties
~~~~~~~~~~

Property *Text*
^^^^^^^^^^^^^^^

``System.String Text``

Property *TextFont*
^^^^^^^^^^^^^^^^^^^

``Font TextFont``

Property *TextBrush*
^^^^^^^^^^^^^^^^^^^^

``SolidColorBrushByte TextBrush``

The text brush.

Property *HorizontalTextAlignment*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Int32 HorizontalTextAlignment``

The horizontal text alignment.
