Class *OrderedPoints*
---------------------

This object keeps a set of points in an orthogonally ordered way of columns x rows cells.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **OrderedPoints** implements the following interfaces:

+--------------------------------------------------------+
| Interface                                              |
+========================================================+
| ``IEquatableOrderedPointsOrderedPointsPointVariant``   |
+--------------------------------------------------------+
| ``ISerializable``                                      |
+--------------------------------------------------------+

The class **OrderedPoints** contains the following variant parameters:

+-------------+-----------------------------------------+
| Variant     | Description                             |
+=============+=========================================+
| ``Point``   | TODO no brief description for variant   |
+-------------+-----------------------------------------+

The class **OrderedPoints** contains the following properties:

+---------------+-------+-------+-------------------------------------------------+
| Property      | Get   | Set   | Description                                     |
+===============+=======+=======+=================================================+
| ``Columns``   | \*    |       | The number of columns of the coordinate grid.   |
+---------------+-------+-------+-------------------------------------------------+
| ``Rows``      | \*    |       | The number of rows of the coordinate grid.      |
+---------------+-------+-------+-------------------------------------------------+
| ``Points``    | \*    | \*    | The number of columns of the coordinate grid.   |
+---------------+-------+-------+-------------------------------------------------+

The class **OrderedPoints** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

The sort\_orthogonal\_calibration\_target\_coordinates creates objects of this type.

Variants
~~~~~~~~

Variant *Point*
^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Point** has the following types:

+-------------------+
| Type              |
+===================+
| ``PointInt32``    |
+-------------------+
| ``PointDouble``   |
+-------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *OrderedPoints*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``OrderedPoints()``

Constructs empty ordered\_points.

Constructors
~~~~~~~~~~~~

Constructor *OrderedPoints*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``OrderedPoints(System.Int32 columns, System.Int32 rows)``

Constructs ordered\_points of specific size with zeroed points.

The constructor has the following parameters:

+---------------+--------------------+--------------------------+
| Parameter     | Type               | Description              |
+===============+====================+==========================+
| ``columns``   | ``System.Int32``   | The number of columns.   |
+---------------+--------------------+--------------------------+
| ``rows``      | ``System.Int32``   | The number of rows.      |
+---------------+--------------------+--------------------------+

Constructor *OrderedPoints*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``OrderedPoints(System.Int32 columns, System.Int32 rows, Point defaultValue)``

Constructs ordered\_points of specific size.

The constructor has the following parameters:

+--------------------+--------------------+--------------------------+
| Parameter          | Type               | Description              |
+====================+====================+==========================+
| ``columns``        | ``System.Int32``   | The number of columns.   |
+--------------------+--------------------+--------------------------+
| ``rows``           | ``System.Int32``   | The number of rows.      |
+--------------------+--------------------+--------------------------+
| ``defaultValue``   | ``Point``          | The default point.       |
+--------------------+--------------------+--------------------------+

Properties
~~~~~~~~~~

Property *Columns*
^^^^^^^^^^^^^^^^^^

``System.Int32 Columns``

The number of columns of the coordinate grid.

Property *Rows*
^^^^^^^^^^^^^^^

``System.Int32 Rows``

The number of rows of the coordinate grid.

Property *Points*
^^^^^^^^^^^^^^^^^

``PointList Points``

The number of columns of the coordinate grid.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
