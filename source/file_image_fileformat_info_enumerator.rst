Class *ImageFileformatInfoEnumerator*
-------------------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **ImageFileformatInfoEnumerator** implements the following interfaces:

+--------------------------------------+
| Interface                            |
+======================================+
| ``IEnumerator``                      |
+--------------------------------------+
| ``IEnumeratorImageFileformatInfo``   |
+--------------------------------------+

The class **ImageFileformatInfoEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **ImageFileformatInfoEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``ImageFileformatInfo Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
