Class *CameraParameterIcategory*
--------------------------------

**Namespace:** Ngi

**Module:** Camera

The class **CameraParameterIcategory** contains the following properties:

+----------------+-------+-------+---------------+
| Property       | Get   | Set   | Description   |
+================+=======+=======+===============+
| ``Features``   | \*    |       |               |
+----------------+-------+-------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Features*
^^^^^^^^^^^^^^^^^^^

``CameraParameterBaseList Features``
