Class *Point3dEnumerator*
-------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Point3dEnumerator** implements the following interfaces:

+--------------------------------------------------+
| Interface                                        |
+==================================================+
| ``IEnumerator``                                  |
+--------------------------------------------------+
| ``IEnumeratorPoint3dEnumeratorPoint3dVariant``   |
+--------------------------------------------------+

The class **Point3dEnumerator** contains the following variant parameters:

+---------------+-----------------------------------------+
| Variant       | Description                             |
+===============+=========================================+
| ``Point3d``   | TODO no brief description for variant   |
+---------------+-----------------------------------------+

The class **Point3dEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **Point3dEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Point3d*
^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Point3d** has the following types:

+---------------------+
| Type                |
+=====================+
| ``Point3dInt32``    |
+---------------------+
| ``Point3dDouble``   |
+---------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Point3d Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
