Class *File*
------------

The file.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **File** contains the following properties:

+----------------+-------+-------+-----------------+
| Property       | Get   | Set   | Description     |
+================+=======+=======+=================+
| ``Filename``   | \*    |       | The filename.   |
+----------------+-------+-------+-----------------+

Description
~~~~~~~~~~~

Constructors
~~~~~~~~~~~~

Constructor *File*
^^^^^^^^^^^^^^^^^^

``File(System.String filename, System.Boolean read, System.Boolean write)``

Construct the file.

The constructor has the following parameters:

+----------------+----------------------+-----------------+
| Parameter      | Type                 | Description     |
+================+======================+=================+
| ``filename``   | ``System.String``    | The filename.   |
+----------------+----------------------+-----------------+
| ``read``       | ``System.Boolean``   |                 |
+----------------+----------------------+-----------------+
| ``write``      | ``System.Boolean``   |                 |
+----------------+----------------------+-----------------+

Properties
~~~~~~~~~~

Property *Filename*
^^^^^^^^^^^^^^^^^^^

``System.String Filename``

The filename.
