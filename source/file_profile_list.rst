Class *ProfileList*
-------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **ProfileList** implements the following interfaces:

+--------------------------------------+
| Interface                            |
+======================================+
| ``IListProfileListProfileVariant``   |
+--------------------------------------+

The class **ProfileList** contains the following variant parameters:

+---------------+-----------------------------------------+
| Variant       | Description                             |
+===============+=========================================+
| ``Profile``   | TODO no brief description for variant   |
+---------------+-----------------------------------------+

The class **ProfileList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **ProfileList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Profile*
^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Profile** has the following types:

+-------------------------+
| Type                    |
+=========================+
| ``ProfileByte``         |
+-------------------------+
| ``ProfileUInt16``       |
+-------------------------+
| ``ProfileUInt32``       |
+-------------------------+
| ``ProfileDouble``       |
+-------------------------+
| ``ProfileRgbByte``      |
+-------------------------+
| ``ProfileRgbUInt16``    |
+-------------------------+
| ``ProfileRgbUInt32``    |
+-------------------------+
| ``ProfileRgbDouble``    |
+-------------------------+
| ``ProfileRgbaByte``     |
+-------------------------+
| ``ProfileRgbaUInt16``   |
+-------------------------+
| ``ProfileRgbaUInt32``   |
+-------------------------+
| ``ProfileRgbaDouble``   |
+-------------------------+
| ``ProfileHlsByte``      |
+-------------------------+
| ``ProfileHlsUInt16``    |
+-------------------------+
| ``ProfileHlsDouble``    |
+-------------------------+
| ``ProfileHsiByte``      |
+-------------------------+
| ``ProfileHsiUInt16``    |
+-------------------------+
| ``ProfileHsiDouble``    |
+-------------------------+
| ``ProfileLabByte``      |
+-------------------------+
| ``ProfileLabUInt16``    |
+-------------------------+
| ``ProfileLabDouble``    |
+-------------------------+
| ``ProfileXyzByte``      |
+-------------------------+
| ``ProfileXyzUInt16``    |
+-------------------------+
| ``ProfileXyzDouble``    |
+-------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *ProfileList*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ProfileList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``Profile [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``ProfileEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(Profile item)``

The method **Add** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``item``    | ``Profile``   |               |
+-------------+---------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(Profile item)``

The method **Contains** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``item``    | ``Profile``   |               |
+-------------+---------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(Profile item)``

The method **Remove** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``item``    | ``Profile``   |               |
+-------------+---------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(Profile item)``

The method **IndexOf** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``item``    | ``Profile``   |               |
+-------------+---------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, Profile item)``

The method **Insert** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``item``    | ``Profile``        |               |
+-------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
