Class *CadContourEnumerator*
----------------------------

**Namespace:** Ngi

**Module:** CadPdf

The class **CadContourEnumerator** implements the following interfaces:

+-----------------------------+
| Interface                   |
+=============================+
| ``IEnumerator``             |
+-----------------------------+
| ``IEnumeratorCadContour``   |
+-----------------------------+

The class **CadContourEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **CadContourEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``CadContour Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
