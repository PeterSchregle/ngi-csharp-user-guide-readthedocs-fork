Class *Extent3d*
----------------

A three-dimensional extent.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Extent3d** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``IEquatableExtent3d``       |
+------------------------------+
| ``ISerializable``            |
+------------------------------+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **Extent3d** contains the following properties:

+--------------+-------+-------+---------------+
| Property     | Get   | Set   | Description   |
+==============+=======+=======+===============+
| ``Width``    | \*    | \*    | The width.    |
+--------------+-------+-------+---------------+
| ``Height``   | \*    | \*    | The height.   |
+--------------+-------+-------+---------------+
| ``Depth``    | \*    | \*    | The depth.    |
+--------------+-------+-------+---------------+

The class **Extent3d** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

This is used to specify the size of a three-dimensional, rectangular area within a buffer.

Together with an index\_3d, this is used to specify a three-dimensional, rectangular subset of a buffer.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Extent3d*
^^^^^^^^^^^^^^^^^^^^^^

``Extent3d()``

Constructs default extent\_3d (1, 1, 1).

Constructors
~~~~~~~~~~~~

Constructor *Extent3d*
^^^^^^^^^^^^^^^^^^^^^^

``Extent3d(System.Int32 width)``

Constructs specific extent\_3d.

The constructor has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``width``   | ``System.Int32``   | The width.    |
+-------------+--------------------+---------------+

Constructor *Extent3d*
^^^^^^^^^^^^^^^^^^^^^^

``Extent3d(System.Int32 width, System.Int32 height)``

Constructs specific extent\_3d from extent (conversion).

The constructor has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``width``    | ``System.Int32``   |               |
+--------------+--------------------+---------------+
| ``height``   | ``System.Int32``   |               |
+--------------+--------------------+---------------+

Constructor *Extent3d*
^^^^^^^^^^^^^^^^^^^^^^

``Extent3d(System.Int32 width, System.Int32 height, System.Int32 depth)``

Constructs specific extent from vector (conversion).

The constructor has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``width``    | ``System.Int32``   |               |
+--------------+--------------------+---------------+
| ``height``   | ``System.Int32``   |               |
+--------------+--------------------+---------------+
| ``depth``    | ``System.Int32``   |               |
+--------------+--------------------+---------------+

Constructor *Extent3d*
^^^^^^^^^^^^^^^^^^^^^^

``Extent3d(Extent size)``

Constructs specific extent from vector\_3d (conversion).

The constructor has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``size``    | ``Extent``   |               |
+-------------+--------------+---------------+

Properties
~~~~~~~~~~

Property *Width*
^^^^^^^^^^^^^^^^

``System.Int32 Width``

The width.

Property *Height*
^^^^^^^^^^^^^^^^^

``System.Int32 Height``

The height.

Property *Depth*
^^^^^^^^^^^^^^^^

``System.Int32 Depth``

The depth.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO no brief description for variant

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO no description for variant
