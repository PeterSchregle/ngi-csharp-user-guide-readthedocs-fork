Class *GeometryLineSegment*
---------------------------

The following operations are implemented: operator== : comparison for equality.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **GeometryLineSegment** implements the following interfaces:

+------------------+
| Interface        |
+==================+
| ``IEquatable``   |
+------------------+

The class **GeometryLineSegment** contains the following methods:

+---------------------+---------------------------------------------------------+
| Method              | Description                                             |
+=====================+=========================================================+
| ``ToString``        | Provide string representation for debugging purposes.   |
+---------------------+---------------------------------------------------------+
| ``ToLineSegment``   | Convert to a standalone line segment.                   |
+---------------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

operator!= : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *GeometryLineSegment*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``GeometryLineSegment()``

Construct a geometry line segment from a point.

Constructors
~~~~~~~~~~~~

Constructor *GeometryLineSegment*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``GeometryLineSegment(PointDouble point)``

Construct a geometry line segment from a point.

The constructor has the following parameters:

+-------------+-------------------+---------------+
| Parameter   | Type              | Description   |
+=============+===================+===============+
| ``point``   | ``PointDouble``   |               |
+-------------+-------------------+---------------+

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Method *ToLineSegment*
^^^^^^^^^^^^^^^^^^^^^^

``LineSegmentDouble ToLineSegment(PointDouble startPoint)``

Convert to a standalone line segment.

The method **ToLineSegment** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``startPoint``   | ``PointDouble``   |               |
+------------------+-------------------+---------------+
