Class *WidgetPolarGrid*
-----------------------

A widget\_polar\_grid is a widget displaying a grid of radial lines crossing a set of concentric circles.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetPolarGrid** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetPolarGrid** contains the following properties:

+----------------+-------+-------+------------------------------------------+
| Property       | Get   | Set   | Description                              |
+================+=======+=======+==========================================+
| ``Position``   | \*    | \*    | The position (location) of the widget.   |
+----------------+-------+-------+------------------------------------------+
| ``Radius``     | \*    | \*    | The radius of the widget.                |
+----------------+-------+-------+------------------------------------------+
| ``Sectors``    | \*    | \*    | The number of sectors of the widget.     |
+----------------+-------+-------+------------------------------------------+
| ``Pen``        | \*    | \*    | The pen of the widget.                   |
+----------------+-------+-------+------------------------------------------+

Description
~~~~~~~~~~~

It can be positioned on some surface. It provides interaction, but tracking is not implemented. A widget\_polar\_grid has interactive widget capabilities.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetPolarGrid*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetPolarGrid()``

Default constructor.

The default constructor creates a widget with default settings.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``PointDouble Position``

The position (location) of the widget.

Property *Radius*
^^^^^^^^^^^^^^^^^

``VectorDouble Radius``

The radius of the widget.

Property *Sectors*
^^^^^^^^^^^^^^^^^^

``System.Int32 Sectors``

The number of sectors of the widget.

Property *Pen*
^^^^^^^^^^^^^^

``PenByte Pen``

The pen of the widget.
