Class *PerspectiveMatrix*
-------------------------

A matrix useful for perspective 2D geometric transformations.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **PerspectiveMatrix** implements the following interfaces:

+-----------------------------------+
| Interface                         |
+===================================+
| ``IEquatablePerspectiveMatrix``   |
+-----------------------------------+
| ``ISerializable``                 |
+-----------------------------------+

The class **PerspectiveMatrix** contains the following properties:

+-------------------+-------+-------+----------------------------------------------------+
| Property          | Get   | Set   | Description                                        |
+===================+=======+=======+====================================================+
| ``M11``           | \*    |       | The element m11 (row 1, column 1) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M12``           | \*    |       | The element m12 (row 1, column 2) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M13``           | \*    |       | The element m13 (row 1, column 3) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M21``           | \*    |       | The element m21 (row 2, column 1) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M22``           | \*    |       | The element m22 (row 2, column 2) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M23``           | \*    |       | The element m23 (row 2, column 3) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M31``           | \*    |       | The element m31 (row 3, column 1) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M32``           | \*    |       | The element m32 (row 3, column 2) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M33``           | \*    |       | The element m33 (row 3, column 3) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``[index]``       | \*    | \*    | The matrix values as a parameterized property.     |
+-------------------+-------+-------+----------------------------------------------------+
| ``Determinant``   | \*    |       | The determinant of the matrix.                     |
+-------------------+-------+-------+----------------------------------------------------+
| ``Trace``         | \*    |       | The trace of the matrix.                           |
+-------------------+-------+-------+----------------------------------------------------+
| ``Inverse``       | \*    |       | The inverse of the matrix.                         |
+-------------------+-------+-------+----------------------------------------------------+

The class **PerspectiveMatrix** contains the following methods:

+-----------------------+--------------------------------------------+
| Method                | Description                                |
+=======================+============================================+
| ``ResetToIdentity``   | Reset the matrix to the identity matrix.   |
+-----------------------+--------------------------------------------+

Description
~~~~~~~~~~~

The values in the matrix are arranged like this:

| value\_[0] value\_[3] value\_[6] \| \| value\_[1] value\_[4] value\_[7] \| \| value\_[2] value\_[5] 1 \|

The last value is not stored within this object but implicit.

This storage assumes row-vectors and post-multiplication for transformations.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *PerspectiveMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``PerspectiveMatrix()``

Default constructor.

By default this is the identity matrix.

Constructors
~~~~~~~~~~~~

Constructor *PerspectiveMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``PerspectiveMatrix(System.Double m11, System.Double m12, System.Double m13, System.Double m21, System.Double m22, System.Double m23, System.Double m31, System.Double m32)``

Constructor.

The constructor has the following parameters:

+-------------+---------------------+--------------------+
| Parameter   | Type                | Description        |
+=============+=====================+====================+
| ``m11``     | ``System.Double``   | The element m11.   |
+-------------+---------------------+--------------------+
| ``m12``     | ``System.Double``   | The element m12.   |
+-------------+---------------------+--------------------+
| ``m13``     | ``System.Double``   | The element m13.   |
+-------------+---------------------+--------------------+
| ``m21``     | ``System.Double``   | The element m21.   |
+-------------+---------------------+--------------------+
| ``m22``     | ``System.Double``   | The element m22.   |
+-------------+---------------------+--------------------+
| ``m23``     | ``System.Double``   | The element m23.   |
+-------------+---------------------+--------------------+
| ``m31``     | ``System.Double``   | The element m31.   |
+-------------+---------------------+--------------------+
| ``m32``     | ``System.Double``   | The element m32.   |
+-------------+---------------------+--------------------+

Constructor *PerspectiveMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``PerspectiveMatrix(AffineMatrix rhs)``

Copy constructor.

The constructor has the following parameters:

+-------------+--------------------+------------------------+
| Parameter   | Type               | Description            |
+=============+====================+========================+
| ``rhs``     | ``AffineMatrix``   | The right hand side.   |
+-------------+--------------------+------------------------+

Properties
~~~~~~~~~~

Property *M11*
^^^^^^^^^^^^^^

``System.Double M11``

The element m11 (row 1, column 1) of the matrix.

Property *M12*
^^^^^^^^^^^^^^

``System.Double M12``

The element m12 (row 1, column 2) of the matrix.

Property *M13*
^^^^^^^^^^^^^^

``System.Double M13``

The element m13 (row 1, column 3) of the matrix.

Property *M21*
^^^^^^^^^^^^^^

``System.Double M21``

The element m21 (row 2, column 1) of the matrix.

Property *M22*
^^^^^^^^^^^^^^

``System.Double M22``

The element m22 (row 2, column 2) of the matrix.

Property *M23*
^^^^^^^^^^^^^^

``System.Double M23``

The element m23 (row 2, column 3) of the matrix.

Property *M31*
^^^^^^^^^^^^^^

``System.Double M31``

The element m31 (row 3, column 1) of the matrix.

Property *M32*
^^^^^^^^^^^^^^

``System.Double M32``

The element m32 (row 3, column 2) of the matrix.

Property *M33*
^^^^^^^^^^^^^^

``System.Double M33``

The element m33 (row 3, column 3) of the matrix.

Property *[index]*
^^^^^^^^^^^^^^^^^^

``System.Double [index]``

The matrix values as a parameterized property.

This property allows you to retrieve the implicit values also, but you cannot set them.

Property *Determinant*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double Determinant``

The determinant of the matrix.

Property *Trace*
^^^^^^^^^^^^^^^^

``System.Double Trace``

The trace of the matrix.

Property *Inverse*
^^^^^^^^^^^^^^^^^^

``PerspectiveMatrix Inverse``

The inverse of the matrix.

Static Methods
~~~~~~~~~~~~~~

Method *Multiply*
^^^^^^^^^^^^^^^^^

``PerspectiveMatrix Multiply(PerspectiveMatrix a, PerspectiveMatrix b)``

Multiply two matrices.

The method **Multiply** has the following parameters:

+-------------+-------------------------+---------------+
| Parameter   | Type                    | Description   |
+=============+=========================+===============+
| ``a``       | ``PerspectiveMatrix``   |               |
+-------------+-------------------------+---------------+
| ``b``       | ``PerspectiveMatrix``   |               |
+-------------+-------------------------+---------------+

The product.

Methods
~~~~~~~

Method *ResetToIdentity*
^^^^^^^^^^^^^^^^^^^^^^^^

``void ResetToIdentity()``

Reset the matrix to the identity matrix.

The identity matrix has the following form. \| 1 0 0 \| \| 0 1 0 \| \| 0 0 1 \|A reference to this object.
