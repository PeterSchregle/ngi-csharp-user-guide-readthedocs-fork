Class *Ocr*
-----------

OCR (optical character recognition) functions.

**Namespace:** Ngi

**Module:** OcrOcv

The class **Ocr** contains the following variant parameters:

+--------------+-----------------------------------------+
| Variant      | Description                             |
+==============+=========================================+
| ``Source``   | TODO no brief description for variant   |
+--------------+-----------------------------------------+

Description
~~~~~~~~~~~

The class contains functions for optical character recognition.

Variants
~~~~~~~~

Variant *Source*
^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Source** has the following types:

+----------------+
| Type           |
+================+
| ``Byte``       |
+----------------+
| ``UInt16``     |
+----------------+
| ``UInt32``     |
+----------------+
| ``RgbByte``    |
+----------------+
| ``RgbaByte``   |
+----------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Static Methods
~~~~~~~~~~~~~~

Method *ReadText*
^^^^^^^^^^^^^^^^^

``System.String ReadText(View source, System.String fontFileName, System.Boolean whiteOnBlack, System.Boolean localThreshold, System.Boolean insertNewlines, System.Double minimumScore, System.Boolean discardBadScores)``

Read text in an image.

The method **ReadText** has the following parameters:

+------------------------+----------------------+---------------+
| Parameter              | Type                 | Description   |
+========================+======================+===============+
| ``source``             | ``View``             |               |
+------------------------+----------------------+---------------+
| ``fontFileName``       | ``System.String``    |               |
+------------------------+----------------------+---------------+
| ``whiteOnBlack``       | ``System.Boolean``   |               |
+------------------------+----------------------+---------------+
| ``localThreshold``     | ``System.Boolean``   |               |
+------------------------+----------------------+---------------+
| ``insertNewlines``     | ``System.Boolean``   |               |
+------------------------+----------------------+---------------+
| ``minimumScore``       | ``System.Double``    |               |
+------------------------+----------------------+---------------+
| ``discardBadScores``   | ``System.Boolean``   |               |
+------------------------+----------------------+---------------+

Method *ReadOcr*
^^^^^^^^^^^^^^^^

``OcrCharacterInfo ReadOcr(View source, System.String fontFileName, System.Boolean whiteOnBlack, System.Boolean localThreshold, System.Boolean insertNewlines, System.Double minimumScore, System.Boolean discardBadScores)``

Read Ocr Info in an image.

The method **ReadOcr** has the following parameters:

+------------------------+----------------------+---------------+
| Parameter              | Type                 | Description   |
+========================+======================+===============+
| ``source``             | ``View``             |               |
+------------------------+----------------------+---------------+
| ``fontFileName``       | ``System.String``    |               |
+------------------------+----------------------+---------------+
| ``whiteOnBlack``       | ``System.Boolean``   |               |
+------------------------+----------------------+---------------+
| ``localThreshold``     | ``System.Boolean``   |               |
+------------------------+----------------------+---------------+
| ``insertNewlines``     | ``System.Boolean``   |               |
+------------------------+----------------------+---------------+
| ``minimumScore``       | ``System.Double``    |               |
+------------------------+----------------------+---------------+
| ``discardBadScores``   | ``System.Boolean``   |               |
+------------------------+----------------------+---------------+

Method *TeachFont*
^^^^^^^^^^^^^^^^^^

``OcrCharacterInfo TeachFont(View source, System.Boolean whiteOnBlack, System.Boolean localThreshold, System.Boolean varaiableWidth, System.String characters, System.String fontFileName)``

Teach OCR font (all chararacters at once)

The method **TeachFont** has the following parameters:

+----------------------+----------------------+---------------+
| Parameter            | Type                 | Description   |
+======================+======================+===============+
| ``source``           | ``View``             |               |
+----------------------+----------------------+---------------+
| ``whiteOnBlack``     | ``System.Boolean``   |               |
+----------------------+----------------------+---------------+
| ``localThreshold``   | ``System.Boolean``   |               |
+----------------------+----------------------+---------------+
| ``varaiableWidth``   | ``System.Boolean``   |               |
+----------------------+----------------------+---------------+
| ``characters``       | ``System.String``    |               |
+----------------------+----------------------+---------------+
| ``fontFileName``     | ``System.String``    |               |
+----------------------+----------------------+---------------+

Source should contain the individual characters vertically one by one.
