Class *SegmentationAlgorithms*
------------------------------

Image segmentation functions.

**Namespace:** Ngi

**Module:** ImageProcessing

Description
~~~~~~~~~~~

The class contains the subpixel threshold function.

Static Methods
~~~~~~~~~~~~~~

Method *SubpixelThreshold*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``PolylineListPolylineDouble SubpixelThreshold(ViewLocatorByte source, System.Double threshold)``

This function performs a subpixel thresholding.

The method **SubpixelThreshold** has the following parameters:

+-----------------+-----------------------+---------------+
| Parameter       | Type                  | Description   |
+=================+=======================+===============+
| ``source``      | ``ViewLocatorByte``   |               |
+-----------------+-----------------------+---------------+
| ``threshold``   | ``System.Double``     |               |
+-----------------+-----------------------+---------------+

The result of the function is a list of contours.

A list of contours.

Method *SubpixelThreshold*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``PolylineListPolylineDouble SubpixelThreshold(ViewLocatorUInt16 source, System.Double threshold)``

This function performs a subpixel thresholding.

The method **SubpixelThreshold** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt16``   |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.Double``       |               |
+-----------------+-------------------------+---------------+

The result of the function is a list of contours.

A list of contours.

Method *SubpixelThreshold*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``PolylineListPolylineDouble SubpixelThreshold(ViewLocatorUInt32 source, System.Double threshold)``

This function performs a subpixel thresholding.

The method **SubpixelThreshold** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt32``   |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.Double``       |               |
+-----------------+-------------------------+---------------+

The result of the function is a list of contours.

A list of contours.

Method *SubpixelThreshold*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``PolylineListPolylineDouble SubpixelThreshold(ViewLocatorDouble source, System.Double threshold)``

This function performs a subpixel thresholding.

The method **SubpixelThreshold** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorDouble``   |               |
+-----------------+-------------------------+---------------+
| ``threshold``   | ``System.Double``       |               |
+-----------------+-------------------------+---------------+

The result of the function is a list of contours.

A list of contours.

Method *SubpixelThreshold*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``PolylineListPolylineDouble SubpixelThreshold(View source, System.Double threshold)``

This function performs a subpixel thresholding.

The method **SubpixelThreshold** has the following parameters:

+-----------------+---------------------+---------------+
| Parameter       | Type                | Description   |
+=================+=====================+===============+
| ``source``      | ``View``            |               |
+-----------------+---------------------+---------------+
| ``threshold``   | ``System.Double``   |               |
+-----------------+---------------------+---------------+

The result of the function is a list of contours.

A list of contours.
