Class *Vector3dEnumerator*
--------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Vector3dEnumerator** implements the following interfaces:

+----------------------------------------------------+
| Interface                                          |
+====================================================+
| ``IEnumerator``                                    |
+----------------------------------------------------+
| ``IEnumeratorVector3dEnumeratorVector3dVariant``   |
+----------------------------------------------------+

The class **Vector3dEnumerator** contains the following variant parameters:

+----------------+-----------------------------------------+
| Variant        | Description                             |
+================+=========================================+
| ``Vector3d``   | TODO no brief description for variant   |
+----------------+-----------------------------------------+

The class **Vector3dEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **Vector3dEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Vector3d*
^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Vector3d** has the following types:

+----------------------+
| Type                 |
+======================+
| ``Vector3dInt32``    |
+----------------------+
| ``Vector3dDouble``   |
+----------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Vector3d Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
