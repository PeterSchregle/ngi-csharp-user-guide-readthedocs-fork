Class *SolidColorBrush*
-----------------------

A solid color brush.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **SolidColorBrush** implements the following interfaces:

+--------------------------------------------------------------+
| Interface                                                    |
+==============================================================+
| ``IEquatableSolidColorBrushSolidColorBrushPrimaryVariant``   |
+--------------------------------------------------------------+
| ``ISerializable``                                            |
+--------------------------------------------------------------+
| ``INotifyPropertyChanged``                                   |
+--------------------------------------------------------------+

The class **SolidColorBrush** contains the following variant parameters:

+---------------+-----------------------------------------+
| Variant       | Description                             |
+===============+=========================================+
| ``Primary``   | TODO no brief description for variant   |
+---------------+-----------------------------------------+

The class **SolidColorBrush** contains the following properties:

+-------------+-------+-------+-----------------------------------------+
| Property    | Get   | Set   | Description                             |
+=============+=======+=======+=========================================+
| ``Color``   | \*    | \*    | The color of the solid\_color\_brush.   |
+-------------+-------+-------+-----------------------------------------+
| ``Alpha``   | \*    | \*    | The alpha of the solid\_color\_brush.   |
+-------------+-------+-------+-----------------------------------------+

The class **SolidColorBrush** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

A solid color brush has a color and opacity (alpha). It is used to fill geometric primitives.

The following operators are implemented for a solid\_color\_brush: operator == : comparison for equality. operator != : comparison for inequality.

Variants
~~~~~~~~

Variant *Primary*
^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Primary** has the following types:

+--------------+
| Type         |
+==============+
| ``Byte``     |
+--------------+
| ``UInt16``   |
+--------------+
| ``UInt32``   |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *SolidColorBrush*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``SolidColorBrush()``

Standard constructor.

Constructors
~~~~~~~~~~~~

Constructor *SolidColorBrush*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``SolidColorBrush(Rgb color, System.Double alpha)``

Standard constructor.

The constructor has the following parameters:

+-------------+---------------------+--------------------------------+
| Parameter   | Type                | Description                    |
+=============+=====================+================================+
| ``color``   | ``Rgb``             | A color (defaults to black).   |
+-------------+---------------------+--------------------------------+
| ``alpha``   | ``System.Double``   | Alpha (defaults to 1.0).       |
+-------------+---------------------+--------------------------------+

Constructor *SolidColorBrush*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``SolidColorBrush(Rgb color)``

Standard constructor.

The constructor has the following parameters:

+-------------+-----------+-----------------------+
| Parameter   | Type      | Description           |
+=============+===========+=======================+
| ``color``   | ``Rgb``   | A color with alpha.   |
+-------------+-----------+-----------------------+

Constructor *SolidColorBrush*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``SolidColorBrush(Primary color)``

Standard constructor.

The constructor has the following parameters:

+-------------+---------------+-----------------------+
| Parameter   | Type          | Description           |
+=============+===============+=======================+
| ``color``   | ``Primary``   | A color with alpha.   |
+-------------+---------------+-----------------------+

Properties
~~~~~~~~~~

Property *Color*
^^^^^^^^^^^^^^^^

``Rgb Color``

The color of the solid\_color\_brush.

Property *Alpha*
^^^^^^^^^^^^^^^^

``System.Double Alpha``

The alpha of the solid\_color\_brush.

Alpha is the opacity, the inverse of the transparency.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO documentation missing

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO documentation missing
