Class *Extent3dEnumerator*
--------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Extent3dEnumerator** implements the following interfaces:

+---------------------------+
| Interface                 |
+===========================+
| ``IEnumerator``           |
+---------------------------+
| ``IEnumeratorExtent3d``   |
+---------------------------+

The class **Extent3dEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **Extent3dEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Extent3d Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
