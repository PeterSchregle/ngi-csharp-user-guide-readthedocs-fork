Class *BlobFeaturesList*
------------------------

A list of blobs.

**Namespace:** Ngi

**Module:** AnalysisMeasuring

The class **BlobFeaturesList** implements the following interfaces:

+------------------------------------------------+
| Interface                                      |
+================================================+
| ``IListBlobFeaturesListBlobFeaturesVariant``   |
+------------------------------------------------+

The class **BlobFeaturesList** contains the following variant parameters:

+--------------------+-----------------------------------------+
| Variant            | Description                             |
+====================+=========================================+
| ``BlobFeatures``   | TODO no brief description for variant   |
+--------------------+-----------------------------------------+

The class **BlobFeaturesList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **BlobFeaturesList** contains the following methods:

+--------------------------------------------------+--------------------------------------------------------------------+
| Method                                           | Description                                                        |
+==================================================+====================================================================+
| ``Translate``                                    | Translate a blob\_features\_list.                                  |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateArea``                                | Calculate the area of all blobs.                                   |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateAreaOfHoles``                         | Calculate the area of holes of all blobs.                          |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateAspectRatio``                         | Calculate the aspect ratio value of all blobs.                     |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateBottom``                              | Calculate the bottom value of all blobs.                           |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateBoundingRectangle``                   | Calculate the bounding rectangle of all blobs.                     |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateBounds``                              | Calculate the bounds of all blobs.                                 |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateRegionCentralMoments``                | Calculate the region-based central moments of all blobs.           |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateRegionCentroid``                      | Calculate the region-based centroid of all blobs.                  |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateCircularity``                         | Calculate the circularity of all blobs.                            |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateChannelMaximum``                      | Calculate the channel maximum of all blobs.                        |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateChannelMinimum``                      | Calculate the channel minimum of all blobs.                        |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateCompactness``                         | Calculate the compactness of all blobs.                            |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateContrast``                            | Calculate the contrast of all blobs.                               |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateConvexArea``                          | Calculate the convex area of all blobs.                            |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateConvexHull``                          | Calculate the convex hull of all blobs.                            |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateConvexity``                           | Calculate the convexity of all blobs.                              |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateConvexPerimeter``                     | Calculate the convex perimeter of all blobs.                       |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateRegionEquivalentEllipse``             | Calculate the region-based equivalent ellipse of all blobs.        |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateFiberLength``                         | Calculate the fiber length of all blobs.                           |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateFiberWidth``                          | Calculate the fiber width of all blobs.                            |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateFirstPoint``                          | Calculate the first\_point of all blobs.                           |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateRegionFlusserMoments``                | Calculate the region-based flusser moments of all blobs.           |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateHeight``                              | Calculate the height value of all blobs.                           |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateHistogram``                           | Calculate the histogram of all blobs.                              |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateHoles``                               | Calculate the holes of all blobs.                                  |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateHorizontalProfile``                   | Calculate the horizontal profile of all blobs.                     |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateRegionHuMoments``                     | Calculate the region-based hu moments of all blobs.                |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateInnerContour``                        | Calculate the inner contour of all blobs.                          |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateInnerContourPerimeter``               | Calculate the inner contour perimeter of all blobs.                |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateLeft``                                | Calculate the left value of all blobs.                             |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateMaximum``                             | Calculate the maximum of all blobs.                                |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateMaximumFeretDiameter``                | Calculate the maximum feret diameter of all blobs.                 |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateMaximumPosition``                     | Calculate the maximum position of all blobs.                       |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateMean``                                | Calculate the mean of all blobs.                                   |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateMichelsonContrast``                   | Calculate the Michelson contrast of all blobs.                     |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateMinimum``                             | Calculate the minimum of all blobs.                                |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateMinimumAreaBoundingRectangle``        | Calculate the minimum area bounding rectangle of all blobs.        |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateMinimumBoundingCircle``               | Calculate the minimum bounding circle of all blobs.                |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateMinimumFeretDiameter``                | Calculate the minimum feret diameter of all blobs.                 |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateMinimumPerimeterBoundingRectangle``   | Calculate the minimum perimeter bounding rectangle of all blobs.   |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateMinimumPosition``                     | Calculate the minimum position of all blobs.                       |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateRegionRawMoments``                    | Calculate the region-based raw moments of all blobs.               |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateRegionNormalizedMoments``             | Calculate the region-based normalized moments of all blobs.        |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateNumberOfHoles``                       | Calculate the number of holes of all blobs.                        |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateOuterContour``                        | Calculate the outer contour of all blobs.                          |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateOuterContourPerimeter``               | Calculate the outer contour perimeter of all blobs.                |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculatePerforation``                         | Calculate the perforation of all blobs.                            |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculatePerimeter``                           | Calculate the perimeter of all blobs.                              |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculatePixelCentralMoments``                 | Calculate the pixel-based central moments of all blobs.            |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculatePixelCentroid``                       | Calculate the pixel-based centroid of all blobs.                   |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculatePixelEquivalentEllipse``              | Calculate the pixel-based equivalent ellipse of all blobs.         |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculatePixelFlusserMoments``                 | Calculate the pixel-based flusser moments of all blobs.            |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculatePixelHuMoments``                      | Calculate the pixel-based hu moments of all blobs.                 |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculatePixelRawMoments``                     | Calculate the pixel-based raw moments of all blobs.                |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculatePixelNormalizedMoments``              | Calculate the pixel-based normalized moments of all blobs.         |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculatePixelScaleInvariantMoments``          | Calculate the pixel-based scale invariant moments of all blobs.    |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateRight``                               | Calculate the right value of all blobs.                            |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateRoughness``                           | Calculate the roughness of all blobs.                              |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateRoundness``                           | Calculate the roundness of all blobs.                              |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateRegionScaleInvariantMoments``         | Calculate the region-based scale invariant moments of all blobs.   |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateSkewness``                            | Calculate the skewness of all blobs.                               |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateSphericity``                          | Calculate the sphericity of all blobs.                             |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateStandardDeviation``                   | Calculate the standard deviation of all blobs.                     |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateTop``                                 | Calculate the top value of all blobs.                              |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateTotal``                               | Calculate the total of all blobs.                                  |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateVerticalProfile``                     | Calculate the vertical profile of all blobs.                       |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateWeberContrast``                       | Calculate the Weber contrast of all blobs.                         |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``CalculateWidth``                               | Calculate the width value of all blobs.                            |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``GetEnumerator``                                |                                                                    |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``Add``                                          |                                                                    |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``Add``                                          |                                                                    |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``Clear``                                        |                                                                    |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``Contains``                                     |                                                                    |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``Remove``                                       |                                                                    |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``IndexOf``                                      |                                                                    |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``Insert``                                       |                                                                    |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``Insert``                                       |                                                                    |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``RemoveAt``                                     |                                                                    |
+--------------------------------------------------+--------------------------------------------------------------------+
| ``ToString``                                     |                                                                    |
+--------------------------------------------------+--------------------------------------------------------------------+

Description
~~~~~~~~~~~

This list can be used to calculate features of all blobs in the list. This calculation is parallelized to utilize the available processing cores. It is a list.

Variants
~~~~~~~~

Variant *BlobFeatures*
^^^^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **BlobFeatures** has the following types:

+------------------------------+
| Type                         |
+==============================+
| ``BlobFeaturesByte``         |
+------------------------------+
| ``BlobFeaturesUInt16``       |
+------------------------------+
| ``BlobFeaturesUInt32``       |
+------------------------------+
| ``BlobFeaturesDouble``       |
+------------------------------+
| ``BlobFeaturesRgbByte``      |
+------------------------------+
| ``BlobFeaturesRgbUInt16``    |
+------------------------------+
| ``BlobFeaturesRgbUInt32``    |
+------------------------------+
| ``BlobFeaturesRgbDouble``    |
+------------------------------+
| ``BlobFeaturesRgbaByte``     |
+------------------------------+
| ``BlobFeaturesRgbaUInt16``   |
+------------------------------+
| ``BlobFeaturesRgbaUInt32``   |
+------------------------------+
| ``BlobFeaturesRgbaDouble``   |
+------------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *BlobFeaturesList*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``BlobFeaturesList()``

Create an empty blob list.

Constructors
~~~~~~~~~~~~

Constructor *BlobFeaturesList*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``BlobFeaturesList(RegionList regions)``

Create a blob\_list from a region list.

The constructor has the following parameters:

+---------------+------------------+---------------------------------+
| Parameter     | Type             | Description                     |
+===============+==================+=================================+
| ``regions``   | ``RegionList``   | The region list to copy from.   |
+---------------+------------------+---------------------------------+

Constructor *BlobFeaturesList*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``BlobFeaturesList(RegionList regions, Variant~view pixels)``

Create a blob\_list from an image view and a region list.

The constructor has the following parameters:

+---------------+--------------------+---------------------------+
| Parameter     | Type               | Description               |
+===============+====================+===========================+
| ``regions``   | ``RegionList``     | The region list.          |
+---------------+--------------------+---------------------------+
| ``pixels``    | ``Variant~view``   | The view to the pixels.   |
+---------------+--------------------+---------------------------+

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``System.Object [index]``

Methods
~~~~~~~

Method *Translate*
^^^^^^^^^^^^^^^^^^

``BlobFeaturesList Translate(VectorInt32 translation)``

Translate a blob\_features\_list.

The method **Translate** has the following parameters:

+-------------------+-------------------+---------------------------+
| Parameter         | Type              | Description               |
+===================+===================+===========================+
| ``translation``   | ``VectorInt32``   | The translation vector.   |
+-------------------+-------------------+---------------------------+

This function returns a translated copy of a blob\_features list. Each blob\_features object in the list is translated by the same vector.

The translated blob\_features list.

Method *CalculateArea*
^^^^^^^^^^^^^^^^^^^^^^

``void CalculateArea()``

Calculate the area of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateAreaOfHoles*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateAreaOfHoles()``

Calculate the area of holes of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateAspectRatio*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateAspectRatio()``

Calculate the aspect ratio value of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateBottom*
^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateBottom()``

Calculate the bottom value of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateBoundingRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateBoundingRectangle()``

Calculate the bounding rectangle of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateBounds*
^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateBounds()``

Calculate the bounds of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateRegionCentralMoments*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateRegionCentralMoments()``

Calculate the region-based central moments of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateRegionCentroid*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateRegionCentroid()``

Calculate the region-based centroid of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateCircularity*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateCircularity()``

Calculate the circularity of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateChannelMaximum*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateChannelMaximum()``

Calculate the channel maximum of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateChannelMinimum*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateChannelMinimum()``

Calculate the channel minimum of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateCompactness*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateCompactness()``

Calculate the compactness of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateContrast*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateContrast()``

Calculate the contrast of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateConvexArea*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateConvexArea()``

Calculate the convex area of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateConvexHull*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateConvexHull()``

Calculate the convex hull of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateConvexity*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateConvexity()``

Calculate the convexity of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateConvexPerimeter*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateConvexPerimeter()``

Calculate the convex perimeter of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateRegionEquivalentEllipse*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateRegionEquivalentEllipse()``

Calculate the region-based equivalent ellipse of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateFiberLength*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateFiberLength()``

Calculate the fiber length of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateFiberWidth*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateFiberWidth()``

Calculate the fiber width of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateFirstPoint*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateFirstPoint()``

Calculate the first\_point of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateRegionFlusserMoments*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateRegionFlusserMoments()``

Calculate the region-based flusser moments of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateHeight*
^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateHeight()``

Calculate the height value of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateHistogram*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateHistogram()``

Calculate the histogram of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateHoles*
^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateHoles()``

Calculate the holes of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateHorizontalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateHorizontalProfile()``

Calculate the horizontal profile of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateRegionHuMoments*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateRegionHuMoments()``

Calculate the region-based hu moments of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateInnerContour*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateInnerContour()``

Calculate the inner contour of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateInnerContourPerimeter*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateInnerContourPerimeter()``

Calculate the inner contour perimeter of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateLeft*
^^^^^^^^^^^^^^^^^^^^^^

``void CalculateLeft()``

Calculate the left value of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateMaximum*
^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateMaximum()``

Calculate the maximum of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateMaximumFeretDiameter*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateMaximumFeretDiameter()``

Calculate the maximum feret diameter of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateMaximumPosition*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateMaximumPosition()``

Calculate the maximum position of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateMean*
^^^^^^^^^^^^^^^^^^^^^^

``void CalculateMean()``

Calculate the mean of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateMichelsonContrast*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateMichelsonContrast()``

Calculate the Michelson contrast of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateMinimum*
^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateMinimum()``

Calculate the minimum of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateMinimumAreaBoundingRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateMinimumAreaBoundingRectangle()``

Calculate the minimum area bounding rectangle of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateMinimumBoundingCircle*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateMinimumBoundingCircle()``

Calculate the minimum bounding circle of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateMinimumFeretDiameter*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateMinimumFeretDiameter()``

Calculate the minimum feret diameter of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateMinimumPerimeterBoundingRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateMinimumPerimeterBoundingRectangle()``

Calculate the minimum perimeter bounding rectangle of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateMinimumPosition*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateMinimumPosition()``

Calculate the minimum position of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateRegionRawMoments*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateRegionRawMoments()``

Calculate the region-based raw moments of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateRegionNormalizedMoments*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateRegionNormalizedMoments()``

Calculate the region-based normalized moments of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateNumberOfHoles*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateNumberOfHoles()``

Calculate the number of holes of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateOuterContour*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateOuterContour()``

Calculate the outer contour of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateOuterContourPerimeter*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateOuterContourPerimeter()``

Calculate the outer contour perimeter of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculatePerforation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculatePerforation()``

Calculate the perforation of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculatePerimeter*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculatePerimeter()``

Calculate the perimeter of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculatePixelCentralMoments*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculatePixelCentralMoments()``

Calculate the pixel-based central moments of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculatePixelCentroid*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculatePixelCentroid()``

Calculate the pixel-based centroid of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculatePixelEquivalentEllipse*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculatePixelEquivalentEllipse()``

Calculate the pixel-based equivalent ellipse of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculatePixelFlusserMoments*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculatePixelFlusserMoments()``

Calculate the pixel-based flusser moments of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculatePixelHuMoments*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculatePixelHuMoments()``

Calculate the pixel-based hu moments of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculatePixelRawMoments*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculatePixelRawMoments()``

Calculate the pixel-based raw moments of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculatePixelNormalizedMoments*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculatePixelNormalizedMoments()``

Calculate the pixel-based normalized moments of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculatePixelScaleInvariantMoments*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculatePixelScaleInvariantMoments()``

Calculate the pixel-based scale invariant moments of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateRight*
^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateRight()``

Calculate the right value of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateRoughness*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateRoughness()``

Calculate the roughness of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateRoundness*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateRoundness()``

Calculate the roundness of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateRegionScaleInvariantMoments*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateRegionScaleInvariantMoments()``

Calculate the region-based scale invariant moments of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateSkewness*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateSkewness()``

Calculate the skewness of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateSphericity*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateSphericity()``

Calculate the sphericity of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateStandardDeviation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateStandardDeviation()``

Calculate the standard deviation of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateTop*
^^^^^^^^^^^^^^^^^^^^^

``void CalculateTop()``

Calculate the top value of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateTotal*
^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateTotal()``

Calculate the total of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateVerticalProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateVerticalProfile()``

Calculate the vertical profile of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateWeberContrast*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateWeberContrast()``

Calculate the Weber contrast of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *CalculateWidth*
^^^^^^^^^^^^^^^^^^^^^^^

``void CalculateWidth()``

Calculate the width value of all blobs.

The calculation is carried out in parallel and makes use of all CPU cores.

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``BlobFeaturesEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(System.Object item)``

The method **Add** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``item``    | ``System.Object``   |               |
+-------------+---------------------+---------------+

Method *Add*
^^^^^^^^^^^^

``void Add(BlobFeaturesList list)``

The method **Add** has the following parameters:

+-------------+------------------------+---------------+
| Parameter   | Type                   | Description   |
+=============+========================+===============+
| ``list``    | ``BlobFeaturesList``   |               |
+-------------+------------------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(System.Object item)``

The method **Contains** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``item``    | ``System.Object``   |               |
+-------------+---------------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(System.Object item)``

The method **Remove** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``item``    | ``System.Object``   |               |
+-------------+---------------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(System.Object item)``

The method **IndexOf** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``item``    | ``System.Object``   |               |
+-------------+---------------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, System.Object item)``

The method **Insert** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``index``   | ``System.Int32``    |               |
+-------------+---------------------+---------------+
| ``item``    | ``System.Object``   |               |
+-------------+---------------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, BlobFeaturesList list)``

The method **Insert** has the following parameters:

+-------------+------------------------+---------------+
| Parameter   | Type                   | Description   |
+=============+========================+===============+
| ``index``   | ``System.Int32``       |               |
+-------------+------------------------+---------------+
| ``list``    | ``BlobFeaturesList``   |               |
+-------------+------------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
