Class *Index*
-------------

A two-dimensional, zero-based index into a buffer.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Index** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``IEquatableIndex``          |
+------------------------------+
| ``ISerializable``            |
+------------------------------+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **Index** contains the following properties:

+------------+-------+-------+----------------------------------+
| Property   | Get   | Set   | Description                      |
+============+=======+=======+==================================+
| ``X``      | \*    | \*    | The horizontal index position.   |
+------------+-------+-------+----------------------------------+
| ``Y``      | \*    | \*    | The vertical index position.     |
+------------+-------+-------+----------------------------------+

The class **Index** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

This is used to specify a position within a buffer. The position is zero- based, i.e. the leftmost horizontal position is 0 and the topmost vertical position is 0.

Together with an extent, this is used to specify a three-dimensional, rectangular subset of a buffer.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Index*
^^^^^^^^^^^^^^^^^^^

``Index()``

Constructs default index (0, 0).

Constructors
~~~~~~~~~~~~

Constructor *Index*
^^^^^^^^^^^^^^^^^^^

``Index(System.Int32 x)``

Constructs specific index.

The constructor has the following parameters:

+-------------+--------------------+------------------------+
| Parameter   | Type               | Description            |
+=============+====================+========================+
| ``x``       | ``System.Int32``   | The horizontal part.   |
+-------------+--------------------+------------------------+

Constructor *Index*
^^^^^^^^^^^^^^^^^^^

``Index(System.Int32 x, System.Int32 y)``

Constructs specific index from point (conversion).

The constructor has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Properties
~~~~~~~~~~

Property *X*
^^^^^^^^^^^^

``System.Int32 X``

The horizontal index position.

Property *Y*
^^^^^^^^^^^^

``System.Int32 Y``

The vertical index position.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO no brief description for variant

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO no description for variant
