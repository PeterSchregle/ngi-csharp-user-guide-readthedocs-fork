Introduction
============

**nGI** is a library for machine vision development. The main use is to create automated machine vision applications that are used in the industry. It can be used with C# in a Microsoft .Net programming environment.

**nGI** helps you with a variety of vision based tasks needed in the industry:

-  Check the position of a part to guide a handling systems or to put a vision tool in the right position.
-  Identify a part based on marks, shape or other visual aspects.
-  Verify a part to check if it has been built or assembled correctly.
-  Measure a parts dimensions.
-  Inspect a part for defects.

Structure
---------

**nGI** provides its vision functionality in a modular way. The following modules are available:

+---------------------------+-----------------------------------------------------------------------------------------------------+
| Module                    | Description                                                                                         |
+===========================+=====================================================================================================+
| **Image Processing**      | Contains basic image processing functionality, both monochrome and color.                           |
+---------------------------+-----------------------------------------------------------------------------------------------------+
| **Camera**                | Contains support of GenICam based cameras and framegrabbers.                                        |
+---------------------------+-----------------------------------------------------------------------------------------------------+
| **Analysis, Measuring**   | Contains image analysis functionality, blob analysis, camera calibration, gauging measurement.      |
+---------------------------+-----------------------------------------------------------------------------------------------------+
| **Template Matching**     | Contains the matching and searching functionality, template matching, geometric pattern matching.   |
+---------------------------+-----------------------------------------------------------------------------------------------------+
| **Barcode, Matrixcode**   | Contains the barcode reading functionality, both linear and two dimensional symbologies.            |
+---------------------------+-----------------------------------------------------------------------------------------------------+
| **OCR, OCV**              | Contains the Optical Character Recognition and Optical Character Verification functionality.        |
+---------------------------+-----------------------------------------------------------------------------------------------------+
| **CAD, PDF**              | Contains functions to read CAD and PDF files and convert them into images.                          |
+---------------------------+-----------------------------------------------------------------------------------------------------+

Requirements
------------

**nGI** runs on Windows 7 or higher, 64 bit (recommended) or 32 bit (memory limitations apply). It supports many cameras of different vendors, via the GigE Vision, USB3 Vision and GenICam standards.

Licensing
---------

**nGI** (Designer and Runtime) is licensed to the specific hardware. In order to run **nGI**, you need a license key.

TODO

When **nVision** is started for the first time, it needs to be activated. In addition to the license key, activation needs a hardware key, which is determined automatically. The activation process uses the he hardware key, together with the license key and creates an activation key. It is the activation key - in combination with the two other keys - that actually unlocks the **nVision** software.

Usually, activation works through an internet connection and is simple and transparent. If you copy your license key to the clipboard before you start **nVision** for the very first time, you will hardly notice the process. Otherwise, a dialog such as the following will be displayed:

.. figure:: images/nVisionLicensing1.png
   :alt: The **nVision** licensing dialog.

   The **nVision** licensing dialog.

 

In order to continue, you should enter your license key and press the Return key. **nGI** will contact the licensing server and use the returned activation key to unlock. If the license server cannot be connected for some reason, you can phone (+49 8245 7749600) or write an email to info@impuls-imaging.com to tell us both your license key and the hardware key, and we will provide you with an activation key that you can enter manually.

Activation is perpetual for a specific hardware. If the hardware changes, this is considered a new hardware, which means that you have to activate **nGI** again, and therefore need a new hardware key.

Camera and Device Interfaces
----------------------------

The interfaces to cameras used in **nGI** are the **GigEVision** and the **USB3 Vision** interfaces. These interfaces are standardized. Image data can be accessed via **GenTL** and camera properties can be read and written via **GenICam**.

For cameras interfaced via **GenTL** and **GenICam**, the full set of properties is accessible from within **nVision**.

TODO The camera properties are accessible in the form of a properties window.

.. figure:: images/camera_properties.png
   :alt: The camera properties window.

   The camera properties window.

 

In addition, the camera properties can be read and written from within a graphical pipeline, i.e. the properties can be changed from within a program.

.. figure:: images/camera_properties_pipeline.png
   :alt: Writing camera properties from within a pipeline.

   Writing camera properties from within a pipeline.

 

Secondary interfaces to cameras and devices exist via manufacturer SDKs for historical reasons. USB2 cameras are interfaced this way, since they were available before standardization, as well as interfaces to frame grabbers.

The following table lists tested devices:

+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Manufacturer         |             | URL                                                               | Device                  | Interface                           |
+======================+=============+===================================================================+=========================+=====================================+
| Allied Vision        | |image0|    | `www.alliedvision.com <http://www.alliedvision.com/>`__           | GigE Cameras            | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Basler               | |image1|    | `www.baslerweb.com <http://www.baslerweb.com/>`__                 | GigE Cameras            | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Baumer               | |image2|    | `www.baumer.com <http://www.baumer.com/>`__                       | GigE / USB3 Cameras     | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Flir                 | |image3|    | `www.flir.com <http://www.flir.com/>`__                           | GigE Cameras (A315)     | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| IDS                  | |image4|    | `www.ids-imaging.com <http://www.ids-imaging.com/>`__             | GigE Cameras            | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| JAI                  | |image5|    | `www.jai.com <http://www.jai.com/>`__                             | GigE / USB3 Cameras     | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Gardasoft            | |image6|    | `www.gardasoft.com <http://www.gardasoft.com/>`__                 | GigE Flash Controller   | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Matrix Vision        | |image7|    | `www.matrix-vision.com <http://www.matrix-vision.com/>`__         | GigE /USB3 Cameras      | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Point Grey           | |image8|    | `www.ptgrey.com <http://www.ptgrey.com/>`__                       | GigE / USB3 Cameras     | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Sentech              |             | `www.sentecheurope.com <http://www.sentecheurope.com/>`__         | GigE / USB3 Cameras     | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Smartek              | |image9|    | `www.smartek.vision <http://www.smartek.vision/>`__               | GigE Cameras            | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| SVS Vistek           | |image10|   | `www.svs-vistek.com <http://www.svs-vistek.com/>`__               | GigE Cameras            | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| The Imaging Source   | |image11|   | `www.theimagingsource.com <http://www.theimagingsource.com/>`__   | GigE Cameras            | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Various              | |image12|   |                                                                   | Webcams                 | Direct (Windows Media Foundation)   |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+
| Ximea                | |image13|   | `www.ximea.com <http://www.ximea.com/>`__                         | USB3 Cameras            | GenTL / GenICam                     |
+----------------------+-------------+-------------------------------------------------------------------+-------------------------+-------------------------------------+

All other **GigE Vision** or **USB3 Vision** capable cameras should work with **nGI** right our of the box, but have not been tested in-house so far.

.. |image0| image:: images/allied_vision_logo.png
.. |image1| image:: images/basler_logo.png
.. |image2| image:: images/baumer_logo.png
.. |image3| image:: images/flir_logo.png
.. |image4| image:: images/ids_logo.png
.. |image5| image:: images/jai_logo.png
.. |image6| image:: images/gardasoft_logo.png
.. |image7| image:: images/mv_logo.png
.. |image8| image:: images/point_grey_logo.png
.. |image9| image:: images/smartek_logo.png
.. |image10| image:: images/svs_vistek_logo.png
.. |image11| image:: images/the_imaging_source_logo.png
.. |image12| image:: images/windows_media_foundation_logo.png
.. |image13| image:: images/ximea_logo.png

