Class *UnknownException*
------------------------

Exception class for unknown exceptions.

**Namespace:** Ngi

**Module:**

Description
~~~~~~~~~~~

Unknown exceptions are used in the ngi\_native\_lib, when a catch(...) clause causes an unknown exception to be caught. In this case, it is transformed into a ngi::lv::unknown\_exception, so that it can be caught in upper layers.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *UnknownException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``UnknownException()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *UnknownException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``UnknownException(System.String message)``

Construct an exception from a message.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+

Constructor *UnknownException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``UnknownException(System.String message, System.UInt32 id)``

Construct an exception from a message and an id.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+
| ``id``        | ``System.UInt32``   | The exception id.        |
+---------------+---------------------+--------------------------+
