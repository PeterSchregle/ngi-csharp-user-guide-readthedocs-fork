Class *Polyline*
----------------

A polyline in two-dimensional euclidean space.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Polyline** implements the following interfaces:

+---------------------------------------------------+
| Interface                                         |
+===================================================+
| ``IEquatablePolylinePolylineCoordinateVariant``   |
+---------------------------------------------------+
| ``ISerializable``                                 |
+---------------------------------------------------+
| ``INotifyPropertyChanged``                        |
+---------------------------------------------------+

The class **Polyline** contains the following variant parameters:

+------------------+-----------------------------------------+
| Variant          | Description                             |
+==================+=========================================+
| ``Coordinate``   | TODO no brief description for variant   |
+------------------+-----------------------------------------+

The class **Polyline** contains the following properties:

+-----------------------------------------+-------+-------+---------------------------------------------------------------------------+
| Property                                | Get   | Set   | Description                                                               |
+=========================================+=======+=======+===========================================================================+
| ``Points``                              | \*    | \*    | The vertices of the polyline.                                             |
+-----------------------------------------+-------+-------+---------------------------------------------------------------------------+
| ``BoundingBox``                         | \*    |       | The bounding box of the polyline.                                         |
+-----------------------------------------+-------+-------+---------------------------------------------------------------------------+
| ``ConvexHull``                          | \*    |       | Calculate the convex hull.                                                |
+-----------------------------------------+-------+-------+---------------------------------------------------------------------------+
| ``MinimumWidth``                        | \*    |       | Calculate the minimum width of a convex polygon.                          |
+-----------------------------------------+-------+-------+---------------------------------------------------------------------------+
| ``MaximumWidth``                        | \*    |       | Calculate the maximum width of a convex polygon.                          |
+-----------------------------------------+-------+-------+---------------------------------------------------------------------------+
| ``MinimumAreaBoundingRectangle``        | \*    |       | Calculate the minimum area bounding rectangle of a convex polygon.        |
+-----------------------------------------+-------+-------+---------------------------------------------------------------------------+
| ``MinimumPerimeterBoundingRectangle``   | \*    |       | Calculate the minimum perimeter bounding rectangle of a convex polygon.   |
+-----------------------------------------+-------+-------+---------------------------------------------------------------------------+
| ``MinimumBoundingCircle``               | \*    |       | Calculate the minimum bounding circle of a convex polygon.                |
+-----------------------------------------+-------+-------+---------------------------------------------------------------------------+
| ``Perimeter``                           | \*    |       | Calculate the perimeter of a polygon.                                     |
+-----------------------------------------+-------+-------+---------------------------------------------------------------------------+
| ``Area``                                | \*    |       | Calculate the area of a polygon.                                          |
+-----------------------------------------+-------+-------+---------------------------------------------------------------------------+

The class **Polyline** contains the following methods:

+------------------------------+--------------------------------------------------------------+
| Method                       | Description                                                  |
+==============================+==============================================================+
| ``Translate``                | Move polyline.                                               |
+------------------------------+--------------------------------------------------------------+
| ``SimplifyRadialDistance``   | Simplify the polyline using the radial distance algorithm.   |
+------------------------------+--------------------------------------------------------------+
| ``ToString``                 | Provide string representation for debugging purposes.        |
+------------------------------+--------------------------------------------------------------+

Description
~~~~~~~~~~~

A polyline can be seen as sequence of connected line\_segments. The vertices of a polyline are stored in the points collection.

The following operations are implemented: operator == : comparison for equality. operator != : comparison for inequality.

Variants
~~~~~~~~

Variant *Coordinate*
^^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Coordinate** has the following types:

+--------------+
| Type         |
+==============+
| ``Int32``    |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Polyline*
^^^^^^^^^^^^^^^^^^^^^^

``Polyline()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *Polyline*
^^^^^^^^^^^^^^^^^^^^^^

``Polyline(Coordinate geometry)``

Construct a polyline from a point.

The constructor has the following parameters:

+----------------+------------------+---------------+
| Parameter      | Type             | Description   |
+================+==================+===============+
| ``geometry``   | ``Coordinate``   |               |
+----------------+------------------+---------------+

Constructor *Polyline*
^^^^^^^^^^^^^^^^^^^^^^

``Polyline(PointList points)``

Construct a polyline from a point\_list.

The constructor has the following parameters:

+--------------+-----------------+---------------+
| Parameter    | Type            | Description   |
+==============+=================+===============+
| ``points``   | ``PointList``   |               |
+--------------+-----------------+---------------+

Constructor *Polyline*
^^^^^^^^^^^^^^^^^^^^^^

``Polyline(Coordinate geometry)``

Construct a polyline from a line segment.

The constructor has the following parameters:

+----------------+------------------+---------------+
| Parameter      | Type             | Description   |
+================+==================+===============+
| ``geometry``   | ``Coordinate``   |               |
+----------------+------------------+---------------+

Constructor *Polyline*
^^^^^^^^^^^^^^^^^^^^^^

``Polyline(Coordinate geometry)``

Construct a polyline from a triangle.

The constructor has the following parameters:

+----------------+------------------+---------------+
| Parameter      | Type             | Description   |
+================+==================+===============+
| ``geometry``   | ``Coordinate``   |               |
+----------------+------------------+---------------+

Constructor *Polyline*
^^^^^^^^^^^^^^^^^^^^^^

``Polyline(Coordinate geometry)``

Construct a polyline from a quadrilateral.

The constructor has the following parameters:

+----------------+------------------+---------------+
| Parameter      | Type             | Description   |
+================+==================+===============+
| ``geometry``   | ``Coordinate``   |               |
+----------------+------------------+---------------+

Constructor *Polyline*
^^^^^^^^^^^^^^^^^^^^^^

``Polyline(Box geometry)``

Construct a polyline from a box.

The constructor has the following parameters:

+----------------+-----------+---------------+
| Parameter      | Type      | Description   |
+================+===========+===============+
| ``geometry``   | ``Box``   |               |
+----------------+-----------+---------------+

Constructor *Polyline*
^^^^^^^^^^^^^^^^^^^^^^

``Polyline(Rectangle geometry)``

Construct a polyline from a rectangle.

The constructor has the following parameters:

+----------------+-----------------+---------------+
| Parameter      | Type            | Description   |
+================+=================+===============+
| ``geometry``   | ``Rectangle``   |               |
+----------------+-----------------+---------------+

Constructor *Polyline*
^^^^^^^^^^^^^^^^^^^^^^

``Polyline(Circle geometry)``

Construct a polyline from a circle.

The constructor has the following parameters:

+----------------+--------------+---------------+
| Parameter      | Type         | Description   |
+================+==============+===============+
| ``geometry``   | ``Circle``   |               |
+----------------+--------------+---------------+

Constructor *Polyline*
^^^^^^^^^^^^^^^^^^^^^^

``Polyline(Ellipse geometry)``

Construct a polyline from an ellipse.

The constructor has the following parameters:

+----------------+---------------+---------------+
| Parameter      | Type          | Description   |
+================+===============+===============+
| ``geometry``   | ``Ellipse``   |               |
+----------------+---------------+---------------+

Properties
~~~~~~~~~~

Property *Points*
^^^^^^^^^^^^^^^^^

``PointList Points``

The vertices of the polyline.

Property *BoundingBox*
^^^^^^^^^^^^^^^^^^^^^^

``Box BoundingBox``

The bounding box of the polyline.

The bounding box is an axis aligned box that bounds the polyline.

Property *ConvexHull*
^^^^^^^^^^^^^^^^^^^^^

``Polyline ConvexHull``

Calculate the convex hull.

The convex hull is a pre-requisite for a number of other algorithms, such as the determination of feret diameters, etc.

The convex hull is returned in the form of a clockwise oriented polyline starting with the leftmost point. The term clockwise is chosen with respect to a right-hand coordinate system. Since nGI uses a left hand coordinate system the returned point list is actually oriented counter-clockwise.

This function also handles corner cases. If an empty polyline is passed in an empty polyline is returned. If a polyline consisting of one point is passed in, a copy of this polyline is returned. If a polyline consisting of two or three points is passed in, the returned polyline will contain these two or three points, potentially in different order.

Property *MinimumWidth*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Double MinimumWidth``

Calculate the minimum width of a convex polygon.

Property *MaximumWidth*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Double MaximumWidth``

Calculate the maximum width of a convex polygon.

Property *MinimumAreaBoundingRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Rectangle MinimumAreaBoundingRectangle``

Calculate the minimum area bounding rectangle of a convex polygon.

Property *MinimumPerimeterBoundingRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Rectangle MinimumPerimeterBoundingRectangle``

Calculate the minimum perimeter bounding rectangle of a convex polygon.

Property *MinimumBoundingCircle*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Circle MinimumBoundingCircle``

Calculate the minimum bounding circle of a convex polygon.

Property *Perimeter*
^^^^^^^^^^^^^^^^^^^^

``System.Double Perimeter``

Calculate the perimeter of a polygon.

Property *Area*
^^^^^^^^^^^^^^^

``System.Double Area``

Calculate the area of a polygon.

See https://192.168.176.33:8443/trac/ngi/attachment/wiki/Papers%3A/On%20the%20Calculation%20of%20Arbitraty%20Moments%20of%20Polygons.pdf

Methods
~~~~~~~

Method *Translate*
^^^^^^^^^^^^^^^^^^

``Polyline Translate(Vector offset)``

Move polyline.

The method **Translate** has the following parameters:

+--------------+--------------+--------------------+
| Parameter    | Type         | Description        |
+==============+==============+====================+
| ``offset``   | ``Vector``   | Movement vector.   |
+--------------+--------------+--------------------+

A reference to this object to be able to chain movements.

Method *SimplifyRadialDistance*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Polyline SimplifyRadialDistance(System.Double tolerance, System.Boolean treatAsClosedPolygon)``

Simplify the polyline using the radial distance algorithm.

The method **SimplifyRadialDistance** has the following parameters:

+----------------------------+----------------------+-------------------------------------------+
| Parameter                  | Type                 | Description                               |
+============================+======================+===========================================+
| ``tolerance``              | ``System.Double``    | Radial distance tolerance.                |
+----------------------------+----------------------+-------------------------------------------+
| ``treatAsClosedPolygon``   | ``System.Boolean``   | Treat the polyline as a closed polygon.   |
+----------------------------+----------------------+-------------------------------------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO documentation missing

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO documentation missing
