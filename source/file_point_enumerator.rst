Class *PointEnumerator*
-----------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **PointEnumerator** implements the following interfaces:

+----------------------------------------------+
| Interface                                    |
+==============================================+
| ``IEnumerator``                              |
+----------------------------------------------+
| ``IEnumeratorPointEnumeratorPointVariant``   |
+----------------------------------------------+

The class **PointEnumerator** contains the following variant parameters:

+-------------+-----------------------------------------+
| Variant     | Description                             |
+=============+=========================================+
| ``Point``   | TODO no brief description for variant   |
+-------------+-----------------------------------------+

The class **PointEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **PointEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Point*
^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Point** has the following types:

+-------------------+
| Type              |
+===================+
| ``PointInt32``    |
+-------------------+
| ``PointDouble``   |
+-------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Point Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
