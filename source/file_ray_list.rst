Class *RayList*
---------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **RayList** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``IListRayListRayVariant``   |
+------------------------------+

The class **RayList** contains the following variant parameters:

+-----------+-----------------------------------------+
| Variant   | Description                             |
+===========+=========================================+
| ``Ray``   | TODO no brief description for variant   |
+-----------+-----------------------------------------+

The class **RayList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **RayList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Ray*
^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Ray** has the following types:

+-----------------+
| Type            |
+=================+
| ``RayInt32``    |
+-----------------+
| ``RayDouble``   |
+-----------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *RayList*
^^^^^^^^^^^^^^^^^^^^^

``RayList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``Ray [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``RayEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(Ray item)``

The method **Add** has the following parameters:

+-------------+-----------+---------------+
| Parameter   | Type      | Description   |
+=============+===========+===============+
| ``item``    | ``Ray``   |               |
+-------------+-----------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(Ray item)``

The method **Contains** has the following parameters:

+-------------+-----------+---------------+
| Parameter   | Type      | Description   |
+=============+===========+===============+
| ``item``    | ``Ray``   |               |
+-------------+-----------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(Ray item)``

The method **Remove** has the following parameters:

+-------------+-----------+---------------+
| Parameter   | Type      | Description   |
+=============+===========+===============+
| ``item``    | ``Ray``   |               |
+-------------+-----------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(Ray item)``

The method **IndexOf** has the following parameters:

+-------------+-----------+---------------+
| Parameter   | Type      | Description   |
+=============+===========+===============+
| ``item``    | ``Ray``   |               |
+-------------+-----------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, Ray item)``

The method **Insert** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``item``    | ``Ray``            |               |
+-------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
