Class *TcpipClient*
-------------------

The TCP/IP client.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **TcpipClient** contains the following properties:

+------------+--------+--------+----------------------------------------------------+
| Property   | Get    | Set    | Description                                        |
+============+========+========+====================================================+
| ``Server`` | \*     |        | The server name (such as www.impuls-imaging.com,   |
|            |        |        | 81.169.145.169, localhost, 127.0.0.1, etc.) Any    |
|            |        |        | IPV4 or IPV6 adress can be used.                   |
+------------+--------+--------+----------------------------------------------------+
| ``Port``   | \*     |        | The port or service name (such as 21, http, etc.)  |
|            |        |        | Possible values for the service name are listed in |
|            |        |        | the file WINDIR%.                                  |
+------------+--------+--------+----------------------------------------------------+
| ``Family`` | \*     |        | The address family.                                |
+------------+--------+--------+----------------------------------------------------+
| ``BufferSi | \*     |        | Read and write buffer size.                        |
| ze``       |        |        |                                                    |
+------------+--------+--------+----------------------------------------------------+
| ``ReadTime | \*     |        | Read timeout [ms].                                 |
| out``      |        |        |                                                    |
+------------+--------+--------+----------------------------------------------------+
| ``HostName | \*     |        | Retrieves the name of this client.                 |
| ``         |        |        |                                                    |
+------------+--------+--------+----------------------------------------------------+
| ``PeerName | \*     |        | Retrieves the name of the server to which this     |
| ``         |        |        | client is connected.                               |
+------------+--------+--------+----------------------------------------------------+

The class **TcpipClient** contains the following enumerations:

+---------------------+------------------------------+
| Enumeration         | Description                  |
+=====================+==============================+
| ``AddressFamily``   | TODO documentation missing   |
+---------------------+------------------------------+

Description
~~~~~~~~~~~

The TCP/IP client is one endpoint of a socket connection. The tcpip\_client specifies the other side of the connection with a server address, a server port and the address family to be used.

The server must listen on the specific port, otherwise the connection cannot be made.

Constructors
~~~~~~~~~~~~

Constructor *TcpipClient*
^^^^^^^^^^^^^^^^^^^^^^^^^

``TcpipClient(System.String server, System.String port, TcpipClient.AddressFamily family, System.Int32 bufferSize, System.UInt32 readTimeout)``

Construct the TCP/IP client.

The constructor has the following parameters:

+--------------+------------+----------------------------------------------------+
| Parameter    | Type       | Description                                        |
+==============+============+====================================================+
| ``server``   | ``System.S | The server name (such as www.impuls-imaging.com,   |
|              | tring``    | 81.169.145.169, localhost, 127.0.0.1, etc.) Any    |
|              |            | IPV4 or IPV6 can be used.                          |
+--------------+------------+----------------------------------------------------+
| ``port``     | ``System.S | The port or service name (such as 21, http, etc.)  |
|              | tring``    | Possible values for the service name are listed in |
|              |            | the file WINDIR%.                                  |
+--------------+------------+----------------------------------------------------+
| ``family``   | ``TcpipCli | The address family.                                |
|              | ent.Addres |                                                    |
|              | sFamily``  |                                                    |
+--------------+------------+----------------------------------------------------+
| ``bufferSize | ``System.I | The size of the read and write buffers.            |
| ``           | nt32``     |                                                    |
+--------------+------------+----------------------------------------------------+
| ``readTimeou | ``System.U |                                                    |
| t``          | Int32``    |                                                    |
+--------------+------------+----------------------------------------------------+

Properties
~~~~~~~~~~

Property *Server*
^^^^^^^^^^^^^^^^^

``System.String Server``

The server name (such as www.impuls-imaging.com, 81.169.145.169, localhost, 127.0.0.1, etc.) Any IPV4 or IPV6 adress can be used.

Property *Port*
^^^^^^^^^^^^^^^

``System.String Port``

The port or service name (such as 21, http, etc.) Possible values for the service name are listed in the file WINDIR%.

Property *Family*
^^^^^^^^^^^^^^^^^

``TcpipClient.AddressFamily Family``

The address family.

Property *BufferSize*
^^^^^^^^^^^^^^^^^^^^^

``System.Int32 BufferSize``

Read and write buffer size.

Property *ReadTimeout*
^^^^^^^^^^^^^^^^^^^^^^

``System.UInt32 ReadTimeout``

Read timeout [ms].

Property *HostName*
^^^^^^^^^^^^^^^^^^^

``System.String HostName``

Retrieves the name of this client.

Property *PeerName*
^^^^^^^^^^^^^^^^^^^

``System.String PeerName``

Retrieves the name of the server to which this client is connected.

Enumerations
~~~~~~~~~~~~

Enumeration *AddressFamily*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum AddressFamily``

TODO documentation missing

The enumeration **AddressFamily** has the following constants:

+-------------------+---------+---------------+
| Name              | Value   | Description   |
+===================+=========+===============+
| ``unspecified``   | ``0``   |               |
+-------------------+---------+---------------+
| ``ipv4``          | ``1``   |               |
+-------------------+---------+---------------+
| ``ipv6``          | ``2``   |               |
+-------------------+---------+---------------+

::

    enum AddressFamily
    {
      unspecified = 0,
      ipv4 = 1,
      ipv6 = 2,
    };

TODO documentation missing
