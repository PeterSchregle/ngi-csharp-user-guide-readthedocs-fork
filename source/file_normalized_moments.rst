Class *NormalizedMoments*
-------------------------

Class to hold normalized moments of the zeroth up to the third order.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **NormalizedMoments** implements the following interfaces:

+-----------------------------------+
| Interface                         |
+===================================+
| ``IEquatableNormalizedMoments``   |
+-----------------------------------+
| ``ISerializable``                 |
+-----------------------------------+

The class **NormalizedMoments** contains the following properties:

+----------------+-------+-------+----------------------------------------------------+
| Property       | Get   | Set   | Description                                        |
+================+=======+=======+====================================================+
| ``N10``        | \*    |       | The horizontal first order moment.                 |
+----------------+-------+-------+----------------------------------------------------+
| ``N01``        | \*    |       | The vertical first order moment.                   |
+----------------+-------+-------+----------------------------------------------------+
| ``N20``        | \*    |       | The horizontal second order moment.                |
+----------------+-------+-------+----------------------------------------------------+
| ``N11``        | \*    |       | The mixed second order moment.                     |
+----------------+-------+-------+----------------------------------------------------+
| ``N02``        | \*    |       | The vertical second order moment.                  |
+----------------+-------+-------+----------------------------------------------------+
| ``N30``        | \*    |       | The horizontal third order moment.                 |
+----------------+-------+-------+----------------------------------------------------+
| ``N21``        | \*    |       | The first (horizontal) mixed third order moment.   |
+----------------+-------+-------+----------------------------------------------------+
| ``N12``        | \*    |       | The second (vertical) mixed third order moment.    |
+----------------+-------+-------+----------------------------------------------------+
| ``N03``        | \*    |       | The vertical third order moment.                   |
+----------------+-------+-------+----------------------------------------------------+
| ``Centroid``   | \*    |       | The centroid derived from the moments.             |
+----------------+-------+-------+----------------------------------------------------+

The class **NormalizedMoments** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

For a nice and short treatment of moments see Chaur-Chin Chen, Improved Moment Invariants for Shape Discrimination [1992].

Moments are a sort of weighted averages of the image pixels' intensities (the pixel intensities may be set to 1 in case of binary moments). They are useful to describe segmented objects. Simple properties of objects found via moments include area (or total intensity), its centroid, and information about its orientation.

Normalized moments are invariant with respect to scaling, but are not invariant with respect to translation and rotation.

The zeroth order central moment is not contained in this class, because they it is 1 by definition.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *NormalizedMoments*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``NormalizedMoments()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *NormalizedMoments*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``NormalizedMoments(Moments m)``

Standard constructor.

The constructor has the following parameters:

+-------------+---------------+---------------------------+
| Parameter   | Type          | Description               |
+=============+===============+===========================+
| ``m``       | ``Moments``   | Non-normalized moments.   |
+-------------+---------------+---------------------------+

Initializes the normalized moments by calculating it from non-normalized moments.

Properties
~~~~~~~~~~

Property *N10*
^^^^^^^^^^^^^^

``System.Double N10``

The horizontal first order moment.

Property *N01*
^^^^^^^^^^^^^^

``System.Double N01``

The vertical first order moment.

Property *N20*
^^^^^^^^^^^^^^

``System.Double N20``

The horizontal second order moment.

Property *N11*
^^^^^^^^^^^^^^

``System.Double N11``

The mixed second order moment.

Property *N02*
^^^^^^^^^^^^^^

``System.Double N02``

The vertical second order moment.

Property *N30*
^^^^^^^^^^^^^^

``System.Double N30``

The horizontal third order moment.

Property *N21*
^^^^^^^^^^^^^^

``System.Double N21``

The first (horizontal) mixed third order moment.

Property *N12*
^^^^^^^^^^^^^^

``System.Double N12``

The second (vertical) mixed third order moment.

Property *N03*
^^^^^^^^^^^^^^

``System.Double N03``

The vertical third order moment.

Property *Centroid*
^^^^^^^^^^^^^^^^^^^

``PointDouble Centroid``

The centroid derived from the moments.

The centroid is calculated by the following formulas: xc = m10 / m00 and yc = m01 / m00.

Static Methods
~~~~~~~~~~~~~~

Method *CalculateN10*
^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateN10(Moments m)``

Calculate n10 from moments.

The method **CalculateN10** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``m``       | ``Moments``   |               |
+-------------+---------------+---------------+

n10 is is a first order normalized moment.

The n10 normalized moment.

Method *CalculateN01*
^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateN01(Moments m)``

Calculate n01 from moments.

The method **CalculateN01** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``m``       | ``Moments``   |               |
+-------------+---------------+---------------+

n01 is is a first order normalized moment.

The n01 normalized moment.

Method *CalculateN20*
^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateN20(Moments m)``

Calculate n20 from moments.

The method **CalculateN20** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``m``       | ``Moments``   |               |
+-------------+---------------+---------------+

n20 is is a second order normalized moment.

The n20 normalized moment.

Method *CalculateN11*
^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateN11(Moments m)``

Calculate n11 from moments.

The method **CalculateN11** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``m``       | ``Moments``   |               |
+-------------+---------------+---------------+

n11 is is a second order normalized moment.

The n11 normalized moment.

Method *CalculateN02*
^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateN02(Moments m)``

Calculate n02 from moments.

The method **CalculateN02** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``m``       | ``Moments``   |               |
+-------------+---------------+---------------+

n02 is is a second order normalized moment.

The n02 normalized moment.

Method *CalculateN30*
^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateN30(Moments m)``

Calculate n30 from moments.

The method **CalculateN30** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``m``       | ``Moments``   |               |
+-------------+---------------+---------------+

n30 is is a third order normalized moment.

The n30 normalized moment.

Method *CalculateN21*
^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateN21(Moments m)``

Calculate n21 from moments.

The method **CalculateN21** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``m``       | ``Moments``   |               |
+-------------+---------------+---------------+

n21 is is a third order normalized moment.

The n21 normalized moment.

Method *CalculateN12*
^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateN12(Moments m)``

Calculate n12 from moments.

The method **CalculateN12** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``m``       | ``Moments``   |               |
+-------------+---------------+---------------+

n12 is is a third order normalized moment.

The n12 normalized moment.

Method *CalculateN03*
^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateN03(Moments m)``

Calculate n03 from moments.

The method **CalculateN03** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``m``       | ``Moments``   |               |
+-------------+---------------+---------------+

n03 is is a third order normalized moment.

The n03 normalized moment.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
