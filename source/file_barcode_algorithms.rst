Class *BarcodeAlgorithms*
-------------------------

Wraps free-standing functions for barcode decoding.

**Namespace:** Ngi

**Module:** BarcodeMatrixcode

Description
~~~~~~~~~~~

The purpose of this class is to make the functions available for the .NET wrapper.

Static Methods
~~~~~~~~~~~~~~

Method *MvizDecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``MvizBarcode MvizDecodeBarcode(ViewLocatorByte source, MvizBarcodeDecoder configuration)``

The method **MvizDecodeBarcode** has the following parameters:

+---------------------+--------------------------+---------------+
| Parameter           | Type                     | Description   |
+=====================+==========================+===============+
| ``source``          | ``ViewLocatorByte``      |               |
+---------------------+--------------------------+---------------+
| ``configuration``   | ``MvizBarcodeDecoder``   |               |
+---------------------+--------------------------+---------------+

Method *MvizDecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``MvizBarcode MvizDecodeBarcode(ViewLocatorUInt16 source, MvizBarcodeDecoder configuration)``

The method **MvizDecodeBarcode** has the following parameters:

+---------------------+--------------------------+---------------+
| Parameter           | Type                     | Description   |
+=====================+==========================+===============+
| ``source``          | ``ViewLocatorUInt16``    |               |
+---------------------+--------------------------+---------------+
| ``configuration``   | ``MvizBarcodeDecoder``   |               |
+---------------------+--------------------------+---------------+

Method *MvizDecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``MvizBarcode MvizDecodeBarcode(ViewLocatorUInt32 source, MvizBarcodeDecoder configuration)``

The method **MvizDecodeBarcode** has the following parameters:

+---------------------+--------------------------+---------------+
| Parameter           | Type                     | Description   |
+=====================+==========================+===============+
| ``source``          | ``ViewLocatorUInt32``    |               |
+---------------------+--------------------------+---------------+
| ``configuration``   | ``MvizBarcodeDecoder``   |               |
+---------------------+--------------------------+---------------+

Method *MvizDecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``MvizBarcode MvizDecodeBarcode(ViewLocatorRgbByte source, MvizBarcodeDecoder configuration)``

The method **MvizDecodeBarcode** has the following parameters:

+---------------------+--------------------------+---------------+
| Parameter           | Type                     | Description   |
+=====================+==========================+===============+
| ``source``          | ``ViewLocatorRgbByte``   |               |
+---------------------+--------------------------+---------------+
| ``configuration``   | ``MvizBarcodeDecoder``   |               |
+---------------------+--------------------------+---------------+

Method *MvizDecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``MvizBarcode MvizDecodeBarcode(ViewLocatorRgbaByte source, MvizBarcodeDecoder configuration)``

The method **MvizDecodeBarcode** has the following parameters:

+---------------------+---------------------------+---------------+
| Parameter           | Type                      | Description   |
+=====================+===========================+===============+
| ``source``          | ``ViewLocatorRgbaByte``   |               |
+---------------------+---------------------------+---------------+
| ``configuration``   | ``MvizBarcodeDecoder``    |               |
+---------------------+---------------------------+---------------+

Method *MvizDecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``MvizBarcode MvizDecodeBarcode(View source, MvizBarcodeDecoder configuration)``

The method **MvizDecodeBarcode** has the following parameters:

+---------------------+--------------------------+---------------+
| Parameter           | Type                     | Description   |
+=====================+==========================+===============+
| ``source``          | ``View``                 |               |
+---------------------+--------------------------+---------------+
| ``configuration``   | ``MvizBarcodeDecoder``   |               |
+---------------------+--------------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(ViewLocatorByte source, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+-----------------------+---------------+
| Parameter           | Type                  | Description   |
+=====================+=======================+===============+
| ``source``          | ``ViewLocatorByte``   |               |
+---------------------+-----------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``    |               |
+---------------------+-----------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(ViewLocatorUInt16 source, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+-------------------------+---------------+
| Parameter           | Type                    | Description   |
+=====================+=========================+===============+
| ``source``          | ``ViewLocatorUInt16``   |               |
+---------------------+-------------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``      |               |
+---------------------+-------------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(ViewLocatorUInt32 source, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+-------------------------+---------------+
| Parameter           | Type                    | Description   |
+=====================+=========================+===============+
| ``source``          | ``ViewLocatorUInt32``   |               |
+---------------------+-------------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``      |               |
+---------------------+-------------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(ViewLocatorDouble source, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+-------------------------+---------------+
| Parameter           | Type                    | Description   |
+=====================+=========================+===============+
| ``source``          | ``ViewLocatorDouble``   |               |
+---------------------+-------------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``      |               |
+---------------------+-------------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(ViewLocatorRgbByte source, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+--------------------------+---------------+
| Parameter           | Type                     | Description   |
+=====================+==========================+===============+
| ``source``          | ``ViewLocatorRgbByte``   |               |
+---------------------+--------------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``       |               |
+---------------------+--------------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(ViewLocatorRgbUInt16 source, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+----------------------------+---------------+
| Parameter           | Type                       | Description   |
+=====================+============================+===============+
| ``source``          | ``ViewLocatorRgbUInt16``   |               |
+---------------------+----------------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``         |               |
+---------------------+----------------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(ViewLocatorRgbUInt32 source, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+----------------------------+---------------+
| Parameter           | Type                       | Description   |
+=====================+============================+===============+
| ``source``          | ``ViewLocatorRgbUInt32``   |               |
+---------------------+----------------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``         |               |
+---------------------+----------------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(ViewLocatorRgbDouble source, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+----------------------------+---------------+
| Parameter           | Type                       | Description   |
+=====================+============================+===============+
| ``source``          | ``ViewLocatorRgbDouble``   |               |
+---------------------+----------------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``         |               |
+---------------------+----------------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(ViewLocatorRgbaByte source, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+---------------------------+---------------+
| Parameter           | Type                      | Description   |
+=====================+===========================+===============+
| ``source``          | ``ViewLocatorRgbaByte``   |               |
+---------------------+---------------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``        |               |
+---------------------+---------------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(ViewLocatorRgbaUInt16 source, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+-----------------------------+---------------+
| Parameter           | Type                        | Description   |
+=====================+=============================+===============+
| ``source``          | ``ViewLocatorRgbaUInt16``   |               |
+---------------------+-----------------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``          |               |
+---------------------+-----------------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(ViewLocatorRgbaUInt32 source, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+-----------------------------+---------------+
| Parameter           | Type                        | Description   |
+=====================+=============================+===============+
| ``source``          | ``ViewLocatorRgbaUInt32``   |               |
+---------------------+-----------------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``          |               |
+---------------------+-----------------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(ViewLocatorRgbaDouble source, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+-----------------------------+---------------+
| Parameter           | Type                        | Description   |
+=====================+=============================+===============+
| ``source``          | ``ViewLocatorRgbaDouble``   |               |
+---------------------+-----------------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``          |               |
+---------------------+-----------------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(View source, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+----------------------+---------------+
| Parameter           | Type                 | Description   |
+=====================+======================+===============+
| ``source``          | ``View``             |               |
+---------------------+----------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``   |               |
+---------------------+----------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(ViewLocatorByte source, Region region, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+-----------------------+---------------+
| Parameter           | Type                  | Description   |
+=====================+=======================+===============+
| ``source``          | ``ViewLocatorByte``   |               |
+---------------------+-----------------------+---------------+
| ``region``          | ``Region``            |               |
+---------------------+-----------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``    |               |
+---------------------+-----------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(ViewLocatorUInt16 source, Region region, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+-------------------------+---------------+
| Parameter           | Type                    | Description   |
+=====================+=========================+===============+
| ``source``          | ``ViewLocatorUInt16``   |               |
+---------------------+-------------------------+---------------+
| ``region``          | ``Region``              |               |
+---------------------+-------------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``      |               |
+---------------------+-------------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(ViewLocatorUInt32 source, Region region, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+-------------------------+---------------+
| Parameter           | Type                    | Description   |
+=====================+=========================+===============+
| ``source``          | ``ViewLocatorUInt32``   |               |
+---------------------+-------------------------+---------------+
| ``region``          | ``Region``              |               |
+---------------------+-------------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``      |               |
+---------------------+-------------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(ViewLocatorDouble source, Region region, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+-------------------------+---------------+
| Parameter           | Type                    | Description   |
+=====================+=========================+===============+
| ``source``          | ``ViewLocatorDouble``   |               |
+---------------------+-------------------------+---------------+
| ``region``          | ``Region``              |               |
+---------------------+-------------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``      |               |
+---------------------+-------------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(ViewLocatorRgbByte source, Region region, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+--------------------------+---------------+
| Parameter           | Type                     | Description   |
+=====================+==========================+===============+
| ``source``          | ``ViewLocatorRgbByte``   |               |
+---------------------+--------------------------+---------------+
| ``region``          | ``Region``               |               |
+---------------------+--------------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``       |               |
+---------------------+--------------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(ViewLocatorRgbUInt16 source, Region region, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+----------------------------+---------------+
| Parameter           | Type                       | Description   |
+=====================+============================+===============+
| ``source``          | ``ViewLocatorRgbUInt16``   |               |
+---------------------+----------------------------+---------------+
| ``region``          | ``Region``                 |               |
+---------------------+----------------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``         |               |
+---------------------+----------------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(ViewLocatorRgbUInt32 source, Region region, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+----------------------------+---------------+
| Parameter           | Type                       | Description   |
+=====================+============================+===============+
| ``source``          | ``ViewLocatorRgbUInt32``   |               |
+---------------------+----------------------------+---------------+
| ``region``          | ``Region``                 |               |
+---------------------+----------------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``         |               |
+---------------------+----------------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(ViewLocatorRgbDouble source, Region region, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+----------------------------+---------------+
| Parameter           | Type                       | Description   |
+=====================+============================+===============+
| ``source``          | ``ViewLocatorRgbDouble``   |               |
+---------------------+----------------------------+---------------+
| ``region``          | ``Region``                 |               |
+---------------------+----------------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``         |               |
+---------------------+----------------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(ViewLocatorRgbaByte source, Region region, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+---------------------------+---------------+
| Parameter           | Type                      | Description   |
+=====================+===========================+===============+
| ``source``          | ``ViewLocatorRgbaByte``   |               |
+---------------------+---------------------------+---------------+
| ``region``          | ``Region``                |               |
+---------------------+---------------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``        |               |
+---------------------+---------------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(ViewLocatorRgbaUInt16 source, Region region, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+-----------------------------+---------------+
| Parameter           | Type                        | Description   |
+=====================+=============================+===============+
| ``source``          | ``ViewLocatorRgbaUInt16``   |               |
+---------------------+-----------------------------+---------------+
| ``region``          | ``Region``                  |               |
+---------------------+-----------------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``          |               |
+---------------------+-----------------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(ViewLocatorRgbaUInt32 source, Region region, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+-----------------------------+---------------+
| Parameter           | Type                        | Description   |
+=====================+=============================+===============+
| ``source``          | ``ViewLocatorRgbaUInt32``   |               |
+---------------------+-----------------------------+---------------+
| ``region``          | ``Region``                  |               |
+---------------------+-----------------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``          |               |
+---------------------+-----------------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(ViewLocatorRgbaDouble source, Region region, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+-----------------------------+---------------+
| Parameter           | Type                        | Description   |
+=====================+=============================+===============+
| ``source``          | ``ViewLocatorRgbaDouble``   |               |
+---------------------+-----------------------------+---------------+
| ``region``          | ``Region``                  |               |
+---------------------+-----------------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``          |               |
+---------------------+-----------------------------+---------------+

Method *DecodeBarcode*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList DecodeBarcode(View source, Region region, BarcodeDecoder configuration)``

The method **DecodeBarcode** has the following parameters:

+---------------------+----------------------+---------------+
| Parameter           | Type                 | Description   |
+=====================+======================+===============+
| ``source``          | ``View``             |               |
+---------------------+----------------------+---------------+
| ``region``          | ``Region``           |               |
+---------------------+----------------------+---------------+
| ``configuration``   | ``BarcodeDecoder``   |               |
+---------------------+----------------------+---------------+
