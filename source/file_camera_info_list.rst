Class *CameraInfoList*
----------------------

**Namespace:** Ngi

**Module:** Camera

The class **CameraInfoList** implements the following interfaces:

+-----------------------+
| Interface             |
+=======================+
| ``IListCameraInfo``   |
+-----------------------+

The class **CameraInfoList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **CameraInfoList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *CameraInfoList*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``CameraInfoList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``CameraInfo [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``CameraInfoEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(CameraInfo item)``

The method **Add** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``item``    | ``CameraInfo``   |               |
+-------------+------------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(CameraInfo item)``

The method **Contains** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``item``    | ``CameraInfo``   |               |
+-------------+------------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(CameraInfo item)``

The method **Remove** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``item``    | ``CameraInfo``   |               |
+-------------+------------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(CameraInfo item)``

The method **IndexOf** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``item``    | ``CameraInfo``   |               |
+-------------+------------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, CameraInfo item)``

The method **Insert** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``item``    | ``CameraInfo``     |               |
+-------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
