Class *OcrCharacterInfoEnumerator*
----------------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **OcrCharacterInfoEnumerator** implements the following interfaces:

+-----------------------------------+
| Interface                         |
+===================================+
| ``IEnumerator``                   |
+-----------------------------------+
| ``IEnumeratorOcrCharacterInfo``   |
+-----------------------------------+

The class **OcrCharacterInfoEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **OcrCharacterInfoEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``OcrCharacterInfo Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
