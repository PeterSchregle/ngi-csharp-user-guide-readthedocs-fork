Class *Calibration*
-------------------

Camera calibration functions.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Calibration** contains the following enumerations:

+---------------------------+------------------------------+
| Enumeration               | Description                  |
+===========================+==============================+
| ``DistanceOrientation``   | TODO documentation missing   |
+---------------------------+------------------------------+

Description
~~~~~~~~~~~

The class contains a group of functions that help with camera calibration.

Static Methods
~~~~~~~~~~~~~~

Method *SortOrthogonalCalibrationTargetCoordinates*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``OrderedPointsPointDouble SortOrthogonalCalibrationTargetCoordinates(PointListPointDouble imagePoints)``

This functions sorts the image coordinates of a calibration target in a row column fashion.

The method **SortOrthogonalCalibrationTargetCoordinates** has the following parameters:

+-------------------+----------------------------+---------------+
| Parameter         | Type                       | Description   |
+===================+============================+===============+
| ``imagePoints``   | ``PointListPointDouble``   |               |
+-------------------+----------------------------+---------------+

The coordinates of the calibration target are given in the image coordinate system, i.e. pixels, as determined by some measurement process. The function assumes that the coordinates are arranged in an orthogonal fashion.

The sorted coordinates of the calibration target image\_points.

Method *InterPointDistances*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``DoubleList InterPointDistances(OrderedPointsPointDouble orderedGrid, Calibration.DistanceOrientation orientation)``

This functions calculates the inter-point distances of an ordered calibration point grid.

The method **InterPointDistances** has the following parameters:

+-------------------+---------------------------------------+---------------+
| Parameter         | Type                                  | Description   |
+===================+=======================================+===============+
| ``orderedGrid``   | ``OrderedPointsPointDouble``          |               |
+-------------------+---------------------------------------+---------------+
| ``orientation``   | ``Calibration.DistanceOrientation``   |               |
+-------------------+---------------------------------------+---------------+

The distances can be calculate in four orientations: horizontal, diagonal upwards, vertical and diagonal downwards.

The inter-point distances of an ordered points grid.

Method *FindOrthogonalCalibrationTargetCoordinates*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``PointListPointDouble FindOrthogonalCalibrationTargetCoordinates(PointListPointDouble imagePoints, System.Double worldDistance)``

This functions associates the image coordinates of a calibration target to coordinates in the world.

The method **FindOrthogonalCalibrationTargetCoordinates** has the following parameters:

+---------------------+----------------------------+---------------+
| Parameter           | Type                       | Description   |
+=====================+============================+===============+
| ``imagePoints``     | ``PointListPointDouble``   |               |
+---------------------+----------------------------+---------------+
| ``worldDistance``   | ``System.Double``          |               |
+---------------------+----------------------------+---------------+

The coordinates of the calibration target are given in the image coordinate system, i.e. pixels, as determined by some measurement process. The function assumes that the coordinates are arranged in an orthogonal fashion and uses the given world\_distance to establish correspondent world coordinates. The origin of the world coordinate system is assumed to be in the center of the calibration target.

The correspondent world coordinates of the calibration target image\_points.

Method *AffineTransformationFromPasspoints*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``AffineMatrix AffineTransformationFromPasspoints(PointListPointDouble imageCoordinates, PointListPointDouble worldCoordinates)``

Calculates an affine transformation matrix from a set of matching world coordinates to image coordinates (passpoints).

The method **AffineTransformationFromPasspoints** has the following parameters:

+------------------------+----------------------------+---------------+
| Parameter              | Type                       | Description   |
+========================+============================+===============+
| ``imageCoordinates``   | ``PointListPointDouble``   |               |
+------------------------+----------------------------+---------------+
| ``worldCoordinates``   | ``PointListPointDouble``   |               |
+------------------------+----------------------------+---------------+

This function uses a the points ind the world and image coordinate systems to calculate an affine transformation matrix, that can be used to transform from image to world coordinates.

Depending on the number of point-pairs passed, different types of transformations are possible:

.. raw:: html

   <itemizedlist>

0 point pairs: the identity transformation is returned.

1 point pair: a matrix that performs a translation is returned.

2 point pairs: a matrix that performs a translation, rotation and scaling is returned.

3 or more point pairs: a matrix that performs a general affine transformation is returned.

.. raw:: html

   </itemizedlist>

Passing the same point multiple times, or passing points on a line only may internally result in a system of equations that is under-determined and cannot be solved properly. In this case, the function may throw a matrix\_exception with the matrix\_is\_rank\_deficient error id.

If more than 3 point pairs are passed, the function uses a least squares method to minimize the mean square error when calculating the transformation matrix.

There is no upper limit to the number of point pairs. However, if an excessive number is used, the processing time may increase.

The affine transformation matrix.

Method *PerspectiveTransformationFromPasspoints*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``PerspectiveMatrix PerspectiveTransformationFromPasspoints(PointListPointDouble imageCoordinates, PointListPointDouble worldCoordinates)``

Calculates a perspective transformation matrix from a set of matching point pairs (passpoints).

The method **PerspectiveTransformationFromPasspoints** has the following parameters:

+------------------------+----------------------------+---------------+
| Parameter              | Type                       | Description   |
+========================+============================+===============+
| ``imageCoordinates``   | ``PointListPointDouble``   |               |
+------------------------+----------------------------+---------------+
| ``worldCoordinates``   | ``PointListPointDouble``   |               |
+------------------------+----------------------------+---------------+

This function uses a sets of pairs of points defined within two scenes to calculate a perspective transformation matrix, that can be used to transform the source scene into the destination scene.

The function needs at least four point-pairs to calculate the perspective transformation matrix.

If less than four point-pairs are used, the function falls back to the affine\_transformation\_from\_passpoints method.

If four pairs of points are passed, you cannot use the same point pair twice, since internally this would result in a system of equations that is under-determined, and therefore cannot be solved properly. In this case, the function may throw a matrix\_exception with the matrix\_is\_rank\_deficient error id.

If more than four point pairs are passed, the function uses a method to minimize the mean square error when calculating the transformation matrix.

There is no upper limit to the number of point pairs. However, if an excessive number is used, the processing time may increase.

The perspective transformation matrix.

Method *SingleViewCameraCalibration*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``PerspectiveMatrix SingleViewCameraCalibration(PointListPointDouble imageCoordinates, PointListPointDouble worldCoordinates)``

Calculates the intrinsic matrix A of the camera and the first two coefficients of the radial distortion from a single image of the calibration target.

The method **SingleViewCameraCalibration** has the following parameters:

+------------------------+----------------------------+---------------+
| Parameter              | Type                       | Description   |
+========================+============================+===============+
| ``imageCoordinates``   | ``PointListPointDouble``   |               |
+------------------------+----------------------------+---------------+
| ``worldCoordinates``   | ``PointListPointDouble``   |               |
+------------------------+----------------------------+---------------+

The intrinsic matrix A is defined as: \| alpha gamma u0 \| A = \| 0 beta v0 \| \| 0 0 1 \|

in which:

alpha = f/px, where f is the focal length and px is the width of the pixels. beta = f/py, where f is the focal length and py is the height of the pixels. gamma = skewness of the two image axes. u0 = x coordinate of the principal point. v0 = y coordinate of the principal point.

The radial distortion is taken in account, for which the first two coefficients k1 and k2 are estimated.

The function implements the method proposed in: Hartley and Zisserman "Multiple view geometry in computer vision" , 2nd edition pp 223 - 226

The intrinsic matrix A of the camera and the first two coefficients of the radial distortion.

Enumerations
~~~~~~~~~~~~

Enumeration *DistanceOrientation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum DistanceOrientation``

TODO documentation missing

The enumeration **DistanceOrientation** has the following constants:

+--------------------+---------+---------------+
| Name               | Value   | Description   |
+====================+=========+===============+
| ``horizontal``     | ``0``   |               |
+--------------------+---------+---------------+
| ``diagonalUp``     | ``1``   |               |
+--------------------+---------+---------------+
| ``vertical``       | ``2``   |               |
+--------------------+---------+---------------+
| ``diagonalDown``   | ``3``   |               |
+--------------------+---------+---------------+

::

    enum DistanceOrientation
    {
      horizontal = 0,
      diagonalUp = 1,
      vertical = 2,
      diagonalDown = 3,
    };

TODO documentation missing
