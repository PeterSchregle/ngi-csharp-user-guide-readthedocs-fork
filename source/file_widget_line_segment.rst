Class *WidgetLineSegment*
-------------------------

A widget\_line\_segment displays a line segment on a surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetLineSegment** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetLineSegment** contains the following properties:

+----------------+-------+-------+-------------------------------+
| Property       | Get   | Set   | Description                   |
+================+=======+=======+===============================+
| ``Position``   | \*    | \*    | The position of the widget.   |
+----------------+-------+-------+-------------------------------+
| ``Pen``        | \*    | \*    | The pen of the widget.        |
+----------------+-------+-------+-------------------------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_line\_segment. These settings affect the look and the behavior of the widget\_line\_segment.

The widget\_line\_segment supports interactivity via the widget\_interactive base class, i.e. it can be selected, move around. In general it can react on mouse and keyboard interactions. This widget supports layout.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetLineSegment*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetLineSegment()``

Default constructor.

The default constructor creates a widget with default settings.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``LineSegmentDouble Position``

The position of the widget.

Property *Pen*
^^^^^^^^^^^^^^

``PenByte Pen``

The pen of the widget.
