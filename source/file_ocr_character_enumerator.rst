Class *OcrCharacterEnumerator*
------------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **OcrCharacterEnumerator** implements the following interfaces:

+-------------------------------+
| Interface                     |
+===============================+
| ``IEnumerator``               |
+-------------------------------+
| ``IEnumeratorOcrCharacter``   |
+-------------------------------+

The class **OcrCharacterEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **OcrCharacterEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``OcrCharacter Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
