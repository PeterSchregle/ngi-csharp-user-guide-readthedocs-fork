Class *Circle*
--------------

The geometric circle primitive.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Circle** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``IEquatableCircle``         |
+------------------------------+
| ``ISerializable``            |
+------------------------------+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **Circle** contains the following properties:

+---------------------+-------+-------+---------------------------------------+
| Property            | Get   | Set   | Description                           |
+=====================+=======+=======+=======================================+
| ``Center``          | \*    | \*    | The center of the circle.             |
+---------------------+-------+-------+---------------------------------------+
| ``Radius``          | \*    | \*    | The radius of the circle.             |
+---------------------+-------+-------+---------------------------------------+
| ``Diameter``        | \*    |       | The diameter of the circle.           |
+---------------------+-------+-------+---------------------------------------+
| ``Area``            | \*    |       | The area of the circle.               |
+---------------------+-------+-------+---------------------------------------+
| ``Circumference``   | \*    |       | The circumference of the circle.      |
+---------------------+-------+-------+---------------------------------------+
| ``BoundingBox``     | \*    |       | Get the bounding box of the circle.   |
+---------------------+-------+-------+---------------------------------------+

The class **Circle** contains the following methods:

+------------------------+------------------------------------------------------------+
| Method                 | Description                                                |
+========================+============================================================+
| ``PointOnOutline``     | Calculate a point on the circle outline, given an angle.   |
+------------------------+------------------------------------------------------------+
| ``ToString``           | Provide string representation for debugging purposes.      |
+------------------------+------------------------------------------------------------+
| ``Normalize``          | Normalize a circle.                                        |
+------------------------+------------------------------------------------------------+
| ``NormalizeInPlace``   | Normalize a circle.                                        |
+------------------------+------------------------------------------------------------+
| ``Contains``           |                                                            |
+------------------------+------------------------------------------------------------+

Description
~~~~~~~~~~~

A circle can be seen as a point at the center combined with a radius in two-dimensional space.

The following operations are implemented: operator == : comparison for equality. operator != : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Circle*
^^^^^^^^^^^^^^^^^^^^

``Circle()``

Default constructor.

The default constructor builds a circle with its center at the origin and a zero radius.

Constructors
~~~~~~~~~~~~

Constructor *Circle*
^^^^^^^^^^^^^^^^^^^^

``Circle(PointDouble center, System.Double radius)``

Standard constructor.

The constructor has the following parameters:

+--------------+---------------------+---------------+
| Parameter    | Type                | Description   |
+==============+=====================+===============+
| ``center``   | ``PointDouble``     | The center.   |
+--------------+---------------------+---------------+
| ``radius``   | ``System.Double``   | The radius.   |
+--------------+---------------------+---------------+

Construct a circle from its center and a radius.

Constructor *Circle*
^^^^^^^^^^^^^^^^^^^^

``Circle(PointDouble a, PointDouble b)``

Standard constructor.

The constructor has the following parameters:

+-------------+-------------------+---------------------+
| Parameter   | Type              | Description         |
+=============+===================+=====================+
| ``a``       | ``PointDouble``   | The first point.    |
+-------------+-------------------+---------------------+
| ``b``       | ``PointDouble``   | The second point.   |
+-------------+-------------------+---------------------+

Construct a circle from two points. In this case, the center is on the midpoint between the two points and the radius is half the distance between the two points.

Constructor *Circle*
^^^^^^^^^^^^^^^^^^^^

``Circle(PointDouble a, PointDouble b, PointDouble c)``

Standard constructor.

The constructor has the following parameters:

+-------------+-------------------+---------------------+
| Parameter   | Type              | Description         |
+=============+===================+=====================+
| ``a``       | ``PointDouble``   | The first point.    |
+-------------+-------------------+---------------------+
| ``b``       | ``PointDouble``   | The second point.   |
+-------------+-------------------+---------------------+
| ``c``       | ``PointDouble``   | The third point.    |
+-------------+-------------------+---------------------+

Construct a circle from three points. In this case, the center is on the midpoint between the two points and the radius is half the distance between the two points.

Properties
~~~~~~~~~~

Property *Center*
^^^^^^^^^^^^^^^^^

``PointDouble Center``

The center of the circle.

Property *Radius*
^^^^^^^^^^^^^^^^^

``System.Double Radius``

The radius of the circle.

Property *Diameter*
^^^^^^^^^^^^^^^^^^^

``System.Double Diameter``

The diameter of the circle.

Property *Area*
^^^^^^^^^^^^^^^

``System.Double Area``

The area of the circle.

Property *Circumference*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double Circumference``

The circumference of the circle.

Property *BoundingBox*
^^^^^^^^^^^^^^^^^^^^^^

``BoxDouble BoundingBox``

Get the bounding box of the circle.

The bounding box is an axis aligned box that bounds the circle.

The bounding box of the circle.

Methods
~~~~~~~

Method *PointOnOutline*
^^^^^^^^^^^^^^^^^^^^^^^

``PointDouble PointOnOutline(System.Double angle)``

Calculate a point on the circle outline, given an angle.

The method **PointOnOutline** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``angle``   | ``System.Double``   | The angle.    |
+-------------+---------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Method *Normalize*
^^^^^^^^^^^^^^^^^^

``Circle Normalize()``

Normalize a circle.

A normalized circle has a positive radius.

This method checks these conditions and returns a normalized variant of the circle.

Method *NormalizeInPlace*
^^^^^^^^^^^^^^^^^^^^^^^^^

``void NormalizeInPlace()``

Normalize a circle.

A normalized circle has a positive radius.

This method checks these conditions and returns a normalized variant of the circle. It modifies the circle in place.

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(PointDouble point)``

The method **Contains** has the following parameters:

+-------------+-------------------+---------------+
| Parameter   | Type              | Description   |
+=============+===================+===============+
| ``point``   | ``PointDouble``   |               |
+-------------+-------------------+---------------+

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO no brief description for variant

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO no description for variant
