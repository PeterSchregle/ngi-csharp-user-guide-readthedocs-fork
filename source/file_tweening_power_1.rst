Class *TweeningPower1*
----------------------

Calculating functor that calculates a power tweening.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **TweeningPower1** contains the following properties:

+-------------+-------+-------+---------------------+
| Property    | Get   | Set   | Description         |
+=============+=======+=======+=====================+
| ``Power``   | \*    |       | The kernel width.   |
+-------------+-------+-------+---------------------+

Description
~~~~~~~~~~~

The function used for the tweening is the power function between 0 and 1.

This function starts with a slope of 0 and ends with a slope of power-1.

This functor is used for the preset functions.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *TweeningPower1*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TweeningPower1()``

Default Constructor.

Constructors
~~~~~~~~~~~~

Constructor *TweeningPower1*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TweeningPower1(System.Double power)``

Constructor.

The constructor has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``power``   | ``System.Double``   | The power.    |
+-------------+---------------------+---------------+

Properties
~~~~~~~~~~

Property *Power*
^^^^^^^^^^^^^^^^

``System.Double Power``

The kernel width.
