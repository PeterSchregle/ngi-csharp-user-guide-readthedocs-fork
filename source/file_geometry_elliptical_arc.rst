Class *GeometryEllipticalArc*
-----------------------------

The following operations are implemented: operator== : comparison for equality.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **GeometryEllipticalArc** implements the following interfaces:

+------------------+
| Interface        |
+==================+
| ``IEquatable``   |
+------------------+

The class **GeometryEllipticalArc** contains the following properties:

+-----------------+-------+-------+------------------------------------------------+
| Property        | Get   | Set   | Description                                    |
+=================+=======+=======+================================================+
| ``Radius``      | \*    | \*    | The radius of the elliptical arc.              |
+-----------------+-------+-------+------------------------------------------------+
| ``Direction``   | \*    | \*    | The direction of the elliptical arc.           |
+-----------------+-------+-------+------------------------------------------------+
| ``Sweep``       | \*    | \*    | The drawing direction of the elliptical arc.   |
+-----------------+-------+-------+------------------------------------------------+
| ``Size``        | \*    | \*    | The size of an arc.                            |
+-----------------+-------+-------+------------------------------------------------+

The class **GeometryEllipticalArc** contains the following methods:

+-----------------------+---------------------------------------------------------+
| Method                | Description                                             |
+=======================+=========================================================+
| ``ToString``          | Provide string representation for debugging purposes.   |
+-----------------------+---------------------------------------------------------+
| ``ToEllipticalArc``   | Convert to a standalone elliptical arc.                 |
+-----------------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

operator!= : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *GeometryEllipticalArc*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``GeometryEllipticalArc()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *GeometryEllipticalArc*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``GeometryEllipticalArc(PointDouble point)``

Construct a default geometry element elliptical arc from a point.

The constructor has the following parameters:

+-------------+-------------------+---------------+
| Parameter   | Type              | Description   |
+=============+===================+===============+
| ``point``   | ``PointDouble``   |               |
+-------------+-------------------+---------------+

Constructor *GeometryEllipticalArc*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``GeometryEllipticalArc(PointDouble point, VectorDouble radius, Direction direction, EllipticalArc.SweepDirection sweep, EllipticalArc.ArcSize size)``

Construct a geometry element elliptical arc.

The constructor has the following parameters:

+-----------------+------------------------------------+------------------------+
| Parameter       | Type                               | Description            |
+=================+====================================+========================+
| ``point``       | ``PointDouble``                    | The target point.      |
+-----------------+------------------------------------+------------------------+
| ``radius``      | ``VectorDouble``                   | The radius.            |
+-----------------+------------------------------------+------------------------+
| ``direction``   | ``Direction``                      |                        |
+-----------------+------------------------------------+------------------------+
| ``sweep``       | ``EllipticalArc.SweepDirection``   | The sweep direction.   |
+-----------------+------------------------------------+------------------------+
| ``size``        | ``EllipticalArc.ArcSize``          | The arc size.          |
+-----------------+------------------------------------+------------------------+

Properties
~~~~~~~~~~

Property *Radius*
^^^^^^^^^^^^^^^^^

``VectorDouble Radius``

The radius of the elliptical arc.

Property *Direction*
^^^^^^^^^^^^^^^^^^^^

``Direction Direction``

The direction of the elliptical arc.

Property *Sweep*
^^^^^^^^^^^^^^^^

``EllipticalArc.SweepDirection Sweep``

The drawing direction of the elliptical arc.

Property *Size*
^^^^^^^^^^^^^^^

``EllipticalArc.ArcSize Size``

The size of an arc.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Method *ToEllipticalArc*
^^^^^^^^^^^^^^^^^^^^^^^^

``EllipticalArc ToEllipticalArc(PointDouble startPoint)``

Convert to a standalone elliptical arc.

The method **ToEllipticalArc** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``startPoint``   | ``PointDouble``   |               |
+------------------+-------------------+---------------+
