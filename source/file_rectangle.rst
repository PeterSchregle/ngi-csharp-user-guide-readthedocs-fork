Class *Rectangle*
-----------------

The geometric rectangle primitive.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Rectangle** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``IEquatableRectangle``      |
+------------------------------+
| ``ISerializable``            |
+------------------------------+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **Rectangle** contains the following properties:

+---------------------+-------+-------+--------------------------------------------+
| Property            | Get   | Set   | Description                                |
+=====================+=======+=======+============================================+
| ``Center``          | \*    | \*    | The center of the rectangle.               |
+---------------------+-------+-------+--------------------------------------------+
| ``HalfDiagonal``    | \*    | \*    | The half of the diagonal vector.           |
+---------------------+-------+-------+--------------------------------------------+
| ``Rotation``        | \*    | \*    | The rotation angle of the rectangle.       |
+---------------------+-------+-------+--------------------------------------------+
| ``Width``           | \*    |       | The width of the rectangle.                |
+---------------------+-------+-------+--------------------------------------------+
| ``Height``          | \*    |       | The height of the rectangle.               |
+---------------------+-------+-------+--------------------------------------------+
| ``TopLeft``         | \*    |       | The top-left point of the rectangle.       |
+---------------------+-------+-------+--------------------------------------------+
| ``TopRight``        | \*    |       | The top-right point of the rectangle.      |
+---------------------+-------+-------+--------------------------------------------+
| ``BottomRight``     | \*    |       | The bottom-right point of the rectangle.   |
+---------------------+-------+-------+--------------------------------------------+
| ``BottomLeft``      | \*    |       | The bottom-left point of the rectangle.    |
+---------------------+-------+-------+--------------------------------------------+
| ``BoundingBox``     | \*    |       | Get the bounding box of the rectangle.     |
+---------------------+-------+-------+--------------------------------------------+
| ``Area``            | \*    |       | The area of the rectangle.                 |
+---------------------+-------+-------+--------------------------------------------+
| ``Circumference``   | \*    |       | The circumference of the rectangle.        |
+---------------------+-------+-------+--------------------------------------------+

The class **Rectangle** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``Move``       | Move rectangle.                                         |
+----------------+---------------------------------------------------------+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

A rectangle is a quadrilateral with four right angles. It can be described using it's center point and it's diagonal vector centered on the center point, combined with a rotation angle.

The following operations are implemented: operator == : comparison for equality. operator != : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Rectangle*
^^^^^^^^^^^^^^^^^^^^^^^

``Rectangle()``

Default constructor.

Construct a rectangle from a center, a half-diagonal vector and a rotation angle.

Constructors
~~~~~~~~~~~~

Constructor *Rectangle*
^^^^^^^^^^^^^^^^^^^^^^^

``Rectangle(PointDouble center, VectorDouble halfDiagonal, Direction rotation)``

Constructor.

The constructor has the following parameters:

+--------------------+--------------------+---------------+
| Parameter          | Type               | Description   |
+====================+====================+===============+
| ``center``         | ``PointDouble``    |               |
+--------------------+--------------------+---------------+
| ``halfDiagonal``   | ``VectorDouble``   |               |
+--------------------+--------------------+---------------+
| ``rotation``       | ``Direction``      |               |
+--------------------+--------------------+---------------+

Construct a rectangle from a line segment and a thickness.

Constructor *Rectangle*
^^^^^^^^^^^^^^^^^^^^^^^

``Rectangle(LineSegmentDouble lineSegment, System.Double halfThickness)``

Constructor.

The constructor has the following parameters:

+---------------------+-------------------------+---------------+
| Parameter           | Type                    | Description   |
+=====================+=========================+===============+
| ``lineSegment``     | ``LineSegmentDouble``   |               |
+---------------------+-------------------------+---------------+
| ``halfThickness``   | ``System.Double``       |               |
+---------------------+-------------------------+---------------+

Construct a rectangle from a corner point, a width, a height and a rotation angle.

Constructor *Rectangle*
^^^^^^^^^^^^^^^^^^^^^^^

``Rectangle(PointDouble corner, System.Double width, System.Double height, Direction rotation)``

Constructor.

The constructor has the following parameters:

+----------------+---------------------+---------------+
| Parameter      | Type                | Description   |
+================+=====================+===============+
| ``corner``     | ``PointDouble``     |               |
+----------------+---------------------+---------------+
| ``width``      | ``System.Double``   |               |
+----------------+---------------------+---------------+
| ``height``     | ``System.Double``   |               |
+----------------+---------------------+---------------+
| ``rotation``   | ``Direction``       |               |
+----------------+---------------------+---------------+

Construct a rectangle from a box.

Constructor *Rectangle*
^^^^^^^^^^^^^^^^^^^^^^^

``Rectangle(BoxDouble box, System.Boolean vertical)``

Standard converting constructor.

The constructor has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``box``        | ``BoxDouble``        |               |
+----------------+----------------------+---------------+
| ``vertical``   | ``System.Boolean``   |               |
+----------------+----------------------+---------------+

Construct a rectangle from a center, a half-diagonal vector and a rotation angle.

Properties
~~~~~~~~~~

Property *Center*
^^^^^^^^^^^^^^^^^

``PointDouble Center``

The center of the rectangle.

Property *HalfDiagonal*
^^^^^^^^^^^^^^^^^^^^^^^

``VectorDouble HalfDiagonal``

The half of the diagonal vector.

Property *Rotation*
^^^^^^^^^^^^^^^^^^^

``Direction Rotation``

The rotation angle of the rectangle.

Property *Width*
^^^^^^^^^^^^^^^^

``System.Double Width``

The width of the rectangle.

Property *Height*
^^^^^^^^^^^^^^^^^

``System.Double Height``

The height of the rectangle.

Property *TopLeft*
^^^^^^^^^^^^^^^^^^

``PointDouble TopLeft``

The top-left point of the rectangle.

This point is the top-left point with respect to a rotation angle of 0, i.e. if the rectangle would be horizontal. If the rectangle has a different rotation angle, this point may actually lie somewhere else.

Property *TopRight*
^^^^^^^^^^^^^^^^^^^

``PointDouble TopRight``

The top-right point of the rectangle.

This point is the top-right point with respect to a rotation angle of 0, i.e. if the rectangle would be horizontal. If the rectangle has a different rotation angle, this point may actually lie somewhere else.

Property *BottomRight*
^^^^^^^^^^^^^^^^^^^^^^

``PointDouble BottomRight``

The bottom-right point of the rectangle.

This point is the bottom-right point with respect to a rotation angle of 0, i.e. if the rectangle would be horizontal. If the rectangle has a different rotation angle, this point may actually lie somewhere else.

Property *BottomLeft*
^^^^^^^^^^^^^^^^^^^^^

``PointDouble BottomLeft``

The bottom-left point of the rectangle.

This point is the bottom-left point with respect to a rotation angle of 0, i.e. if the rectangle would be horizontal. If the rectangle has a different rotation angle, this point may actually lie somewhere else.

Property *BoundingBox*
^^^^^^^^^^^^^^^^^^^^^^

``BoxDouble BoundingBox``

Get the bounding box of the rectangle.

The bounding box is an axis aligned box that bounds the rectangle.

The bounding box of the rectangle.

Property *Area*
^^^^^^^^^^^^^^^

``System.Double Area``

The area of the rectangle.

Property *Circumference*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double Circumference``

The circumference of the rectangle.

Methods
~~~~~~~

Method *Move*
^^^^^^^^^^^^^

``Rectangle Move(VectorDouble movement)``

Move rectangle.

The method **Move** has the following parameters:

+----------------+--------------------+--------------------+
| Parameter      | Type               | Description        |
+================+====================+====================+
| ``movement``   | ``VectorDouble``   | Movement vector.   |
+----------------+--------------------+--------------------+

Moves a rectangle by a vector.

The moved rectangle.

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO no brief description for variant

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO no description for variant
