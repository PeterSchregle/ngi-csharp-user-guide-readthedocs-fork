Class *CountToolGroupEnumerator*
--------------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **CountToolGroupEnumerator** implements the following interfaces:

+---------------------------------+
| Interface                       |
+=================================+
| ``IEnumerator``                 |
+---------------------------------+
| ``IEnumeratorCountToolGroup``   |
+---------------------------------+

The class **CountToolGroupEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **CountToolGroupEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``CountToolGroup Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
