Class *Edge2dInfoList*
----------------------

**Namespace:** Ngi

**Module:** AnalysisMeasuring

The class **Edge2dInfoList** implements the following interfaces:

+-----------------------+
| Interface             |
+=======================+
| ``IListEdge2dInfo``   |
+-----------------------+

The class **Edge2dInfoList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **Edge2dInfoList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Edge2dInfoList*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``Edge2dInfo [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(Edge2dInfo item)``

The method **Add** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``item``    | ``Edge2dInfo``   |               |
+-------------+------------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(Edge2dInfo item)``

The method **Contains** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``item``    | ``Edge2dInfo``   |               |
+-------------+------------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(Edge2dInfo item)``

The method **Remove** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``item``    | ``Edge2dInfo``   |               |
+-------------+------------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(Edge2dInfo item)``

The method **IndexOf** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``item``    | ``Edge2dInfo``   |               |
+-------------+------------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, Edge2dInfo item)``

The method **Insert** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``item``    | ``Edge2dInfo``     |               |
+-------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
