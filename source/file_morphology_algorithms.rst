Class *MorphologyAlgorithms*
----------------------------

Image morphology functions.

**Namespace:** Ngi

**Module:** ImageProcessing

Description
~~~~~~~~~~~

The class contains a group of image morphology functions. It includes erosion, dilation, opening, closing and morphological\_gradient functions.

Static Methods
~~~~~~~~~~~~~~

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageByte Erosion(ViewLocatorByte source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+-----------------------+---------------+
| Parameter                | Type                  | Description   |
+==========================+=======================+===============+
| ``source``               | ``ViewLocatorByte``   |               |
+--------------------------+-----------------------+---------------+
| ``regionOfInterest``     | ``Region``            |               |
+--------------------------+-----------------------+---------------+
| ``structuringElement``   | ``Region``            |               |
+--------------------------+-----------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageUInt16 Erosion(ViewLocatorUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorUInt16``   |               |
+--------------------------+-------------------------+---------------+
| ``regionOfInterest``     | ``Region``              |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageUInt32 Erosion(ViewLocatorUInt32 source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorUInt32``   |               |
+--------------------------+-------------------------+---------------+
| ``regionOfInterest``     | ``Region``              |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageDouble Erosion(ViewLocatorDouble source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorDouble``   |               |
+--------------------------+-------------------------+---------------+
| ``regionOfInterest``     | ``Region``              |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageRgbByte Erosion(ViewLocatorRgbByte source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorRgbByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageRgbUInt16 Erosion(ViewLocatorRgbUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageRgbUInt32 Erosion(ViewLocatorRgbUInt32 source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbUInt32``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageRgbDouble Erosion(ViewLocatorRgbDouble source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageRgbaByte Erosion(ViewLocatorRgbaByte source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+---------------------------+---------------+
| Parameter                | Type                      | Description   |
+==========================+===========================+===============+
| ``source``               | ``ViewLocatorRgbaByte``   |               |
+--------------------------+---------------------------+---------------+
| ``regionOfInterest``     | ``Region``                |               |
+--------------------------+---------------------------+---------------+
| ``structuringElement``   | ``Region``                |               |
+--------------------------+---------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Erosion(ViewLocatorRgbaUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaUInt16``   |               |
+--------------------------+-----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Erosion(ViewLocatorRgbaUInt32 source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaUInt32``   |               |
+--------------------------+-----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageRgbaDouble Erosion(ViewLocatorRgbaDouble source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaDouble``   |               |
+--------------------------+-----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageHlsByte Erosion(ViewLocatorHlsByte source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorHlsByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageHlsUInt16 Erosion(ViewLocatorHlsUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHlsUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageHlsDouble Erosion(ViewLocatorHlsDouble source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHlsDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageHsiByte Erosion(ViewLocatorHsiByte source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorHsiByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageHsiUInt16 Erosion(ViewLocatorHsiUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHsiUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageHsiDouble Erosion(ViewLocatorHsiDouble source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHsiDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageLabByte Erosion(ViewLocatorLabByte source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorLabByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageLabUInt16 Erosion(ViewLocatorLabUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorLabUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageLabDouble Erosion(ViewLocatorLabDouble source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorLabDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageXyzByte Erosion(ViewLocatorXyzByte source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorXyzByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageXyzUInt16 Erosion(ViewLocatorXyzUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorXyzUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageXyzDouble Erosion(ViewLocatorXyzDouble source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorXyzDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``Image Erosion(View source, Region regionOfInterest, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+--------------+---------------+
| Parameter                | Type         | Description   |
+==========================+==============+===============+
| ``source``               | ``View``     |               |
+--------------------------+--------------+---------------+
| ``regionOfInterest``     | ``Region``   |               |
+--------------------------+--------------+---------------+
| ``structuringElement``   | ``Region``   |               |
+--------------------------+--------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageByte Erosion(ViewLocatorByte source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+-----------------------+---------------+
| Parameter                | Type                  | Description   |
+==========================+=======================+===============+
| ``source``               | ``ViewLocatorByte``   |               |
+--------------------------+-----------------------+---------------+
| ``structuringElement``   | ``Region``            |               |
+--------------------------+-----------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageUInt16 Erosion(ViewLocatorUInt16 source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorUInt16``   |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageUInt32 Erosion(ViewLocatorUInt32 source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorUInt32``   |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageDouble Erosion(ViewLocatorDouble source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorDouble``   |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageRgbByte Erosion(ViewLocatorRgbByte source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorRgbByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageRgbUInt16 Erosion(ViewLocatorRgbUInt16 source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageRgbUInt32 Erosion(ViewLocatorRgbUInt32 source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbUInt32``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageRgbDouble Erosion(ViewLocatorRgbDouble source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageRgbaByte Erosion(ViewLocatorRgbaByte source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+---------------------------+---------------+
| Parameter                | Type                      | Description   |
+==========================+===========================+===============+
| ``source``               | ``ViewLocatorRgbaByte``   |               |
+--------------------------+---------------------------+---------------+
| ``structuringElement``   | ``Region``                |               |
+--------------------------+---------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Erosion(ViewLocatorRgbaUInt16 source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaUInt16``   |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Erosion(ViewLocatorRgbaUInt32 source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaUInt32``   |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageRgbaDouble Erosion(ViewLocatorRgbaDouble source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaDouble``   |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageHlsByte Erosion(ViewLocatorHlsByte source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorHlsByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageHlsUInt16 Erosion(ViewLocatorHlsUInt16 source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHlsUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageHlsDouble Erosion(ViewLocatorHlsDouble source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHlsDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageHsiByte Erosion(ViewLocatorHsiByte source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorHsiByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageHsiUInt16 Erosion(ViewLocatorHsiUInt16 source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHsiUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageHsiDouble Erosion(ViewLocatorHsiDouble source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHsiDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageLabByte Erosion(ViewLocatorLabByte source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorLabByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageLabUInt16 Erosion(ViewLocatorLabUInt16 source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorLabUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageLabDouble Erosion(ViewLocatorLabDouble source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorLabDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageXyzByte Erosion(ViewLocatorXyzByte source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorXyzByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageXyzUInt16 Erosion(ViewLocatorXyzUInt16 source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorXyzUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``ImageXyzDouble Erosion(ViewLocatorXyzDouble source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorXyzDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Erosion*
^^^^^^^^^^^^^^^^

``Image Erosion(View source, Region structuringElement)``

Perform a binary erosion.

The method **Erosion** has the following parameters:

+--------------------------+--------------+---------------+
| Parameter                | Type         | Description   |
+==========================+==============+===============+
| ``source``               | ``View``     |               |
+--------------------------+--------------+---------------+
| ``structuringElement``   | ``Region``   |               |
+--------------------------+--------------+---------------+

For each pixel in the source view, the erosion calculates the minimum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageByte Dilation(ViewLocatorByte source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+-----------------------+---------------+
| Parameter                | Type                  | Description   |
+==========================+=======================+===============+
| ``source``               | ``ViewLocatorByte``   |               |
+--------------------------+-----------------------+---------------+
| ``regionOfInterest``     | ``Region``            |               |
+--------------------------+-----------------------+---------------+
| ``structuringElement``   | ``Region``            |               |
+--------------------------+-----------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageUInt16 Dilation(ViewLocatorUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorUInt16``   |               |
+--------------------------+-------------------------+---------------+
| ``regionOfInterest``     | ``Region``              |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageUInt32 Dilation(ViewLocatorUInt32 source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorUInt32``   |               |
+--------------------------+-------------------------+---------------+
| ``regionOfInterest``     | ``Region``              |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageDouble Dilation(ViewLocatorDouble source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorDouble``   |               |
+--------------------------+-------------------------+---------------+
| ``regionOfInterest``     | ``Region``              |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageRgbByte Dilation(ViewLocatorRgbByte source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorRgbByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 Dilation(ViewLocatorRgbUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 Dilation(ViewLocatorRgbUInt32 source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbUInt32``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageRgbDouble Dilation(ViewLocatorRgbDouble source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageRgbaByte Dilation(ViewLocatorRgbaByte source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+---------------------------+---------------+
| Parameter                | Type                      | Description   |
+==========================+===========================+===============+
| ``source``               | ``ViewLocatorRgbaByte``   |               |
+--------------------------+---------------------------+---------------+
| ``regionOfInterest``     | ``Region``                |               |
+--------------------------+---------------------------+---------------+
| ``structuringElement``   | ``Region``                |               |
+--------------------------+---------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Dilation(ViewLocatorRgbaUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaUInt16``   |               |
+--------------------------+-----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Dilation(ViewLocatorRgbaUInt32 source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaUInt32``   |               |
+--------------------------+-----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageRgbaDouble Dilation(ViewLocatorRgbaDouble source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaDouble``   |               |
+--------------------------+-----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageHlsByte Dilation(ViewLocatorHlsByte source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorHlsByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 Dilation(ViewLocatorHlsUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHlsUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageHlsDouble Dilation(ViewLocatorHlsDouble source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHlsDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageHsiByte Dilation(ViewLocatorHsiByte source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorHsiByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 Dilation(ViewLocatorHsiUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHsiUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageHsiDouble Dilation(ViewLocatorHsiDouble source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHsiDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageLabByte Dilation(ViewLocatorLabByte source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorLabByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageLabUInt16 Dilation(ViewLocatorLabUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorLabUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageLabDouble Dilation(ViewLocatorLabDouble source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorLabDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageXyzByte Dilation(ViewLocatorXyzByte source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorXyzByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 Dilation(ViewLocatorXyzUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorXyzUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageXyzDouble Dilation(ViewLocatorXyzDouble source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorXyzDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``Image Dilation(View source, Region regionOfInterest, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+--------------+---------------+
| Parameter                | Type         | Description   |
+==========================+==============+===============+
| ``source``               | ``View``     |               |
+--------------------------+--------------+---------------+
| ``regionOfInterest``     | ``Region``   |               |
+--------------------------+--------------+---------------+
| ``structuringElement``   | ``Region``   |               |
+--------------------------+--------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageByte Dilation(ViewLocatorByte source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+-----------------------+---------------+
| Parameter                | Type                  | Description   |
+==========================+=======================+===============+
| ``source``               | ``ViewLocatorByte``   |               |
+--------------------------+-----------------------+---------------+
| ``structuringElement``   | ``Region``            |               |
+--------------------------+-----------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageUInt16 Dilation(ViewLocatorUInt16 source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorUInt16``   |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageUInt32 Dilation(ViewLocatorUInt32 source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorUInt32``   |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageDouble Dilation(ViewLocatorDouble source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorDouble``   |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageRgbByte Dilation(ViewLocatorRgbByte source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorRgbByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 Dilation(ViewLocatorRgbUInt16 source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 Dilation(ViewLocatorRgbUInt32 source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbUInt32``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageRgbDouble Dilation(ViewLocatorRgbDouble source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageRgbaByte Dilation(ViewLocatorRgbaByte source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+---------------------------+---------------+
| Parameter                | Type                      | Description   |
+==========================+===========================+===============+
| ``source``               | ``ViewLocatorRgbaByte``   |               |
+--------------------------+---------------------------+---------------+
| ``structuringElement``   | ``Region``                |               |
+--------------------------+---------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Dilation(ViewLocatorRgbaUInt16 source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaUInt16``   |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Dilation(ViewLocatorRgbaUInt32 source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaUInt32``   |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageRgbaDouble Dilation(ViewLocatorRgbaDouble source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaDouble``   |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageHlsByte Dilation(ViewLocatorHlsByte source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorHlsByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 Dilation(ViewLocatorHlsUInt16 source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHlsUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageHlsDouble Dilation(ViewLocatorHlsDouble source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHlsDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageHsiByte Dilation(ViewLocatorHsiByte source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorHsiByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 Dilation(ViewLocatorHsiUInt16 source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHsiUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageHsiDouble Dilation(ViewLocatorHsiDouble source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHsiDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageLabByte Dilation(ViewLocatorLabByte source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorLabByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageLabUInt16 Dilation(ViewLocatorLabUInt16 source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorLabUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageLabDouble Dilation(ViewLocatorLabDouble source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorLabDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageXyzByte Dilation(ViewLocatorXyzByte source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorXyzByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 Dilation(ViewLocatorXyzUInt16 source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorXyzUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``ImageXyzDouble Dilation(ViewLocatorXyzDouble source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorXyzDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Dilation*
^^^^^^^^^^^^^^^^^

``Image Dilation(View source, Region structuringElement)``

Perform a binary dilation.

The method **Dilation** has the following parameters:

+--------------------------+--------------+---------------+
| Parameter                | Type         | Description   |
+==========================+==============+===============+
| ``source``               | ``View``     |               |
+--------------------------+--------------+---------------+
| ``structuringElement``   | ``Region``   |               |
+--------------------------+--------------+---------------+

For each pixel in the source view, the erosion calculates the maximum within a neighborhood centered on the pixel and returns this as the result.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageByte Opening(ViewLocatorByte source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+-----------------------+---------------+
| Parameter                | Type                  | Description   |
+==========================+=======================+===============+
| ``source``               | ``ViewLocatorByte``   |               |
+--------------------------+-----------------------+---------------+
| ``regionOfInterest``     | ``Region``            |               |
+--------------------------+-----------------------+---------------+
| ``structuringElement``   | ``Region``            |               |
+--------------------------+-----------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageUInt16 Opening(ViewLocatorUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorUInt16``   |               |
+--------------------------+-------------------------+---------------+
| ``regionOfInterest``     | ``Region``              |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageUInt32 Opening(ViewLocatorUInt32 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorUInt32``   |               |
+--------------------------+-------------------------+---------------+
| ``regionOfInterest``     | ``Region``              |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageDouble Opening(ViewLocatorDouble source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorDouble``   |               |
+--------------------------+-------------------------+---------------+
| ``regionOfInterest``     | ``Region``              |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageRgbByte Opening(ViewLocatorRgbByte source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorRgbByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageRgbUInt16 Opening(ViewLocatorRgbUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageRgbUInt32 Opening(ViewLocatorRgbUInt32 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbUInt32``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageRgbDouble Opening(ViewLocatorRgbDouble source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageRgbaByte Opening(ViewLocatorRgbaByte source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+---------------------------+---------------+
| Parameter                | Type                      | Description   |
+==========================+===========================+===============+
| ``source``               | ``ViewLocatorRgbaByte``   |               |
+--------------------------+---------------------------+---------------+
| ``regionOfInterest``     | ``Region``                |               |
+--------------------------+---------------------------+---------------+
| ``structuringElement``   | ``Region``                |               |
+--------------------------+---------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Opening(ViewLocatorRgbaUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaUInt16``   |               |
+--------------------------+-----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Opening(ViewLocatorRgbaUInt32 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaUInt32``   |               |
+--------------------------+-----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageRgbaDouble Opening(ViewLocatorRgbaDouble source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaDouble``   |               |
+--------------------------+-----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageHlsByte Opening(ViewLocatorHlsByte source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorHlsByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageHlsUInt16 Opening(ViewLocatorHlsUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHlsUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageHlsDouble Opening(ViewLocatorHlsDouble source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHlsDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageHsiByte Opening(ViewLocatorHsiByte source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorHsiByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageHsiUInt16 Opening(ViewLocatorHsiUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHsiUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageHsiDouble Opening(ViewLocatorHsiDouble source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHsiDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageLabByte Opening(ViewLocatorLabByte source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorLabByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageLabUInt16 Opening(ViewLocatorLabUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorLabUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageLabDouble Opening(ViewLocatorLabDouble source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorLabDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageXyzByte Opening(ViewLocatorXyzByte source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorXyzByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageXyzUInt16 Opening(ViewLocatorXyzUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorXyzUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageXyzDouble Opening(ViewLocatorXyzDouble source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorXyzDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``Image Opening(View source, Region regionOfInterest, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+--------------+---------------+
| Parameter                | Type         | Description   |
+==========================+==============+===============+
| ``source``               | ``View``     |               |
+--------------------------+--------------+---------------+
| ``regionOfInterest``     | ``Region``   |               |
+--------------------------+--------------+---------------+
| ``structuringElement``   | ``Region``   |               |
+--------------------------+--------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageByte Opening(ViewLocatorByte source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+-----------------------+---------------+
| Parameter                | Type                  | Description   |
+==========================+=======================+===============+
| ``source``               | ``ViewLocatorByte``   |               |
+--------------------------+-----------------------+---------------+
| ``structuringElement``   | ``Region``            |               |
+--------------------------+-----------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageUInt16 Opening(ViewLocatorUInt16 source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorUInt16``   |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageUInt32 Opening(ViewLocatorUInt32 source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorUInt32``   |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageDouble Opening(ViewLocatorDouble source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorDouble``   |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageRgbByte Opening(ViewLocatorRgbByte source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorRgbByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageRgbUInt16 Opening(ViewLocatorRgbUInt16 source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageRgbUInt32 Opening(ViewLocatorRgbUInt32 source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbUInt32``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageRgbDouble Opening(ViewLocatorRgbDouble source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageRgbaByte Opening(ViewLocatorRgbaByte source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+---------------------------+---------------+
| Parameter                | Type                      | Description   |
+==========================+===========================+===============+
| ``source``               | ``ViewLocatorRgbaByte``   |               |
+--------------------------+---------------------------+---------------+
| ``structuringElement``   | ``Region``                |               |
+--------------------------+---------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Opening(ViewLocatorRgbaUInt16 source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaUInt16``   |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Opening(ViewLocatorRgbaUInt32 source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaUInt32``   |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageRgbaDouble Opening(ViewLocatorRgbaDouble source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaDouble``   |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageHlsByte Opening(ViewLocatorHlsByte source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorHlsByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageHlsUInt16 Opening(ViewLocatorHlsUInt16 source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHlsUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageHlsDouble Opening(ViewLocatorHlsDouble source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHlsDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageHsiByte Opening(ViewLocatorHsiByte source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorHsiByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageHsiUInt16 Opening(ViewLocatorHsiUInt16 source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHsiUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageHsiDouble Opening(ViewLocatorHsiDouble source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHsiDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageLabByte Opening(ViewLocatorLabByte source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorLabByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageLabUInt16 Opening(ViewLocatorLabUInt16 source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorLabUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageLabDouble Opening(ViewLocatorLabDouble source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorLabDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageXyzByte Opening(ViewLocatorXyzByte source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorXyzByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageXyzUInt16 Opening(ViewLocatorXyzUInt16 source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorXyzUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``ImageXyzDouble Opening(ViewLocatorXyzDouble source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorXyzDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Opening*
^^^^^^^^^^^^^^^^

``Image Opening(View source, Region structuringElement)``

Perform a morphological opening.

The method **Opening** has the following parameters:

+--------------------------+--------------+---------------+
| Parameter                | Type         | Description   |
+==========================+==============+===============+
| ``source``               | ``View``     |               |
+--------------------------+--------------+---------------+
| ``structuringElement``   | ``Region``   |               |
+--------------------------+--------------+---------------+

An opening is an erosion followed by a dilation.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageByte Closing(ViewLocatorByte source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+-----------------------+---------------+
| Parameter                | Type                  | Description   |
+==========================+=======================+===============+
| ``source``               | ``ViewLocatorByte``   |               |
+--------------------------+-----------------------+---------------+
| ``regionOfInterest``     | ``Region``            |               |
+--------------------------+-----------------------+---------------+
| ``structuringElement``   | ``Region``            |               |
+--------------------------+-----------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageUInt16 Closing(ViewLocatorUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorUInt16``   |               |
+--------------------------+-------------------------+---------------+
| ``regionOfInterest``     | ``Region``              |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageUInt32 Closing(ViewLocatorUInt32 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorUInt32``   |               |
+--------------------------+-------------------------+---------------+
| ``regionOfInterest``     | ``Region``              |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageDouble Closing(ViewLocatorDouble source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorDouble``   |               |
+--------------------------+-------------------------+---------------+
| ``regionOfInterest``     | ``Region``              |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageRgbByte Closing(ViewLocatorRgbByte source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorRgbByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageRgbUInt16 Closing(ViewLocatorRgbUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageRgbUInt32 Closing(ViewLocatorRgbUInt32 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbUInt32``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageRgbDouble Closing(ViewLocatorRgbDouble source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageRgbaByte Closing(ViewLocatorRgbaByte source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+---------------------------+---------------+
| Parameter                | Type                      | Description   |
+==========================+===========================+===============+
| ``source``               | ``ViewLocatorRgbaByte``   |               |
+--------------------------+---------------------------+---------------+
| ``regionOfInterest``     | ``Region``                |               |
+--------------------------+---------------------------+---------------+
| ``structuringElement``   | ``Region``                |               |
+--------------------------+---------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Closing(ViewLocatorRgbaUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaUInt16``   |               |
+--------------------------+-----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Closing(ViewLocatorRgbaUInt32 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaUInt32``   |               |
+--------------------------+-----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageRgbaDouble Closing(ViewLocatorRgbaDouble source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaDouble``   |               |
+--------------------------+-----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageHlsByte Closing(ViewLocatorHlsByte source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorHlsByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageHlsUInt16 Closing(ViewLocatorHlsUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHlsUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageHlsDouble Closing(ViewLocatorHlsDouble source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHlsDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageHsiByte Closing(ViewLocatorHsiByte source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorHsiByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageHsiUInt16 Closing(ViewLocatorHsiUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHsiUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageHsiDouble Closing(ViewLocatorHsiDouble source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHsiDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageLabByte Closing(ViewLocatorLabByte source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorLabByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageLabUInt16 Closing(ViewLocatorLabUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorLabUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageLabDouble Closing(ViewLocatorLabDouble source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorLabDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageXyzByte Closing(ViewLocatorXyzByte source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorXyzByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageXyzUInt16 Closing(ViewLocatorXyzUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorXyzUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageXyzDouble Closing(ViewLocatorXyzDouble source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorXyzDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``Image Closing(View source, Region regionOfInterest, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+--------------+---------------+
| Parameter                | Type         | Description   |
+==========================+==============+===============+
| ``source``               | ``View``     |               |
+--------------------------+--------------+---------------+
| ``regionOfInterest``     | ``Region``   |               |
+--------------------------+--------------+---------------+
| ``structuringElement``   | ``Region``   |               |
+--------------------------+--------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageByte Closing(ViewLocatorByte source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+-----------------------+---------------+
| Parameter                | Type                  | Description   |
+==========================+=======================+===============+
| ``source``               | ``ViewLocatorByte``   |               |
+--------------------------+-----------------------+---------------+
| ``structuringElement``   | ``Region``            |               |
+--------------------------+-----------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageUInt16 Closing(ViewLocatorUInt16 source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorUInt16``   |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageUInt32 Closing(ViewLocatorUInt32 source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorUInt32``   |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageDouble Closing(ViewLocatorDouble source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorDouble``   |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageRgbByte Closing(ViewLocatorRgbByte source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorRgbByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageRgbUInt16 Closing(ViewLocatorRgbUInt16 source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageRgbUInt32 Closing(ViewLocatorRgbUInt32 source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbUInt32``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageRgbDouble Closing(ViewLocatorRgbDouble source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageRgbaByte Closing(ViewLocatorRgbaByte source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+---------------------------+---------------+
| Parameter                | Type                      | Description   |
+==========================+===========================+===============+
| ``source``               | ``ViewLocatorRgbaByte``   |               |
+--------------------------+---------------------------+---------------+
| ``structuringElement``   | ``Region``                |               |
+--------------------------+---------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Closing(ViewLocatorRgbaUInt16 source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaUInt16``   |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Closing(ViewLocatorRgbaUInt32 source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaUInt32``   |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageRgbaDouble Closing(ViewLocatorRgbaDouble source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaDouble``   |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageHlsByte Closing(ViewLocatorHlsByte source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorHlsByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageHlsUInt16 Closing(ViewLocatorHlsUInt16 source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHlsUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageHlsDouble Closing(ViewLocatorHlsDouble source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHlsDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageHsiByte Closing(ViewLocatorHsiByte source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorHsiByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageHsiUInt16 Closing(ViewLocatorHsiUInt16 source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHsiUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageHsiDouble Closing(ViewLocatorHsiDouble source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHsiDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageLabByte Closing(ViewLocatorLabByte source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorLabByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageLabUInt16 Closing(ViewLocatorLabUInt16 source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorLabUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageLabDouble Closing(ViewLocatorLabDouble source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorLabDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageXyzByte Closing(ViewLocatorXyzByte source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorXyzByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageXyzUInt16 Closing(ViewLocatorXyzUInt16 source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorXyzUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``ImageXyzDouble Closing(ViewLocatorXyzDouble source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorXyzDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *Closing*
^^^^^^^^^^^^^^^^

``Image Closing(View source, Region structuringElement)``

Perform a morphological closing.

The method **Closing** has the following parameters:

+--------------------------+--------------+---------------+
| Parameter                | Type         | Description   |
+==========================+==============+===============+
| ``source``               | ``View``     |               |
+--------------------------+--------------+---------------+
| ``structuringElement``   | ``Region``   |               |
+--------------------------+--------------+---------------+

A closing is a dilation followed by an erosion.

The structuring element is specified in the form of a region. Only pixels in the neighborhood which correspond to pixels inside the region are used for the calculation.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte MorphologicalGradient(ViewLocatorByte source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+-----------------------+---------------+
| Parameter                | Type                  | Description   |
+==========================+=======================+===============+
| ``source``               | ``ViewLocatorByte``   |               |
+--------------------------+-----------------------+---------------+
| ``regionOfInterest``     | ``Region``            |               |
+--------------------------+-----------------------+---------------+
| ``structuringElement``   | ``Region``            |               |
+--------------------------+-----------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 MorphologicalGradient(ViewLocatorUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorUInt16``   |               |
+--------------------------+-------------------------+---------------+
| ``regionOfInterest``     | ``Region``              |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 MorphologicalGradient(ViewLocatorUInt32 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorUInt32``   |               |
+--------------------------+-------------------------+---------------+
| ``regionOfInterest``     | ``Region``              |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble MorphologicalGradient(ViewLocatorDouble source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorDouble``   |               |
+--------------------------+-------------------------+---------------+
| ``regionOfInterest``     | ``Region``              |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte MorphologicalGradient(ViewLocatorRgbByte source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorRgbByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 MorphologicalGradient(ViewLocatorRgbUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 MorphologicalGradient(ViewLocatorRgbUInt32 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbUInt32``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble MorphologicalGradient(ViewLocatorRgbDouble source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte MorphologicalGradient(ViewLocatorRgbaByte source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+---------------------------+---------------+
| Parameter                | Type                      | Description   |
+==========================+===========================+===============+
| ``source``               | ``ViewLocatorRgbaByte``   |               |
+--------------------------+---------------------------+---------------+
| ``regionOfInterest``     | ``Region``                |               |
+--------------------------+---------------------------+---------------+
| ``structuringElement``   | ``Region``                |               |
+--------------------------+---------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 MorphologicalGradient(ViewLocatorRgbaUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaUInt16``   |               |
+--------------------------+-----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 MorphologicalGradient(ViewLocatorRgbaUInt32 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaUInt32``   |               |
+--------------------------+-----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble MorphologicalGradient(ViewLocatorRgbaDouble source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaDouble``   |               |
+--------------------------+-----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte MorphologicalGradient(ViewLocatorHlsByte source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorHlsByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 MorphologicalGradient(ViewLocatorHlsUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHlsUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble MorphologicalGradient(ViewLocatorHlsDouble source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHlsDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte MorphologicalGradient(ViewLocatorHsiByte source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorHsiByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 MorphologicalGradient(ViewLocatorHsiUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHsiUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble MorphologicalGradient(ViewLocatorHsiDouble source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHsiDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte MorphologicalGradient(ViewLocatorLabByte source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorLabByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 MorphologicalGradient(ViewLocatorLabUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorLabUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble MorphologicalGradient(ViewLocatorLabDouble source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorLabDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte MorphologicalGradient(ViewLocatorXyzByte source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorXyzByte``   |               |
+--------------------------+--------------------------+---------------+
| ``regionOfInterest``     | ``Region``               |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 MorphologicalGradient(ViewLocatorXyzUInt16 source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorXyzUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble MorphologicalGradient(ViewLocatorXyzDouble source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorXyzDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``regionOfInterest``     | ``Region``                 |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Image MorphologicalGradient(View source, Region regionOfInterest, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+--------------+---------------+
| Parameter                | Type         | Description   |
+==========================+==============+===============+
| ``source``               | ``View``     |               |
+--------------------------+--------------+---------------+
| ``regionOfInterest``     | ``Region``   |               |
+--------------------------+--------------+---------------+
| ``structuringElement``   | ``Region``   |               |
+--------------------------+--------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte MorphologicalGradient(ViewLocatorByte source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+-----------------------+---------------+
| Parameter                | Type                  | Description   |
+==========================+=======================+===============+
| ``source``               | ``ViewLocatorByte``   |               |
+--------------------------+-----------------------+---------------+
| ``structuringElement``   | ``Region``            |               |
+--------------------------+-----------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 MorphologicalGradient(ViewLocatorUInt16 source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorUInt16``   |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 MorphologicalGradient(ViewLocatorUInt32 source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorUInt32``   |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble MorphologicalGradient(ViewLocatorDouble source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+-------------------------+---------------+
| Parameter                | Type                    | Description   |
+==========================+=========================+===============+
| ``source``               | ``ViewLocatorDouble``   |               |
+--------------------------+-------------------------+---------------+
| ``structuringElement``   | ``Region``              |               |
+--------------------------+-------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte MorphologicalGradient(ViewLocatorRgbByte source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorRgbByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 MorphologicalGradient(ViewLocatorRgbUInt16 source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 MorphologicalGradient(ViewLocatorRgbUInt32 source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbUInt32``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble MorphologicalGradient(ViewLocatorRgbDouble source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorRgbDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte MorphologicalGradient(ViewLocatorRgbaByte source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+---------------------------+---------------+
| Parameter                | Type                      | Description   |
+==========================+===========================+===============+
| ``source``               | ``ViewLocatorRgbaByte``   |               |
+--------------------------+---------------------------+---------------+
| ``structuringElement``   | ``Region``                |               |
+--------------------------+---------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 MorphologicalGradient(ViewLocatorRgbaUInt16 source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaUInt16``   |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 MorphologicalGradient(ViewLocatorRgbaUInt32 source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaUInt32``   |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble MorphologicalGradient(ViewLocatorRgbaDouble source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+-----------------------------+---------------+
| Parameter                | Type                        | Description   |
+==========================+=============================+===============+
| ``source``               | ``ViewLocatorRgbaDouble``   |               |
+--------------------------+-----------------------------+---------------+
| ``structuringElement``   | ``Region``                  |               |
+--------------------------+-----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte MorphologicalGradient(ViewLocatorHlsByte source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorHlsByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 MorphologicalGradient(ViewLocatorHlsUInt16 source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHlsUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble MorphologicalGradient(ViewLocatorHlsDouble source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHlsDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte MorphologicalGradient(ViewLocatorHsiByte source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorHsiByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 MorphologicalGradient(ViewLocatorHsiUInt16 source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHsiUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble MorphologicalGradient(ViewLocatorHsiDouble source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorHsiDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte MorphologicalGradient(ViewLocatorLabByte source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorLabByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 MorphologicalGradient(ViewLocatorLabUInt16 source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorLabUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble MorphologicalGradient(ViewLocatorLabDouble source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorLabDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte MorphologicalGradient(ViewLocatorXyzByte source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+--------------------------+---------------+
| Parameter                | Type                     | Description   |
+==========================+==========================+===============+
| ``source``               | ``ViewLocatorXyzByte``   |               |
+--------------------------+--------------------------+---------------+
| ``structuringElement``   | ``Region``               |               |
+--------------------------+--------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 MorphologicalGradient(ViewLocatorXyzUInt16 source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorXyzUInt16``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble MorphologicalGradient(ViewLocatorXyzDouble source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+----------------------------+---------------+
| Parameter                | Type                       | Description   |
+==========================+============================+===============+
| ``source``               | ``ViewLocatorXyzDouble``   |               |
+--------------------------+----------------------------+---------------+
| ``structuringElement``   | ``Region``                 |               |
+--------------------------+----------------------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *MorphologicalGradient*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Image MorphologicalGradient(View source, Region structuringElement)``

Perform a morphological gradient.

The method **MorphologicalGradient** has the following parameters:

+--------------------------+--------------+---------------+
| Parameter                | Type         | Description   |
+==========================+==============+===============+
| ``source``               | ``View``     |               |
+--------------------------+--------------+---------------+
| ``structuringElement``   | ``Region``   |               |
+--------------------------+--------------+---------------+

The saturated difference between the dilated image and the eroded image is calculated as the gradient.

Sometimes the morphological gradient is also called range filter, because the difference between the dilated image and the eroded image is the same as the range, i.e. the difference between the maximum and the minimum in the vicinity of some pixel.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageByte HitOrMiss(ViewLocatorByte source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+-----------------------+---------------+
| Parameter              | Type                  | Description   |
+========================+=======================+===============+
| ``source``             | ``ViewLocatorByte``   |               |
+------------------------+-----------------------+---------------+
| ``regionOfInterest``   | ``Region``            |               |
+------------------------+-----------------------+---------------+
| ``hit``                | ``Region``            |               |
+------------------------+-----------------------+---------------+
| ``miss``               | ``Region``            |               |
+------------------------+-----------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageUInt16 HitOrMiss(ViewLocatorUInt16 source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+-------------------------+---------------+
| Parameter              | Type                    | Description   |
+========================+=========================+===============+
| ``source``             | ``ViewLocatorUInt16``   |               |
+------------------------+-------------------------+---------------+
| ``regionOfInterest``   | ``Region``              |               |
+------------------------+-------------------------+---------------+
| ``hit``                | ``Region``              |               |
+------------------------+-------------------------+---------------+
| ``miss``               | ``Region``              |               |
+------------------------+-------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageUInt32 HitOrMiss(ViewLocatorUInt32 source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+-------------------------+---------------+
| Parameter              | Type                    | Description   |
+========================+=========================+===============+
| ``source``             | ``ViewLocatorUInt32``   |               |
+------------------------+-------------------------+---------------+
| ``regionOfInterest``   | ``Region``              |               |
+------------------------+-------------------------+---------------+
| ``hit``                | ``Region``              |               |
+------------------------+-------------------------+---------------+
| ``miss``               | ``Region``              |               |
+------------------------+-------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageDouble HitOrMiss(ViewLocatorDouble source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+-------------------------+---------------+
| Parameter              | Type                    | Description   |
+========================+=========================+===============+
| ``source``             | ``ViewLocatorDouble``   |               |
+------------------------+-------------------------+---------------+
| ``regionOfInterest``   | ``Region``              |               |
+------------------------+-------------------------+---------------+
| ``hit``                | ``Region``              |               |
+------------------------+-------------------------+---------------+
| ``miss``               | ``Region``              |               |
+------------------------+-------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageRgbByte HitOrMiss(ViewLocatorRgbByte source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+--------------------------+---------------+
| Parameter              | Type                     | Description   |
+========================+==========================+===============+
| ``source``             | ``ViewLocatorRgbByte``   |               |
+------------------------+--------------------------+---------------+
| ``regionOfInterest``   | ``Region``               |               |
+------------------------+--------------------------+---------------+
| ``hit``                | ``Region``               |               |
+------------------------+--------------------------+---------------+
| ``miss``               | ``Region``               |               |
+------------------------+--------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 HitOrMiss(ViewLocatorRgbUInt16 source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+----------------------------+---------------+
| Parameter              | Type                       | Description   |
+========================+============================+===============+
| ``source``             | ``ViewLocatorRgbUInt16``   |               |
+------------------------+----------------------------+---------------+
| ``regionOfInterest``   | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``hit``                | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``miss``               | ``Region``                 |               |
+------------------------+----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 HitOrMiss(ViewLocatorRgbUInt32 source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+----------------------------+---------------+
| Parameter              | Type                       | Description   |
+========================+============================+===============+
| ``source``             | ``ViewLocatorRgbUInt32``   |               |
+------------------------+----------------------------+---------------+
| ``regionOfInterest``   | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``hit``                | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``miss``               | ``Region``                 |               |
+------------------------+----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageRgbDouble HitOrMiss(ViewLocatorRgbDouble source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+----------------------------+---------------+
| Parameter              | Type                       | Description   |
+========================+============================+===============+
| ``source``             | ``ViewLocatorRgbDouble``   |               |
+------------------------+----------------------------+---------------+
| ``regionOfInterest``   | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``hit``                | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``miss``               | ``Region``                 |               |
+------------------------+----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageRgbaByte HitOrMiss(ViewLocatorRgbaByte source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+---------------------------+---------------+
| Parameter              | Type                      | Description   |
+========================+===========================+===============+
| ``source``             | ``ViewLocatorRgbaByte``   |               |
+------------------------+---------------------------+---------------+
| ``regionOfInterest``   | ``Region``                |               |
+------------------------+---------------------------+---------------+
| ``hit``                | ``Region``                |               |
+------------------------+---------------------------+---------------+
| ``miss``               | ``Region``                |               |
+------------------------+---------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 HitOrMiss(ViewLocatorRgbaUInt16 source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+-----------------------------+---------------+
| Parameter              | Type                        | Description   |
+========================+=============================+===============+
| ``source``             | ``ViewLocatorRgbaUInt16``   |               |
+------------------------+-----------------------------+---------------+
| ``regionOfInterest``   | ``Region``                  |               |
+------------------------+-----------------------------+---------------+
| ``hit``                | ``Region``                  |               |
+------------------------+-----------------------------+---------------+
| ``miss``               | ``Region``                  |               |
+------------------------+-----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 HitOrMiss(ViewLocatorRgbaUInt32 source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+-----------------------------+---------------+
| Parameter              | Type                        | Description   |
+========================+=============================+===============+
| ``source``             | ``ViewLocatorRgbaUInt32``   |               |
+------------------------+-----------------------------+---------------+
| ``regionOfInterest``   | ``Region``                  |               |
+------------------------+-----------------------------+---------------+
| ``hit``                | ``Region``                  |               |
+------------------------+-----------------------------+---------------+
| ``miss``               | ``Region``                  |               |
+------------------------+-----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble HitOrMiss(ViewLocatorRgbaDouble source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+-----------------------------+---------------+
| Parameter              | Type                        | Description   |
+========================+=============================+===============+
| ``source``             | ``ViewLocatorRgbaDouble``   |               |
+------------------------+-----------------------------+---------------+
| ``regionOfInterest``   | ``Region``                  |               |
+------------------------+-----------------------------+---------------+
| ``hit``                | ``Region``                  |               |
+------------------------+-----------------------------+---------------+
| ``miss``               | ``Region``                  |               |
+------------------------+-----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageHlsByte HitOrMiss(ViewLocatorHlsByte source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+--------------------------+---------------+
| Parameter              | Type                     | Description   |
+========================+==========================+===============+
| ``source``             | ``ViewLocatorHlsByte``   |               |
+------------------------+--------------------------+---------------+
| ``regionOfInterest``   | ``Region``               |               |
+------------------------+--------------------------+---------------+
| ``hit``                | ``Region``               |               |
+------------------------+--------------------------+---------------+
| ``miss``               | ``Region``               |               |
+------------------------+--------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 HitOrMiss(ViewLocatorHlsUInt16 source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+----------------------------+---------------+
| Parameter              | Type                       | Description   |
+========================+============================+===============+
| ``source``             | ``ViewLocatorHlsUInt16``   |               |
+------------------------+----------------------------+---------------+
| ``regionOfInterest``   | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``hit``                | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``miss``               | ``Region``                 |               |
+------------------------+----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageHlsDouble HitOrMiss(ViewLocatorHlsDouble source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+----------------------------+---------------+
| Parameter              | Type                       | Description   |
+========================+============================+===============+
| ``source``             | ``ViewLocatorHlsDouble``   |               |
+------------------------+----------------------------+---------------+
| ``regionOfInterest``   | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``hit``                | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``miss``               | ``Region``                 |               |
+------------------------+----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageHsiByte HitOrMiss(ViewLocatorHsiByte source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+--------------------------+---------------+
| Parameter              | Type                     | Description   |
+========================+==========================+===============+
| ``source``             | ``ViewLocatorHsiByte``   |               |
+------------------------+--------------------------+---------------+
| ``regionOfInterest``   | ``Region``               |               |
+------------------------+--------------------------+---------------+
| ``hit``                | ``Region``               |               |
+------------------------+--------------------------+---------------+
| ``miss``               | ``Region``               |               |
+------------------------+--------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 HitOrMiss(ViewLocatorHsiUInt16 source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+----------------------------+---------------+
| Parameter              | Type                       | Description   |
+========================+============================+===============+
| ``source``             | ``ViewLocatorHsiUInt16``   |               |
+------------------------+----------------------------+---------------+
| ``regionOfInterest``   | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``hit``                | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``miss``               | ``Region``                 |               |
+------------------------+----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageHsiDouble HitOrMiss(ViewLocatorHsiDouble source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+----------------------------+---------------+
| Parameter              | Type                       | Description   |
+========================+============================+===============+
| ``source``             | ``ViewLocatorHsiDouble``   |               |
+------------------------+----------------------------+---------------+
| ``regionOfInterest``   | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``hit``                | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``miss``               | ``Region``                 |               |
+------------------------+----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageLabByte HitOrMiss(ViewLocatorLabByte source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+--------------------------+---------------+
| Parameter              | Type                     | Description   |
+========================+==========================+===============+
| ``source``             | ``ViewLocatorLabByte``   |               |
+------------------------+--------------------------+---------------+
| ``regionOfInterest``   | ``Region``               |               |
+------------------------+--------------------------+---------------+
| ``hit``                | ``Region``               |               |
+------------------------+--------------------------+---------------+
| ``miss``               | ``Region``               |               |
+------------------------+--------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 HitOrMiss(ViewLocatorLabUInt16 source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+----------------------------+---------------+
| Parameter              | Type                       | Description   |
+========================+============================+===============+
| ``source``             | ``ViewLocatorLabUInt16``   |               |
+------------------------+----------------------------+---------------+
| ``regionOfInterest``   | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``hit``                | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``miss``               | ``Region``                 |               |
+------------------------+----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageLabDouble HitOrMiss(ViewLocatorLabDouble source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+----------------------------+---------------+
| Parameter              | Type                       | Description   |
+========================+============================+===============+
| ``source``             | ``ViewLocatorLabDouble``   |               |
+------------------------+----------------------------+---------------+
| ``regionOfInterest``   | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``hit``                | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``miss``               | ``Region``                 |               |
+------------------------+----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageXyzByte HitOrMiss(ViewLocatorXyzByte source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+--------------------------+---------------+
| Parameter              | Type                     | Description   |
+========================+==========================+===============+
| ``source``             | ``ViewLocatorXyzByte``   |               |
+------------------------+--------------------------+---------------+
| ``regionOfInterest``   | ``Region``               |               |
+------------------------+--------------------------+---------------+
| ``hit``                | ``Region``               |               |
+------------------------+--------------------------+---------------+
| ``miss``               | ``Region``               |               |
+------------------------+--------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 HitOrMiss(ViewLocatorXyzUInt16 source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+----------------------------+---------------+
| Parameter              | Type                       | Description   |
+========================+============================+===============+
| ``source``             | ``ViewLocatorXyzUInt16``   |               |
+------------------------+----------------------------+---------------+
| ``regionOfInterest``   | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``hit``                | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``miss``               | ``Region``                 |               |
+------------------------+----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageXyzDouble HitOrMiss(ViewLocatorXyzDouble source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+----------------------------+---------------+
| Parameter              | Type                       | Description   |
+========================+============================+===============+
| ``source``             | ``ViewLocatorXyzDouble``   |               |
+------------------------+----------------------------+---------------+
| ``regionOfInterest``   | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``hit``                | ``Region``                 |               |
+------------------------+----------------------------+---------------+
| ``miss``               | ``Region``                 |               |
+------------------------+----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``Image HitOrMiss(View source, Region regionOfInterest, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+------------------------+--------------+---------------+
| Parameter              | Type         | Description   |
+========================+==============+===============+
| ``source``             | ``View``     |               |
+------------------------+--------------+---------------+
| ``regionOfInterest``   | ``Region``   |               |
+------------------------+--------------+---------------+
| ``hit``                | ``Region``   |               |
+------------------------+--------------+---------------+
| ``miss``               | ``Region``   |               |
+------------------------+--------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

The region\_of\_interest parameter constrains the function to a region of interest within the source view only.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageByte HitOrMiss(ViewLocatorByte source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``hit``      | ``Region``            |               |
+--------------+-----------------------+---------------+
| ``miss``     | ``Region``            |               |
+--------------+-----------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageUInt16 HitOrMiss(ViewLocatorUInt16 source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``hit``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``miss``     | ``Region``              |               |
+--------------+-------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageUInt32 HitOrMiss(ViewLocatorUInt32 source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``hit``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``miss``     | ``Region``              |               |
+--------------+-------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageDouble HitOrMiss(ViewLocatorDouble source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``hit``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``miss``     | ``Region``              |               |
+--------------+-------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageRgbByte HitOrMiss(ViewLocatorRgbByte source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``hit``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``miss``     | ``Region``               |               |
+--------------+--------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 HitOrMiss(ViewLocatorRgbUInt16 source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``hit``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``miss``     | ``Region``                 |               |
+--------------+----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 HitOrMiss(ViewLocatorRgbUInt32 source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``hit``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``miss``     | ``Region``                 |               |
+--------------+----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageRgbDouble HitOrMiss(ViewLocatorRgbDouble source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``hit``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``miss``     | ``Region``                 |               |
+--------------+----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageRgbaByte HitOrMiss(ViewLocatorRgbaByte source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``hit``      | ``Region``                |               |
+--------------+---------------------------+---------------+
| ``miss``     | ``Region``                |               |
+--------------+---------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 HitOrMiss(ViewLocatorRgbaUInt16 source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``hit``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``miss``     | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 HitOrMiss(ViewLocatorRgbaUInt32 source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``hit``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``miss``     | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble HitOrMiss(ViewLocatorRgbaDouble source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``hit``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``miss``     | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageHlsByte HitOrMiss(ViewLocatorHlsByte source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``hit``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``miss``     | ``Region``               |               |
+--------------+--------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 HitOrMiss(ViewLocatorHlsUInt16 source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``hit``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``miss``     | ``Region``                 |               |
+--------------+----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageHlsDouble HitOrMiss(ViewLocatorHlsDouble source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``hit``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``miss``     | ``Region``                 |               |
+--------------+----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageHsiByte HitOrMiss(ViewLocatorHsiByte source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``hit``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``miss``     | ``Region``               |               |
+--------------+--------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 HitOrMiss(ViewLocatorHsiUInt16 source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``hit``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``miss``     | ``Region``                 |               |
+--------------+----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageHsiDouble HitOrMiss(ViewLocatorHsiDouble source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``hit``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``miss``     | ``Region``                 |               |
+--------------+----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageLabByte HitOrMiss(ViewLocatorLabByte source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``hit``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``miss``     | ``Region``               |               |
+--------------+--------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 HitOrMiss(ViewLocatorLabUInt16 source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``hit``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``miss``     | ``Region``                 |               |
+--------------+----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageLabDouble HitOrMiss(ViewLocatorLabDouble source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``hit``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``miss``     | ``Region``                 |               |
+--------------+----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageXyzByte HitOrMiss(ViewLocatorXyzByte source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``hit``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``miss``     | ``Region``               |               |
+--------------+--------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 HitOrMiss(ViewLocatorXyzUInt16 source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``hit``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``miss``     | ``Region``                 |               |
+--------------+----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``ImageXyzDouble HitOrMiss(ViewLocatorXyzDouble source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``hit``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``miss``     | ``Region``                 |               |
+--------------+----------------------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.

Method *HitOrMiss*
^^^^^^^^^^^^^^^^^^

``Image HitOrMiss(View source, Region hit, Region miss)``

Perform a hit or miss transform.

The method **HitOrMiss** has the following parameters:

+--------------+--------------+---------------+
| Parameter    | Type         | Description   |
+==============+==============+===============+
| ``source``   | ``View``     |               |
+--------------+--------------+---------------+
| ``hit``      | ``Region``   |               |
+--------------+--------------+---------------+
| ``miss``     | ``Region``   |               |
+--------------+--------------+---------------+

The transform works with two structuring elements. The structuring element hit must fit the foreground and the structuring element miss must fit the background at the same time. Thus, a better name for the transform would be hit\_and\_miss, but since hit\_or\_miss is an established name in the imaging profession, we chose to stay with that name.

The intersection of the two structuring elements must be empty.

/return The result image.
