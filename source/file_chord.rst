Class *Chord*
-------------

A chord in a region.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Chord** implements the following interfaces:

+-----------------------+
| Interface             |
+=======================+
| ``IEquatableChord``   |
+-----------------------+
| ``ISerializable``     |
+-----------------------+

The class **Chord** contains the following properties:

+-------------------+-------+-------+---------------------------------------------------------+
| Property          | Get   | Set   | Description                                             |
+===================+=======+=======+=========================================================+
| ``Row``           | \*    | \*    | The row coordinate of the chord.                        |
+-------------------+-------+-------+---------------------------------------------------------+
| ``ColumnRange``   | \*    | \*    | The begin column coordinate (inclusive) of the chord.   |
+-------------------+-------+-------+---------------------------------------------------------+
| ``Length``        | \*    |       | The length of a chord.                                  |
+-------------------+-------+-------+---------------------------------------------------------+
| ``Begin``         | \*    |       | The begin (start point, inclusive) of the chord.        |
+-------------------+-------+-------+---------------------------------------------------------+
| ``End``           | \*    |       | The end (end point, exclusive) of the chord.            |
+-------------------+-------+-------+---------------------------------------------------------+
| ``Moment00``      | \*    |       | Zeroth order moment of a chord.                         |
+-------------------+-------+-------+---------------------------------------------------------+
| ``Moment10``      | \*    |       | First order moment of a chord.                          |
+-------------------+-------+-------+---------------------------------------------------------+
| ``Moment01``      | \*    |       | First order moment of a chord.                          |
+-------------------+-------+-------+---------------------------------------------------------+
| ``Moment20``      | \*    |       | Second order moment of a chord.                         |
+-------------------+-------+-------+---------------------------------------------------------+
| ``Moment11``      | \*    |       | Second order moment of a chord.                         |
+-------------------+-------+-------+---------------------------------------------------------+
| ``Moment02``      | \*    |       | Second order moment of a chord.                         |
+-------------------+-------+-------+---------------------------------------------------------+
| ``Moment30``      | \*    |       | Third order moment of a chord.                          |
+-------------------+-------+-------+---------------------------------------------------------+
| ``Moment21``      | \*    |       | Third order moment of a chord.                          |
+-------------------+-------+-------+---------------------------------------------------------+
| ``Moment12``      | \*    |       | Third order moment of a chord.                          |
+-------------------+-------+-------+---------------------------------------------------------+
| ``Moment03``      | \*    |       | Third order moment of a chord.                          |
+-------------------+-------+-------+---------------------------------------------------------+

The class **Chord** contains the following methods:

+-----------------+---------------------------------------------------------+
| Method          | Description                                             |
+=================+=========================================================+
| ``Contains``    | Test if point is contained in chord.                    |
+-----------------+---------------------------------------------------------+
| ``Contains``    | Test if point is contained in chord.                    |
+-----------------+---------------------------------------------------------+
| ``Transpose``   | Transpose a chord.                                      |
+-----------------+---------------------------------------------------------+
| ``Translate``   | Translate a chord.                                      |
+-----------------+---------------------------------------------------------+
| ``Translate``   | Translate a chord.                                      |
+-----------------+---------------------------------------------------------+
| ``ToString``    | Provide string representation for debugging purposes.   |
+-----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

A chord is a (possibly partial) scan along a horizontal line of a region. A chord consists of row coordinate and a range of column coordinates.

Chords specify a sort order. One chord is smaller than the other if it starts before the other chord modeling the image as a continuous sequence of pixels from left to right and top to bottom.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Chord*
^^^^^^^^^^^^^^^^^^^

``Chord()``

Construct an empty chord.

Both row and the column\_range are zero-initialized.

Constructors
~~~~~~~~~~~~

Constructor *Chord*
^^^^^^^^^^^^^^^^^^^

``Chord(System.Int32 row, Range columnRange)``

Construct a chord with specific values.

The constructor has the following parameters:

+-------------------+--------------------+----------------------------------+
| Parameter         | Type               | Description                      |
+===================+====================+==================================+
| ``row``           | ``System.Int32``   | The row of the chord.            |
+-------------------+--------------------+----------------------------------+
| ``columnRange``   | ``Range``          | The column range of the chord.   |
+-------------------+--------------------+----------------------------------+

Both row and column\_range are initialized to the passed values.

Properties
~~~~~~~~~~

Property *Row*
^^^^^^^^^^^^^^

``System.Int32 Row``

The row coordinate of the chord.

Property *ColumnRange*
^^^^^^^^^^^^^^^^^^^^^^

``Range ColumnRange``

The begin column coordinate (inclusive) of the chord.

Property *Length*
^^^^^^^^^^^^^^^^^

``System.Int32 Length``

The length of a chord.

Property *Begin*
^^^^^^^^^^^^^^^^

``PointInt32 Begin``

The begin (start point, inclusive) of the chord.

Property *End*
^^^^^^^^^^^^^^

``PointInt32 End``

The end (end point, exclusive) of the chord.

Property *Moment00*
^^^^^^^^^^^^^^^^^^^

``System.Double Moment00``

Zeroth order moment of a chord.

Property *Moment10*
^^^^^^^^^^^^^^^^^^^

``System.Double Moment10``

First order moment of a chord.

Property *Moment01*
^^^^^^^^^^^^^^^^^^^

``System.Double Moment01``

First order moment of a chord.

Property *Moment20*
^^^^^^^^^^^^^^^^^^^

``System.Double Moment20``

Second order moment of a chord.

Property *Moment11*
^^^^^^^^^^^^^^^^^^^

``System.Double Moment11``

Second order moment of a chord.

Property *Moment02*
^^^^^^^^^^^^^^^^^^^

``System.Double Moment02``

Second order moment of a chord.

Property *Moment30*
^^^^^^^^^^^^^^^^^^^

``System.Double Moment30``

Third order moment of a chord.

Property *Moment21*
^^^^^^^^^^^^^^^^^^^

``System.Double Moment21``

Third order moment of a chord.

Property *Moment12*
^^^^^^^^^^^^^^^^^^^

``System.Double Moment12``

Third order moment of a chord.

Property *Moment03*
^^^^^^^^^^^^^^^^^^^

``System.Double Moment03``

Third order moment of a chord.

Static Methods
~~~~~~~~~~~~~~

Method *AreChordsOverlapping*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean AreChordsOverlapping(Chord a, Chord b)``

Test if two chords overlap.

The method **AreChordsOverlapping** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``a``       | ``Chord``   |               |
+-------------+-------------+---------------+
| ``b``       | ``Chord``   |               |
+-------------+-------------+---------------+

Touching does not count as overlap.

Method *AreChordsTouching*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean AreChordsTouching(Chord a, Chord b)``

Test if two chords touch.

The method **AreChordsTouching** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``a``       | ``Chord``   |               |
+-------------+-------------+---------------+
| ``b``       | ``Chord``   |               |
+-------------+-------------+---------------+

Method *AreChordsClose*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean AreChordsClose(Chord a, Chord b, System.Int32 dx, System.Int32 dy)``

Test if two chords are close.

The method **AreChordsClose** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``a``       | ``Chord``          |               |
+-------------+--------------------+---------------+
| ``b``       | ``Chord``          |               |
+-------------+--------------------+---------------+
| ``dx``      | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``dy``      | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *AreChordsClose*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean AreChordsClose(Chord a, Chord b, VectorInt32 distance)``

Test if two chords are close.

The method **AreChordsClose** has the following parameters:

+----------------+-------------------+---------------+
| Parameter      | Type              | Description   |
+================+===================+===============+
| ``a``          | ``Chord``         |               |
+----------------+-------------------+---------------+
| ``b``          | ``Chord``         |               |
+----------------+-------------------+---------------+
| ``distance``   | ``VectorInt32``   |               |
+----------------+-------------------+---------------+

Methods
~~~~~~~

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(System.Int32 x, System.Int32 y)``

Test if point is contained in chord.

The method **Contains** has the following parameters:

+-------------+--------------------+------------------------------------------+
| Parameter   | Type               | Description                              |
+=============+====================+==========================================+
| ``x``       | ``System.Int32``   | The x coordinate of the point to test.   |
+-------------+--------------------+------------------------------------------+
| ``y``       | ``System.Int32``   | The y coordinate of the point to test.   |
+-------------+--------------------+------------------------------------------+

Return true if point is within the chord otherwise return false.

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(PointInt32 coordinate)``

Test if point is contained in chord.

The method **Contains** has the following parameters:

+------------------+------------------+---------------+
| Parameter        | Type             | Description   |
+==================+==================+===============+
| ``coordinate``   | ``PointInt32``   |               |
+------------------+------------------+---------------+

Return true if point is within the chord otherwise return false.

Method *Transpose*
^^^^^^^^^^^^^^^^^^

``Chord Transpose()``

Transpose a chord.

This function returns a transposed copy of a chord. Transposition mirrors a chord at the origin.A transposed chord object.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``Chord Translate(System.Int32 x, System.Int32 y)``

Translate a chord.

The method **Translate** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``x``       | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``y``       | ``System.Int32``   |               |
+-------------+--------------------+---------------+

This function returns a translated copy of a chord.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``Chord Translate(VectorInt32 translation)``

Translate a chord.

The method **Translate** has the following parameters:

+-------------------+-------------------+---------------------------+
| Parameter         | Type              | Description               |
+===================+===================+===========================+
| ``translation``   | ``VectorInt32``   | The translation offset.   |
+-------------------+-------------------+---------------------------+

This function returns a translated copy of a chord.

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
