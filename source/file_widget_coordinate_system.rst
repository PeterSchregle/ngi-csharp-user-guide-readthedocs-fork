Class *WidgetCoordinateSystem*
------------------------------

A widget\_coordinate\_system sets a left-handed or right-handed coordinate system on a surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetCoordinateSystem** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetCoordinateSystem** contains the following properties:

+-------------------+-------+-------+---------------------------------------------+
| Property          | Get   | Set   | Description                                 |
+===================+=======+=======+=============================================+
| ``Transform``     | \*    | \*    | The transform of the coordinate system.     |
+-------------------+-------+-------+---------------------------------------------+
| ``Orientation``   | \*    | \*    | The orientation of the coordinate system.   |
+-------------------+-------+-------+---------------------------------------------+

The class **WidgetCoordinateSystem** contains the following enumerations:

+-----------------------------------+------------------------------+
| Enumeration                       | Description                  |
+===================================+==============================+
| ``CoordinateSystemOrientation``   | TODO documentation missing   |
+-----------------------------------+------------------------------+

Description
~~~~~~~~~~~

The left- or right-handedness as well as the origin can be set for a widget\_coordinate\_system. These settings affect the behavior of the widget\_coordinate\_system.

See https://en.wikipedia.org/wiki/Cartesian\_coordinate\_system for an explanation of left- or right-handedness in a two-dimensional coordinate system.

Citing Wikipedia: "A commonly used mnemonic for defining the positive orientation is the right hand rule. Placing a somewhat closed right hand on the plane with the thumb pointing up, the fingers point from the x-axis to the y-axis, in a positively oriented coordinate system. The other way of orienting the axes is following the left hand rule, placing the left hand on the plane with the thumb pointing up. When pointing the thumb away from the origin along an axis towards positive, the curvature of the fingers indicates a positive rotation along that axis." This widget supports interactivity.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetCoordinateSystem*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetCoordinateSystem()``

Default constructor.

The default constructor creates a widget with default settings. By default the coordinate system is left-handed and has a zero origin.

Properties
~~~~~~~~~~

Property *Transform*
^^^^^^^^^^^^^^^^^^^^

``SimilarityMatrix Transform``

The transform of the coordinate system.

The transform of the widget\_coordinate\_system is specified with a similarity matrix.

Property *Orientation*
^^^^^^^^^^^^^^^^^^^^^^

``WidgetCoordinateSystem.CoordinateSystemOrientation Orientation``

The orientation of the coordinate system.

The coordinate system can be left- or right-handed.

Enumerations
~~~~~~~~~~~~

Enumeration *CoordinateSystemOrientation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum CoordinateSystemOrientation``

TODO documentation missing

The enumeration **CoordinateSystemOrientation** has the following constants:

+-------------------+---------+---------------+
| Name              | Value   | Description   |
+===================+=========+===============+
| ``leftHanded``    | ``0``   |               |
+-------------------+---------+---------------+
| ``rightHanded``   | ``1``   |               |
+-------------------+---------+---------------+

::

    enum CoordinateSystemOrientation
    {
      leftHanded = 0,
      rightHanded = 1,
    };

TODO documentation missing
