Class *BufferEnumerator*
------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **BufferEnumerator** contains the following variant parameters:

+--------------+-----------------------------------------+
| Variant      | Description                             |
+==============+=========================================+
| ``Buffer``   | TODO no brief description for variant   |
+--------------+-----------------------------------------+

The class **BufferEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **BufferEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Buffer*
^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Buffer** has the following types:

+---------------------------------+
| Type                            |
+=================================+
| ``SharedPtrBufferByte``         |
+---------------------------------+
| ``SharedPtrBufferUInt16``       |
+---------------------------------+
| ``SharedPtrBufferUInt32``       |
+---------------------------------+
| ``SharedPtrBufferDouble``       |
+---------------------------------+
| ``SharedPtrBufferRgbByte``      |
+---------------------------------+
| ``SharedPtrBufferRgbUInt16``    |
+---------------------------------+
| ``SharedPtrBufferRgbUInt32``    |
+---------------------------------+
| ``SharedPtrBufferRgbDouble``    |
+---------------------------------+
| ``SharedPtrBufferRgbaByte``     |
+---------------------------------+
| ``SharedPtrBufferRgbaUInt16``   |
+---------------------------------+
| ``SharedPtrBufferRgbaUInt32``   |
+---------------------------------+
| ``SharedPtrBufferRgbaDouble``   |
+---------------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``System.Object Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
