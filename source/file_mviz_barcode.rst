Class *MvizBarcode*
-------------------

A barcode result.

**Namespace:** Ngi

**Module:** BarcodeMatrixcode

The class **MvizBarcode** implements the following interfaces:

+-----------------------------+
| Interface                   |
+=============================+
| ``IEquatableMvizBarcode``   |
+-----------------------------+
| ``ISerializable``           |
+-----------------------------+

The class **MvizBarcode** contains the following properties:

+-------------------------+-------+-------+----------------------------------------------------------------------------------------------+
| Property                | Get   | Set   | Description                                                                                  |
+=========================+=======+=======+==============================================================================================+
| ``Text``                | \*    |       | The decoded text of the barcode.                                                             |
+-------------------------+-------+-------+----------------------------------------------------------------------------------------------+
| ``Position``            | \*    |       | The position of the barcode.                                                                 |
+-------------------------+-------+-------+----------------------------------------------------------------------------------------------+
| ``SymbologyId``         | \*    |       | The symbology of the barcode.                                                                |
+-------------------------+-------+-------+----------------------------------------------------------------------------------------------+
| ``SymbologyName``       | \*    |       | The symbology of the barcode.                                                                |
+-------------------------+-------+-------+----------------------------------------------------------------------------------------------+
| ``ChecksumValid``       | \*    |       | This is true, if a valid checksum was calculated.                                            |
+-------------------------+-------+-------+----------------------------------------------------------------------------------------------+
| ``Fnc1``                | \*    |       | This is true, if a valid FNC1 character was found.                                           |
+-------------------------+-------+-------+----------------------------------------------------------------------------------------------+
| ``ReaderProgramming``   | \*    |       | This is true, if the reader programming control character was found in the first position.   |
+-------------------------+-------+-------+----------------------------------------------------------------------------------------------+
| ``Append``              | \*    |       | This is true, if an append control character was found.                                      |
+-------------------------+-------+-------+----------------------------------------------------------------------------------------------+

The class **MvizBarcode** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

The class **MvizBarcode** contains the following enumerations:

+---------------------+------------------------------+
| Enumeration         | Description                  |
+=====================+==============================+
| ``SymbologyType``   | TODO documentation missing   |
+---------------------+------------------------------+

Description
~~~~~~~~~~~

A barcode result consists of the read barcode, the symbology and the location of the barcode, and a few other flags.

The following operators are implemented for a barcode: operator == : comparison for equality. operator != : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *MvizBarcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MvizBarcode()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *MvizBarcode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``MvizBarcode(System.String text, Rectangle position, MvizBarcode.SymbologyType symbologyId, System.Boolean checksumValid, System.Boolean fnc1, System.Boolean readerProgramming, System.Boolean append)``

Constructor.

The constructor has the following parameters:

+--------------+------------+----------------------------------------------------+
| Parameter    | Type       | Description                                        |
+==============+============+====================================================+
| ``text``     | ``System.S | The decoded text of the barcode.                   |
|              | tring``    |                                                    |
+--------------+------------+----------------------------------------------------+
| ``position`` | ``Rectangl | The position of the barcode.                       |
|              | e``        |                                                    |
+--------------+------------+----------------------------------------------------+
| ``symbologyI | ``MvizBarc | The symbology of the barcode.                      |
| d``          | ode.Symbol |                                                    |
|              | ogyType``  |                                                    |
+--------------+------------+----------------------------------------------------+
| ``checksumVa | ``System.B | The checksum flag, true if a valid checksum was    |
| lid``        | oolean``   | found.                                             |
+--------------+------------+----------------------------------------------------+
| ``fnc1``     | ``System.B | The FNC1 flag, true if a valid FNC1 character was  |
|              | oolean``   | found.                                             |
+--------------+------------+----------------------------------------------------+
| ``readerProg | ``System.B | The reader programming flag, true if the reader    |
| ramming``    | oolean``   | programming control character was found in the     |
|              |            | first position.                                    |
+--------------+------------+----------------------------------------------------+
| ``append``   | ``System.B | The append flag, true if an append control         |
|              | oolean``   | character was found.                               |
+--------------+------------+----------------------------------------------------+

Properties
~~~~~~~~~~

Property *Text*
^^^^^^^^^^^^^^^

``System.String Text``

The decoded text of the barcode.

Property *Position*
^^^^^^^^^^^^^^^^^^^

``Rectangle Position``

The position of the barcode.

Property *SymbologyId*
^^^^^^^^^^^^^^^^^^^^^^

``MvizBarcode.SymbologyType SymbologyId``

The symbology of the barcode.

Property *SymbologyName*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.String SymbologyName``

The symbology of the barcode.

Property *ChecksumValid*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean ChecksumValid``

This is true, if a valid checksum was calculated.

Property *Fnc1*
^^^^^^^^^^^^^^^

``System.Boolean Fnc1``

This is true, if a valid FNC1 character was found.

Property *ReaderProgramming*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean ReaderProgramming``

This is true, if the reader programming control character was found in the first position.

Property *Append*
^^^^^^^^^^^^^^^^^

``System.Boolean Append``

This is true, if an append control character was found.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Enumerations
~~~~~~~~~~~~

Enumeration *SymbologyType*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum SymbologyType``

TODO documentation missing

The enumeration **SymbologyType** has the following constants:

+----------------------+----------+---------------+
| Name                 | Value    | Description   |
+======================+==========+===============+
| ``unknown``          | ``0``    |               |
+----------------------+----------+---------------+
| ``code39``           | ``1``    |               |
+----------------------+----------+---------------+
| ``code39Ext``        | ``2``    |               |
+----------------------+----------+---------------+
| ``code93``           | ``3``    |               |
+----------------------+----------+---------------+
| ``code128``          | ``4``    |               |
+----------------------+----------+---------------+
| ``ean8``             | ``5``    |               |
+----------------------+----------+---------------+
| ``ean13``            | ``6``    |               |
+----------------------+----------+---------------+
| ``itf``              | ``7``    |               |
+----------------------+----------+---------------+
| ``upcA``             | ``8``    |               |
+----------------------+----------+---------------+
| ``upcE``             | ``9``    |               |
+----------------------+----------+---------------+
| ``databar``          | ``10``   |               |
+----------------------+----------+---------------+
| ``databarLimited``   | ``11``   |               |
+----------------------+----------+---------------+
| ``codabar``          | ``12``   |               |
+----------------------+----------+---------------+
| ``pharmacode``       | ``13``   |               |
+----------------------+----------+---------------+

::

    enum SymbologyType
    {
      unknown = 0,
      code39 = 1,
      code39Ext = 2,
      code93 = 3,
      code128 = 4,
      ean8 = 5,
      ean13 = 6,
      itf = 7,
      upcA = 8,
      upcE = 9,
      databar = 10,
      databarLimited = 11,
      codabar = 12,
      pharmacode = 13,
    };

TODO documentation missing
