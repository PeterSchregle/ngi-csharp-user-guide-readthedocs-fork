Class *CameraParameterIfloat*
-----------------------------

**Namespace:** Ngi

**Module:** Camera

The class **CameraParameterIfloat** contains the following properties:

+------------------------+-------+-------+---------------+
| Property               | Get   | Set   | Description   |
+========================+=======+=======+===============+
| ``Value``              | \*    | \*    |               |
+------------------------+-------+-------+---------------+
| ``Min``                | \*    |       |               |
+------------------------+-------+-------+---------------+
| ``Max``                | \*    |       |               |
+------------------------+-------+-------+---------------+
| ``HasInc``             | \*    |       |               |
+------------------------+-------+-------+---------------+
| ``IncMode``            | \*    |       |               |
+------------------------+-------+-------+---------------+
| ``Inc``                | \*    |       |               |
+------------------------+-------+-------+---------------+
| ``Representation``     | \*    |       |               |
+------------------------+-------+-------+---------------+
| ``DisplayPrecision``   | \*    |       |               |
+------------------------+-------+-------+---------------+
| ``Unit``               | \*    |       |               |
+------------------------+-------+-------+---------------+
| ``DisplayNotation``    | \*    |       |               |
+------------------------+-------+-------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Value*
^^^^^^^^^^^^^^^^

``System.Double Value``

Property *Min*
^^^^^^^^^^^^^^

``System.Double Min``

Property *Max*
^^^^^^^^^^^^^^

``System.Double Max``

Property *HasInc*
^^^^^^^^^^^^^^^^^

``System.Boolean HasInc``

Property *IncMode*
^^^^^^^^^^^^^^^^^^

``CameraParameterBase.IncModeType IncMode``

Property *Inc*
^^^^^^^^^^^^^^

``System.Double Inc``

Property *Representation*
^^^^^^^^^^^^^^^^^^^^^^^^^

``CameraParameterBase.RepresentationType Representation``

Property *DisplayPrecision*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Int64 DisplayPrecision``

Property *Unit*
^^^^^^^^^^^^^^^

``System.String Unit``

Property *DisplayNotation*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``CameraParameterBase.DisplayNotationType DisplayNotation``
