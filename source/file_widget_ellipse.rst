Class *WidgetEllipse*
---------------------

A widget\_ellipse displays an ellipse on a surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetEllipse** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetEllipse** contains the following properties:

+----------------+-------+-------+-------------------------------+
| Property       | Get   | Set   | Description                   |
+================+=======+=======+===============================+
| ``Position``   | \*    | \*    | The position of the widget.   |
+----------------+-------+-------+-------------------------------+
| ``Outline``    | \*    | \*    | The outline of the widget.    |
+----------------+-------+-------+-------------------------------+
| ``Fill``       | \*    | \*    | The fill of the widget.       |
+----------------+-------+-------+-------------------------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_ellipse. These settings affect the look and the behavior of the widget\_ellipse.

The widget\_ellipse supports interactivity via the widget\_interactive base class, i.e. it can be selected, move around. In general it can react on mouse and keyboard interactions. This widget supports layout.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetEllipse*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetEllipse()``

Default constructor.

The default constructor creates a widget with default settings. By default the ellipse has zero radii at the origin, the outline is one-pixel wide black and the fill is black transparent.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``Ellipse Position``

The position of the widget.

The position of the widget\_ellipse is specified with an ellipse<>.

Property *Outline*
^^^^^^^^^^^^^^^^^^

``PenByte Outline``

The outline of the widget.

The outline of the widget\_ellipse is specified with a pen<>.

Property *Fill*
^^^^^^^^^^^^^^^

``SolidColorBrushByte Fill``

The fill of the widget.

The fill of the widget\_ellipse is specified with a brush.
