Class *TweeningSinusoidal4*
---------------------------

Calculating functor that calculates a sinusoidal tweening.

**Namespace:** Ngi

**Module:** ImageProcessing

Description
~~~~~~~~~~~

The function used for the tweening is a curve built from tweening\_sinusoidal\_1 on the left half and tweening\_sinusoidal\_2 on the right half.

This function starts and ends with a slope of 0 and has a slope of 1 in the middle.

This functor is used for the preset functions.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *TweeningSinusoidal4*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TweeningSinusoidal4()``

Default Constructor.
