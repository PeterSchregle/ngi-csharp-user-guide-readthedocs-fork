Class *FreemanCode*
-------------------

Freeman code representation.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **FreemanCode** implements the following interfaces:

+-----------------------------+
| Interface                   |
+=============================+
| ``IEquatableFreemanCode``   |
+-----------------------------+
| ``ISerializable``           |
+-----------------------------+

The class **FreemanCode** contains the following properties:

+-----------------+-------+-------+-----------------------------------------------------------------------------------------------------+
| Property        | Get   | Set   | Description                                                                                         |
+=================+=======+=======+=====================================================================================================+
| ``Dir``         | \*    | \*    | The direction of the freeman code.                                                                  |
+-----------------+-------+-------+-----------------------------------------------------------------------------------------------------+
| ``MaskedDir``   | \*    |       | Returns the masked direction that converts the freeman code constrained to the range from 0 to 7.   |
+-----------------+-------+-------+-----------------------------------------------------------------------------------------------------+
| ``Dx``          | \*    |       | Get the horizontal displacement that corresponds to this direction.                                 |
+-----------------+-------+-------+-----------------------------------------------------------------------------------------------------+
| ``Dy``          | \*    |       | Get the vertical displacement that corresponds to this direction.                                   |
+-----------------+-------+-------+-----------------------------------------------------------------------------------------------------+
| ``Even``        | \*    |       | True for even directions and false for odd directions.                                              |
+-----------------+-------+-------+-----------------------------------------------------------------------------------------------------+
| ``Odd``         | \*    |       | True for odd directions and false for even directions.                                              |
+-----------------+-------+-------+-----------------------------------------------------------------------------------------------------+

The class **FreemanCode** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

A Freeman code is a representation of a direction, according to the following scheme:

++++ \| 3 \| 2 \| 1 \| ++++ \| 4 \| x \| 0 \| ++++ \| 5 \| 6 \| 7 \| ++++

A Freeman code thus represents the following directions: East (0), North-East (1), North(2), North-West (3), West (4), South-West (5), South (6), and South-East (7).

In addition to the Freeman code representation per se, this type also provides some utility functions.

Among other uses, Freeman codes are the basis for chain codes.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *FreemanCode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``FreemanCode()``

Default constructor, defaults to east (0).

Constructors
~~~~~~~~~~~~

Constructor *FreemanCode*
^^^^^^^^^^^^^^^^^^^^^^^^^

``FreemanCode(System.Int32 dir)``

Constructor.

The constructor has the following parameters:

+-------------+--------------------+------------------+
| Parameter   | Type               | Description      |
+=============+====================+==================+
| ``dir``     | ``System.Int32``   | The direction.   |
+-------------+--------------------+------------------+

Properties
~~~~~~~~~~

Property *Dir*
^^^^^^^^^^^^^^

``System.Int32 Dir``

The direction of the freeman code.

East := 0, North-East := 1, North := 2, North-West := 3, West := 4, South-West := 5, South := 6, South-East := 7.

Property *MaskedDir*
^^^^^^^^^^^^^^^^^^^^

``System.Byte MaskedDir``

Returns the masked direction that converts the freeman code constrained to the range from 0 to 7.

Property *Dx*
^^^^^^^^^^^^^

``System.Int32 Dx``

Get the horizontal displacement that corresponds to this direction.

Depending on the direction, the horizontal displacement can be -1, 0 or +1.

Property *Dy*
^^^^^^^^^^^^^

``System.Int32 Dy``

Get the vertical displacement that corresponds to this direction.

Depending on the direction, the vertical displacement can be -1, 0 or +1.

The vertical displacement corresponding to this direction.

Property *Even*
^^^^^^^^^^^^^^^

``System.Boolean Even``

True for even directions and false for odd directions.

Orthogonal directions (East, North, West, South) return true, diagonal directions (North-East, North-West, South-West, South-East) return false.

Property *Odd*
^^^^^^^^^^^^^^

``System.Boolean Odd``

True for odd directions and false for even directions.

Diagonal directions (North-East, North-West, South-West, South-East) return true, orthogonal directions (East, North, West, South) return false.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
