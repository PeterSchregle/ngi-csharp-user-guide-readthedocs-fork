Class *RingEnumerator*
----------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **RingEnumerator** implements the following interfaces:

+-----------------------+
| Interface             |
+=======================+
| ``IEnumerator``       |
+-----------------------+
| ``IEnumeratorRing``   |
+-----------------------+

The class **RingEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **RingEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Ring Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
