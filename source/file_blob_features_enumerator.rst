Class *BlobFeaturesEnumerator*
------------------------------

**Namespace:** Ngi

**Module:** AnalysisMeasuring

The class **BlobFeaturesEnumerator** implements the following interfaces:

+------------------------------------------------------------+
| Interface                                                  |
+============================================================+
| ``IEnumerator``                                            |
+------------------------------------------------------------+
| ``IEnumeratorBlobFeaturesEnumeratorBlobFeaturesVariant``   |
+------------------------------------------------------------+

The class **BlobFeaturesEnumerator** contains the following variant parameters:

+--------------------+-----------------------------------------+
| Variant            | Description                             |
+====================+=========================================+
| ``BlobFeatures``   | TODO no brief description for variant   |
+--------------------+-----------------------------------------+

The class **BlobFeaturesEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **BlobFeaturesEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *BlobFeatures*
^^^^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **BlobFeatures** has the following types:

+------------------------------+
| Type                         |
+==============================+
| ``BlobFeaturesByte``         |
+------------------------------+
| ``BlobFeaturesUInt16``       |
+------------------------------+
| ``BlobFeaturesUInt32``       |
+------------------------------+
| ``BlobFeaturesDouble``       |
+------------------------------+
| ``BlobFeaturesRgbByte``      |
+------------------------------+
| ``BlobFeaturesRgbUInt16``    |
+------------------------------+
| ``BlobFeaturesRgbUInt32``    |
+------------------------------+
| ``BlobFeaturesRgbDouble``    |
+------------------------------+
| ``BlobFeaturesRgbaByte``     |
+------------------------------+
| ``BlobFeaturesRgbaUInt16``   |
+------------------------------+
| ``BlobFeaturesRgbaUInt32``   |
+------------------------------+
| ``BlobFeaturesRgbaDouble``   |
+------------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``System.Object Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
