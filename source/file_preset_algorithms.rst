Class *PresetAlgorithms*
------------------------

Image preset functions.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **PresetAlgorithms** contains the following enumerations:

+------------------------+------------------------------+
| Enumeration            | Description                  |
+========================+==============================+
| ``TweeningFunction``   | TODO documentation missing   |
+------------------------+------------------------------+

Description
~~~~~~~~~~~

The class contains a group of image preset functions. It includes functions to fill an image region with different pixels (constant, gradient, checkerboard, noise).

Static Methods
~~~~~~~~~~~~~~

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageByte ConstantFill(System.Byte value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+-------------------+---------------+
| Parameter   | Type              | Description   |
+=============+===================+===============+
| ``value``   | ``System.Byte``   |               |
+-------------+-------------------+---------------+
| ``size``    | ``Extent3d``      |               |
+-------------+-------------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 ConstantFill(System.UInt16 value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``value``   | ``System.UInt16``   |               |
+-------------+---------------------+---------------+
| ``size``    | ``Extent3d``        |               |
+-------------+---------------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 ConstantFill(System.UInt32 value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``value``   | ``System.UInt32``   |               |
+-------------+---------------------+---------------+
| ``size``    | ``Extent3d``        |               |
+-------------+---------------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageDouble ConstantFill(System.Double value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``value``   | ``System.Double``   |               |
+-------------+---------------------+---------------+
| ``size``    | ``Extent3d``        |               |
+-------------+---------------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte ConstantFill(RgbByte value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``value``   | ``RgbByte``    |               |
+-------------+----------------+---------------+
| ``size``    | ``Extent3d``   |               |
+-------------+----------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 ConstantFill(RgbUInt16 value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+-----------------+---------------+
| Parameter   | Type            | Description   |
+=============+=================+===============+
| ``value``   | ``RgbUInt16``   |               |
+-------------+-----------------+---------------+
| ``size``    | ``Extent3d``    |               |
+-------------+-----------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 ConstantFill(RgbUInt32 value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+-----------------+---------------+
| Parameter   | Type            | Description   |
+=============+=================+===============+
| ``value``   | ``RgbUInt32``   |               |
+-------------+-----------------+---------------+
| ``size``    | ``Extent3d``    |               |
+-------------+-----------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble ConstantFill(RgbDouble value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+-----------------+---------------+
| Parameter   | Type            | Description   |
+=============+=================+===============+
| ``value``   | ``RgbDouble``   |               |
+-------------+-----------------+---------------+
| ``size``    | ``Extent3d``    |               |
+-------------+-----------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte ConstantFill(RgbaByte value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``value``   | ``RgbaByte``   |               |
+-------------+----------------+---------------+
| ``size``    | ``Extent3d``   |               |
+-------------+----------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 ConstantFill(RgbaUInt16 value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``value``   | ``RgbaUInt16``   |               |
+-------------+------------------+---------------+
| ``size``    | ``Extent3d``     |               |
+-------------+------------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 ConstantFill(RgbaUInt32 value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``value``   | ``RgbaUInt32``   |               |
+-------------+------------------+---------------+
| ``size``    | ``Extent3d``     |               |
+-------------+------------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble ConstantFill(RgbaDouble value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``value``   | ``RgbaDouble``   |               |
+-------------+------------------+---------------+
| ``size``    | ``Extent3d``     |               |
+-------------+------------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte ConstantFill(HlsByte value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``value``   | ``HlsByte``    |               |
+-------------+----------------+---------------+
| ``size``    | ``Extent3d``   |               |
+-------------+----------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 ConstantFill(HlsUInt16 value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+-----------------+---------------+
| Parameter   | Type            | Description   |
+=============+=================+===============+
| ``value``   | ``HlsUInt16``   |               |
+-------------+-----------------+---------------+
| ``size``    | ``Extent3d``    |               |
+-------------+-----------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble ConstantFill(HlsDouble value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+-----------------+---------------+
| Parameter   | Type            | Description   |
+=============+=================+===============+
| ``value``   | ``HlsDouble``   |               |
+-------------+-----------------+---------------+
| ``size``    | ``Extent3d``    |               |
+-------------+-----------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte ConstantFill(HsiByte value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``value``   | ``HsiByte``    |               |
+-------------+----------------+---------------+
| ``size``    | ``Extent3d``   |               |
+-------------+----------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 ConstantFill(HsiUInt16 value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+-----------------+---------------+
| Parameter   | Type            | Description   |
+=============+=================+===============+
| ``value``   | ``HsiUInt16``   |               |
+-------------+-----------------+---------------+
| ``size``    | ``Extent3d``    |               |
+-------------+-----------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble ConstantFill(HsiDouble value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+-----------------+---------------+
| Parameter   | Type            | Description   |
+=============+=================+===============+
| ``value``   | ``HsiDouble``   |               |
+-------------+-----------------+---------------+
| ``size``    | ``Extent3d``    |               |
+-------------+-----------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte ConstantFill(LabByte value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``value``   | ``LabByte``    |               |
+-------------+----------------+---------------+
| ``size``    | ``Extent3d``   |               |
+-------------+----------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 ConstantFill(LabUInt16 value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+-----------------+---------------+
| Parameter   | Type            | Description   |
+=============+=================+===============+
| ``value``   | ``LabUInt16``   |               |
+-------------+-----------------+---------------+
| ``size``    | ``Extent3d``    |               |
+-------------+-----------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble ConstantFill(LabDouble value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+-----------------+---------------+
| Parameter   | Type            | Description   |
+=============+=================+===============+
| ``value``   | ``LabDouble``   |               |
+-------------+-----------------+---------------+
| ``size``    | ``Extent3d``    |               |
+-------------+-----------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte ConstantFill(XyzByte value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``value``   | ``XyzByte``    |               |
+-------------+----------------+---------------+
| ``size``    | ``Extent3d``   |               |
+-------------+----------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 ConstantFill(XyzUInt16 value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+-----------------+---------------+
| Parameter   | Type            | Description   |
+=============+=================+===============+
| ``value``   | ``XyzUInt16``   |               |
+-------------+-----------------+---------------+
| ``size``    | ``Extent3d``    |               |
+-------------+-----------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble ConstantFill(XyzDouble value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+-----------------+---------------+
| Parameter   | Type            | Description   |
+=============+=================+===============+
| ``value``   | ``XyzDouble``   |               |
+-------------+-----------------+---------------+
| ``size``    | ``Extent3d``    |               |
+-------------+-----------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``Image ConstantFill(object value, Extent3d size)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``value``   | ``object``     |               |
+-------------+----------------+---------------+
| ``size``    | ``Extent3d``   |               |
+-------------+----------------+---------------+

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageByte ConstantFill(Region region, System.Byte value, Extent3d size, System.Byte background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``region``       | ``Region``        |               |
+------------------+-------------------+---------------+
| ``value``        | ``System.Byte``   |               |
+------------------+-------------------+---------------+
| ``size``         | ``Extent3d``      |               |
+------------------+-------------------+---------------+
| ``background``   | ``System.Byte``   |               |
+------------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 ConstantFill(Region region, System.UInt16 value, Extent3d size, System.UInt16 background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+---------------------+---------------+
| Parameter        | Type                | Description   |
+==================+=====================+===============+
| ``region``       | ``Region``          |               |
+------------------+---------------------+---------------+
| ``value``        | ``System.UInt16``   |               |
+------------------+---------------------+---------------+
| ``size``         | ``Extent3d``        |               |
+------------------+---------------------+---------------+
| ``background``   | ``System.UInt16``   |               |
+------------------+---------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 ConstantFill(Region region, System.UInt32 value, Extent3d size, System.UInt32 background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+---------------------+---------------+
| Parameter        | Type                | Description   |
+==================+=====================+===============+
| ``region``       | ``Region``          |               |
+------------------+---------------------+---------------+
| ``value``        | ``System.UInt32``   |               |
+------------------+---------------------+---------------+
| ``size``         | ``Extent3d``        |               |
+------------------+---------------------+---------------+
| ``background``   | ``System.UInt32``   |               |
+------------------+---------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageDouble ConstantFill(Region region, System.Double value, Extent3d size, System.Double background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+---------------------+---------------+
| Parameter        | Type                | Description   |
+==================+=====================+===============+
| ``region``       | ``Region``          |               |
+------------------+---------------------+---------------+
| ``value``        | ``System.Double``   |               |
+------------------+---------------------+---------------+
| ``size``         | ``Extent3d``        |               |
+------------------+---------------------+---------------+
| ``background``   | ``System.Double``   |               |
+------------------+---------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte ConstantFill(Region region, RgbByte value, Extent3d size, RgbByte background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+----------------+---------------+
| Parameter        | Type           | Description   |
+==================+================+===============+
| ``region``       | ``Region``     |               |
+------------------+----------------+---------------+
| ``value``        | ``RgbByte``    |               |
+------------------+----------------+---------------+
| ``size``         | ``Extent3d``   |               |
+------------------+----------------+---------------+
| ``background``   | ``RgbByte``    |               |
+------------------+----------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 ConstantFill(Region region, RgbUInt16 value, Extent3d size, RgbUInt16 background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+-----------------+---------------+
| Parameter        | Type            | Description   |
+==================+=================+===============+
| ``region``       | ``Region``      |               |
+------------------+-----------------+---------------+
| ``value``        | ``RgbUInt16``   |               |
+------------------+-----------------+---------------+
| ``size``         | ``Extent3d``    |               |
+------------------+-----------------+---------------+
| ``background``   | ``RgbUInt16``   |               |
+------------------+-----------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 ConstantFill(Region region, RgbUInt32 value, Extent3d size, RgbUInt32 background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+-----------------+---------------+
| Parameter        | Type            | Description   |
+==================+=================+===============+
| ``region``       | ``Region``      |               |
+------------------+-----------------+---------------+
| ``value``        | ``RgbUInt32``   |               |
+------------------+-----------------+---------------+
| ``size``         | ``Extent3d``    |               |
+------------------+-----------------+---------------+
| ``background``   | ``RgbUInt32``   |               |
+------------------+-----------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble ConstantFill(Region region, RgbDouble value, Extent3d size, RgbDouble background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+-----------------+---------------+
| Parameter        | Type            | Description   |
+==================+=================+===============+
| ``region``       | ``Region``      |               |
+------------------+-----------------+---------------+
| ``value``        | ``RgbDouble``   |               |
+------------------+-----------------+---------------+
| ``size``         | ``Extent3d``    |               |
+------------------+-----------------+---------------+
| ``background``   | ``RgbDouble``   |               |
+------------------+-----------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte ConstantFill(Region region, RgbaByte value, Extent3d size, RgbaByte background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+----------------+---------------+
| Parameter        | Type           | Description   |
+==================+================+===============+
| ``region``       | ``Region``     |               |
+------------------+----------------+---------------+
| ``value``        | ``RgbaByte``   |               |
+------------------+----------------+---------------+
| ``size``         | ``Extent3d``   |               |
+------------------+----------------+---------------+
| ``background``   | ``RgbaByte``   |               |
+------------------+----------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 ConstantFill(Region region, RgbaUInt16 value, Extent3d size, RgbaUInt16 background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+------------------+---------------+
| Parameter        | Type             | Description   |
+==================+==================+===============+
| ``region``       | ``Region``       |               |
+------------------+------------------+---------------+
| ``value``        | ``RgbaUInt16``   |               |
+------------------+------------------+---------------+
| ``size``         | ``Extent3d``     |               |
+------------------+------------------+---------------+
| ``background``   | ``RgbaUInt16``   |               |
+------------------+------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 ConstantFill(Region region, RgbaUInt32 value, Extent3d size, RgbaUInt32 background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+------------------+---------------+
| Parameter        | Type             | Description   |
+==================+==================+===============+
| ``region``       | ``Region``       |               |
+------------------+------------------+---------------+
| ``value``        | ``RgbaUInt32``   |               |
+------------------+------------------+---------------+
| ``size``         | ``Extent3d``     |               |
+------------------+------------------+---------------+
| ``background``   | ``RgbaUInt32``   |               |
+------------------+------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble ConstantFill(Region region, RgbaDouble value, Extent3d size, RgbaDouble background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+------------------+---------------+
| Parameter        | Type             | Description   |
+==================+==================+===============+
| ``region``       | ``Region``       |               |
+------------------+------------------+---------------+
| ``value``        | ``RgbaDouble``   |               |
+------------------+------------------+---------------+
| ``size``         | ``Extent3d``     |               |
+------------------+------------------+---------------+
| ``background``   | ``RgbaDouble``   |               |
+------------------+------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte ConstantFill(Region region, HlsByte value, Extent3d size, HlsByte background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+----------------+---------------+
| Parameter        | Type           | Description   |
+==================+================+===============+
| ``region``       | ``Region``     |               |
+------------------+----------------+---------------+
| ``value``        | ``HlsByte``    |               |
+------------------+----------------+---------------+
| ``size``         | ``Extent3d``   |               |
+------------------+----------------+---------------+
| ``background``   | ``HlsByte``    |               |
+------------------+----------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 ConstantFill(Region region, HlsUInt16 value, Extent3d size, HlsUInt16 background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+-----------------+---------------+
| Parameter        | Type            | Description   |
+==================+=================+===============+
| ``region``       | ``Region``      |               |
+------------------+-----------------+---------------+
| ``value``        | ``HlsUInt16``   |               |
+------------------+-----------------+---------------+
| ``size``         | ``Extent3d``    |               |
+------------------+-----------------+---------------+
| ``background``   | ``HlsUInt16``   |               |
+------------------+-----------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble ConstantFill(Region region, HlsDouble value, Extent3d size, HlsDouble background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+-----------------+---------------+
| Parameter        | Type            | Description   |
+==================+=================+===============+
| ``region``       | ``Region``      |               |
+------------------+-----------------+---------------+
| ``value``        | ``HlsDouble``   |               |
+------------------+-----------------+---------------+
| ``size``         | ``Extent3d``    |               |
+------------------+-----------------+---------------+
| ``background``   | ``HlsDouble``   |               |
+------------------+-----------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte ConstantFill(Region region, HsiByte value, Extent3d size, HsiByte background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+----------------+---------------+
| Parameter        | Type           | Description   |
+==================+================+===============+
| ``region``       | ``Region``     |               |
+------------------+----------------+---------------+
| ``value``        | ``HsiByte``    |               |
+------------------+----------------+---------------+
| ``size``         | ``Extent3d``   |               |
+------------------+----------------+---------------+
| ``background``   | ``HsiByte``    |               |
+------------------+----------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 ConstantFill(Region region, HsiUInt16 value, Extent3d size, HsiUInt16 background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+-----------------+---------------+
| Parameter        | Type            | Description   |
+==================+=================+===============+
| ``region``       | ``Region``      |               |
+------------------+-----------------+---------------+
| ``value``        | ``HsiUInt16``   |               |
+------------------+-----------------+---------------+
| ``size``         | ``Extent3d``    |               |
+------------------+-----------------+---------------+
| ``background``   | ``HsiUInt16``   |               |
+------------------+-----------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble ConstantFill(Region region, HsiDouble value, Extent3d size, HsiDouble background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+-----------------+---------------+
| Parameter        | Type            | Description   |
+==================+=================+===============+
| ``region``       | ``Region``      |               |
+------------------+-----------------+---------------+
| ``value``        | ``HsiDouble``   |               |
+------------------+-----------------+---------------+
| ``size``         | ``Extent3d``    |               |
+------------------+-----------------+---------------+
| ``background``   | ``HsiDouble``   |               |
+------------------+-----------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte ConstantFill(Region region, LabByte value, Extent3d size, LabByte background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+----------------+---------------+
| Parameter        | Type           | Description   |
+==================+================+===============+
| ``region``       | ``Region``     |               |
+------------------+----------------+---------------+
| ``value``        | ``LabByte``    |               |
+------------------+----------------+---------------+
| ``size``         | ``Extent3d``   |               |
+------------------+----------------+---------------+
| ``background``   | ``LabByte``    |               |
+------------------+----------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 ConstantFill(Region region, LabUInt16 value, Extent3d size, LabUInt16 background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+-----------------+---------------+
| Parameter        | Type            | Description   |
+==================+=================+===============+
| ``region``       | ``Region``      |               |
+------------------+-----------------+---------------+
| ``value``        | ``LabUInt16``   |               |
+------------------+-----------------+---------------+
| ``size``         | ``Extent3d``    |               |
+------------------+-----------------+---------------+
| ``background``   | ``LabUInt16``   |               |
+------------------+-----------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble ConstantFill(Region region, LabDouble value, Extent3d size, LabDouble background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+-----------------+---------------+
| Parameter        | Type            | Description   |
+==================+=================+===============+
| ``region``       | ``Region``      |               |
+------------------+-----------------+---------------+
| ``value``        | ``LabDouble``   |               |
+------------------+-----------------+---------------+
| ``size``         | ``Extent3d``    |               |
+------------------+-----------------+---------------+
| ``background``   | ``LabDouble``   |               |
+------------------+-----------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte ConstantFill(Region region, XyzByte value, Extent3d size, XyzByte background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+----------------+---------------+
| Parameter        | Type           | Description   |
+==================+================+===============+
| ``region``       | ``Region``     |               |
+------------------+----------------+---------------+
| ``value``        | ``XyzByte``    |               |
+------------------+----------------+---------------+
| ``size``         | ``Extent3d``   |               |
+------------------+----------------+---------------+
| ``background``   | ``XyzByte``    |               |
+------------------+----------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 ConstantFill(Region region, XyzUInt16 value, Extent3d size, XyzUInt16 background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+-----------------+---------------+
| Parameter        | Type            | Description   |
+==================+=================+===============+
| ``region``       | ``Region``      |               |
+------------------+-----------------+---------------+
| ``value``        | ``XyzUInt16``   |               |
+------------------+-----------------+---------------+
| ``size``         | ``Extent3d``    |               |
+------------------+-----------------+---------------+
| ``background``   | ``XyzUInt16``   |               |
+------------------+-----------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble ConstantFill(Region region, XyzDouble value, Extent3d size, XyzDouble background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+-----------------+---------------+
| Parameter        | Type            | Description   |
+==================+=================+===============+
| ``region``       | ``Region``      |               |
+------------------+-----------------+---------------+
| ``value``        | ``XyzDouble``   |               |
+------------------+-----------------+---------------+
| ``size``         | ``Extent3d``    |               |
+------------------+-----------------+---------------+
| ``background``   | ``XyzDouble``   |               |
+------------------+-----------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *ConstantFill*
^^^^^^^^^^^^^^^^^^^^^

``Image ConstantFill(Region region, object value, Extent3d size, object background)``

Preset an image with a constant value.

The method **ConstantFill** has the following parameters:

+------------------+----------------+---------------+
| Parameter        | Type           | Description   |
+==================+================+===============+
| ``region``       | ``Region``     |               |
+------------------+----------------+---------------+
| ``value``        | ``object``     |               |
+------------------+----------------+---------------+
| ``size``         | ``Extent3d``   |               |
+------------------+----------------+---------------+
| ``background``   | ``object``     |               |
+------------------+----------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageByte NoiseFill(System.Byte dark, System.Byte bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``System.Byte``                         |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``System.Byte``                         |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageUInt16 NoiseFill(System.UInt16 dark, System.UInt16 bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``System.UInt16``                       |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``System.UInt16``                       |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageUInt32 NoiseFill(System.UInt32 dark, System.UInt32 bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageDouble NoiseFill(System.Double dark, System.Double bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageRgbByte NoiseFill(RgbByte dark, RgbByte bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``RgbByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 NoiseFill(RgbUInt16 dark, RgbUInt16 bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``RgbUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 NoiseFill(RgbUInt32 dark, RgbUInt32 bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``RgbUInt32``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbUInt32``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageRgbDouble NoiseFill(RgbDouble dark, RgbDouble bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``RgbDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageRgbaByte NoiseFill(RgbaByte dark, RgbaByte bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``RgbaByte``                            |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbaByte``                            |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 NoiseFill(RgbaUInt16 dark, RgbaUInt16 bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``RgbaUInt16``                          |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbaUInt16``                          |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 NoiseFill(RgbaUInt32 dark, RgbaUInt32 bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``RgbaUInt32``                          |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbaUInt32``                          |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble NoiseFill(RgbaDouble dark, RgbaDouble bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``RgbaDouble``                          |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbaDouble``                          |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageHlsByte NoiseFill(HlsByte dark, HlsByte bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``HlsByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HlsByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 NoiseFill(HlsUInt16 dark, HlsUInt16 bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``HlsUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HlsUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageHlsDouble NoiseFill(HlsDouble dark, HlsDouble bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``HlsDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HlsDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageHsiByte NoiseFill(HsiByte dark, HsiByte bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``HsiByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HsiByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 NoiseFill(HsiUInt16 dark, HsiUInt16 bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``HsiUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HsiUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageHsiDouble NoiseFill(HsiDouble dark, HsiDouble bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``HsiDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HsiDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageLabByte NoiseFill(LabByte dark, LabByte bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``LabByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``LabByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 NoiseFill(LabUInt16 dark, LabUInt16 bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``LabUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``LabUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageLabDouble NoiseFill(LabDouble dark, LabDouble bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``LabDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``LabDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageXyzByte NoiseFill(XyzByte dark, XyzByte bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``XyzByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``XyzByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 NoiseFill(XyzUInt16 dark, XyzUInt16 bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``XyzUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``XyzUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageXyzDouble NoiseFill(XyzDouble dark, XyzDouble bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``XyzDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``XyzDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``Image NoiseFill(object dark, object bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``dark``       | ``object``                              |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``object``                              |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``seed``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageByte NoiseFill(Region region, System.Byte dark, System.Byte bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, System.Byte background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``System.Byte``                         |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``System.Byte``                         |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``System.Byte``                         |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageUInt16 NoiseFill(Region region, System.UInt16 dark, System.UInt16 bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, System.UInt16 background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``System.UInt16``                       |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``System.UInt16``                       |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``System.UInt16``                       |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageUInt32 NoiseFill(Region region, System.UInt32 dark, System.UInt32 bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, System.UInt32 background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageDouble NoiseFill(Region region, System.Double dark, System.Double bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, System.Double background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageRgbByte NoiseFill(Region region, RgbByte dark, RgbByte bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, RgbByte background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 NoiseFill(Region region, RgbUInt16 dark, RgbUInt16 bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, RgbUInt16 background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 NoiseFill(Region region, RgbUInt32 dark, RgbUInt32 bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, RgbUInt32 background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbUInt32``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbUInt32``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbUInt32``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageRgbDouble NoiseFill(Region region, RgbDouble dark, RgbDouble bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, RgbDouble background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageRgbaByte NoiseFill(Region region, RgbaByte dark, RgbaByte bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, RgbaByte background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbaByte``                            |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbaByte``                            |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbaByte``                            |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 NoiseFill(Region region, RgbaUInt16 dark, RgbaUInt16 bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, RgbaUInt16 background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbaUInt16``                          |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbaUInt16``                          |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbaUInt16``                          |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 NoiseFill(Region region, RgbaUInt32 dark, RgbaUInt32 bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, RgbaUInt32 background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbaUInt32``                          |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbaUInt32``                          |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbaUInt32``                          |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble NoiseFill(Region region, RgbaDouble dark, RgbaDouble bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, RgbaDouble background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbaDouble``                          |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbaDouble``                          |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbaDouble``                          |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageHlsByte NoiseFill(Region region, HlsByte dark, HlsByte bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, HlsByte background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HlsByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HlsByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HlsByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 NoiseFill(Region region, HlsUInt16 dark, HlsUInt16 bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, HlsUInt16 background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HlsUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HlsUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HlsUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageHlsDouble NoiseFill(Region region, HlsDouble dark, HlsDouble bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, HlsDouble background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HlsDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HlsDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HlsDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageHsiByte NoiseFill(Region region, HsiByte dark, HsiByte bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, HsiByte background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HsiByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HsiByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HsiByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 NoiseFill(Region region, HsiUInt16 dark, HsiUInt16 bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, HsiUInt16 background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HsiUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HsiUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HsiUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageHsiDouble NoiseFill(Region region, HsiDouble dark, HsiDouble bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, HsiDouble background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HsiDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HsiDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HsiDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageLabByte NoiseFill(Region region, LabByte dark, LabByte bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, LabByte background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``LabByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``LabByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``LabByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 NoiseFill(Region region, LabUInt16 dark, LabUInt16 bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, LabUInt16 background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``LabUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``LabUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``LabUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageLabDouble NoiseFill(Region region, LabDouble dark, LabDouble bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, LabDouble background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``LabDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``LabDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``LabDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageXyzByte NoiseFill(Region region, XyzByte dark, XyzByte bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, XyzByte background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``XyzByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``XyzByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``XyzByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 NoiseFill(Region region, XyzUInt16 dark, XyzUInt16 bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, XyzUInt16 background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``XyzUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``XyzUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``XyzUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``ImageXyzDouble NoiseFill(Region region, XyzDouble dark, XyzDouble bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, XyzDouble background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``XyzDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``XyzDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``XyzDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *NoiseFill*
^^^^^^^^^^^^^^^^^^

``Image NoiseFill(Region region, object dark, object bright, Extent3d size, System.UInt32 seed, PresetAlgorithms.TweeningFunction tweening, object background)``

Preset an image with pseudo random values.

The method **NoiseFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``object``                              |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``object``                              |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``seed``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``object``                              |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageByte CheckerFill(VectorInt32 fieldSize, System.Byte dark, System.Byte bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+-------------------+---------------+
| Parameter       | Type              | Description   |
+=================+===================+===============+
| ``fieldSize``   | ``VectorInt32``   |               |
+-----------------+-------------------+---------------+
| ``dark``        | ``System.Byte``   |               |
+-----------------+-------------------+---------------+
| ``bright``      | ``System.Byte``   |               |
+-----------------+-------------------+---------------+
| ``size``        | ``Extent3d``      |               |
+-----------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 CheckerFill(VectorInt32 fieldSize, System.UInt16 dark, System.UInt16 bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+---------------------+---------------+
| Parameter       | Type                | Description   |
+=================+=====================+===============+
| ``fieldSize``   | ``VectorInt32``     |               |
+-----------------+---------------------+---------------+
| ``dark``        | ``System.UInt16``   |               |
+-----------------+---------------------+---------------+
| ``bright``      | ``System.UInt16``   |               |
+-----------------+---------------------+---------------+
| ``size``        | ``Extent3d``        |               |
+-----------------+---------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 CheckerFill(VectorInt32 fieldSize, System.UInt32 dark, System.UInt32 bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+---------------------+---------------+
| Parameter       | Type                | Description   |
+=================+=====================+===============+
| ``fieldSize``   | ``VectorInt32``     |               |
+-----------------+---------------------+---------------+
| ``dark``        | ``System.UInt32``   |               |
+-----------------+---------------------+---------------+
| ``bright``      | ``System.UInt32``   |               |
+-----------------+---------------------+---------------+
| ``size``        | ``Extent3d``        |               |
+-----------------+---------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageDouble CheckerFill(VectorInt32 fieldSize, System.Double dark, System.Double bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+---------------------+---------------+
| Parameter       | Type                | Description   |
+=================+=====================+===============+
| ``fieldSize``   | ``VectorInt32``     |               |
+-----------------+---------------------+---------------+
| ``dark``        | ``System.Double``   |               |
+-----------------+---------------------+---------------+
| ``bright``      | ``System.Double``   |               |
+-----------------+---------------------+---------------+
| ``size``        | ``Extent3d``        |               |
+-----------------+---------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte CheckerFill(VectorInt32 fieldSize, RgbByte dark, RgbByte bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+-------------------+---------------+
| Parameter       | Type              | Description   |
+=================+===================+===============+
| ``fieldSize``   | ``VectorInt32``   |               |
+-----------------+-------------------+---------------+
| ``dark``        | ``RgbByte``       |               |
+-----------------+-------------------+---------------+
| ``bright``      | ``RgbByte``       |               |
+-----------------+-------------------+---------------+
| ``size``        | ``Extent3d``      |               |
+-----------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 CheckerFill(VectorInt32 fieldSize, RgbUInt16 dark, RgbUInt16 bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+-------------------+---------------+
| Parameter       | Type              | Description   |
+=================+===================+===============+
| ``fieldSize``   | ``VectorInt32``   |               |
+-----------------+-------------------+---------------+
| ``dark``        | ``RgbUInt16``     |               |
+-----------------+-------------------+---------------+
| ``bright``      | ``RgbUInt16``     |               |
+-----------------+-------------------+---------------+
| ``size``        | ``Extent3d``      |               |
+-----------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 CheckerFill(VectorInt32 fieldSize, RgbUInt32 dark, RgbUInt32 bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+-------------------+---------------+
| Parameter       | Type              | Description   |
+=================+===================+===============+
| ``fieldSize``   | ``VectorInt32``   |               |
+-----------------+-------------------+---------------+
| ``dark``        | ``RgbUInt32``     |               |
+-----------------+-------------------+---------------+
| ``bright``      | ``RgbUInt32``     |               |
+-----------------+-------------------+---------------+
| ``size``        | ``Extent3d``      |               |
+-----------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble CheckerFill(VectorInt32 fieldSize, RgbDouble dark, RgbDouble bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+-------------------+---------------+
| Parameter       | Type              | Description   |
+=================+===================+===============+
| ``fieldSize``   | ``VectorInt32``   |               |
+-----------------+-------------------+---------------+
| ``dark``        | ``RgbDouble``     |               |
+-----------------+-------------------+---------------+
| ``bright``      | ``RgbDouble``     |               |
+-----------------+-------------------+---------------+
| ``size``        | ``Extent3d``      |               |
+-----------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte CheckerFill(VectorInt32 fieldSize, RgbaByte dark, RgbaByte bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+-------------------+---------------+
| Parameter       | Type              | Description   |
+=================+===================+===============+
| ``fieldSize``   | ``VectorInt32``   |               |
+-----------------+-------------------+---------------+
| ``dark``        | ``RgbaByte``      |               |
+-----------------+-------------------+---------------+
| ``bright``      | ``RgbaByte``      |               |
+-----------------+-------------------+---------------+
| ``size``        | ``Extent3d``      |               |
+-----------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 CheckerFill(VectorInt32 fieldSize, RgbaUInt16 dark, RgbaUInt16 bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+-------------------+---------------+
| Parameter       | Type              | Description   |
+=================+===================+===============+
| ``fieldSize``   | ``VectorInt32``   |               |
+-----------------+-------------------+---------------+
| ``dark``        | ``RgbaUInt16``    |               |
+-----------------+-------------------+---------------+
| ``bright``      | ``RgbaUInt16``    |               |
+-----------------+-------------------+---------------+
| ``size``        | ``Extent3d``      |               |
+-----------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 CheckerFill(VectorInt32 fieldSize, RgbaUInt32 dark, RgbaUInt32 bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+-------------------+---------------+
| Parameter       | Type              | Description   |
+=================+===================+===============+
| ``fieldSize``   | ``VectorInt32``   |               |
+-----------------+-------------------+---------------+
| ``dark``        | ``RgbaUInt32``    |               |
+-----------------+-------------------+---------------+
| ``bright``      | ``RgbaUInt32``    |               |
+-----------------+-------------------+---------------+
| ``size``        | ``Extent3d``      |               |
+-----------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble CheckerFill(VectorInt32 fieldSize, RgbaDouble dark, RgbaDouble bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+-------------------+---------------+
| Parameter       | Type              | Description   |
+=================+===================+===============+
| ``fieldSize``   | ``VectorInt32``   |               |
+-----------------+-------------------+---------------+
| ``dark``        | ``RgbaDouble``    |               |
+-----------------+-------------------+---------------+
| ``bright``      | ``RgbaDouble``    |               |
+-----------------+-------------------+---------------+
| ``size``        | ``Extent3d``      |               |
+-----------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte CheckerFill(VectorInt32 fieldSize, HlsByte dark, HlsByte bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+-------------------+---------------+
| Parameter       | Type              | Description   |
+=================+===================+===============+
| ``fieldSize``   | ``VectorInt32``   |               |
+-----------------+-------------------+---------------+
| ``dark``        | ``HlsByte``       |               |
+-----------------+-------------------+---------------+
| ``bright``      | ``HlsByte``       |               |
+-----------------+-------------------+---------------+
| ``size``        | ``Extent3d``      |               |
+-----------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 CheckerFill(VectorInt32 fieldSize, HlsUInt16 dark, HlsUInt16 bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+-------------------+---------------+
| Parameter       | Type              | Description   |
+=================+===================+===============+
| ``fieldSize``   | ``VectorInt32``   |               |
+-----------------+-------------------+---------------+
| ``dark``        | ``HlsUInt16``     |               |
+-----------------+-------------------+---------------+
| ``bright``      | ``HlsUInt16``     |               |
+-----------------+-------------------+---------------+
| ``size``        | ``Extent3d``      |               |
+-----------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble CheckerFill(VectorInt32 fieldSize, HlsDouble dark, HlsDouble bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+-------------------+---------------+
| Parameter       | Type              | Description   |
+=================+===================+===============+
| ``fieldSize``   | ``VectorInt32``   |               |
+-----------------+-------------------+---------------+
| ``dark``        | ``HlsDouble``     |               |
+-----------------+-------------------+---------------+
| ``bright``      | ``HlsDouble``     |               |
+-----------------+-------------------+---------------+
| ``size``        | ``Extent3d``      |               |
+-----------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte CheckerFill(VectorInt32 fieldSize, HsiByte dark, HsiByte bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+-------------------+---------------+
| Parameter       | Type              | Description   |
+=================+===================+===============+
| ``fieldSize``   | ``VectorInt32``   |               |
+-----------------+-------------------+---------------+
| ``dark``        | ``HsiByte``       |               |
+-----------------+-------------------+---------------+
| ``bright``      | ``HsiByte``       |               |
+-----------------+-------------------+---------------+
| ``size``        | ``Extent3d``      |               |
+-----------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 CheckerFill(VectorInt32 fieldSize, HsiUInt16 dark, HsiUInt16 bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+-------------------+---------------+
| Parameter       | Type              | Description   |
+=================+===================+===============+
| ``fieldSize``   | ``VectorInt32``   |               |
+-----------------+-------------------+---------------+
| ``dark``        | ``HsiUInt16``     |               |
+-----------------+-------------------+---------------+
| ``bright``      | ``HsiUInt16``     |               |
+-----------------+-------------------+---------------+
| ``size``        | ``Extent3d``      |               |
+-----------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble CheckerFill(VectorInt32 fieldSize, HsiDouble dark, HsiDouble bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+-------------------+---------------+
| Parameter       | Type              | Description   |
+=================+===================+===============+
| ``fieldSize``   | ``VectorInt32``   |               |
+-----------------+-------------------+---------------+
| ``dark``        | ``HsiDouble``     |               |
+-----------------+-------------------+---------------+
| ``bright``      | ``HsiDouble``     |               |
+-----------------+-------------------+---------------+
| ``size``        | ``Extent3d``      |               |
+-----------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageLabByte CheckerFill(VectorInt32 fieldSize, LabByte dark, LabByte bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+-------------------+---------------+
| Parameter       | Type              | Description   |
+=================+===================+===============+
| ``fieldSize``   | ``VectorInt32``   |               |
+-----------------+-------------------+---------------+
| ``dark``        | ``LabByte``       |               |
+-----------------+-------------------+---------------+
| ``bright``      | ``LabByte``       |               |
+-----------------+-------------------+---------------+
| ``size``        | ``Extent3d``      |               |
+-----------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 CheckerFill(VectorInt32 fieldSize, LabUInt16 dark, LabUInt16 bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+-------------------+---------------+
| Parameter       | Type              | Description   |
+=================+===================+===============+
| ``fieldSize``   | ``VectorInt32``   |               |
+-----------------+-------------------+---------------+
| ``dark``        | ``LabUInt16``     |               |
+-----------------+-------------------+---------------+
| ``bright``      | ``LabUInt16``     |               |
+-----------------+-------------------+---------------+
| ``size``        | ``Extent3d``      |               |
+-----------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble CheckerFill(VectorInt32 fieldSize, LabDouble dark, LabDouble bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+-------------------+---------------+
| Parameter       | Type              | Description   |
+=================+===================+===============+
| ``fieldSize``   | ``VectorInt32``   |               |
+-----------------+-------------------+---------------+
| ``dark``        | ``LabDouble``     |               |
+-----------------+-------------------+---------------+
| ``bright``      | ``LabDouble``     |               |
+-----------------+-------------------+---------------+
| ``size``        | ``Extent3d``      |               |
+-----------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte CheckerFill(VectorInt32 fieldSize, XyzByte dark, XyzByte bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+-------------------+---------------+
| Parameter       | Type              | Description   |
+=================+===================+===============+
| ``fieldSize``   | ``VectorInt32``   |               |
+-----------------+-------------------+---------------+
| ``dark``        | ``XyzByte``       |               |
+-----------------+-------------------+---------------+
| ``bright``      | ``XyzByte``       |               |
+-----------------+-------------------+---------------+
| ``size``        | ``Extent3d``      |               |
+-----------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 CheckerFill(VectorInt32 fieldSize, XyzUInt16 dark, XyzUInt16 bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+-------------------+---------------+
| Parameter       | Type              | Description   |
+=================+===================+===============+
| ``fieldSize``   | ``VectorInt32``   |               |
+-----------------+-------------------+---------------+
| ``dark``        | ``XyzUInt16``     |               |
+-----------------+-------------------+---------------+
| ``bright``      | ``XyzUInt16``     |               |
+-----------------+-------------------+---------------+
| ``size``        | ``Extent3d``      |               |
+-----------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble CheckerFill(VectorInt32 fieldSize, XyzDouble dark, XyzDouble bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+-------------------+---------------+
| Parameter       | Type              | Description   |
+=================+===================+===============+
| ``fieldSize``   | ``VectorInt32``   |               |
+-----------------+-------------------+---------------+
| ``dark``        | ``XyzDouble``     |               |
+-----------------+-------------------+---------------+
| ``bright``      | ``XyzDouble``     |               |
+-----------------+-------------------+---------------+
| ``size``        | ``Extent3d``      |               |
+-----------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``Image CheckerFill(VectorInt32 fieldSize, object dark, object bright, Extent3d size)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+-----------------+-------------------+---------------+
| Parameter       | Type              | Description   |
+=================+===================+===============+
| ``fieldSize``   | ``VectorInt32``   |               |
+-----------------+-------------------+---------------+
| ``dark``        | ``object``        |               |
+-----------------+-------------------+---------------+
| ``bright``      | ``object``        |               |
+-----------------+-------------------+---------------+
| ``size``        | ``Extent3d``      |               |
+-----------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageByte CheckerFill(Region region, VectorInt32 fieldSize, System.Byte dark, System.Byte bright, Extent3d size, System.Byte background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``region``       | ``Region``        |               |
+------------------+-------------------+---------------+
| ``fieldSize``    | ``VectorInt32``   |               |
+------------------+-------------------+---------------+
| ``dark``         | ``System.Byte``   |               |
+------------------+-------------------+---------------+
| ``bright``       | ``System.Byte``   |               |
+------------------+-------------------+---------------+
| ``size``         | ``Extent3d``      |               |
+------------------+-------------------+---------------+
| ``background``   | ``System.Byte``   |               |
+------------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 CheckerFill(Region region, VectorInt32 fieldSize, System.UInt16 dark, System.UInt16 bright, Extent3d size, System.UInt16 background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+---------------------+---------------+
| Parameter        | Type                | Description   |
+==================+=====================+===============+
| ``region``       | ``Region``          |               |
+------------------+---------------------+---------------+
| ``fieldSize``    | ``VectorInt32``     |               |
+------------------+---------------------+---------------+
| ``dark``         | ``System.UInt16``   |               |
+------------------+---------------------+---------------+
| ``bright``       | ``System.UInt16``   |               |
+------------------+---------------------+---------------+
| ``size``         | ``Extent3d``        |               |
+------------------+---------------------+---------------+
| ``background``   | ``System.UInt16``   |               |
+------------------+---------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 CheckerFill(Region region, VectorInt32 fieldSize, System.UInt32 dark, System.UInt32 bright, Extent3d size, System.UInt32 background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+---------------------+---------------+
| Parameter        | Type                | Description   |
+==================+=====================+===============+
| ``region``       | ``Region``          |               |
+------------------+---------------------+---------------+
| ``fieldSize``    | ``VectorInt32``     |               |
+------------------+---------------------+---------------+
| ``dark``         | ``System.UInt32``   |               |
+------------------+---------------------+---------------+
| ``bright``       | ``System.UInt32``   |               |
+------------------+---------------------+---------------+
| ``size``         | ``Extent3d``        |               |
+------------------+---------------------+---------------+
| ``background``   | ``System.UInt32``   |               |
+------------------+---------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageDouble CheckerFill(Region region, VectorInt32 fieldSize, System.Double dark, System.Double bright, Extent3d size, System.Double background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+---------------------+---------------+
| Parameter        | Type                | Description   |
+==================+=====================+===============+
| ``region``       | ``Region``          |               |
+------------------+---------------------+---------------+
| ``fieldSize``    | ``VectorInt32``     |               |
+------------------+---------------------+---------------+
| ``dark``         | ``System.Double``   |               |
+------------------+---------------------+---------------+
| ``bright``       | ``System.Double``   |               |
+------------------+---------------------+---------------+
| ``size``         | ``Extent3d``        |               |
+------------------+---------------------+---------------+
| ``background``   | ``System.Double``   |               |
+------------------+---------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte CheckerFill(Region region, VectorInt32 fieldSize, RgbByte dark, RgbByte bright, Extent3d size, RgbByte background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``region``       | ``Region``        |               |
+------------------+-------------------+---------------+
| ``fieldSize``    | ``VectorInt32``   |               |
+------------------+-------------------+---------------+
| ``dark``         | ``RgbByte``       |               |
+------------------+-------------------+---------------+
| ``bright``       | ``RgbByte``       |               |
+------------------+-------------------+---------------+
| ``size``         | ``Extent3d``      |               |
+------------------+-------------------+---------------+
| ``background``   | ``RgbByte``       |               |
+------------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 CheckerFill(Region region, VectorInt32 fieldSize, RgbUInt16 dark, RgbUInt16 bright, Extent3d size, RgbUInt16 background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``region``       | ``Region``        |               |
+------------------+-------------------+---------------+
| ``fieldSize``    | ``VectorInt32``   |               |
+------------------+-------------------+---------------+
| ``dark``         | ``RgbUInt16``     |               |
+------------------+-------------------+---------------+
| ``bright``       | ``RgbUInt16``     |               |
+------------------+-------------------+---------------+
| ``size``         | ``Extent3d``      |               |
+------------------+-------------------+---------------+
| ``background``   | ``RgbUInt16``     |               |
+------------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 CheckerFill(Region region, VectorInt32 fieldSize, RgbUInt32 dark, RgbUInt32 bright, Extent3d size, RgbUInt32 background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``region``       | ``Region``        |               |
+------------------+-------------------+---------------+
| ``fieldSize``    | ``VectorInt32``   |               |
+------------------+-------------------+---------------+
| ``dark``         | ``RgbUInt32``     |               |
+------------------+-------------------+---------------+
| ``bright``       | ``RgbUInt32``     |               |
+------------------+-------------------+---------------+
| ``size``         | ``Extent3d``      |               |
+------------------+-------------------+---------------+
| ``background``   | ``RgbUInt32``     |               |
+------------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble CheckerFill(Region region, VectorInt32 fieldSize, RgbDouble dark, RgbDouble bright, Extent3d size, RgbDouble background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``region``       | ``Region``        |               |
+------------------+-------------------+---------------+
| ``fieldSize``    | ``VectorInt32``   |               |
+------------------+-------------------+---------------+
| ``dark``         | ``RgbDouble``     |               |
+------------------+-------------------+---------------+
| ``bright``       | ``RgbDouble``     |               |
+------------------+-------------------+---------------+
| ``size``         | ``Extent3d``      |               |
+------------------+-------------------+---------------+
| ``background``   | ``RgbDouble``     |               |
+------------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte CheckerFill(Region region, VectorInt32 fieldSize, RgbaByte dark, RgbaByte bright, Extent3d size, RgbaByte background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``region``       | ``Region``        |               |
+------------------+-------------------+---------------+
| ``fieldSize``    | ``VectorInt32``   |               |
+------------------+-------------------+---------------+
| ``dark``         | ``RgbaByte``      |               |
+------------------+-------------------+---------------+
| ``bright``       | ``RgbaByte``      |               |
+------------------+-------------------+---------------+
| ``size``         | ``Extent3d``      |               |
+------------------+-------------------+---------------+
| ``background``   | ``RgbaByte``      |               |
+------------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 CheckerFill(Region region, VectorInt32 fieldSize, RgbaUInt16 dark, RgbaUInt16 bright, Extent3d size, RgbaUInt16 background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``region``       | ``Region``        |               |
+------------------+-------------------+---------------+
| ``fieldSize``    | ``VectorInt32``   |               |
+------------------+-------------------+---------------+
| ``dark``         | ``RgbaUInt16``    |               |
+------------------+-------------------+---------------+
| ``bright``       | ``RgbaUInt16``    |               |
+------------------+-------------------+---------------+
| ``size``         | ``Extent3d``      |               |
+------------------+-------------------+---------------+
| ``background``   | ``RgbaUInt16``    |               |
+------------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 CheckerFill(Region region, VectorInt32 fieldSize, RgbaUInt32 dark, RgbaUInt32 bright, Extent3d size, RgbaUInt32 background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``region``       | ``Region``        |               |
+------------------+-------------------+---------------+
| ``fieldSize``    | ``VectorInt32``   |               |
+------------------+-------------------+---------------+
| ``dark``         | ``RgbaUInt32``    |               |
+------------------+-------------------+---------------+
| ``bright``       | ``RgbaUInt32``    |               |
+------------------+-------------------+---------------+
| ``size``         | ``Extent3d``      |               |
+------------------+-------------------+---------------+
| ``background``   | ``RgbaUInt32``    |               |
+------------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble CheckerFill(Region region, VectorInt32 fieldSize, RgbaDouble dark, RgbaDouble bright, Extent3d size, RgbaDouble background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``region``       | ``Region``        |               |
+------------------+-------------------+---------------+
| ``fieldSize``    | ``VectorInt32``   |               |
+------------------+-------------------+---------------+
| ``dark``         | ``RgbaDouble``    |               |
+------------------+-------------------+---------------+
| ``bright``       | ``RgbaDouble``    |               |
+------------------+-------------------+---------------+
| ``size``         | ``Extent3d``      |               |
+------------------+-------------------+---------------+
| ``background``   | ``RgbaDouble``    |               |
+------------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte CheckerFill(Region region, VectorInt32 fieldSize, HlsByte dark, HlsByte bright, Extent3d size, HlsByte background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``region``       | ``Region``        |               |
+------------------+-------------------+---------------+
| ``fieldSize``    | ``VectorInt32``   |               |
+------------------+-------------------+---------------+
| ``dark``         | ``HlsByte``       |               |
+------------------+-------------------+---------------+
| ``bright``       | ``HlsByte``       |               |
+------------------+-------------------+---------------+
| ``size``         | ``Extent3d``      |               |
+------------------+-------------------+---------------+
| ``background``   | ``HlsByte``       |               |
+------------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 CheckerFill(Region region, VectorInt32 fieldSize, HlsUInt16 dark, HlsUInt16 bright, Extent3d size, HlsUInt16 background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``region``       | ``Region``        |               |
+------------------+-------------------+---------------+
| ``fieldSize``    | ``VectorInt32``   |               |
+------------------+-------------------+---------------+
| ``dark``         | ``HlsUInt16``     |               |
+------------------+-------------------+---------------+
| ``bright``       | ``HlsUInt16``     |               |
+------------------+-------------------+---------------+
| ``size``         | ``Extent3d``      |               |
+------------------+-------------------+---------------+
| ``background``   | ``HlsUInt16``     |               |
+------------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble CheckerFill(Region region, VectorInt32 fieldSize, HlsDouble dark, HlsDouble bright, Extent3d size, HlsDouble background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``region``       | ``Region``        |               |
+------------------+-------------------+---------------+
| ``fieldSize``    | ``VectorInt32``   |               |
+------------------+-------------------+---------------+
| ``dark``         | ``HlsDouble``     |               |
+------------------+-------------------+---------------+
| ``bright``       | ``HlsDouble``     |               |
+------------------+-------------------+---------------+
| ``size``         | ``Extent3d``      |               |
+------------------+-------------------+---------------+
| ``background``   | ``HlsDouble``     |               |
+------------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte CheckerFill(Region region, VectorInt32 fieldSize, HsiByte dark, HsiByte bright, Extent3d size, HsiByte background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``region``       | ``Region``        |               |
+------------------+-------------------+---------------+
| ``fieldSize``    | ``VectorInt32``   |               |
+------------------+-------------------+---------------+
| ``dark``         | ``HsiByte``       |               |
+------------------+-------------------+---------------+
| ``bright``       | ``HsiByte``       |               |
+------------------+-------------------+---------------+
| ``size``         | ``Extent3d``      |               |
+------------------+-------------------+---------------+
| ``background``   | ``HsiByte``       |               |
+------------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 CheckerFill(Region region, VectorInt32 fieldSize, HsiUInt16 dark, HsiUInt16 bright, Extent3d size, HsiUInt16 background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``region``       | ``Region``        |               |
+------------------+-------------------+---------------+
| ``fieldSize``    | ``VectorInt32``   |               |
+------------------+-------------------+---------------+
| ``dark``         | ``HsiUInt16``     |               |
+------------------+-------------------+---------------+
| ``bright``       | ``HsiUInt16``     |               |
+------------------+-------------------+---------------+
| ``size``         | ``Extent3d``      |               |
+------------------+-------------------+---------------+
| ``background``   | ``HsiUInt16``     |               |
+------------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble CheckerFill(Region region, VectorInt32 fieldSize, HsiDouble dark, HsiDouble bright, Extent3d size, HsiDouble background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``region``       | ``Region``        |               |
+------------------+-------------------+---------------+
| ``fieldSize``    | ``VectorInt32``   |               |
+------------------+-------------------+---------------+
| ``dark``         | ``HsiDouble``     |               |
+------------------+-------------------+---------------+
| ``bright``       | ``HsiDouble``     |               |
+------------------+-------------------+---------------+
| ``size``         | ``Extent3d``      |               |
+------------------+-------------------+---------------+
| ``background``   | ``HsiDouble``     |               |
+------------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageLabByte CheckerFill(Region region, VectorInt32 fieldSize, LabByte dark, LabByte bright, Extent3d size, LabByte background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``region``       | ``Region``        |               |
+------------------+-------------------+---------------+
| ``fieldSize``    | ``VectorInt32``   |               |
+------------------+-------------------+---------------+
| ``dark``         | ``LabByte``       |               |
+------------------+-------------------+---------------+
| ``bright``       | ``LabByte``       |               |
+------------------+-------------------+---------------+
| ``size``         | ``Extent3d``      |               |
+------------------+-------------------+---------------+
| ``background``   | ``LabByte``       |               |
+------------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 CheckerFill(Region region, VectorInt32 fieldSize, LabUInt16 dark, LabUInt16 bright, Extent3d size, LabUInt16 background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``region``       | ``Region``        |               |
+------------------+-------------------+---------------+
| ``fieldSize``    | ``VectorInt32``   |               |
+------------------+-------------------+---------------+
| ``dark``         | ``LabUInt16``     |               |
+------------------+-------------------+---------------+
| ``bright``       | ``LabUInt16``     |               |
+------------------+-------------------+---------------+
| ``size``         | ``Extent3d``      |               |
+------------------+-------------------+---------------+
| ``background``   | ``LabUInt16``     |               |
+------------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble CheckerFill(Region region, VectorInt32 fieldSize, LabDouble dark, LabDouble bright, Extent3d size, LabDouble background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``region``       | ``Region``        |               |
+------------------+-------------------+---------------+
| ``fieldSize``    | ``VectorInt32``   |               |
+------------------+-------------------+---------------+
| ``dark``         | ``LabDouble``     |               |
+------------------+-------------------+---------------+
| ``bright``       | ``LabDouble``     |               |
+------------------+-------------------+---------------+
| ``size``         | ``Extent3d``      |               |
+------------------+-------------------+---------------+
| ``background``   | ``LabDouble``     |               |
+------------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte CheckerFill(Region region, VectorInt32 fieldSize, XyzByte dark, XyzByte bright, Extent3d size, XyzByte background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``region``       | ``Region``        |               |
+------------------+-------------------+---------------+
| ``fieldSize``    | ``VectorInt32``   |               |
+------------------+-------------------+---------------+
| ``dark``         | ``XyzByte``       |               |
+------------------+-------------------+---------------+
| ``bright``       | ``XyzByte``       |               |
+------------------+-------------------+---------------+
| ``size``         | ``Extent3d``      |               |
+------------------+-------------------+---------------+
| ``background``   | ``XyzByte``       |               |
+------------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 CheckerFill(Region region, VectorInt32 fieldSize, XyzUInt16 dark, XyzUInt16 bright, Extent3d size, XyzUInt16 background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``region``       | ``Region``        |               |
+------------------+-------------------+---------------+
| ``fieldSize``    | ``VectorInt32``   |               |
+------------------+-------------------+---------------+
| ``dark``         | ``XyzUInt16``     |               |
+------------------+-------------------+---------------+
| ``bright``       | ``XyzUInt16``     |               |
+------------------+-------------------+---------------+
| ``size``         | ``Extent3d``      |               |
+------------------+-------------------+---------------+
| ``background``   | ``XyzUInt16``     |               |
+------------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble CheckerFill(Region region, VectorInt32 fieldSize, XyzDouble dark, XyzDouble bright, Extent3d size, XyzDouble background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``region``       | ``Region``        |               |
+------------------+-------------------+---------------+
| ``fieldSize``    | ``VectorInt32``   |               |
+------------------+-------------------+---------------+
| ``dark``         | ``XyzDouble``     |               |
+------------------+-------------------+---------------+
| ``bright``       | ``XyzDouble``     |               |
+------------------+-------------------+---------------+
| ``size``         | ``Extent3d``      |               |
+------------------+-------------------+---------------+
| ``background``   | ``XyzDouble``     |               |
+------------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CheckerFill*
^^^^^^^^^^^^^^^^^^^^

``Image CheckerFill(Region region, VectorInt32 fieldSize, object dark, object bright, Extent3d size, object background)``

Preset an image a checkerboard pattern.

The method **CheckerFill** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``region``       | ``Region``        |               |
+------------------+-------------------+---------------+
| ``fieldSize``    | ``VectorInt32``   |               |
+------------------+-------------------+---------------+
| ``dark``         | ``object``        |               |
+------------------+-------------------+---------------+
| ``bright``       | ``object``        |               |
+------------------+-------------------+---------------+
| ``size``         | ``Extent3d``      |               |
+------------------+-------------------+---------------+
| ``background``   | ``object``        |               |
+------------------+-------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageByte JaehneFill(PointDouble center, System.Double radius, System.Byte dark, System.Byte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``System.Byte``                         |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``System.Byte``                         |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageUInt16 JaehneFill(PointDouble center, System.Double radius, System.UInt16 dark, System.UInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``System.UInt16``                       |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``System.UInt16``                       |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageUInt32 JaehneFill(PointDouble center, System.Double radius, System.UInt32 dark, System.UInt32 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageDouble JaehneFill(PointDouble center, System.Double radius, System.Double dark, System.Double bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageRgbByte JaehneFill(PointDouble center, System.Double radius, RgbByte dark, RgbByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 JaehneFill(PointDouble center, System.Double radius, RgbUInt16 dark, RgbUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 JaehneFill(PointDouble center, System.Double radius, RgbUInt32 dark, RgbUInt32 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbUInt32``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbUInt32``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble JaehneFill(PointDouble center, System.Double radius, RgbDouble dark, RgbDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte JaehneFill(PointDouble center, System.Double radius, RgbaByte dark, RgbaByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbaByte``                            |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbaByte``                            |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 JaehneFill(PointDouble center, System.Double radius, RgbaUInt16 dark, RgbaUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbaUInt16``                          |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbaUInt16``                          |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 JaehneFill(PointDouble center, System.Double radius, RgbaUInt32 dark, RgbaUInt32 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbaUInt32``                          |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbaUInt32``                          |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble JaehneFill(PointDouble center, System.Double radius, RgbaDouble dark, RgbaDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbaDouble``                          |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbaDouble``                          |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageHlsByte JaehneFill(PointDouble center, System.Double radius, HlsByte dark, HlsByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``HlsByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HlsByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 JaehneFill(PointDouble center, System.Double radius, HlsUInt16 dark, HlsUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``HlsUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HlsUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble JaehneFill(PointDouble center, System.Double radius, HlsDouble dark, HlsDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``HlsDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HlsDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageHsiByte JaehneFill(PointDouble center, System.Double radius, HsiByte dark, HsiByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``HsiByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HsiByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 JaehneFill(PointDouble center, System.Double radius, HsiUInt16 dark, HsiUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``HsiUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HsiUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble JaehneFill(PointDouble center, System.Double radius, HsiDouble dark, HsiDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``HsiDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HsiDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageLabByte JaehneFill(PointDouble center, System.Double radius, LabByte dark, LabByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``LabByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``LabByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 JaehneFill(PointDouble center, System.Double radius, LabUInt16 dark, LabUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``LabUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``LabUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageLabDouble JaehneFill(PointDouble center, System.Double radius, LabDouble dark, LabDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``LabDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``LabDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageXyzByte JaehneFill(PointDouble center, System.Double radius, XyzByte dark, XyzByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``XyzByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``XyzByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 JaehneFill(PointDouble center, System.Double radius, XyzUInt16 dark, XyzUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``XyzUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``XyzUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble JaehneFill(PointDouble center, System.Double radius, XyzDouble dark, XyzDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``XyzDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``XyzDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``Image JaehneFill(PointDouble center, System.Double radius, object dark, object bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``object``                              |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``object``                              |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageByte JaehneFill(Region region, PointDouble center, System.Double radius, System.Byte dark, System.Byte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, System.Byte background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``System.Byte``                         |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``System.Byte``                         |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``System.Byte``                         |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageUInt16 JaehneFill(Region region, PointDouble center, System.Double radius, System.UInt16 dark, System.UInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, System.UInt16 background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``System.UInt16``                       |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``System.UInt16``                       |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``System.UInt16``                       |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageUInt32 JaehneFill(Region region, PointDouble center, System.Double radius, System.UInt32 dark, System.UInt32 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, System.UInt32 background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageDouble JaehneFill(Region region, PointDouble center, System.Double radius, System.Double dark, System.Double bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, System.Double background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageRgbByte JaehneFill(Region region, PointDouble center, System.Double radius, RgbByte dark, RgbByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbByte background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 JaehneFill(Region region, PointDouble center, System.Double radius, RgbUInt16 dark, RgbUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbUInt16 background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 JaehneFill(Region region, PointDouble center, System.Double radius, RgbUInt32 dark, RgbUInt32 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbUInt32 background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbUInt32``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbUInt32``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbUInt32``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble JaehneFill(Region region, PointDouble center, System.Double radius, RgbDouble dark, RgbDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbDouble background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte JaehneFill(Region region, PointDouble center, System.Double radius, RgbaByte dark, RgbaByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbaByte background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbaByte``                            |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbaByte``                            |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbaByte``                            |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 JaehneFill(Region region, PointDouble center, System.Double radius, RgbaUInt16 dark, RgbaUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbaUInt16 background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbaUInt16``                          |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbaUInt16``                          |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbaUInt16``                          |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 JaehneFill(Region region, PointDouble center, System.Double radius, RgbaUInt32 dark, RgbaUInt32 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbaUInt32 background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbaUInt32``                          |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbaUInt32``                          |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbaUInt32``                          |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble JaehneFill(Region region, PointDouble center, System.Double radius, RgbaDouble dark, RgbaDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbaDouble background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbaDouble``                          |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbaDouble``                          |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbaDouble``                          |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageHlsByte JaehneFill(Region region, PointDouble center, System.Double radius, HlsByte dark, HlsByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, HlsByte background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HlsByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HlsByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HlsByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 JaehneFill(Region region, PointDouble center, System.Double radius, HlsUInt16 dark, HlsUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, HlsUInt16 background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HlsUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HlsUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HlsUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble JaehneFill(Region region, PointDouble center, System.Double radius, HlsDouble dark, HlsDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, HlsDouble background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HlsDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HlsDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HlsDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageHsiByte JaehneFill(Region region, PointDouble center, System.Double radius, HsiByte dark, HsiByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, HsiByte background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HsiByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HsiByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HsiByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 JaehneFill(Region region, PointDouble center, System.Double radius, HsiUInt16 dark, HsiUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, HsiUInt16 background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HsiUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HsiUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HsiUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble JaehneFill(Region region, PointDouble center, System.Double radius, HsiDouble dark, HsiDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, HsiDouble background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HsiDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HsiDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HsiDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageLabByte JaehneFill(Region region, PointDouble center, System.Double radius, LabByte dark, LabByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, LabByte background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``LabByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``LabByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``LabByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 JaehneFill(Region region, PointDouble center, System.Double radius, LabUInt16 dark, LabUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, LabUInt16 background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``LabUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``LabUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``LabUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageLabDouble JaehneFill(Region region, PointDouble center, System.Double radius, LabDouble dark, LabDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, LabDouble background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``LabDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``LabDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``LabDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageXyzByte JaehneFill(Region region, PointDouble center, System.Double radius, XyzByte dark, XyzByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, XyzByte background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``XyzByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``XyzByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``XyzByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 JaehneFill(Region region, PointDouble center, System.Double radius, XyzUInt16 dark, XyzUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, XyzUInt16 background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``XyzUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``XyzUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``XyzUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble JaehneFill(Region region, PointDouble center, System.Double radius, XyzDouble dark, XyzDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, XyzDouble background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``XyzDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``XyzDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``XyzDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *JaehneFill*
^^^^^^^^^^^^^^^^^^^

``Image JaehneFill(Region region, PointDouble center, System.Double radius, object dark, object bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, object background)``

Preset an image a Jaehne pattern.

The method **JaehneFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``object``                              |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``object``                              |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``object``                              |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte LinearGradientFill(PointDouble start, PointDouble stop, System.Byte dark, System.Byte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``System.Byte``                         |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``System.Byte``                         |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 LinearGradientFill(PointDouble start, PointDouble stop, System.UInt16 dark, System.UInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``System.UInt16``                       |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``System.UInt16``                       |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 LinearGradientFill(PointDouble start, PointDouble stop, System.UInt32 dark, System.UInt32 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble LinearGradientFill(PointDouble start, PointDouble stop, System.Double dark, System.Double bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte LinearGradientFill(PointDouble start, PointDouble stop, RgbByte dark, RgbByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 LinearGradientFill(PointDouble start, PointDouble stop, RgbUInt16 dark, RgbUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 LinearGradientFill(PointDouble start, PointDouble stop, RgbUInt32 dark, RgbUInt32 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbUInt32``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbUInt32``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble LinearGradientFill(PointDouble start, PointDouble stop, RgbDouble dark, RgbDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte LinearGradientFill(PointDouble start, PointDouble stop, RgbaByte dark, RgbaByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbaByte``                            |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbaByte``                            |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 LinearGradientFill(PointDouble start, PointDouble stop, RgbaUInt16 dark, RgbaUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbaUInt16``                          |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbaUInt16``                          |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 LinearGradientFill(PointDouble start, PointDouble stop, RgbaUInt32 dark, RgbaUInt32 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbaUInt32``                          |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbaUInt32``                          |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble LinearGradientFill(PointDouble start, PointDouble stop, RgbaDouble dark, RgbaDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbaDouble``                          |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbaDouble``                          |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte LinearGradientFill(PointDouble start, PointDouble stop, HlsByte dark, HlsByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``HlsByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HlsByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 LinearGradientFill(PointDouble start, PointDouble stop, HlsUInt16 dark, HlsUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``HlsUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HlsUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble LinearGradientFill(PointDouble start, PointDouble stop, HlsDouble dark, HlsDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``HlsDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HlsDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte LinearGradientFill(PointDouble start, PointDouble stop, HsiByte dark, HsiByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``HsiByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HsiByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 LinearGradientFill(PointDouble start, PointDouble stop, HsiUInt16 dark, HsiUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``HsiUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HsiUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble LinearGradientFill(PointDouble start, PointDouble stop, HsiDouble dark, HsiDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``HsiDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HsiDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte LinearGradientFill(PointDouble start, PointDouble stop, LabByte dark, LabByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``LabByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``LabByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 LinearGradientFill(PointDouble start, PointDouble stop, LabUInt16 dark, LabUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``LabUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``LabUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble LinearGradientFill(PointDouble start, PointDouble stop, LabDouble dark, LabDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``LabDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``LabDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte LinearGradientFill(PointDouble start, PointDouble stop, XyzByte dark, XyzByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``XyzByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``XyzByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 LinearGradientFill(PointDouble start, PointDouble stop, XyzUInt16 dark, XyzUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``XyzUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``XyzUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble LinearGradientFill(PointDouble start, PointDouble stop, XyzDouble dark, XyzDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``XyzDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``XyzDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Image LinearGradientFill(PointDouble start, PointDouble stop, object dark, object bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``start``      | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``stop``       | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``object``                              |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``object``                              |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte LinearGradientFill(Region region, PointDouble start, PointDouble stop, System.Byte dark, System.Byte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, System.Byte background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``System.Byte``                         |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``System.Byte``                         |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``System.Byte``                         |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 LinearGradientFill(Region region, PointDouble start, PointDouble stop, System.UInt16 dark, System.UInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, System.UInt16 background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``System.UInt16``                       |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``System.UInt16``                       |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``System.UInt16``                       |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 LinearGradientFill(Region region, PointDouble start, PointDouble stop, System.UInt32 dark, System.UInt32 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, System.UInt32 background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble LinearGradientFill(Region region, PointDouble start, PointDouble stop, System.Double dark, System.Double bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, System.Double background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte LinearGradientFill(Region region, PointDouble start, PointDouble stop, RgbByte dark, RgbByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbByte background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 LinearGradientFill(Region region, PointDouble start, PointDouble stop, RgbUInt16 dark, RgbUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbUInt16 background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 LinearGradientFill(Region region, PointDouble start, PointDouble stop, RgbUInt32 dark, RgbUInt32 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbUInt32 background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbUInt32``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbUInt32``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbUInt32``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble LinearGradientFill(Region region, PointDouble start, PointDouble stop, RgbDouble dark, RgbDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbDouble background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte LinearGradientFill(Region region, PointDouble start, PointDouble stop, RgbaByte dark, RgbaByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbaByte background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbaByte``                            |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbaByte``                            |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbaByte``                            |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 LinearGradientFill(Region region, PointDouble start, PointDouble stop, RgbaUInt16 dark, RgbaUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbaUInt16 background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbaUInt16``                          |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbaUInt16``                          |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbaUInt16``                          |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 LinearGradientFill(Region region, PointDouble start, PointDouble stop, RgbaUInt32 dark, RgbaUInt32 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbaUInt32 background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbaUInt32``                          |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbaUInt32``                          |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbaUInt32``                          |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble LinearGradientFill(Region region, PointDouble start, PointDouble stop, RgbaDouble dark, RgbaDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbaDouble background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbaDouble``                          |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbaDouble``                          |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbaDouble``                          |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte LinearGradientFill(Region region, PointDouble start, PointDouble stop, HlsByte dark, HlsByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, HlsByte background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HlsByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HlsByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HlsByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 LinearGradientFill(Region region, PointDouble start, PointDouble stop, HlsUInt16 dark, HlsUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, HlsUInt16 background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HlsUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HlsUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HlsUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble LinearGradientFill(Region region, PointDouble start, PointDouble stop, HlsDouble dark, HlsDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, HlsDouble background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HlsDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HlsDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HlsDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte LinearGradientFill(Region region, PointDouble start, PointDouble stop, HsiByte dark, HsiByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, HsiByte background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HsiByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HsiByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HsiByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 LinearGradientFill(Region region, PointDouble start, PointDouble stop, HsiUInt16 dark, HsiUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, HsiUInt16 background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HsiUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HsiUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HsiUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble LinearGradientFill(Region region, PointDouble start, PointDouble stop, HsiDouble dark, HsiDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, HsiDouble background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HsiDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HsiDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HsiDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte LinearGradientFill(Region region, PointDouble start, PointDouble stop, LabByte dark, LabByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, LabByte background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``LabByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``LabByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``LabByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 LinearGradientFill(Region region, PointDouble start, PointDouble stop, LabUInt16 dark, LabUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, LabUInt16 background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``LabUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``LabUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``LabUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble LinearGradientFill(Region region, PointDouble start, PointDouble stop, LabDouble dark, LabDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, LabDouble background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``LabDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``LabDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``LabDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte LinearGradientFill(Region region, PointDouble start, PointDouble stop, XyzByte dark, XyzByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, XyzByte background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``XyzByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``XyzByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``XyzByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 LinearGradientFill(Region region, PointDouble start, PointDouble stop, XyzUInt16 dark, XyzUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, XyzUInt16 background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``XyzUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``XyzUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``XyzUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble LinearGradientFill(Region region, PointDouble start, PointDouble stop, XyzDouble dark, XyzDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, XyzDouble background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``XyzDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``XyzDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``XyzDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *LinearGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Image LinearGradientFill(Region region, PointDouble start, PointDouble stop, object dark, object bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, object background)``

Preset an image with a linear gradient pattern.

The method **LinearGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``start``        | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``stop``         | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``object``                              |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``object``                              |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``object``                              |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte RadialGradientFill(PointDouble center, System.Double radius, System.Byte dark, System.Byte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``System.Byte``                         |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``System.Byte``                         |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 RadialGradientFill(PointDouble center, System.Double radius, System.UInt16 dark, System.UInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``System.UInt16``                       |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``System.UInt16``                       |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 RadialGradientFill(PointDouble center, System.Double radius, System.UInt32 dark, System.UInt32 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble RadialGradientFill(PointDouble center, System.Double radius, System.Double dark, System.Double bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte RadialGradientFill(PointDouble center, System.Double radius, RgbByte dark, RgbByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 RadialGradientFill(PointDouble center, System.Double radius, RgbUInt16 dark, RgbUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 RadialGradientFill(PointDouble center, System.Double radius, RgbUInt32 dark, RgbUInt32 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbUInt32``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbUInt32``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble RadialGradientFill(PointDouble center, System.Double radius, RgbDouble dark, RgbDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte RadialGradientFill(PointDouble center, System.Double radius, RgbaByte dark, RgbaByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbaByte``                            |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbaByte``                            |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 RadialGradientFill(PointDouble center, System.Double radius, RgbaUInt16 dark, RgbaUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbaUInt16``                          |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbaUInt16``                          |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 RadialGradientFill(PointDouble center, System.Double radius, RgbaUInt32 dark, RgbaUInt32 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbaUInt32``                          |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbaUInt32``                          |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble RadialGradientFill(PointDouble center, System.Double radius, RgbaDouble dark, RgbaDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbaDouble``                          |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbaDouble``                          |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte RadialGradientFill(PointDouble center, System.Double radius, HlsByte dark, HlsByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``HlsByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HlsByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 RadialGradientFill(PointDouble center, System.Double radius, HlsUInt16 dark, HlsUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``HlsUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HlsUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble RadialGradientFill(PointDouble center, System.Double radius, HlsDouble dark, HlsDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``HlsDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HlsDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte RadialGradientFill(PointDouble center, System.Double radius, HsiByte dark, HsiByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``HsiByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HsiByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 RadialGradientFill(PointDouble center, System.Double radius, HsiUInt16 dark, HsiUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``HsiUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HsiUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble RadialGradientFill(PointDouble center, System.Double radius, HsiDouble dark, HsiDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``HsiDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HsiDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte RadialGradientFill(PointDouble center, System.Double radius, LabByte dark, LabByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``LabByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``LabByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 RadialGradientFill(PointDouble center, System.Double radius, LabUInt16 dark, LabUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``LabUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``LabUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble RadialGradientFill(PointDouble center, System.Double radius, LabDouble dark, LabDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``LabDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``LabDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte RadialGradientFill(PointDouble center, System.Double radius, XyzByte dark, XyzByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``XyzByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``XyzByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 RadialGradientFill(PointDouble center, System.Double radius, XyzUInt16 dark, XyzUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``XyzUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``XyzUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble RadialGradientFill(PointDouble center, System.Double radius, XyzDouble dark, XyzDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``XyzDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``XyzDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Image RadialGradientFill(PointDouble center, System.Double radius, object dark, object bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``radius``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``object``                              |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``object``                              |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte RadialGradientFill(Region region, PointDouble center, System.Double radius, System.Byte dark, System.Byte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, System.Byte background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``System.Byte``                         |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``System.Byte``                         |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``System.Byte``                         |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 RadialGradientFill(Region region, PointDouble center, System.Double radius, System.UInt16 dark, System.UInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, System.UInt16 background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``System.UInt16``                       |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``System.UInt16``                       |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``System.UInt16``                       |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 RadialGradientFill(Region region, PointDouble center, System.Double radius, System.UInt32 dark, System.UInt32 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, System.UInt32 background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble RadialGradientFill(Region region, PointDouble center, System.Double radius, System.Double dark, System.Double bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, System.Double background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte RadialGradientFill(Region region, PointDouble center, System.Double radius, RgbByte dark, RgbByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbByte background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 RadialGradientFill(Region region, PointDouble center, System.Double radius, RgbUInt16 dark, RgbUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbUInt16 background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 RadialGradientFill(Region region, PointDouble center, System.Double radius, RgbUInt32 dark, RgbUInt32 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbUInt32 background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbUInt32``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbUInt32``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbUInt32``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble RadialGradientFill(Region region, PointDouble center, System.Double radius, RgbDouble dark, RgbDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbDouble background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte RadialGradientFill(Region region, PointDouble center, System.Double radius, RgbaByte dark, RgbaByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbaByte background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbaByte``                            |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbaByte``                            |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbaByte``                            |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 RadialGradientFill(Region region, PointDouble center, System.Double radius, RgbaUInt16 dark, RgbaUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbaUInt16 background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbaUInt16``                          |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbaUInt16``                          |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbaUInt16``                          |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 RadialGradientFill(Region region, PointDouble center, System.Double radius, RgbaUInt32 dark, RgbaUInt32 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbaUInt32 background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbaUInt32``                          |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbaUInt32``                          |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbaUInt32``                          |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble RadialGradientFill(Region region, PointDouble center, System.Double radius, RgbaDouble dark, RgbaDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbaDouble background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbaDouble``                          |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbaDouble``                          |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbaDouble``                          |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte RadialGradientFill(Region region, PointDouble center, System.Double radius, HlsByte dark, HlsByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, HlsByte background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HlsByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HlsByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HlsByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 RadialGradientFill(Region region, PointDouble center, System.Double radius, HlsUInt16 dark, HlsUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, HlsUInt16 background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HlsUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HlsUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HlsUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble RadialGradientFill(Region region, PointDouble center, System.Double radius, HlsDouble dark, HlsDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, HlsDouble background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HlsDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HlsDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HlsDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte RadialGradientFill(Region region, PointDouble center, System.Double radius, HsiByte dark, HsiByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, HsiByte background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HsiByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HsiByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HsiByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 RadialGradientFill(Region region, PointDouble center, System.Double radius, HsiUInt16 dark, HsiUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, HsiUInt16 background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HsiUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HsiUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HsiUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble RadialGradientFill(Region region, PointDouble center, System.Double radius, HsiDouble dark, HsiDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, HsiDouble background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HsiDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HsiDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HsiDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte RadialGradientFill(Region region, PointDouble center, System.Double radius, LabByte dark, LabByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, LabByte background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``LabByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``LabByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``LabByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 RadialGradientFill(Region region, PointDouble center, System.Double radius, LabUInt16 dark, LabUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, LabUInt16 background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``LabUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``LabUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``LabUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble RadialGradientFill(Region region, PointDouble center, System.Double radius, LabDouble dark, LabDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, LabDouble background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``LabDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``LabDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``LabDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte RadialGradientFill(Region region, PointDouble center, System.Double radius, XyzByte dark, XyzByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, XyzByte background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``XyzByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``XyzByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``XyzByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 RadialGradientFill(Region region, PointDouble center, System.Double radius, XyzUInt16 dark, XyzUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, XyzUInt16 background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``XyzUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``XyzUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``XyzUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble RadialGradientFill(Region region, PointDouble center, System.Double radius, XyzDouble dark, XyzDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, XyzDouble background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``XyzDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``XyzDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``XyzDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *RadialGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Image RadialGradientFill(Region region, PointDouble center, System.Double radius, object dark, object bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, object background)``

Preset an image with a radial gradient pattern.

The method **RadialGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``radius``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``object``                              |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``object``                              |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``object``                              |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte CircularGradientFill(PointDouble center, System.Byte dark, System.Byte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``System.Byte``                         |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``System.Byte``                         |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 CircularGradientFill(PointDouble center, System.UInt16 dark, System.UInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``System.UInt16``                       |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``System.UInt16``                       |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 CircularGradientFill(PointDouble center, System.UInt32 dark, System.UInt32 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``System.UInt32``                       |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble CircularGradientFill(PointDouble center, System.Double dark, System.Double bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``System.Double``                       |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte CircularGradientFill(PointDouble center, RgbByte dark, RgbByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 CircularGradientFill(PointDouble center, RgbUInt16 dark, RgbUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 CircularGradientFill(PointDouble center, RgbUInt32 dark, RgbUInt32 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbUInt32``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbUInt32``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble CircularGradientFill(PointDouble center, RgbDouble dark, RgbDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte CircularGradientFill(PointDouble center, RgbaByte dark, RgbaByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbaByte``                            |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbaByte``                            |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 CircularGradientFill(PointDouble center, RgbaUInt16 dark, RgbaUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbaUInt16``                          |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbaUInt16``                          |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 CircularGradientFill(PointDouble center, RgbaUInt32 dark, RgbaUInt32 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbaUInt32``                          |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbaUInt32``                          |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble CircularGradientFill(PointDouble center, RgbaDouble dark, RgbaDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``RgbaDouble``                          |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``RgbaDouble``                          |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte CircularGradientFill(PointDouble center, HlsByte dark, HlsByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``HlsByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HlsByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 CircularGradientFill(PointDouble center, HlsUInt16 dark, HlsUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``HlsUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HlsUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble CircularGradientFill(PointDouble center, HlsDouble dark, HlsDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``HlsDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HlsDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte CircularGradientFill(PointDouble center, HsiByte dark, HsiByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``HsiByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HsiByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 CircularGradientFill(PointDouble center, HsiUInt16 dark, HsiUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``HsiUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HsiUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble CircularGradientFill(PointDouble center, HsiDouble dark, HsiDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``HsiDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``HsiDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte CircularGradientFill(PointDouble center, LabByte dark, LabByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``LabByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``LabByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 CircularGradientFill(PointDouble center, LabUInt16 dark, LabUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``LabUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``LabUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble CircularGradientFill(PointDouble center, LabDouble dark, LabDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``LabDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``LabDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte CircularGradientFill(PointDouble center, XyzByte dark, XyzByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``XyzByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``XyzByte``                             |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 CircularGradientFill(PointDouble center, XyzUInt16 dark, XyzUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``XyzUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``XyzUInt16``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble CircularGradientFill(PointDouble center, XyzDouble dark, XyzDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``XyzDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``XyzDouble``                           |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Image CircularGradientFill(PointDouble center, object dark, object bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+----------------+-----------------------------------------+---------------+
| Parameter      | Type                                    | Description   |
+================+=========================================+===============+
| ``center``     | ``PointDouble``                         |               |
+----------------+-----------------------------------------+---------------+
| ``dark``       | ``object``                              |               |
+----------------+-----------------------------------------+---------------+
| ``bright``     | ``object``                              |               |
+----------------+-----------------------------------------+---------------+
| ``size``       | ``Extent3d``                            |               |
+----------------+-----------------------------------------+---------------+
| ``tweening``   | ``PresetAlgorithms.TweeningFunction``   |               |
+----------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte CircularGradientFill(Region region, PointDouble center, System.Byte dark, System.Byte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, System.Byte background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``System.Byte``                         |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``System.Byte``                         |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``System.Byte``                         |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 CircularGradientFill(Region region, PointDouble center, System.UInt16 dark, System.UInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, System.UInt16 background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``System.UInt16``                       |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``System.UInt16``                       |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``System.UInt16``                       |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 CircularGradientFill(Region region, PointDouble center, System.UInt32 dark, System.UInt32 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, System.UInt32 background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``System.UInt32``                       |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble CircularGradientFill(Region region, PointDouble center, System.Double dark, System.Double bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, System.Double background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``System.Double``                       |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte CircularGradientFill(Region region, PointDouble center, RgbByte dark, RgbByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbByte background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 CircularGradientFill(Region region, PointDouble center, RgbUInt16 dark, RgbUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbUInt16 background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 CircularGradientFill(Region region, PointDouble center, RgbUInt32 dark, RgbUInt32 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbUInt32 background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbUInt32``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbUInt32``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbUInt32``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble CircularGradientFill(Region region, PointDouble center, RgbDouble dark, RgbDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbDouble background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte CircularGradientFill(Region region, PointDouble center, RgbaByte dark, RgbaByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbaByte background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbaByte``                            |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbaByte``                            |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbaByte``                            |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 CircularGradientFill(Region region, PointDouble center, RgbaUInt16 dark, RgbaUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbaUInt16 background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbaUInt16``                          |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbaUInt16``                          |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbaUInt16``                          |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 CircularGradientFill(Region region, PointDouble center, RgbaUInt32 dark, RgbaUInt32 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbaUInt32 background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbaUInt32``                          |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbaUInt32``                          |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbaUInt32``                          |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble CircularGradientFill(Region region, PointDouble center, RgbaDouble dark, RgbaDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, RgbaDouble background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``RgbaDouble``                          |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``RgbaDouble``                          |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``RgbaDouble``                          |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte CircularGradientFill(Region region, PointDouble center, HlsByte dark, HlsByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, HlsByte background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HlsByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HlsByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HlsByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 CircularGradientFill(Region region, PointDouble center, HlsUInt16 dark, HlsUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, HlsUInt16 background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HlsUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HlsUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HlsUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble CircularGradientFill(Region region, PointDouble center, HlsDouble dark, HlsDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, HlsDouble background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HlsDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HlsDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HlsDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte CircularGradientFill(Region region, PointDouble center, HsiByte dark, HsiByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, HsiByte background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HsiByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HsiByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HsiByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 CircularGradientFill(Region region, PointDouble center, HsiUInt16 dark, HsiUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, HsiUInt16 background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HsiUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HsiUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HsiUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble CircularGradientFill(Region region, PointDouble center, HsiDouble dark, HsiDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, HsiDouble background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``HsiDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``HsiDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``HsiDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte CircularGradientFill(Region region, PointDouble center, LabByte dark, LabByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, LabByte background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``LabByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``LabByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``LabByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 CircularGradientFill(Region region, PointDouble center, LabUInt16 dark, LabUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, LabUInt16 background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``LabUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``LabUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``LabUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble CircularGradientFill(Region region, PointDouble center, LabDouble dark, LabDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, LabDouble background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``LabDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``LabDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``LabDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte CircularGradientFill(Region region, PointDouble center, XyzByte dark, XyzByte bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, XyzByte background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``XyzByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``XyzByte``                             |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``XyzByte``                             |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 CircularGradientFill(Region region, PointDouble center, XyzUInt16 dark, XyzUInt16 bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, XyzUInt16 background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``XyzUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``XyzUInt16``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``XyzUInt16``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble CircularGradientFill(Region region, PointDouble center, XyzDouble dark, XyzDouble bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, XyzDouble background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``XyzDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``XyzDouble``                           |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``XyzDouble``                           |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Method *CircularGradientFill*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Image CircularGradientFill(Region region, PointDouble center, object dark, object bright, Extent3d size, PresetAlgorithms.TweeningFunction tweening, object background)``

Preset an image with a circular gradient pattern.

The method **CircularGradientFill** has the following parameters:

+------------------+-----------------------------------------+---------------+
| Parameter        | Type                                    | Description   |
+==================+=========================================+===============+
| ``region``       | ``Region``                              |               |
+------------------+-----------------------------------------+---------------+
| ``center``       | ``PointDouble``                         |               |
+------------------+-----------------------------------------+---------------+
| ``dark``         | ``object``                              |               |
+------------------+-----------------------------------------+---------------+
| ``bright``       | ``object``                              |               |
+------------------+-----------------------------------------+---------------+
| ``size``         | ``Extent3d``                            |               |
+------------------+-----------------------------------------+---------------+
| ``tweening``     | ``PresetAlgorithms.TweeningFunction``   |               |
+------------------+-----------------------------------------+---------------+
| ``background``   | ``object``                              |               |
+------------------+-----------------------------------------+---------------+

The region parameter constrains the function to the domain of the region.

/return The result image.

Enumerations
~~~~~~~~~~~~

Enumeration *TweeningFunction*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum TweeningFunction``

TODO documentation missing

The enumeration **TweeningFunction** has the following constants:

+----------------+---------+---------------+
| Name           | Value   | Description   |
+================+=========+===============+
| ``tfLinear``   | ``0``   |               |
+----------------+---------+---------------+
| ``tfBinary``   | ``1``   |               |
+----------------+---------+---------------+
| ``tfSinus``    | ``2``   |               |
+----------------+---------+---------------+
| ``tfSquare``   | ``3``   |               |
+----------------+---------+---------------+
| ``tfCubic``    | ``4``   |               |
+----------------+---------+---------------+

::

    enum TweeningFunction
    {
      tfLinear = 0,
      tfBinary = 1,
      tfSinus = 2,
      tfSquare = 3,
      tfCubic = 4,
    };

TODO documentation missing
