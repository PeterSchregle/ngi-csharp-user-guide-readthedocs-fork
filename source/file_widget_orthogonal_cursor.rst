Class *WidgetOrthogonalCursor*
------------------------------

A widget\_orthogonal\_cursor is a widget displaying a pair of crossing horizontal and vertical lines.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetOrthogonalCursor** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetOrthogonalCursor** contains the following properties:

+----------------+-------+-------+------------------------------------------+
| Property       | Get   | Set   | Description                              |
+================+=======+=======+==========================================+
| ``Position``   | \*    | \*    | The position (location) of the cursor.   |
+----------------+-------+-------+------------------------------------------+
| ``Pen``        | \*    | \*    | The pen of the cursor.                   |
+----------------+-------+-------+------------------------------------------+

Description
~~~~~~~~~~~

It can be positioned on some surface. It provides interaction, i.e. can be selected, move around. In general it can react on mouse and keyboard interactions. A widget\_orthogonal\_cursor has interactive widget capabilities.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetOrthogonalCursor*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetOrthogonalCursor()``

Default constructor.

The default constructor creates a cursor with default settings. By default the cursor is at the origin and the lines are one-pixel wide black.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``PointDouble Position``

The position (location) of the cursor.

Property *Pen*
^^^^^^^^^^^^^^

``PenByte Pen``

The pen of the cursor.
