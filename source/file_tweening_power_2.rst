Class *TweeningPower2*
----------------------

Calculating functor that calculates a power tweening.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **TweeningPower2** contains the following properties:

+-------------+-------+-------+---------------------+
| Property    | Get   | Set   | Description         |
+=============+=======+=======+=====================+
| ``Power``   | \*    |       | The kernel width.   |
+-------------+-------+-------+---------------------+

Description
~~~~~~~~~~~

The function used for the tweening is the power function between 0 and 1.

This function starts with a slope of power-1 and ends with a slope of 0.

This functor is used for the preset functions.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *TweeningPower2*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TweeningPower2()``

Default Constructor.

Constructors
~~~~~~~~~~~~

Constructor *TweeningPower2*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TweeningPower2(System.Double power)``

Constructor.

The constructor has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``power``   | ``System.Double``   | The power.    |
+-------------+---------------------+---------------+

Properties
~~~~~~~~~~

Property *Power*
^^^^^^^^^^^^^^^^

``System.Double Power``

The kernel width.
