Class *ArcList*
---------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **ArcList** implements the following interfaces:

+----------------+
| Interface      |
+================+
| ``IListArc``   |
+----------------+

The class **ArcList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **ArcList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *ArcList*
^^^^^^^^^^^^^^^^^^^^^

``ArcList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``Arc [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``ArcEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(Arc item)``

The method **Add** has the following parameters:

+-------------+-----------+---------------+
| Parameter   | Type      | Description   |
+=============+===========+===============+
| ``item``    | ``Arc``   |               |
+-------------+-----------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(Arc item)``

The method **Contains** has the following parameters:

+-------------+-----------+---------------+
| Parameter   | Type      | Description   |
+=============+===========+===============+
| ``item``    | ``Arc``   |               |
+-------------+-----------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(Arc item)``

The method **Remove** has the following parameters:

+-------------+-----------+---------------+
| Parameter   | Type      | Description   |
+=============+===========+===============+
| ``item``    | ``Arc``   |               |
+-------------+-----------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(Arc item)``

The method **IndexOf** has the following parameters:

+-------------+-----------+---------------+
| Parameter   | Type      | Description   |
+=============+===========+===============+
| ``item``    | ``Arc``   |               |
+-------------+-----------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, Arc item)``

The method **Insert** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``item``    | ``Arc``            |               |
+-------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
