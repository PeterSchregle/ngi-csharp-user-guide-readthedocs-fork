Class *WidgetScaleBar*
----------------------

A widget\_scale\_bar displays ...

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetScaleBar** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetScaleBar** contains the following properties:

+-----------------------+-------+-------+------------------------------------------+
| Property              | Get   | Set   | Description                              |
+=======================+=======+=======+==========================================+
| ``Position``          | \*    | \*    | The position of the ruler.               |
+-----------------------+-------+-------+------------------------------------------+
| ``BackgroundBrush``   | \*    | \*    | The background brush of the scale bar.   |
+-----------------------+-------+-------+------------------------------------------+
| ``BarBrush``          | \*    | \*    | The bar brush of the scale bar.          |
+-----------------------+-------+-------+------------------------------------------+
| ``Length``            | \*    | \*    | The length to be displayed.              |
+-----------------------+-------+-------+------------------------------------------+
| ``Unit``              | \*    | \*    | The unit to be displayed.                |
+-----------------------+-------+-------+------------------------------------------+
| ``TextFont``          | \*    | \*    | The legth text font.                     |
+-----------------------+-------+-------+------------------------------------------+
| ``TextBrush``         | \*    | \*    | The text brush of the scale bar.         |
+-----------------------+-------+-------+------------------------------------------+
| ``Scale``             | \*    | \*    | The scale.                               |
+-----------------------+-------+-------+------------------------------------------+
| ``BarHeight``         | \*    | \*    | The bar height.                          |
+-----------------------+-------+-------+------------------------------------------+
| ``Padding``           | \*    | \*    | The padding.                             |
+-----------------------+-------+-------+------------------------------------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_scale\_bar. These settings affect the look and the behavior of the widget\_scale\_bar.

The widget\_scale\_bar supports interactivity via the widget\_interactive base class, i.e. it can be selected, move around. In general it can react on mouse and keyboard interactions. A widget\_scale\_bar has interactive widget capabilities.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetScaleBar*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetScaleBar()``

Default constructor.

The default constructor creates a widget with default settings.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``PointDouble Position``

The position of the ruler.

Property *BackgroundBrush*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``SolidColorBrushByte BackgroundBrush``

The background brush of the scale bar.

Property *BarBrush*
^^^^^^^^^^^^^^^^^^^

``SolidColorBrushByte BarBrush``

The bar brush of the scale bar.

Property *Length*
^^^^^^^^^^^^^^^^^

``System.Double Length``

The length to be displayed.

Property *Unit*
^^^^^^^^^^^^^^^

``System.String Unit``

The unit to be displayed.

Property *TextFont*
^^^^^^^^^^^^^^^^^^^

``Font TextFont``

The legth text font.

Property *TextBrush*
^^^^^^^^^^^^^^^^^^^^

``SolidColorBrushByte TextBrush``

The text brush of the scale bar.

Property *Scale*
^^^^^^^^^^^^^^^^

``System.Double Scale``

The scale.

Property *BarHeight*
^^^^^^^^^^^^^^^^^^^^

``System.Double BarHeight``

The bar height.

Property *Padding*
^^^^^^^^^^^^^^^^^^

``System.Double Padding``

The padding.
