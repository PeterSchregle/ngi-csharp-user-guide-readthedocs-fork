Class *TeighaLayerEnumerator*
-----------------------------

**Namespace:** Ngi

**Module:** CadPdf

The class **TeighaLayerEnumerator** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``IEnumerator``              |
+------------------------------+
| ``IEnumeratorTeighaLayer``   |
+------------------------------+

The class **TeighaLayerEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **TeighaLayerEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``TeighaLayer Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
