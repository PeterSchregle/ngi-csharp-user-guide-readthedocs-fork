Class *RadiometricStereoResult*
-------------------------------

The radiometric\_stereo\_result consists of albedo, gradient (x,y), and angles of the nomals with the camera/viewer direction.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **RadiometricStereoResult** implements the following interfaces:

+-----------------------------------------+
| Interface                               |
+=========================================+
| ``IEquatableRadiometricStereoResult``   |
+-----------------------------------------+
| ``ISerializable``                       |
+-----------------------------------------+

The class **RadiometricStereoResult** contains the following properties:

+-----------------------------+-------+-------+---------------+
| Property                    | Get   | Set   | Description   |
+=============================+=======+=======+===============+
| ``Albedo``                  | \*    |       |               |
+-----------------------------+-------+-------+---------------+
| ``GradientX``               | \*    |       |               |
+-----------------------------+-------+-------+---------------+
| ``GradientY``               | \*    |       |               |
+-----------------------------+-------+-------+---------------+
| ``NormalAngles``            | \*    |       |               |
+-----------------------------+-------+-------+---------------+
| ``GradientAbsoluteValue``   | \*    |       |               |
+-----------------------------+-------+-------+---------------+

The class **RadiometricStereoResult** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *RadiometricStereoResult*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RadiometricStereoResult()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *RadiometricStereoResult*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RadiometricStereoResult(ImageDouble albedo, ImageDouble gradientX, ImageDouble gradientY, ImageDouble normalAngles, ImageDouble gradientAbsoluteValue)``

The constructor has the following parameters:

+-----------------------------+-------------------+---------------+
| Parameter                   | Type              | Description   |
+=============================+===================+===============+
| ``albedo``                  | ``ImageDouble``   |               |
+-----------------------------+-------------------+---------------+
| ``gradientX``               | ``ImageDouble``   |               |
+-----------------------------+-------------------+---------------+
| ``gradientY``               | ``ImageDouble``   |               |
+-----------------------------+-------------------+---------------+
| ``normalAngles``            | ``ImageDouble``   |               |
+-----------------------------+-------------------+---------------+
| ``gradientAbsoluteValue``   | ``ImageDouble``   |               |
+-----------------------------+-------------------+---------------+

Properties
~~~~~~~~~~

Property *Albedo*
^^^^^^^^^^^^^^^^^

``ImageDouble Albedo``

Property *GradientX*
^^^^^^^^^^^^^^^^^^^^

``ImageDouble GradientX``

Property *GradientY*
^^^^^^^^^^^^^^^^^^^^

``ImageDouble GradientY``

Property *NormalAngles*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble NormalAngles``

Property *GradientAbsoluteValue*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble GradientAbsoluteValue``

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
