Class *Range*
-------------

A range is specified by a begin and an end.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Range** implements the following interfaces:

+-----------------------+
| Interface             |
+=======================+
| ``IEquatableRange``   |
+-----------------------+
| ``ISerializable``     |
+-----------------------+

The class **Range** contains the following properties:

+--------------+-------+-------+---------------------------------------+
| Property     | Get   | Set   | Description                           |
+==============+=======+=======+=======================================+
| ``Begin``    | \*    | \*    | The begin of the range (inclusive).   |
+--------------+-------+-------+---------------------------------------+
| ``End``      | \*    | \*    | The end of the range (exclusive).     |
+--------------+-------+-------+---------------------------------------+
| ``Length``   | \*    |       | The length of a range.                |
+--------------+-------+-------+---------------------------------------+

The class **Range** contains the following methods:

+-----------------+---------------------------------------------------------+
| Method          | Description                                             |
+=================+=========================================================+
| ``Contains``    | Test if coordinate is contained in range.               |
+-----------------+---------------------------------------------------------+
| ``Transpose``   | Transpose a range.                                      |
+-----------------+---------------------------------------------------------+
| ``Translate``   | Translate a range.                                      |
+-----------------+---------------------------------------------------------+
| ``ToString``    | Provide string representation for debugging purposes.   |
+-----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

All values between begin and end are part of the range. As with STL iterators the begin is inclusive, while the end is exclusive. In addition, the begin must be smaller than the end, i.e. end - begin must be a positive number.

Ranges specify a sort order. One range is smaller than the other if its begin is smaller than the other's begin. The ends do not participate in ordering.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Range*
^^^^^^^^^^^^^^^^^^^

``Range()``

Constructs an empty range.

Both begin and end are intialized to zero.

Constructors
~~~~~~~~~~~~

Constructor *Range*
^^^^^^^^^^^^^^^^^^^

``Range(System.Int32 begin, System.Int32 end)``

Constructs a range with specific begin and end.

The constructor has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``begin``   | ``System.Int32``   | The begin.    |
+-------------+--------------------+---------------+
| ``end``     | ``System.Int32``   | The end.      |
+-------------+--------------------+---------------+

Both begin and end are initialized to the passed values. End must be bigger or equal than begin.

Properties
~~~~~~~~~~

Property *Begin*
^^^^^^^^^^^^^^^^

``System.Int32 Begin``

The begin of the range (inclusive).

Property *End*
^^^^^^^^^^^^^^

``System.Int32 End``

The end of the range (exclusive).

Property *Length*
^^^^^^^^^^^^^^^^^

``System.Int32 Length``

The length of a range.

Static Methods
~~~~~~~~~~~~~~

Method *AreRangesOverlapping*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean AreRangesOverlapping(Range a, Range b)``

Test if two ranges overlap.

The method **AreRangesOverlapping** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``a``       | ``Range``   |               |
+-------------+-------------+---------------+
| ``b``       | ``Range``   |               |
+-------------+-------------+---------------+

Method *AreRangesTouching*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean AreRangesTouching(Range a, Range b)``

Test if two ranges touch.

The method **AreRangesTouching** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``a``       | ``Range``   |               |
+-------------+-------------+---------------+
| ``b``       | ``Range``   |               |
+-------------+-------------+---------------+

Method *AreRangesClose*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean AreRangesClose(Range a, Range b, System.Int32 distance)``

Test if two ranges are close.

The method **AreRangesClose** has the following parameters:

+----------------+--------------------+---------------+
| Parameter      | Type               | Description   |
+================+====================+===============+
| ``a``          | ``Range``          |               |
+----------------+--------------------+---------------+
| ``b``          | ``Range``          |               |
+----------------+--------------------+---------------+
| ``distance``   | ``System.Int32``   |               |
+----------------+--------------------+---------------+

If distance == 0 this is the same as testing of overlap. If distance == 1 this is the same as testing of touching. If distance > 1 this is testing of closeness.

Methods
~~~~~~~

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(System.Int32 coordinate)``

Test if coordinate is contained in range.

The method **Contains** has the following parameters:

+------------------+--------------------+---------------------------+
| Parameter        | Type               | Description               |
+==================+====================+===========================+
| ``coordinate``   | ``System.Int32``   | The coordinate to test.   |
+------------------+--------------------+---------------------------+

Return true if coordinate is within the range, otherwise return false.

Method *Transpose*
^^^^^^^^^^^^^^^^^^

``Range Transpose()``

Transpose a range.

This function returns a transposed copy of a range. Transposition mirrors a range at the origin. A range is transposed by reversing, negating and adding one to each of its coordinates.A transposed range object.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``Range Translate(System.Int32 translation)``

Translate a range.

The method **Translate** has the following parameters:

+-------------------+--------------------+---------------------------+
| Parameter         | Type               | Description               |
+===================+====================+===========================+
| ``translation``   | ``System.Int32``   | The translation offset.   |
+-------------------+--------------------+---------------------------+

This function returns a translated copy of a range.

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
