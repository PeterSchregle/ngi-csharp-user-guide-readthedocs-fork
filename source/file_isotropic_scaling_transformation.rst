Class *IsotropicScalingTransformation*
--------------------------------------

Isotropic scaling transformation of geometric primitives.

**Namespace:** Ngi

**Module:** ImageProcessing

Description
~~~~~~~~~~~

This class provides static methods to transform geometric primitives via an isotropic scaling transformation.

The class can transform these geometric primitives: point, vector, direction, line\_segment, ray, line, box, rectangle, circle, ellipse, arc elliptical\_arc, triangle, quadrilateral, polyline, quadratic\_bezier and geometry.

All primitives retain their nature, when transformed by an isotropic scaling transform.

Some primitives can have an integer coordinate type on input, but the resulting types are always of floating point coordinate type. This is because the transformation cannot retain the integer nature in general. To illustrate, a point<int> type is transformed and the result is a point<double> type.

Static Methods
~~~~~~~~~~~~~~

Method *Transform*
^^^^^^^^^^^^^^^^^^

``VectorDouble Transform(VectorDouble vector, IsotropicScalingMatrix isotropicScaling)``

Transform a vector.

The method **Transform** has the following parameters:

+------------------------+------------------------------+---------------+
| Parameter              | Type                         | Description   |
+========================+==============================+===============+
| ``vector``             | ``VectorDouble``             |               |
+------------------------+------------------------------+---------------+
| ``isotropicScaling``   | ``IsotropicScalingMatrix``   |               |
+------------------------+------------------------------+---------------+

A vector transformed with a isotropic\_scaling transform results in another vector.

The isotropic scaling transformation is carried out with a matrix multiplication.

Returns the transformed vector.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``PointDouble Transform(PointDouble point, IsotropicScalingMatrix isotropicScaling)``

Transform a point.

The method **Transform** has the following parameters:

+------------------------+------------------------------+---------------+
| Parameter              | Type                         | Description   |
+========================+==============================+===============+
| ``point``              | ``PointDouble``              |               |
+------------------------+------------------------------+---------------+
| ``isotropicScaling``   | ``IsotropicScalingMatrix``   |               |
+------------------------+------------------------------+---------------+

A point transformed with a isotropic\_scaling transform results in another point.

The isotropic scaling transformation is carried out with a matrix multiplication.

Returns the transformed vector.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Direction Transform(Direction direction, IsotropicScalingMatrix isotropicScaling)``

Transform a direction.

The method **Transform** has the following parameters:

+------------------------+------------------------------+---------------+
| Parameter              | Type                         | Description   |
+========================+==============================+===============+
| ``direction``          | ``Direction``                |               |
+------------------------+------------------------------+---------------+
| ``isotropicScaling``   | ``IsotropicScalingMatrix``   |               |
+------------------------+------------------------------+---------------+

A direction transformed with a isotropic\_scaling transform is teh very same direction, i.e. a no-op.

Returns the transformed direction.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``LineSegmentDouble Transform(LineSegmentDouble lineSegment, IsotropicScalingMatrix isotropicScaling)``

A line segment transformed with a isotropic\_scaling transform results in another line segment.

The method **Transform** has the following parameters:

+------------------------+------------------------------+---------------+
| Parameter              | Type                         | Description   |
+========================+==============================+===============+
| ``lineSegment``        | ``LineSegmentDouble``        |               |
+------------------------+------------------------------+---------------+
| ``isotropicScaling``   | ``IsotropicScalingMatrix``   |               |
+------------------------+------------------------------+---------------+

The isotropic scaling transformation is carried out with a matrix multiplication.

Returns the transformed line segment.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``RayDouble Transform(RayDouble ray, IsotropicScalingMatrix isotropicScaling)``

Transform a ray.

The method **Transform** has the following parameters:

+------------------------+------------------------------+---------------+
| Parameter              | Type                         | Description   |
+========================+==============================+===============+
| ``ray``                | ``RayDouble``                |               |
+------------------------+------------------------------+---------------+
| ``isotropicScaling``   | ``IsotropicScalingMatrix``   |               |
+------------------------+------------------------------+---------------+

A ray transformed with a isotropic\_scaling transform results in another ray.

The isotropic scaling transformation is carried out with a matrix multiplication.

Returns the transformed ray.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``LineDouble Transform(LineDouble line, IsotropicScalingMatrix isotropicScaling)``

Transform a line.

The method **Transform** has the following parameters:

+------------------------+------------------------------+---------------+
| Parameter              | Type                         | Description   |
+========================+==============================+===============+
| ``line``               | ``LineDouble``               |               |
+------------------------+------------------------------+---------------+
| ``isotropicScaling``   | ``IsotropicScalingMatrix``   |               |
+------------------------+------------------------------+---------------+

A line transformed with a isotropic\_scaling transform results in another line.

The isotropic scaling transformation is carried out with a matrix multiplication.

Returns the transformed line.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``BoxDouble Transform(BoxDouble box, IsotropicScalingMatrix isotropicScaling)``

Transform a box.

The method **Transform** has the following parameters:

+------------------------+------------------------------+---------------+
| Parameter              | Type                         | Description   |
+========================+==============================+===============+
| ``box``                | ``BoxDouble``                |               |
+------------------------+------------------------------+---------------+
| ``isotropicScaling``   | ``IsotropicScalingMatrix``   |               |
+------------------------+------------------------------+---------------+

A box transformed with a isotropic\_scaling transform results in another box.

The isotropic scaling transformation is carried out with a matrix multiplication.

Returns the transformed box in the form of a rectangle.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Rectangle Transform(Rectangle box, IsotropicScalingMatrix isotropicScaling)``

Transform a rectangle.

The method **Transform** has the following parameters:

+------------------------+------------------------------+---------------+
| Parameter              | Type                         | Description   |
+========================+==============================+===============+
| ``box``                | ``Rectangle``                |               |
+------------------------+------------------------------+---------------+
| ``isotropicScaling``   | ``IsotropicScalingMatrix``   |               |
+------------------------+------------------------------+---------------+

A rectangle transformed with a isotropic\_scaling transform results in another rectangle.

The isotropic scaling transformation is carried out with a matrix multiplication.

Returns the transformed box in the form of a rectangle.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Circle Transform(Circle circle, IsotropicScalingMatrix isotropicScaling)``

Transform a circle.

The method **Transform** has the following parameters:

+------------------------+------------------------------+---------------+
| Parameter              | Type                         | Description   |
+========================+==============================+===============+
| ``circle``             | ``Circle``                   |               |
+------------------------+------------------------------+---------------+
| ``isotropicScaling``   | ``IsotropicScalingMatrix``   |               |
+------------------------+------------------------------+---------------+

A circle transformed with a isotropic\_scaling transform results in another circle.

The isotropic scaling transformation is carried out with a matrix multiplication.

Returns the transformed circle in the form of an ellipse.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Ellipse Transform(Ellipse ellipse, IsotropicScalingMatrix isotropicScaling)``

Transform an ellipse.

The method **Transform** has the following parameters:

+------------------------+------------------------------+---------------+
| Parameter              | Type                         | Description   |
+========================+==============================+===============+
| ``ellipse``            | ``Ellipse``                  |               |
+------------------------+------------------------------+---------------+
| ``isotropicScaling``   | ``IsotropicScalingMatrix``   |               |
+------------------------+------------------------------+---------------+

An ellipse transformed with a isotropic\_scaling transform results in another ellipse.

The isotropic scaling transformation is carried out with a matrix multiplication.

Returns the transformed ellipse.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Arc Transform(Arc arc, IsotropicScalingMatrix isotropicScaling)``

Transform an arc.

The method **Transform** has the following parameters:

+------------------------+------------------------------+---------------+
| Parameter              | Type                         | Description   |
+========================+==============================+===============+
| ``arc``                | ``Arc``                      |               |
+------------------------+------------------------------+---------------+
| ``isotropicScaling``   | ``IsotropicScalingMatrix``   |               |
+------------------------+------------------------------+---------------+

An arc transformed with a isotropic\_scaling transform results in another arc.

The isotropic scaling transformation is carried out with a matrix multiplication.

Returns the transformed arc.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``EllipticalArc Transform(EllipticalArc ellipticalArc, IsotropicScalingMatrix isotropicScaling)``

Transform an elliptical arc.

The method **Transform** has the following parameters:

+------------------------+------------------------------+---------------+
| Parameter              | Type                         | Description   |
+========================+==============================+===============+
| ``ellipticalArc``      | ``EllipticalArc``            |               |
+------------------------+------------------------------+---------------+
| ``isotropicScaling``   | ``IsotropicScalingMatrix``   |               |
+------------------------+------------------------------+---------------+

An elliptical arc transformed with a isotropic\_scaling transform results in another elliptical arc

The isotropic scaling transformation is carried out with a matrix multiplication.

Returns the transformed elliptical arc.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``TriangleDouble Transform(TriangleDouble triangle, IsotropicScalingMatrix isotropicScaling)``

Transform a triangle.

The method **Transform** has the following parameters:

+------------------------+------------------------------+---------------+
| Parameter              | Type                         | Description   |
+========================+==============================+===============+
| ``triangle``           | ``TriangleDouble``           |               |
+------------------------+------------------------------+---------------+
| ``isotropicScaling``   | ``IsotropicScalingMatrix``   |               |
+------------------------+------------------------------+---------------+

A triangle transformed with a isotropic\_scaling transform results in another triangle.

The isotropic scaling transformation is carried out with a matrix multiplication.

Returns the transformed triangle.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``QuadrilateralDouble Transform(QuadrilateralDouble quadrilateral, IsotropicScalingMatrix isotropicScaling)``

Transform a quadrilateral.

The method **Transform** has the following parameters:

+------------------------+------------------------------+---------------+
| Parameter              | Type                         | Description   |
+========================+==============================+===============+
| ``quadrilateral``      | ``QuadrilateralDouble``      |               |
+------------------------+------------------------------+---------------+
| ``isotropicScaling``   | ``IsotropicScalingMatrix``   |               |
+------------------------+------------------------------+---------------+

A quadrilateral transformed with a isotropic\_scaling transform results in another quadrilateral.

The isotropic scaling transformation is carried out with a matrix multiplication.

Returns the transformed quadrilateral.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``PolylineDouble Transform(PolylineDouble polyline, IsotropicScalingMatrix isotropicScaling)``

Transform a polyline.

The method **Transform** has the following parameters:

+------------------------+------------------------------+---------------+
| Parameter              | Type                         | Description   |
+========================+==============================+===============+
| ``polyline``           | ``PolylineDouble``           |               |
+------------------------+------------------------------+---------------+
| ``isotropicScaling``   | ``IsotropicScalingMatrix``   |               |
+------------------------+------------------------------+---------------+

A polyline transformed with a isotropic\_scaling transform results in another polyline.

The isotropic scaling transformation is carried out with a matrix multiplication.

Returns the transformed polyline.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``QuadraticBezier Transform(QuadraticBezier bezier, IsotropicScalingMatrix isotropicScaling)``

Transform a quadratic bezier curve.

The method **Transform** has the following parameters:

+------------------------+------------------------------+---------------+
| Parameter              | Type                         | Description   |
+========================+==============================+===============+
| ``bezier``             | ``QuadraticBezier``          |               |
+------------------------+------------------------------+---------------+
| ``isotropicScaling``   | ``IsotropicScalingMatrix``   |               |
+------------------------+------------------------------+---------------+

A quadratic bezier curve transformed with a isotropic\_scaling transform results in another quadratic bezier curve.

The isotropic scaling transformation is carried out with a matrix multiplication.

Returns the transformed quadratic bezier curve.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``CubicBezier Transform(CubicBezier bezier, IsotropicScalingMatrix isotropicScaling)``

Transform a cubic bezier curve.

The method **Transform** has the following parameters:

+------------------------+------------------------------+---------------+
| Parameter              | Type                         | Description   |
+========================+==============================+===============+
| ``bezier``             | ``CubicBezier``              |               |
+------------------------+------------------------------+---------------+
| ``isotropicScaling``   | ``IsotropicScalingMatrix``   |               |
+------------------------+------------------------------+---------------+

A cubic bezier curve transformed with a isotropic\_scaling transform results in another cubic bezier curve.

The isotropic scaling transformation is carried out with a matrix multiplication.

Returns the transformed cubic bezier curve.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Geometry Transform(Geometry geometry, IsotropicScalingMatrix isotropicScaling)``

Transform a geometry.

The method **Transform** has the following parameters:

+------------------------+------------------------------+---------------+
| Parameter              | Type                         | Description   |
+========================+==============================+===============+
| ``geometry``           | ``Geometry``                 |               |
+------------------------+------------------------------+---------------+
| ``isotropicScaling``   | ``IsotropicScalingMatrix``   |               |
+------------------------+------------------------------+---------------+

A geometry transformed with a isotropic\_scaling transform results in another geometry.

The isotropic scaling transformation is carried out by affine transformation of each geometry element.

Returns the transformed geometry.
