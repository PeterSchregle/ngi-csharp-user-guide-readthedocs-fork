Class *EllipticalArc*
---------------------

The following operations are implemented: operator== : comparison for equality.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **EllipticalArc** implements the following interfaces:

+-------------------------------+
| Interface                     |
+===============================+
| ``IEquatableEllipticalArc``   |
+-------------------------------+
| ``ISerializable``             |
+-------------------------------+
| ``INotifyPropertyChanged``    |
+-------------------------------+

The class **EllipticalArc** contains the following properties:

+----------------------+-------+-------+---------------------------------------------------------------+
| Property             | Get   | Set   | Description                                                   |
+======================+=======+=======+===============================================================+
| ``A``                | \*    | \*    | The start point of the elliptical arc.                        |
+----------------------+-------+-------+---------------------------------------------------------------+
| ``B``                | \*    | \*    | The end point of the elliptical arc.                          |
+----------------------+-------+-------+---------------------------------------------------------------+
| ``Radius``           | \*    | \*    | The radius of the elliptical arc.                             |
+----------------------+-------+-------+---------------------------------------------------------------+
| ``Direction``        | \*    | \*    | The direction of the elliptical arc.                          |
+----------------------+-------+-------+---------------------------------------------------------------+
| ``Sweep``            | \*    | \*    | The drawing direction of an elliptical arc.                   |
+----------------------+-------+-------+---------------------------------------------------------------+
| ``Size``             | \*    | \*    | The size of an elliptical arc.                                |
+----------------------+-------+-------+---------------------------------------------------------------+
| ``Normal``           | \*    |       | The normal direction of the elliptical arc.                   |
+----------------------+-------+-------+---------------------------------------------------------------+
| ``Center``           | \*    |       | Calculate the center point of the ellipse defining the arc.   |
+----------------------+-------+-------+---------------------------------------------------------------+
| ``BoundingBox``      | \*    |       | Get the bounding box of the elliptical\_arc.                  |
+----------------------+-------+-------+---------------------------------------------------------------+
| ``StartDirection``   | \*    |       | Get the start direction.                                      |
+----------------------+-------+-------+---------------------------------------------------------------+
| ``StopDirection``    | \*    |       | Get the stop direction.                                       |
+----------------------+-------+-------+---------------------------------------------------------------+

The class **EllipticalArc** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

The class **EllipticalArc** contains the following enumerations:

+----------------------+------------------------------+
| Enumeration          | Description                  |
+======================+==============================+
| ``SweepDirection``   | TODO documentation missing   |
+----------------------+------------------------------+
| ``ArcSize``          | TODO documentation missing   |
+----------------------+------------------------------+

Description
~~~~~~~~~~~

operator != : comparison for inequality. operator- : negation, directly implemented. operator+ : addition, implemented through boost::additive operator-= : subtraction, directly implemented. operator- : subtraction, implemented through boost::additive

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *EllipticalArc*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``EllipticalArc()``

Construct an elliptical\_arc.

Constructors
~~~~~~~~~~~~

Constructor *EllipticalArc*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``EllipticalArc(PointDouble a, PointDouble b, VectorDouble radius, Direction direction, EllipticalArc.SweepDirection sweep, EllipticalArc.ArcSize size)``

Construct an elliptical\_arc.

The constructor has the following parameters:

+-----------------+------------------------------------+------------------------------------------+
| Parameter       | Type                               | Description                              |
+=================+====================================+==========================================+
| ``a``           | ``PointDouble``                    | The point a.                             |
+-----------------+------------------------------------+------------------------------------------+
| ``b``           | ``PointDouble``                    | The point b.                             |
+-----------------+------------------------------------+------------------------------------------+
| ``radius``      | ``VectorDouble``                   | The radius.                              |
+-----------------+------------------------------------+------------------------------------------+
| ``direction``   | ``Direction``                      | The direction of the defining ellipse.   |
+-----------------+------------------------------------+------------------------------------------+
| ``sweep``       | ``EllipticalArc.SweepDirection``   | The sweep direction.                     |
+-----------------+------------------------------------+------------------------------------------+
| ``size``        | ``EllipticalArc.ArcSize``          | The arc size.                            |
+-----------------+------------------------------------+------------------------------------------+

Properties
~~~~~~~~~~

Property *A*
^^^^^^^^^^^^

``PointDouble A``

The start point of the elliptical arc.

Property *B*
^^^^^^^^^^^^

``PointDouble B``

The end point of the elliptical arc.

Property *Radius*
^^^^^^^^^^^^^^^^^

``VectorDouble Radius``

The radius of the elliptical arc.

Property *Direction*
^^^^^^^^^^^^^^^^^^^^

``Direction Direction``

The direction of the elliptical arc.

Property *Sweep*
^^^^^^^^^^^^^^^^

``EllipticalArc.SweepDirection Sweep``

The drawing direction of an elliptical arc.

Property *Size*
^^^^^^^^^^^^^^^

``EllipticalArc.ArcSize Size``

The size of an elliptical arc.

Property *Normal*
^^^^^^^^^^^^^^^^^

``Direction Normal``

The normal direction of the elliptical arc.

Property *Center*
^^^^^^^^^^^^^^^^^

``PointDouble Center``

Calculate the center point of the ellipse defining the arc.

This follows roughly the algorithm published here: http://fridrich.blogspot.de/2011/06/bounding-box-of-svg-elliptical-arc.html or http://www.w3.org/TR/SVG/implnote.html#ArcConversionEndpointToCenter

Property *BoundingBox*
^^^^^^^^^^^^^^^^^^^^^^

``BoxDouble BoundingBox``

Get the bounding box of the elliptical\_arc.

The bounding box is an axis aligned box that bounds the elliptical\_arc This follows the algorithm published here: http://fridrich.blogspot.de/2011/06/bounding-box-of-svg-elliptical-arc.html

It uses the parametric description of an ellipse: x(theta) = cx + rx\ *cos(theta)*\ cos(alpha) - ry\ *sin(theta)*\ sin(alpha) y(theta) = cy + rx\ *cos(theta)*\ sin(alpha) + ry\ *sin(theta)*\ cos(alpha)

where cx and cy are the coordinates of the centre of the ellipse, rx and ry are the radii and alpha is the x-axis-rotation.

The extreme x values of the ellipse are found for theta\_xmin = -atan(ry*tan(alpha)/rx) and theta\_xmax = M\_PI -atan(ry*\ tan(alpha)/rx)

The extreme y values of the ellipse are found for theta\_ymin = atan(ry/(tan(alpha)\ *rx)) and theta\_ymax = M\_PI + atan(ry/(tan(alpha)*\ rx))

The start and end points of the arc may result in a reduction of the size of the box, depending on their positions and further parameters of the arc. This is checked by converting all relevant points to polar coordinates and checking which of the extremes of the whole ellipse actually lie on the arc. The bounding box of the elliptical\_arc.

Property *StartDirection*
^^^^^^^^^^^^^^^^^^^^^^^^^

``Direction StartDirection``

Get the start direction.

The start direction is opposite to the direction.

Property *StopDirection*
^^^^^^^^^^^^^^^^^^^^^^^^

``Direction StopDirection``

Get the stop direction.

The stop direction is the same as the direction.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Enumerations
~~~~~~~~~~~~

Enumeration *SweepDirection*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum SweepDirection``

TODO documentation missing

The enumeration **SweepDirection** has the following constants:

+--------------------------------------+---------+---------------+
| Name                                 | Value   | Description   |
+======================================+=========+===============+
| ``sweepDirectionCounterClockwise``   | ``0``   |               |
+--------------------------------------+---------+---------------+
| ``sweepDirectionClockwise``          | ``1``   |               |
+--------------------------------------+---------+---------------+

::

    enum SweepDirection
    {
      sweepDirectionCounterClockwise = 0,
      sweepDirectionClockwise = 1,
    };

TODO documentation missing

Enumeration *ArcSize*
^^^^^^^^^^^^^^^^^^^^^

``enum ArcSize``

TODO documentation missing

The enumeration **ArcSize** has the following constants:

+--------------------+---------+---------------+
| Name               | Value   | Description   |
+====================+=========+===============+
| ``arcSizeSmall``   | ``0``   |               |
+--------------------+---------+---------------+
| ``arcSizeLarge``   | ``1``   |               |
+--------------------+---------+---------------+

::

    enum ArcSize
    {
      arcSizeSmall = 0,
      arcSizeLarge = 1,
    };

TODO documentation missing

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO no brief description for variant

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO no description for variant
