Class *WidgetCountTool*
-----------------------

A widget\_count\_tool is a tool for interactive counting.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetCountTool** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetCountTool** contains the following properties:

+------------------------------------+-------+-------+--------------------------------------------------+
| Property                           | Get   | Set   | Description                                      |
+====================================+=======+=======+==================================================+
| ``Position``                       | \*    | \*    | The position of the cross-hair.                  |
+------------------------------------+-------+-------+--------------------------------------------------+
| ``CrosshairPen``                   | \*    | \*    | The pen of the crosshair lines.                  |
+------------------------------------+-------+-------+--------------------------------------------------+
| ``Groups``                         | \*    | \*    | The groups.                                      |
+------------------------------------+-------+-------+--------------------------------------------------+
| ``CurrentGroupIndex``              | \*    | \*    | The current group.                               |
+------------------------------------+-------+-------+--------------------------------------------------+
| ``TotalCount``                     | \*    |       | The total count.                                 |
+------------------------------------+-------+-------+--------------------------------------------------+
| ``CanResetExecute``                | \*    |       | Can the reset method execute?                    |
+------------------------------------+-------+-------+--------------------------------------------------+
| ``CanDeleteCurrentGroupExecute``   | \*    |       | Can the delete\_current\_group method execute?   |
+------------------------------------+-------+-------+--------------------------------------------------+

The class **WidgetCountTool** contains the following methods:

+--------------------------+-----------------------------+
| Method                   | Description                 |
+==========================+=============================+
| ``Reset``                | Reset the count tool.       |
+--------------------------+-----------------------------+
| ``DeleteCurrentGroup``   | Delete the current group.   |
+--------------------------+-----------------------------+
| ``AddGroup``             | Add group.                  |
+--------------------------+-----------------------------+

Description
~~~~~~~~~~~

A widget\_count\_tool has interactive widget capabilities.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetCountTool*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetCountTool()``

Default constructor.

The default constructor creates a widget with default settings.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``PointDouble Position``

The position of the cross-hair.

Property *CrosshairPen*
^^^^^^^^^^^^^^^^^^^^^^^

``PenByte CrosshairPen``

The pen of the crosshair lines.

Property *Groups*
^^^^^^^^^^^^^^^^^

``CountToolGroupList Groups``

The groups.

Property *CurrentGroupIndex*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Int32 CurrentGroupIndex``

The current group.

Property *TotalCount*
^^^^^^^^^^^^^^^^^^^^^

``System.Int32 TotalCount``

The total count.

Property *CanResetExecute*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean CanResetExecute``

Can the reset method execute?

Property *CanDeleteCurrentGroupExecute*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean CanDeleteCurrentGroupExecute``

Can the delete\_current\_group method execute?

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Reset the count tool.

Method *DeleteCurrentGroup*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void DeleteCurrentGroup()``

Delete the current group.

Method *AddGroup*
^^^^^^^^^^^^^^^^^

``void AddGroup()``

Add group.
