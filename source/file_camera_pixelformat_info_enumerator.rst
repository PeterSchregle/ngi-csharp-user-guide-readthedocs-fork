Class *CameraPixelformatInfoEnumerator*
---------------------------------------

**Namespace:** Ngi

**Module:** Camera

The class **CameraPixelformatInfoEnumerator** implements the following interfaces:

+----------------------------------------+
| Interface                              |
+========================================+
| ``IEnumerator``                        |
+----------------------------------------+
| ``IEnumeratorCameraPixelformatInfo``   |
+----------------------------------------+

The class **CameraPixelformatInfoEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **CameraPixelformatInfoEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``CameraPixelformatInfo Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
