Class *TeighaParser2*
---------------------

Parse DXF/DWG file and create a drawing.

**Namespace:** Ngi

**Module:** CadPdf

Description
~~~~~~~~~~~

A drawing consists of a box specifying its dimensions, and of a list of layers.

Static Methods
~~~~~~~~~~~~~~

Method *ParseDxfFile*
^^^^^^^^^^^^^^^^^^^^^

``TeighaDrawing2 ParseDxfFile(System.String fileName)``

Opens and parses a DXF file.

The method **ParseDxfFile** has the following parameters:

+----------------+---------------------+---------------+
| Parameter      | Type                | Description   |
+================+=====================+===============+
| ``fileName``   | ``System.String``   |               |
+----------------+---------------------+---------------+

A teigha\_drawing, containing the layers.
