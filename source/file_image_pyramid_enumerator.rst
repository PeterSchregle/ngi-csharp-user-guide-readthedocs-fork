Class *ImagePyramidEnumerator*
------------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **ImagePyramidEnumerator** implements the following interfaces:

+-----------------------------------------------------+
| Interface                                           |
+=====================================================+
| ``IEnumerator``                                     |
+-----------------------------------------------------+
| ``IEnumeratorImagePyramidEnumeratorImageVariant``   |
+-----------------------------------------------------+

The class **ImagePyramidEnumerator** contains the following variant parameters:

+-------------+-----------------------------------------+
| Variant     | Description                             |
+=============+=========================================+
| ``Image``   | TODO no brief description for variant   |
+-------------+-----------------------------------------+

The class **ImagePyramidEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **ImagePyramidEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Image*
^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Image** has the following types:

+-----------------------+
| Type                  |
+=======================+
| ``ImageByte``         |
+-----------------------+
| ``ImageUInt16``       |
+-----------------------+
| ``ImageUInt32``       |
+-----------------------+
| ``ImageDouble``       |
+-----------------------+
| ``ImageRgbByte``      |
+-----------------------+
| ``ImageRgbUInt16``    |
+-----------------------+
| ``ImageRgbUInt32``    |
+-----------------------+
| ``ImageRgbDouble``    |
+-----------------------+
| ``ImageRgbaByte``     |
+-----------------------+
| ``ImageRgbaUInt16``   |
+-----------------------+
| ``ImageRgbaUInt32``   |
+-----------------------+
| ``ImageRgbaDouble``   |
+-----------------------+
| ``ImageHlsByte``      |
+-----------------------+
| ``ImageHlsUInt16``    |
+-----------------------+
| ``ImageHlsDouble``    |
+-----------------------+
| ``ImageHsiByte``      |
+-----------------------+
| ``ImageHsiUInt16``    |
+-----------------------+
| ``ImageHsiDouble``    |
+-----------------------+
| ``ImageLabByte``      |
+-----------------------+
| ``ImageLabUInt16``    |
+-----------------------+
| ``ImageLabDouble``    |
+-----------------------+
| ``ImageXyzByte``      |
+-----------------------+
| ``ImageXyzUInt16``    |
+-----------------------+
| ``ImageXyzDouble``    |
+-----------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Image Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
