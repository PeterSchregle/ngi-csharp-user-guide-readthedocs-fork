Class *Matrix*
--------------

A matrix class with fundamental operations of numerical linear algebra.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Matrix** implements the following interfaces:

+------------------------+
| Interface              |
+========================+
| ``IEquatableMatrix``   |
+------------------------+
| ``ISerializable``      |
+------------------------+

The class **Matrix** contains the following properties:

+-----------------------+-------+-------+--------------------------------------------------------------------------------+
| Property              | Get   | Set   | Description                                                                    |
+=======================+=======+=======+================================================================================+
| ``Rows``              | \*    |       | The number of rows of the matrix.                                              |
+-----------------------+-------+-------+--------------------------------------------------------------------------------+
| ``Columns``           | \*    |       | The number of columns of the matrix.                                           |
+-----------------------+-------+-------+--------------------------------------------------------------------------------+
| ``OneNorm``           | \*    |       | The one norm is the maximum columns sum of the matrix.                         |
+-----------------------+-------+-------+--------------------------------------------------------------------------------+
| ``InfinityNorm``      | \*    |       | The infinity norm is the maximum row sum of the matrix.                        |
+-----------------------+-------+-------+--------------------------------------------------------------------------------+
| ``FrobeniusNorm``     | \*    |       | The Frobenius norm is the square root of the sum of squares of all elements.   |
+-----------------------+-------+-------+--------------------------------------------------------------------------------+
| ``IsSquare``          | \*    |       | A matrix is square if it has the same number of rows and columns.              |
+-----------------------+-------+-------+--------------------------------------------------------------------------------+
| ``IsSymmetric``       | \*    |       | A matrix is symmetric if it is equal to its transposed matrix.                 |
+-----------------------+-------+-------+--------------------------------------------------------------------------------+
| ``IsAntisymmetric``   | \*    |       | A matrix is antisymmetric if it is equal to the negative of its.               |
+-----------------------+-------+-------+--------------------------------------------------------------------------------+
| ``Determinant``       | \*    |       | The determinant is defined for square matrices only.                           |
+-----------------------+-------+-------+--------------------------------------------------------------------------------+
| ``Trace``             | \*    |       | The trace of a matrix is the sum of its diagonal elements.                     |
+-----------------------+-------+-------+--------------------------------------------------------------------------------+

The class **Matrix** contains the following methods:

+-----------------+-----------------------------------------------------------+
| Method          | Description                                               |
+=================+===========================================================+
| ``Submatrix``   | Returns a sub matrix extracted from the current matrix.   |
+-----------------+-----------------------------------------------------------+
| ``Transpose``   | Returns the transpose of the matrix.                      |
+-----------------+-----------------------------------------------------------+

Description
~~~~~~~~~~~

The size of the matrix, as well as the type is determined with template parameters.

This implementation has been inspired by JAMA (see http://math.nist.gov/javanumerics/jama/). JAMA is a basic linear algebra package for Java. It provides user-level classes for constructing and manipulating real, dense matrices. It is meant to provide sufficient functionality for routine problems, packaged in a way that is natural and understandable to non-experts.

This implementation is comprised of six classes: matrix, cholesky\_decomposition, lu\_decomposition, ar\_decomposition, singular\_value\_decomposition and eigenvalue\_decomposition.

The matrix class provides the fundamental operations of numerical linear algebra. Various constructors create matrices from two dimensional arrays of double precision floating point numbers. Various gets and sets provide access to submatrices and matrix elements. The basic arithmetic operations include matrix addition and multiplication, matrix norms and selected element-by-element array operations.

Five fundamental matrix decompositions, which consist of pairs or triples of matrices, permutation vectors, and the like, produce results in five decomposition classes. These decompositions are accessed by the matrix class to compute solutions of simultaneous linear equations, determinants, inverses and other matrix functions. The five decompositions are

.. raw:: html

   <itemizedlist>

Cholesky Decomposition of symmetric, positive definite matrices,

LU Decomposition (Gaussian elimination) of rectangular matrices,

QR Decomposition of rectangular matrices,

Eigenvalue Decomposition of both symmetric and non-symmetric square matrices,

Singular Value Decomposition of rectangular matrices .

.. raw:: html

   </itemizedlist>

The following operations are implemented: operator - : negation, directly implemented. operator + : addition, implemented through boost::additive operator -= : subtraction, directly implemented. operator - : subtraction, implemented through boost::additive

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Matrix*
^^^^^^^^^^^^^^^^^^^^

``Matrix()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *Matrix*
^^^^^^^^^^^^^^^^^^^^

``Matrix(System.Int32 rows, System.Int32 columns, System.Double value)``

Construct a matrix of a given size and assigns a value to all.

The constructor has the following parameters:

+---------------+---------------------+-------------------------------------------------+
| Parameter     | Type                | Description                                     |
+===============+=====================+=================================================+
| ``rows``      | ``System.Int32``    | The number of rows of the matrix.               |
+---------------+---------------------+-------------------------------------------------+
| ``columns``   | ``System.Int32``    | The number of columns of the matrix.            |
+---------------+---------------------+-------------------------------------------------+
| ``value``     | ``System.Double``   | The value to assign to the diagonal elements.   |
+---------------+---------------------+-------------------------------------------------+

diagonal elements. All other elements are set to zero.

Constructor *Matrix*
^^^^^^^^^^^^^^^^^^^^

``Matrix(PerspectiveMatrix rhs)``

Construct a matrix from a given affine\_matrix.

The constructor has the following parameters:

+-------------+-------------------------+---------------+
| Parameter   | Type                    | Description   |
+=============+=========================+===============+
| ``rhs``     | ``PerspectiveMatrix``   |               |
+-------------+-------------------------+---------------+

Properties
~~~~~~~~~~

Property *Rows*
^^^^^^^^^^^^^^^

``System.Int32 Rows``

The number of rows of the matrix.

Property *Columns*
^^^^^^^^^^^^^^^^^^

``System.Int32 Columns``

The number of columns of the matrix.

Property *OneNorm*
^^^^^^^^^^^^^^^^^^

``System.Double OneNorm``

The one norm is the maximum columns sum of the matrix.

Property *InfinityNorm*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Double InfinityNorm``

The infinity norm is the maximum row sum of the matrix.

Property *FrobeniusNorm*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double FrobeniusNorm``

The Frobenius norm is the square root of the sum of squares of all elements.

Property *IsSquare*
^^^^^^^^^^^^^^^^^^^

``System.Boolean IsSquare``

A matrix is square if it has the same number of rows and columns.

Property *IsSymmetric*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsSymmetric``

A matrix is symmetric if it is equal to its transposed matrix.

Property *IsAntisymmetric*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsAntisymmetric``

A matrix is antisymmetric if it is equal to the negative of its.

transposed matrix.

Property *Determinant*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double Determinant``

The determinant is defined for square matrices only.

If the determinant is zero, the matrix is singular and has no inverse.

Property *Trace*
^^^^^^^^^^^^^^^^

``System.Double Trace``

The trace of a matrix is the sum of its diagonal elements.

Methods
~~~~~~~

Method *Submatrix*
^^^^^^^^^^^^^^^^^^

``Matrix Submatrix(System.Int32 startRow, System.Int32 rows, System.Int32 startColumn, System.Int32 columns)``

Returns a sub matrix extracted from the current matrix.

The method **Submatrix** has the following parameters:

+-------------------+--------------------+--------------------------+
| Parameter         | Type               | Description              |
+===================+====================+==========================+
| ``startRow``      | ``System.Int32``   | Starting row index.      |
+-------------------+--------------------+--------------------------+
| ``rows``          | ``System.Int32``   | Number of rows.          |
+-------------------+--------------------+--------------------------+
| ``startColumn``   | ``System.Int32``   | Starting column index.   |
+-------------------+--------------------+--------------------------+
| ``columns``       | ``System.Int32``   | Number of columns.       |
+-------------------+--------------------+--------------------------+

The submatrix is extracted by copying values from this matrix to a new (sub)matrix of the specified size.

The extracted matrix.

Method *Transpose*
^^^^^^^^^^^^^^^^^^

``Matrix Transpose()``

Returns the transpose of the matrix.

The transpose is calculated by creating a new matrix of the specified size and copying the values of this matrix to it in a way that rows and columns are exchanged.

The transposed matrix.
