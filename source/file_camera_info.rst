Class *CameraInfo*
------------------

**Namespace:** Ngi

**Module:** Camera

The class **CameraInfo** contains the following properties:

+--------------------+-------+-------+---------------+
| Property           | Get   | Set   | Description   |
+====================+=======+=======+===============+
| ``Id``             | \*    |       |               |
+--------------------+-------+-------+---------------+
| ``Vendor``         | \*    |       |               |
+--------------------+-------+-------+---------------+
| ``Model``          | \*    |       |               |
+--------------------+-------+-------+---------------+
| ``Displayname``    | \*    |       |               |
+--------------------+-------+-------+---------------+
| ``Pixelformats``   | \*    |       |               |
+--------------------+-------+-------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Id*
^^^^^^^^^^^^^

``System.String Id``

Property *Vendor*
^^^^^^^^^^^^^^^^^

``System.String Vendor``

Property *Model*
^^^^^^^^^^^^^^^^

``System.String Model``

Property *Displayname*
^^^^^^^^^^^^^^^^^^^^^^

``System.String Displayname``

Property *Pixelformats*
^^^^^^^^^^^^^^^^^^^^^^^

``CameraPixelformatInfoList Pixelformats``
