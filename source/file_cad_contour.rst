Class *CadContour*
------------------

A class holding a contour parsed from a cad file (dxf or geo).

**Namespace:** Ngi

**Module:** CadPdf

The class **CadContour** implements the following interfaces:

+----------------------------+
| Interface                  |
+============================+
| ``IEquatableCadContour``   |
+----------------------------+

The class **CadContour** contains the following properties:

+----------------+-------+-------+--------------------------------+
| Property       | Get   | Set   | Description                    |
+================+=======+=======+================================+
| ``IsCircle``   | \*    | \*    | Flag whether it is a circle.   |
+----------------+-------+-------+--------------------------------+
| ``Geometry``   | \*    | \*    | The geometry.                  |
+----------------+-------+-------+--------------------------------+
| ``Circle``     | \*    | \*    | The circle.                    |
+----------------+-------+-------+--------------------------------+

The class **CadContour** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

A contour can be a geometry or a circle which is currently realised by a geometry field, a circle field and a flag that indicates whether it is a geometry or a circle.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *CadContour*
^^^^^^^^^^^^^^^^^^^^^^^^

``CadContour()``

Constructs an empty cad\_contour.

Properties
~~~~~~~~~~

Property *IsCircle*
^^^^^^^^^^^^^^^^^^^

``System.Boolean IsCircle``

Flag whether it is a circle.

Property *Geometry*
^^^^^^^^^^^^^^^^^^^

``Geometry Geometry``

The geometry.

Property *Circle*
^^^^^^^^^^^^^^^^^

``Circle Circle``

The circle.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
