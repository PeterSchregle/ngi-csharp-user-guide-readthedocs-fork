Class *Edge1dInfoEnumerator*
----------------------------

**Namespace:** Ngi

**Module:**

The class **Edge1dInfoEnumerator** implements the following interfaces:

+-----------------------------+
| Interface                   |
+=============================+
| ``IEnumerator``             |
+-----------------------------+
| ``IEnumeratorEdge1dInfo``   |
+-----------------------------+

The class **Edge1dInfoEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **Edge1dInfoEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Edge1dInfo Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
