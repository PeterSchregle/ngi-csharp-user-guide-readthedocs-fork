Class *CameraParameterIinteger*
-------------------------------

**Namespace:** Ngi

**Module:** Camera

The class **CameraParameterIinteger** contains the following properties:

+----------------------+-------+-------+---------------+
| Property             | Get   | Set   | Description   |
+======================+=======+=======+===============+
| ``Value``            | \*    | \*    |               |
+----------------------+-------+-------+---------------+
| ``Min``              | \*    |       |               |
+----------------------+-------+-------+---------------+
| ``Max``              | \*    |       |               |
+----------------------+-------+-------+---------------+
| ``IncMode``          | \*    |       |               |
+----------------------+-------+-------+---------------+
| ``Inc``              | \*    |       |               |
+----------------------+-------+-------+---------------+
| ``Representation``   | \*    |       |               |
+----------------------+-------+-------+---------------+
| ``Unit``             | \*    |       |               |
+----------------------+-------+-------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Value*
^^^^^^^^^^^^^^^^

``System.Int64 Value``

Property *Min*
^^^^^^^^^^^^^^

``System.Int64 Min``

Property *Max*
^^^^^^^^^^^^^^

``System.Int64 Max``

Property *IncMode*
^^^^^^^^^^^^^^^^^^

``CameraParameterBase.IncModeType IncMode``

Property *Inc*
^^^^^^^^^^^^^^

``System.Int64 Inc``

Property *Representation*
^^^^^^^^^^^^^^^^^^^^^^^^^

``CameraParameterBase.RepresentationType Representation``

Property *Unit*
^^^^^^^^^^^^^^^

``System.String Unit``
