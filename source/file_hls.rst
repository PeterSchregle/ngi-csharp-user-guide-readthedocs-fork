Class *Hls*
-----------

The hls color type.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Hls** contains the following variant parameters:

+-----------------+-----------------------------------------+
| Variant         | Description                             |
+=================+=========================================+
| ``Component``   | TODO no brief description for variant   |
+-----------------+-----------------------------------------+

The class **Hls** contains the following properties:

+------------------+-------+-------+----------------------------------------------+
| Property         | Get   | Set   | Description                                  |
+==================+=======+=======+==============================================+
| ``Hue``          | \*    |       | The hue\_ color component property.          |
+------------------+-------+-------+----------------------------------------------+
| ``Lightness``    | \*    |       | The lightness\_ color component property.    |
+------------------+-------+-------+----------------------------------------------+
| ``Saturation``   | \*    |       | The saturation\_ color component property.   |
+------------------+-------+-------+----------------------------------------------+

The class **Hls** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

The hls color type template allows to be instantiated for the basic integer and floating point types.

Physically, the color primitives are in saturation - lightness - hue order, to match the most obvious bitmap organization. Logically, however, the color primitives are in hue - lightness - saturation order. The logical order is reflected in the parameter order in methods.

This class does not provide an implicit conversion operator to the hlsa type, but the other way round: the hlsa type provides a conversion from the hls type, so that buffers of this type can be conveniently displayed.

Variants
~~~~~~~~

Variant *Component*
^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Component** has the following types:

+--------------+
| Type         |
+==============+
| ``Int8``     |
+--------------+
| ``Byte``     |
+--------------+
| ``Int16``    |
+--------------+
| ``UInt16``   |
+--------------+
| ``Int32``    |
+--------------+
| ``UInt32``   |
+--------------+
| ``Int64``    |
+--------------+
| ``UInt64``   |
+--------------+
| ``Single``   |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Hls*
^^^^^^^^^^^^^^^^^

``Hls()``

Default constructor.

This constructor sets the three components to 0. This corresponds to a color that is pure black.

Constructors
~~~~~~~~~~~~

Constructor *Hls*
^^^^^^^^^^^^^^^^^

``Hls(System.Object value)``

Standard constructor.

The constructor has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``value``   | ``System.Object``   |               |
+-------------+---------------------+---------------+

This constructor initializes the three components with a gray value.

The constructor also serves as a conversion operator from the scalar type T to type hls<T>.

Constructor *Hls*
^^^^^^^^^^^^^^^^^

``Hls(System.Object hue, System.Object lightness, System.Object saturation)``

Standard constructor.

The constructor has the following parameters:

+------------------+---------------------+-----------------------------+
| Parameter        | Type                | Description                 |
+==================+=====================+=============================+
| ``hue``          | ``System.Object``   | The hue component.          |
+------------------+---------------------+-----------------------------+
| ``lightness``    | ``System.Object``   | The lightness component.    |
+------------------+---------------------+-----------------------------+
| ``saturation``   | ``System.Object``   | The saturation component.   |
+------------------+---------------------+-----------------------------+

This constructor initializes the three components with different values.

Properties
~~~~~~~~~~

Property *Hue*
^^^^^^^^^^^^^^

``System.Object Hue``

The hue\_ color component property.

Property *Lightness*
^^^^^^^^^^^^^^^^^^^^

``System.Object Lightness``

The lightness\_ color component property.

Property *Saturation*
^^^^^^^^^^^^^^^^^^^^^

``System.Object Saturation``

The saturation\_ color component property.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
