Class *CountToolGroup*
----------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **CountToolGroup** implements the following interfaces:

+--------------------------------+
| Interface                      |
+================================+
| ``IEquatableCountToolGroup``   |
+--------------------------------+
| ``ISerializable``              |
+--------------------------------+

The class **CountToolGroup** contains the following properties:

+-------------------+-------+-------+-----------------------------------------+
| Property          | Get   | Set   | Description                             |
+===================+=======+=======+=========================================+
| ``Name``          | \*    | \*    | The name of the count\_tool\_group.     |
+-------------------+-------+-------+-----------------------------------------+
| ``Points``        | \*    | \*    | The points in the count\_tool\_group.   |
+-------------------+-------+-------+-----------------------------------------+
| ``SymbolColor``   | \*    | \*    | The color of the group symbol.          |
+-------------------+-------+-------+-----------------------------------------+

The class **CountToolGroup** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+
| ``Reset``      | Reset the group.                                        |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *CountToolGroup*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``CountToolGroup()``

Standard constructor.

Properties
~~~~~~~~~~

Property *Name*
^^^^^^^^^^^^^^^

``System.String Name``

The name of the count\_tool\_group.

Property *Points*
^^^^^^^^^^^^^^^^^

``PointListPointDouble Points``

The points in the count\_tool\_group.

Property *SymbolColor*
^^^^^^^^^^^^^^^^^^^^^^

``RgbByte SymbolColor``

The color of the group symbol.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Reset the group.
