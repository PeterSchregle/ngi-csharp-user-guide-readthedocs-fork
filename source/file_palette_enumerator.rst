Class *PaletteEnumerator*
-------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **PaletteEnumerator** implements the following interfaces:

+--------------------------------------------------+
| Interface                                        |
+==================================================+
| ``IEnumerator``                                  |
+--------------------------------------------------+
| ``IEnumeratorPaletteEnumeratorPaletteVariant``   |
+--------------------------------------------------+

The class **PaletteEnumerator** contains the following variant parameters:

+---------------+-----------------------------------------+
| Variant       | Description                             |
+===============+=========================================+
| ``Palette``   | TODO no brief description for variant   |
+---------------+-----------------------------------------+

The class **PaletteEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **PaletteEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Palette*
^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Palette** has the following types:

+-------------------------+
| Type                    |
+=========================+
| ``PaletteByte``         |
+-------------------------+
| ``PaletteUInt16``       |
+-------------------------+
| ``PaletteUInt32``       |
+-------------------------+
| ``PaletteDouble``       |
+-------------------------+
| ``PaletteRgbByte``      |
+-------------------------+
| ``PaletteRgbUInt16``    |
+-------------------------+
| ``PaletteRgbUInt32``    |
+-------------------------+
| ``PaletteRgbDouble``    |
+-------------------------+
| ``PaletteRgbaByte``     |
+-------------------------+
| ``PaletteRgbaUInt16``   |
+-------------------------+
| ``PaletteRgbaUInt32``   |
+-------------------------+
| ``PaletteRgbaDouble``   |
+-------------------------+
| ``PaletteHlsByte``      |
+-------------------------+
| ``PaletteHlsUInt16``    |
+-------------------------+
| ``PaletteHlsDouble``    |
+-------------------------+
| ``PaletteHsiByte``      |
+-------------------------+
| ``PaletteHsiUInt16``    |
+-------------------------+
| ``PaletteHsiDouble``    |
+-------------------------+
| ``PaletteLabByte``      |
+-------------------------+
| ``PaletteLabUInt16``    |
+-------------------------+
| ``PaletteLabDouble``    |
+-------------------------+
| ``PaletteXyzByte``      |
+-------------------------+
| ``PaletteXyzUInt16``    |
+-------------------------+
| ``PaletteXyzDouble``    |
+-------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Palette Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
