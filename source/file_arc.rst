Class *Arc*
-----------

An arc in two-dimensional euclidean space.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Arc** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``IEquatableArc``            |
+------------------------------+
| ``ISerializable``            |
+------------------------------+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **Arc** contains the following properties:

+----------------------+-------+-------+--------------------------------------------------------------+
| Property             | Get   | Set   | Description                                                  |
+======================+=======+=======+==============================================================+
| ``A``                | \*    | \*    | The start point of the arc.                                  |
+----------------------+-------+-------+--------------------------------------------------------------+
| ``B``                | \*    | \*    | The end point of the arc.                                    |
+----------------------+-------+-------+--------------------------------------------------------------+
| ``Radius``           | \*    | \*    | The radius of the arc.                                       |
+----------------------+-------+-------+--------------------------------------------------------------+
| ``Sweep``            | \*    | \*    | The drawing direction of an arc.                             |
+----------------------+-------+-------+--------------------------------------------------------------+
| ``Size``             | \*    | \*    | The size of an arc.                                          |
+----------------------+-------+-------+--------------------------------------------------------------+
| ``Center``           | \*    |       | Calculate the center point of the circle defining the arc.   |
+----------------------+-------+-------+--------------------------------------------------------------+
| ``BoundingBox``      | \*    |       | Get the bounding box of the arc.                             |
+----------------------+-------+-------+--------------------------------------------------------------+
| ``StartDirection``   | \*    |       | Get the start direction.                                     |
+----------------------+-------+-------+--------------------------------------------------------------+
| ``StopDirection``    | \*    |       | Get the stop direction.                                      |
+----------------------+-------+-------+--------------------------------------------------------------+

The class **Arc** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

The class **Arc** contains the following enumerations:

+----------------------+------------------------------+
| Enumeration          | Description                  |
+======================+==============================+
| ``SweepDirection``   | TODO documentation missing   |
+----------------------+------------------------------+
| ``ArcSize``          | TODO documentation missing   |
+----------------------+------------------------------+

Description
~~~~~~~~~~~

An arc is stored with two points, the radius, the sweep direction and the size.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Arc*
^^^^^^^^^^^^^^^^^

``Arc()``

Construct default arc.

All members are set to zero or the defaults.

Constructors
~~~~~~~~~~~~

Constructor *Arc*
^^^^^^^^^^^^^^^^^

``Arc(PointDouble a, PointDouble b, System.Double radius, Arc.SweepDirection sweep, Arc.ArcSize size)``

Construct an arc.

The constructor has the following parameters:

+--------------+--------------------------+------------------------+
| Parameter    | Type                     | Description            |
+==============+==========================+========================+
| ``a``        | ``PointDouble``          | The point a.           |
+--------------+--------------------------+------------------------+
| ``b``        | ``PointDouble``          | The point b.           |
+--------------+--------------------------+------------------------+
| ``radius``   | ``System.Double``        | The radius.            |
+--------------+--------------------------+------------------------+
| ``sweep``    | ``Arc.SweepDirection``   | The sweep direction.   |
+--------------+--------------------------+------------------------+
| ``size``     | ``Arc.ArcSize``          | The arc size.          |
+--------------+--------------------------+------------------------+

The arc is constructed from its two endpoints, a radius, a sweep direction and a size. This is the internal representation of the arc and also the representation that is used within Direct2D.

Constructor *Arc*
^^^^^^^^^^^^^^^^^

``Arc(PointDouble center, System.Double radius, System.Double startAngle, System.Double endAngle)``

Construct an arc.

The constructor has the following parameters:

+------------------+---------------------+---------------------+
| Parameter        | Type                | Description         |
+==================+=====================+=====================+
| ``center``       | ``PointDouble``     | The center point.   |
+------------------+---------------------+---------------------+
| ``radius``       | ``System.Double``   | The radius.         |
+------------------+---------------------+---------------------+
| ``startAngle``   | ``System.Double``   | The start angle.    |
+------------------+---------------------+---------------------+
| ``endAngle``     | ``System.Double``   | The end angle.      |
+------------------+---------------------+---------------------+

The arc is constructed from its center, a radius, a start and an end angle. This is the representation of an arc used in DXF files.

Properties
~~~~~~~~~~

Property *A*
^^^^^^^^^^^^

``PointDouble A``

The start point of the arc.

Property *B*
^^^^^^^^^^^^

``PointDouble B``

The end point of the arc.

Property *Radius*
^^^^^^^^^^^^^^^^^

``System.Double Radius``

The radius of the arc.

Property *Sweep*
^^^^^^^^^^^^^^^^

``Arc.SweepDirection Sweep``

The drawing direction of an arc.

Property *Size*
^^^^^^^^^^^^^^^

``Arc.ArcSize Size``

The size of an arc.

Property *Center*
^^^^^^^^^^^^^^^^^

``PointDouble Center``

Calculate the center point of the circle defining the arc.

Property *BoundingBox*
^^^^^^^^^^^^^^^^^^^^^^

``BoxDouble BoundingBox``

Get the bounding box of the arc.

The bounding box is an axis aligned box that bounds the arc The bounding box of the arc.

Property *StartDirection*
^^^^^^^^^^^^^^^^^^^^^^^^^

``Direction StartDirection``

Get the start direction.

The start direction is the same as the direction.

Property *StopDirection*
^^^^^^^^^^^^^^^^^^^^^^^^

``Direction StopDirection``

Get the stop direction.

The stop direction is opposite to the direction.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Enumerations
~~~~~~~~~~~~

Enumeration *SweepDirection*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum SweepDirection``

TODO documentation missing

The enumeration **SweepDirection** has the following constants:

+--------------------------------------+---------+---------------+
| Name                                 | Value   | Description   |
+======================================+=========+===============+
| ``sweepDirectionCounterClockwise``   | ``0``   |               |
+--------------------------------------+---------+---------------+
| ``sweepDirectionClockwise``          | ``1``   |               |
+--------------------------------------+---------+---------------+

::

    enum SweepDirection
    {
      sweepDirectionCounterClockwise = 0,
      sweepDirectionClockwise = 1,
    };

TODO documentation missing

Enumeration *ArcSize*
^^^^^^^^^^^^^^^^^^^^^

``enum ArcSize``

TODO documentation missing

The enumeration **ArcSize** has the following constants:

+--------------------+---------+---------------+
| Name               | Value   | Description   |
+====================+=========+===============+
| ``arcSizeSmall``   | ``0``   |               |
+--------------------+---------+---------------+
| ``arcSizeLarge``   | ``1``   |               |
+--------------------+---------+---------------+

::

    enum ArcSize
    {
      arcSizeSmall = 0,
      arcSizeLarge = 1,
    };

TODO documentation missing

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

This event occurs when a property value changes.

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

The event can indicate, that all properties on the object have changed by using either null or the empty string as the property name.
