Class *PreconditionViolatedException*
-------------------------------------

Exception class for precondition violations.

**Namespace:** Ngi

**Module:**

Description
~~~~~~~~~~~

In NGI, a precondition is a condition or predicate that must always be true prior to the execution of some section of code. If a precondition is violated, the effect of the section of code becomes undefined and thus may or may not work as intended.

NGI checks preconditions with the NGI\_PRECONDITION macro, which - when defined - throws the precondition\_violated exception when the precondition is not true.

The user of NGI can inhibit this behavior by defining the NGI\_PRECONDITION(condition, message) macro as an empty macro before the inclusion of any NGI header file.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *PreconditionViolatedException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``PreconditionViolatedException()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *PreconditionViolatedException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``PreconditionViolatedException(System.String message)``

Construct an exception from a message.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+

Constructor *PreconditionViolatedException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``PreconditionViolatedException(System.String message, System.UInt32 id)``

Construct an exception from a message and an id.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+
| ``id``        | ``System.UInt32``   | The exception id.        |
+---------------+---------------------+--------------------------+
