Class *PaletteList*
-------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **PaletteList** implements the following interfaces:

+--------------------------------------+
| Interface                            |
+======================================+
| ``IListPaletteListPaletteVariant``   |
+--------------------------------------+

The class **PaletteList** contains the following variant parameters:

+---------------+-----------------------------------------+
| Variant       | Description                             |
+===============+=========================================+
| ``Palette``   | TODO no brief description for variant   |
+---------------+-----------------------------------------+

The class **PaletteList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **PaletteList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Palette*
^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Palette** has the following types:

+-------------------------+
| Type                    |
+=========================+
| ``PaletteByte``         |
+-------------------------+
| ``PaletteUInt16``       |
+-------------------------+
| ``PaletteUInt32``       |
+-------------------------+
| ``PaletteDouble``       |
+-------------------------+
| ``PaletteRgbByte``      |
+-------------------------+
| ``PaletteRgbUInt16``    |
+-------------------------+
| ``PaletteRgbUInt32``    |
+-------------------------+
| ``PaletteRgbDouble``    |
+-------------------------+
| ``PaletteRgbaByte``     |
+-------------------------+
| ``PaletteRgbaUInt16``   |
+-------------------------+
| ``PaletteRgbaUInt32``   |
+-------------------------+
| ``PaletteRgbaDouble``   |
+-------------------------+
| ``PaletteHlsByte``      |
+-------------------------+
| ``PaletteHlsUInt16``    |
+-------------------------+
| ``PaletteHlsDouble``    |
+-------------------------+
| ``PaletteHsiByte``      |
+-------------------------+
| ``PaletteHsiUInt16``    |
+-------------------------+
| ``PaletteHsiDouble``    |
+-------------------------+
| ``PaletteLabByte``      |
+-------------------------+
| ``PaletteLabUInt16``    |
+-------------------------+
| ``PaletteLabDouble``    |
+-------------------------+
| ``PaletteXyzByte``      |
+-------------------------+
| ``PaletteXyzUInt16``    |
+-------------------------+
| ``PaletteXyzDouble``    |
+-------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *PaletteList*
^^^^^^^^^^^^^^^^^^^^^^^^^

``PaletteList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``Palette [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``PaletteEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(Palette item)``

The method **Add** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``item``    | ``Palette``   |               |
+-------------+---------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(Palette item)``

The method **Contains** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``item``    | ``Palette``   |               |
+-------------+---------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(Palette item)``

The method **Remove** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``item``    | ``Palette``   |               |
+-------------+---------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(Palette item)``

The method **IndexOf** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``item``    | ``Palette``   |               |
+-------------+---------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, Palette item)``

The method **Insert** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``item``    | ``Palette``        |               |
+-------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
