Class *ColorAlgorithms*
-----------------------

Basic color processing functions.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **ColorAlgorithms** contains the following enumerations:

+---------------+------------------------------+
| Enumeration   | Description                  |
+===============+==============================+
| ``Channel``   | TODO documentation missing   |
+---------------+------------------------------+

Description
~~~~~~~~~~~

The class contains a group of color image processing functions. It includes extracting channels from color images or combining channels to color images, color space conversion, demosaicking, as well as quantizing to a number of colors.

Static Methods
~~~~~~~~~~~~~~

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte ExtractChannel(ViewLocatorByte source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``ViewLocatorByte``           |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 ExtractChannel(ViewLocatorUInt16 source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``ViewLocatorUInt16``         |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 ExtractChannel(ViewLocatorUInt32 source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``ViewLocatorUInt32``         |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble ExtractChannel(ViewLocatorDouble source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``ViewLocatorDouble``         |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte ExtractChannel(ViewLocatorRgbByte source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``ViewLocatorRgbByte``        |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 ExtractChannel(ViewLocatorRgbUInt16 source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``ViewLocatorRgbUInt16``      |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 ExtractChannel(ViewLocatorRgbUInt32 source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``ViewLocatorRgbUInt32``      |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble ExtractChannel(ViewLocatorRgbDouble source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``ViewLocatorRgbDouble``      |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte ExtractChannel(ViewLocatorRgbaByte source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``ViewLocatorRgbaByte``       |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 ExtractChannel(ViewLocatorRgbaUInt16 source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``ViewLocatorRgbaUInt16``     |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 ExtractChannel(ViewLocatorRgbaUInt32 source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``ViewLocatorRgbaUInt32``     |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble ExtractChannel(ViewLocatorRgbaDouble source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``ViewLocatorRgbaDouble``     |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte ExtractChannel(ViewLocatorHlsByte source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``ViewLocatorHlsByte``        |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 ExtractChannel(ViewLocatorHlsUInt16 source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``ViewLocatorHlsUInt16``      |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble ExtractChannel(ViewLocatorHlsDouble source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``ViewLocatorHlsDouble``      |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte ExtractChannel(ViewLocatorHsiByte source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``ViewLocatorHsiByte``        |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 ExtractChannel(ViewLocatorHsiUInt16 source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``ViewLocatorHsiUInt16``      |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble ExtractChannel(ViewLocatorHsiDouble source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``ViewLocatorHsiDouble``      |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte ExtractChannel(ViewLocatorLabByte source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``ViewLocatorLabByte``        |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 ExtractChannel(ViewLocatorLabUInt16 source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``ViewLocatorLabUInt16``      |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble ExtractChannel(ViewLocatorLabDouble source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``ViewLocatorLabDouble``      |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte ExtractChannel(ViewLocatorXyzByte source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``ViewLocatorXyzByte``        |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 ExtractChannel(ViewLocatorXyzUInt16 source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``ViewLocatorXyzUInt16``      |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble ExtractChannel(ViewLocatorXyzDouble source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``ViewLocatorXyzDouble``      |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *ExtractChannel*
^^^^^^^^^^^^^^^^^^^^^^^

``Image ExtractChannel(View source, ColorAlgorithms.Channel extract)``

Extract the specified channel from the source.

The method **ExtractChannel** has the following parameters:

+---------------+-------------------------------+---------------+
| Parameter     | Type                          | Description   |
+===============+===============================+===============+
| ``source``    | ``View``                      |               |
+---------------+-------------------------------+---------------+
| ``extract``   | ``ColorAlgorithms.Channel``   |               |
+---------------+-------------------------------+---------------+

On a monochrome buffer, you can use only channel\_first or channel\_intensity and this defaults to a mere copy.

The algorithm supports parallel execution on multiple cores.

Method *CombineChannelsRgb*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte CombineChannelsRgb(ViewLocatorByte red, ViewLocatorByte green, ViewLocatorByte blue)``

Combine an image from multiple channels.

The method **CombineChannelsRgb** has the following parameters:

+-------------+-----------------------+---------------+
| Parameter   | Type                  | Description   |
+=============+=======================+===============+
| ``red``     | ``ViewLocatorByte``   |               |
+-------------+-----------------------+---------------+
| ``green``   | ``ViewLocatorByte``   |               |
+-------------+-----------------------+---------------+
| ``blue``    | ``ViewLocatorByte``   |               |
+-------------+-----------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *CombineChannelsRgb*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 CombineChannelsRgb(ViewLocatorUInt16 red, ViewLocatorUInt16 green, ViewLocatorUInt16 blue)``

Combine an image from multiple channels.

The method **CombineChannelsRgb** has the following parameters:

+-------------+-------------------------+---------------+
| Parameter   | Type                    | Description   |
+=============+=========================+===============+
| ``red``     | ``ViewLocatorUInt16``   |               |
+-------------+-------------------------+---------------+
| ``green``   | ``ViewLocatorUInt16``   |               |
+-------------+-------------------------+---------------+
| ``blue``    | ``ViewLocatorUInt16``   |               |
+-------------+-------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *CombineChannelsRgb*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble CombineChannelsRgb(ViewLocatorDouble red, ViewLocatorDouble green, ViewLocatorDouble blue)``

Combine an image from multiple channels.

The method **CombineChannelsRgb** has the following parameters:

+-------------+-------------------------+---------------+
| Parameter   | Type                    | Description   |
+=============+=========================+===============+
| ``red``     | ``ViewLocatorDouble``   |               |
+-------------+-------------------------+---------------+
| ``green``   | ``ViewLocatorDouble``   |               |
+-------------+-------------------------+---------------+
| ``blue``    | ``ViewLocatorDouble``   |               |
+-------------+-------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *CombineChannelsRgb*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Image CombineChannelsRgb(View red, View green, View blue)``

Combine an image from multiple channels.

The method **CombineChannelsRgb** has the following parameters:

+-------------+------------+---------------+
| Parameter   | Type       | Description   |
+=============+============+===============+
| ``red``     | ``View``   |               |
+-------------+------------+---------------+
| ``green``   | ``View``   |               |
+-------------+------------+---------------+
| ``blue``    | ``View``   |               |
+-------------+------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *CombineChannelsRgba*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte CombineChannelsRgba(ViewLocatorByte red, ViewLocatorByte green, ViewLocatorByte blue, ViewLocatorByte alpha)``

Combine an image from multiple channels.

The method **CombineChannelsRgba** has the following parameters:

+-------------+-----------------------+---------------+
| Parameter   | Type                  | Description   |
+=============+=======================+===============+
| ``red``     | ``ViewLocatorByte``   |               |
+-------------+-----------------------+---------------+
| ``green``   | ``ViewLocatorByte``   |               |
+-------------+-----------------------+---------------+
| ``blue``    | ``ViewLocatorByte``   |               |
+-------------+-----------------------+---------------+
| ``alpha``   | ``ViewLocatorByte``   |               |
+-------------+-----------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *CombineChannelsRgba*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 CombineChannelsRgba(ViewLocatorUInt16 red, ViewLocatorUInt16 green, ViewLocatorUInt16 blue, ViewLocatorUInt16 alpha)``

Combine an image from multiple channels.

The method **CombineChannelsRgba** has the following parameters:

+-------------+-------------------------+---------------+
| Parameter   | Type                    | Description   |
+=============+=========================+===============+
| ``red``     | ``ViewLocatorUInt16``   |               |
+-------------+-------------------------+---------------+
| ``green``   | ``ViewLocatorUInt16``   |               |
+-------------+-------------------------+---------------+
| ``blue``    | ``ViewLocatorUInt16``   |               |
+-------------+-------------------------+---------------+
| ``alpha``   | ``ViewLocatorUInt16``   |               |
+-------------+-------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *CombineChannelsRgba*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble CombineChannelsRgba(ViewLocatorDouble red, ViewLocatorDouble green, ViewLocatorDouble blue, ViewLocatorDouble alpha)``

Combine an image from multiple channels.

The method **CombineChannelsRgba** has the following parameters:

+-------------+-------------------------+---------------+
| Parameter   | Type                    | Description   |
+=============+=========================+===============+
| ``red``     | ``ViewLocatorDouble``   |               |
+-------------+-------------------------+---------------+
| ``green``   | ``ViewLocatorDouble``   |               |
+-------------+-------------------------+---------------+
| ``blue``    | ``ViewLocatorDouble``   |               |
+-------------+-------------------------+---------------+
| ``alpha``   | ``ViewLocatorDouble``   |               |
+-------------+-------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *CombineChannelsRgba*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Image CombineChannelsRgba(View red, View green, View blue, View alpha)``

Combine an image from multiple channels.

The method **CombineChannelsRgba** has the following parameters:

+-------------+------------+---------------+
| Parameter   | Type       | Description   |
+=============+============+===============+
| ``red``     | ``View``   |               |
+-------------+------------+---------------+
| ``green``   | ``View``   |               |
+-------------+------------+---------------+
| ``blue``    | ``View``   |               |
+-------------+------------+---------------+
| ``alpha``   | ``View``   |               |
+-------------+------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *CombineChannelsHls*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte CombineChannelsHls(ViewLocatorByte hue, ViewLocatorByte lightness, ViewLocatorByte saturation)``

Combine an image from multiple channels.

The method **CombineChannelsHls** has the following parameters:

+------------------+-----------------------+---------------+
| Parameter        | Type                  | Description   |
+==================+=======================+===============+
| ``hue``          | ``ViewLocatorByte``   |               |
+------------------+-----------------------+---------------+
| ``lightness``    | ``ViewLocatorByte``   |               |
+------------------+-----------------------+---------------+
| ``saturation``   | ``ViewLocatorByte``   |               |
+------------------+-----------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *CombineChannelsHls*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 CombineChannelsHls(ViewLocatorUInt16 hue, ViewLocatorUInt16 lightness, ViewLocatorUInt16 saturation)``

Combine an image from multiple channels.

The method **CombineChannelsHls** has the following parameters:

+------------------+-------------------------+---------------+
| Parameter        | Type                    | Description   |
+==================+=========================+===============+
| ``hue``          | ``ViewLocatorUInt16``   |               |
+------------------+-------------------------+---------------+
| ``lightness``    | ``ViewLocatorUInt16``   |               |
+------------------+-------------------------+---------------+
| ``saturation``   | ``ViewLocatorUInt16``   |               |
+------------------+-------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *CombineChannelsHls*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble CombineChannelsHls(ViewLocatorDouble hue, ViewLocatorDouble lightness, ViewLocatorDouble saturation)``

Combine an image from multiple channels.

The method **CombineChannelsHls** has the following parameters:

+------------------+-------------------------+---------------+
| Parameter        | Type                    | Description   |
+==================+=========================+===============+
| ``hue``          | ``ViewLocatorDouble``   |               |
+------------------+-------------------------+---------------+
| ``lightness``    | ``ViewLocatorDouble``   |               |
+------------------+-------------------------+---------------+
| ``saturation``   | ``ViewLocatorDouble``   |               |
+------------------+-------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *CombineChannelsHls*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Image CombineChannelsHls(View hue, View lightness, View saturation)``

Combine an image from multiple channels.

The method **CombineChannelsHls** has the following parameters:

+------------------+------------+---------------+
| Parameter        | Type       | Description   |
+==================+============+===============+
| ``hue``          | ``View``   |               |
+------------------+------------+---------------+
| ``lightness``    | ``View``   |               |
+------------------+------------+---------------+
| ``saturation``   | ``View``   |               |
+------------------+------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *CombineChannelsHsi*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte CombineChannelsHsi(ViewLocatorByte hue, ViewLocatorByte saturation, ViewLocatorByte intensity)``

Combine an image from multiple channels.

The method **CombineChannelsHsi** has the following parameters:

+------------------+-----------------------+---------------+
| Parameter        | Type                  | Description   |
+==================+=======================+===============+
| ``hue``          | ``ViewLocatorByte``   |               |
+------------------+-----------------------+---------------+
| ``saturation``   | ``ViewLocatorByte``   |               |
+------------------+-----------------------+---------------+
| ``intensity``    | ``ViewLocatorByte``   |               |
+------------------+-----------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *CombineChannelsHsi*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 CombineChannelsHsi(ViewLocatorUInt16 hue, ViewLocatorUInt16 saturation, ViewLocatorUInt16 intensity)``

Combine an image from multiple channels.

The method **CombineChannelsHsi** has the following parameters:

+------------------+-------------------------+---------------+
| Parameter        | Type                    | Description   |
+==================+=========================+===============+
| ``hue``          | ``ViewLocatorUInt16``   |               |
+------------------+-------------------------+---------------+
| ``saturation``   | ``ViewLocatorUInt16``   |               |
+------------------+-------------------------+---------------+
| ``intensity``    | ``ViewLocatorUInt16``   |               |
+------------------+-------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *CombineChannelsHsi*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble CombineChannelsHsi(ViewLocatorDouble hue, ViewLocatorDouble saturation, ViewLocatorDouble intensity)``

Combine an image from multiple channels.

The method **CombineChannelsHsi** has the following parameters:

+------------------+-------------------------+---------------+
| Parameter        | Type                    | Description   |
+==================+=========================+===============+
| ``hue``          | ``ViewLocatorDouble``   |               |
+------------------+-------------------------+---------------+
| ``saturation``   | ``ViewLocatorDouble``   |               |
+------------------+-------------------------+---------------+
| ``intensity``    | ``ViewLocatorDouble``   |               |
+------------------+-------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *CombineChannelsHsi*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Image CombineChannelsHsi(View hue, View saturation, View intensity)``

Combine an image from multiple channels.

The method **CombineChannelsHsi** has the following parameters:

+------------------+------------+---------------+
| Parameter        | Type       | Description   |
+==================+============+===============+
| ``hue``          | ``View``   |               |
+------------------+------------+---------------+
| ``saturation``   | ``View``   |               |
+------------------+------------+---------------+
| ``intensity``    | ``View``   |               |
+------------------+------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *CombineChannelsLab*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte CombineChannelsLab(ViewLocatorByte l, ViewLocatorByte a, ViewLocatorByte b)``

Combine an image from multiple channels.

The method **CombineChannelsLab** has the following parameters:

+-------------+-----------------------+---------------+
| Parameter   | Type                  | Description   |
+=============+=======================+===============+
| ``l``       | ``ViewLocatorByte``   |               |
+-------------+-----------------------+---------------+
| ``a``       | ``ViewLocatorByte``   |               |
+-------------+-----------------------+---------------+
| ``b``       | ``ViewLocatorByte``   |               |
+-------------+-----------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *CombineChannelsLab*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 CombineChannelsLab(ViewLocatorUInt16 l, ViewLocatorUInt16 a, ViewLocatorUInt16 b)``

Combine an image from multiple channels.

The method **CombineChannelsLab** has the following parameters:

+-------------+-------------------------+---------------+
| Parameter   | Type                    | Description   |
+=============+=========================+===============+
| ``l``       | ``ViewLocatorUInt16``   |               |
+-------------+-------------------------+---------------+
| ``a``       | ``ViewLocatorUInt16``   |               |
+-------------+-------------------------+---------------+
| ``b``       | ``ViewLocatorUInt16``   |               |
+-------------+-------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *CombineChannelsLab*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble CombineChannelsLab(ViewLocatorDouble l, ViewLocatorDouble a, ViewLocatorDouble b)``

Combine an image from multiple channels.

The method **CombineChannelsLab** has the following parameters:

+-------------+-------------------------+---------------+
| Parameter   | Type                    | Description   |
+=============+=========================+===============+
| ``l``       | ``ViewLocatorDouble``   |               |
+-------------+-------------------------+---------------+
| ``a``       | ``ViewLocatorDouble``   |               |
+-------------+-------------------------+---------------+
| ``b``       | ``ViewLocatorDouble``   |               |
+-------------+-------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *CombineChannelsLab*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Image CombineChannelsLab(View l, View a, View b)``

Combine an image from multiple channels.

The method **CombineChannelsLab** has the following parameters:

+-------------+------------+---------------+
| Parameter   | Type       | Description   |
+=============+============+===============+
| ``l``       | ``View``   |               |
+-------------+------------+---------------+
| ``a``       | ``View``   |               |
+-------------+------------+---------------+
| ``b``       | ``View``   |               |
+-------------+------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *CombineChannelsXyz*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte CombineChannelsXyz(ViewLocatorByte x, ViewLocatorByte y, ViewLocatorByte z)``

Combine an image from multiple channels.

The method **CombineChannelsXyz** has the following parameters:

+-------------+-----------------------+---------------+
| Parameter   | Type                  | Description   |
+=============+=======================+===============+
| ``x``       | ``ViewLocatorByte``   |               |
+-------------+-----------------------+---------------+
| ``y``       | ``ViewLocatorByte``   |               |
+-------------+-----------------------+---------------+
| ``z``       | ``ViewLocatorByte``   |               |
+-------------+-----------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *CombineChannelsXyz*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 CombineChannelsXyz(ViewLocatorUInt16 x, ViewLocatorUInt16 y, ViewLocatorUInt16 z)``

Combine an image from multiple channels.

The method **CombineChannelsXyz** has the following parameters:

+-------------+-------------------------+---------------+
| Parameter   | Type                    | Description   |
+=============+=========================+===============+
| ``x``       | ``ViewLocatorUInt16``   |               |
+-------------+-------------------------+---------------+
| ``y``       | ``ViewLocatorUInt16``   |               |
+-------------+-------------------------+---------------+
| ``z``       | ``ViewLocatorUInt16``   |               |
+-------------+-------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *CombineChannelsXyz*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble CombineChannelsXyz(ViewLocatorDouble x, ViewLocatorDouble y, ViewLocatorDouble z)``

Combine an image from multiple channels.

The method **CombineChannelsXyz** has the following parameters:

+-------------+-------------------------+---------------+
| Parameter   | Type                    | Description   |
+=============+=========================+===============+
| ``x``       | ``ViewLocatorDouble``   |               |
+-------------+-------------------------+---------------+
| ``y``       | ``ViewLocatorDouble``   |               |
+-------------+-------------------------+---------------+
| ``z``       | ``ViewLocatorDouble``   |               |
+-------------+-------------------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *CombineChannelsXyz*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Image CombineChannelsXyz(View x, View y, View z)``

Combine an image from multiple channels.

The method **CombineChannelsXyz** has the following parameters:

+-------------+------------+---------------+
| Parameter   | Type       | Description   |
+=============+============+===============+
| ``x``       | ``View``   |               |
+-------------+------------+---------------+
| ``y``       | ``View``   |               |
+-------------+------------+---------------+
| ``z``       | ``View``   |               |
+-------------+------------+---------------+

The algorithm supports parallel execution on multiple cores.

Method *ConvertToGray*
^^^^^^^^^^^^^^^^^^^^^^

``ImageByte ConvertToGray(ViewLocatorRgbByte source)``

Convert to gray/monochrome.

The method **ConvertToGray** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToGray*
^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 ConvertToGray(ViewLocatorRgbUInt16 source)``

Convert to gray/monochrome.

The method **ConvertToGray** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToGray*
^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble ConvertToGray(ViewLocatorRgbDouble source)``

Convert to gray/monochrome.

The method **ConvertToGray** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToGray*
^^^^^^^^^^^^^^^^^^^^^^

``System.Object ConvertToGray(View source)``

Convert to gray/monochrome.

The method **ConvertToGray** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

Method *ConvertToRgb*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte ConvertToRgb(ViewLocatorRgbByte source)``

Convert to rgb color space.

The method **ConvertToRgb** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToRgb*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 ConvertToRgb(ViewLocatorRgbUInt16 source)``

Convert to rgb color space.

The method **ConvertToRgb** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToRgb*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble ConvertToRgb(ViewLocatorRgbDouble source)``

Convert to rgb color space.

The method **ConvertToRgb** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToRgb*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte ConvertToRgb(ViewLocatorHlsByte source)``

Convert to rgb color space.

The method **ConvertToRgb** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToRgb*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 ConvertToRgb(ViewLocatorHlsUInt16 source)``

Convert to rgb color space.

The method **ConvertToRgb** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToRgb*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble ConvertToRgb(ViewLocatorHlsDouble source)``

Convert to rgb color space.

The method **ConvertToRgb** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToRgb*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte ConvertToRgb(ViewLocatorHsiByte source)``

Convert to rgb color space.

The method **ConvertToRgb** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToRgb*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 ConvertToRgb(ViewLocatorHsiUInt16 source)``

Convert to rgb color space.

The method **ConvertToRgb** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToRgb*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble ConvertToRgb(ViewLocatorHsiDouble source)``

Convert to rgb color space.

The method **ConvertToRgb** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToRgb*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte ConvertToRgb(ViewLocatorLabByte source)``

Convert to rgb color space.

The method **ConvertToRgb** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToRgb*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 ConvertToRgb(ViewLocatorLabUInt16 source)``

Convert to rgb color space.

The method **ConvertToRgb** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToRgb*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble ConvertToRgb(ViewLocatorLabDouble source)``

Convert to rgb color space.

The method **ConvertToRgb** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToRgb*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte ConvertToRgb(ViewLocatorXyzByte source)``

Convert to rgb color space.

The method **ConvertToRgb** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToRgb*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 ConvertToRgb(ViewLocatorXyzUInt16 source)``

Convert to rgb color space.

The method **ConvertToRgb** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToRgb*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble ConvertToRgb(ViewLocatorXyzDouble source)``

Convert to rgb color space.

The method **ConvertToRgb** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToRgb*
^^^^^^^^^^^^^^^^^^^^^

``System.Object ConvertToRgb(View source)``

Convert to rgb color space.

The method **ConvertToRgb** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

Method *ConvertToHls*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte ConvertToHls(ViewLocatorRgbByte source)``

Convert to hls color space.

The method **ConvertToHls** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToHls*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 ConvertToHls(ViewLocatorRgbUInt16 source)``

Convert to hls color space.

The method **ConvertToHls** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToHls*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble ConvertToHls(ViewLocatorRgbDouble source)``

Convert to hls color space.

The method **ConvertToHls** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToHls*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte ConvertToHls(ViewLocatorHlsByte source)``

Convert to hls color space.

The method **ConvertToHls** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToHls*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 ConvertToHls(ViewLocatorHlsUInt16 source)``

Convert to hls color space.

The method **ConvertToHls** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToHls*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble ConvertToHls(ViewLocatorHlsDouble source)``

Convert to hls color space.

The method **ConvertToHls** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToHls*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte ConvertToHls(ViewLocatorHsiByte source)``

Convert to hls color space.

The method **ConvertToHls** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToHls*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 ConvertToHls(ViewLocatorHsiUInt16 source)``

Convert to hls color space.

The method **ConvertToHls** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToHls*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble ConvertToHls(ViewLocatorHsiDouble source)``

Convert to hls color space.

The method **ConvertToHls** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToHls*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte ConvertToHls(ViewLocatorLabByte source)``

Convert to hls color space.

The method **ConvertToHls** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToHls*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 ConvertToHls(ViewLocatorLabUInt16 source)``

Convert to hls color space.

The method **ConvertToHls** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToHls*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble ConvertToHls(ViewLocatorLabDouble source)``

Convert to hls color space.

The method **ConvertToHls** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToHls*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte ConvertToHls(ViewLocatorXyzByte source)``

Convert to hls color space.

The method **ConvertToHls** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToHls*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 ConvertToHls(ViewLocatorXyzUInt16 source)``

Convert to hls color space.

The method **ConvertToHls** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToHls*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble ConvertToHls(ViewLocatorXyzDouble source)``

Convert to hls color space.

The method **ConvertToHls** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToHls*
^^^^^^^^^^^^^^^^^^^^^

``System.Object ConvertToHls(View source)``

Convert to hls color space.

The method **ConvertToHls** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

Method *ConvertToHsi*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte ConvertToHsi(ViewLocatorRgbByte source)``

Convert to hsi color space.

The method **ConvertToHsi** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToHsi*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 ConvertToHsi(ViewLocatorRgbUInt16 source)``

Convert to hsi color space.

The method **ConvertToHsi** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToHsi*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble ConvertToHsi(ViewLocatorRgbDouble source)``

Convert to hsi color space.

The method **ConvertToHsi** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToHsi*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte ConvertToHsi(ViewLocatorHlsByte source)``

Convert to hsi color space.

The method **ConvertToHsi** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToHsi*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 ConvertToHsi(ViewLocatorHlsUInt16 source)``

Convert to hsi color space.

The method **ConvertToHsi** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToHsi*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble ConvertToHsi(ViewLocatorHlsDouble source)``

Convert to hsi color space.

The method **ConvertToHsi** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToHsi*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte ConvertToHsi(ViewLocatorHsiByte source)``

Convert to hsi color space.

The method **ConvertToHsi** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToHsi*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 ConvertToHsi(ViewLocatorHsiUInt16 source)``

Convert to hsi color space.

The method **ConvertToHsi** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToHsi*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble ConvertToHsi(ViewLocatorHsiDouble source)``

Convert to hsi color space.

The method **ConvertToHsi** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToHsi*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte ConvertToHsi(ViewLocatorLabByte source)``

Convert to hsi color space.

The method **ConvertToHsi** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToHsi*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 ConvertToHsi(ViewLocatorLabUInt16 source)``

Convert to hsi color space.

The method **ConvertToHsi** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToHsi*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble ConvertToHsi(ViewLocatorLabDouble source)``

Convert to hsi color space.

The method **ConvertToHsi** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToHsi*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte ConvertToHsi(ViewLocatorXyzByte source)``

Convert to hsi color space.

The method **ConvertToHsi** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToHsi*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 ConvertToHsi(ViewLocatorXyzUInt16 source)``

Convert to hsi color space.

The method **ConvertToHsi** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToHsi*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble ConvertToHsi(ViewLocatorXyzDouble source)``

Convert to hsi color space.

The method **ConvertToHsi** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToHsi*
^^^^^^^^^^^^^^^^^^^^^

``System.Object ConvertToHsi(View source)``

Convert to hsi color space.

The method **ConvertToHsi** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

Method *ConvertToLab*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte ConvertToLab(ViewLocatorRgbByte source)``

Convert to lab color space.

The method **ConvertToLab** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToLab*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 ConvertToLab(ViewLocatorRgbUInt16 source)``

Convert to lab color space.

The method **ConvertToLab** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToLab*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble ConvertToLab(ViewLocatorRgbDouble source)``

Convert to lab color space.

The method **ConvertToLab** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToLab*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte ConvertToLab(ViewLocatorHlsByte source)``

Convert to lab color space.

The method **ConvertToLab** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToLab*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 ConvertToLab(ViewLocatorHlsUInt16 source)``

Convert to lab color space.

The method **ConvertToLab** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToLab*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble ConvertToLab(ViewLocatorHlsDouble source)``

Convert to lab color space.

The method **ConvertToLab** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToLab*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte ConvertToLab(ViewLocatorHsiByte source)``

Convert to lab color space.

The method **ConvertToLab** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToLab*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 ConvertToLab(ViewLocatorHsiUInt16 source)``

Convert to lab color space.

The method **ConvertToLab** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToLab*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble ConvertToLab(ViewLocatorHsiDouble source)``

Convert to lab color space.

The method **ConvertToLab** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToLab*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte ConvertToLab(ViewLocatorLabByte source)``

Convert to lab color space.

The method **ConvertToLab** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToLab*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 ConvertToLab(ViewLocatorLabUInt16 source)``

Convert to lab color space.

The method **ConvertToLab** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToLab*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble ConvertToLab(ViewLocatorLabDouble source)``

Convert to lab color space.

The method **ConvertToLab** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToLab*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte ConvertToLab(ViewLocatorXyzByte source)``

Convert to lab color space.

The method **ConvertToLab** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToLab*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 ConvertToLab(ViewLocatorXyzUInt16 source)``

Convert to lab color space.

The method **ConvertToLab** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToLab*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble ConvertToLab(ViewLocatorXyzDouble source)``

Convert to lab color space.

The method **ConvertToLab** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToLab*
^^^^^^^^^^^^^^^^^^^^^

``System.Object ConvertToLab(View source)``

Convert to lab color space.

The method **ConvertToLab** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

Method *ConvertToXyz*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte ConvertToXyz(ViewLocatorRgbByte source)``

Convert to xyz color space.

The method **ConvertToXyz** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToXyz*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 ConvertToXyz(ViewLocatorRgbUInt16 source)``

Convert to xyz color space.

The method **ConvertToXyz** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToXyz*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble ConvertToXyz(ViewLocatorRgbDouble source)``

Convert to xyz color space.

The method **ConvertToXyz** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToXyz*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte ConvertToXyz(ViewLocatorHlsByte source)``

Convert to xyz color space.

The method **ConvertToXyz** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToXyz*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 ConvertToXyz(ViewLocatorHlsUInt16 source)``

Convert to xyz color space.

The method **ConvertToXyz** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToXyz*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble ConvertToXyz(ViewLocatorHlsDouble source)``

Convert to xyz color space.

The method **ConvertToXyz** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToXyz*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte ConvertToXyz(ViewLocatorHsiByte source)``

Convert to xyz color space.

The method **ConvertToXyz** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToXyz*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 ConvertToXyz(ViewLocatorHsiUInt16 source)``

Convert to xyz color space.

The method **ConvertToXyz** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToXyz*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble ConvertToXyz(ViewLocatorHsiDouble source)``

Convert to xyz color space.

The method **ConvertToXyz** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToXyz*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte ConvertToXyz(ViewLocatorLabByte source)``

Convert to xyz color space.

The method **ConvertToXyz** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToXyz*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 ConvertToXyz(ViewLocatorLabUInt16 source)``

Convert to xyz color space.

The method **ConvertToXyz** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToXyz*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble ConvertToXyz(ViewLocatorLabDouble source)``

Convert to xyz color space.

The method **ConvertToXyz** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToXyz*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte ConvertToXyz(ViewLocatorXyzByte source)``

Convert to xyz color space.

The method **ConvertToXyz** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

Method *ConvertToXyz*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 ConvertToXyz(ViewLocatorXyzUInt16 source)``

Convert to xyz color space.

The method **ConvertToXyz** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToXyz*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble ConvertToXyz(ViewLocatorXyzDouble source)``

Convert to xyz color space.

The method **ConvertToXyz** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

Method *ConvertToXyz*
^^^^^^^^^^^^^^^^^^^^^

``System.Object ConvertToXyz(View source)``

Convert to xyz color space.

The method **ConvertToXyz** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

Method *DemosaicBilinear*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte DemosaicBilinear(ViewLocatorByte source, System.Int32 cfaOffset)``

The method **DemosaicBilinear** has the following parameters:

+-----------------+-----------------------+---------------+
| Parameter       | Type                  | Description   |
+=================+=======================+===============+
| ``source``      | ``ViewLocatorByte``   |               |
+-----------------+-----------------------+---------------+
| ``cfaOffset``   | ``System.Int32``      |               |
+-----------------+-----------------------+---------------+

Method *DemosaicBilinear*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 DemosaicBilinear(ViewLocatorUInt16 source, System.Int32 cfaOffset)``

The method **DemosaicBilinear** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt16``   |               |
+-----------------+-------------------------+---------------+
| ``cfaOffset``   | ``System.Int32``        |               |
+-----------------+-------------------------+---------------+

Method *DemosaicBilinear*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 DemosaicBilinear(ViewLocatorUInt32 source, System.Int32 cfaOffset)``

The method **DemosaicBilinear** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt32``   |               |
+-----------------+-------------------------+---------------+
| ``cfaOffset``   | ``System.Int32``        |               |
+-----------------+-------------------------+---------------+

Method *DemosaicBilinear*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble DemosaicBilinear(ViewLocatorDouble source, System.Int32 cfaOffset)``

The method **DemosaicBilinear** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorDouble``   |               |
+-----------------+-------------------------+---------------+
| ``cfaOffset``   | ``System.Int32``        |               |
+-----------------+-------------------------+---------------+

Method *DemosaicBilinear*
^^^^^^^^^^^^^^^^^^^^^^^^^

``Image DemosaicBilinear(View source, System.Int32 cfaOffset)``

The method **DemosaicBilinear** has the following parameters:

+-----------------+--------------------+---------------+
| Parameter       | Type               | Description   |
+=================+====================+===============+
| ``source``      | ``View``           |               |
+-----------------+--------------------+---------------+
| ``cfaOffset``   | ``System.Int32``   |               |
+-----------------+--------------------+---------------+

Method *Quantize*
^^^^^^^^^^^^^^^^^

``ImageByte Quantize(ViewLocatorByte source, ViewLocatorRgbByte second)``

Quantize an image down to 256 or less colors.

The method **Quantize** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorByte``      |               |
+--------------+--------------------------+---------------+
| ``second``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

Method *Quantize*
^^^^^^^^^^^^^^^^^

``ImageByte Quantize(ViewLocatorUInt16 source, ViewLocatorRgbByte second)``

Quantize an image down to 256 or less colors.

The method **Quantize** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorUInt16``    |               |
+--------------+--------------------------+---------------+
| ``second``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

Method *Quantize*
^^^^^^^^^^^^^^^^^

``ImageByte Quantize(ViewLocatorUInt32 source, ViewLocatorRgbByte second)``

Quantize an image down to 256 or less colors.

The method **Quantize** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorUInt32``    |               |
+--------------+--------------------------+---------------+
| ``second``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

Method *Quantize*
^^^^^^^^^^^^^^^^^

``ImageByte Quantize(ViewLocatorDouble source, ViewLocatorRgbByte second)``

Quantize an image down to 256 or less colors.

The method **Quantize** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorDouble``    |               |
+--------------+--------------------------+---------------+
| ``second``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

Method *Quantize*
^^^^^^^^^^^^^^^^^

``ImageByte Quantize(ViewLocatorRgbByte source, ViewLocatorRgbByte second)``

Quantize an image down to 256 or less colors.

The method **Quantize** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``second``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

Method *Quantize*
^^^^^^^^^^^^^^^^^

``ImageByte Quantize(ViewLocatorRgbUInt16 source, ViewLocatorRgbByte second)``

Quantize an image down to 256 or less colors.

The method **Quantize** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``second``   | ``ViewLocatorRgbByte``     |               |
+--------------+----------------------------+---------------+

Method *Quantize*
^^^^^^^^^^^^^^^^^

``ImageByte Quantize(ViewLocatorRgbUInt32 source, ViewLocatorRgbByte second)``

Quantize an image down to 256 or less colors.

The method **Quantize** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``second``   | ``ViewLocatorRgbByte``     |               |
+--------------+----------------------------+---------------+

Method *Quantize*
^^^^^^^^^^^^^^^^^

``ImageByte Quantize(ViewLocatorRgbDouble source, ViewLocatorRgbByte second)``

Quantize an image down to 256 or less colors.

The method **Quantize** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``second``   | ``ViewLocatorRgbByte``     |               |
+--------------+----------------------------+---------------+

Method *Quantize*
^^^^^^^^^^^^^^^^^

``ImageByte Quantize(ViewLocatorRgbaByte source, ViewLocatorRgbByte second)``

Quantize an image down to 256 or less colors.

The method **Quantize** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``second``   | ``ViewLocatorRgbByte``    |               |
+--------------+---------------------------+---------------+

Method *Quantize*
^^^^^^^^^^^^^^^^^

``ImageByte Quantize(ViewLocatorRgbaUInt16 source, ViewLocatorRgbByte second)``

Quantize an image down to 256 or less colors.

The method **Quantize** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``second``   | ``ViewLocatorRgbByte``      |               |
+--------------+-----------------------------+---------------+

Method *Quantize*
^^^^^^^^^^^^^^^^^

``ImageByte Quantize(ViewLocatorRgbaUInt32 source, ViewLocatorRgbByte second)``

Quantize an image down to 256 or less colors.

The method **Quantize** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``second``   | ``ViewLocatorRgbByte``      |               |
+--------------+-----------------------------+---------------+

Method *Quantize*
^^^^^^^^^^^^^^^^^

``ImageByte Quantize(ViewLocatorRgbaDouble source, ViewLocatorRgbByte second)``

Quantize an image down to 256 or less colors.

The method **Quantize** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``second``   | ``ViewLocatorRgbByte``      |               |
+--------------+-----------------------------+---------------+

Method *Quantize*
^^^^^^^^^^^^^^^^^

``ImageByte Quantize(View source, ViewLocatorRgbByte second)``

Quantize an image down to 256 or less colors.

The method **Quantize** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``View``                 |               |
+--------------+--------------------------+---------------+
| ``second``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

Method *DominantColor*
^^^^^^^^^^^^^^^^^^^^^^

``RgbByte DominantColor(ViewLocatorByte source)``

Returns a dominant color of the image.

The method **DominantColor** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

Method *DominantColor*
^^^^^^^^^^^^^^^^^^^^^^

``RgbByte DominantColor(ViewLocatorUInt16 source)``

Returns a dominant color of the image.

The method **DominantColor** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

Method *DominantColor*
^^^^^^^^^^^^^^^^^^^^^^

``RgbByte DominantColor(ViewLocatorUInt32 source)``

Returns a dominant color of the image.

The method **DominantColor** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

Method *DominantColor*
^^^^^^^^^^^^^^^^^^^^^^

``RgbByte DominantColor(ViewLocatorDouble source)``

Returns a dominant color of the image.

The method **DominantColor** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

Method *DominantColor*
^^^^^^^^^^^^^^^^^^^^^^

``RgbByte DominantColor(ViewLocatorRgbByte source)``

Returns a dominant color of the image.

The method **DominantColor** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

Method *DominantColor*
^^^^^^^^^^^^^^^^^^^^^^

``RgbByte DominantColor(ViewLocatorRgbUInt16 source)``

Returns a dominant color of the image.

The method **DominantColor** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

Method *DominantColor*
^^^^^^^^^^^^^^^^^^^^^^

``RgbByte DominantColor(ViewLocatorRgbUInt32 source)``

Returns a dominant color of the image.

The method **DominantColor** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

Method *DominantColor*
^^^^^^^^^^^^^^^^^^^^^^

``RgbByte DominantColor(ViewLocatorRgbDouble source)``

Returns a dominant color of the image.

The method **DominantColor** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

Method *DominantColor*
^^^^^^^^^^^^^^^^^^^^^^

``RgbByte DominantColor(ViewLocatorRgbaByte source)``

Returns a dominant color of the image.

The method **DominantColor** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+

Method *DominantColor*
^^^^^^^^^^^^^^^^^^^^^^

``RgbByte DominantColor(ViewLocatorRgbaUInt16 source)``

Returns a dominant color of the image.

The method **DominantColor** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+

Method *DominantColor*
^^^^^^^^^^^^^^^^^^^^^^

``RgbByte DominantColor(ViewLocatorRgbaUInt32 source)``

Returns a dominant color of the image.

The method **DominantColor** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+

Method *DominantColor*
^^^^^^^^^^^^^^^^^^^^^^

``RgbByte DominantColor(ViewLocatorRgbaDouble source)``

Returns a dominant color of the image.

The method **DominantColor** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

Method *DominantColor*
^^^^^^^^^^^^^^^^^^^^^^

``RgbByte DominantColor(View source)``

Returns a dominant color of the image.

The method **DominantColor** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

Enumerations
~~~~~~~~~~~~

Enumeration *Channel*
^^^^^^^^^^^^^^^^^^^^^

``enum Channel``

TODO documentation missing

The enumeration **Channel** has the following constants:

+-------------------------+----------+---------------+
| Name                    | Value    | Description   |
+=========================+==========+===============+
| ``channelFirst``        | ``0``    |               |
+-------------------------+----------+---------------+
| ``channelSecond``       | ``1``    |               |
+-------------------------+----------+---------------+
| ``channelThird``        | ``2``    |               |
+-------------------------+----------+---------------+
| ``channelFourth``       | ``3``    |               |
+-------------------------+----------+---------------+
| ``channelRed``          | ``4``    |               |
+-------------------------+----------+---------------+
| ``channelGreen``        | ``5``    |               |
+-------------------------+----------+---------------+
| ``channelBlue``         | ``6``    |               |
+-------------------------+----------+---------------+
| ``channelHue``          | ``7``    |               |
+-------------------------+----------+---------------+
| ``channelLightness``    | ``8``    |               |
+-------------------------+----------+---------------+
| ``channelSaturation``   | ``9``    |               |
+-------------------------+----------+---------------+
| ``channelL``            | ``10``   |               |
+-------------------------+----------+---------------+
| ``channelA``            | ``11``   |               |
+-------------------------+----------+---------------+
| ``channelB``            | ``12``   |               |
+-------------------------+----------+---------------+
| ``channelAlpha``        | ``13``   |               |
+-------------------------+----------+---------------+
| ``channelIntensity``    | ``14``   |               |
+-------------------------+----------+---------------+
| ``channelX``            | ``15``   |               |
+-------------------------+----------+---------------+
| ``channelY``            | ``16``   |               |
+-------------------------+----------+---------------+
| ``channelZ``            | ``17``   |               |
+-------------------------+----------+---------------+

::

    enum Channel
    {
      channelFirst = 0,
      channelSecond = 1,
      channelThird = 2,
      channelFourth = 3,
      channelRed = 4,
      channelGreen = 5,
      channelBlue = 6,
      channelHue = 7,
      channelLightness = 8,
      channelSaturation = 9,
      channelL = 10,
      channelA = 11,
      channelB = 12,
      channelAlpha = 13,
      channelIntensity = 14,
      channelX = 15,
      channelY = 16,
      channelZ = 17,
    };

TODO documentation missing
