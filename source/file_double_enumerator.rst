Class *DoubleEnumerator*
------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **DoubleEnumerator** implements the following interfaces:

+-------------------------+
| Interface               |
+=========================+
| ``IEnumerator``         |
+-------------------------+
| ``IEnumeratorDouble``   |
+-------------------------+

The class **DoubleEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **DoubleEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``System.Double Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
