Class *FitResult*
-----------------

The result of a fit procedure.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **FitResult** implements the following interfaces:

+--------------------------------------------------------------------------+
| Interface                                                                |
+==========================================================================+
| ``IEquatableFitResultFitResultModelVariantFitResultCoordinateVariant``   |
+--------------------------------------------------------------------------+
| ``ISerializable``                                                        |
+--------------------------------------------------------------------------+

The class **FitResult** contains the following variant parameters:

+------------------+-----------------------------------------+
| Variant          | Description                             |
+==================+=========================================+
| ``Model``        | TODO no brief description for variant   |
+------------------+-----------------------------------------+
| ``Coordinate``   | TODO no brief description for variant   |
+------------------+-----------------------------------------+

The class **FitResult** contains the following properties:

+----------------+-------+-------+------------------------+
| Property       | Get   | Set   | Description            |
+================+=======+=======+========================+
| ``Model``      | \*    |       | The fitted model.      |
+----------------+-------+-------+------------------------+
| ``Inliers``    | \*    |       | The inliers points.    |
+----------------+-------+-------+------------------------+
| ``Outliers``   | \*    |       | The outliers points.   |
+----------------+-------+-------+------------------------+

The class **FitResult** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

The fit result consists of the fitted model, the inliers and the outliers points.

Variants
~~~~~~~~

Variant *Model*
^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Model** has the following types:

+------------------+
| Type             |
+==================+
| ``LineDouble``   |
+------------------+
| ``Circle``       |
+------------------+
| ``Ellipse``      |
+------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Variant *Coordinate*
^^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Coordinate** has the following types:

+--------------+
| Type         |
+==============+
| ``Int32``    |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *FitResult*
^^^^^^^^^^^^^^^^^^^^^^^

``FitResult()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *FitResult*
^^^^^^^^^^^^^^^^^^^^^^^

``FitResult(System.Object model, PointList inliers, PointList outliers)``

Construct a fit\_result from a model, a list of inliers points and a list of outliers points.

The constructor has the following parameters:

+----------------+---------------------+------------------------+
| Parameter      | Type                | Description            |
+================+=====================+========================+
| ``model``      | ``System.Object``   | The fitted model.      |
+----------------+---------------------+------------------------+
| ``inliers``    | ``PointList``       | The inliers points.    |
+----------------+---------------------+------------------------+
| ``outliers``   | ``PointList``       | The outliers points.   |
+----------------+---------------------+------------------------+

Properties
~~~~~~~~~~

Property *Model*
^^^^^^^^^^^^^^^^

``System.Object Model``

The fitted model.

Property *Inliers*
^^^^^^^^^^^^^^^^^^

``PointList Inliers``

The inliers points.

Property *Outliers*
^^^^^^^^^^^^^^^^^^^

``PointList Outliers``

The outliers points.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
