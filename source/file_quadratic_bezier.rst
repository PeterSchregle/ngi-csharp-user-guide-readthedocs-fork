Class *QuadraticBezier*
-----------------------

The following operations are implemented: operator== : comparison for equality.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **QuadraticBezier** implements the following interfaces:

+---------------------------------+
| Interface                       |
+=================================+
| ``IEquatableQuadraticBezier``   |
+---------------------------------+
| ``ISerializable``               |
+---------------------------------+
| ``INotifyPropertyChanged``      |
+---------------------------------+

The class **QuadraticBezier** contains the following properties:

+--------------------+-------+-------+----------------------------------------------+
| Property           | Get   | Set   | Description                                  |
+====================+=======+=======+==============================================+
| ``StartPoint``     | \*    | \*    | The start point of the quadratic bezier.     |
+--------------------+-------+-------+----------------------------------------------+
| ``ControlPoint``   | \*    | \*    | The control point of the quadratic bezier.   |
+--------------------+-------+-------+----------------------------------------------+
| ``EndPoint``       | \*    | \*    | The end point of the quadratic bezier.       |
+--------------------+-------+-------+----------------------------------------------+

The class **QuadraticBezier** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``Move``       | Move quadratic bezier.                                  |
+----------------+---------------------------------------------------------+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

operator != : comparison for inequality. operator- : negation, directly implemented. operator+ : addition, implemented through boost::additive operator-= : subtraction, directly implemented. operator- : subtraction, implemented through boost::additive

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *QuadraticBezier*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``QuadraticBezier()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *QuadraticBezier*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``QuadraticBezier(PointDouble startPoint, PointDouble controlPoint, PointDouble endPoint)``

Construct a quadratic\_bezier from three points.

The constructor has the following parameters:

+--------------------+-------------------+----------------------+
| Parameter          | Type              | Description          |
+====================+===================+======================+
| ``startPoint``     | ``PointDouble``   | The start point.     |
+--------------------+-------------------+----------------------+
| ``controlPoint``   | ``PointDouble``   | The control point.   |
+--------------------+-------------------+----------------------+
| ``endPoint``       | ``PointDouble``   | The end point.       |
+--------------------+-------------------+----------------------+

Properties
~~~~~~~~~~

Property *StartPoint*
^^^^^^^^^^^^^^^^^^^^^

``PointDouble StartPoint``

The start point of the quadratic bezier.

Property *ControlPoint*
^^^^^^^^^^^^^^^^^^^^^^^

``PointDouble ControlPoint``

The control point of the quadratic bezier.

Property *EndPoint*
^^^^^^^^^^^^^^^^^^^

``PointDouble EndPoint``

The end point of the quadratic bezier.

Methods
~~~~~~~

Method *Move*
^^^^^^^^^^^^^

``QuadraticBezier Move(VectorDouble movement)``

Move quadratic bezier.

The method **Move** has the following parameters:

+----------------+--------------------+--------------------+
| Parameter      | Type               | Description        |
+================+====================+====================+
| ``movement``   | ``VectorDouble``   | Movement vector.   |
+----------------+--------------------+--------------------+

Moves a quadratic bezier by a vector.

The moved quadratic bezier.

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO no brief description for variant

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO no description for variant
