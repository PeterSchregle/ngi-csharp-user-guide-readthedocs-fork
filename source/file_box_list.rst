Class *BoxList*
---------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **BoxList** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``IListBoxListBoxVariant``   |
+------------------------------+

The class **BoxList** contains the following variant parameters:

+-----------+-----------------------------------------+
| Variant   | Description                             |
+===========+=========================================+
| ``Box``   | TODO no brief description for variant   |
+-----------+-----------------------------------------+

The class **BoxList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **BoxList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Box*
^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Box** has the following types:

+-----------------+
| Type            |
+=================+
| ``BoxInt32``    |
+-----------------+
| ``BoxDouble``   |
+-----------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *BoxList*
^^^^^^^^^^^^^^^^^^^^^

``BoxList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``Box [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``BoxEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(Box item)``

The method **Add** has the following parameters:

+-------------+-----------+---------------+
| Parameter   | Type      | Description   |
+=============+===========+===============+
| ``item``    | ``Box``   |               |
+-------------+-----------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(Box item)``

The method **Contains** has the following parameters:

+-------------+-----------+---------------+
| Parameter   | Type      | Description   |
+=============+===========+===============+
| ``item``    | ``Box``   |               |
+-------------+-----------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(Box item)``

The method **Remove** has the following parameters:

+-------------+-----------+---------------+
| Parameter   | Type      | Description   |
+=============+===========+===============+
| ``item``    | ``Box``   |               |
+-------------+-----------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(Box item)``

The method **IndexOf** has the following parameters:

+-------------+-----------+---------------+
| Parameter   | Type      | Description   |
+=============+===========+===============+
| ``item``    | ``Box``   |               |
+-------------+-----------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, Box item)``

The method **Insert** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``item``    | ``Box``            |               |
+-------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
