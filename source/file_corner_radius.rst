Class *CornerRadius*
--------------------

The corner\_radius describes the four corner radii of a rounded rectangle.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **CornerRadius** implements the following interfaces:

+-----------------------------------------------------------+
| Interface                                                 |
+===========================================================+
| ``IEquatableCornerRadiusCornerRadiusCoordinateVariant``   |
+-----------------------------------------------------------+
| ``ISerializable``                                         |
+-----------------------------------------------------------+
| ``INotifyPropertyChanged``                                |
+-----------------------------------------------------------+

The class **CornerRadius** contains the following variant parameters:

+------------------+-----------------------------------------+
| Variant          | Description                             |
+==================+=========================================+
| ``Coordinate``   | TODO no brief description for variant   |
+------------------+-----------------------------------------+

The class **CornerRadius** contains the following properties:

+-------------------+-------+-------+-------------------------------------------------+
| Property          | Get   | Set   | Description                                     |
+===================+=======+=======+=================================================+
| ``TopLeft``       | \*    | \*    | The top\_left side of the corner\_radius.       |
+-------------------+-------+-------+-------------------------------------------------+
| ``TopRight``      | \*    | \*    | The top\_right side of the corner\_radius.      |
+-------------------+-------+-------+-------------------------------------------------+
| ``BottomRight``   | \*    | \*    | The bottom\_right side of the corner\_radius.   |
+-------------------+-------+-------+-------------------------------------------------+
| ``BottomLeft``    | \*    | \*    | The bottom\_left side of the corner\_radius.    |
+-------------------+-------+-------+-------------------------------------------------+

The class **CornerRadius** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

Four values describe the top\_left, top\_right, bottom\_right and bottom\_left corners of the rectangle, respectively.

Variants
~~~~~~~~

Variant *Coordinate*
^^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Coordinate** has the following types:

+--------------+
| Type         |
+==============+
| ``Int32``    |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *CornerRadius*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``CornerRadius()``

Constructs a corner\_radius with the same value for all corners.

If no parameter is passed, a zero corner\_radius is constructed.

If a parameter is passed, all four radii are set to this value.

Constructors
~~~~~~~~~~~~

Constructor *CornerRadius*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``CornerRadius(System.Object all)``

Constructs a corner\_radius with different values for all corners.

The constructor has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``all``     | ``System.Object``   |               |
+-------------+---------------------+---------------+

All four radii are set to specific values.

Constructor *CornerRadius*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``CornerRadius(System.Object topLeft, System.Object topRight, System.Object bottomRight, System.Object bottomLeft)``

Copy construct another corner\_radius.

The constructor has the following parameters:

+-------------------+---------------------+---------------+
| Parameter         | Type                | Description   |
+===================+=====================+===============+
| ``topLeft``       | ``System.Object``   |               |
+-------------------+---------------------+---------------+
| ``topRight``      | ``System.Object``   |               |
+-------------------+---------------------+---------------+
| ``bottomRight``   | ``System.Object``   |               |
+-------------------+---------------------+---------------+
| ``bottomLeft``    | ``System.Object``   |               |
+-------------------+---------------------+---------------+

Properties
~~~~~~~~~~

Property *TopLeft*
^^^^^^^^^^^^^^^^^^

``System.Object TopLeft``

The top\_left side of the corner\_radius.

Property *TopRight*
^^^^^^^^^^^^^^^^^^^

``System.Object TopRight``

The top\_right side of the corner\_radius.

Property *BottomRight*
^^^^^^^^^^^^^^^^^^^^^^

``System.Object BottomRight``

The bottom\_right side of the corner\_radius.

Property *BottomLeft*
^^^^^^^^^^^^^^^^^^^^^

``System.Object BottomLeft``

The bottom\_left side of the corner\_radius.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO documentation missing

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO documentation missing
