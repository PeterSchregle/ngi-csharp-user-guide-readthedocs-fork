Class *PolylineEnumerator*
--------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **PolylineEnumerator** implements the following interfaces:

+----------------------------------------------------+
| Interface                                          |
+====================================================+
| ``IEnumerator``                                    |
+----------------------------------------------------+
| ``IEnumeratorPolylineEnumeratorPolylineVariant``   |
+----------------------------------------------------+

The class **PolylineEnumerator** contains the following variant parameters:

+----------------+-----------------------------------------+
| Variant        | Description                             |
+================+=========================================+
| ``Polyline``   | TODO no brief description for variant   |
+----------------+-----------------------------------------+

The class **PolylineEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **PolylineEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Polyline*
^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Polyline** has the following types:

+----------------------+
| Type                 |
+======================+
| ``PolylineInt32``    |
+----------------------+
| ``PolylineDouble``   |
+----------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Polyline Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
