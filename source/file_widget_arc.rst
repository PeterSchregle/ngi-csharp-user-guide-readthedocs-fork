Class *WidgetArc*
-----------------

A widget\_arc displays an arc on a surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetArc** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetArc** contains the following properties:

+----------------+-------+-------+-------------------------------+
| Property       | Get   | Set   | Description                   |
+================+=======+=======+===============================+
| ``Position``   | \*    | \*    | The position of the widget.   |
+----------------+-------+-------+-------------------------------+
| ``Pen``        | \*    | \*    | The pen of the widget.        |
+----------------+-------+-------+-------------------------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_arc. These settings affect the look and the behavior of the widget\_arc.

The widget\_arc supports interactivity via the widget\_interactive base class, i.e. it can be selected, move around. In general it can react on mouse and keyboard interactions. This widget supports layout.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetArc*
^^^^^^^^^^^^^^^^^^^^^^^

``WidgetArc()``

Default constructor.

The default constructor creates a widget with default settings.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``Arc Position``

The position of the widget.

Property *Pen*
^^^^^^^^^^^^^^

``PenByte Pen``

The pen of the widget.
