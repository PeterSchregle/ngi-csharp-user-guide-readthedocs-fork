Class *GeometryCubicBezier*
---------------------------

The following operations are implemented: operator== : comparison for equality.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **GeometryCubicBezier** implements the following interfaces:

+------------------+
| Interface        |
+==================+
| ``IEquatable``   |
+------------------+

The class **GeometryCubicBezier** contains the following properties:

+---------------------+-------+-------+-----------------------------------------------------+
| Property            | Get   | Set   | Description                                         |
+=====================+=======+=======+=====================================================+
| ``ControlPoint1``   | \*    | \*    | The first control point of the quadratic bezier.    |
+---------------------+-------+-------+-----------------------------------------------------+
| ``ControlPoint2``   | \*    | \*    | The second control point of the quadratic bezier.   |
+---------------------+-------+-------+-----------------------------------------------------+

The class **GeometryCubicBezier** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

operator!= : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *GeometryCubicBezier*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``GeometryCubicBezier()``

Construct a quadratic bezier.

Constructors
~~~~~~~~~~~~

Constructor *GeometryCubicBezier*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``GeometryCubicBezier(PointDouble controlPoint1, PointDouble controlPoint2, PointDouble endPoint)``

Construct a quadratic bezier.

The constructor has the following parameters:

+---------------------+-------------------+-----------------------------+
| Parameter           | Type              | Description                 |
+=====================+===================+=============================+
| ``controlPoint1``   | ``PointDouble``   | The first control point.    |
+---------------------+-------------------+-----------------------------+
| ``controlPoint2``   | ``PointDouble``   | The second control point.   |
+---------------------+-------------------+-----------------------------+
| ``endPoint``        | ``PointDouble``   | The end point.              |
+---------------------+-------------------+-----------------------------+

Properties
~~~~~~~~~~

Property *ControlPoint1*
^^^^^^^^^^^^^^^^^^^^^^^^

``PointDouble ControlPoint1``

The first control point of the quadratic bezier.

Property *ControlPoint2*
^^^^^^^^^^^^^^^^^^^^^^^^

``PointDouble ControlPoint2``

The second control point of the quadratic bezier.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
