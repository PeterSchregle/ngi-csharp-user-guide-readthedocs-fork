Class *ExtentEnumerator*
------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **ExtentEnumerator** implements the following interfaces:

+-------------------------+
| Interface               |
+=========================+
| ``IEnumerator``         |
+-------------------------+
| ``IEnumeratorExtent``   |
+-------------------------+

The class **ExtentEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **ExtentEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Extent Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
