Class *CameraParameterIenumeration*
-----------------------------------

**Namespace:** Ngi

**Module:** Camera

The class **CameraParameterIenumeration** contains the following properties:

+-----------------+-------+-------+---------------+
| Property        | Get   | Set   | Description   |
+=================+=======+=======+===============+
| ``Value``       | \*    | \*    |               |
+-----------------+-------+-------+---------------+
| ``Symbolics``   | \*    |       |               |
+-----------------+-------+-------+---------------+
| ``IntValue``    | \*    | \*    |               |
+-----------------+-------+-------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Value*
^^^^^^^^^^^^^^^^

``System.String Value``

Property *Symbolics*
^^^^^^^^^^^^^^^^^^^^

``StringList Symbolics``

Property *IntValue*
^^^^^^^^^^^^^^^^^^^

``System.Int64 IntValue``
