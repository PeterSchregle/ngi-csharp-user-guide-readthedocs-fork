Class *CubicBezier*
-------------------

The following operations are implemented: operator== : comparison for equality.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **CubicBezier** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``IEquatableCubicBezier``    |
+------------------------------+
| ``ISerializable``            |
+------------------------------+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **CubicBezier** contains the following properties:

+---------------------+-------+-------+-------------------------------------------------+
| Property            | Get   | Set   | Description                                     |
+=====================+=======+=======+=================================================+
| ``StartPoint``      | \*    | \*    | The start point of the cubic bezier.            |
+---------------------+-------+-------+-------------------------------------------------+
| ``ControlPoint1``   | \*    | \*    | The first control point of the cubic bezier.    |
+---------------------+-------+-------+-------------------------------------------------+
| ``ControlPoint2``   | \*    | \*    | The second control point of the cubic bezier.   |
+---------------------+-------+-------+-------------------------------------------------+
| ``EndPoint``        | \*    | \*    | The end point of the cubic bezier.              |
+---------------------+-------+-------+-------------------------------------------------+

The class **CubicBezier** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``Move``       | Move cubic bezier.                                      |
+----------------+---------------------------------------------------------+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

operator != : comparison for inequality. operator- : negation, directly implemented. operator+ : addition, implemented through boost::additive operator-= : subtraction, directly implemented. operator- : subtraction, implemented through boost::additive

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *CubicBezier*
^^^^^^^^^^^^^^^^^^^^^^^^^

``CubicBezier()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *CubicBezier*
^^^^^^^^^^^^^^^^^^^^^^^^^

``CubicBezier(PointDouble startPoint, PointDouble controlPoint1, PointDouble controlPoint2, PointDouble endPoint)``

Construct a cubic\_bezier from three points.

The constructor has the following parameters:

+---------------------+-------------------+-----------------------------+
| Parameter           | Type              | Description                 |
+=====================+===================+=============================+
| ``startPoint``      | ``PointDouble``   | The start point.            |
+---------------------+-------------------+-----------------------------+
| ``controlPoint1``   | ``PointDouble``   | The first control point.    |
+---------------------+-------------------+-----------------------------+
| ``controlPoint2``   | ``PointDouble``   | The second control point.   |
+---------------------+-------------------+-----------------------------+
| ``endPoint``        | ``PointDouble``   | The end point.              |
+---------------------+-------------------+-----------------------------+

Properties
~~~~~~~~~~

Property *StartPoint*
^^^^^^^^^^^^^^^^^^^^^

``PointDouble StartPoint``

The start point of the cubic bezier.

Property *ControlPoint1*
^^^^^^^^^^^^^^^^^^^^^^^^

``PointDouble ControlPoint1``

The first control point of the cubic bezier.

Property *ControlPoint2*
^^^^^^^^^^^^^^^^^^^^^^^^

``PointDouble ControlPoint2``

The second control point of the cubic bezier.

Property *EndPoint*
^^^^^^^^^^^^^^^^^^^

``PointDouble EndPoint``

The end point of the cubic bezier.

Methods
~~~~~~~

Method *Move*
^^^^^^^^^^^^^

``CubicBezier Move(VectorDouble movement)``

Move cubic bezier.

The method **Move** has the following parameters:

+----------------+--------------------+--------------------+
| Parameter      | Type               | Description        |
+================+====================+====================+
| ``movement``   | ``VectorDouble``   | Movement vector.   |
+----------------+--------------------+--------------------+

Moves a cubic bezier by a vector.

The moved cubic bezier.

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO no brief description for variant

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO no description for variant
