Class *LineList*
----------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **LineList** implements the following interfaces:

+--------------------------------+
| Interface                      |
+================================+
| ``IListLineListLineVariant``   |
+--------------------------------+

The class **LineList** contains the following variant parameters:

+------------+-----------------------------------------+
| Variant    | Description                             |
+============+=========================================+
| ``Line``   | TODO no brief description for variant   |
+------------+-----------------------------------------+

The class **LineList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **LineList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Line*
^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Line** has the following types:

+------------------+
| Type             |
+==================+
| ``LineInt32``    |
+------------------+
| ``LineDouble``   |
+------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *LineList*
^^^^^^^^^^^^^^^^^^^^^^

``LineList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``Line [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``LineEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(Line item)``

The method **Add** has the following parameters:

+-------------+------------+---------------+
| Parameter   | Type       | Description   |
+=============+============+===============+
| ``item``    | ``Line``   |               |
+-------------+------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(Line item)``

The method **Contains** has the following parameters:

+-------------+------------+---------------+
| Parameter   | Type       | Description   |
+=============+============+===============+
| ``item``    | ``Line``   |               |
+-------------+------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(Line item)``

The method **Remove** has the following parameters:

+-------------+------------+---------------+
| Parameter   | Type       | Description   |
+=============+============+===============+
| ``item``    | ``Line``   |               |
+-------------+------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(Line item)``

The method **IndexOf** has the following parameters:

+-------------+------------+---------------+
| Parameter   | Type       | Description   |
+=============+============+===============+
| ``item``    | ``Line``   |               |
+-------------+------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, Line item)``

The method **Insert** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``item``    | ``Line``           |               |
+-------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
