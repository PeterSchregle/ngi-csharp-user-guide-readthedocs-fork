Class *ImagePyramid*
--------------------

...

**Namespace:** Ngi

**Module:** ImageProcessing

The class **ImagePyramid** contains the following variant parameters:

+-------------+-----------------------------------------+
| Variant     | Description                             |
+=============+=========================================+
| ``Image``   | TODO no brief description for variant   |
+-------------+-----------------------------------------+

The class **ImagePyramid** contains the following properties:

+-----------------------+-------+-------+------------------------------------------+
| Property              | Get   | Set   | Description                              |
+=======================+=======+=======+==========================================+
| ``DownscaleFactor``   | \*    |       | The factor for successive downscaling.   |
+-----------------------+-------+-------+------------------------------------------+

The class **ImagePyramid** contains the following methods:

+----------------------+-----------------------------------------------------+
| Method               | Description                                         |
+======================+=====================================================+
| ``AllocateLevels``   | Allocates the pyramid levels.                       |
+----------------------+-----------------------------------------------------+
| ``Downscale``        | Calculates the pyramid by successive downscaling.   |
+----------------------+-----------------------------------------------------+
| ``ToString``         |                                                     |
+----------------------+-----------------------------------------------------+

The class **ImagePyramid** contains the following enumerations:

+-----------------------+------------------------------+
| Enumeration           | Description                  |
+=======================+==============================+
| ``GeometricFilter``   | TODO documentation missing   |
+-----------------------+------------------------------+

Description
~~~~~~~~~~~

The base class.

Variants
~~~~~~~~

Variant *Image*
^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Image** has the following types:

+-----------------------+
| Type                  |
+=======================+
| ``ImageByte``         |
+-----------------------+
| ``ImageUInt16``       |
+-----------------------+
| ``ImageUInt32``       |
+-----------------------+
| ``ImageDouble``       |
+-----------------------+
| ``ImageRgbByte``      |
+-----------------------+
| ``ImageRgbUInt16``    |
+-----------------------+
| ``ImageRgbUInt32``    |
+-----------------------+
| ``ImageRgbDouble``    |
+-----------------------+
| ``ImageRgbaByte``     |
+-----------------------+
| ``ImageRgbaUInt16``   |
+-----------------------+
| ``ImageRgbaUInt32``   |
+-----------------------+
| ``ImageRgbaDouble``   |
+-----------------------+
| ``ImageHlsByte``      |
+-----------------------+
| ``ImageHlsUInt16``    |
+-----------------------+
| ``ImageHlsDouble``    |
+-----------------------+
| ``ImageHsiByte``      |
+-----------------------+
| ``ImageHsiUInt16``    |
+-----------------------+
| ``ImageHsiDouble``    |
+-----------------------+
| ``ImageLabByte``      |
+-----------------------+
| ``ImageLabUInt16``    |
+-----------------------+
| ``ImageLabDouble``    |
+-----------------------+
| ``ImageXyzByte``      |
+-----------------------+
| ``ImageXyzUInt16``    |
+-----------------------+
| ``ImageXyzDouble``    |
+-----------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *ImagePyramid*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImagePyramid()``

Constructs an empty image\_pyramid.

Constructors
~~~~~~~~~~~~

Constructor *ImagePyramid*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImagePyramid(Image source)``

Constructs an image\_pyramid from an image.

The constructor has the following parameters:

+--------------+-------------+-------------------+
| Parameter    | Type        | Description       |
+==============+=============+===================+
| ``source``   | ``Image``   | Original image.   |
+--------------+-------------+-------------------+

Copies only the original image into the pyramid. It does not allocate the additional levels. In order to do so, you need to call allocate\_levels(). It does also not calculate the downscaled images. In order to do so, you need to call downscale().

Constructor *ImagePyramid*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImagePyramid(Image source, System.Double downscaleFactor, System.Int32 levels)``

Constructs an image\_pyramid from an image.

The constructor has the following parameters:

+--------------+------------+----------------------------------------------------+
| Parameter    | Type       | Description                                        |
+==============+============+====================================================+
| ``source``   | ``Image``  | Original image.                                    |
+--------------+------------+----------------------------------------------------+
| ``downscaleF | ``System.D | The factor for successive downscaling. This factor |
| actor``      | ouble``    | must be between 0 and 1 (both excluded). Usually   |
|              |            | it is set to 0.5.                                  |
+--------------+------------+----------------------------------------------------+
| ``levels``   | ``System.I | The number of levels of the pyramid. This is the   |
|              | nt32``     | count of images in the pyramid, i.e. the number of |
|              |            | successively downscaled images plus 1 (the         |
|              |            | original image).                                   |
+--------------+------------+----------------------------------------------------+

Copies the original image into the pyramid and allocates the additional levels. It does not calculate the downscaled images. In order to do so, you need to call downscale().

Constructor *ImagePyramid*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImagePyramid(Image source, System.Double downscaleFactor, System.Int32 levels, ImagePyramid.GeometricFilter filter)``

Constructs an image\_pyramid from an image.

The constructor has the following parameters:

+--------------+------------+----------------------------------------------------+
| Parameter    | Type       | Description                                        |
+==============+============+====================================================+
| ``source``   | ``Image``  | Original image.                                    |
+--------------+------------+----------------------------------------------------+
| ``downscaleF | ``System.D | The factor for successive downscaling. This factor |
| actor``      | ouble``    | must be between 0 and 1 (both excluded). Usually   |
|              |            | it is set to 0.5.                                  |
+--------------+------------+----------------------------------------------------+
| ``levels``   | ``System.I | The number of levels of the pyramid. This is the   |
|              | nt32``     | count of images in the pyramid, i.e. the number of |
|              |            | successively downscaled images plus 1 (the         |
|              |            | original image).                                   |
+--------------+------------+----------------------------------------------------+
| ``filter``   | ``ImagePyr | The geometric filter function.                     |
|              | amid.Geome |                                                    |
|              | tricFilter |                                                    |
|              | ``         |                                                    |
+--------------+------------+----------------------------------------------------+

Copies the original image into the pyramid and allocates the additional levels. It also calculates the downscaled images.

Properties
~~~~~~~~~~

Property *DownscaleFactor*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double DownscaleFactor``

The factor for successive downscaling.

This factor must be between 0 and 1 (both excluded). Usually it is set to 0.5.

Methods
~~~~~~~

Method *AllocateLevels*
^^^^^^^^^^^^^^^^^^^^^^^

``void AllocateLevels(System.Int32 levels)``

Allocates the pyramid levels.

The method **AllocateLevels** has the following parameters:

+--------------+------------+----------------------------------------------------+
| Parameter    | Type       | Description                                        |
+==============+============+====================================================+
| ``levels``   | ``System.I | The number of levels of the pyramid. This is the   |
|              | nt32``     | count of images in the pyramid, i.e. the number of |
|              |            | successively downscaled images plus 1 (the         |
|              |            | original image).                                   |
+--------------+------------+----------------------------------------------------+

Method *Downscale*
^^^^^^^^^^^^^^^^^^

``void Downscale(ImagePyramid.GeometricFilter filter)``

Calculates the pyramid by successive downscaling.

The method **Downscale** has the following parameters:

+--------------+------------------------------------+----------------------------------+
| Parameter    | Type                               | Description                      |
+==============+====================================+==================================+
| ``filter``   | ``ImagePyramid.GeometricFilter``   | The geometric filter function.   |
+--------------+------------------------------------+----------------------------------+

The function assumes that the necessary number of levels have been already allocated.

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Enumerations
~~~~~~~~~~~~

Enumeration *GeometricFilter*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum GeometricFilter``

TODO documentation missing

The enumeration **GeometricFilter** has the following constants:

+-------------------------+---------+---------------+
| Name                    | Value   | Description   |
+=========================+=========+===============+
| ``gfNearestNeighbor``   | ``0``   |               |
+-------------------------+---------+---------------+
| ``gfBoxFilter``         | ``1``   |               |
+-------------------------+---------+---------------+
| ``gfTriangleFilter``    | ``2``   |               |
+-------------------------+---------+---------------+
| ``gfCubicFilter``       | ``3``   |               |
+-------------------------+---------+---------------+
| ``gfBsplineFilter``     | ``4``   |               |
+-------------------------+---------+---------------+
| ``gfSincFilter``        | ``5``   |               |
+-------------------------+---------+---------------+
| ``gfLanczosFilter``     | ``6``   |               |
+-------------------------+---------+---------------+
| ``gfKaiserFilter``      | ``7``   |               |
+-------------------------+---------+---------------+

::

    enum GeometricFilter
    {
      gfNearestNeighbor = 0,
      gfBoxFilter = 1,
      gfTriangleFilter = 2,
      gfCubicFilter = 3,
      gfBsplineFilter = 4,
      gfSincFilter = 5,
      gfLanczosFilter = 6,
      gfKaiserFilter = 7,
    };

TODO documentation missing
