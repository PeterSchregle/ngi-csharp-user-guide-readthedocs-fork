Class *SimulationDeviceException*
---------------------------------

The simulation camera specific device\_exception type.

**Namespace:** Ngi

**Module:**

The class **SimulationDeviceException** contains the following enumerations:

+--------------------+------------------------------+
| Enumeration        | Description                  |
+====================+==============================+
| ``ExceptionIds``   | TODO documentation missing   |
+--------------------+------------------------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *SimulationDeviceException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``SimulationDeviceException()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *SimulationDeviceException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``SimulationDeviceException(System.String message)``

Construct an exception from a message.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+

Constructor *SimulationDeviceException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``SimulationDeviceException(System.String message, System.UInt32 id)``

Construct an exception from a message and an id.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+
| ``id``        | ``System.UInt32``   | The exception id.        |
+---------------+---------------------+--------------------------+

Enumerations
~~~~~~~~~~~~

Enumeration *ExceptionIds*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum ExceptionIds``

TODO documentation missing

The enumeration **ExceptionIds** has the following constants:

+----------------------+---------+---------------+
| Name                 | Value   | Description   |
+======================+=========+===============+
| ``notAnError``       | ``0``   |               |
+----------------------+---------+---------------+
| ``imageListEmpty``   | ``1``   |               |
+----------------------+---------+---------------+

::

    enum ExceptionIds
    {
      notAnError = 0,
      imageListEmpty = 1,
    };

TODO documentation missing
