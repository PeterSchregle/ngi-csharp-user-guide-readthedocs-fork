Class *LineSegmentEnumerator*
-----------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **LineSegmentEnumerator** implements the following interfaces:

+----------------------------------------------------------+
| Interface                                                |
+==========================================================+
| ``IEnumerator``                                          |
+----------------------------------------------------------+
| ``IEnumeratorLineSegmentEnumeratorLineSegmentVariant``   |
+----------------------------------------------------------+

The class **LineSegmentEnumerator** contains the following variant parameters:

+-------------------+-----------------------------------------+
| Variant           | Description                             |
+===================+=========================================+
| ``LineSegment``   | TODO no brief description for variant   |
+-------------------+-----------------------------------------+

The class **LineSegmentEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **LineSegmentEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *LineSegment*
^^^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **LineSegment** has the following types:

+-------------------------+
| Type                    |
+=========================+
| ``LineSegmentInt32``    |
+-------------------------+
| ``LineSegmentDouble``   |
+-------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``LineSegment Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
