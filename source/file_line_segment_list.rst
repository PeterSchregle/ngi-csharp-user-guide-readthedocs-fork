Class *LineSegmentList*
-----------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **LineSegmentList** implements the following interfaces:

+----------------------------------------------+
| Interface                                    |
+==============================================+
| ``IListLineSegmentListLineSegmentVariant``   |
+----------------------------------------------+

The class **LineSegmentList** contains the following variant parameters:

+-------------------+-----------------------------------------+
| Variant           | Description                             |
+===================+=========================================+
| ``LineSegment``   | TODO no brief description for variant   |
+-------------------+-----------------------------------------+

The class **LineSegmentList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **LineSegmentList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *LineSegment*
^^^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **LineSegment** has the following types:

+-------------------------+
| Type                    |
+=========================+
| ``LineSegmentInt32``    |
+-------------------------+
| ``LineSegmentDouble``   |
+-------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *LineSegmentList*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``LineSegmentList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``LineSegment [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``LineSegmentEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(LineSegment item)``

The method **Add** has the following parameters:

+-------------+-------------------+---------------+
| Parameter   | Type              | Description   |
+=============+===================+===============+
| ``item``    | ``LineSegment``   |               |
+-------------+-------------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(LineSegment item)``

The method **Contains** has the following parameters:

+-------------+-------------------+---------------+
| Parameter   | Type              | Description   |
+=============+===================+===============+
| ``item``    | ``LineSegment``   |               |
+-------------+-------------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(LineSegment item)``

The method **Remove** has the following parameters:

+-------------+-------------------+---------------+
| Parameter   | Type              | Description   |
+=============+===================+===============+
| ``item``    | ``LineSegment``   |               |
+-------------+-------------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(LineSegment item)``

The method **IndexOf** has the following parameters:

+-------------+-------------------+---------------+
| Parameter   | Type              | Description   |
+=============+===================+===============+
| ``item``    | ``LineSegment``   |               |
+-------------+-------------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, LineSegment item)``

The method **Insert** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``item``    | ``LineSegment``    |               |
+-------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
