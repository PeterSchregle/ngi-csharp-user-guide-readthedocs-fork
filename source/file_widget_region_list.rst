Class *WidgetRegionList*
------------------------

A widget\_region\_list displays a list of regions on a surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetRegionList** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetRegionList** contains the following properties:

+---------------+-------+-------+-----------------------------+
| Property      | Get   | Set   | Description                 |
+===============+=======+=======+=============================+
| ``Regions``   | \*    | \*    | The region of the widget.   |
+---------------+-------+-------+-----------------------------+
| ``Colors``    | \*    | \*    | The colors of the widget.   |
+---------------+-------+-------+-----------------------------+
| ``Alpha``     | \*    | \*    | The colors of the widget.   |
+---------------+-------+-------+-----------------------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_region\_list. These settings affect the look and the behavior of the widget\_region\_list. This widget displays a view.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetRegionList*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetRegionList()``

Default constructor.

The default constructor creates a widget with default settings. No regions, no colors, fully opaque.

Properties
~~~~~~~~~~

Property *Regions*
^^^^^^^^^^^^^^^^^^

``RegionList Regions``

The region of the widget.

Property *Colors*
^^^^^^^^^^^^^^^^^

``ColorListRgbaByte Colors``

The colors of the widget.

The fill of the widget\_region\_list is specified with a list of colors.

Property *Alpha*
^^^^^^^^^^^^^^^^

``System.Double Alpha``

The colors of the widget.

The fill of the widget\_region\_list is specified with a list of colors.
