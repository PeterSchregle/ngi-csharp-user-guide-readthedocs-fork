Class *EllipticalRingList*
--------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **EllipticalRingList** implements the following interfaces:

+---------------------------+
| Interface                 |
+===========================+
| ``IListEllipticalRing``   |
+---------------------------+

The class **EllipticalRingList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **EllipticalRingList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *EllipticalRingList*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``EllipticalRingList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``EllipticalRing [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``EllipticalRingEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(EllipticalRing item)``

The method **Add** has the following parameters:

+-------------+----------------------+---------------+
| Parameter   | Type                 | Description   |
+=============+======================+===============+
| ``item``    | ``EllipticalRing``   |               |
+-------------+----------------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(EllipticalRing item)``

The method **Contains** has the following parameters:

+-------------+----------------------+---------------+
| Parameter   | Type                 | Description   |
+=============+======================+===============+
| ``item``    | ``EllipticalRing``   |               |
+-------------+----------------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(EllipticalRing item)``

The method **Remove** has the following parameters:

+-------------+----------------------+---------------+
| Parameter   | Type                 | Description   |
+=============+======================+===============+
| ``item``    | ``EllipticalRing``   |               |
+-------------+----------------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(EllipticalRing item)``

The method **IndexOf** has the following parameters:

+-------------+----------------------+---------------+
| Parameter   | Type                 | Description   |
+=============+======================+===============+
| ``item``    | ``EllipticalRing``   |               |
+-------------+----------------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, EllipticalRing item)``

The method **Insert** has the following parameters:

+-------------+----------------------+---------------+
| Parameter   | Type                 | Description   |
+=============+======================+===============+
| ``index``   | ``System.Int32``     |               |
+-------------+----------------------+---------------+
| ``item``    | ``EllipticalRing``   |               |
+-------------+----------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
