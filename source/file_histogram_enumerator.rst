Class *HistogramEnumerator*
---------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **HistogramEnumerator** implements the following interfaces:

+------------------------------------------------------+
| Interface                                            |
+======================================================+
| ``IEnumerator``                                      |
+------------------------------------------------------+
| ``IEnumeratorHistogramEnumeratorHistogramVariant``   |
+------------------------------------------------------+

The class **HistogramEnumerator** contains the following variant parameters:

+-----------------+-----------------------------------------+
| Variant         | Description                             |
+=================+=========================================+
| ``Histogram``   | TODO no brief description for variant   |
+-----------------+-----------------------------------------+

The class **HistogramEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **HistogramEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Histogram*
^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Histogram** has the following types:

+---------------------------+
| Type                      |
+===========================+
| ``HistogramUInt32``       |
+---------------------------+
| ``HistogramDouble``       |
+---------------------------+
| ``HistogramRgbUInt32``    |
+---------------------------+
| ``HistogramRgbDouble``    |
+---------------------------+
| ``HistogramRgbaUInt32``   |
+---------------------------+
| ``HistogramRgbaDouble``   |
+---------------------------+
| ``HistogramHsiDouble``    |
+---------------------------+
| ``HistogramHlsDouble``    |
+---------------------------+
| ``HistogramLabDouble``    |
+---------------------------+
| ``HistogramXyzDouble``    |
+---------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Histogram Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
