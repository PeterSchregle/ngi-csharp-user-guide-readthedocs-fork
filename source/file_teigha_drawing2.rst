Class *TeighaDrawing2*
----------------------

A class holding a drawing parsed from a dxf file.

**Namespace:** Ngi

**Module:** CadPdf

The class **TeighaDrawing2** implements the following interfaces:

+--------------------------------+
| Interface                      |
+================================+
| ``IEquatableTeighaDrawing2``   |
+--------------------------------+

The class **TeighaDrawing2** contains the following properties:

+--------------+-------+-------+----------------------------------------+
| Property     | Get   | Set   | Description                            |
+==============+=======+=======+========================================+
| ``Bounds``   | \*    | \*    | The outer dimensions of the drawing.   |
+--------------+-------+-------+----------------------------------------+
| ``Layers``   | \*    | \*    | The list of contours.                  |
+--------------+-------+-------+----------------------------------------+

The class **TeighaDrawing2** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

A drawing consists of a box specifying its dimensions, and of a list of layers.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *TeighaDrawing2*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TeighaDrawing2()``

Constructs an empty teigha\_drawing2.

Properties
~~~~~~~~~~

Property *Bounds*
^^^^^^^^^^^^^^^^^

``BoxDouble Bounds``

The outer dimensions of the drawing.

Property *Layers*
^^^^^^^^^^^^^^^^^

``TeighaLayerList Layers``

The list of contours.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
