Class *SectorList*
------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **SectorList** implements the following interfaces:

+-------------------+
| Interface         |
+===================+
| ``IListSector``   |
+-------------------+

The class **SectorList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **SectorList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *SectorList*
^^^^^^^^^^^^^^^^^^^^^^^^

``SectorList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``Sector [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``SectorEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(Sector item)``

The method **Add** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``item``    | ``Sector``   |               |
+-------------+--------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(Sector item)``

The method **Contains** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``item``    | ``Sector``   |               |
+-------------+--------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(Sector item)``

The method **Remove** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``item``    | ``Sector``   |               |
+-------------+--------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(Sector item)``

The method **IndexOf** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``item``    | ``Sector``   |               |
+-------------+--------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, Sector item)``

The method **Insert** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``item``    | ``Sector``         |               |
+-------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
