Class *License*
---------------

Licensing functions.

**Namespace:** Ngi

**Module:**

Description
~~~~~~~~~~~

The class contains a group of functions that retrieve the licensing status.

Static Methods
~~~~~~~~~~~~~~

Method *IsDongleConnected*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsDongleConnected()``

Check if a proper dongle is connected to the computer.

The dongle must be connected to an USB port.

Method *GetHardwareId*
^^^^^^^^^^^^^^^^^^^^^^

``System.String GetHardwareId()``

Returns a string that uniquely identifies the computer on which the method is called.

Software licensing is used when no dongle is connected. In this case the hardware id encodes the following information: amount of physical RAM installed, processor id, MAC addresses and disk serial numbers.

If a dongle is connected, the hardware id is "DONGL-FFFFF-FFFFF-FFFFF-FFFFF".

The hardware id is a string of 5 groups of 5 alphanumeric characters (certain digits and uppercase letters). An example of a hardware key is C4NBL-Q8EFC-GNKL9-CK9QU-JLEAA (the hardware id on my machine at the time I wrote this code).

The hardware id of the machine.

Method *MatchHardwareId*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean MatchHardwareId(System.String hardwareId)``

Checks if the supplied hardware id string matches this computer.

The method **MatchHardwareId** has the following parameters:

+------------------+---------------------+---------------+
| Parameter        | Type                | Description   |
+==================+=====================+===============+
| ``hardwareId``   | ``System.String``   |               |
+------------------+---------------------+---------------+

Software licensing is used, when no dongle is connected. In this case, the hardware id encodes the following information: amount of physical RAM installed, processor id, MAC addresses and disk serial numbers.

If a dongle is connected, the matching returns true always.

The hardware id is a string of 5 groups of 5 alphanumeric characters (certain digits and uppercase letters). An example of a hardware key is C4NBL-Q8EFC-GNKL9-CK9QU-JLEAA (the hardware id on my machine at the time I wrote this code).

True, if the passed hardware id matches the current hardware, false otherwise.

Method *GetBuildDate*
^^^^^^^^^^^^^^^^^^^^^

``Date GetBuildDate()``

Returns the build date.

The build date.

Method *GenerateLicenseKey*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.String GenerateLicenseKey(LicenseData license, System.String publicKey, System.String privateKey, System.String saLicenseKey)``

Generate a license key.

The method **GenerateLicenseKey** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``license``        | ``LicenseData``     |               |
+--------------------+---------------------+---------------+
| ``publicKey``      | ``System.String``   |               |
+--------------------+---------------------+---------------+
| ``privateKey``     | ``System.String``   |               |
+--------------------+---------------------+---------------+
| ``saLicenseKey``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

You need to pass a product id, a public and private key pair as well as the license key of the SoftActivate SDK. If any of these parameters is wrong, the function will throw an exception.

Returns a license key. Afterwards, the license key needs to be activated on a specific machine.

Method *ActivateLicenseKey*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.String ActivateLicenseKey(System.String licenseKey, System.String hardwareId, System.String publicKey, System.String activationPublicKey, System.String activationServerUrl)``

Activates a license key.

The method **ActivateLicenseKey** has the following parameters:

+---------------------------+---------------------+---------------+
| Parameter                 | Type                | Description   |
+===========================+=====================+===============+
| ``licenseKey``            | ``System.String``   |               |
+---------------------------+---------------------+---------------+
| ``hardwareId``            | ``System.String``   |               |
+---------------------------+---------------------+---------------+
| ``publicKey``             | ``System.String``   |               |
+---------------------------+---------------------+---------------+
| ``activationPublicKey``   | ``System.String``   |               |
+---------------------------+---------------------+---------------+
| ``activationServerUrl``   | ``System.String``   |               |
+---------------------------+---------------------+---------------+

Activation is performed using the license server on the internet. If activation was successfull, the activation key is returned by this function.

Method *CheckUpdateExpiration*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean CheckUpdateExpiration(System.String licenseKey, Date buildDate, System.String publicKey)``

Check update expiration.

The method **CheckUpdateExpiration** has the following parameters:

+------------------+---------------------+---------------+
| Parameter        | Type                | Description   |
+==================+=====================+===============+
| ``licenseKey``   | ``System.String``   |               |
+------------------+---------------------+---------------+
| ``buildDate``    | ``Date``            |               |
+------------------+---------------------+---------------+
| ``publicKey``    | ``System.String``   |               |
+------------------+---------------------+---------------+

The expiration date for updates is stored in the license key or in the dongle. If the build date of nGI is newer, then the license is considered invalid.

True, if the license is valid. False, if the build date of the software is newer and the license is invalid.

Method *CheckLicenseKey*
^^^^^^^^^^^^^^^^^^^^^^^^

``LicenseData CheckLicenseKey(System.String licenseKey, System.String hardwareId, System.String activationKey, Date buildDate, System.String publicKey, System.String activationPublicKey)``

Check a license.

The method **CheckLicenseKey** has the following parameters:

+---------------------------+---------------------+---------------+
| Parameter                 | Type                | Description   |
+===========================+=====================+===============+
| ``licenseKey``            | ``System.String``   |               |
+---------------------------+---------------------+---------------+
| ``hardwareId``            | ``System.String``   |               |
+---------------------------+---------------------+---------------+
| ``activationKey``         | ``System.String``   |               |
+---------------------------+---------------------+---------------+
| ``buildDate``             | ``Date``            |               |
+---------------------------+---------------------+---------------+
| ``publicKey``             | ``System.String``   |               |
+---------------------------+---------------------+---------------+
| ``activationPublicKey``   | ``System.String``   |               |
+---------------------------+---------------------+---------------+

Software licensing is used, when no dongle is connected. In this case, the license key is checked against a hardware id and an activation key. Also, the expiration date of the license is checked. In addition, the build date must be before the encoded update\_expiration\_date. Further, the module flags are read back from the license information encoded in the keys.

If a dongle is checked, the build date must be before the encoded update\_expiration\_date. Further, the module flags are read back from the dongle memory.

A structure containing the licensing data. The valid field in the license\_data is set to true, if the license is valid. The rest of the returned is only meaningul, if the license is valid.

Method *ReadLicense*
^^^^^^^^^^^^^^^^^^^^

``LicenseData ReadLicense(System.String licenseKey, System.String activationKey, System.String publicKey, System.String activationPublicKey)``

Read license information from dongle or key.

The method **ReadLicense** has the following parameters:

+---------------------------+---------------------+---------------+
| Parameter                 | Type                | Description   |
+===========================+=====================+===============+
| ``licenseKey``            | ``System.String``   |               |
+---------------------------+---------------------+---------------+
| ``activationKey``         | ``System.String``   |               |
+---------------------------+---------------------+---------------+
| ``publicKey``             | ``System.String``   |               |
+---------------------------+---------------------+---------------+
| ``activationPublicKey``   | ``System.String``   |               |
+---------------------------+---------------------+---------------+

A structure containing the licensing data.

Method *GetLicense*
^^^^^^^^^^^^^^^^^^^

``LicenseData GetLicense()``

Get the license information.

If check\_license() has been called at least once, the license information can be retrieved with this call.

A structure containing the licensing data. The valid field in the license\_data is set to true, if the license is valid. The rest of the returned is only meaningul, if the license is valid.
