Class *GeometryQuadraticBezier*
-------------------------------

The following operations are implemented: operator== : comparison for equality.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **GeometryQuadraticBezier** implements the following interfaces:

+------------------+
| Interface        |
+==================+
| ``IEquatable``   |
+------------------+

The class **GeometryQuadraticBezier** contains the following properties:

+--------------------+-------+-------+----------------------------------------------+
| Property           | Get   | Set   | Description                                  |
+====================+=======+=======+==============================================+
| ``ControlPoint``   | \*    | \*    | The control point of the quadratic bezier.   |
+--------------------+-------+-------+----------------------------------------------+

The class **GeometryQuadraticBezier** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

operator!= : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *GeometryQuadraticBezier*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``GeometryQuadraticBezier()``

Construct a quadratic bezier.

Constructors
~~~~~~~~~~~~

Constructor *GeometryQuadraticBezier*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``GeometryQuadraticBezier(PointDouble controlPoint, PointDouble endPoint)``

Construct a quadratic bezier.

The constructor has the following parameters:

+--------------------+-------------------+----------------------+
| Parameter          | Type              | Description          |
+====================+===================+======================+
| ``controlPoint``   | ``PointDouble``   | The control point.   |
+--------------------+-------------------+----------------------+
| ``endPoint``       | ``PointDouble``   | The end point.       |
+--------------------+-------------------+----------------------+

Properties
~~~~~~~~~~

Property *ControlPoint*
^^^^^^^^^^^^^^^^^^^^^^^

``PointDouble ControlPoint``

The control point of the quadratic bezier.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
