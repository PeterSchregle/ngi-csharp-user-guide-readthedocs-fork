﻿.. figure:: images/n-vision.png
   :alt: 

nGI C# User Guide

**nGI Machine Vision Development Library**

Release 2017.3

.. figure:: images/monitor.png
   :alt: 

| *Copyright (c) 2017 Impuls Imaging GmbH*
| *All rights reserved.*

.. figure:: images/ImpulsLogo.png
   :alt: 

| **Impuls Imaging GmbH**
| Schlingener Str. 4
| 86842 Türkheim

Germany/European Union

http://www.impuls-imaging.com

.. figure:: images/made-in-germany.png
   :alt: 

User Guide
==========
   
.. toctree::
   :maxdepth: 2

   introduction

Reference
=========

Modules
-------
   
.. toctree::
   :maxdepth: 2

   module_ImageProcessing
   module_BarcodeMatrixcode
   module_AnalysisMeasuring
   module_CadPdf
   module_Camera
   module_TemplateMatching
   module_OcrOcv

Static Classes
--------------
   
.. toctree::
   :maxdepth: 2

   file_affine_transformation
   file_basic_algorithms
   file_binning_algorithms
   file_calibration
   file_color_algorithms
   file_filter_algorithms
   file_geometry_algorithms
   file_isotropic_scaling_transformation
   file_map_algorithms
   file_mathematics
   file_morphology_algorithms
   file_perspective_transformation
   file_point_algorithms
   file_preset_algorithms
   file_rigid_transformation
   file_rotation_transformation
   file_scaling_transformation
   file_segmentation_algorithms
   file_similarity_transformation
   file_split_and_combine_algorithms
   file_statistic
   file_stereo_algorithms
   file_subsample_algorithms
   file_translation_transformation
   file_xcoder_algorithms
   file_information
   file_license
   file_tests
   file_barcode_algorithms
   file_matrixcode_algorithms
   file_gauging_algorithms
   file_camera_buffer_converter
   file_template_matching_algorithms
   file_ocr

Classes
-------
   
.. toctree::
   :maxdepth: 2

   file_affine_matrix
   file_angle
   file_arc
   file_arc_enumerator
   file_arc_list
   file_arithmetic_mean_functor
   file_barcode_decoder
   file_box
   file_box_3d
   file_box_3d_enumerator
   file_box_3d_list
   file_box_enumerator
   file_box_list
   file_brush_enumerator
   file_brush_list
   file_buffer
   file_buffer_enumerator
   file_buffer_list
   file_central_moments
   file_chain_code
   file_cifx_driver
   file_cifx_channel
   file_chord
   file_chord_enumerator
   file_chord_list
   file_circle
   file_circle_enumerator
   file_circle_list
   file_color
   file_color_enumerator
   file_color_list
   file_corner_radius
   file_count_tool_group
   file_count_tool_group_enumerator
   file_count_tool_group_list
   file_cubic_bezier
   file_data_enumerator
   file_data_list
   file_direction
   file_display
   file_display_base
   file_double_enumerator
   file_double_list
   file_dt_openlayers_system
   file_dt_openlayers_module
   file_ellipse
   file_ellipse_enumerator
   file_ellipse_list
   file_elliptical_arc
   file_elliptical_arc_enumerator
   file_elliptical_arc_list
   file_elliptical_ring
   file_elliptical_ring_enumerator
   file_elliptical_ring_list
   file_elliptical_sector
   file_elliptical_sector_enumerator
   file_elliptical_sector_list
   file_extent
   file_extent_enumerator
   file_extent_list
   file_extent_3d
   file_extent_3d_enumerator
   file_extent_3d_list
   file_file
   file_fit_result
   file_flusser_moments
   file_font
   file_font_enumerator
   file_font_list
   file_freeman_code
   file_freeman_code_enumerator
   file_freeman_code_list
   file_geometric_mean_functor
   file_geometry_element
   file_geometry_element_enumerator
   file_geometry_element_list
   file_geometry
   file_geometry_enumerator
   file_geometry_list
   file_geometry_start
   file_geometry_line_segment
   file_geometry_arc
   file_geometry_cubic_bezier
   file_geometry_elliptical_arc
   file_geometry_quadratic_bezier
   file_graphics_context
   file_harmonic_mean_functor
   file_histogram
   file_histogram_enumerator
   file_histogram_list
   file_hls
   file_hsi
   file_hu_moments
   file_image
   file_image_enumerator
   file_image_fileformat_info
   file_image_fileformat_info_enumerator
   file_image_fileformat_info_list
   file_image_file_info
   file_image_list
   file_image_with_palette
   file_image_pyramid
   file_image_pyramid_enumerator
   file_image_pyramid_list
   file_index
   file_index_enumerator
   file_index_list
   file_index_3d
   file_index_3d_enumerator
   file_index_3d_list
   file_inpout
   file_io_resource
   file_isotropic_scaling_matrix
   file_lab
   file_line
   file_line_enumerator
   file_line_list
   file_line_segment
   file_line_segment_enumerator
   file_line_segment_list
   file_locator
   file_matrixcode_decoder
   file_matrix
   file_maximum_functor
   file_median_functor
   file_midrange_functor
   file_minimum_functor
   file_moments
   file_mono
   file_mono_alpha
   file_normalized_moments
   file_number
   file_ordered_points
   file_palette
   file_palette_enumerator
   file_palette_list
   file_pen
   file_pen_enumerator
   file_pen_list
   file_perspective_matrix
   file_point
   file_point_3d
   file_point_3d_enumerator
   file_point_3d_list
   file_point_enumerator
   file_point_list
   file_polyline
   file_polyline_enumerator
   file_polyline_list
   file_pose
   file_profile
   file_profile_enumerator
   file_profile_list
   file_quadratic_bezier
   file_quadratic_mean_functor
   file_quadrilateral
   file_quadrilateral_enumerator
   file_quadrilateral_list
   file_radiometric_stereo_result
   file_range
   file_range_functor
   file_ray
   file_ray_enumerator
   file_ray_list
   file_rectangle
   file_rectangle_enumerator
   file_rectangle_list
   file_region
   file_region_enumerator
   file_region_list
   file_region_pyramid
   file_rgb
   file_rgba
   file_rigid_matrix
   file_ring
   file_ring_enumerator
   file_ring_list
   file_rotation_matrix
   file_scale_invariant_moments
   file_scaling_matrix
   file_sector
   file_sector_enumerator
   file_sector_list
   file_serial_port
   file_similarity_matrix
   file_solid_color_brush
   file_tcpip_client
   file_tcpip_server
   file_thick_line_segment
   file_thick_line_segment_enumerator
   file_thick_line_segment_list
   file_thickness
   file_translation_matrix
   file_triangle
   file_triangle_enumerator
   file_triangle_list
   file_tweening
   file_tweening_linear
   file_tweening_binary
   file_tweening_power_1
   file_tweening_power_2
   file_tweening_power_3
   file_tweening_power_4
   file_tweening_sinusoidal_1
   file_tweening_sinusoidal_2
   file_tweening_sinusoidal_3
   file_tweening_sinusoidal_4
   file_uint16_enumerator
   file_uint16_list
   file_uint32_enumerator
   file_uint32_list
   file_vector
   file_vector_3d
   file_vector_3d_enumerator
   file_vector_3d_list
   file_vector_enumerator
   file_vector_list
   file_view
   file_widget_arc
   file_widget_base
   file_widget_base_enumerator
   file_widget_base_list
   file_widget_box
   file_widget_circle
   file_widget_coordinate_system
   file_widget_curve
   file_widget_count_tool
   file_widget_ellipse
   file_widget_horizontal_cursor
   file_widget_horizontal_scale
   file_widget_image
   file_widget_interactive
   file_widget_layer
   file_widget_layoutable
   file_widget_line
   file_widget_line_segment
   file_widget_mouse
   file_widget_orthogonal_cursor
   file_widget_orthogonal_grid
   file_widget_palette
   file_widget_point
   file_widget_polar_grid
   file_widget_polygon
   file_widget_polyline
   file_widget_quadrilateral
   file_widget_ray
   file_widget_rectangle
   file_widget_region_list
   file_widget_rule
   file_widget_scale_bar
   file_widget_text
   file_widget_text_box
   file_widget_thick_line_segment
   file_widget_triangle
   file_widget_vertical_cursor
   file_widget_vertical_scale
   file_widget_view
   file_xyz
   file_ocr_character
   file_ocr_character_list
   file_ocr_character_enumerator
   file_ocr_character_info
   file_ocr_character_info_list
   file_ocr_character_info_enumerator
   file_argument_out_of_range_exception
   file_blue_cougar_device_exception
   file_blue_fox_device_exception
   file_com_error_exception
   file_date
   file_device_exception
   file_edge_1d_info_enumerator
   file_exception
   file_file_format_exception
   file_int32_enumerator
   file_int32_list
   file_int64_enumerator
   file_int64_list
   file_invalid_operation_exception
   file_invariant_violated_exception
   file_ipp_exception
   file_license_data
   file_license_exception
   file_matrix_exception
   file_postcondition_violated_exception
   file_precondition_violated_exception
   file_simulation_device_exception
   file_string_enumerator
   file_string_list
   file_type_mismatch_exception
   file_ueye_device_exception
   file_unknown_exception
   file_wrap_test_1
   file_wrap_test_2
   file_barcode
   file_barcode_enumerator
   file_barcode_list
   file_matrixcode
   file_matrixcode_enumerator
   file_matrixcode_list
   file_mviz_barcode
   file_mviz_barcode_decoder
   file_mviz_matrixcode
   file_mviz_matrixcode_decoder
   file_blob_features
   file_blob_features_enumerator
   file_blob_features_list
   file_edge_1d
   file_edge_1d_enumerator
   file_edge_1d_info
   file_edge_1d_info_list
   file_edge_1d_list
   file_edge_2d
   file_edge_2d_enumerator
   file_edge_2d_info
   file_edge_2d_info_enumerator
   file_edge_2d_info_list
   file_edge_2d_list
   file_cad_contour
   file_cad_contour_enumerator
   file_cad_contour_list
   file_cad_drawing
   file_cad_parser
   file_teigha_drawing
   file_teigha_drawing2
   file_teigha_layer
   file_teigha_layer_enumerator
   file_teigha_layer_list
   file_teigha_parser
   file_teigha_parser2
   file_camera
   file_camera_buffer
   file_camera_info
   file_camera_info_enumerator
   file_camera_info_list
   file_camera_manager
   file_camera_parameter_base
   file_camera_parameter_base_enumerator
   file_camera_parameter_base_list
   file_camera_parameter_iboolean
   file_camera_parameter_icategory
   file_camera_parameter_icommand
   file_camera_parameter_ienumeration
   file_camera_parameter_ienum_entry
   file_camera_parameter_iinteger
   file_camera_parameter_ifloat
   file_camera_parameter_istring
   file_camera_pixelformat_info
   file_camera_pixelformat_info_enumerator
   file_camera_pixelformat_info_list
   file_match_score_and_position
   file_match_score_and_position_enumerator
   file_match_score_and_position_list
   file_template_search_model
   file_ocr_reader

