Class *Uint16Enumerator*
------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Uint16Enumerator** implements the following interfaces:

+-------------------------+
| Interface               |
+=========================+
| ``IEnumerator``         |
+-------------------------+
| ``IEnumeratorUInt16``   |
+-------------------------+

The class **Uint16Enumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **Uint16Enumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``System.UInt16 Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
