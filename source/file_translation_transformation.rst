Class *TranslationTransformation*
---------------------------------

Translation transformation of geometric primitives.

**Namespace:** Ngi

**Module:** ImageProcessing

Description
~~~~~~~~~~~

This class provides static methods to transform geometric primitives via a translation transformation.

The class can transform these geometric primitives: point, vector, direction, line\_segment, ray, line, box, rectangle, circle, ellipse, arc elliptical\_arc, triangle, quadrilateral, polyline, quadratic\_bezier and geometry.

All primitives retain their nature, when transformed by a translation transform.

Some primitives can have an integer coordinate type on input, but the resulting types are always of floating point coordinate type. This is because the transformation cannot retain the integer nature in general. To illustrate, a point<int> type is transformed and the result is a point<double> type.

Static Methods
~~~~~~~~~~~~~~

Method *Transform*
^^^^^^^^^^^^^^^^^^

``VectorDouble Transform(VectorDouble vector, TranslationMatrix translation)``

Transform a vector.

The method **Transform** has the following parameters:

+-------------------+-------------------------+---------------+
| Parameter         | Type                    | Description   |
+===================+=========================+===============+
| ``vector``        | ``VectorDouble``        |               |
+-------------------+-------------------------+---------------+
| ``translation``   | ``TranslationMatrix``   |               |
+-------------------+-------------------------+---------------+

A vector transformed with a translation transform is the very same vector, i.e. a no-op.

Returns the transformed vector.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``PointDouble Transform(PointDouble point, TranslationMatrix translation)``

Transform a point.

The method **Transform** has the following parameters:

+-------------------+-------------------------+---------------+
| Parameter         | Type                    | Description   |
+===================+=========================+===============+
| ``point``         | ``PointDouble``         |               |
+-------------------+-------------------------+---------------+
| ``translation``   | ``TranslationMatrix``   |               |
+-------------------+-------------------------+---------------+

A point transformed with a translation transform results in another point.

The translation transformation is carried out with a matrix multiplication.

Returns the transformed vector.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Direction Transform(Direction direction, TranslationMatrix translation)``

Transform a direction.

The method **Transform** has the following parameters:

+-------------------+-------------------------+---------------+
| Parameter         | Type                    | Description   |
+===================+=========================+===============+
| ``direction``     | ``Direction``           |               |
+-------------------+-------------------------+---------------+
| ``translation``   | ``TranslationMatrix``   |               |
+-------------------+-------------------------+---------------+

A direction transformed with a translation transform is the very same direction, i.e. a no-op.

Returns the transformed direction.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``LineSegmentDouble Transform(LineSegmentDouble lineSegment, TranslationMatrix translation)``

A line segment transformed with a translation transform results in another line segment.

The method **Transform** has the following parameters:

+-------------------+-------------------------+---------------+
| Parameter         | Type                    | Description   |
+===================+=========================+===============+
| ``lineSegment``   | ``LineSegmentDouble``   |               |
+-------------------+-------------------------+---------------+
| ``translation``   | ``TranslationMatrix``   |               |
+-------------------+-------------------------+---------------+

The translation transformation is carried out with a matrix multiplication.

Returns the transformed line segment.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``RayDouble Transform(RayDouble ray, TranslationMatrix translation)``

Transform a ray.

The method **Transform** has the following parameters:

+-------------------+-------------------------+---------------+
| Parameter         | Type                    | Description   |
+===================+=========================+===============+
| ``ray``           | ``RayDouble``           |               |
+-------------------+-------------------------+---------------+
| ``translation``   | ``TranslationMatrix``   |               |
+-------------------+-------------------------+---------------+

A ray transformed with a translation transform results in another ray.

The translation transformation is carried out with a matrix multiplication.

Returns the transformed ray.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``LineDouble Transform(LineDouble line, TranslationMatrix translation)``

Transform a line.

The method **Transform** has the following parameters:

+-------------------+-------------------------+---------------+
| Parameter         | Type                    | Description   |
+===================+=========================+===============+
| ``line``          | ``LineDouble``          |               |
+-------------------+-------------------------+---------------+
| ``translation``   | ``TranslationMatrix``   |               |
+-------------------+-------------------------+---------------+

A line transformed with a translation transform results in another line.

The translation transformation is carried out with a matrix multiplication.

Returns the transformed line.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``BoxDouble Transform(BoxDouble box, TranslationMatrix translation)``

Transform a box.

The method **Transform** has the following parameters:

+-------------------+-------------------------+---------------+
| Parameter         | Type                    | Description   |
+===================+=========================+===============+
| ``box``           | ``BoxDouble``           |               |
+-------------------+-------------------------+---------------+
| ``translation``   | ``TranslationMatrix``   |               |
+-------------------+-------------------------+---------------+

A box transformed with a translation transform results in another box.

The translation transformation is carried out with a matrix multiplication.

Returns the transformed box.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Rectangle Transform(Rectangle rectangle, TranslationMatrix translation)``

Transform a rectangle.

The method **Transform** has the following parameters:

+-------------------+-------------------------+---------------+
| Parameter         | Type                    | Description   |
+===================+=========================+===============+
| ``rectangle``     | ``Rectangle``           |               |
+-------------------+-------------------------+---------------+
| ``translation``   | ``TranslationMatrix``   |               |
+-------------------+-------------------------+---------------+

A rectangle transformed with a translation transform results in another rectangle.

The translation transformation is carried out with a matrix multiplication.

Returns the transformed rectangle.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Circle Transform(Circle circle, TranslationMatrix translation)``

Transform a circle.

The method **Transform** has the following parameters:

+-------------------+-------------------------+---------------+
| Parameter         | Type                    | Description   |
+===================+=========================+===============+
| ``circle``        | ``Circle``              |               |
+-------------------+-------------------------+---------------+
| ``translation``   | ``TranslationMatrix``   |               |
+-------------------+-------------------------+---------------+

A circle transformed with a translation transform results in another circle.

The translation transformation is carried out with a matrix multiplication.

Returns the transformed circle.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Ellipse Transform(Ellipse ellipse, TranslationMatrix translation)``

Transform an ellipse.

The method **Transform** has the following parameters:

+-------------------+-------------------------+---------------+
| Parameter         | Type                    | Description   |
+===================+=========================+===============+
| ``ellipse``       | ``Ellipse``             |               |
+-------------------+-------------------------+---------------+
| ``translation``   | ``TranslationMatrix``   |               |
+-------------------+-------------------------+---------------+

An ellipse transformed with a translation transform results in another ellipse.

The translation transformation is carried out with a matrix multiplication.

Returns the transformed ellipse.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Arc Transform(Arc arc, TranslationMatrix translation)``

Transform an arc.

The method **Transform** has the following parameters:

+-------------------+-------------------------+---------------+
| Parameter         | Type                    | Description   |
+===================+=========================+===============+
| ``arc``           | ``Arc``                 |               |
+-------------------+-------------------------+---------------+
| ``translation``   | ``TranslationMatrix``   |               |
+-------------------+-------------------------+---------------+

An arc transformed with a translation transform results in another arc.

The translation transformation is carried out with a matrix multiplication.

Returns the transformed arc.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``EllipticalArc Transform(EllipticalArc ellipticalArc, TranslationMatrix translation)``

Transform an elliptical arc.

The method **Transform** has the following parameters:

+---------------------+-------------------------+---------------+
| Parameter           | Type                    | Description   |
+=====================+=========================+===============+
| ``ellipticalArc``   | ``EllipticalArc``       |               |
+---------------------+-------------------------+---------------+
| ``translation``     | ``TranslationMatrix``   |               |
+---------------------+-------------------------+---------------+

An elliptical arc transformed with a translation transform results in another elliptical arc

The translation transformation is carried out with a matrix multiplication.

Returns the transformed elliptical arc.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``TriangleDouble Transform(TriangleDouble triangle, TranslationMatrix translation)``

Transform a triangle.

The method **Transform** has the following parameters:

+-------------------+-------------------------+---------------+
| Parameter         | Type                    | Description   |
+===================+=========================+===============+
| ``triangle``      | ``TriangleDouble``      |               |
+-------------------+-------------------------+---------------+
| ``translation``   | ``TranslationMatrix``   |               |
+-------------------+-------------------------+---------------+

A triangle transformed with a translation transform results in another triangle.

The translation transformation is carried out with a matrix multiplication.

Returns the transformed triangle.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``QuadrilateralDouble Transform(QuadrilateralDouble quadrilateral, TranslationMatrix translation)``

Transform a quadrilateral.

The method **Transform** has the following parameters:

+---------------------+---------------------------+---------------+
| Parameter           | Type                      | Description   |
+=====================+===========================+===============+
| ``quadrilateral``   | ``QuadrilateralDouble``   |               |
+---------------------+---------------------------+---------------+
| ``translation``     | ``TranslationMatrix``     |               |
+---------------------+---------------------------+---------------+

A quadrilateral transformed with a translation transform results in another quadrilateral.

The translation transformation is carried out with a matrix multiplication.

Returns the transformed quadrilateral.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``PolylineDouble Transform(PolylineDouble polyline, TranslationMatrix translation)``

Transform a polyline.

The method **Transform** has the following parameters:

+-------------------+-------------------------+---------------+
| Parameter         | Type                    | Description   |
+===================+=========================+===============+
| ``polyline``      | ``PolylineDouble``      |               |
+-------------------+-------------------------+---------------+
| ``translation``   | ``TranslationMatrix``   |               |
+-------------------+-------------------------+---------------+

A polyline transformed with a translation transform results in another polyline.

The translation transformation is carried out with a matrix multiplication.

Returns the transformed polyline.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``QuadraticBezier Transform(QuadraticBezier bezier, TranslationMatrix translation)``

Transform a quadratic bezier curve.

The method **Transform** has the following parameters:

+-------------------+-------------------------+---------------+
| Parameter         | Type                    | Description   |
+===================+=========================+===============+
| ``bezier``        | ``QuadraticBezier``     |               |
+-------------------+-------------------------+---------------+
| ``translation``   | ``TranslationMatrix``   |               |
+-------------------+-------------------------+---------------+

A quadratic bezier curve transformed with a translation transform results in another quadratic bezier curve.

The translation transformation is carried out with a matrix multiplication.

Returns the transformed quadratic bezier curve.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``CubicBezier Transform(CubicBezier bezier, TranslationMatrix translation)``

Transform a cubic bezier curve.

The method **Transform** has the following parameters:

+-------------------+-------------------------+---------------+
| Parameter         | Type                    | Description   |
+===================+=========================+===============+
| ``bezier``        | ``CubicBezier``         |               |
+-------------------+-------------------------+---------------+
| ``translation``   | ``TranslationMatrix``   |               |
+-------------------+-------------------------+---------------+

A cubic bezier curve transformed with a translation transform results in another cubic bezier curve.

The translation transformation is carried out with a matrix multiplication.

Returns the transformed cubic bezier curve.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Geometry Transform(Geometry geometry, TranslationMatrix translation)``

Transform a geometry.

The method **Transform** has the following parameters:

+-------------------+-------------------------+---------------+
| Parameter         | Type                    | Description   |
+===================+=========================+===============+
| ``geometry``      | ``Geometry``            |               |
+-------------------+-------------------------+---------------+
| ``translation``   | ``TranslationMatrix``   |               |
+-------------------+-------------------------+---------------+

A geometry transformed with a translation transform results in another geometry.

The translation transformation is carried out by affine transformation of each geometry element.

Returns the transformed geometry.
