Class *BufferList*
------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **BufferList** contains the following variant parameters:

+--------------+-----------------------------------------+
| Variant      | Description                             |
+==============+=========================================+
| ``Buffer``   | TODO no brief description for variant   |
+--------------+-----------------------------------------+

The class **BufferList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **BufferList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Buffer*
^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Buffer** has the following types:

+---------------------------------+
| Type                            |
+=================================+
| ``SharedPtrBufferByte``         |
+---------------------------------+
| ``SharedPtrBufferUInt16``       |
+---------------------------------+
| ``SharedPtrBufferUInt32``       |
+---------------------------------+
| ``SharedPtrBufferDouble``       |
+---------------------------------+
| ``SharedPtrBufferRgbByte``      |
+---------------------------------+
| ``SharedPtrBufferRgbUInt16``    |
+---------------------------------+
| ``SharedPtrBufferRgbUInt32``    |
+---------------------------------+
| ``SharedPtrBufferRgbDouble``    |
+---------------------------------+
| ``SharedPtrBufferRgbaByte``     |
+---------------------------------+
| ``SharedPtrBufferRgbaUInt16``   |
+---------------------------------+
| ``SharedPtrBufferRgbaUInt32``   |
+---------------------------------+
| ``SharedPtrBufferRgbaDouble``   |
+---------------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *BufferList*
^^^^^^^^^^^^^^^^^^^^^^^^

``BufferList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``Buffer [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``BufferEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(Buffer item)``

The method **Add** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``item``    | ``Buffer``   |               |
+-------------+--------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(Buffer item)``

The method **Contains** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``item``    | ``Buffer``   |               |
+-------------+--------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(Buffer item)``

The method **Remove** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``item``    | ``Buffer``   |               |
+-------------+--------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(Buffer item)``

The method **IndexOf** has the following parameters:

+-------------+--------------+---------------+
| Parameter   | Type         | Description   |
+=============+==============+===============+
| ``item``    | ``Buffer``   |               |
+-------------+--------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, Buffer item)``

The method **Insert** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``item``    | ``Buffer``         |               |
+-------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
