Class *IndexEnumerator*
-----------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **IndexEnumerator** implements the following interfaces:

+------------------------+
| Interface              |
+========================+
| ``IEnumerator``        |
+------------------------+
| ``IEnumeratorIndex``   |
+------------------------+

The class **IndexEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **IndexEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Index Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
