Class *HistogramList*
---------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **HistogramList** implements the following interfaces:

+------------------------------------------+
| Interface                                |
+==========================================+
| ``IListHistogramListHistogramVariant``   |
+------------------------------------------+

The class **HistogramList** contains the following variant parameters:

+-----------------+-----------------------------------------+
| Variant         | Description                             |
+=================+=========================================+
| ``Histogram``   | TODO no brief description for variant   |
+-----------------+-----------------------------------------+

The class **HistogramList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **HistogramList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Histogram*
^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Histogram** has the following types:

+---------------------------+
| Type                      |
+===========================+
| ``HistogramUInt32``       |
+---------------------------+
| ``HistogramDouble``       |
+---------------------------+
| ``HistogramRgbUInt32``    |
+---------------------------+
| ``HistogramRgbDouble``    |
+---------------------------+
| ``HistogramRgbaUInt32``   |
+---------------------------+
| ``HistogramRgbaDouble``   |
+---------------------------+
| ``HistogramHsiDouble``    |
+---------------------------+
| ``HistogramHlsDouble``    |
+---------------------------+
| ``HistogramLabDouble``    |
+---------------------------+
| ``HistogramXyzDouble``    |
+---------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *HistogramList*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``HistogramList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``Histogram [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``HistogramEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(Histogram item)``

The method **Add** has the following parameters:

+-------------+-----------------+---------------+
| Parameter   | Type            | Description   |
+=============+=================+===============+
| ``item``    | ``Histogram``   |               |
+-------------+-----------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(Histogram item)``

The method **Contains** has the following parameters:

+-------------+-----------------+---------------+
| Parameter   | Type            | Description   |
+=============+=================+===============+
| ``item``    | ``Histogram``   |               |
+-------------+-----------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(Histogram item)``

The method **Remove** has the following parameters:

+-------------+-----------------+---------------+
| Parameter   | Type            | Description   |
+=============+=================+===============+
| ``item``    | ``Histogram``   |               |
+-------------+-----------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(Histogram item)``

The method **IndexOf** has the following parameters:

+-------------+-----------------+---------------+
| Parameter   | Type            | Description   |
+=============+=================+===============+
| ``item``    | ``Histogram``   |               |
+-------------+-----------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, Histogram item)``

The method **Insert** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``item``    | ``Histogram``      |               |
+-------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
