Class *TcpipServer*
-------------------

The TCP/IP server.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **TcpipServer** contains the following properties:

+-------------------+-------+-------+--------------------------------------------------------------------------------------------------------------------------+
| Property          | Get   | Set   | Description                                                                                                              |
+===================+=======+=======+==========================================================================================================================+
| ``Port``          | \*    |       | The port or service name (such as 21, http, etc.) Possible values for the service name are listed in the file WINDIR%.   |
+-------------------+-------+-------+--------------------------------------------------------------------------------------------------------------------------+
| ``Family``        | \*    |       | The size of an arc.                                                                                                      |
+-------------------+-------+-------+--------------------------------------------------------------------------------------------------------------------------+
| ``BufferSize``    | \*    |       | Read and write buffer size.                                                                                              |
+-------------------+-------+-------+--------------------------------------------------------------------------------------------------------------------------+
| ``IsListening``   | \*    |       | True, if the server is listing for client connections.                                                                   |
+-------------------+-------+-------+--------------------------------------------------------------------------------------------------------------------------+
| ``ReadTimeout``   | \*    |       | Read timeout [ms].                                                                                                       |
+-------------------+-------+-------+--------------------------------------------------------------------------------------------------------------------------+
| ``HostName``      | \*    |       | Retrieves the name of this server.                                                                                       |
+-------------------+-------+-------+--------------------------------------------------------------------------------------------------------------------------+
| ``PeerName``      | \*    |       | Retrieves the name of the client which connected to this server.                                                         |
+-------------------+-------+-------+--------------------------------------------------------------------------------------------------------------------------+

The class **TcpipServer** contains the following methods:

+---------------------+-------------------------------------------+
| Method              | Description                               |
+=====================+===========================================+
| ``Listen``          | Start listening for client connections.   |
+---------------------+-------------------------------------------+
| ``StopListening``   | Stop listening for client connections.    |
+---------------------+-------------------------------------------+

The class **TcpipServer** contains the following enumerations:

+---------------------+------------------------------+
| Enumeration         | Description                  |
+=====================+==============================+
| ``AddressFamily``   | TODO documentation missing   |
+---------------------+------------------------------+

Description
~~~~~~~~~~~

The TCP/IP server is one endpoint of a socket connection. The tcpip\_server specifies a port and the adress family to be used.

The server listens on the specific port for incoming connections from a client.

Constructors
~~~~~~~~~~~~

Constructor *TcpipServer*
^^^^^^^^^^^^^^^^^^^^^^^^^

``TcpipServer(System.String port, TcpipServer.AddressFamily family, System.Int32 bufferSize, System.UInt32 readTimeout)``

Construct the TCP/IP server.

The constructor has the following parameters:

+--------------+------------+----------------------------------------------------+
| Parameter    | Type       | Description                                        |
+==============+============+====================================================+
| ``port``     | ``System.S | The port or service name (such as 21, http, etc.)  |
|              | tring``    | Possible values for the service name are listed in |
|              |            | the file WINDIR%.                                  |
+--------------+------------+----------------------------------------------------+
| ``family``   | ``TcpipSer | The address family.                                |
|              | ver.Addres |                                                    |
|              | sFamily``  |                                                    |
+--------------+------------+----------------------------------------------------+
| ``bufferSize | ``System.I | The size of the read and write buffers.            |
| ``           | nt32``     |                                                    |
+--------------+------------+----------------------------------------------------+
| ``readTimeou | ``System.U |                                                    |
| t``          | Int32``    |                                                    |
+--------------+------------+----------------------------------------------------+

Properties
~~~~~~~~~~

Property *Port*
^^^^^^^^^^^^^^^

``System.String Port``

The port or service name (such as 21, http, etc.) Possible values for the service name are listed in the file WINDIR%.

Property *Family*
^^^^^^^^^^^^^^^^^

``TcpipServer.AddressFamily Family``

The size of an arc.

Property *BufferSize*
^^^^^^^^^^^^^^^^^^^^^

``System.Int32 BufferSize``

Read and write buffer size.

Property *IsListening*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsListening``

True, if the server is listing for client connections.

Property *ReadTimeout*
^^^^^^^^^^^^^^^^^^^^^^

``System.UInt32 ReadTimeout``

Read timeout [ms].

Property *HostName*
^^^^^^^^^^^^^^^^^^^

``System.String HostName``

Retrieves the name of this server.

Property *PeerName*
^^^^^^^^^^^^^^^^^^^

``System.String PeerName``

Retrieves the name of the client which connected to this server.

Methods
~~~~~~~

Method *Listen*
^^^^^^^^^^^^^^^

``void Listen()``

Start listening for client connections.

listen() should be called before open() is called.

Method *StopListening*
^^^^^^^^^^^^^^^^^^^^^^

``void StopListening()``

Stop listening for client connections.

If another thread should be blocked in open(), calling stop\_listening() will casue open() to return.

Enumerations
~~~~~~~~~~~~

Enumeration *AddressFamily*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum AddressFamily``

TODO documentation missing

The enumeration **AddressFamily** has the following constants:

+------------+---------+---------------+
| Name       | Value   | Description   |
+============+=========+===============+
| ``ipv4``   | ``1``   |               |
+------------+---------+---------------+
| ``ipv6``   | ``2``   |               |
+------------+---------+---------------+

::

    enum AddressFamily
    {
      ipv4 = 1,
      ipv6 = 2,
    };

TODO documentation missing
