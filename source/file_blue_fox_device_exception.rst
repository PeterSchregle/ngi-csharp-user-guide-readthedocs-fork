Class *BlueFoxDeviceException*
------------------------------

The mvBlueFOX specific device\_exception type.

**Namespace:** Ngi

**Module:**

The class **BlueFoxDeviceException** contains the following enumerations:

+--------------------+------------------------------+
| Enumeration        | Description                  |
+====================+==============================+
| ``ExceptionIds``   | TODO documentation missing   |
+--------------------+------------------------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *BlueFoxDeviceException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``BlueFoxDeviceException()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *BlueFoxDeviceException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``BlueFoxDeviceException(System.String message)``

Construct an exception from a message.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+

Constructor *BlueFoxDeviceException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``BlueFoxDeviceException(System.String message, System.UInt32 id)``

Construct an exception from a message and an id.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+
| ``id``        | ``System.UInt32``   | The exception id.        |
+---------------+---------------------+--------------------------+

Enumerations
~~~~~~~~~~~~

Enumeration *ExceptionIds*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum ExceptionIds``

TODO documentation missing

The enumeration **ExceptionIds** has the following constants:

+------------------------------------+---------+---------------+
| Name                               | Value   | Description   |
+====================================+=========+===============+
| ``notAnError``                     | ``0``   |               |
+------------------------------------+---------+---------------+
| ``nativeBlueFoxError``             | ``1``   |               |
+------------------------------------+---------+---------------+
| ``cameraIdOutOfRange``             | ``2``   |               |
+------------------------------------+---------+---------------+
| ``threadPriorityOutOfRange``       | ``3``   |               |
+------------------------------------+---------+---------------+
| ``noHrtcAvailable``                | ``4``   |               |
+------------------------------------+---------+---------------+
| ``frameRateOutOfRange``            | ``5``   |               |
+------------------------------------+---------+---------------+
| ``cameraSerialNumberOutOfRange``   | ``6``   |               |
+------------------------------------+---------+---------------+

::

    enum ExceptionIds
    {
      notAnError = 0,
      nativeBlueFoxError = 1,
      cameraIdOutOfRange = 2,
      threadPriorityOutOfRange = 3,
      noHrtcAvailable = 4,
      frameRateOutOfRange = 5,
      cameraSerialNumberOutOfRange = 6,
    };

TODO documentation missing
