Class *EllipticalRingEnumerator*
--------------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **EllipticalRingEnumerator** implements the following interfaces:

+---------------------------------+
| Interface                       |
+=================================+
| ``IEnumerator``                 |
+---------------------------------+
| ``IEnumeratorEllipticalRing``   |
+---------------------------------+

The class **EllipticalRingEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **EllipticalRingEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``EllipticalRing Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
