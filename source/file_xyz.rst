Class *Xyz*
-----------

The xyz color type.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Xyz** contains the following variant parameters:

+-----------------+-----------------------------------------+
| Variant         | Description                             |
+=================+=========================================+
| ``Component``   | TODO no brief description for variant   |
+-----------------+-----------------------------------------+

The class **Xyz** contains the following properties:

+------------+-------+-------+-------------------------------------+
| Property   | Get   | Set   | Description                         |
+============+=======+=======+=====================================+
| ``X``      | \*    |       | The x\_ color component property.   |
+------------+-------+-------+-------------------------------------+
| ``Y``      | \*    |       | The y\_ color component property.   |
+------------+-------+-------+-------------------------------------+
| ``Z``      | \*    |       | The z\_ color component property.   |
+------------+-------+-------+-------------------------------------+

The class **Xyz** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

The xyz color type template allows to be instantiated for the basic integer and floating point types.

Variants
~~~~~~~~

Variant *Component*
^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Component** has the following types:

+--------------+
| Type         |
+==============+
| ``Int8``     |
+--------------+
| ``Byte``     |
+--------------+
| ``Int16``    |
+--------------+
| ``UInt16``   |
+--------------+
| ``Int32``    |
+--------------+
| ``UInt32``   |
+--------------+
| ``Int64``    |
+--------------+
| ``UInt64``   |
+--------------+
| ``Single``   |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Xyz*
^^^^^^^^^^^^^^^^^

``Xyz()``

Default constructor.

This constructor sets the three components to 0.

Constructors
~~~~~~~~~~~~

Constructor *Xyz*
^^^^^^^^^^^^^^^^^

``Xyz(System.Object value)``

Standard constructor.

The constructor has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``value``   | ``System.Object``   | The value.    |
+-------------+---------------------+---------------+

This constructor initializes the three components with a value.

The constructor also serves as a conversion operator from the scalar type T to type xyz<T>.

Constructor *Xyz*
^^^^^^^^^^^^^^^^^

``Xyz(System.Object x, System.Object y, System.Object z)``

Standard constructor.

The constructor has the following parameters:

+-------------+---------------------+--------------------+
| Parameter   | Type                | Description        |
+=============+=====================+====================+
| ``x``       | ``System.Object``   | The x component.   |
+-------------+---------------------+--------------------+
| ``y``       | ``System.Object``   | The y component.   |
+-------------+---------------------+--------------------+
| ``z``       | ``System.Object``   | The z component.   |
+-------------+---------------------+--------------------+

This constructor initializes the three components with different values.

Properties
~~~~~~~~~~

Property *X*
^^^^^^^^^^^^

``System.Object X``

The x\_ color component property.

Property *Y*
^^^^^^^^^^^^

``System.Object Y``

The y\_ color component property.

Property *Z*
^^^^^^^^^^^^

``System.Object Z``

The z\_ color component property.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
