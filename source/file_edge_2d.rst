Class *Edge2d*
--------------

The edge\_2d as returned by gauging.

**Namespace:** Ngi

**Module:** AnalysisMeasuring

The class **Edge2d** implements the following interfaces:

+------------------------+
| Interface              |
+========================+
| ``IEquatableEdge2d``   |
+------------------------+
| ``ISerializable``      |
+------------------------+

The class **Edge2d** contains the following properties:

+----------------+-------+-------+-------------------------------+
| Property       | Get   | Set   | Description                   |
+================+=======+=======+===============================+
| ``Position``   | \*    |       | The position of the edge.     |
+----------------+-------+-------+-------------------------------+
| ``Strength``   | \*    |       | The strength of the edge.     |
+----------------+-------+-------+-------------------------------+
| ``Grey``       | \*    |       | The grey value of the edge.   |
+----------------+-------+-------+-------------------------------+

The class **Edge2d** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

Edge characteristics are the position, the strength and the grey value.

The following operators are implemented for a edge\_2d: operator == : comparison for equality. operator != : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Edge2d*
^^^^^^^^^^^^^^^^^^^^

``Edge2d()``

Standard constructor.

Constructors
~~~~~~~~~~~~

Constructor *Edge2d*
^^^^^^^^^^^^^^^^^^^^

``Edge2d(PointDouble position, System.Double strength, System.Double grey)``

Standard constructor.

The constructor has the following parameters:

+----------------+---------------------+-------------------------------+
| Parameter      | Type                | Description                   |
+================+=====================+===============================+
| ``position``   | ``PointDouble``     | The position of the edge.     |
+----------------+---------------------+-------------------------------+
| ``strength``   | ``System.Double``   | The strength of the edge.     |
+----------------+---------------------+-------------------------------+
| ``grey``       | ``System.Double``   | The grey value of the edge.   |
+----------------+---------------------+-------------------------------+

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``PointDouble Position``

The position of the edge.

Property *Strength*
^^^^^^^^^^^^^^^^^^^

``System.Double Strength``

The strength of the edge.

Property *Grey*
^^^^^^^^^^^^^^^

``System.Double Grey``

The grey value of the edge.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
