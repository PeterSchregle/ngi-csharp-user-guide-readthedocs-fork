Class *GeometryAlgorithms*
--------------------------

Geometric image transformation functions.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **GeometryAlgorithms** contains the following enumerations:

+-------------------------------+------------------------------+
| Enumeration                   | Description                  |
+===============================+==============================+
| ``GeometricFilter``           | TODO documentation missing   |
+-------------------------------+------------------------------+
| ``ResampleBorderTreatment``   | TODO documentation missing   |
+-------------------------------+------------------------------+
| ``ResampleBoundsExtension``   | TODO documentation missing   |
+-------------------------------+------------------------------+

Description
~~~~~~~~~~~

The class contains a group of geometric image transformation functions. It includes mirroring, flipping and rotating images, as well as functions for affine, perspective and polar transformations.

Static Methods
~~~~~~~~~~~~~~

Method *Mirror*
^^^^^^^^^^^^^^^

``ImageByte Mirror(ViewLocatorByte source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Mirror*
^^^^^^^^^^^^^^^

``ImageUInt16 Mirror(ViewLocatorUInt16 source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Mirror*
^^^^^^^^^^^^^^^

``ImageUInt32 Mirror(ViewLocatorUInt32 source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Mirror*
^^^^^^^^^^^^^^^

``ImageDouble Mirror(ViewLocatorDouble source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Mirror*
^^^^^^^^^^^^^^^

``ImageRgbByte Mirror(ViewLocatorRgbByte source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Mirror*
^^^^^^^^^^^^^^^

``ImageRgbUInt16 Mirror(ViewLocatorRgbUInt16 source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Mirror*
^^^^^^^^^^^^^^^

``ImageRgbUInt32 Mirror(ViewLocatorRgbUInt32 source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Mirror*
^^^^^^^^^^^^^^^

``ImageRgbDouble Mirror(ViewLocatorRgbDouble source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Mirror*
^^^^^^^^^^^^^^^

``ImageRgbaByte Mirror(ViewLocatorRgbaByte source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Mirror*
^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Mirror(ViewLocatorRgbaUInt16 source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Mirror*
^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Mirror(ViewLocatorRgbaUInt32 source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Mirror*
^^^^^^^^^^^^^^^

``ImageRgbaDouble Mirror(ViewLocatorRgbaDouble source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Mirror*
^^^^^^^^^^^^^^^

``ImageHlsByte Mirror(ViewLocatorHlsByte source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Mirror*
^^^^^^^^^^^^^^^

``ImageHlsUInt16 Mirror(ViewLocatorHlsUInt16 source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Mirror*
^^^^^^^^^^^^^^^

``ImageHlsDouble Mirror(ViewLocatorHlsDouble source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Mirror*
^^^^^^^^^^^^^^^

``ImageHsiByte Mirror(ViewLocatorHsiByte source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Mirror*
^^^^^^^^^^^^^^^

``ImageHsiUInt16 Mirror(ViewLocatorHsiUInt16 source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Mirror*
^^^^^^^^^^^^^^^

``ImageHsiDouble Mirror(ViewLocatorHsiDouble source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Mirror*
^^^^^^^^^^^^^^^

``ImageLabByte Mirror(ViewLocatorLabByte source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Mirror*
^^^^^^^^^^^^^^^

``ImageLabUInt16 Mirror(ViewLocatorLabUInt16 source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Mirror*
^^^^^^^^^^^^^^^

``ImageLabDouble Mirror(ViewLocatorLabDouble source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Mirror*
^^^^^^^^^^^^^^^

``ImageXyzByte Mirror(ViewLocatorXyzByte source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Mirror*
^^^^^^^^^^^^^^^

``ImageXyzUInt16 Mirror(ViewLocatorXyzUInt16 source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Mirror*
^^^^^^^^^^^^^^^

``ImageXyzDouble Mirror(ViewLocatorXyzDouble source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Mirror*
^^^^^^^^^^^^^^^

``Image Mirror(View source)``

Mirrors an image.

The method **Mirror** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

Reverses an image in the horizontal direction (exchanges left and right).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``ImageByte Flip(ViewLocatorByte source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``ImageUInt16 Flip(ViewLocatorUInt16 source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``ImageUInt32 Flip(ViewLocatorUInt32 source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``ImageDouble Flip(ViewLocatorDouble source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``ImageRgbByte Flip(ViewLocatorRgbByte source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``ImageRgbUInt16 Flip(ViewLocatorRgbUInt16 source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``ImageRgbUInt32 Flip(ViewLocatorRgbUInt32 source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``ImageRgbDouble Flip(ViewLocatorRgbDouble source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``ImageRgbaByte Flip(ViewLocatorRgbaByte source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``ImageRgbaUInt16 Flip(ViewLocatorRgbaUInt16 source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``ImageRgbaUInt32 Flip(ViewLocatorRgbaUInt32 source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``ImageRgbaDouble Flip(ViewLocatorRgbaDouble source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``ImageHlsByte Flip(ViewLocatorHlsByte source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``ImageHlsUInt16 Flip(ViewLocatorHlsUInt16 source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``ImageHlsDouble Flip(ViewLocatorHlsDouble source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``ImageHsiByte Flip(ViewLocatorHsiByte source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``ImageHsiUInt16 Flip(ViewLocatorHsiUInt16 source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``ImageHsiDouble Flip(ViewLocatorHsiDouble source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``ImageLabByte Flip(ViewLocatorLabByte source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``ImageLabUInt16 Flip(ViewLocatorLabUInt16 source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``ImageLabDouble Flip(ViewLocatorLabDouble source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``ImageXyzByte Flip(ViewLocatorXyzByte source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``ImageXyzUInt16 Flip(ViewLocatorXyzUInt16 source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``ImageXyzDouble Flip(ViewLocatorXyzDouble source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Flip*
^^^^^^^^^^^^^

``Image Flip(View source)``

Flips an image.

The method **Flip** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

Reverses an image in the vertical direction (turns it upside down).

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``ImageByte Rotate90cw(ViewLocatorByte source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``ImageUInt16 Rotate90cw(ViewLocatorUInt16 source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``ImageUInt32 Rotate90cw(ViewLocatorUInt32 source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``ImageDouble Rotate90cw(ViewLocatorDouble source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``ImageRgbByte Rotate90cw(ViewLocatorRgbByte source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 Rotate90cw(ViewLocatorRgbUInt16 source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 Rotate90cw(ViewLocatorRgbUInt32 source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble Rotate90cw(ViewLocatorRgbDouble source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte Rotate90cw(ViewLocatorRgbaByte source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Rotate90cw(ViewLocatorRgbaUInt16 source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Rotate90cw(ViewLocatorRgbaUInt32 source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble Rotate90cw(ViewLocatorRgbaDouble source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``ImageHlsByte Rotate90cw(ViewLocatorHlsByte source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 Rotate90cw(ViewLocatorHlsUInt16 source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble Rotate90cw(ViewLocatorHlsDouble source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``ImageHsiByte Rotate90cw(ViewLocatorHsiByte source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 Rotate90cw(ViewLocatorHsiUInt16 source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble Rotate90cw(ViewLocatorHsiDouble source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``ImageLabByte Rotate90cw(ViewLocatorLabByte source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 Rotate90cw(ViewLocatorLabUInt16 source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``ImageLabDouble Rotate90cw(ViewLocatorLabDouble source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``ImageXyzByte Rotate90cw(ViewLocatorXyzByte source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 Rotate90cw(ViewLocatorXyzUInt16 source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble Rotate90cw(ViewLocatorXyzDouble source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate90cw*
^^^^^^^^^^^^^^^^^^^

``Image Rotate90cw(View source)``

Rotates an image by 90 degrees in clockwise direction.

The method **Rotate90cw** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``ImageByte Rotate180(ViewLocatorByte source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``ImageUInt16 Rotate180(ViewLocatorUInt16 source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``ImageUInt32 Rotate180(ViewLocatorUInt32 source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``ImageDouble Rotate180(ViewLocatorDouble source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``ImageRgbByte Rotate180(ViewLocatorRgbByte source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 Rotate180(ViewLocatorRgbUInt16 source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 Rotate180(ViewLocatorRgbUInt32 source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``ImageRgbDouble Rotate180(ViewLocatorRgbDouble source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``ImageRgbaByte Rotate180(ViewLocatorRgbaByte source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Rotate180(ViewLocatorRgbaUInt16 source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Rotate180(ViewLocatorRgbaUInt32 source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble Rotate180(ViewLocatorRgbaDouble source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``ImageHlsByte Rotate180(ViewLocatorHlsByte source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 Rotate180(ViewLocatorHlsUInt16 source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``ImageHlsDouble Rotate180(ViewLocatorHlsDouble source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``ImageHsiByte Rotate180(ViewLocatorHsiByte source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 Rotate180(ViewLocatorHsiUInt16 source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``ImageHsiDouble Rotate180(ViewLocatorHsiDouble source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``ImageLabByte Rotate180(ViewLocatorLabByte source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 Rotate180(ViewLocatorLabUInt16 source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``ImageLabDouble Rotate180(ViewLocatorLabDouble source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``ImageXyzByte Rotate180(ViewLocatorXyzByte source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 Rotate180(ViewLocatorXyzUInt16 source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``ImageXyzDouble Rotate180(ViewLocatorXyzDouble source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate180*
^^^^^^^^^^^^^^^^^^

``Image Rotate180(View source)``

Rotates an image by 180 degrees.

The method **Rotate180** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``ImageByte Rotate90ccw(ViewLocatorByte source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 Rotate90ccw(ViewLocatorUInt16 source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 Rotate90ccw(ViewLocatorUInt32 source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``ImageDouble Rotate90ccw(ViewLocatorDouble source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte Rotate90ccw(ViewLocatorRgbByte source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 Rotate90ccw(ViewLocatorRgbUInt16 source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 Rotate90ccw(ViewLocatorRgbUInt32 source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble Rotate90ccw(ViewLocatorRgbDouble source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte Rotate90ccw(ViewLocatorRgbaByte source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Rotate90ccw(ViewLocatorRgbaUInt16 source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Rotate90ccw(ViewLocatorRgbaUInt32 source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble Rotate90ccw(ViewLocatorRgbaDouble source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte Rotate90ccw(ViewLocatorHlsByte source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 Rotate90ccw(ViewLocatorHlsUInt16 source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble Rotate90ccw(ViewLocatorHlsDouble source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte Rotate90ccw(ViewLocatorHsiByte source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 Rotate90ccw(ViewLocatorHsiUInt16 source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble Rotate90ccw(ViewLocatorHsiDouble source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``ImageLabByte Rotate90ccw(ViewLocatorLabByte source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 Rotate90ccw(ViewLocatorLabUInt16 source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble Rotate90ccw(ViewLocatorLabDouble source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte Rotate90ccw(ViewLocatorXyzByte source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 Rotate90ccw(ViewLocatorXyzUInt16 source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble Rotate90ccw(ViewLocatorXyzDouble source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

/return The new image.

Method *Rotate90ccw*
^^^^^^^^^^^^^^^^^^^^

``Image Rotate90ccw(View source)``

Rotates an image by 90 degrees in counter-clockwise direction.

The method **Rotate90ccw** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``ImageByte Rescale1d(ViewLocatorByte source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorByte``                      |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``ImageUInt16 Rescale1d(ViewLocatorUInt16 source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorUInt16``                    |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``ImageUInt32 Rescale1d(ViewLocatorUInt32 source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorUInt32``                    |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``ImageDouble Rescale1d(ViewLocatorDouble source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorDouble``                    |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``ImageRgbByte Rescale1d(ViewLocatorRgbByte source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorRgbByte``                   |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 Rescale1d(ViewLocatorRgbUInt16 source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorRgbUInt16``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 Rescale1d(ViewLocatorRgbUInt32 source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorRgbUInt32``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``ImageRgbDouble Rescale1d(ViewLocatorRgbDouble source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorRgbDouble``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``ImageRgbaByte Rescale1d(ViewLocatorRgbaByte source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorRgbaByte``                  |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Rescale1d(ViewLocatorRgbaUInt16 source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorRgbaUInt16``                |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Rescale1d(ViewLocatorRgbaUInt32 source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorRgbaUInt32``                |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble Rescale1d(ViewLocatorRgbaDouble source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorRgbaDouble``                |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``ImageHlsByte Rescale1d(ViewLocatorHlsByte source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorHlsByte``                   |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 Rescale1d(ViewLocatorHlsUInt16 source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorHlsUInt16``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``ImageHlsDouble Rescale1d(ViewLocatorHlsDouble source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorHlsDouble``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``ImageHsiByte Rescale1d(ViewLocatorHsiByte source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorHsiByte``                   |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 Rescale1d(ViewLocatorHsiUInt16 source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorHsiUInt16``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``ImageHsiDouble Rescale1d(ViewLocatorHsiDouble source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorHsiDouble``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``ImageLabByte Rescale1d(ViewLocatorLabByte source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorLabByte``                   |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 Rescale1d(ViewLocatorLabUInt16 source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorLabUInt16``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``ImageLabDouble Rescale1d(ViewLocatorLabDouble source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorLabDouble``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``ImageXyzByte Rescale1d(ViewLocatorXyzByte source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorXyzByte``                   |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 Rescale1d(ViewLocatorXyzUInt16 source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorXyzUInt16``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``ImageXyzDouble Rescale1d(ViewLocatorXyzDouble source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorXyzDouble``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale1d*
^^^^^^^^^^^^^^^^^^

``Image Rescale1d(View source, System.Double xFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal dimension by an arbitrary scale factor.

The method **Rescale1d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``View``                                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``ImageByte Rescale2d(ViewLocatorByte source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorByte``                      |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``ImageUInt16 Rescale2d(ViewLocatorUInt16 source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorUInt16``                    |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``ImageUInt32 Rescale2d(ViewLocatorUInt32 source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorUInt32``                    |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``ImageDouble Rescale2d(ViewLocatorDouble source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorDouble``                    |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``ImageRgbByte Rescale2d(ViewLocatorRgbByte source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorRgbByte``                   |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 Rescale2d(ViewLocatorRgbUInt16 source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorRgbUInt16``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 Rescale2d(ViewLocatorRgbUInt32 source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorRgbUInt32``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``ImageRgbDouble Rescale2d(ViewLocatorRgbDouble source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorRgbDouble``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``ImageRgbaByte Rescale2d(ViewLocatorRgbaByte source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorRgbaByte``                  |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Rescale2d(ViewLocatorRgbaUInt16 source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorRgbaUInt16``                |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Rescale2d(ViewLocatorRgbaUInt32 source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorRgbaUInt32``                |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble Rescale2d(ViewLocatorRgbaDouble source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorRgbaDouble``                |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``ImageHlsByte Rescale2d(ViewLocatorHlsByte source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorHlsByte``                   |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 Rescale2d(ViewLocatorHlsUInt16 source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorHlsUInt16``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``ImageHlsDouble Rescale2d(ViewLocatorHlsDouble source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorHlsDouble``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``ImageHsiByte Rescale2d(ViewLocatorHsiByte source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorHsiByte``                   |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 Rescale2d(ViewLocatorHsiUInt16 source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorHsiUInt16``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``ImageHsiDouble Rescale2d(ViewLocatorHsiDouble source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorHsiDouble``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``ImageLabByte Rescale2d(ViewLocatorLabByte source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorLabByte``                   |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 Rescale2d(ViewLocatorLabUInt16 source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorLabUInt16``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``ImageLabDouble Rescale2d(ViewLocatorLabDouble source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorLabDouble``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``ImageXyzByte Rescale2d(ViewLocatorXyzByte source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorXyzByte``                   |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 Rescale2d(ViewLocatorXyzUInt16 source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorXyzUInt16``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``ImageXyzDouble Rescale2d(ViewLocatorXyzDouble source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorXyzDouble``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale2d*
^^^^^^^^^^^^^^^^^^

``Image Rescale2d(View source, System.Double xFactor, System.Double yFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal and vertical dimensions by arbitrary scale factors.

The method **Rescale2d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``View``                                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``ImageByte Rescale3d(ViewLocatorByte source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorByte``                      |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``ImageUInt16 Rescale3d(ViewLocatorUInt16 source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorUInt16``                    |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``ImageUInt32 Rescale3d(ViewLocatorUInt32 source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorUInt32``                    |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``ImageDouble Rescale3d(ViewLocatorDouble source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorDouble``                    |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``ImageRgbByte Rescale3d(ViewLocatorRgbByte source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorRgbByte``                   |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 Rescale3d(ViewLocatorRgbUInt16 source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorRgbUInt16``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 Rescale3d(ViewLocatorRgbUInt32 source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorRgbUInt32``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``ImageRgbDouble Rescale3d(ViewLocatorRgbDouble source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorRgbDouble``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``ImageRgbaByte Rescale3d(ViewLocatorRgbaByte source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorRgbaByte``                  |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Rescale3d(ViewLocatorRgbaUInt16 source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorRgbaUInt16``                |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Rescale3d(ViewLocatorRgbaUInt32 source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorRgbaUInt32``                |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble Rescale3d(ViewLocatorRgbaDouble source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorRgbaDouble``                |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``ImageHlsByte Rescale3d(ViewLocatorHlsByte source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorHlsByte``                   |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 Rescale3d(ViewLocatorHlsUInt16 source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorHlsUInt16``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``ImageHlsDouble Rescale3d(ViewLocatorHlsDouble source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorHlsDouble``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``ImageHsiByte Rescale3d(ViewLocatorHsiByte source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorHsiByte``                   |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 Rescale3d(ViewLocatorHsiUInt16 source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorHsiUInt16``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``ImageHsiDouble Rescale3d(ViewLocatorHsiDouble source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorHsiDouble``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``ImageLabByte Rescale3d(ViewLocatorLabByte source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorLabByte``                   |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 Rescale3d(ViewLocatorLabUInt16 source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorLabUInt16``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``ImageLabDouble Rescale3d(ViewLocatorLabDouble source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorLabDouble``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``ImageXyzByte Rescale3d(ViewLocatorXyzByte source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorXyzByte``                   |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 Rescale3d(ViewLocatorXyzUInt16 source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorXyzUInt16``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``ImageXyzDouble Rescale3d(ViewLocatorXyzDouble source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``ViewLocatorXyzDouble``                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Rescale3d*
^^^^^^^^^^^^^^^^^^

``Image Rescale3d(View source, System.Double xFactor, System.Double yFactor, System.Double zFactor, GeometryAlgorithms.GeometricFilter filter)``

Resize in horizontal, vertical and planar dimensions by arbitrary scale factors.

The method **Rescale3d** has the following parameters:

+---------------+------------------------------------------+---------------+
| Parameter     | Type                                     | Description   |
+===============+==========================================+===============+
| ``source``    | ``View``                                 |               |
+---------------+------------------------------------------+---------------+
| ``xFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``yFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``zFactor``   | ``System.Double``                        |               |
+---------------+------------------------------------------+---------------+
| ``filter``    | ``GeometryAlgorithms.GeometricFilter``   |               |
+---------------+------------------------------------------+---------------+

The resampling engine can resample the source view to an arbitrary output size, using one of several filters.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``ImageByte Translate(ViewLocatorByte source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.Byte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorByte``                              |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.Byte``                                  |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``ImageUInt16 Translate(ViewLocatorUInt16 source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.UInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorUInt16``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.UInt16``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``ImageUInt32 Translate(ViewLocatorUInt32 source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.UInt32 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorUInt32``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.UInt32``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``ImageDouble Translate(ViewLocatorDouble source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.Double extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorDouble``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``ImageRgbByte Translate(ViewLocatorRgbByte source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 Translate(ViewLocatorRgbUInt16 source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 Translate(ViewLocatorRgbUInt32 source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbUInt32 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbUInt32``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbUInt32``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``ImageRgbDouble Translate(ViewLocatorRgbDouble source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``ImageRgbaByte Translate(ViewLocatorRgbaByte source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaByte``                          |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaByte``                                     |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Translate(ViewLocatorRgbaUInt16 source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaUInt16``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaUInt16``                                   |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Translate(ViewLocatorRgbaUInt32 source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaUInt32 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaUInt32``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaUInt32``                                   |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble Translate(ViewLocatorRgbaDouble source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaDouble``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaDouble``                                   |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``ImageHlsByte Translate(ViewLocatorHlsByte source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HlsByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHlsByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HlsByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 Translate(ViewLocatorHlsUInt16 source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HlsUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHlsUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HlsUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``ImageHlsDouble Translate(ViewLocatorHlsDouble source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HlsDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHlsDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HlsDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``ImageHsiByte Translate(ViewLocatorHsiByte source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HsiByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHsiByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HsiByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 Translate(ViewLocatorHsiUInt16 source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HsiUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHsiUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HsiUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``ImageHsiDouble Translate(ViewLocatorHsiDouble source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HsiDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHsiDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HsiDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``ImageLabByte Translate(ViewLocatorLabByte source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, LabByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorLabByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``LabByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 Translate(ViewLocatorLabUInt16 source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, LabUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorLabUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``LabUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``ImageLabDouble Translate(ViewLocatorLabDouble source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, LabDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorLabDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``LabDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``ImageXyzByte Translate(ViewLocatorXyzByte source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, XyzByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorXyzByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``XyzByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 Translate(ViewLocatorXyzUInt16 source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, XyzUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorXyzUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``XyzUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``ImageXyzDouble Translate(ViewLocatorXyzDouble source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, XyzDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorXyzDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``XyzDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``Image Translate(View source, VectorDouble translation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, object extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Translate an image according to a translation vector.

The method **Translate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``View``                                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``translation``      | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``object``                                       |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The destination image size is optionally adapted, so that the translated image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``ImageByte Scale(ViewLocatorByte source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.Byte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorByte``                              |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.Byte``                                  |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``ImageUInt16 Scale(ViewLocatorUInt16 source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.UInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorUInt16``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.UInt16``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``ImageUInt32 Scale(ViewLocatorUInt32 source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.UInt32 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorUInt32``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.UInt32``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``ImageDouble Scale(ViewLocatorDouble source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.Double extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorDouble``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``ImageRgbByte Scale(ViewLocatorRgbByte source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``ImageRgbUInt16 Scale(ViewLocatorRgbUInt16 source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``ImageRgbUInt32 Scale(ViewLocatorRgbUInt32 source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbUInt32 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbUInt32``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbUInt32``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``ImageRgbDouble Scale(ViewLocatorRgbDouble source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``ImageRgbaByte Scale(ViewLocatorRgbaByte source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaByte``                          |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaByte``                                     |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``ImageRgbaUInt16 Scale(ViewLocatorRgbaUInt16 source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaUInt16``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaUInt16``                                   |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``ImageRgbaUInt32 Scale(ViewLocatorRgbaUInt32 source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaUInt32 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaUInt32``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaUInt32``                                   |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``ImageRgbaDouble Scale(ViewLocatorRgbaDouble source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaDouble``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaDouble``                                   |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``ImageHlsByte Scale(ViewLocatorHlsByte source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HlsByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHlsByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HlsByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``ImageHlsUInt16 Scale(ViewLocatorHlsUInt16 source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HlsUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHlsUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HlsUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``ImageHlsDouble Scale(ViewLocatorHlsDouble source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HlsDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHlsDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HlsDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``ImageHsiByte Scale(ViewLocatorHsiByte source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HsiByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHsiByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HsiByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``ImageHsiUInt16 Scale(ViewLocatorHsiUInt16 source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HsiUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHsiUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HsiUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``ImageHsiDouble Scale(ViewLocatorHsiDouble source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HsiDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHsiDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HsiDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``ImageLabByte Scale(ViewLocatorLabByte source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, LabByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorLabByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``LabByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``ImageLabUInt16 Scale(ViewLocatorLabUInt16 source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, LabUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorLabUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``LabUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``ImageLabDouble Scale(ViewLocatorLabDouble source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, LabDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorLabDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``LabDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``ImageXyzByte Scale(ViewLocatorXyzByte source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, XyzByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorXyzByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``XyzByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``ImageXyzUInt16 Scale(ViewLocatorXyzUInt16 source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, XyzUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorXyzUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``XyzUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``ImageXyzDouble Scale(ViewLocatorXyzDouble source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, XyzDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorXyzDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``XyzDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Scale*
^^^^^^^^^^^^^^

``Image Scale(View source, VectorDouble scaling, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, object extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Scale an image according to a scaling vector.

The method **Scale** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``View``                                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``scaling``          | ``VectorDouble``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``object``                                       |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The scaling occurs around the image center. The destination image size is optionally adapted, so that the scaled image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``ImageByte Rotate(ViewLocatorByte source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.Byte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorByte``                              |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.Byte``                                  |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``ImageUInt16 Rotate(ViewLocatorUInt16 source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.UInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorUInt16``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.UInt16``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``ImageUInt32 Rotate(ViewLocatorUInt32 source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.UInt32 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorUInt32``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.UInt32``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``ImageDouble Rotate(ViewLocatorDouble source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.Double extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorDouble``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``ImageRgbByte Rotate(ViewLocatorRgbByte source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``ImageRgbUInt16 Rotate(ViewLocatorRgbUInt16 source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``ImageRgbUInt32 Rotate(ViewLocatorRgbUInt32 source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbUInt32 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbUInt32``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbUInt32``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``ImageRgbDouble Rotate(ViewLocatorRgbDouble source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``ImageRgbaByte Rotate(ViewLocatorRgbaByte source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaByte``                          |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaByte``                                     |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Rotate(ViewLocatorRgbaUInt16 source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaUInt16``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaUInt16``                                   |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Rotate(ViewLocatorRgbaUInt32 source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaUInt32 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaUInt32``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaUInt32``                                   |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``ImageRgbaDouble Rotate(ViewLocatorRgbaDouble source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaDouble``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaDouble``                                   |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``ImageHlsByte Rotate(ViewLocatorHlsByte source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HlsByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHlsByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HlsByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``ImageHlsUInt16 Rotate(ViewLocatorHlsUInt16 source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HlsUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHlsUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HlsUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``ImageHlsDouble Rotate(ViewLocatorHlsDouble source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HlsDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHlsDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HlsDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``ImageHsiByte Rotate(ViewLocatorHsiByte source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HsiByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHsiByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HsiByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``ImageHsiUInt16 Rotate(ViewLocatorHsiUInt16 source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HsiUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHsiUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HsiUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``ImageHsiDouble Rotate(ViewLocatorHsiDouble source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HsiDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHsiDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HsiDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``ImageLabByte Rotate(ViewLocatorLabByte source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, LabByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorLabByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``LabByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``ImageLabUInt16 Rotate(ViewLocatorLabUInt16 source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, LabUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorLabUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``LabUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``ImageLabDouble Rotate(ViewLocatorLabDouble source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, LabDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorLabDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``LabDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``ImageXyzByte Rotate(ViewLocatorXyzByte source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, XyzByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorXyzByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``XyzByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``ImageXyzUInt16 Rotate(ViewLocatorXyzUInt16 source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, XyzUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorXyzUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``XyzUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``ImageXyzDouble Rotate(ViewLocatorXyzDouble source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, XyzDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorXyzDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``XyzDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *Rotate*
^^^^^^^^^^^^^^^

``Image Rotate(View source, System.Double rotation, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, object extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Rotate an image according to a rotation angle.

The method **Rotate** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``View``                                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``rotation``         | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``object``                                       |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

The rotation occurs around the image center. The destination image size is optionally adapted, so that the rotated image fits into it.

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte AffineResample(ViewLocatorByte source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.Byte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorByte``                              |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.Byte``                                  |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 AffineResample(ViewLocatorUInt16 source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.UInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorUInt16``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.UInt16``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 AffineResample(ViewLocatorUInt32 source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.UInt32 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorUInt32``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.UInt32``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble AffineResample(ViewLocatorDouble source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.Double extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorDouble``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte AffineResample(ViewLocatorRgbByte source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 AffineResample(ViewLocatorRgbUInt16 source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 AffineResample(ViewLocatorRgbUInt32 source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbUInt32 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbUInt32``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbUInt32``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble AffineResample(ViewLocatorRgbDouble source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte AffineResample(ViewLocatorRgbaByte source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaByte``                          |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaByte``                                     |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 AffineResample(ViewLocatorRgbaUInt16 source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaUInt16``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaUInt16``                                   |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 AffineResample(ViewLocatorRgbaUInt32 source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaUInt32 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaUInt32``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaUInt32``                                   |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble AffineResample(ViewLocatorRgbaDouble source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaDouble``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaDouble``                                   |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte AffineResample(ViewLocatorHlsByte source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HlsByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHlsByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HlsByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 AffineResample(ViewLocatorHlsUInt16 source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HlsUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHlsUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HlsUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble AffineResample(ViewLocatorHlsDouble source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HlsDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHlsDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HlsDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte AffineResample(ViewLocatorHsiByte source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HsiByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHsiByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HsiByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 AffineResample(ViewLocatorHsiUInt16 source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HsiUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHsiUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HsiUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble AffineResample(ViewLocatorHsiDouble source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HsiDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHsiDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HsiDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte AffineResample(ViewLocatorLabByte source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, LabByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorLabByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``LabByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 AffineResample(ViewLocatorLabUInt16 source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, LabUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorLabUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``LabUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble AffineResample(ViewLocatorLabDouble source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, LabDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorLabDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``LabDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte AffineResample(ViewLocatorXyzByte source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, XyzByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorXyzByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``XyzByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 AffineResample(ViewLocatorXyzUInt16 source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, XyzUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorXyzUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``XyzUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble AffineResample(ViewLocatorXyzDouble source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, XyzDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorXyzDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``XyzDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``Image AffineResample(View source, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, object extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Affine transform of an image according to an affine transformation matrix.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``View``                                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``object``                                       |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(ViewLocatorByte source, ViewLocatorByte destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.Byte extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorByte``                              |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorByte``                              |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.Byte``                                  |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(ViewLocatorUInt16 source, ViewLocatorUInt16 destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.UInt16 extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorUInt16``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorUInt16``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.UInt16``                                |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(ViewLocatorUInt32 source, ViewLocatorUInt32 destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.UInt32 extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorUInt32``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorUInt32``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.UInt32``                                |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(ViewLocatorDouble source, ViewLocatorDouble destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.Double extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorDouble``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorDouble``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(ViewLocatorRgbByte source, ViewLocatorRgbByte destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbByte extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorRgbByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(ViewLocatorRgbUInt16 source, ViewLocatorRgbUInt16 destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbUInt16 extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorRgbUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(ViewLocatorRgbUInt32 source, ViewLocatorRgbUInt32 destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbUInt32 extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbUInt32``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorRgbUInt32``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbUInt32``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(ViewLocatorRgbDouble source, ViewLocatorRgbDouble destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbDouble extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorRgbDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(ViewLocatorRgbaByte source, ViewLocatorRgbaByte destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaByte extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaByte``                          |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorRgbaByte``                          |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaByte``                                     |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(ViewLocatorRgbaUInt16 source, ViewLocatorRgbaUInt16 destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaUInt16 extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaUInt16``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorRgbaUInt16``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaUInt16``                                   |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(ViewLocatorRgbaUInt32 source, ViewLocatorRgbaUInt32 destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaUInt32 extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaUInt32``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorRgbaUInt32``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaUInt32``                                   |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(ViewLocatorRgbaDouble source, ViewLocatorRgbaDouble destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaDouble extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaDouble``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorRgbaDouble``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaDouble``                                   |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(ViewLocatorHlsByte source, ViewLocatorHlsByte destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HlsByte extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHlsByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorHlsByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HlsByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(ViewLocatorHlsUInt16 source, ViewLocatorHlsUInt16 destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HlsUInt16 extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHlsUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorHlsUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HlsUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(ViewLocatorHlsDouble source, ViewLocatorHlsDouble destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HlsDouble extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHlsDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorHlsDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HlsDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(ViewLocatorHsiByte source, ViewLocatorHsiByte destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HsiByte extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHsiByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorHsiByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HsiByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(ViewLocatorHsiUInt16 source, ViewLocatorHsiUInt16 destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HsiUInt16 extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHsiUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorHsiUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HsiUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(ViewLocatorHsiDouble source, ViewLocatorHsiDouble destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HsiDouble extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHsiDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorHsiDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HsiDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(ViewLocatorLabByte source, ViewLocatorLabByte destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, LabByte extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorLabByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorLabByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``LabByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(ViewLocatorLabUInt16 source, ViewLocatorLabUInt16 destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, LabUInt16 extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorLabUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorLabUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``LabUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(ViewLocatorLabDouble source, ViewLocatorLabDouble destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, LabDouble extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorLabDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorLabDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``LabDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(ViewLocatorXyzByte source, ViewLocatorXyzByte destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, XyzByte extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorXyzByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorXyzByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``XyzByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(ViewLocatorXyzUInt16 source, ViewLocatorXyzUInt16 destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, XyzUInt16 extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorXyzUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorXyzUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``XyzUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(ViewLocatorXyzDouble source, ViewLocatorXyzDouble destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, XyzDouble extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorXyzDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorXyzDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``XyzDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *AffineResample*
^^^^^^^^^^^^^^^^^^^^^^^

``void AffineResample(View source, View destination, AffineMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, object extensionValue)``

Affine transform of an image according to an affine transformation matrix into a destination view.

The method **AffineResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``View``                                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``View``                                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``AffineMatrix``                                 |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``object``                                       |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte ResampleRectangle(ViewLocatorByte source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.Byte extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorByte``                              |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.Byte``                                  |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 ResampleRectangle(ViewLocatorUInt16 source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.UInt16 extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorUInt16``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.UInt16``                                |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 ResampleRectangle(ViewLocatorUInt32 source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.UInt32 extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorUInt32``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.UInt32``                                |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble ResampleRectangle(ViewLocatorDouble source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.Double extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorDouble``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte ResampleRectangle(ViewLocatorRgbByte source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbByte extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 ResampleRectangle(ViewLocatorRgbUInt16 source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbUInt16 extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 ResampleRectangle(ViewLocatorRgbUInt32 source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbUInt32 extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbUInt32``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbUInt32``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble ResampleRectangle(ViewLocatorRgbDouble source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbDouble extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte ResampleRectangle(ViewLocatorRgbaByte source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaByte extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaByte``                          |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaByte``                                     |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 ResampleRectangle(ViewLocatorRgbaUInt16 source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaUInt16 extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaUInt16``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaUInt16``                                   |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 ResampleRectangle(ViewLocatorRgbaUInt32 source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaUInt32 extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaUInt32``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaUInt32``                                   |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble ResampleRectangle(ViewLocatorRgbaDouble source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaDouble extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaDouble``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaDouble``                                   |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte ResampleRectangle(ViewLocatorHlsByte source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HlsByte extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHlsByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HlsByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 ResampleRectangle(ViewLocatorHlsUInt16 source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HlsUInt16 extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHlsUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HlsUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble ResampleRectangle(ViewLocatorHlsDouble source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HlsDouble extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHlsDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HlsDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte ResampleRectangle(ViewLocatorHsiByte source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HsiByte extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHsiByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HsiByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 ResampleRectangle(ViewLocatorHsiUInt16 source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HsiUInt16 extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHsiUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HsiUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble ResampleRectangle(ViewLocatorHsiDouble source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HsiDouble extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHsiDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HsiDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte ResampleRectangle(ViewLocatorLabByte source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, LabByte extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorLabByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``LabByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 ResampleRectangle(ViewLocatorLabUInt16 source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, LabUInt16 extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorLabUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``LabUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble ResampleRectangle(ViewLocatorLabDouble source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, LabDouble extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorLabDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``LabDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte ResampleRectangle(ViewLocatorXyzByte source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, XyzByte extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorXyzByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``XyzByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 ResampleRectangle(ViewLocatorXyzUInt16 source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, XyzUInt16 extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorXyzUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``XyzUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble ResampleRectangle(ViewLocatorXyzDouble source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, XyzDouble extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorXyzDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``XyzDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *ResampleRectangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``Image ResampleRectangle(View source, Rectangle rectangle, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, object extensionValue)``

Affine transform of a rectangular portion of an image.

The method **ResampleRectangle** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``View``                                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``rectangle``        | ``Rectangle``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``object``                                       |               |
+----------------------+--------------------------------------------------+---------------+

The rectangular portion is specified with a rectangle. The size of the destination image is equal to the size of the rectangle (subject to rounding). The function calculates an affine transformation matrix and carries out the transformation.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte PerspectiveResample(ViewLocatorByte source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.Byte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorByte``                              |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.Byte``                                  |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 PerspectiveResample(ViewLocatorUInt16 source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.UInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorUInt16``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.UInt16``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 PerspectiveResample(ViewLocatorUInt32 source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.UInt32 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorUInt32``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.UInt32``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble PerspectiveResample(ViewLocatorDouble source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.Double extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorDouble``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte PerspectiveResample(ViewLocatorRgbByte source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 PerspectiveResample(ViewLocatorRgbUInt16 source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 PerspectiveResample(ViewLocatorRgbUInt32 source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbUInt32 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbUInt32``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbUInt32``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble PerspectiveResample(ViewLocatorRgbDouble source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte PerspectiveResample(ViewLocatorRgbaByte source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaByte``                          |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaByte``                                     |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 PerspectiveResample(ViewLocatorRgbaUInt16 source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaUInt16``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaUInt16``                                   |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 PerspectiveResample(ViewLocatorRgbaUInt32 source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaUInt32 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaUInt32``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaUInt32``                                   |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble PerspectiveResample(ViewLocatorRgbaDouble source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaDouble``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaDouble``                                   |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte PerspectiveResample(ViewLocatorHlsByte source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HlsByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHlsByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HlsByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 PerspectiveResample(ViewLocatorHlsUInt16 source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HlsUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHlsUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HlsUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble PerspectiveResample(ViewLocatorHlsDouble source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HlsDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHlsDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HlsDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte PerspectiveResample(ViewLocatorHsiByte source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HsiByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHsiByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HsiByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 PerspectiveResample(ViewLocatorHsiUInt16 source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HsiUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHsiUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HsiUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble PerspectiveResample(ViewLocatorHsiDouble source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HsiDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHsiDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HsiDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte PerspectiveResample(ViewLocatorLabByte source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, LabByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorLabByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``LabByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 PerspectiveResample(ViewLocatorLabUInt16 source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, LabUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorLabUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``LabUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble PerspectiveResample(ViewLocatorLabDouble source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, LabDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorLabDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``LabDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte PerspectiveResample(ViewLocatorXyzByte source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, XyzByte extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorXyzByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``XyzByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 PerspectiveResample(ViewLocatorXyzUInt16 source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, XyzUInt16 extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorXyzUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``XyzUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble PerspectiveResample(ViewLocatorXyzDouble source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, XyzDouble extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorXyzDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``XyzDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Image PerspectiveResample(View source, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, object extensionValue, GeometryAlgorithms.ResampleBoundsExtension bounds)``

Perspective transform of an image according to a perspective transformation matrix.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``View``                                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``object``                                       |               |
+----------------------+--------------------------------------------------+---------------+
| ``bounds``           | ``GeometryAlgorithms.ResampleBoundsExtension``   |               |
+----------------------+--------------------------------------------------+---------------+

Depending on the value of the bounds parameter the destination image size is adapted so that the transformed image fits into it (extend), or kept the same as the original image (keep\_size).

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(ViewLocatorByte source, ViewLocatorByte destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.Byte extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorByte``                              |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorByte``                              |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.Byte``                                  |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(ViewLocatorUInt16 source, ViewLocatorUInt16 destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.UInt16 extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorUInt16``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorUInt16``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.UInt16``                                |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(ViewLocatorUInt32 source, ViewLocatorUInt32 destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.UInt32 extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorUInt32``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorUInt32``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.UInt32``                                |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(ViewLocatorDouble source, ViewLocatorDouble destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, System.Double extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorDouble``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorDouble``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``System.Double``                                |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(ViewLocatorRgbByte source, ViewLocatorRgbByte destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbByte extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorRgbByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(ViewLocatorRgbUInt16 source, ViewLocatorRgbUInt16 destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbUInt16 extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorRgbUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(ViewLocatorRgbUInt32 source, ViewLocatorRgbUInt32 destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbUInt32 extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbUInt32``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorRgbUInt32``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbUInt32``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(ViewLocatorRgbDouble source, ViewLocatorRgbDouble destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbDouble extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorRgbDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(ViewLocatorRgbaByte source, ViewLocatorRgbaByte destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaByte extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaByte``                          |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorRgbaByte``                          |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaByte``                                     |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(ViewLocatorRgbaUInt16 source, ViewLocatorRgbaUInt16 destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaUInt16 extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaUInt16``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorRgbaUInt16``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaUInt16``                                   |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(ViewLocatorRgbaUInt32 source, ViewLocatorRgbaUInt32 destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaUInt32 extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaUInt32``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorRgbaUInt32``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaUInt32``                                   |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(ViewLocatorRgbaDouble source, ViewLocatorRgbaDouble destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, RgbaDouble extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorRgbaDouble``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorRgbaDouble``                        |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``RgbaDouble``                                   |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(ViewLocatorHlsByte source, ViewLocatorHlsByte destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HlsByte extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHlsByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorHlsByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HlsByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(ViewLocatorHlsUInt16 source, ViewLocatorHlsUInt16 destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HlsUInt16 extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHlsUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorHlsUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HlsUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(ViewLocatorHlsDouble source, ViewLocatorHlsDouble destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HlsDouble extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHlsDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorHlsDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HlsDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(ViewLocatorHsiByte source, ViewLocatorHsiByte destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HsiByte extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHsiByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorHsiByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HsiByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(ViewLocatorHsiUInt16 source, ViewLocatorHsiUInt16 destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HsiUInt16 extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHsiUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorHsiUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HsiUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(ViewLocatorHsiDouble source, ViewLocatorHsiDouble destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, HsiDouble extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorHsiDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorHsiDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``HsiDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(ViewLocatorLabByte source, ViewLocatorLabByte destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, LabByte extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorLabByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorLabByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``LabByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(ViewLocatorLabUInt16 source, ViewLocatorLabUInt16 destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, LabUInt16 extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorLabUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorLabUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``LabUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(ViewLocatorLabDouble source, ViewLocatorLabDouble destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, LabDouble extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorLabDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorLabDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``LabDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(ViewLocatorXyzByte source, ViewLocatorXyzByte destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, XyzByte extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorXyzByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorXyzByte``                           |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``XyzByte``                                      |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(ViewLocatorXyzUInt16 source, ViewLocatorXyzUInt16 destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, XyzUInt16 extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorXyzUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorXyzUInt16``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``XyzUInt16``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(ViewLocatorXyzDouble source, ViewLocatorXyzDouble destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, XyzDouble extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``ViewLocatorXyzDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``ViewLocatorXyzDouble``                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``XyzDouble``                                    |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *PerspectiveResample*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void PerspectiveResample(View source, View destination, PerspectiveMatrix backward, GeometryAlgorithms.GeometricFilter filter, GeometryAlgorithms.ResampleBorderTreatment border, object extensionValue)``

Perspective transform of an image according to a perspective transformation matrix into a destination view.

The method **PerspectiveResample** has the following parameters:

+----------------------+--------------------------------------------------+---------------+
| Parameter            | Type                                             | Description   |
+======================+==================================================+===============+
| ``source``           | ``View``                                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``destination``      | ``View``                                         |               |
+----------------------+--------------------------------------------------+---------------+
| ``backward``         | ``PerspectiveMatrix``                            |               |
+----------------------+--------------------------------------------------+---------------+
| ``filter``           | ``GeometryAlgorithms.GeometricFilter``           |               |
+----------------------+--------------------------------------------------+---------------+
| ``border``           | ``GeometryAlgorithms.ResampleBorderTreatment``   |               |
+----------------------+--------------------------------------------------+---------------+
| ``extensionValue``   | ``object``                                       |               |
+----------------------+--------------------------------------------------+---------------+

The destination is located at (0,0) parallel to the coordinate system axes.

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(ViewLocatorByte source, ViewLocatorByte destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorByte``                      |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorByte``                      |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(ViewLocatorUInt16 source, ViewLocatorUInt16 destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorUInt16``                    |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorUInt16``                    |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(ViewLocatorUInt32 source, ViewLocatorUInt32 destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorUInt32``                    |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorUInt32``                    |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(ViewLocatorDouble source, ViewLocatorDouble destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorDouble``                    |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorDouble``                    |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(ViewLocatorRgbByte source, ViewLocatorRgbByte destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorRgbByte``                   |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorRgbByte``                   |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(ViewLocatorRgbUInt16 source, ViewLocatorRgbUInt16 destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorRgbUInt16``                 |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorRgbUInt16``                 |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(ViewLocatorRgbUInt32 source, ViewLocatorRgbUInt32 destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorRgbUInt32``                 |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorRgbUInt32``                 |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(ViewLocatorRgbDouble source, ViewLocatorRgbDouble destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorRgbDouble``                 |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorRgbDouble``                 |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(ViewLocatorRgbaByte source, ViewLocatorRgbaByte destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorRgbaByte``                  |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorRgbaByte``                  |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(ViewLocatorRgbaUInt16 source, ViewLocatorRgbaUInt16 destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorRgbaUInt16``                |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorRgbaUInt16``                |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(ViewLocatorRgbaUInt32 source, ViewLocatorRgbaUInt32 destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorRgbaUInt32``                |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorRgbaUInt32``                |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(ViewLocatorRgbaDouble source, ViewLocatorRgbaDouble destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorRgbaDouble``                |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorRgbaDouble``                |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(ViewLocatorHlsByte source, ViewLocatorHlsByte destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorHlsByte``                   |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorHlsByte``                   |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(ViewLocatorHlsUInt16 source, ViewLocatorHlsUInt16 destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorHlsUInt16``                 |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorHlsUInt16``                 |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(ViewLocatorHlsDouble source, ViewLocatorHlsDouble destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorHlsDouble``                 |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorHlsDouble``                 |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(ViewLocatorHsiByte source, ViewLocatorHsiByte destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorHsiByte``                   |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorHsiByte``                   |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(ViewLocatorHsiUInt16 source, ViewLocatorHsiUInt16 destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorHsiUInt16``                 |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorHsiUInt16``                 |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(ViewLocatorHsiDouble source, ViewLocatorHsiDouble destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorHsiDouble``                 |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorHsiDouble``                 |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(ViewLocatorLabByte source, ViewLocatorLabByte destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorLabByte``                   |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorLabByte``                   |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(ViewLocatorLabUInt16 source, ViewLocatorLabUInt16 destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorLabUInt16``                 |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorLabUInt16``                 |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(ViewLocatorLabDouble source, ViewLocatorLabDouble destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorLabDouble``                 |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorLabDouble``                 |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(ViewLocatorXyzByte source, ViewLocatorXyzByte destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorXyzByte``                   |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorXyzByte``                   |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(ViewLocatorXyzUInt16 source, ViewLocatorXyzUInt16 destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorXyzUInt16``                 |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorXyzUInt16``                 |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(ViewLocatorXyzDouble source, ViewLocatorXyzDouble destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorXyzDouble``                 |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorXyzDouble``                 |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResamplePolar*
^^^^^^^^^^^^^^^^^^^^^^

``void ResamplePolar(View source, View destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResamplePolar** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``View``                                 |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``View``                                 |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(ViewLocatorByte source, ViewLocatorByte destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorByte``                      |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorByte``                      |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(ViewLocatorUInt16 source, ViewLocatorUInt16 destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorUInt16``                    |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorUInt16``                    |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(ViewLocatorUInt32 source, ViewLocatorUInt32 destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorUInt32``                    |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorUInt32``                    |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(ViewLocatorDouble source, ViewLocatorDouble destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorDouble``                    |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorDouble``                    |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(ViewLocatorRgbByte source, ViewLocatorRgbByte destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorRgbByte``                   |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorRgbByte``                   |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(ViewLocatorRgbUInt16 source, ViewLocatorRgbUInt16 destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorRgbUInt16``                 |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorRgbUInt16``                 |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(ViewLocatorRgbUInt32 source, ViewLocatorRgbUInt32 destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorRgbUInt32``                 |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorRgbUInt32``                 |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(ViewLocatorRgbDouble source, ViewLocatorRgbDouble destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorRgbDouble``                 |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorRgbDouble``                 |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(ViewLocatorRgbaByte source, ViewLocatorRgbaByte destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorRgbaByte``                  |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorRgbaByte``                  |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(ViewLocatorRgbaUInt16 source, ViewLocatorRgbaUInt16 destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorRgbaUInt16``                |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorRgbaUInt16``                |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(ViewLocatorRgbaUInt32 source, ViewLocatorRgbaUInt32 destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorRgbaUInt32``                |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorRgbaUInt32``                |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(ViewLocatorRgbaDouble source, ViewLocatorRgbaDouble destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorRgbaDouble``                |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorRgbaDouble``                |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(ViewLocatorHlsByte source, ViewLocatorHlsByte destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorHlsByte``                   |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorHlsByte``                   |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(ViewLocatorHlsUInt16 source, ViewLocatorHlsUInt16 destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorHlsUInt16``                 |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorHlsUInt16``                 |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(ViewLocatorHlsDouble source, ViewLocatorHlsDouble destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorHlsDouble``                 |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorHlsDouble``                 |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(ViewLocatorHsiByte source, ViewLocatorHsiByte destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorHsiByte``                   |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorHsiByte``                   |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(ViewLocatorHsiUInt16 source, ViewLocatorHsiUInt16 destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorHsiUInt16``                 |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorHsiUInt16``                 |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(ViewLocatorHsiDouble source, ViewLocatorHsiDouble destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorHsiDouble``                 |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorHsiDouble``                 |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(ViewLocatorLabByte source, ViewLocatorLabByte destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorLabByte``                   |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorLabByte``                   |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(ViewLocatorLabUInt16 source, ViewLocatorLabUInt16 destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorLabUInt16``                 |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorLabUInt16``                 |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(ViewLocatorLabDouble source, ViewLocatorLabDouble destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorLabDouble``                 |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorLabDouble``                 |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(ViewLocatorXyzByte source, ViewLocatorXyzByte destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorXyzByte``                   |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorXyzByte``                   |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(ViewLocatorXyzUInt16 source, ViewLocatorXyzUInt16 destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorXyzUInt16``                 |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorXyzUInt16``                 |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(ViewLocatorXyzDouble source, ViewLocatorXyzDouble destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``ViewLocatorXyzDouble``                 |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``ViewLocatorXyzDouble``                 |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Method *ResampleCartesian*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ResampleCartesian(View source, View destination, PointDouble center, System.Double lengthOffset, System.Double lengthScale, System.Double angleOffset, System.Double angleScale, GeometryAlgorithms.GeometricFilter filter)``

...

The method **ResampleCartesian** has the following parameters:

+--------------------+------------------------------------------+---------------+
| Parameter          | Type                                     | Description   |
+====================+==========================================+===============+
| ``source``         | ``View``                                 |               |
+--------------------+------------------------------------------+---------------+
| ``destination``    | ``View``                                 |               |
+--------------------+------------------------------------------+---------------+
| ``center``         | ``PointDouble``                          |               |
+--------------------+------------------------------------------+---------------+
| ``lengthOffset``   | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``lengthScale``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleOffset``    | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``angleScale``     | ``System.Double``                        |               |
+--------------------+------------------------------------------+---------------+
| ``filter``         | ``GeometryAlgorithms.GeometricFilter``   |               |
+--------------------+------------------------------------------+---------------+

/return The new image.

Enumerations
~~~~~~~~~~~~

Enumeration *GeometricFilter*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum GeometricFilter``

TODO documentation missing

The enumeration **GeometricFilter** has the following constants:

+-------------------------+---------+---------------+
| Name                    | Value   | Description   |
+=========================+=========+===============+
| ``gfNearestNeighbor``   | ``0``   |               |
+-------------------------+---------+---------------+
| ``gfBoxFilter``         | ``1``   |               |
+-------------------------+---------+---------------+
| ``gfTriangleFilter``    | ``2``   |               |
+-------------------------+---------+---------------+
| ``gfCubicFilter``       | ``3``   |               |
+-------------------------+---------+---------------+
| ``gfBsplineFilter``     | ``4``   |               |
+-------------------------+---------+---------------+
| ``gfSincFilter``        | ``5``   |               |
+-------------------------+---------+---------------+
| ``gfLanczosFilter``     | ``6``   |               |
+-------------------------+---------+---------------+
| ``gfKaiserFilter``      | ``7``   |               |
+-------------------------+---------+---------------+

::

    enum GeometricFilter
    {
      gfNearestNeighbor = 0,
      gfBoxFilter = 1,
      gfTriangleFilter = 2,
      gfCubicFilter = 3,
      gfBsplineFilter = 4,
      gfSincFilter = 5,
      gfLanczosFilter = 6,
      gfKaiserFilter = 7,
    };

TODO documentation missing

Enumeration *ResampleBorderTreatment*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum ResampleBorderTreatment``

TODO documentation missing

The enumeration **ResampleBorderTreatment** has the following constants:

+--------------------------+---------+---------------+
| Name                     | Value   | Description   |
+==========================+=========+===============+
| ``noExtension``          | ``0``   |               |
+--------------------------+---------+---------------+
| ``extendWithValue``      | ``1``   |               |
+--------------------------+---------+---------------+
| ``extendPeriodically``   | ``2``   |               |
+--------------------------+---------+---------------+
| ``extendByReflection``   | ``3``   |               |
+--------------------------+---------+---------------+
| ``extendEdge``           | ``4``   |               |
+--------------------------+---------+---------------+

::

    enum ResampleBorderTreatment
    {
      noExtension = 0,
      extendWithValue = 1,
      extendPeriodically = 2,
      extendByReflection = 3,
      extendEdge = 4,
    };

TODO documentation missing

Enumeration *ResampleBoundsExtension*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum ResampleBoundsExtension``

TODO documentation missing

The enumeration **ResampleBoundsExtension** has the following constants:

+----------------+---------+---------------+
| Name           | Value   | Description   |
+================+=========+===============+
| ``keepSize``   | ``0``   |               |
+----------------+---------+---------------+
| ``extend``     | ``1``   |               |
+----------------+---------+---------------+

::

    enum ResampleBoundsExtension
    {
      keepSize = 0,
      extend = 1,
    };

TODO documentation missing
