Class *EllipseEnumerator*
-------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **EllipseEnumerator** implements the following interfaces:

+--------------------------+
| Interface                |
+==========================+
| ``IEnumerator``          |
+--------------------------+
| ``IEnumeratorEllipse``   |
+--------------------------+

The class **EllipseEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **EllipseEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Ellipse Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
