Class *GeometryElementEnumerator*
---------------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **GeometryElementEnumerator** implements the following interfaces:

+----------------------------------+
| Interface                        |
+==================================+
| ``IEnumerator``                  |
+----------------------------------+
| ``IEnumeratorGeometryElement``   |
+----------------------------------+

The class **GeometryElementEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **GeometryElementEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``GeometryElement Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
