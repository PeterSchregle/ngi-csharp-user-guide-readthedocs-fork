Class *EllipseList*
-------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **EllipseList** implements the following interfaces:

+--------------------+
| Interface          |
+====================+
| ``IListEllipse``   |
+--------------------+

The class **EllipseList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **EllipseList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *EllipseList*
^^^^^^^^^^^^^^^^^^^^^^^^^

``EllipseList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``Ellipse [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``EllipseEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(Ellipse item)``

The method **Add** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``item``    | ``Ellipse``   |               |
+-------------+---------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(Ellipse item)``

The method **Contains** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``item``    | ``Ellipse``   |               |
+-------------+---------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(Ellipse item)``

The method **Remove** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``item``    | ``Ellipse``   |               |
+-------------+---------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(Ellipse item)``

The method **IndexOf** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``item``    | ``Ellipse``   |               |
+-------------+---------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, Ellipse item)``

The method **Insert** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``item``    | ``Ellipse``        |               |
+-------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
