Class *InvariantViolatedException*
----------------------------------

Exception class for invariant violations.

**Namespace:** Ngi

**Module:**

Description
~~~~~~~~~~~

In NGI, an invariant is a predicate that is supposed to remain true throughout the whole lifetime of a class.

NGI checks invariants with the NGI\_INVARIANT macro, which - when defined - throws the invariant\_violated exception when the invariant is not true.

The user of NGI can inhibit this behavior by defining the NGI\_INVARIANT(condition, message) macro as an empty macro before the inclusion of any NGI header file.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *InvariantViolatedException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``InvariantViolatedException()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *InvariantViolatedException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``InvariantViolatedException(System.String message)``

Construct an exception from a message.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+

Constructor *InvariantViolatedException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``InvariantViolatedException(System.String message, System.UInt32 id)``

Construct an exception from a message and an id.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+
| ``id``        | ``System.UInt32``   | The exception id.        |
+---------------+---------------------+--------------------------+
