Class *CameraParameterIcommand*
-------------------------------

**Namespace:** Ngi

**Module:** Camera

The class **CameraParameterIcommand** contains the following methods:

+---------------+---------------+
| Method        | Description   |
+===============+===============+
| ``Execute``   |               |
+---------------+---------------+

Description
~~~~~~~~~~~

Methods
~~~~~~~

Method *Execute*
^^^^^^^^^^^^^^^^

``void Execute()``
