Class *CifxChannel*
-------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **CifxChannel** contains the following methods:

+---------------+---------------+
| Method        | Description   |
+===============+===============+
| ``IoWrite``   |               |
+---------------+---------------+
| ``IoRead``    |               |
+---------------+---------------+

Description
~~~~~~~~~~~

Constructors
~~~~~~~~~~~~

Constructor *CifxChannel*
^^^^^^^^^^^^^^^^^^^^^^^^^

``CifxChannel(System.String board, System.UInt32 channelIdx)``

The constructor has the following parameters:

+------------------+---------------------+---------------+
| Parameter        | Type                | Description   |
+==================+=====================+===============+
| ``board``        | ``System.String``   |               |
+------------------+---------------------+---------------+
| ``channelIdx``   | ``System.UInt32``   |               |
+------------------+---------------------+---------------+

Methods
~~~~~~~

Method *IoWrite*
^^^^^^^^^^^^^^^^

``void IoWrite(System.UInt32 areaNumber, System.UInt32 offset, DataList data, System.UInt32 timeoutMs)``

The method **IoWrite** has the following parameters:

+------------------+---------------------+---------------+
| Parameter        | Type                | Description   |
+==================+=====================+===============+
| ``areaNumber``   | ``System.UInt32``   |               |
+------------------+---------------------+---------------+
| ``offset``       | ``System.UInt32``   |               |
+------------------+---------------------+---------------+
| ``data``         | ``DataList``        |               |
+------------------+---------------------+---------------+
| ``timeoutMs``    | ``System.UInt32``   |               |
+------------------+---------------------+---------------+

Method *IoRead*
^^^^^^^^^^^^^^^

``DataList IoRead(System.UInt32 areaNumber, System.UInt32 offset, System.UInt32 count, System.UInt32 timeoutMs)``

The method **IoRead** has the following parameters:

+------------------+---------------------+---------------+
| Parameter        | Type                | Description   |
+==================+=====================+===============+
| ``areaNumber``   | ``System.UInt32``   |               |
+------------------+---------------------+---------------+
| ``offset``       | ``System.UInt32``   |               |
+------------------+---------------------+---------------+
| ``count``        | ``System.UInt32``   |               |
+------------------+---------------------+---------------+
| ``timeoutMs``    | ``System.UInt32``   |               |
+------------------+---------------------+---------------+
