Class *WidgetBox*
-----------------

A widget\_box displays a box (axis parallel rectangle) on a surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetBox** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetBox** contains the following properties:

+----------------+-------+-------+------------------------------------+
| Property       | Get   | Set   | Description                        |
+================+=======+=======+====================================+
| ``Position``   | \*    | \*    | The position of the widget\_box.   |
+----------------+-------+-------+------------------------------------+
| ``Outline``    | \*    | \*    | The outline of the widget.         |
+----------------+-------+-------+------------------------------------+
| ``Fill``       | \*    | \*    | The fill of the widget.            |
+----------------+-------+-------+------------------------------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_box. These settings affect the look and the behavior of the widget\_box.

The widget\_box supports interactivity via the widget\_interactive base class, i.e. it can be selected, move around. In general it can react on mouse and keyboard interactions. This widget supports layout.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetBox*
^^^^^^^^^^^^^^^^^^^^^^^

``WidgetBox()``

Default constructor.

The default constructor creates a widget with default settings. By default the box is of zero width at the origin, the outline is one-pixel wide black and the fill is black transparent.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``BoxDouble Position``

The position of the widget\_box.

Property *Outline*
^^^^^^^^^^^^^^^^^^

``PenByte Outline``

The outline of the widget.

The outline of the widget\_box is specified with a pen<>.

Property *Fill*
^^^^^^^^^^^^^^^

``SolidColorBrushByte Fill``

The fill of the widget.

The fill of the widget\_box is specified with a brush.
