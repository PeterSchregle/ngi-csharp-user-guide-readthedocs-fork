Class *Angle*
-------------

An angle in two-dimensional euclidean space.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Angle** implements the following interfaces:

+-----------------------+
| Interface             |
+=======================+
| ``IEquatableAngle``   |
+-----------------------+
| ``ISerializable``     |
+-----------------------+

The class **Angle** contains the following properties:

+------------------+-------+-------+------------------------------+
| Property         | Get   | Set   | Description                  |
+==================+=======+=======+==============================+
| ``Turns``        | \*    |       | The angle in turns.          |
+------------------+-------+-------+------------------------------+
| ``Radians``      | \*    |       | The angle in radians.        |
+------------------+-------+-------+------------------------------+
| ``Degrees``      | \*    |       | The angle in degrees.        |
+------------------+-------+-------+------------------------------+
| ``Quadrant``     | \*    |       | The quadrant of the angle.   |
+------------------+-------+-------+------------------------------+
| ``IsPositive``   | \*    |       | Is this a positive angle?    |
+------------------+-------+-------+------------------------------+
| ``IsNegative``   | \*    |       | Is this a negative angle?    |
+------------------+-------+-------+------------------------------+
| ``IsAcute``      | \*    |       | Is this an acute angle?      |
+------------------+-------+-------+------------------------------+
| ``IsObtuse``     | \*    |       | Is this an obtuse angle?     |
+------------------+-------+-------+------------------------------+
| ``IsReflex``     | \*    |       | Is this a reflex angle?      |
+------------------+-------+-------+------------------------------+
| ``IsRight``      | \*    |       | Is this a right angle?       |
+------------------+-------+-------+------------------------------+
| ``IsStraight``   | \*    |       | Is this a straight angle?    |
+------------------+-------+-------+------------------------------+

The class **Angle** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

The class **Angle** contains the following enumerations:

+----------------------+------------------------------+
| Enumeration          | Description                  |
+======================+==============================+
| ``QuadrantRegion``   | TODO documentation missing   |
+----------------------+------------------------------+

Description
~~~~~~~~~~~

An angle is stored in it's most natural representation, which unfortunately is seldomly used: the number of turns. Thus, zero turns represent an angle of zero degrees or zero radians, one turn represents an angle of 360 degrees or :math:`2\pi` radians.

The angle is directed, i.e. it can be positive or negative. A positive angle is oriented counter-clockwise (in a right-handed coordinate system), a negative angle is oriented counter-clockwise.

.. figure:: images/angle.png
   :alt: 

The angle is normalized to stay in the open interval of :math:`(-1, 1)` (-1 and 1 excluded), regardless of the operations done to the angle.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Angle*
^^^^^^^^^^^^^^^^^^^

``Angle()``

Default constructor.

This constructor initializes the angle to the direction of the positive x-axis, which corresponds to an angle of 0 degrees.

Constructors
~~~~~~~~~~~~

Constructor *Angle*
^^^^^^^^^^^^^^^^^^^

``Angle(System.Double turns)``

Construct an angle given turns.

The constructor has the following parameters:

+-------------+---------------------+-----------------------+
| Parameter   | Type                | Description           |
+=============+=====================+=======================+
| ``turns``   | ``System.Double``   | The angle in turns.   |
+-------------+---------------------+-----------------------+

The turns are normalized to the open interval of :math:`(-1, 1)` (-1 and 1 excluded). Such an angle would correspond to an angle in the range :math:`(-360, 360)` in degrees or :math:`(-2\pi, 2\pi)` in radians.

Properties
~~~~~~~~~~

Property *Turns*
^^^^^^^^^^^^^^^^

``System.Double Turns``

The angle in turns.

Property *Radians*
^^^^^^^^^^^^^^^^^^

``System.Double Radians``

The angle in radians.

Property *Degrees*
^^^^^^^^^^^^^^^^^^

``System.Double Degrees``

The angle in degrees.

Property *Quadrant*
^^^^^^^^^^^^^^^^^^^

``Angle.QuadrantRegion Quadrant``

The quadrant of the angle.

The axes of a two-dimensional cartesian coordinate system divide the plane into four infinite regions, called quadrants, often denoted by roman numerals.

.. figure:: images/quadrant.png
   :alt: 

Usually, the concept of a quadrant is shown in a right-handed coordinate system. We want to alert you to the definition in a left-handed coordinate system, since this is used within nGI very often.

Contrary to the strict mathematical definition, angles that are on an axis are considered part of a quadrant (an angle of 0° is part of quadrant I, an angle of 90° is part of quadrant II, an angle of 180° is part of quadrant III and an angle of 270° is part of quadrant IV).

Property *IsPositive*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsPositive``

Is this a positive angle?

An angle is positive, if it is counter-clockwise in a right handed coordinate system, or clockwise in a left-handed coordinate system.

An angle of zero is considered positive.

Property *IsNegative*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsNegative``

Is this a negative angle?

An angle is negative, if it is clockwise in a right handed coordinate system, or counter-clockwise in a left-handed coordinate system.

An angle of zero is considered positive.

Property *IsAcute*
^^^^^^^^^^^^^^^^^^

``System.Boolean IsAcute``

Is this an acute angle?

An angle is acute, if it is positive and in quadrant I or negative and in quadrant IV.

.. figure:: images/acute_angle.png
   :alt: 

Property *IsObtuse*
^^^^^^^^^^^^^^^^^^^

``System.Boolean IsObtuse``

Is this an obtuse angle?

An angle is obtuse, if it is in positive and in quadrant II or negative and in quadrant II.

.. figure:: images/obtuse_angle.png
   :alt: 

Property *IsReflex*
^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReflex``

Is this a reflex angle?

An angle is reflex, if it is positive and in quadrants III or IV, or if it is negative and in quadrants I or II.

.. figure:: images/reflex_angle.png
   :alt: 

Property *IsRight*
^^^^^^^^^^^^^^^^^^

``System.Boolean IsRight``

Is this a right angle?

An angle is right, if it is 1/4th or 3/4th turns.

Property *IsStraight*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsStraight``

Is this a straight angle?

An angle is straight, if it is 1/2 turns.

Static Methods
~~~~~~~~~~~~~~

Method *FromRadians*
^^^^^^^^^^^^^^^^^^^^

``Angle FromRadians(System.Double angleInRadians)``

Create an angle given radians.

The method **FromRadians** has the following parameters:

+----------------------+---------------------+---------------+
| Parameter            | Type                | Description   |
+======================+=====================+===============+
| ``angleInRadians``   | ``System.Double``   |               |
+----------------------+---------------------+---------------+

The turns are normalized to the open interval of :math:`(-1, 1)` (-1 and 1 excluded). Such an angle would correspond to an angle in the range :math:`(-360, 360)` in degrees or :math:`(-2\pi, 2\pi)` in radians.

Method *FromDegrees*
^^^^^^^^^^^^^^^^^^^^

``Angle FromDegrees(System.Double angleInDegrees)``

Create an angle given degrees.

The method **FromDegrees** has the following parameters:

+----------------------+---------------------+---------------+
| Parameter            | Type                | Description   |
+======================+=====================+===============+
| ``angleInDegrees``   | ``System.Double``   |               |
+----------------------+---------------------+---------------+

The turns are normalized to the open interval of :math:`(-1, 1)` (-1 and 1 excluded). Such an angle would correspond to an angle in the range :math:`(-360, 360)` in degrees or :math:`(-2\pi, 2\pi)` in radians.

Method *Add*
^^^^^^^^^^^^

``Angle Add(Angle a, Angle b)``

Add two angles.

The method **Add** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``a``       | ``Angle``   |               |
+-------------+-------------+---------------+
| ``b``       | ``Angle``   |               |
+-------------+-------------+---------------+

The turns are normalized to the open interval of :math:`(-1, 1)` (-1 and 1 excluded). Such an angle would correspond to an angle in the range :math:`(-360, 360)` in degrees or :math:`(-2\pi, 2\pi)` in radians.

Returns a + b;

Method *Subtract*
^^^^^^^^^^^^^^^^^

``Angle Subtract(Angle a, Angle b)``

Subtract two angles.

The method **Subtract** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``a``       | ``Angle``   |               |
+-------------+-------------+---------------+
| ``b``       | ``Angle``   |               |
+-------------+-------------+---------------+

The turns are normalized to the open interval of :math:`(-1, 1)` (-1 and 1 excluded). Such an angle would correspond to an angle in the range :math:`(-360, 360)` in degrees or :math:`(-2\pi, 2\pi)` in radians.

/return Returns a - b;

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Enumerations
~~~~~~~~~~~~

Enumeration *QuadrantRegion*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum QuadrantRegion``

TODO documentation missing

The enumeration **QuadrantRegion** has the following constants:

+-----------+---------+---------------+
| Name      | Value   | Description   |
+===========+=========+===============+
| ``i``     | ``1``   |               |
+-----------+---------+---------------+
| ``ii``    | ``2``   |               |
+-----------+---------+---------------+
| ``iii``   | ``3``   |               |
+-----------+---------+---------------+
| ``iv``    | ``4``   |               |
+-----------+---------+---------------+

::

    enum QuadrantRegion
    {
      i = 1,
      ii = 2,
      iii = 3,
      iv = 4,
    };

TODO documentation missing
