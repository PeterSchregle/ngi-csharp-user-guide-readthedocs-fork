Class *Box3dList*
-----------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Box3dList** implements the following interfaces:

+----------------------------------+
| Interface                        |
+==================================+
| ``IListBox3dListBox3dVariant``   |
+----------------------------------+

The class **Box3dList** contains the following variant parameters:

+-------------+-----------------------------------------+
| Variant     | Description                             |
+=============+=========================================+
| ``Box3d``   | TODO no brief description for variant   |
+-------------+-----------------------------------------+

The class **Box3dList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **Box3dList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Box3d*
^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Box3d** has the following types:

+-------------------+
| Type              |
+===================+
| ``Box3dInt32``    |
+-------------------+
| ``Box3dDouble``   |
+-------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Box3dList*
^^^^^^^^^^^^^^^^^^^^^^^

``Box3dList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``Box3d [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``Box3dEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(Box3d item)``

The method **Add** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``item``    | ``Box3d``   |               |
+-------------+-------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(Box3d item)``

The method **Contains** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``item``    | ``Box3d``   |               |
+-------------+-------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(Box3d item)``

The method **Remove** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``item``    | ``Box3d``   |               |
+-------------+-------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(Box3d item)``

The method **IndexOf** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``item``    | ``Box3d``   |               |
+-------------+-------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, Box3d item)``

The method **Insert** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``item``    | ``Box3d``          |               |
+-------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
