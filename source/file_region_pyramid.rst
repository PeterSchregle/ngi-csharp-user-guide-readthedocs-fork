Class *RegionPyramid*
---------------------

The base class.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **RegionPyramid** contains the following properties:

+-----------------------+-------+-------+------------------------------------------+
| Property              | Get   | Set   | Description                              |
+=======================+=======+=======+==========================================+
| ``DownscaleFactor``   | \*    |       | The factor for successive downscaling.   |
+-----------------------+-------+-------+------------------------------------------+

The class **RegionPyramid** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``ToString``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *RegionPyramid*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RegionPyramid()``

Constructs an empty region\_pyramid.

Constructors
~~~~~~~~~~~~

Constructor *RegionPyramid*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``RegionPyramid(Region source, System.Double downscaleFactor, System.Int32 levels)``

Constructs a region\_pyramid from a region.

The constructor has the following parameters:

+-----------------------+---------------------+--------------------+
| Parameter             | Type                | Description        |
+=======================+=====================+====================+
| ``source``            | ``Region``          | Original region.   |
+-----------------------+---------------------+--------------------+
| ``downscaleFactor``   | ``System.Double``   |                    |
+-----------------------+---------------------+--------------------+
| ``levels``            | ``System.Int32``    |                    |
+-----------------------+---------------------+--------------------+

Copies only the original region into the pyramid. It does not allocate the additional levels. In order to do so, you need to call allocate\_levels(). It does also not calculate the downscaled regions. In order to do so, you need to call downscale().

Properties
~~~~~~~~~~

Property *DownscaleFactor*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Int32 DownscaleFactor``

The factor for successive downscaling.

This factor must be between 0 and 1 (both excluded). Usually it is set to 0.5.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
