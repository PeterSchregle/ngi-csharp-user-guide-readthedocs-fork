Class *WidgetVerticalScale*
---------------------------

A widget\_vertical\_scale displays a scale on a surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetVerticalScale** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetVerticalScale** contains the following properties:

+-----------------------+-------+-------+---------------------------------------------+
| Property              | Get   | Set   | Description                                 |
+=======================+=======+=======+=============================================+
| ``Position``          | \*    | \*    | The position of the widget.                 |
+-----------------------+-------+-------+---------------------------------------------+
| ``Pen``               | \*    | \*    | The pen of the widget.                      |
+-----------------------+-------+-------+---------------------------------------------+
| ``Brush``             | \*    | \*    | The brush of the widget.                    |
+-----------------------+-------+-------+---------------------------------------------+
| ``Direction``         | \*    | \*    | The drawing direction of the widget.        |
+-----------------------+-------+-------+---------------------------------------------+
| ``Minimum``           | \*    | \*    | The minimum.                                |
+-----------------------+-------+-------+---------------------------------------------+
| ``Maximum``           | \*    | \*    | The maximum.                                |
+-----------------------+-------+-------+---------------------------------------------+
| ``AutoTickSpacing``   | \*    | \*    | Determine the tick-spacing automatically.   |
+-----------------------+-------+-------+---------------------------------------------+
| ``TextPosition``      | \*    | \*    |                                             |
+-----------------------+-------+-------+---------------------------------------------+
| ``TextFont``          | \*    | \*    |                                             |
+-----------------------+-------+-------+---------------------------------------------+

The class **WidgetVerticalScale** contains the following enumerations:

+---------------------+------------------------------+
| Enumeration         | Description                  |
+=====================+==============================+
| ``DirectionType``   | TODO documentation missing   |
+---------------------+------------------------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_vertical\_scale. These settings affect the look and the behavior of the widget\_vertical\_scale.

The widget\_vertical\_scale supports interactivity via the widget\_layoutable base class, i.e. it can be selected, moved around. In general it can react on mouse and keyboard interactions.This widget is layoutable.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetVerticalScale*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetVerticalScale()``

Default constructor.

The default constructor creates a widget with default settings.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``BoxDouble Position``

The position of the widget.

Property *Pen*
^^^^^^^^^^^^^^

``PenByte Pen``

The pen of the widget.

Property *Brush*
^^^^^^^^^^^^^^^^

``SolidColorBrushByte Brush``

The brush of the widget.

Property *Direction*
^^^^^^^^^^^^^^^^^^^^

``WidgetVerticalScale.DirectionType Direction``

The drawing direction of the widget.

Property *Minimum*
^^^^^^^^^^^^^^^^^^

``System.Double Minimum``

The minimum.

Property *Maximum*
^^^^^^^^^^^^^^^^^^

``System.Double Maximum``

The maximum.

Property *AutoTickSpacing*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean AutoTickSpacing``

Determine the tick-spacing automatically.

Property *TextPosition*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Double TextPosition``

Property *TextFont*
^^^^^^^^^^^^^^^^^^^

``Font TextFont``

Enumerations
~~~~~~~~~~~~

Enumeration *DirectionType*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum DirectionType``

TODO documentation missing

The enumeration **DirectionType** has the following constants:

+-------------------+---------+---------------+
| Name              | Value   | Description   |
+===================+=========+===============+
| ``bottomToTop``   | ``0``   |               |
+-------------------+---------+---------------+
| ``topToBottom``   | ``1``   |               |
+-------------------+---------+---------------+

::

    enum DirectionType
    {
      bottomToTop = 0,
      topToBottom = 1,
    };

TODO documentation missing
