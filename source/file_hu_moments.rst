Class *HuMoments*
-----------------

Class to hold Hu's moments.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **HuMoments** implements the following interfaces:

+---------------------------+
| Interface                 |
+===========================+
| ``IEquatableHuMoments``   |
+---------------------------+
| ``ISerializable``         |
+---------------------------+

The class **HuMoments** contains the following properties:

+------------+-------+-------+--------------------------+
| Property   | Get   | Set   | Description              |
+============+=======+=======+==========================+
| ``Phi1``   | \*    |       | The first Hu moment.     |
+------------+-------+-------+--------------------------+
| ``Phi2``   | \*    |       | The second Hu moment.    |
+------------+-------+-------+--------------------------+
| ``Phi3``   | \*    |       | The third Hu moment.     |
+------------+-------+-------+--------------------------+
| ``Phi4``   | \*    |       | The fourth Hu moment.    |
+------------+-------+-------+--------------------------+
| ``Phi5``   | \*    |       | The fifth Hu moment.     |
+------------+-------+-------+--------------------------+
| ``Phi6``   | \*    |       | The sixth Hu moment.     |
+------------+-------+-------+--------------------------+
| ``Phi7``   | \*    |       | The seventh Hu moment.   |
+------------+-------+-------+--------------------------+

The class **HuMoments** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

For a nice and short treatment of moments see Chaur-Chin Chen, Improved Moment Invariants for Shape Discrimination [1992].

Moments are a sort of weighted averages of the image pixels' intensities (the pixel intensities may be set to 1 in case of binary moments). They are useful to describe segmented objects. Simple properties of objects found via moments include area (or total intensity), its centroid, and information about its orientation.

The Hu moments are invariant under translation, scaling and rotation.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *HuMoments*
^^^^^^^^^^^^^^^^^^^^^^^

``HuMoments()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *HuMoments*
^^^^^^^^^^^^^^^^^^^^^^^

``HuMoments(ScaleInvariantMoments eta)``

Calculates the Hu moments from some scale invariant moments.

The constructor has the following parameters:

+-------------+-----------------------------+-----------------------------------------+
| Parameter   | Type                        | Description                             |
+=============+=============================+=========================================+
| ``eta``     | ``ScaleInvariantMoments``   | Second order scale invariant moments.   |
+-------------+-----------------------------+-----------------------------------------+

Initializes the moments.

Properties
~~~~~~~~~~

Property *Phi1*
^^^^^^^^^^^^^^^

``System.Double Phi1``

The first Hu moment.

Property *Phi2*
^^^^^^^^^^^^^^^

``System.Double Phi2``

The second Hu moment.

Property *Phi3*
^^^^^^^^^^^^^^^

``System.Double Phi3``

The third Hu moment.

Property *Phi4*
^^^^^^^^^^^^^^^

``System.Double Phi4``

The fourth Hu moment.

Property *Phi5*
^^^^^^^^^^^^^^^

``System.Double Phi5``

The fifth Hu moment.

Property *Phi6*
^^^^^^^^^^^^^^^

``System.Double Phi6``

The sixth Hu moment.

Property *Phi7*
^^^^^^^^^^^^^^^

``System.Double Phi7``

The seventh Hu moment.

Static Methods
~~~~~~~~~~~~~~

Method *CalculatePhi1*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculatePhi1(ScaleInvariantMoments eta)``

Calculate phi1 from scale invariant moments.

The method **CalculatePhi1** has the following parameters:

+-------------+-----------------------------+---------------+
| Parameter   | Type                        | Description   |
+=============+=============================+===============+
| ``eta``     | ``ScaleInvariantMoments``   |               |
+-------------+-----------------------------+---------------+

phi1 is the first of Hu's moments.

First Hu moment.

Method *CalculatePhi2*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculatePhi2(ScaleInvariantMoments eta)``

Calculate phi2 from scale invariant moments.

The method **CalculatePhi2** has the following parameters:

+-------------+-----------------------------+---------------+
| Parameter   | Type                        | Description   |
+=============+=============================+===============+
| ``eta``     | ``ScaleInvariantMoments``   |               |
+-------------+-----------------------------+---------------+

phi2 is the second of Hu's moments.

Second Hu moment.

Method *CalculatePhi3*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculatePhi3(ScaleInvariantMoments eta)``

Calculate phi3 from scale invariant moments.

The method **CalculatePhi3** has the following parameters:

+-------------+-----------------------------+---------------+
| Parameter   | Type                        | Description   |
+=============+=============================+===============+
| ``eta``     | ``ScaleInvariantMoments``   |               |
+-------------+-----------------------------+---------------+

phi1 is the third of Hu's moments.

Third Hu moment.

Method *CalculatePhi4*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculatePhi4(ScaleInvariantMoments eta)``

Calculate phi4 from scale invariant moments.

The method **CalculatePhi4** has the following parameters:

+-------------+-----------------------------+---------------+
| Parameter   | Type                        | Description   |
+=============+=============================+===============+
| ``eta``     | ``ScaleInvariantMoments``   |               |
+-------------+-----------------------------+---------------+

phi1 is the fourth of Hu's moments.

Fourth Hu moment.

Method *CalculatePhi5*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculatePhi5(ScaleInvariantMoments eta)``

Calculate phi5 from scale invariant moments.

The method **CalculatePhi5** has the following parameters:

+-------------+-----------------------------+---------------+
| Parameter   | Type                        | Description   |
+=============+=============================+===============+
| ``eta``     | ``ScaleInvariantMoments``   |               |
+-------------+-----------------------------+---------------+

phi5 is the fifth of Hu's moments.

Fifth Hu moment.

Method *CalculatePhi6*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculatePhi6(ScaleInvariantMoments eta)``

Calculate phi6 from scale invariant moments.

The method **CalculatePhi6** has the following parameters:

+-------------+-----------------------------+---------------+
| Parameter   | Type                        | Description   |
+=============+=============================+===============+
| ``eta``     | ``ScaleInvariantMoments``   |               |
+-------------+-----------------------------+---------------+

phi1 is the sixth of Hu's moments.

Sixth Hu moment.

Method *CalculatePhi7*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculatePhi7(ScaleInvariantMoments eta)``

Calculate phi7 from scale invariant moments.

The method **CalculatePhi7** has the following parameters:

+-------------+-----------------------------+---------------+
| Parameter   | Type                        | Description   |
+=============+=============================+===============+
| ``eta``     | ``ScaleInvariantMoments``   |               |
+-------------+-----------------------------+---------------+

phi1 is the seventh of Hu's moments.

Seventh Hu moment.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
