Class *ImageWithPalette*
------------------------

This class pairs an image with a palette.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **ImageWithPalette** contains the following variant parameters:

+-----------+-----------------------------------------+
| Variant   | Description                             |
+===========+=========================================+
| ``Img``   | TODO no brief description for variant   |
+-----------+-----------------------------------------+
| ``Pal``   | TODO no brief description for variant   |
+-----------+-----------------------------------------+

The class **ImageWithPalette** contains the following properties:

+---------------+-------+-------+----------------+
| Property      | Get   | Set   | Description    |
+===============+=======+=======+================+
| ``Image``     | \*    | \*    | The image.     |
+---------------+-------+-------+----------------+
| ``Palette``   | \*    | \*    | The palette.   |
+---------------+-------+-------+----------------+

The class **ImageWithPalette** contains the following methods:

+--------------------------------+---------------------------------------------------------------+
| Method                         | Description                                                   |
+================================+===============================================================+
| ``ConvertToRgbaImage``         | Converts an image of any type to an image with rgba pixels.   |
+--------------------------------+---------------------------------------------------------------+
| ``ConvertToRgbaImage``         | Converts an image of any type to an image with rgba pixels.   |
+--------------------------------+---------------------------------------------------------------+
| ``ExportImageWithPalette``     | Exports an image to a file.                                   |
+--------------------------------+---------------------------------------------------------------+
| ``ExportImageWithPalette``     | Exports an image to memory.                                   |
+--------------------------------+---------------------------------------------------------------+
| ``ExportImageWithPaletteDl``   | Exports an image to a data\_list.                             |
+--------------------------------+---------------------------------------------------------------+

Description
~~~~~~~~~~~

The palette is used to affect the display of the image.

Variants
~~~~~~~~

Variant *Img*
^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Img** has the following types:

+------------------+
| Type             |
+==================+
| ``Byte``         |
+------------------+
| ``UInt16``       |
+------------------+
| ``UInt32``       |
+------------------+
| ``Double``       |
+------------------+
| ``RgbByte``      |
+------------------+
| ``RgbUInt16``    |
+------------------+
| ``RgbUInt32``    |
+------------------+
| ``RgbDouble``    |
+------------------+
| ``RgbaByte``     |
+------------------+
| ``RgbaUInt16``   |
+------------------+
| ``RgbaUInt32``   |
+------------------+
| ``RgbaDouble``   |
+------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Variant *Pal*
^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Pal** has the following types:

+----------------+
| Type           |
+================+
| ``RgbaByte``   |
+----------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *ImageWithPalette*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageWithPalette()``

Constructs an empty image\_with\_palette.

Constructors
~~~~~~~~~~~~

Constructor *ImageWithPalette*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageWithPalette(Image image)``

Constructs an image\_with\_palette given an image only.

The constructor has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``image``   | ``Image``   | The image.    |
+-------------+-------------+---------------+

Constructor *ImageWithPalette*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageWithPalette(Image image, Palette palette)``

Constructs an image\_with\_palette given an image and a palette.

The constructor has the following parameters:

+---------------+---------------+----------------+
| Parameter     | Type          | Description    |
+===============+===============+================+
| ``image``     | ``Image``     | The image.     |
+---------------+---------------+----------------+
| ``palette``   | ``Palette``   | The palette.   |
+---------------+---------------+----------------+

Properties
~~~~~~~~~~

Property *Image*
^^^^^^^^^^^^^^^^

``Image Image``

The image.

Property *Palette*
^^^^^^^^^^^^^^^^^^

``Palette Palette``

The palette.

Static Methods
~~~~~~~~~~~~~~

Method *ImportImageWithPalette*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageWithPalette ImportImageWithPalette(System.String filename)``

Imports an image from a file.

The method **ImportImageWithPalette** has the following parameters:

+----------------+---------------------+---------------+
| Parameter      | Type                | Description   |
+================+=====================+===============+
| ``filename``   | ``System.String``   |               |
+----------------+---------------------+---------------+

This function loads an image using FreeImage and then copies it into a data structure of type image.

The filename can either be a complete (absolute or relative) path to a file, or it can be a partial filename.

If a complete path to a file is given, the function tries to load the respective file. If a partial filename is given, the function appends it to the path fragment stored in the NGI\_IMAGE\_PATH environment variable. It then tries to load the file addressed by this combined path.

If the type of the image does not fit the type of the data in the file, the function attempts to do a conversion.

The loaded data in a image.

Method *ImportImageWithPalette*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageWithPalette ImportImageWithPalette(System.IntPtr memory, System.Int32 size)``

Imports an image from memory.

The method **ImportImageWithPalette** has the following parameters:

+--------------+---------------------+---------------+
| Parameter    | Type                | Description   |
+==============+=====================+===============+
| ``memory``   | ``System.IntPtr``   |               |
+--------------+---------------------+---------------+
| ``size``     | ``System.Int32``    |               |
+--------------+---------------------+---------------+

This function loads an image using FreeImage and then copies it into a data structure of type image.

If the type of the image does not fit the type of the data in the file, the function attempts to do a conversion.

The loaded data in a image.

Method *ImportImageWithPalette*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageWithPalette ImportImageWithPalette(DataList data)``

Imports an image from a data\_list.

The method **ImportImageWithPalette** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``data``    | ``DataList``   |               |
+-------------+----------------+---------------+

This function loads an image using FreeImage and then copies it into a data structure of type image.

If the type of the image does not fit the type of the data in the file, the function attempts to do a conversion.

The loaded data in a image.

Methods
~~~~~~~

Method *ConvertToRgbaImage*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte ConvertToRgbaImage()``

Converts an image of any type to an image with rgba pixels.

The function allocates an image in 32 bit rgba format. It copies the pixels from the source view into the image, performing potential color space conversion and mapping over a palette on the fly.

This is the catch-all function that can convert all types of color spaces.

Method *ConvertToRgbaImage*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte ConvertToRgbaImage(System.Double offset, System.Double scale)``

Converts an image of any type to an image with rgba pixels.

The method **ConvertToRgbaImage** has the following parameters:

+--------------+---------------------+----------------------------------------------+
| Parameter    | Type                | Description                                  |
+==============+=====================+==============================================+
| ``offset``   | ``System.Double``   | The offset to be added to the pixel value.   |
+--------------+---------------------+----------------------------------------------+
| ``scale``    | ``System.Double``   | The scale factor to be applied,              |
+--------------+---------------------+----------------------------------------------+

The function allocates an image in 32 bit rgba format. It copies the pixels from the source view into the image, while adding an offset and scaling by scale, performing potential color space conversion and mapping over a palette on the fly.

This is the catch-all function that can convert all types of color spaces.

Method *ExportImageWithPalette*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ExportImageWithPalette(System.String filename)``

Exports an image to a file.

The method **ExportImageWithPalette** has the following parameters:

+----------------+---------------------+-------------------------------------+
| Parameter      | Type                | Description                         |
+================+=====================+=====================================+
| ``filename``   | ``System.String``   | The pathname of the file to load.   |
+----------------+---------------------+-------------------------------------+

The pathname must be a complete (absolute or relative) path to a file.

This function copies a data structure of type image into a structure that FreeImage understands, and then saves it to a file.

The type of the file is determined by examining the extension of the Pathname.

This function only saves the first plane.

Method *ExportImageWithPalette*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Int32 ExportImageWithPalette(System.IntPtr memory, System.String format)``

Exports an image to memory.

The method **ExportImageWithPalette** has the following parameters:

+--------------+---------------------+---------------------+
| Parameter    | Type                | Description         |
+==============+=====================+=====================+
| ``memory``   | ``System.IntPtr``   | The memory block.   |
+--------------+---------------------+---------------------+
| ``format``   | ``System.String``   | The format.         |
+--------------+---------------------+---------------------+

The memory block must be allocated in the proper size before calling this function with a non-null memory parameter. In order to find out the proper size, call the function with a null value for memory.

This function copies a data structure of type BUFFER into a structure that FreeImage understands, and then saves it to memory.

The format is determined by giving a file extension.

This function only saves the first plane.

Method *ExportImageWithPaletteDl*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``DataList ExportImageWithPaletteDl(System.String format)``

Exports an image to a data\_list.

The method **ExportImageWithPaletteDl** has the following parameters:

+--------------+---------------------+---------------+
| Parameter    | Type                | Description   |
+==============+=====================+===============+
| ``format``   | ``System.String``   | The format.   |
+--------------+---------------------+---------------+

This function copies a data structure of type BUFFER into a structure that FreeImage understands, and then saves it to a data\_list.

The format is determined by giving a file extension.

This function only saves the first plane.

/return The data\_list.
