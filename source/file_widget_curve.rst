Class *WidgetCurve*
-------------------

A widget\_curve displays a curve on a surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetCurve** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetCurve** contains the following variant parameters:

+------------+-----------------------------------------+
| Variant    | Description                             |
+============+=========================================+
| ``View``   | TODO no brief description for variant   |
+------------+-----------------------------------------+

The class **WidgetCurve** contains the following properties:

+-----------------+-------+-------+----------------------------------------+
| Property        | Get   | Set   | Description                            |
+=================+=======+=======+========================================+
| ``Position``    | \*    | \*    | The position of the widget.            |
+-----------------+-------+-------+----------------------------------------+
| ``Outline``     | \*    | \*    | The outline pen.                       |
+-----------------+-------+-------+----------------------------------------+
| ``Fill``        | \*    | \*    | The fill brush.                        |
+-----------------+-------+-------+----------------------------------------+
| ``Direction``   | \*    | \*    | The drawing direction of the widget.   |
+-----------------+-------+-------+----------------------------------------+
| ``Minimum``     | \*    | \*    | The minimum.                           |
+-----------------+-------+-------+----------------------------------------+
| ``Maximum``     | \*    | \*    | The maximum.                           |
+-----------------+-------+-------+----------------------------------------+
| ``Reverse``     | \*    | \*    | Reverse drawing order.                 |
+-----------------+-------+-------+----------------------------------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_curve. These settings affect the look and the behavior of the widget\_curve.

The widget\_curve supports automatic layout via the widget\_layoutable base class. The size of the widget is determined by the size of the view which is displayed.

The widget\_curve supports interactivity via the widget\_interactive base class, i.e. it can be selected, move around. In general it can react on mouse and keyboard interactions. This widget displays a view.

Variants
~~~~~~~~

Variant *View*
^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **View** has the following types:

+-----------------------------+
| Type                        |
+=============================+
| ``ViewLocatorByte``         |
+-----------------------------+
| ``ViewLocatorUInt16``       |
+-----------------------------+
| ``ViewLocatorUInt32``       |
+-----------------------------+
| ``ViewLocatorDouble``       |
+-----------------------------+
| ``ViewLocatorRgbByte``      |
+-----------------------------+
| ``ViewLocatorRgbUInt16``    |
+-----------------------------+
| ``ViewLocatorRgbUInt32``    |
+-----------------------------+
| ``ViewLocatorRgbDouble``    |
+-----------------------------+
| ``ViewLocatorRgbaByte``     |
+-----------------------------+
| ``ViewLocatorRgbaUInt16``   |
+-----------------------------+
| ``ViewLocatorRgbaUInt32``   |
+-----------------------------+
| ``ViewLocatorRgbaDouble``   |
+-----------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetCurve*
^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetCurve()``

Default constructor.

The default constructor creates a widget with default settings. By default the pen is one-pixel wide black and the brush is black half transparent.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``BoxDouble Position``

The position of the widget.

Property *Outline*
^^^^^^^^^^^^^^^^^^

``PenByte Outline``

The outline pen.

Property *Fill*
^^^^^^^^^^^^^^^

``SolidColorBrushByte Fill``

The fill brush.

Property *Direction*
^^^^^^^^^^^^^^^^^^^^

``System.Int32 Direction``

The drawing direction of the widget.

Property *Minimum*
^^^^^^^^^^^^^^^^^^

``System.Double Minimum``

The minimum.

Property *Maximum*
^^^^^^^^^^^^^^^^^^

``System.Double Maximum``

The maximum.

Property *Reverse*
^^^^^^^^^^^^^^^^^^

``System.Boolean Reverse``

Reverse drawing order.
