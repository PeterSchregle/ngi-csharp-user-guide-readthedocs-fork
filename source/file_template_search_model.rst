Class *TemplateSearchModel*
---------------------------

A class holding a preprocessed model for template matching.

**Namespace:** Ngi

**Module:** TemplateMatching

The class **TemplateSearchModel** implements the following interfaces:

+--------------------------------------------------------------------+
| Interface                                                          |
+====================================================================+
| ``IEquatableTemplateSearchModelTemplateSearchModelPixelVariant``   |
+--------------------------------------------------------------------+
| ``ISerializable``                                                  |
+--------------------------------------------------------------------+

The class **TemplateSearchModel** contains the following variant parameters:

+-------------+-----------------------------------------+
| Variant     | Description                             |
+=============+=========================================+
| ``Pixel``   | TODO no brief description for variant   |
+-------------+-----------------------------------------+

The class **TemplateSearchModel** contains the following properties:

+------------------------+-------+-------+-------------------------------------------------------------------------+
| Property               | Get   | Set   | Description                                                             |
+========================+=======+=======+=========================================================================+
| ``ModelPyramid``       | \*    |       | The model pyramid.                                                      |
+------------------------+-------+-------+-------------------------------------------------------------------------+
| ``MaskPyramid``        | \*    |       | The mask pyramid.                                                       |
+------------------------+-------+-------+-------------------------------------------------------------------------+
| ``MinimalModelSize``   | \*    |       | The minimal size of the template on the highest level of the pyramid.   |
+------------------------+-------+-------+-------------------------------------------------------------------------+

The class **TemplateSearchModel** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

An important use case for template matching searches the same pattern many times in a loop. Therefore, anything that can be done up-front in a one-time initialization will increase performance, because the processing time is taken out of the loop. Therefore, overall performance ist better if the search model is pre-computed outside the loop.

Variants
~~~~~~~~

Variant *Pixel*
^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Pixel** has the following types:

+------------------+
| Type             |
+==================+
| ``Byte``         |
+------------------+
| ``UInt16``       |
+------------------+
| ``UInt32``       |
+------------------+
| ``Double``       |
+------------------+
| ``RgbByte``      |
+------------------+
| ``RgbUInt16``    |
+------------------+
| ``RgbUInt32``    |
+------------------+
| ``RgbDouble``    |
+------------------+
| ``RgbaByte``     |
+------------------+
| ``RgbaUInt16``   |
+------------------+
| ``RgbaUInt32``   |
+------------------+
| ``RgbaDouble``   |
+------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *TemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModel()``

Constructs a template\_search\_model at origin (0, 0).

Constructors
~~~~~~~~~~~~

Constructor *TemplateSearchModel*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TemplateSearchModel(ImagePyramid modelPyramid, RegionPyramid maskPyramid, System.Int32 minimalModelSize)``

Constructs a template\_search\_model with specific values.

The constructor has the following parameters:

+------------------------+---------------------+------------------------------------------------------------------------+
| Parameter              | Type                | Description                                                            |
+========================+=====================+========================================================================+
| ``modelPyramid``       | ``ImagePyramid``    | The model pyramid.                                                     |
+------------------------+---------------------+------------------------------------------------------------------------+
| ``maskPyramid``        | ``RegionPyramid``   | The The mask pyramid.                                                  |
+------------------------+---------------------+------------------------------------------------------------------------+
| ``minimalModelSize``   | ``System.Int32``    | The minimal size of the template on the highest step of the pyramid.   |
+------------------------+---------------------+------------------------------------------------------------------------+

Properties
~~~~~~~~~~

Property *ModelPyramid*
^^^^^^^^^^^^^^^^^^^^^^^

``ImagePyramid ModelPyramid``

The model pyramid.

Property *MaskPyramid*
^^^^^^^^^^^^^^^^^^^^^^

``RegionPyramid MaskPyramid``

The mask pyramid.

Property *MinimalModelSize*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Int32 MinimalModelSize``

The minimal size of the template on the highest level of the pyramid.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
