Class *FreemanCodeList*
-----------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **FreemanCodeList** implements the following interfaces:

+------------------------+
| Interface              |
+========================+
| ``IListFreemanCode``   |
+------------------------+

The class **FreemanCodeList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **FreemanCodeList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *FreemanCodeList*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``FreemanCodeList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``FreemanCode [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``FreemanCodeEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(FreemanCode item)``

The method **Add** has the following parameters:

+-------------+-------------------+---------------+
| Parameter   | Type              | Description   |
+=============+===================+===============+
| ``item``    | ``FreemanCode``   |               |
+-------------+-------------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(FreemanCode item)``

The method **Contains** has the following parameters:

+-------------+-------------------+---------------+
| Parameter   | Type              | Description   |
+=============+===================+===============+
| ``item``    | ``FreemanCode``   |               |
+-------------+-------------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(FreemanCode item)``

The method **Remove** has the following parameters:

+-------------+-------------------+---------------+
| Parameter   | Type              | Description   |
+=============+===================+===============+
| ``item``    | ``FreemanCode``   |               |
+-------------+-------------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(FreemanCode item)``

The method **IndexOf** has the following parameters:

+-------------+-------------------+---------------+
| Parameter   | Type              | Description   |
+=============+===================+===============+
| ``item``    | ``FreemanCode``   |               |
+-------------+-------------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 freemanCode, FreemanCode item)``

The method **Insert** has the following parameters:

+-------------------+--------------------+---------------+
| Parameter         | Type               | Description   |
+===================+====================+===============+
| ``freemanCode``   | ``System.Int32``   |               |
+-------------------+--------------------+---------------+
| ``item``          | ``FreemanCode``    |               |
+-------------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 freemanCode)``

The method **RemoveAt** has the following parameters:

+-------------------+--------------------+---------------+
| Parameter         | Type               | Description   |
+===================+====================+===============+
| ``freemanCode``   | ``System.Int32``   |               |
+-------------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
