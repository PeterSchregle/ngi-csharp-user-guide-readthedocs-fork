Class *PostconditionViolatedException*
--------------------------------------

Exception class for postcondition violations.

**Namespace:** Ngi

**Module:**

Description
~~~~~~~~~~~

In NGI, a postcondition is a condition or predicate that must always be true after the execution of some section of code.

NGI checks postconditions with the NGI\_POSTCONDITION macro, which - when defined - throws the postcondition\_violated exception when the postcondition is not true.

The user of NGI can inhibit this behavior by defining the NGI\_POSTCONDITION(condition, message) macro as an empty macro before the inclusion of any NGI header file.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *PostconditionViolatedException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``PostconditionViolatedException()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *PostconditionViolatedException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``PostconditionViolatedException(System.String message)``

Construct an exception from a message.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+

Constructor *PostconditionViolatedException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``PostconditionViolatedException(System.String message, System.UInt32 id)``

Construct an exception from a message and an id.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+
| ``id``        | ``System.UInt32``   | The exception id.        |
+---------------+---------------------+--------------------------+
