Class *FilterAlgorithms*
------------------------

Image processing filter functions.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **FilterAlgorithms** contains the following enumerations:

+-----------------------+------------------------------+
| Enumeration           | Description                  |
+=======================+==============================+
| ``FramingFunction``   | TODO documentation missing   |
+-----------------------+------------------------------+

Description
~~~~~~~~~~~

The class contains a group of image processing filter functions. It includes various median filters, various convolutions as well as some helper functions.

Static Methods
~~~~~~~~~~~~~~

Method *Frame*
^^^^^^^^^^^^^^

``ImageByte Frame(ViewLocatorByte source, Extent3d size, FilterAlgorithms.FramingFunction function, System.Byte fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorByte``                    |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``System.Byte``                        |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageUInt16 Frame(ViewLocatorUInt16 source, Extent3d size, FilterAlgorithms.FramingFunction function, System.UInt16 fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorUInt16``                  |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``System.UInt16``                      |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageUInt32 Frame(ViewLocatorUInt32 source, Extent3d size, FilterAlgorithms.FramingFunction function, System.UInt32 fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorUInt32``                  |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``System.UInt32``                      |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageDouble Frame(ViewLocatorDouble source, Extent3d size, FilterAlgorithms.FramingFunction function, System.Double fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorDouble``                  |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``System.Double``                      |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageRgbByte Frame(ViewLocatorRgbByte source, Extent3d size, FilterAlgorithms.FramingFunction function, RgbByte fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorRgbByte``                 |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``RgbByte``                            |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageRgbUInt16 Frame(ViewLocatorRgbUInt16 source, Extent3d size, FilterAlgorithms.FramingFunction function, RgbUInt16 fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorRgbUInt16``               |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``RgbUInt16``                          |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageRgbUInt32 Frame(ViewLocatorRgbUInt32 source, Extent3d size, FilterAlgorithms.FramingFunction function, RgbUInt32 fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorRgbUInt32``               |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``RgbUInt32``                          |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageRgbDouble Frame(ViewLocatorRgbDouble source, Extent3d size, FilterAlgorithms.FramingFunction function, RgbDouble fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorRgbDouble``               |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``RgbDouble``                          |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageRgbaByte Frame(ViewLocatorRgbaByte source, Extent3d size, FilterAlgorithms.FramingFunction function, RgbaByte fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorRgbaByte``                |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``RgbaByte``                           |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageRgbaUInt16 Frame(ViewLocatorRgbaUInt16 source, Extent3d size, FilterAlgorithms.FramingFunction function, RgbaUInt16 fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorRgbaUInt16``              |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``RgbaUInt16``                         |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageRgbaUInt32 Frame(ViewLocatorRgbaUInt32 source, Extent3d size, FilterAlgorithms.FramingFunction function, RgbaUInt32 fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorRgbaUInt32``              |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``RgbaUInt32``                         |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageRgbaDouble Frame(ViewLocatorRgbaDouble source, Extent3d size, FilterAlgorithms.FramingFunction function, RgbaDouble fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorRgbaDouble``              |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``RgbaDouble``                         |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageHlsByte Frame(ViewLocatorHlsByte source, Extent3d size, FilterAlgorithms.FramingFunction function, HlsByte fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorHlsByte``                 |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``HlsByte``                            |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageHlsUInt16 Frame(ViewLocatorHlsUInt16 source, Extent3d size, FilterAlgorithms.FramingFunction function, HlsUInt16 fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorHlsUInt16``               |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``HlsUInt16``                          |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageHlsDouble Frame(ViewLocatorHlsDouble source, Extent3d size, FilterAlgorithms.FramingFunction function, HlsDouble fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorHlsDouble``               |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``HlsDouble``                          |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageHsiByte Frame(ViewLocatorHsiByte source, Extent3d size, FilterAlgorithms.FramingFunction function, HsiByte fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorHsiByte``                 |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``HsiByte``                            |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageHsiUInt16 Frame(ViewLocatorHsiUInt16 source, Extent3d size, FilterAlgorithms.FramingFunction function, HsiUInt16 fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorHsiUInt16``               |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``HsiUInt16``                          |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageHsiDouble Frame(ViewLocatorHsiDouble source, Extent3d size, FilterAlgorithms.FramingFunction function, HsiDouble fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorHsiDouble``               |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``HsiDouble``                          |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageLabByte Frame(ViewLocatorLabByte source, Extent3d size, FilterAlgorithms.FramingFunction function, LabByte fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorLabByte``                 |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``LabByte``                            |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageLabUInt16 Frame(ViewLocatorLabUInt16 source, Extent3d size, FilterAlgorithms.FramingFunction function, LabUInt16 fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorLabUInt16``               |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``LabUInt16``                          |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageLabDouble Frame(ViewLocatorLabDouble source, Extent3d size, FilterAlgorithms.FramingFunction function, LabDouble fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorLabDouble``               |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``LabDouble``                          |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageXyzByte Frame(ViewLocatorXyzByte source, Extent3d size, FilterAlgorithms.FramingFunction function, XyzByte fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorXyzByte``                 |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``XyzByte``                            |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageXyzUInt16 Frame(ViewLocatorXyzUInt16 source, Extent3d size, FilterAlgorithms.FramingFunction function, XyzUInt16 fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorXyzUInt16``               |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``XyzUInt16``                          |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageXyzDouble Frame(ViewLocatorXyzDouble source, Extent3d size, FilterAlgorithms.FramingFunction function, XyzDouble fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorXyzDouble``               |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``XyzDouble``                          |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``Image Frame(View source, Extent3d size, FilterAlgorithms.FramingFunction function, object fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``View``                               |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``Extent3d``                           |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``object``                             |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageByte Frame(ViewLocatorByte source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, System.Byte fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorByte``                    |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``System.Byte``                        |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageUInt16 Frame(ViewLocatorUInt16 source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, System.UInt16 fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorUInt16``                  |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``System.UInt16``                      |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageUInt32 Frame(ViewLocatorUInt32 source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, System.UInt32 fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorUInt32``                  |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``System.UInt32``                      |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageDouble Frame(ViewLocatorDouble source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, System.Double fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorDouble``                  |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``System.Double``                      |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageRgbByte Frame(ViewLocatorRgbByte source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, RgbByte fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorRgbByte``                 |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``RgbByte``                            |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageRgbUInt16 Frame(ViewLocatorRgbUInt16 source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, RgbUInt16 fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorRgbUInt16``               |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``RgbUInt16``                          |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageRgbUInt32 Frame(ViewLocatorRgbUInt32 source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, RgbUInt32 fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorRgbUInt32``               |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``RgbUInt32``                          |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageRgbDouble Frame(ViewLocatorRgbDouble source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, RgbDouble fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorRgbDouble``               |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``RgbDouble``                          |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageRgbaByte Frame(ViewLocatorRgbaByte source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, RgbaByte fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorRgbaByte``                |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``RgbaByte``                           |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageRgbaUInt16 Frame(ViewLocatorRgbaUInt16 source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, RgbaUInt16 fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorRgbaUInt16``              |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``RgbaUInt16``                         |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageRgbaUInt32 Frame(ViewLocatorRgbaUInt32 source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, RgbaUInt32 fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorRgbaUInt32``              |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``RgbaUInt32``                         |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageRgbaDouble Frame(ViewLocatorRgbaDouble source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, RgbaDouble fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorRgbaDouble``              |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``RgbaDouble``                         |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageHlsByte Frame(ViewLocatorHlsByte source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, HlsByte fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorHlsByte``                 |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``HlsByte``                            |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageHlsUInt16 Frame(ViewLocatorHlsUInt16 source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, HlsUInt16 fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorHlsUInt16``               |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``HlsUInt16``                          |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageHlsDouble Frame(ViewLocatorHlsDouble source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, HlsDouble fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorHlsDouble``               |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``HlsDouble``                          |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageHsiByte Frame(ViewLocatorHsiByte source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, HsiByte fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorHsiByte``                 |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``HsiByte``                            |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageHsiUInt16 Frame(ViewLocatorHsiUInt16 source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, HsiUInt16 fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorHsiUInt16``               |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``HsiUInt16``                          |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageHsiDouble Frame(ViewLocatorHsiDouble source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, HsiDouble fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorHsiDouble``               |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``HsiDouble``                          |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageLabByte Frame(ViewLocatorLabByte source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, LabByte fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorLabByte``                 |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``LabByte``                            |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageLabUInt16 Frame(ViewLocatorLabUInt16 source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, LabUInt16 fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorLabUInt16``               |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``LabUInt16``                          |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageLabDouble Frame(ViewLocatorLabDouble source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, LabDouble fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorLabDouble``               |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``LabDouble``                          |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageXyzByte Frame(ViewLocatorXyzByte source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, XyzByte fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorXyzByte``                 |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``XyzByte``                            |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageXyzUInt16 Frame(ViewLocatorXyzUInt16 source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, XyzUInt16 fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorXyzUInt16``               |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``XyzUInt16``                          |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``ImageXyzDouble Frame(ViewLocatorXyzDouble source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, XyzDouble fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``ViewLocatorXyzDouble``               |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``XyzDouble``                          |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Frame*
^^^^^^^^^^^^^^

``Image Frame(View source, ThicknessInt32 size, FilterAlgorithms.FramingFunction function, object fill)``

Puts a frame around a view.

The method **Frame** has the following parameters:

+----------------+----------------------------------------+---------------+
| Parameter      | Type                                   | Description   |
+================+========================================+===============+
| ``source``     | ``View``                               |               |
+----------------+----------------------------------------+---------------+
| ``size``       | ``ThicknessInt32``                     |               |
+----------------+----------------------------------------+---------------+
| ``function``   | ``FilterAlgorithms.FramingFunction``   |               |
+----------------+----------------------------------------+---------------+
| ``fill``       | ``object``                             |               |
+----------------+----------------------------------------+---------------+

The type of the frame is selectable.

/return The framed image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageByte Lowpass(ViewLocatorByte source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``size``     | ``VectorInt32``       |               |
+--------------+-----------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageUInt16 Lowpass(ViewLocatorUInt16 source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``size``     | ``VectorInt32``         |               |
+--------------+-------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageUInt32 Lowpass(ViewLocatorUInt32 source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``size``     | ``VectorInt32``         |               |
+--------------+-------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageDouble Lowpass(ViewLocatorDouble source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``size``     | ``VectorInt32``         |               |
+--------------+-------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageRgbByte Lowpass(ViewLocatorRgbByte source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``VectorInt32``          |               |
+--------------+--------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageRgbUInt16 Lowpass(ViewLocatorRgbUInt16 source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageRgbUInt32 Lowpass(ViewLocatorRgbUInt32 source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageRgbDouble Lowpass(ViewLocatorRgbDouble source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageRgbaByte Lowpass(ViewLocatorRgbaByte source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``size``     | ``VectorInt32``           |               |
+--------------+---------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Lowpass(ViewLocatorRgbaUInt16 source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``VectorInt32``             |               |
+--------------+-----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Lowpass(ViewLocatorRgbaUInt32 source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``VectorInt32``             |               |
+--------------+-----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageRgbaDouble Lowpass(ViewLocatorRgbaDouble source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``VectorInt32``             |               |
+--------------+-----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageHlsByte Lowpass(ViewLocatorHlsByte source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``VectorInt32``          |               |
+--------------+--------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageHlsUInt16 Lowpass(ViewLocatorHlsUInt16 source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageHlsDouble Lowpass(ViewLocatorHlsDouble source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageHsiByte Lowpass(ViewLocatorHsiByte source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``VectorInt32``          |               |
+--------------+--------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageHsiUInt16 Lowpass(ViewLocatorHsiUInt16 source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageHsiDouble Lowpass(ViewLocatorHsiDouble source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageLabByte Lowpass(ViewLocatorLabByte source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``VectorInt32``          |               |
+--------------+--------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageLabUInt16 Lowpass(ViewLocatorLabUInt16 source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageLabDouble Lowpass(ViewLocatorLabDouble source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageXyzByte Lowpass(ViewLocatorXyzByte source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``VectorInt32``          |               |
+--------------+--------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageXyzUInt16 Lowpass(ViewLocatorXyzUInt16 source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageXyzDouble Lowpass(ViewLocatorXyzDouble source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``Image Lowpass(View source, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+-------------------+---------------+
| Parameter    | Type              | Description   |
+==============+===================+===============+
| ``source``   | ``View``          |               |
+--------------+-------------------+---------------+
| ``size``     | ``VectorInt32``   |               |
+--------------+-------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageByte Lowpass(ViewLocatorByte source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``aoi``      | ``Region``            |               |
+--------------+-----------------------+---------------+
| ``size``     | ``VectorInt32``       |               |
+--------------+-----------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageUInt16 Lowpass(ViewLocatorUInt16 source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``size``     | ``VectorInt32``         |               |
+--------------+-------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageUInt32 Lowpass(ViewLocatorUInt32 source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``size``     | ``VectorInt32``         |               |
+--------------+-------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageDouble Lowpass(ViewLocatorDouble source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``size``     | ``VectorInt32``         |               |
+--------------+-------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageRgbByte Lowpass(ViewLocatorRgbByte source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``VectorInt32``          |               |
+--------------+--------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageRgbUInt16 Lowpass(ViewLocatorRgbUInt16 source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageRgbUInt32 Lowpass(ViewLocatorRgbUInt32 source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageRgbDouble Lowpass(ViewLocatorRgbDouble source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageRgbaByte Lowpass(ViewLocatorRgbaByte source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``aoi``      | ``Region``                |               |
+--------------+---------------------------+---------------+
| ``size``     | ``VectorInt32``           |               |
+--------------+---------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Lowpass(ViewLocatorRgbaUInt16 source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``VectorInt32``             |               |
+--------------+-----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Lowpass(ViewLocatorRgbaUInt32 source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``VectorInt32``             |               |
+--------------+-----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageRgbaDouble Lowpass(ViewLocatorRgbaDouble source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``VectorInt32``             |               |
+--------------+-----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageHlsByte Lowpass(ViewLocatorHlsByte source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``VectorInt32``          |               |
+--------------+--------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageHlsUInt16 Lowpass(ViewLocatorHlsUInt16 source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageHlsDouble Lowpass(ViewLocatorHlsDouble source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageHsiByte Lowpass(ViewLocatorHsiByte source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``VectorInt32``          |               |
+--------------+--------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageHsiUInt16 Lowpass(ViewLocatorHsiUInt16 source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageHsiDouble Lowpass(ViewLocatorHsiDouble source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageLabByte Lowpass(ViewLocatorLabByte source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``VectorInt32``          |               |
+--------------+--------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageLabUInt16 Lowpass(ViewLocatorLabUInt16 source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageLabDouble Lowpass(ViewLocatorLabDouble source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageXyzByte Lowpass(ViewLocatorXyzByte source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``VectorInt32``          |               |
+--------------+--------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageXyzUInt16 Lowpass(ViewLocatorXyzUInt16 source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``ImageXyzDouble Lowpass(ViewLocatorXyzDouble source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Lowpass*
^^^^^^^^^^^^^^^^

``Image Lowpass(View source, Region aoi, VectorInt32 size)``

Filters an image using a lowpass.

The method **Lowpass** has the following parameters:

+--------------+-------------------+---------------+
| Parameter    | Type              | Description   |
+==============+===================+===============+
| ``source``   | ``View``          |               |
+--------------+-------------------+---------------+
| ``aoi``      | ``Region``        |               |
+--------------+-------------------+---------------+
| ``size``     | ``VectorInt32``   |               |
+--------------+-------------------+---------------+

The function applies a lowpass filter. The kernel is rectangular with all n x m values set to 1 / (n\*m).

The effect of a lowpass filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageByte Gauss(ViewLocatorByte source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``size``     | ``System.Int32``      |               |
+--------------+-----------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageUInt16 Gauss(ViewLocatorUInt16 source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageUInt32 Gauss(ViewLocatorUInt32 source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageDouble Gauss(ViewLocatorDouble source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageRgbByte Gauss(ViewLocatorRgbByte source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageRgbUInt16 Gauss(ViewLocatorRgbUInt16 source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageRgbUInt32 Gauss(ViewLocatorRgbUInt32 source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageRgbDouble Gauss(ViewLocatorRgbDouble source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageRgbaByte Gauss(ViewLocatorRgbaByte source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``size``     | ``System.Int32``          |               |
+--------------+---------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageRgbaUInt16 Gauss(ViewLocatorRgbaUInt16 source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageRgbaUInt32 Gauss(ViewLocatorRgbaUInt32 source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageRgbaDouble Gauss(ViewLocatorRgbaDouble source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageHlsByte Gauss(ViewLocatorHlsByte source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageHlsUInt16 Gauss(ViewLocatorHlsUInt16 source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageHlsDouble Gauss(ViewLocatorHlsDouble source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageHsiByte Gauss(ViewLocatorHsiByte source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageHsiUInt16 Gauss(ViewLocatorHsiUInt16 source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageHsiDouble Gauss(ViewLocatorHsiDouble source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageLabByte Gauss(ViewLocatorLabByte source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageLabUInt16 Gauss(ViewLocatorLabUInt16 source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageLabDouble Gauss(ViewLocatorLabDouble source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageXyzByte Gauss(ViewLocatorXyzByte source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageXyzUInt16 Gauss(ViewLocatorXyzUInt16 source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageXyzDouble Gauss(ViewLocatorXyzDouble source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``Image Gauss(View source, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``source``   | ``View``           |               |
+--------------+--------------------+---------------+
| ``size``     | ``System.Int32``   |               |
+--------------+--------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageByte Gauss(ViewLocatorByte source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``aoi``      | ``Region``            |               |
+--------------+-----------------------+---------------+
| ``size``     | ``System.Int32``      |               |
+--------------+-----------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageUInt16 Gauss(ViewLocatorUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageUInt32 Gauss(ViewLocatorUInt32 source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageDouble Gauss(ViewLocatorDouble source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageRgbByte Gauss(ViewLocatorRgbByte source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageRgbUInt16 Gauss(ViewLocatorRgbUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageRgbUInt32 Gauss(ViewLocatorRgbUInt32 source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageRgbDouble Gauss(ViewLocatorRgbDouble source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageRgbaByte Gauss(ViewLocatorRgbaByte source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``aoi``      | ``Region``                |               |
+--------------+---------------------------+---------------+
| ``size``     | ``System.Int32``          |               |
+--------------+---------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageRgbaUInt16 Gauss(ViewLocatorRgbaUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageRgbaUInt32 Gauss(ViewLocatorRgbaUInt32 source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageRgbaDouble Gauss(ViewLocatorRgbaDouble source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageHlsByte Gauss(ViewLocatorHlsByte source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageHlsUInt16 Gauss(ViewLocatorHlsUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageHlsDouble Gauss(ViewLocatorHlsDouble source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageHsiByte Gauss(ViewLocatorHsiByte source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageHsiUInt16 Gauss(ViewLocatorHsiUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageHsiDouble Gauss(ViewLocatorHsiDouble source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageLabByte Gauss(ViewLocatorLabByte source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageLabUInt16 Gauss(ViewLocatorLabUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageLabDouble Gauss(ViewLocatorLabDouble source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageXyzByte Gauss(ViewLocatorXyzByte source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageXyzUInt16 Gauss(ViewLocatorXyzUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``ImageXyzDouble Gauss(ViewLocatorXyzDouble source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gauss*
^^^^^^^^^^^^^^

``Image Gauss(View source, Region aoi, System.Int32 size)``

Filter an image using a gauss kernel.

The method **Gauss** has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``source``   | ``View``           |               |
+--------------+--------------------+---------------+
| ``aoi``      | ``Region``         |               |
+--------------+--------------------+---------------+
| ``size``     | ``System.Int32``   |               |
+--------------+--------------------+---------------+

The function applies a gauss filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 2 1
   2 4 2
   1 2 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    2  7  12  7  2
    7 31  52 31  7
   12 52 127 52 12
    7 31  52 31  7
    2  7  12  7  2
   </pre>

The effect of a gauss filter is a lowpass. The lowpass filter emphasizes low frequencies and attenuates high frequencies. The strength of the lowpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageByte Gaussian(ViewLocatorByte source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``sigma``    | ``System.Double``     |               |
+--------------+-----------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageUInt16 Gaussian(ViewLocatorUInt16 source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``sigma``    | ``System.Double``       |               |
+--------------+-------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageUInt32 Gaussian(ViewLocatorUInt32 source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``sigma``    | ``System.Double``       |               |
+--------------+-------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageDouble Gaussian(ViewLocatorDouble source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``sigma``    | ``System.Double``       |               |
+--------------+-------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageRgbByte Gaussian(ViewLocatorRgbByte source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``sigma``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 Gaussian(ViewLocatorRgbUInt16 source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 Gaussian(ViewLocatorRgbUInt32 source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageRgbDouble Gaussian(ViewLocatorRgbDouble source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageRgbaByte Gaussian(ViewLocatorRgbaByte source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``sigma``    | ``System.Double``         |               |
+--------------+---------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Gaussian(ViewLocatorRgbaUInt16 source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``sigma``    | ``System.Double``           |               |
+--------------+-----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Gaussian(ViewLocatorRgbaUInt32 source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``sigma``    | ``System.Double``           |               |
+--------------+-----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageRgbaDouble Gaussian(ViewLocatorRgbaDouble source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``sigma``    | ``System.Double``           |               |
+--------------+-----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageHlsByte Gaussian(ViewLocatorHlsByte source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``sigma``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 Gaussian(ViewLocatorHlsUInt16 source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageHlsDouble Gaussian(ViewLocatorHlsDouble source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageHsiByte Gaussian(ViewLocatorHsiByte source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``sigma``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 Gaussian(ViewLocatorHsiUInt16 source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageHsiDouble Gaussian(ViewLocatorHsiDouble source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageLabByte Gaussian(ViewLocatorLabByte source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``sigma``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageLabUInt16 Gaussian(ViewLocatorLabUInt16 source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageLabDouble Gaussian(ViewLocatorLabDouble source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageXyzByte Gaussian(ViewLocatorXyzByte source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``sigma``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 Gaussian(ViewLocatorXyzUInt16 source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageXyzDouble Gaussian(ViewLocatorXyzDouble source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``Image Gaussian(View source, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+---------------------+---------------+
| Parameter    | Type                | Description   |
+==============+=====================+===============+
| ``source``   | ``View``            |               |
+--------------+---------------------+---------------+
| ``sigma``    | ``System.Double``   |               |
+--------------+---------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageByte Gaussian(ViewLocatorByte source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``aoi``      | ``Region``            |               |
+--------------+-----------------------+---------------+
| ``sigma``    | ``System.Double``     |               |
+--------------+-----------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageUInt16 Gaussian(ViewLocatorUInt16 source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``sigma``    | ``System.Double``       |               |
+--------------+-------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageUInt32 Gaussian(ViewLocatorUInt32 source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``sigma``    | ``System.Double``       |               |
+--------------+-------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageDouble Gaussian(ViewLocatorDouble source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``sigma``    | ``System.Double``       |               |
+--------------+-------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageRgbByte Gaussian(ViewLocatorRgbByte source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``sigma``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 Gaussian(ViewLocatorRgbUInt16 source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 Gaussian(ViewLocatorRgbUInt32 source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageRgbDouble Gaussian(ViewLocatorRgbDouble source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageRgbaByte Gaussian(ViewLocatorRgbaByte source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``aoi``      | ``Region``                |               |
+--------------+---------------------------+---------------+
| ``sigma``    | ``System.Double``         |               |
+--------------+---------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Gaussian(ViewLocatorRgbaUInt16 source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``sigma``    | ``System.Double``           |               |
+--------------+-----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Gaussian(ViewLocatorRgbaUInt32 source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``sigma``    | ``System.Double``           |               |
+--------------+-----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageRgbaDouble Gaussian(ViewLocatorRgbaDouble source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``sigma``    | ``System.Double``           |               |
+--------------+-----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageHlsByte Gaussian(ViewLocatorHlsByte source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``sigma``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 Gaussian(ViewLocatorHlsUInt16 source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageHlsDouble Gaussian(ViewLocatorHlsDouble source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageHsiByte Gaussian(ViewLocatorHsiByte source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``sigma``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 Gaussian(ViewLocatorHsiUInt16 source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageHsiDouble Gaussian(ViewLocatorHsiDouble source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageLabByte Gaussian(ViewLocatorLabByte source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``sigma``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageLabUInt16 Gaussian(ViewLocatorLabUInt16 source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageLabDouble Gaussian(ViewLocatorLabDouble source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageXyzByte Gaussian(ViewLocatorXyzByte source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``sigma``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 Gaussian(ViewLocatorXyzUInt16 source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``ImageXyzDouble Gaussian(ViewLocatorXyzDouble source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Gaussian*
^^^^^^^^^^^^^^^^^

``Image Gaussian(View source, Region aoi, System.Double sigma)``

Filter an image using a gaussian kernel.

The method **Gaussian** has the following parameters:

+--------------+---------------------+---------------+
| Parameter    | Type                | Description   |
+==============+=====================+===============+
| ``source``   | ``View``            |               |
+--------------+---------------------+---------------+
| ``aoi``      | ``Region``          |               |
+--------------+---------------------+---------------+
| ``sigma``    | ``System.Double``   |               |
+--------------+---------------------+---------------+

The gaussian filter is considered an optimal lowpass or bluring filter. It is isotropic if the filter kernel is large enough for a sufficient approximation at least 5x5, i.e. sigma > 5/3. It behaves well in frequency space and is clearly superior to a box filter. The kernel size of the gaussian is 3 times sigma, rounded up to the next odd integer.

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte UnsharpMasking(ViewLocatorByte source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``alpha``    | ``System.Double``     |               |
+--------------+-----------------------+---------------+
| ``sigma``    | ``System.Double``     |               |
+--------------+-----------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 UnsharpMasking(ViewLocatorUInt16 source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``alpha``    | ``System.Double``       |               |
+--------------+-------------------------+---------------+
| ``sigma``    | ``System.Double``       |               |
+--------------+-------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 UnsharpMasking(ViewLocatorUInt32 source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``alpha``    | ``System.Double``       |               |
+--------------+-------------------------+---------------+
| ``sigma``    | ``System.Double``       |               |
+--------------+-------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble UnsharpMasking(ViewLocatorDouble source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``alpha``    | ``System.Double``       |               |
+--------------+-------------------------+---------------+
| ``sigma``    | ``System.Double``       |               |
+--------------+-------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte UnsharpMasking(ViewLocatorRgbByte source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``alpha``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+
| ``sigma``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 UnsharpMasking(ViewLocatorRgbUInt16 source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 UnsharpMasking(ViewLocatorRgbUInt32 source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble UnsharpMasking(ViewLocatorRgbDouble source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte UnsharpMasking(ViewLocatorRgbaByte source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``alpha``    | ``System.Double``         |               |
+--------------+---------------------------+---------------+
| ``sigma``    | ``System.Double``         |               |
+--------------+---------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 UnsharpMasking(ViewLocatorRgbaUInt16 source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``alpha``    | ``System.Double``           |               |
+--------------+-----------------------------+---------------+
| ``sigma``    | ``System.Double``           |               |
+--------------+-----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 UnsharpMasking(ViewLocatorRgbaUInt32 source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``alpha``    | ``System.Double``           |               |
+--------------+-----------------------------+---------------+
| ``sigma``    | ``System.Double``           |               |
+--------------+-----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble UnsharpMasking(ViewLocatorRgbaDouble source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``alpha``    | ``System.Double``           |               |
+--------------+-----------------------------+---------------+
| ``sigma``    | ``System.Double``           |               |
+--------------+-----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte UnsharpMasking(ViewLocatorHlsByte source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``alpha``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+
| ``sigma``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 UnsharpMasking(ViewLocatorHlsUInt16 source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble UnsharpMasking(ViewLocatorHlsDouble source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte UnsharpMasking(ViewLocatorHsiByte source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``alpha``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+
| ``sigma``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 UnsharpMasking(ViewLocatorHsiUInt16 source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble UnsharpMasking(ViewLocatorHsiDouble source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte UnsharpMasking(ViewLocatorLabByte source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``alpha``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+
| ``sigma``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 UnsharpMasking(ViewLocatorLabUInt16 source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble UnsharpMasking(ViewLocatorLabDouble source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte UnsharpMasking(ViewLocatorXyzByte source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``alpha``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+
| ``sigma``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 UnsharpMasking(ViewLocatorXyzUInt16 source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble UnsharpMasking(ViewLocatorXyzDouble source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``Image UnsharpMasking(View source, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+---------------------+---------------+
| Parameter    | Type                | Description   |
+==============+=====================+===============+
| ``source``   | ``View``            |               |
+--------------+---------------------+---------------+
| ``alpha``    | ``System.Double``   |               |
+--------------+---------------------+---------------+
| ``sigma``    | ``System.Double``   |               |
+--------------+---------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte UnsharpMasking(ViewLocatorByte source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``aoi``      | ``Region``            |               |
+--------------+-----------------------+---------------+
| ``alpha``    | ``System.Double``     |               |
+--------------+-----------------------+---------------+
| ``sigma``    | ``System.Double``     |               |
+--------------+-----------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 UnsharpMasking(ViewLocatorUInt16 source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``alpha``    | ``System.Double``       |               |
+--------------+-------------------------+---------------+
| ``sigma``    | ``System.Double``       |               |
+--------------+-------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 UnsharpMasking(ViewLocatorUInt32 source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``alpha``    | ``System.Double``       |               |
+--------------+-------------------------+---------------+
| ``sigma``    | ``System.Double``       |               |
+--------------+-------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble UnsharpMasking(ViewLocatorDouble source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``alpha``    | ``System.Double``       |               |
+--------------+-------------------------+---------------+
| ``sigma``    | ``System.Double``       |               |
+--------------+-------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte UnsharpMasking(ViewLocatorRgbByte source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``alpha``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+
| ``sigma``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 UnsharpMasking(ViewLocatorRgbUInt16 source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 UnsharpMasking(ViewLocatorRgbUInt32 source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble UnsharpMasking(ViewLocatorRgbDouble source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte UnsharpMasking(ViewLocatorRgbaByte source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``aoi``      | ``Region``                |               |
+--------------+---------------------------+---------------+
| ``alpha``    | ``System.Double``         |               |
+--------------+---------------------------+---------------+
| ``sigma``    | ``System.Double``         |               |
+--------------+---------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 UnsharpMasking(ViewLocatorRgbaUInt16 source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``alpha``    | ``System.Double``           |               |
+--------------+-----------------------------+---------------+
| ``sigma``    | ``System.Double``           |               |
+--------------+-----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 UnsharpMasking(ViewLocatorRgbaUInt32 source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``alpha``    | ``System.Double``           |               |
+--------------+-----------------------------+---------------+
| ``sigma``    | ``System.Double``           |               |
+--------------+-----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble UnsharpMasking(ViewLocatorRgbaDouble source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``alpha``    | ``System.Double``           |               |
+--------------+-----------------------------+---------------+
| ``sigma``    | ``System.Double``           |               |
+--------------+-----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte UnsharpMasking(ViewLocatorHlsByte source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``alpha``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+
| ``sigma``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 UnsharpMasking(ViewLocatorHlsUInt16 source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble UnsharpMasking(ViewLocatorHlsDouble source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte UnsharpMasking(ViewLocatorHsiByte source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``alpha``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+
| ``sigma``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 UnsharpMasking(ViewLocatorHsiUInt16 source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble UnsharpMasking(ViewLocatorHsiDouble source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte UnsharpMasking(ViewLocatorLabByte source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``alpha``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+
| ``sigma``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 UnsharpMasking(ViewLocatorLabUInt16 source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble UnsharpMasking(ViewLocatorLabDouble source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte UnsharpMasking(ViewLocatorXyzByte source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``alpha``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+
| ``sigma``    | ``System.Double``        |               |
+--------------+--------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 UnsharpMasking(ViewLocatorXyzUInt16 source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble UnsharpMasking(ViewLocatorXyzDouble source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *UnsharpMasking*
^^^^^^^^^^^^^^^^^^^^^^^

``Image UnsharpMasking(View source, Region aoi, System.Double alpha, System.Double sigma)``

Sharpen an image using unsharp masking.

The method **UnsharpMasking** has the following parameters:

+--------------+---------------------+---------------+
| Parameter    | Type                | Description   |
+==============+=====================+===============+
| ``source``   | ``View``            |               |
+--------------+---------------------+---------------+
| ``aoi``      | ``Region``          |               |
+--------------+---------------------+---------------+
| ``alpha``    | ``System.Double``   |               |
+--------------+---------------------+---------------+
| ``sigma``    | ``System.Double``   |               |
+--------------+---------------------+---------------+

Unsharp masking sharpens the image by subtracting the blurred image from the original and adding a part of this difference back to the original.

The blurring part is done with a gaussian filter. .

The filter kernel corresponds to a two-dimensional gaussian, where sigma corresponds to the width of the bell-shaped curve and r = (x^2 + y^2) is the distance from the center:

G(x, y) = e ^ -(x^2 + y^2)/(2\*sigma^2)

The formula for the unsharp masking is:

D(x, y) = S(x, y) + alpha(S(x,y) - G(x,y))

The function uses a separable gaussian filter implementation and supports parallelization on multiple cores.

The region parameter refers to the source view and constrains the function to an aoi inside of the region only. Pixels outside the region are not touched.

/return The filtered image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageByte Hipass(ViewLocatorByte source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``size``     | ``System.Int32``      |               |
+--------------+-----------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageUInt16 Hipass(ViewLocatorUInt16 source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageUInt32 Hipass(ViewLocatorUInt32 source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageDouble Hipass(ViewLocatorDouble source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageRgbByte Hipass(ViewLocatorRgbByte source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageRgbUInt16 Hipass(ViewLocatorRgbUInt16 source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageRgbUInt32 Hipass(ViewLocatorRgbUInt32 source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageRgbDouble Hipass(ViewLocatorRgbDouble source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageRgbaByte Hipass(ViewLocatorRgbaByte source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``size``     | ``System.Int32``          |               |
+--------------+---------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Hipass(ViewLocatorRgbaUInt16 source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Hipass(ViewLocatorRgbaUInt32 source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageRgbaDouble Hipass(ViewLocatorRgbaDouble source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageHlsByte Hipass(ViewLocatorHlsByte source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageHlsUInt16 Hipass(ViewLocatorHlsUInt16 source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageHlsDouble Hipass(ViewLocatorHlsDouble source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageHsiByte Hipass(ViewLocatorHsiByte source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageHsiUInt16 Hipass(ViewLocatorHsiUInt16 source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageHsiDouble Hipass(ViewLocatorHsiDouble source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageLabByte Hipass(ViewLocatorLabByte source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageLabUInt16 Hipass(ViewLocatorLabUInt16 source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageLabDouble Hipass(ViewLocatorLabDouble source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageXyzByte Hipass(ViewLocatorXyzByte source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageXyzUInt16 Hipass(ViewLocatorXyzUInt16 source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageXyzDouble Hipass(ViewLocatorXyzDouble source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``Image Hipass(View source, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``source``   | ``View``           |               |
+--------------+--------------------+---------------+
| ``size``     | ``System.Int32``   |               |
+--------------+--------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageByte Hipass(ViewLocatorByte source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``aoi``      | ``Region``            |               |
+--------------+-----------------------+---------------+
| ``size``     | ``System.Int32``      |               |
+--------------+-----------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageUInt16 Hipass(ViewLocatorUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageUInt32 Hipass(ViewLocatorUInt32 source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageDouble Hipass(ViewLocatorDouble source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageRgbByte Hipass(ViewLocatorRgbByte source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageRgbUInt16 Hipass(ViewLocatorRgbUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageRgbUInt32 Hipass(ViewLocatorRgbUInt32 source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageRgbDouble Hipass(ViewLocatorRgbDouble source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageRgbaByte Hipass(ViewLocatorRgbaByte source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``aoi``      | ``Region``                |               |
+--------------+---------------------------+---------------+
| ``size``     | ``System.Int32``          |               |
+--------------+---------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Hipass(ViewLocatorRgbaUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Hipass(ViewLocatorRgbaUInt32 source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageRgbaDouble Hipass(ViewLocatorRgbaDouble source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageHlsByte Hipass(ViewLocatorHlsByte source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageHlsUInt16 Hipass(ViewLocatorHlsUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageHlsDouble Hipass(ViewLocatorHlsDouble source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageHsiByte Hipass(ViewLocatorHsiByte source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageHsiUInt16 Hipass(ViewLocatorHsiUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageHsiDouble Hipass(ViewLocatorHsiDouble source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageLabByte Hipass(ViewLocatorLabByte source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageLabUInt16 Hipass(ViewLocatorLabUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageLabDouble Hipass(ViewLocatorLabDouble source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageXyzByte Hipass(ViewLocatorXyzByte source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageXyzUInt16 Hipass(ViewLocatorXyzUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``ImageXyzDouble Hipass(ViewLocatorXyzDouble source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Hipass*
^^^^^^^^^^^^^^^

``Image Hipass(View source, Region aoi, System.Int32 size)``

Filter an image using a hipass kernel.

The method **Hipass** has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``source``   | ``View``           |               |
+--------------+--------------------+---------------+
| ``aoi``      | ``Region``         |               |
+--------------+--------------------+---------------+
| ``size``     | ``System.Int32``   |               |
+--------------+--------------------+---------------+

The function applies a hipass filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 24 -1 -1
   -1 -1 -1 -1 -1
   -1 -1 -1 -1 -1
   </pre>

The effect of a hipass filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte HorizontalPrewitt(ViewLocatorByte source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 HorizontalPrewitt(ViewLocatorUInt16 source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 HorizontalPrewitt(ViewLocatorUInt32 source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble HorizontalPrewitt(ViewLocatorDouble source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte HorizontalPrewitt(ViewLocatorRgbByte source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 HorizontalPrewitt(ViewLocatorRgbUInt16 source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 HorizontalPrewitt(ViewLocatorRgbUInt32 source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble HorizontalPrewitt(ViewLocatorRgbDouble source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte HorizontalPrewitt(ViewLocatorRgbaByte source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 HorizontalPrewitt(ViewLocatorRgbaUInt16 source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 HorizontalPrewitt(ViewLocatorRgbaUInt32 source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble HorizontalPrewitt(ViewLocatorRgbaDouble source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte HorizontalPrewitt(ViewLocatorHlsByte source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 HorizontalPrewitt(ViewLocatorHlsUInt16 source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble HorizontalPrewitt(ViewLocatorHlsDouble source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte HorizontalPrewitt(ViewLocatorHsiByte source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 HorizontalPrewitt(ViewLocatorHsiUInt16 source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble HorizontalPrewitt(ViewLocatorHsiDouble source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte HorizontalPrewitt(ViewLocatorLabByte source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 HorizontalPrewitt(ViewLocatorLabUInt16 source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble HorizontalPrewitt(ViewLocatorLabDouble source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte HorizontalPrewitt(ViewLocatorXyzByte source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 HorizontalPrewitt(ViewLocatorXyzUInt16 source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble HorizontalPrewitt(ViewLocatorXyzDouble source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``Image HorizontalPrewitt(View source)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte HorizontalPrewitt(ViewLocatorByte source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``aoi``      | ``Region``            |               |
+--------------+-----------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 HorizontalPrewitt(ViewLocatorUInt16 source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 HorizontalPrewitt(ViewLocatorUInt32 source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble HorizontalPrewitt(ViewLocatorDouble source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte HorizontalPrewitt(ViewLocatorRgbByte source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 HorizontalPrewitt(ViewLocatorRgbUInt16 source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 HorizontalPrewitt(ViewLocatorRgbUInt32 source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble HorizontalPrewitt(ViewLocatorRgbDouble source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte HorizontalPrewitt(ViewLocatorRgbaByte source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``aoi``      | ``Region``                |               |
+--------------+---------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 HorizontalPrewitt(ViewLocatorRgbaUInt16 source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 HorizontalPrewitt(ViewLocatorRgbaUInt32 source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble HorizontalPrewitt(ViewLocatorRgbaDouble source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte HorizontalPrewitt(ViewLocatorHlsByte source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 HorizontalPrewitt(ViewLocatorHlsUInt16 source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble HorizontalPrewitt(ViewLocatorHlsDouble source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte HorizontalPrewitt(ViewLocatorHsiByte source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 HorizontalPrewitt(ViewLocatorHsiUInt16 source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble HorizontalPrewitt(ViewLocatorHsiDouble source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte HorizontalPrewitt(ViewLocatorLabByte source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 HorizontalPrewitt(ViewLocatorLabUInt16 source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble HorizontalPrewitt(ViewLocatorLabDouble source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte HorizontalPrewitt(ViewLocatorXyzByte source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 HorizontalPrewitt(ViewLocatorXyzUInt16 source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble HorizontalPrewitt(ViewLocatorXyzDouble source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``Image HorizontalPrewitt(View source, Region aoi)``

Filter an image using a horizontal prewitt filter.

The method **HorizontalPrewitt** has the following parameters:

+--------------+--------------+---------------+
| Parameter    | Type         | Description   |
+==============+==============+===============+
| ``source``   | ``View``     |               |
+--------------+--------------+---------------+
| ``aoi``      | ``Region``   |               |
+--------------+--------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  1  1
    0  0  0
   -1 -1 -1
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte HorizontalScharr(ViewLocatorByte source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 HorizontalScharr(ViewLocatorUInt16 source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 HorizontalScharr(ViewLocatorUInt32 source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble HorizontalScharr(ViewLocatorDouble source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte HorizontalScharr(ViewLocatorRgbByte source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 HorizontalScharr(ViewLocatorRgbUInt16 source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 HorizontalScharr(ViewLocatorRgbUInt32 source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble HorizontalScharr(ViewLocatorRgbDouble source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte HorizontalScharr(ViewLocatorRgbaByte source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 HorizontalScharr(ViewLocatorRgbaUInt16 source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 HorizontalScharr(ViewLocatorRgbaUInt32 source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble HorizontalScharr(ViewLocatorRgbaDouble source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte HorizontalScharr(ViewLocatorHlsByte source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 HorizontalScharr(ViewLocatorHlsUInt16 source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble HorizontalScharr(ViewLocatorHlsDouble source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte HorizontalScharr(ViewLocatorHsiByte source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 HorizontalScharr(ViewLocatorHsiUInt16 source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble HorizontalScharr(ViewLocatorHsiDouble source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte HorizontalScharr(ViewLocatorLabByte source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 HorizontalScharr(ViewLocatorLabUInt16 source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble HorizontalScharr(ViewLocatorLabDouble source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte HorizontalScharr(ViewLocatorXyzByte source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 HorizontalScharr(ViewLocatorXyzUInt16 source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble HorizontalScharr(ViewLocatorXyzDouble source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``Image HorizontalScharr(View source)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte HorizontalScharr(ViewLocatorByte source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``aoi``      | ``Region``            |               |
+--------------+-----------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 HorizontalScharr(ViewLocatorUInt16 source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 HorizontalScharr(ViewLocatorUInt32 source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble HorizontalScharr(ViewLocatorDouble source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte HorizontalScharr(ViewLocatorRgbByte source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 HorizontalScharr(ViewLocatorRgbUInt16 source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 HorizontalScharr(ViewLocatorRgbUInt32 source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble HorizontalScharr(ViewLocatorRgbDouble source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte HorizontalScharr(ViewLocatorRgbaByte source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``aoi``      | ``Region``                |               |
+--------------+---------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 HorizontalScharr(ViewLocatorRgbaUInt16 source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 HorizontalScharr(ViewLocatorRgbaUInt32 source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble HorizontalScharr(ViewLocatorRgbaDouble source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte HorizontalScharr(ViewLocatorHlsByte source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 HorizontalScharr(ViewLocatorHlsUInt16 source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble HorizontalScharr(ViewLocatorHlsDouble source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte HorizontalScharr(ViewLocatorHsiByte source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 HorizontalScharr(ViewLocatorHsiUInt16 source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble HorizontalScharr(ViewLocatorHsiDouble source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte HorizontalScharr(ViewLocatorLabByte source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 HorizontalScharr(ViewLocatorLabUInt16 source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble HorizontalScharr(ViewLocatorLabDouble source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte HorizontalScharr(ViewLocatorXyzByte source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 HorizontalScharr(ViewLocatorXyzUInt16 source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble HorizontalScharr(ViewLocatorXyzDouble source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalScharr*
^^^^^^^^^^^^^^^^^^^^^^^^^

``Image HorizontalScharr(View source, Region aoi)``

Filter an image using a horizontal scharr filter.

The method **HorizontalScharr** has the following parameters:

+--------------+--------------+---------------+
| Parameter    | Type         | Description   |
+==============+==============+===============+
| ``source``   | ``View``     |               |
+--------------+--------------+---------------+
| ``aoi``      | ``Region``   |               |
+--------------+--------------+---------------+

The function applies a horizontal prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3  10  3
    0   0  0
   -3 -10 -3
   </pre>

The filter attenuates horizontal edges while at the same time smoothing in the horizontal direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte HorizontalSobel(ViewLocatorByte source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``size``     | ``System.Int32``      |               |
+--------------+-----------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 HorizontalSobel(ViewLocatorUInt16 source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 HorizontalSobel(ViewLocatorUInt32 source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble HorizontalSobel(ViewLocatorDouble source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte HorizontalSobel(ViewLocatorRgbByte source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 HorizontalSobel(ViewLocatorRgbUInt16 source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 HorizontalSobel(ViewLocatorRgbUInt32 source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble HorizontalSobel(ViewLocatorRgbDouble source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte HorizontalSobel(ViewLocatorRgbaByte source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``size``     | ``System.Int32``          |               |
+--------------+---------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 HorizontalSobel(ViewLocatorRgbaUInt16 source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 HorizontalSobel(ViewLocatorRgbaUInt32 source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble HorizontalSobel(ViewLocatorRgbaDouble source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte HorizontalSobel(ViewLocatorHlsByte source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 HorizontalSobel(ViewLocatorHlsUInt16 source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble HorizontalSobel(ViewLocatorHlsDouble source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte HorizontalSobel(ViewLocatorHsiByte source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 HorizontalSobel(ViewLocatorHsiUInt16 source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble HorizontalSobel(ViewLocatorHsiDouble source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte HorizontalSobel(ViewLocatorLabByte source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 HorizontalSobel(ViewLocatorLabUInt16 source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble HorizontalSobel(ViewLocatorLabDouble source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte HorizontalSobel(ViewLocatorXyzByte source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 HorizontalSobel(ViewLocatorXyzUInt16 source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble HorizontalSobel(ViewLocatorXyzDouble source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``Image HorizontalSobel(View source, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``source``   | ``View``           |               |
+--------------+--------------------+---------------+
| ``size``     | ``System.Int32``   |               |
+--------------+--------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte HorizontalSobel(ViewLocatorByte source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``aoi``      | ``Region``            |               |
+--------------+-----------------------+---------------+
| ``size``     | ``System.Int32``      |               |
+--------------+-----------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 HorizontalSobel(ViewLocatorUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 HorizontalSobel(ViewLocatorUInt32 source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble HorizontalSobel(ViewLocatorDouble source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte HorizontalSobel(ViewLocatorRgbByte source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 HorizontalSobel(ViewLocatorRgbUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 HorizontalSobel(ViewLocatorRgbUInt32 source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble HorizontalSobel(ViewLocatorRgbDouble source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte HorizontalSobel(ViewLocatorRgbaByte source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``aoi``      | ``Region``                |               |
+--------------+---------------------------+---------------+
| ``size``     | ``System.Int32``          |               |
+--------------+---------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 HorizontalSobel(ViewLocatorRgbaUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 HorizontalSobel(ViewLocatorRgbaUInt32 source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble HorizontalSobel(ViewLocatorRgbaDouble source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte HorizontalSobel(ViewLocatorHlsByte source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 HorizontalSobel(ViewLocatorHlsUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble HorizontalSobel(ViewLocatorHlsDouble source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte HorizontalSobel(ViewLocatorHsiByte source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 HorizontalSobel(ViewLocatorHsiUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble HorizontalSobel(ViewLocatorHsiDouble source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte HorizontalSobel(ViewLocatorLabByte source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 HorizontalSobel(ViewLocatorLabUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble HorizontalSobel(ViewLocatorLabDouble source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte HorizontalSobel(ViewLocatorXyzByte source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 HorizontalSobel(ViewLocatorXyzUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble HorizontalSobel(ViewLocatorXyzDouble source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *HorizontalSobel*
^^^^^^^^^^^^^^^^^^^^^^^^

``Image HorizontalSobel(View source, Region aoi, System.Int32 size)``

Filter an image using a horizontal\_sobel kernel.

The method **HorizontalSobel** has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``source``   | ``View``           |               |
+--------------+--------------------+---------------+
| ``aoi``      | ``Region``         |               |
+--------------+--------------------+---------------+
| ``size``     | ``System.Int32``   |               |
+--------------+--------------------+---------------+

The function applies a horizontal\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    1  2  1
    0  0  0
   -1 -2 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
    1  4   6  4  1
    2  8  12  8  2
    0  0   0  0  0
   -2 -8 -12 -8 -2
   -1 -4  -6 -4 -1
   </pre>

The effect of a horizontal sobel filter is that it amplifies horizontal edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageByte Laplace(ViewLocatorByte source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``size``     | ``System.Int32``      |               |
+--------------+-----------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageUInt16 Laplace(ViewLocatorUInt16 source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageUInt32 Laplace(ViewLocatorUInt32 source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageDouble Laplace(ViewLocatorDouble source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageRgbByte Laplace(ViewLocatorRgbByte source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageRgbUInt16 Laplace(ViewLocatorRgbUInt16 source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageRgbUInt32 Laplace(ViewLocatorRgbUInt32 source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageRgbDouble Laplace(ViewLocatorRgbDouble source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageRgbaByte Laplace(ViewLocatorRgbaByte source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``size``     | ``System.Int32``          |               |
+--------------+---------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Laplace(ViewLocatorRgbaUInt16 source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Laplace(ViewLocatorRgbaUInt32 source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageRgbaDouble Laplace(ViewLocatorRgbaDouble source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageHlsByte Laplace(ViewLocatorHlsByte source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageHlsUInt16 Laplace(ViewLocatorHlsUInt16 source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageHlsDouble Laplace(ViewLocatorHlsDouble source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageHsiByte Laplace(ViewLocatorHsiByte source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageHsiUInt16 Laplace(ViewLocatorHsiUInt16 source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageHsiDouble Laplace(ViewLocatorHsiDouble source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageLabByte Laplace(ViewLocatorLabByte source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageLabUInt16 Laplace(ViewLocatorLabUInt16 source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageLabDouble Laplace(ViewLocatorLabDouble source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageXyzByte Laplace(ViewLocatorXyzByte source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageXyzUInt16 Laplace(ViewLocatorXyzUInt16 source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageXyzDouble Laplace(ViewLocatorXyzDouble source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``Image Laplace(View source, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``source``   | ``View``           |               |
+--------------+--------------------+---------------+
| ``size``     | ``System.Int32``   |               |
+--------------+--------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageByte Laplace(ViewLocatorByte source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``aoi``      | ``Region``            |               |
+--------------+-----------------------+---------------+
| ``size``     | ``System.Int32``      |               |
+--------------+-----------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageUInt16 Laplace(ViewLocatorUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageUInt32 Laplace(ViewLocatorUInt32 source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageDouble Laplace(ViewLocatorDouble source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageRgbByte Laplace(ViewLocatorRgbByte source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageRgbUInt16 Laplace(ViewLocatorRgbUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageRgbUInt32 Laplace(ViewLocatorRgbUInt32 source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageRgbDouble Laplace(ViewLocatorRgbDouble source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageRgbaByte Laplace(ViewLocatorRgbaByte source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``aoi``      | ``Region``                |               |
+--------------+---------------------------+---------------+
| ``size``     | ``System.Int32``          |               |
+--------------+---------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Laplace(ViewLocatorRgbaUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Laplace(ViewLocatorRgbaUInt32 source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageRgbaDouble Laplace(ViewLocatorRgbaDouble source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageHlsByte Laplace(ViewLocatorHlsByte source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageHlsUInt16 Laplace(ViewLocatorHlsUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageHlsDouble Laplace(ViewLocatorHlsDouble source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageHsiByte Laplace(ViewLocatorHsiByte source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageHsiUInt16 Laplace(ViewLocatorHsiUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageHsiDouble Laplace(ViewLocatorHsiDouble source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageLabByte Laplace(ViewLocatorLabByte source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageLabUInt16 Laplace(ViewLocatorLabUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageLabDouble Laplace(ViewLocatorLabDouble source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageXyzByte Laplace(ViewLocatorXyzByte source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageXyzUInt16 Laplace(ViewLocatorXyzUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``ImageXyzDouble Laplace(ViewLocatorXyzDouble source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Laplace*
^^^^^^^^^^^^^^^^

``Image Laplace(View source, Region aoi, System.Int32 size)``

Filter an image using a laplace kernel.

The method **Laplace** has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``source``   | ``View``           |               |
+--------------+--------------------+---------------+
| ``aoi``      | ``Region``         |               |
+--------------+--------------------+---------------+
| ``size``     | ``System.Int32``   |               |
+--------------+--------------------+---------------+

The function applies a laplace filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 -1 -1
   -1  8 -1
   -1 -1 -1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 -3 -4 -3 -1
   -3  0  6  0 -3 
   -4  6 20  6 -4
   -3  0  6  0 -3 
   -1 -3 -4 -3 -1
   </pre>

The effect of a laplace filter is that it amplifies high frequencies and attenuates low frequencies. The strength of the hiwpass filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte VerticalPrewitt(ViewLocatorByte source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 VerticalPrewitt(ViewLocatorUInt16 source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 VerticalPrewitt(ViewLocatorUInt32 source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble VerticalPrewitt(ViewLocatorDouble source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte VerticalPrewitt(ViewLocatorRgbByte source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 VerticalPrewitt(ViewLocatorRgbUInt16 source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 VerticalPrewitt(ViewLocatorRgbUInt32 source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble VerticalPrewitt(ViewLocatorRgbDouble source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte VerticalPrewitt(ViewLocatorRgbaByte source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 VerticalPrewitt(ViewLocatorRgbaUInt16 source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 VerticalPrewitt(ViewLocatorRgbaUInt32 source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble VerticalPrewitt(ViewLocatorRgbaDouble source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte VerticalPrewitt(ViewLocatorHlsByte source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 VerticalPrewitt(ViewLocatorHlsUInt16 source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble VerticalPrewitt(ViewLocatorHlsDouble source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte VerticalPrewitt(ViewLocatorHsiByte source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 VerticalPrewitt(ViewLocatorHsiUInt16 source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble VerticalPrewitt(ViewLocatorHsiDouble source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte VerticalPrewitt(ViewLocatorLabByte source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 VerticalPrewitt(ViewLocatorLabUInt16 source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble VerticalPrewitt(ViewLocatorLabDouble source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte VerticalPrewitt(ViewLocatorXyzByte source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 VerticalPrewitt(ViewLocatorXyzUInt16 source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble VerticalPrewitt(ViewLocatorXyzDouble source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``Image VerticalPrewitt(View source)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte VerticalPrewitt(ViewLocatorByte source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``aoi``      | ``Region``            |               |
+--------------+-----------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 VerticalPrewitt(ViewLocatorUInt16 source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 VerticalPrewitt(ViewLocatorUInt32 source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble VerticalPrewitt(ViewLocatorDouble source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte VerticalPrewitt(ViewLocatorRgbByte source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 VerticalPrewitt(ViewLocatorRgbUInt16 source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 VerticalPrewitt(ViewLocatorRgbUInt32 source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble VerticalPrewitt(ViewLocatorRgbDouble source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte VerticalPrewitt(ViewLocatorRgbaByte source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``aoi``      | ``Region``                |               |
+--------------+---------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 VerticalPrewitt(ViewLocatorRgbaUInt16 source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 VerticalPrewitt(ViewLocatorRgbaUInt32 source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble VerticalPrewitt(ViewLocatorRgbaDouble source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte VerticalPrewitt(ViewLocatorHlsByte source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 VerticalPrewitt(ViewLocatorHlsUInt16 source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble VerticalPrewitt(ViewLocatorHlsDouble source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte VerticalPrewitt(ViewLocatorHsiByte source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 VerticalPrewitt(ViewLocatorHsiUInt16 source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble VerticalPrewitt(ViewLocatorHsiDouble source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte VerticalPrewitt(ViewLocatorLabByte source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 VerticalPrewitt(ViewLocatorLabUInt16 source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble VerticalPrewitt(ViewLocatorLabDouble source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte VerticalPrewitt(ViewLocatorXyzByte source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 VerticalPrewitt(ViewLocatorXyzUInt16 source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble VerticalPrewitt(ViewLocatorXyzDouble source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalPrewitt*
^^^^^^^^^^^^^^^^^^^^^^^^

``Image VerticalPrewitt(View source, Region aoi)``

Filter an image using a vertical prewitt filter.

The method **VerticalPrewitt** has the following parameters:

+--------------+--------------+---------------+
| Parameter    | Type         | Description   |
+==============+==============+===============+
| ``source``   | ``View``     |               |
+--------------+--------------+---------------+
| ``aoi``      | ``Region``   |               |
+--------------+--------------+---------------+

The function applies a vertical prewitt filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   1 0 -1
   1 0 -1
   1 0 -1
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte VerticalScharr(ViewLocatorByte source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 VerticalScharr(ViewLocatorUInt16 source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 VerticalScharr(ViewLocatorUInt32 source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble VerticalScharr(ViewLocatorDouble source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte VerticalScharr(ViewLocatorRgbByte source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 VerticalScharr(ViewLocatorRgbUInt16 source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 VerticalScharr(ViewLocatorRgbUInt32 source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble VerticalScharr(ViewLocatorRgbDouble source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte VerticalScharr(ViewLocatorRgbaByte source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 VerticalScharr(ViewLocatorRgbaUInt16 source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 VerticalScharr(ViewLocatorRgbaUInt32 source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble VerticalScharr(ViewLocatorRgbaDouble source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte VerticalScharr(ViewLocatorHlsByte source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 VerticalScharr(ViewLocatorHlsUInt16 source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble VerticalScharr(ViewLocatorHlsDouble source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte VerticalScharr(ViewLocatorHsiByte source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 VerticalScharr(ViewLocatorHsiUInt16 source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble VerticalScharr(ViewLocatorHsiDouble source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte VerticalScharr(ViewLocatorLabByte source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 VerticalScharr(ViewLocatorLabUInt16 source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble VerticalScharr(ViewLocatorLabDouble source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte VerticalScharr(ViewLocatorXyzByte source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 VerticalScharr(ViewLocatorXyzUInt16 source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble VerticalScharr(ViewLocatorXyzDouble source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``Image VerticalScharr(View source)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte VerticalScharr(ViewLocatorByte source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``aoi``      | ``Region``            |               |
+--------------+-----------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 VerticalScharr(ViewLocatorUInt16 source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 VerticalScharr(ViewLocatorUInt32 source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble VerticalScharr(ViewLocatorDouble source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte VerticalScharr(ViewLocatorRgbByte source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 VerticalScharr(ViewLocatorRgbUInt16 source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 VerticalScharr(ViewLocatorRgbUInt32 source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble VerticalScharr(ViewLocatorRgbDouble source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte VerticalScharr(ViewLocatorRgbaByte source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``aoi``      | ``Region``                |               |
+--------------+---------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 VerticalScharr(ViewLocatorRgbaUInt16 source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 VerticalScharr(ViewLocatorRgbaUInt32 source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble VerticalScharr(ViewLocatorRgbaDouble source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte VerticalScharr(ViewLocatorHlsByte source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 VerticalScharr(ViewLocatorHlsUInt16 source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble VerticalScharr(ViewLocatorHlsDouble source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte VerticalScharr(ViewLocatorHsiByte source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 VerticalScharr(ViewLocatorHsiUInt16 source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble VerticalScharr(ViewLocatorHsiDouble source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte VerticalScharr(ViewLocatorLabByte source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 VerticalScharr(ViewLocatorLabUInt16 source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble VerticalScharr(ViewLocatorLabDouble source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte VerticalScharr(ViewLocatorXyzByte source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 VerticalScharr(ViewLocatorXyzUInt16 source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble VerticalScharr(ViewLocatorXyzDouble source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalScharr*
^^^^^^^^^^^^^^^^^^^^^^^

``Image VerticalScharr(View source, Region aoi)``

Filter an image using a vertical scharr filter.

The method **VerticalScharr** has the following parameters:

+--------------+--------------+---------------+
| Parameter    | Type         | Description   |
+==============+==============+===============+
| ``source``   | ``View``     |               |
+--------------+--------------+---------------+
| ``aoi``      | ``Region``   |               |
+--------------+--------------+---------------+

The function applies a vertical scharr filter. The corresponding kernel is a 3x3 matrix with the following values:

.. raw:: html

   <pre>
    3 0  -3
   10 0 -10
    3 0  -3
   </pre>

The filter attenuates vertical edges while at the same time smoothing in the vertical direction.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageByte VerticalSobel(ViewLocatorByte source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``size``     | ``System.Int32``      |               |
+--------------+-----------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 VerticalSobel(ViewLocatorUInt16 source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 VerticalSobel(ViewLocatorUInt32 source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble VerticalSobel(ViewLocatorDouble source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte VerticalSobel(ViewLocatorRgbByte source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 VerticalSobel(ViewLocatorRgbUInt16 source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 VerticalSobel(ViewLocatorRgbUInt32 source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble VerticalSobel(ViewLocatorRgbDouble source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte VerticalSobel(ViewLocatorRgbaByte source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``size``     | ``System.Int32``          |               |
+--------------+---------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 VerticalSobel(ViewLocatorRgbaUInt16 source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 VerticalSobel(ViewLocatorRgbaUInt32 source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble VerticalSobel(ViewLocatorRgbaDouble source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte VerticalSobel(ViewLocatorHlsByte source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 VerticalSobel(ViewLocatorHlsUInt16 source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble VerticalSobel(ViewLocatorHlsDouble source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte VerticalSobel(ViewLocatorHsiByte source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 VerticalSobel(ViewLocatorHsiUInt16 source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble VerticalSobel(ViewLocatorHsiDouble source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte VerticalSobel(ViewLocatorLabByte source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 VerticalSobel(ViewLocatorLabUInt16 source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble VerticalSobel(ViewLocatorLabDouble source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte VerticalSobel(ViewLocatorXyzByte source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 VerticalSobel(ViewLocatorXyzUInt16 source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble VerticalSobel(ViewLocatorXyzDouble source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``Image VerticalSobel(View source, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``source``   | ``View``           |               |
+--------------+--------------------+---------------+
| ``size``     | ``System.Int32``   |               |
+--------------+--------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageByte VerticalSobel(ViewLocatorByte source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``aoi``      | ``Region``            |               |
+--------------+-----------------------+---------------+
| ``size``     | ``System.Int32``      |               |
+--------------+-----------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 VerticalSobel(ViewLocatorUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 VerticalSobel(ViewLocatorUInt32 source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageDouble VerticalSobel(ViewLocatorDouble source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte VerticalSobel(ViewLocatorRgbByte source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 VerticalSobel(ViewLocatorRgbUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 VerticalSobel(ViewLocatorRgbUInt32 source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble VerticalSobel(ViewLocatorRgbDouble source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte VerticalSobel(ViewLocatorRgbaByte source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``aoi``      | ``Region``                |               |
+--------------+---------------------------+---------------+
| ``size``     | ``System.Int32``          |               |
+--------------+---------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 VerticalSobel(ViewLocatorRgbaUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 VerticalSobel(ViewLocatorRgbaUInt32 source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble VerticalSobel(ViewLocatorRgbaDouble source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte VerticalSobel(ViewLocatorHlsByte source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 VerticalSobel(ViewLocatorHlsUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble VerticalSobel(ViewLocatorHlsDouble source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte VerticalSobel(ViewLocatorHsiByte source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 VerticalSobel(ViewLocatorHsiUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble VerticalSobel(ViewLocatorHsiDouble source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte VerticalSobel(ViewLocatorLabByte source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 VerticalSobel(ViewLocatorLabUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble VerticalSobel(ViewLocatorLabDouble source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte VerticalSobel(ViewLocatorXyzByte source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 VerticalSobel(ViewLocatorXyzUInt16 source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble VerticalSobel(ViewLocatorXyzDouble source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *VerticalSobel*
^^^^^^^^^^^^^^^^^^^^^^

``Image VerticalSobel(View source, Region aoi, System.Int32 size)``

Filter an image using a vertical\_sobel kernel.

The method **VerticalSobel** has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``source``   | ``View``           |               |
+--------------+--------------------+---------------+
| ``aoi``      | ``Region``         |               |
+--------------+--------------------+---------------+
| ``size``     | ``System.Int32``   |               |
+--------------+--------------------+---------------+

The function applies a vertical\_sobel filter. The corresponding kernel is either a 3x3 matrix with the following values:

.. raw:: html

   <pre>
   -1 0 1
   -2 0 2
   -1 0 1
   </pre>

or a 5x5 kernel with the following values:

.. raw:: html

   <pre>
   -1 - 2 0  2  1
   -4  -8 0  8  4
   -6 -12 0 12  6
   -4  -8 0  8  4
   -1 - 2 0  2  1
   </pre>

The effect of a vertical sobel filter is that it amplifies vertical edges and attenuates everything else. The strength of the sobel filter depends on the size of the kernel.

The region parameter constrains the function to an aoi inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageByte Median(ViewLocatorByte source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``size``     | ``VectorInt32``       |               |
+--------------+-----------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageUInt16 Median(ViewLocatorUInt16 source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``size``     | ``VectorInt32``         |               |
+--------------+-------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageUInt32 Median(ViewLocatorUInt32 source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``size``     | ``VectorInt32``         |               |
+--------------+-------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageDouble Median(ViewLocatorDouble source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``size``     | ``VectorInt32``         |               |
+--------------+-------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageRgbByte Median(ViewLocatorRgbByte source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``VectorInt32``          |               |
+--------------+--------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageRgbUInt16 Median(ViewLocatorRgbUInt16 source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageRgbUInt32 Median(ViewLocatorRgbUInt32 source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageRgbDouble Median(ViewLocatorRgbDouble source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageRgbaByte Median(ViewLocatorRgbaByte source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``size``     | ``VectorInt32``           |               |
+--------------+---------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Median(ViewLocatorRgbaUInt16 source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``VectorInt32``             |               |
+--------------+-----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Median(ViewLocatorRgbaUInt32 source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``VectorInt32``             |               |
+--------------+-----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageRgbaDouble Median(ViewLocatorRgbaDouble source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``VectorInt32``             |               |
+--------------+-----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageHlsByte Median(ViewLocatorHlsByte source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``VectorInt32``          |               |
+--------------+--------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageHlsUInt16 Median(ViewLocatorHlsUInt16 source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageHlsDouble Median(ViewLocatorHlsDouble source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageHsiByte Median(ViewLocatorHsiByte source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``VectorInt32``          |               |
+--------------+--------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageHsiUInt16 Median(ViewLocatorHsiUInt16 source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageHsiDouble Median(ViewLocatorHsiDouble source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageLabByte Median(ViewLocatorLabByte source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``VectorInt32``          |               |
+--------------+--------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageLabUInt16 Median(ViewLocatorLabUInt16 source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageLabDouble Median(ViewLocatorLabDouble source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageXyzByte Median(ViewLocatorXyzByte source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``VectorInt32``          |               |
+--------------+--------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageXyzUInt16 Median(ViewLocatorXyzUInt16 source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageXyzDouble Median(ViewLocatorXyzDouble source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``Image Median(View source, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+-------------------+---------------+
| Parameter    | Type              | Description   |
+==============+===================+===============+
| ``source``   | ``View``          |               |
+--------------+-------------------+---------------+
| ``size``     | ``VectorInt32``   |               |
+--------------+-------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageByte Median(ViewLocatorByte source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``aoi``      | ``Region``            |               |
+--------------+-----------------------+---------------+
| ``size``     | ``VectorInt32``       |               |
+--------------+-----------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageUInt16 Median(ViewLocatorUInt16 source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``size``     | ``VectorInt32``         |               |
+--------------+-------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageUInt32 Median(ViewLocatorUInt32 source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``size``     | ``VectorInt32``         |               |
+--------------+-------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageDouble Median(ViewLocatorDouble source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``size``     | ``VectorInt32``         |               |
+--------------+-------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageRgbByte Median(ViewLocatorRgbByte source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``VectorInt32``          |               |
+--------------+--------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageRgbUInt16 Median(ViewLocatorRgbUInt16 source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageRgbUInt32 Median(ViewLocatorRgbUInt32 source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageRgbDouble Median(ViewLocatorRgbDouble source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageRgbaByte Median(ViewLocatorRgbaByte source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``aoi``      | ``Region``                |               |
+--------------+---------------------------+---------------+
| ``size``     | ``VectorInt32``           |               |
+--------------+---------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Median(ViewLocatorRgbaUInt16 source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``VectorInt32``             |               |
+--------------+-----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Median(ViewLocatorRgbaUInt32 source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``VectorInt32``             |               |
+--------------+-----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageRgbaDouble Median(ViewLocatorRgbaDouble source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``VectorInt32``             |               |
+--------------+-----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageHlsByte Median(ViewLocatorHlsByte source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``VectorInt32``          |               |
+--------------+--------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageHlsUInt16 Median(ViewLocatorHlsUInt16 source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageHlsDouble Median(ViewLocatorHlsDouble source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageHsiByte Median(ViewLocatorHsiByte source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``VectorInt32``          |               |
+--------------+--------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageHsiUInt16 Median(ViewLocatorHsiUInt16 source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageHsiDouble Median(ViewLocatorHsiDouble source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageLabByte Median(ViewLocatorLabByte source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``VectorInt32``          |               |
+--------------+--------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageLabUInt16 Median(ViewLocatorLabUInt16 source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageLabDouble Median(ViewLocatorLabDouble source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageXyzByte Median(ViewLocatorXyzByte source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``VectorInt32``          |               |
+--------------+--------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageXyzUInt16 Median(ViewLocatorXyzUInt16 source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``ImageXyzDouble Median(ViewLocatorXyzDouble source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``VectorInt32``            |               |
+--------------+----------------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Median*
^^^^^^^^^^^^^^^

``Image Median(View source, Region aoi, VectorInt32 size)``

Perform a median filter.

The method **Median** has the following parameters:

+--------------+-------------------+---------------+
| Parameter    | Type              | Description   |
+==============+===================+===============+
| ``source``   | ``View``          |               |
+--------------+-------------------+---------------+
| ``aoi``      | ``Region``        |               |
+--------------+-------------------+---------------+
| ``size``     | ``VectorInt32``   |               |
+--------------+-------------------+---------------+

The median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

For every pixel, the median filter selects the median value of all values in the neighborhood of the pixel.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageByte HybridMedian(ViewLocatorByte source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``size``     | ``System.Int32``      |               |
+--------------+-----------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 HybridMedian(ViewLocatorUInt16 source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 HybridMedian(ViewLocatorUInt32 source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageDouble HybridMedian(ViewLocatorDouble source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte HybridMedian(ViewLocatorRgbByte source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 HybridMedian(ViewLocatorRgbUInt16 source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 HybridMedian(ViewLocatorRgbUInt32 source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble HybridMedian(ViewLocatorRgbDouble source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte HybridMedian(ViewLocatorRgbaByte source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``size``     | ``System.Int32``          |               |
+--------------+---------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 HybridMedian(ViewLocatorRgbaUInt16 source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 HybridMedian(ViewLocatorRgbaUInt32 source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble HybridMedian(ViewLocatorRgbaDouble source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte HybridMedian(ViewLocatorHlsByte source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 HybridMedian(ViewLocatorHlsUInt16 source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble HybridMedian(ViewLocatorHlsDouble source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte HybridMedian(ViewLocatorHsiByte source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 HybridMedian(ViewLocatorHsiUInt16 source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble HybridMedian(ViewLocatorHsiDouble source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte HybridMedian(ViewLocatorLabByte source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 HybridMedian(ViewLocatorLabUInt16 source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble HybridMedian(ViewLocatorLabDouble source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte HybridMedian(ViewLocatorXyzByte source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 HybridMedian(ViewLocatorXyzUInt16 source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble HybridMedian(ViewLocatorXyzDouble source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``Image HybridMedian(View source, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``source``   | ``View``           |               |
+--------------+--------------------+---------------+
| ``size``     | ``System.Int32``   |               |
+--------------+--------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageByte HybridMedian(ViewLocatorByte source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``aoi``      | ``Region``            |               |
+--------------+-----------------------+---------------+
| ``size``     | ``System.Int32``      |               |
+--------------+-----------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageUInt16 HybridMedian(ViewLocatorUInt16 source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt16``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageUInt32 HybridMedian(ViewLocatorUInt32 source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorUInt32``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageDouble HybridMedian(ViewLocatorDouble source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+-------------------------+---------------+
| Parameter    | Type                    | Description   |
+==============+=========================+===============+
| ``source``   | ``ViewLocatorDouble``   |               |
+--------------+-------------------------+---------------+
| ``aoi``      | ``Region``              |               |
+--------------+-------------------------+---------------+
| ``size``     | ``System.Int32``        |               |
+--------------+-------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte HybridMedian(ViewLocatorRgbByte source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 HybridMedian(ViewLocatorRgbUInt16 source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 HybridMedian(ViewLocatorRgbUInt32 source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbDouble HybridMedian(ViewLocatorRgbDouble source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaByte HybridMedian(ViewLocatorRgbaByte source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``source``   | ``ViewLocatorRgbaByte``   |               |
+--------------+---------------------------+---------------+
| ``aoi``      | ``Region``                |               |
+--------------+---------------------------+---------------+
| ``size``     | ``System.Int32``          |               |
+--------------+---------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 HybridMedian(ViewLocatorRgbaUInt16 source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt16``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 HybridMedian(ViewLocatorRgbaUInt32 source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaUInt32``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble HybridMedian(ViewLocatorRgbaDouble source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+-----------------------------+---------------+
| Parameter    | Type                        | Description   |
+==============+=============================+===============+
| ``source``   | ``ViewLocatorRgbaDouble``   |               |
+--------------+-----------------------------+---------------+
| ``aoi``      | ``Region``                  |               |
+--------------+-----------------------------+---------------+
| ``size``     | ``System.Int32``            |               |
+--------------+-----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsByte HybridMedian(ViewLocatorHlsByte source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHlsByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 HybridMedian(ViewLocatorHlsUInt16 source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageHlsDouble HybridMedian(ViewLocatorHlsDouble source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHlsDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiByte HybridMedian(ViewLocatorHsiByte source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorHsiByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 HybridMedian(ViewLocatorHsiUInt16 source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageHsiDouble HybridMedian(ViewLocatorHsiDouble source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorHsiDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabByte HybridMedian(ViewLocatorLabByte source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorLabByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 HybridMedian(ViewLocatorLabUInt16 source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageLabDouble HybridMedian(ViewLocatorLabDouble source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorLabDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzByte HybridMedian(ViewLocatorXyzByte source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorXyzByte``   |               |
+--------------+--------------------------+---------------+
| ``aoi``      | ``Region``               |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Int32``         |               |
+--------------+--------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 HybridMedian(ViewLocatorXyzUInt16 source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzUInt16``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``ImageXyzDouble HybridMedian(ViewLocatorXyzDouble source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorXyzDouble``   |               |
+--------------+----------------------------+---------------+
| ``aoi``      | ``Region``                 |               |
+--------------+----------------------------+---------------+
| ``size``     | ``System.Int32``           |               |
+--------------+----------------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *HybridMedian*
^^^^^^^^^^^^^^^^^^^^^

``Image HybridMedian(View source, Region aoi, System.Int32 size)``

Perform a hybrid median filter.

The method **HybridMedian** has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``source``   | ``View``           |               |
+--------------+--------------------+---------------+
| ``aoi``      | ``Region``         |               |
+--------------+--------------------+---------------+
| ``size``     | ``System.Int32``   |               |
+--------------+--------------------+---------------+

The hybrid median is a noise-reduction filter, which often performs better than simple averaging, especially for salt-and-pepper noise.

The hybrid median calculates the median of a cross-shaped kernel, the median of an x-shaped kernel, and finally the median of the center pixel and these two intermediate reslts.

The hybrid median is more edge-preserving than the standard median filter.

The region parameter constrains the function to the region inside of the view only.

/return The filtered result image.

Method *Canny*
^^^^^^^^^^^^^^

``ImageByte Canny(ViewLocatorByte source, System.Double lowThreshold, System.Double highThreshold, System.Double sigma, System.Int32 sobelKernel, System.Boolean normalization)``

The function applies canny edge detection to an image.

The method **Canny** has the following parameters:

+---------------------+-----------------------+---------------+
| Parameter           | Type                  | Description   |
+=====================+=======================+===============+
| ``source``          | ``ViewLocatorByte``   |               |
+---------------------+-----------------------+---------------+
| ``lowThreshold``    | ``System.Double``     |               |
+---------------------+-----------------------+---------------+
| ``highThreshold``   | ``System.Double``     |               |
+---------------------+-----------------------+---------------+
| ``sigma``           | ``System.Double``     |               |
+---------------------+-----------------------+---------------+
| ``sobelKernel``     | ``System.Int32``      |               |
+---------------------+-----------------------+---------------+
| ``normalization``   | ``System.Boolean``    |               |
+---------------------+-----------------------+---------------+

It receives an image and creates a binary image representing the edges of the first one.

/return the edges image

Method *Canny*
^^^^^^^^^^^^^^

``Image Canny(View source, System.Double lowThreshold, System.Double highThreshold, System.Double sigma, System.Int32 sobelKernel, System.Boolean normalization)``

The function applies canny edge detection to an image.

The method **Canny** has the following parameters:

+---------------------+----------------------+---------------+
| Parameter           | Type                 | Description   |
+=====================+======================+===============+
| ``source``          | ``View``             |               |
+---------------------+----------------------+---------------+
| ``lowThreshold``    | ``System.Double``    |               |
+---------------------+----------------------+---------------+
| ``highThreshold``   | ``System.Double``    |               |
+---------------------+----------------------+---------------+
| ``sigma``           | ``System.Double``    |               |
+---------------------+----------------------+---------------+
| ``sobelKernel``     | ``System.Int32``     |               |
+---------------------+----------------------+---------------+
| ``normalization``   | ``System.Boolean``   |               |
+---------------------+----------------------+---------------+

It receives an image and creates a binary image representing the edges of the first one.

/return the edges image

Method *DistortionCorrection*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageByte DistortionCorrection(ViewLocatorByte source, System.Double paramA, System.Double paramB, System.Double paramC)``

Apply distortion correction to an image.

The method **DistortionCorrection** has the following parameters:

+--------------+-----------------------+---------------+
| Parameter    | Type                  | Description   |
+==============+=======================+===============+
| ``source``   | ``ViewLocatorByte``   |               |
+--------------+-----------------------+---------------+
| ``paramA``   | ``System.Double``     |               |
+--------------+-----------------------+---------------+
| ``paramB``   | ``System.Double``     |               |
+--------------+-----------------------+---------------+
| ``paramC``   | ``System.Double``     |               |
+--------------+-----------------------+---------------+

It receives an image and corrects it.

Method *DistortionCorrection*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Image DistortionCorrection(View source, System.Double paramA, System.Double paramB, System.Double paramC)``

Apply distortion correction to an image.

The method **DistortionCorrection** has the following parameters:

+--------------+---------------------+---------------+
| Parameter    | Type                | Description   |
+==============+=====================+===============+
| ``source``   | ``View``            |               |
+--------------+---------------------+---------------+
| ``paramA``   | ``System.Double``   |               |
+--------------+---------------------+---------------+
| ``paramB``   | ``System.Double``   |               |
+--------------+---------------------+---------------+
| ``paramC``   | ``System.Double``   |               |
+--------------+---------------------+---------------+

It receives an image and corrects it.

Enumerations
~~~~~~~~~~~~

Enumeration *FramingFunction*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum FramingFunction``

TODO documentation missing

The enumeration **FramingFunction** has the following constants:

+------------------------+---------+---------------+
| Name                   | Value   | Description   |
+========================+=========+===============+
| ``nearest``            | ``0``   |               |
+------------------------+---------+---------------+
| ``reflectiveMiddle``   | ``1``   |               |
+------------------------+---------+---------------+
| ``reflectiveBorder``   | ``2``   |               |
+------------------------+---------+---------------+
| ``periodic``           | ``3``   |               |
+------------------------+---------+---------------+
| ``constant``           | ``4``   |               |
+------------------------+---------+---------------+

::

    enum FramingFunction
    {
      nearest = 0,
      reflectiveMiddle = 1,
      reflectiveBorder = 2,
      periodic = 3,
      constant = 4,
    };

TODO documentation missing
