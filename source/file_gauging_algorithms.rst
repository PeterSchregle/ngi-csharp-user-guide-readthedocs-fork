Class *GaugingAlgorithms*
-------------------------

Wraps free-standing functions.

**Namespace:** Ngi

**Module:** AnalysisMeasuring

The class **GaugingAlgorithms** contains the following enumerations:

+----------------------------------+------------------------------+
| Enumeration                      | Description                  |
+==================================+==============================+
| ``EdgeGaugingOrientation``       | TODO documentation missing   |
+----------------------------------+------------------------------+
| ``EdgePolarity``                 | TODO documentation missing   |
+----------------------------------+------------------------------+
| ``EdgeSelector``                 | TODO documentation missing   |
+----------------------------------+------------------------------+
| ``CircularGaugingOrientation``   | TODO documentation missing   |
+----------------------------------+------------------------------+

Description
~~~~~~~~~~~

The purpose of this class is to make the functions available for the .NET wrapper.

Static Methods
~~~~~~~~~~~~~~

Method *GaugeEdgesInfoInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfoInProfile(ViewLocatorByte source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInfoInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``ViewLocatorByte``                  |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the begin of the source profile, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInfoInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfoInProfile(ViewLocatorUInt16 source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInfoInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``ViewLocatorUInt16``                |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the begin of the source profile, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInfoInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfoInProfile(ViewLocatorUInt32 source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInfoInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``ViewLocatorUInt32``                |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the begin of the source profile, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInfoInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfoInProfile(ViewLocatorDouble source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInfoInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``ViewLocatorDouble``                |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the begin of the source profile, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInfoInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfoInProfile(ViewLocatorRgbByte source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInfoInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``ViewLocatorRgbByte``               |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the begin of the source profile, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInfoInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfoInProfile(ViewLocatorRgbUInt16 source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInfoInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``ViewLocatorRgbUInt16``             |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the begin of the source profile, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInfoInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfoInProfile(ViewLocatorRgbUInt32 source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInfoInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``ViewLocatorRgbUInt32``             |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the begin of the source profile, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInfoInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfoInProfile(ViewLocatorRgbDouble source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInfoInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``ViewLocatorRgbDouble``             |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the begin of the source profile, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInfoInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfoInProfile(ViewLocatorRgbaByte source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInfoInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``ViewLocatorRgbaByte``              |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the begin of the source profile, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInfoInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfoInProfile(ViewLocatorRgbaUInt16 source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInfoInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``ViewLocatorRgbaUInt16``            |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the begin of the source profile, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInfoInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfoInProfile(ViewLocatorRgbaUInt32 source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInfoInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``ViewLocatorRgbaUInt32``            |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the begin of the source profile, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInfoInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfoInProfile(ViewLocatorRgbaDouble source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInfoInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``ViewLocatorRgbaDouble``            |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the begin of the source profile, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInfoInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfoInProfile(View source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInfoInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``View``                             |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the begin of the source profile, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdgesInProfile(ViewLocatorByte source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``ViewLocatorByte``                  |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

A list of edges, which consist of a position with respect to the begin of the source profile and an edge strength.

Method *GaugeEdgesInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdgesInProfile(ViewLocatorUInt16 source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``ViewLocatorUInt16``                |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

A list of edges, which consist of a position with respect to the begin of the source profile and an edge strength.

Method *GaugeEdgesInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdgesInProfile(ViewLocatorUInt32 source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``ViewLocatorUInt32``                |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

A list of edges, which consist of a position with respect to the begin of the source profile and an edge strength.

Method *GaugeEdgesInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdgesInProfile(ViewLocatorDouble source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``ViewLocatorDouble``                |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

A list of edges, which consist of a position with respect to the begin of the source profile and an edge strength.

Method *GaugeEdgesInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdgesInProfile(ViewLocatorRgbByte source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``ViewLocatorRgbByte``               |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

A list of edges, which consist of a position with respect to the begin of the source profile and an edge strength.

Method *GaugeEdgesInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdgesInProfile(ViewLocatorRgbUInt16 source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``ViewLocatorRgbUInt16``             |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

A list of edges, which consist of a position with respect to the begin of the source profile and an edge strength.

Method *GaugeEdgesInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdgesInProfile(ViewLocatorRgbUInt32 source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``ViewLocatorRgbUInt32``             |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

A list of edges, which consist of a position with respect to the begin of the source profile and an edge strength.

Method *GaugeEdgesInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdgesInProfile(ViewLocatorRgbDouble source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``ViewLocatorRgbDouble``             |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

A list of edges, which consist of a position with respect to the begin of the source profile and an edge strength.

Method *GaugeEdgesInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdgesInProfile(ViewLocatorRgbaByte source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``ViewLocatorRgbaByte``              |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

A list of edges, which consist of a position with respect to the begin of the source profile and an edge strength.

Method *GaugeEdgesInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdgesInProfile(ViewLocatorRgbaUInt16 source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``ViewLocatorRgbaUInt16``            |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

A list of edges, which consist of a position with respect to the begin of the source profile and an edge strength.

Method *GaugeEdgesInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdgesInProfile(ViewLocatorRgbaUInt32 source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``ViewLocatorRgbaUInt32``            |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

A list of edges, which consist of a position with respect to the begin of the source profile and an edge strength.

Method *GaugeEdgesInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdgesInProfile(ViewLocatorRgbaDouble source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``ViewLocatorRgbaDouble``            |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

A list of edges, which consist of a position with respect to the begin of the source profile and an edge strength.

Method *GaugeEdgesInProfile*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdgesInProfile(View source, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in a profile.

The method **GaugeEdgesInProfile** has the following parameters:

+-----------------+--------------------------------------+---------------+
| Parameter       | Type                                 | Description   |
+=================+======================================+===============+
| ``source``      | ``View``                             |               |
+-----------------+--------------------------------------+---------------+
| ``polarity``    | ``GaugingAlgorithms.EdgePolarity``   |               |
+-----------------+--------------------------------------+---------------+
| ``selector``    | ``GaugingAlgorithms.EdgeSelector``   |               |
+-----------------+--------------------------------------+---------------+
| ``sigma``       | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+
| ``threshold``   | ``System.Double``                    |               |
+-----------------+--------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

A list of edges, which consist of a position with respect to the begin of the source profile and an edge strength.

Method *GaugeEdgesInfo*
^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfo(ViewLocatorByte source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdgesInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorByte``                            |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInfo*
^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfo(ViewLocatorUInt16 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdgesInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorUInt16``                          |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInfo*
^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfo(ViewLocatorUInt32 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdgesInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorUInt32``                          |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInfo*
^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfo(ViewLocatorDouble source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdgesInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorDouble``                          |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInfo*
^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfo(ViewLocatorRgbByte source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdgesInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbByte``                         |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInfo*
^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfo(ViewLocatorRgbUInt16 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdgesInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbUInt16``                       |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInfo*
^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfo(ViewLocatorRgbUInt32 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdgesInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbUInt32``                       |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInfo*
^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfo(ViewLocatorRgbDouble source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdgesInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbDouble``                       |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInfo*
^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfo(ViewLocatorRgbaByte source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdgesInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbaByte``                        |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInfo*
^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfo(ViewLocatorRgbaUInt16 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdgesInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbaUInt16``                      |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInfo*
^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfo(ViewLocatorRgbaUInt32 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdgesInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbaUInt32``                      |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInfo*
^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfo(ViewLocatorRgbaDouble source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdgesInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbaDouble``                      |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgesInfo*
^^^^^^^^^^^^^^^^^^^^^^^

``Edge1dInfo GaugeEdgesInfo(View source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdgesInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``View``                                       |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdges*
^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdges(ViewLocatorByte source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdges** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorByte``                            |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdges*
^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdges(ViewLocatorUInt16 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdges** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorUInt16``                          |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdges*
^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdges(ViewLocatorUInt32 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdges** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorUInt32``                          |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdges*
^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdges(ViewLocatorDouble source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdges** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorDouble``                          |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdges*
^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdges(ViewLocatorRgbByte source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdges** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbByte``                         |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdges*
^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdges(ViewLocatorRgbUInt16 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdges** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbUInt16``                       |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdges*
^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdges(ViewLocatorRgbUInt32 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdges** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbUInt32``                       |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdges*
^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdges(ViewLocatorRgbDouble source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdges** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbDouble``                       |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdges*
^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdges(ViewLocatorRgbaByte source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdges** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbaByte``                        |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdges*
^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdges(ViewLocatorRgbaUInt16 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdges** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbaUInt16``                      |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdges*
^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdges(ViewLocatorRgbaUInt32 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdges** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbaUInt32``                      |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdges*
^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdges(ViewLocatorRgbaDouble source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdges** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbaDouble``                      |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdges*
^^^^^^^^^^^^^^^^^^^

``Edge1dList GaugeEdges(View source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double sigma, System.Double threshold)``

Calculates the position of edges in the source view.

The method **GaugeEdges** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``View``                                       |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

The list of edges contains for each edge a position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeEdgePointsInfo(ViewLocatorByte source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view.

The method **GaugeEdgePointsInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorByte``                            |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The rectangular are within the view is scanned in horizontal or vertical direction, from left to right, right to left, top to bottom or bottom to top, as specified by the orientation parameter. The scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeEdgePointsInfo(ViewLocatorUInt16 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view.

The method **GaugeEdgePointsInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorUInt16``                          |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The rectangular are within the view is scanned in horizontal or vertical direction, from left to right, right to left, top to bottom or bottom to top, as specified by the orientation parameter. The scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeEdgePointsInfo(ViewLocatorUInt32 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view.

The method **GaugeEdgePointsInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorUInt32``                          |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The rectangular are within the view is scanned in horizontal or vertical direction, from left to right, right to left, top to bottom or bottom to top, as specified by the orientation parameter. The scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeEdgePointsInfo(ViewLocatorDouble source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view.

The method **GaugeEdgePointsInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorDouble``                          |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The rectangular are within the view is scanned in horizontal or vertical direction, from left to right, right to left, top to bottom or bottom to top, as specified by the orientation parameter. The scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeEdgePointsInfo(ViewLocatorRgbByte source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view.

The method **GaugeEdgePointsInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbByte``                         |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The rectangular are within the view is scanned in horizontal or vertical direction, from left to right, right to left, top to bottom or bottom to top, as specified by the orientation parameter. The scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeEdgePointsInfo(ViewLocatorRgbUInt16 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view.

The method **GaugeEdgePointsInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbUInt16``                       |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The rectangular are within the view is scanned in horizontal or vertical direction, from left to right, right to left, top to bottom or bottom to top, as specified by the orientation parameter. The scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeEdgePointsInfo(ViewLocatorRgbUInt32 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view.

The method **GaugeEdgePointsInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbUInt32``                       |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The rectangular are within the view is scanned in horizontal or vertical direction, from left to right, right to left, top to bottom or bottom to top, as specified by the orientation parameter. The scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeEdgePointsInfo(ViewLocatorRgbDouble source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view.

The method **GaugeEdgePointsInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbDouble``                       |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The rectangular are within the view is scanned in horizontal or vertical direction, from left to right, right to left, top to bottom or bottom to top, as specified by the orientation parameter. The scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeEdgePointsInfo(ViewLocatorRgbaByte source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view.

The method **GaugeEdgePointsInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbaByte``                        |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The rectangular are within the view is scanned in horizontal or vertical direction, from left to right, right to left, top to bottom or bottom to top, as specified by the orientation parameter. The scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeEdgePointsInfo(ViewLocatorRgbaUInt16 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view.

The method **GaugeEdgePointsInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbaUInt16``                      |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The rectangular are within the view is scanned in horizontal or vertical direction, from left to right, right to left, top to bottom or bottom to top, as specified by the orientation parameter. The scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeEdgePointsInfo(ViewLocatorRgbaUInt32 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view.

The method **GaugeEdgePointsInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbaUInt32``                      |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The rectangular are within the view is scanned in horizontal or vertical direction, from left to right, right to left, top to bottom or bottom to top, as specified by the orientation parameter. The scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeEdgePointsInfo(ViewLocatorRgbaDouble source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view.

The method **GaugeEdgePointsInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbaDouble``                      |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The rectangular are within the view is scanned in horizontal or vertical direction, from left to right, right to left, top to bottom or bottom to top, as specified by the orientation parameter. The scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeEdgePointsInfo(View source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view.

The method **GaugeEdgePointsInfo** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``View``                                       |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The rectangular are within the view is scanned in horizontal or vertical direction, from left to right, right to left, top to bottom or bottom to top, as specified by the orientation parameter. The scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the left or top of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeEdgePoints(ViewLocatorByte source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of an edge in the source view.

The method **GaugeEdgePoints** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorByte``                            |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeEdgePoints(ViewLocatorUInt16 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of an edge in the source view.

The method **GaugeEdgePoints** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorUInt16``                          |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeEdgePoints(ViewLocatorUInt32 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of an edge in the source view.

The method **GaugeEdgePoints** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorUInt32``                          |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeEdgePoints(ViewLocatorDouble source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of an edge in the source view.

The method **GaugeEdgePoints** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorDouble``                          |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeEdgePoints(ViewLocatorRgbByte source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of an edge in the source view.

The method **GaugeEdgePoints** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbByte``                         |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeEdgePoints(ViewLocatorRgbUInt16 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of an edge in the source view.

The method **GaugeEdgePoints** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbUInt16``                       |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeEdgePoints(ViewLocatorRgbUInt32 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of an edge in the source view.

The method **GaugeEdgePoints** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbUInt32``                       |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeEdgePoints(ViewLocatorRgbDouble source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of an edge in the source view.

The method **GaugeEdgePoints** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbDouble``                       |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeEdgePoints(ViewLocatorRgbaByte source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of an edge in the source view.

The method **GaugeEdgePoints** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbaByte``                        |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeEdgePoints(ViewLocatorRgbaUInt16 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of an edge in the source view.

The method **GaugeEdgePoints** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbaUInt16``                      |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeEdgePoints(ViewLocatorRgbaUInt32 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of an edge in the source view.

The method **GaugeEdgePoints** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbaUInt32``                      |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeEdgePoints(ViewLocatorRgbaDouble source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of an edge in the source view.

The method **GaugeEdgePoints** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbaDouble``                      |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeEdgePoints(View source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of an edge in the source view.

The method **GaugeEdgePoints** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``View``                                       |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeCircularEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeCircularEdgePointsInfo(ViewLocatorByte source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view in a radial manner.

The method **GaugeCircularEdgePointsInfo** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorByte``                                |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The rectangular are within the view is scanned in radial direction, starting from the middle in an outwards direction, or starting from the outside in an inwards direction to the middle, as specified by the orientation parameter. The angles of the scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the top or left of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeCircularEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeCircularEdgePointsInfo(ViewLocatorUInt16 source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view in a radial manner.

The method **GaugeCircularEdgePointsInfo** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorUInt16``                              |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The rectangular are within the view is scanned in radial direction, starting from the middle in an outwards direction, or starting from the outside in an inwards direction to the middle, as specified by the orientation parameter. The angles of the scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the top or left of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeCircularEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeCircularEdgePointsInfo(ViewLocatorUInt32 source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view in a radial manner.

The method **GaugeCircularEdgePointsInfo** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorUInt32``                              |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The rectangular are within the view is scanned in radial direction, starting from the middle in an outwards direction, or starting from the outside in an inwards direction to the middle, as specified by the orientation parameter. The angles of the scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the top or left of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeCircularEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeCircularEdgePointsInfo(ViewLocatorDouble source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view in a radial manner.

The method **GaugeCircularEdgePointsInfo** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorDouble``                              |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The rectangular are within the view is scanned in radial direction, starting from the middle in an outwards direction, or starting from the outside in an inwards direction to the middle, as specified by the orientation parameter. The angles of the scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the top or left of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeCircularEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeCircularEdgePointsInfo(ViewLocatorRgbByte source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view in a radial manner.

The method **GaugeCircularEdgePointsInfo** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbByte``                             |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The rectangular are within the view is scanned in radial direction, starting from the middle in an outwards direction, or starting from the outside in an inwards direction to the middle, as specified by the orientation parameter. The angles of the scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the top or left of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeCircularEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeCircularEdgePointsInfo(ViewLocatorRgbUInt16 source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view in a radial manner.

The method **GaugeCircularEdgePointsInfo** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbUInt16``                           |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The rectangular are within the view is scanned in radial direction, starting from the middle in an outwards direction, or starting from the outside in an inwards direction to the middle, as specified by the orientation parameter. The angles of the scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the top or left of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeCircularEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeCircularEdgePointsInfo(ViewLocatorRgbUInt32 source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view in a radial manner.

The method **GaugeCircularEdgePointsInfo** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbUInt32``                           |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The rectangular are within the view is scanned in radial direction, starting from the middle in an outwards direction, or starting from the outside in an inwards direction to the middle, as specified by the orientation parameter. The angles of the scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the top or left of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeCircularEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeCircularEdgePointsInfo(ViewLocatorRgbDouble source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view in a radial manner.

The method **GaugeCircularEdgePointsInfo** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbDouble``                           |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The rectangular are within the view is scanned in radial direction, starting from the middle in an outwards direction, or starting from the outside in an inwards direction to the middle, as specified by the orientation parameter. The angles of the scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the top or left of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeCircularEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeCircularEdgePointsInfo(ViewLocatorRgbaByte source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view in a radial manner.

The method **GaugeCircularEdgePointsInfo** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbaByte``                            |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The rectangular are within the view is scanned in radial direction, starting from the middle in an outwards direction, or starting from the outside in an inwards direction to the middle, as specified by the orientation parameter. The angles of the scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the top or left of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeCircularEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeCircularEdgePointsInfo(ViewLocatorRgbaUInt16 source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view in a radial manner.

The method **GaugeCircularEdgePointsInfo** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbaUInt16``                          |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The rectangular are within the view is scanned in radial direction, starting from the middle in an outwards direction, or starting from the outside in an inwards direction to the middle, as specified by the orientation parameter. The angles of the scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the top or left of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeCircularEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeCircularEdgePointsInfo(ViewLocatorRgbaUInt32 source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view in a radial manner.

The method **GaugeCircularEdgePointsInfo** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbaUInt32``                          |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The rectangular are within the view is scanned in radial direction, starting from the middle in an outwards direction, or starting from the outside in an inwards direction to the middle, as specified by the orientation parameter. The angles of the scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the top or left of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeCircularEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeCircularEdgePointsInfo(ViewLocatorRgbaDouble source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view in a radial manner.

The method **GaugeCircularEdgePointsInfo** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbaDouble``                          |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The rectangular are within the view is scanned in radial direction, starting from the middle in an outwards direction, or starting from the outside in an inwards direction to the middle, as specified by the orientation parameter. The angles of the scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the top or left of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeCircularEdgePointsInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dInfoList GaugeCircularEdgePointsInfo(View source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the edge points in the source view in a radial manner.

The method **GaugeCircularEdgePointsInfo** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``View``                                           |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The rectangular are within the view is scanned in radial direction, starting from the middle in an outwards direction, or starting from the outside in an inwards direction to the middle, as specified by the orientation parameter. The angles of the scans are spaced as specified with the spacing parameter, and their width is specified with the thickness parameter.

The edges are calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy. In addition, the function returns the smoothed profile as well as the gradient profile, which are means to understand the effect of the sigma and threshold parameters.

Edge information for each scan, consisting of a list of edges and the smoothed and gradient profile. The list of edges contains for each edge a 2D position with respect to the top or left of the source view, an edge strength and the greyvalue at the edge.

Method *GaugeCircularEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeCircularEdgePoints(ViewLocatorByte source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of a circular edge in the source view.

The method **GaugeCircularEdgePoints** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorByte``                                |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeCircularEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeCircularEdgePoints(ViewLocatorUInt16 source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of a circular edge in the source view.

The method **GaugeCircularEdgePoints** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorUInt16``                              |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeCircularEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeCircularEdgePoints(ViewLocatorUInt32 source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of a circular edge in the source view.

The method **GaugeCircularEdgePoints** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorUInt32``                              |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeCircularEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeCircularEdgePoints(ViewLocatorDouble source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of a circular edge in the source view.

The method **GaugeCircularEdgePoints** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorDouble``                              |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeCircularEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeCircularEdgePoints(ViewLocatorRgbByte source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of a circular edge in the source view.

The method **GaugeCircularEdgePoints** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbByte``                             |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeCircularEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeCircularEdgePoints(ViewLocatorRgbUInt16 source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of a circular edge in the source view.

The method **GaugeCircularEdgePoints** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbUInt16``                           |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeCircularEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeCircularEdgePoints(ViewLocatorRgbUInt32 source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of a circular edge in the source view.

The method **GaugeCircularEdgePoints** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbUInt32``                           |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeCircularEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeCircularEdgePoints(ViewLocatorRgbDouble source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of a circular edge in the source view.

The method **GaugeCircularEdgePoints** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbDouble``                           |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeCircularEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeCircularEdgePoints(ViewLocatorRgbaByte source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of a circular edge in the source view.

The method **GaugeCircularEdgePoints** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbaByte``                            |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeCircularEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeCircularEdgePoints(ViewLocatorRgbaUInt16 source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of a circular edge in the source view.

The method **GaugeCircularEdgePoints** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbaUInt16``                          |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeCircularEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeCircularEdgePoints(ViewLocatorRgbaUInt32 source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of a circular edge in the source view.

The method **GaugeCircularEdgePoints** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbaUInt32``                          |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeCircularEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeCircularEdgePoints(ViewLocatorRgbaDouble source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of a circular edge in the source view.

The method **GaugeCircularEdgePoints** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbaDouble``                          |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *GaugeCircularEdgePoints*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Edge2dList GaugeCircularEdgePoints(View source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the points of a circular edge in the source view.

The method **GaugeCircularEdgePoints** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``View``                                           |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

The edge is calculated by analyzing the profile and finding the positions where grey scale varies most. The positions are calculated with subpixel accuracy.

If no edge is found, the function throws an exception.

The edge position as a numeric displacement with respect to the left or top position of the source view.

Method *FitLine*
^^^^^^^^^^^^^^^^

``LineDouble FitLine(PointListPointInt32 points)``

This function calculates a line fitted to a set of two or more points.

The method **FitLine** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``points``   | ``PointListPointInt32``   |               |
+--------------+---------------------------+---------------+

The line is calculated using a total least squares solving method, thus minimizing orthogonal distances. All points are considered; there is no detection or rejection of outliers.

If you need robust line fitting with outlier detection, consider using the robust\_fit\_line method.

Method *FitLine*
^^^^^^^^^^^^^^^^

``LineDouble FitLine(PointListPointDouble points)``

This function calculates a line fitted to a set of two or more points.

The method **FitLine** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``points``   | ``PointListPointDouble``   |               |
+--------------+----------------------------+---------------+

The line is calculated using a total least squares solving method, thus minimizing orthogonal distances. All points are considered; there is no detection or rejection of outliers.

If you need robust line fitting with outlier detection, consider using the robust\_fit\_line method.

Method *FitLine*
^^^^^^^^^^^^^^^^

``LineDouble FitLine(PointList points)``

This function calculates a line fitted to a set of two or more points.

The method **FitLine** has the following parameters:

+--------------+-----------------+---------------+
| Parameter    | Type            | Description   |
+==============+=================+===============+
| ``points``   | ``PointList``   |               |
+--------------+-----------------+---------------+

The line is calculated using a total least squares solving method, thus minimizing orthogonal distances. All points are considered; there is no detection or rejection of outliers.

If you need robust line fitting with outlier detection, consider using the robust\_fit\_line method.

Method *FitCircle*
^^^^^^^^^^^^^^^^^^

``Circle FitCircle(PointListPointInt32 points)``

This function calculates a circle fitted to a set of three or more points.

The method **FitCircle** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``points``   | ``PointListPointInt32``   |               |
+--------------+---------------------------+---------------+

The function implements the algebraic circle fit by Taubin: G.Taubin, "Estimation Of Planar Curves, Surfaces And Nonplanar Space Curves Defined By Implicit Equations, With Applications To Edge And Range Image Segmentation", IEEE Trans.PAMI, Vol. 13, pages 1115 - 1138, (1991)

following the implementation reported in: N.Chernov, "Circular and linear regression: Fitting circles and lines by least squares", pages 121--125.

All points are considered; there is no detection or rejection of outliers.

If you need robust circle fitting with outlier detection, consider using the robust\_fit\_circle method.

Method *FitCircle*
^^^^^^^^^^^^^^^^^^

``Circle FitCircle(PointListPointDouble points)``

This function calculates a circle fitted to a set of three or more points.

The method **FitCircle** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``points``   | ``PointListPointDouble``   |               |
+--------------+----------------------------+---------------+

The function implements the algebraic circle fit by Taubin: G.Taubin, "Estimation Of Planar Curves, Surfaces And Nonplanar Space Curves Defined By Implicit Equations, With Applications To Edge And Range Image Segmentation", IEEE Trans.PAMI, Vol. 13, pages 1115 - 1138, (1991)

following the implementation reported in: N.Chernov, "Circular and linear regression: Fitting circles and lines by least squares", pages 121--125.

All points are considered; there is no detection or rejection of outliers.

If you need robust circle fitting with outlier detection, consider using the robust\_fit\_circle method.

Method *FitCircle*
^^^^^^^^^^^^^^^^^^

``Circle FitCircle(PointList points)``

This function calculates a circle fitted to a set of three or more points.

The method **FitCircle** has the following parameters:

+--------------+-----------------+---------------+
| Parameter    | Type            | Description   |
+==============+=================+===============+
| ``points``   | ``PointList``   |               |
+--------------+-----------------+---------------+

The function implements the algebraic circle fit by Taubin: G.Taubin, "Estimation Of Planar Curves, Surfaces And Nonplanar Space Curves Defined By Implicit Equations, With Applications To Edge And Range Image Segmentation", IEEE Trans.PAMI, Vol. 13, pages 1115 - 1138, (1991)

following the implementation reported in: N.Chernov, "Circular and linear regression: Fitting circles and lines by least squares", pages 121--125.

All points are considered; there is no detection or rejection of outliers.

If you need robust circle fitting with outlier detection, consider using the robust\_fit\_circle method.

Method *FitEllipse*
^^^^^^^^^^^^^^^^^^^

``Ellipse FitEllipse(PointListPointInt32 points)``

This function calculates an ellipse fitted to a set of five or more points.

The method **FitEllipse** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``points``   | ``PointListPointInt32``   |               |
+--------------+---------------------------+---------------+

The function implements the Direct Ellipse Fit fit by Fitzgibbon et al.: A. W. Fitzgibbon, M. Pilu, R. B. Fisher, "Direct Least Squares Fitting of Ellipses", IEEE Trans.PAMI, Vol. 21, pages 476 - 480 (1999),

employing a numerically stable version of this fit published by Halir and Flusser: HALIR, Radim; FLUSSER, Jan, "Numerically stable direct least squares fitting of ellipses", In: Proc. 6th International Conference in Central Europe on Computer Graphics and Visualization. WSCG. 1998. p. 125 - 132.

The method incorporates the ellipticity constraint 4AC - B^2 meaning that, being ellipse-specific, even bad data will always return an ellipse. (Avoiding hyperbolas, etc...)

All points are considered; there is no detection or rejection of outliers.

If you need robust ellipse fitting with outlier detection, consider using the robust\_fit\_ellipse method.

Method *FitEllipse*
^^^^^^^^^^^^^^^^^^^

``Ellipse FitEllipse(PointListPointDouble points)``

This function calculates an ellipse fitted to a set of five or more points.

The method **FitEllipse** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``points``   | ``PointListPointDouble``   |               |
+--------------+----------------------------+---------------+

The function implements the Direct Ellipse Fit fit by Fitzgibbon et al.: A. W. Fitzgibbon, M. Pilu, R. B. Fisher, "Direct Least Squares Fitting of Ellipses", IEEE Trans.PAMI, Vol. 21, pages 476 - 480 (1999),

employing a numerically stable version of this fit published by Halir and Flusser: HALIR, Radim; FLUSSER, Jan, "Numerically stable direct least squares fitting of ellipses", In: Proc. 6th International Conference in Central Europe on Computer Graphics and Visualization. WSCG. 1998. p. 125 - 132.

The method incorporates the ellipticity constraint 4AC - B^2 meaning that, being ellipse-specific, even bad data will always return an ellipse. (Avoiding hyperbolas, etc...)

All points are considered; there is no detection or rejection of outliers.

If you need robust ellipse fitting with outlier detection, consider using the robust\_fit\_ellipse method.

Method *FitEllipse*
^^^^^^^^^^^^^^^^^^^

``Ellipse FitEllipse(PointList points)``

This function calculates an ellipse fitted to a set of five or more points.

The method **FitEllipse** has the following parameters:

+--------------+-----------------+---------------+
| Parameter    | Type            | Description   |
+==============+=================+===============+
| ``points``   | ``PointList``   |               |
+--------------+-----------------+---------------+

The function implements the Direct Ellipse Fit fit by Fitzgibbon et al.: A. W. Fitzgibbon, M. Pilu, R. B. Fisher, "Direct Least Squares Fitting of Ellipses", IEEE Trans.PAMI, Vol. 21, pages 476 - 480 (1999),

employing a numerically stable version of this fit published by Halir and Flusser: HALIR, Radim; FLUSSER, Jan, "Numerically stable direct least squares fitting of ellipses", In: Proc. 6th International Conference in Central Europe on Computer Graphics and Visualization. WSCG. 1998. p. 125 - 132.

The method incorporates the ellipticity constraint 4AC - B^2 meaning that, being ellipse-specific, even bad data will always return an ellipse. (Avoiding hyperbolas, etc...)

All points are considered; there is no detection or rejection of outliers.

If you need robust ellipse fitting with outlier detection, consider using the robust\_fit\_ellipse method.

Method *RobustFitLine*
^^^^^^^^^^^^^^^^^^^^^^

``FitResultLineDoubleInt32 RobustFitLine(PointListPointInt32 points, System.Double sigma, System.Double alpha)``

A robust line estimator which fits a model from a measured list of points with outliers.

The method **RobustFitLine** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``points``   | ``PointListPointInt32``   |               |
+--------------+---------------------------+---------------+
| ``sigma``    | ``System.Double``         |               |
+--------------+---------------------------+---------------+
| ``alpha``    | ``System.Double``         |               |
+--------------+---------------------------+---------------+

Method *RobustFitLine*
^^^^^^^^^^^^^^^^^^^^^^

``FitResultLineDoubleDouble RobustFitLine(PointListPointDouble points, System.Double sigma, System.Double alpha)``

A robust line estimator which fits a model from a measured list of points with outliers.

The method **RobustFitLine** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``points``   | ``PointListPointDouble``   |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Method *RobustFitLine*
^^^^^^^^^^^^^^^^^^^^^^

``FitResult RobustFitLine(PointList points, System.Double sigma, System.Double alpha)``

A robust line estimator which fits a model from a measured list of points with outliers.

The method **RobustFitLine** has the following parameters:

+--------------+---------------------+---------------+
| Parameter    | Type                | Description   |
+==============+=====================+===============+
| ``points``   | ``PointList``       |               |
+--------------+---------------------+---------------+
| ``sigma``    | ``System.Double``   |               |
+--------------+---------------------+---------------+
| ``alpha``    | ``System.Double``   |               |
+--------------+---------------------+---------------+

Method *RobustFitCircle*
^^^^^^^^^^^^^^^^^^^^^^^^

``FitResultCircleInt32 RobustFitCircle(PointListPointInt32 points, System.Double sigma, System.Double alpha)``

A robust circle estimator which fits a model from a measured list of points with outliers.

The method **RobustFitCircle** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``points``   | ``PointListPointInt32``   |               |
+--------------+---------------------------+---------------+
| ``sigma``    | ``System.Double``         |               |
+--------------+---------------------------+---------------+
| ``alpha``    | ``System.Double``         |               |
+--------------+---------------------------+---------------+

Method *RobustFitCircle*
^^^^^^^^^^^^^^^^^^^^^^^^

``FitResultCircleDouble RobustFitCircle(PointListPointDouble points, System.Double sigma, System.Double alpha)``

A robust circle estimator which fits a model from a measured list of points with outliers.

The method **RobustFitCircle** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``points``   | ``PointListPointDouble``   |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Method *RobustFitCircle*
^^^^^^^^^^^^^^^^^^^^^^^^

``FitResult RobustFitCircle(PointList points, System.Double sigma, System.Double alpha)``

A robust circle estimator which fits a model from a measured list of points with outliers.

The method **RobustFitCircle** has the following parameters:

+--------------+---------------------+---------------+
| Parameter    | Type                | Description   |
+==============+=====================+===============+
| ``points``   | ``PointList``       |               |
+--------------+---------------------+---------------+
| ``sigma``    | ``System.Double``   |               |
+--------------+---------------------+---------------+
| ``alpha``    | ``System.Double``   |               |
+--------------+---------------------+---------------+

Method *RobustFitEllipse*
^^^^^^^^^^^^^^^^^^^^^^^^^

``FitResultEllipseInt32 RobustFitEllipse(PointListPointInt32 points, System.Double sigma, System.Double alpha)``

A robust ellipse estimator which fits a model from a measured list of points with outliers.

The method **RobustFitEllipse** has the following parameters:

+--------------+---------------------------+---------------+
| Parameter    | Type                      | Description   |
+==============+===========================+===============+
| ``points``   | ``PointListPointInt32``   |               |
+--------------+---------------------------+---------------+
| ``sigma``    | ``System.Double``         |               |
+--------------+---------------------------+---------------+
| ``alpha``    | ``System.Double``         |               |
+--------------+---------------------------+---------------+

Method *RobustFitEllipse*
^^^^^^^^^^^^^^^^^^^^^^^^^

``FitResultEllipseDouble RobustFitEllipse(PointListPointDouble points, System.Double sigma, System.Double alpha)``

A robust ellipse estimator which fits a model from a measured list of points with outliers.

The method **RobustFitEllipse** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``points``   | ``PointListPointDouble``   |               |
+--------------+----------------------------+---------------+
| ``sigma``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+
| ``alpha``    | ``System.Double``          |               |
+--------------+----------------------------+---------------+

Method *RobustFitEllipse*
^^^^^^^^^^^^^^^^^^^^^^^^^

``FitResult RobustFitEllipse(PointList points, System.Double sigma, System.Double alpha)``

A robust ellipse estimator which fits a model from a measured list of points with outliers.

The method **RobustFitEllipse** has the following parameters:

+--------------+---------------------+---------------+
| Parameter    | Type                | Description   |
+==============+=====================+===============+
| ``points``   | ``PointList``       |               |
+--------------+---------------------+---------------+
| ``sigma``    | ``System.Double``   |               |
+--------------+---------------------+---------------+
| ``alpha``    | ``System.Double``   |               |
+--------------+---------------------+---------------+

Method *GaugeLine*
^^^^^^^^^^^^^^^^^^

``LineDouble GaugeLine(ViewLocatorByte source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a line in the source view.

The method **GaugeLine** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorByte``                            |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

A set of line profiles in either horizontal or vertical direction is used to calculate a set of points. The set of points is then used to fit a line.

The line with respect to the left or top position of the source view.

Method *GaugeLine*
^^^^^^^^^^^^^^^^^^

``LineDouble GaugeLine(ViewLocatorUInt16 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a line in the source view.

The method **GaugeLine** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorUInt16``                          |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

A set of line profiles in either horizontal or vertical direction is used to calculate a set of points. The set of points is then used to fit a line.

The line with respect to the left or top position of the source view.

Method *GaugeLine*
^^^^^^^^^^^^^^^^^^

``LineDouble GaugeLine(ViewLocatorUInt32 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a line in the source view.

The method **GaugeLine** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorUInt32``                          |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

A set of line profiles in either horizontal or vertical direction is used to calculate a set of points. The set of points is then used to fit a line.

The line with respect to the left or top position of the source view.

Method *GaugeLine*
^^^^^^^^^^^^^^^^^^

``LineDouble GaugeLine(ViewLocatorDouble source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a line in the source view.

The method **GaugeLine** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorDouble``                          |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

A set of line profiles in either horizontal or vertical direction is used to calculate a set of points. The set of points is then used to fit a line.

The line with respect to the left or top position of the source view.

Method *GaugeLine*
^^^^^^^^^^^^^^^^^^

``LineDouble GaugeLine(ViewLocatorRgbByte source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a line in the source view.

The method **GaugeLine** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbByte``                         |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

A set of line profiles in either horizontal or vertical direction is used to calculate a set of points. The set of points is then used to fit a line.

The line with respect to the left or top position of the source view.

Method *GaugeLine*
^^^^^^^^^^^^^^^^^^

``LineDouble GaugeLine(ViewLocatorRgbUInt16 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a line in the source view.

The method **GaugeLine** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbUInt16``                       |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

A set of line profiles in either horizontal or vertical direction is used to calculate a set of points. The set of points is then used to fit a line.

The line with respect to the left or top position of the source view.

Method *GaugeLine*
^^^^^^^^^^^^^^^^^^

``LineDouble GaugeLine(ViewLocatorRgbUInt32 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a line in the source view.

The method **GaugeLine** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbUInt32``                       |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

A set of line profiles in either horizontal or vertical direction is used to calculate a set of points. The set of points is then used to fit a line.

The line with respect to the left or top position of the source view.

Method *GaugeLine*
^^^^^^^^^^^^^^^^^^

``LineDouble GaugeLine(ViewLocatorRgbDouble source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a line in the source view.

The method **GaugeLine** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbDouble``                       |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

A set of line profiles in either horizontal or vertical direction is used to calculate a set of points. The set of points is then used to fit a line.

The line with respect to the left or top position of the source view.

Method *GaugeLine*
^^^^^^^^^^^^^^^^^^

``LineDouble GaugeLine(ViewLocatorRgbaByte source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a line in the source view.

The method **GaugeLine** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbaByte``                        |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

A set of line profiles in either horizontal or vertical direction is used to calculate a set of points. The set of points is then used to fit a line.

The line with respect to the left or top position of the source view.

Method *GaugeLine*
^^^^^^^^^^^^^^^^^^

``LineDouble GaugeLine(ViewLocatorRgbaUInt16 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a line in the source view.

The method **GaugeLine** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbaUInt16``                      |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

A set of line profiles in either horizontal or vertical direction is used to calculate a set of points. The set of points is then used to fit a line.

The line with respect to the left or top position of the source view.

Method *GaugeLine*
^^^^^^^^^^^^^^^^^^

``LineDouble GaugeLine(ViewLocatorRgbaUInt32 source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a line in the source view.

The method **GaugeLine** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbaUInt32``                      |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

A set of line profiles in either horizontal or vertical direction is used to calculate a set of points. The set of points is then used to fit a line.

The line with respect to the left or top position of the source view.

Method *GaugeLine*
^^^^^^^^^^^^^^^^^^

``LineDouble GaugeLine(ViewLocatorRgbaDouble source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a line in the source view.

The method **GaugeLine** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``ViewLocatorRgbaDouble``                      |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

A set of line profiles in either horizontal or vertical direction is used to calculate a set of points. The set of points is then used to fit a line.

The line with respect to the left or top position of the source view.

Method *GaugeLine*
^^^^^^^^^^^^^^^^^^

``LineDouble GaugeLine(View source, GaugingAlgorithms.EdgeGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Int32 spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a line in the source view.

The method **GaugeLine** has the following parameters:

+-------------------+------------------------------------------------+---------------+
| Parameter         | Type                                           | Description   |
+===================+================================================+===============+
| ``source``        | ``View``                                       |               |
+-------------------+------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.EdgeGaugingOrientation``   |               |
+-------------------+------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``             |               |
+-------------------+------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``             |               |
+-------------------+------------------------------------------------+---------------+
| ``spacing``       | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                               |               |
+-------------------+------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                              |               |
+-------------------+------------------------------------------------+---------------+

A set of line profiles in either horizontal or vertical direction is used to calculate a set of points. The set of points is then used to fit a line.

The line with respect to the left or top position of the source view.

Method *GaugeCircle*
^^^^^^^^^^^^^^^^^^^^

``Circle GaugeCircle(ViewLocatorByte source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a circle in the source view.

The method **GaugeCircle** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorByte``                                |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit a circle.

The circle with respect to the left or top position of the source view.

Method *GaugeCircle*
^^^^^^^^^^^^^^^^^^^^

``Circle GaugeCircle(ViewLocatorUInt16 source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a circle in the source view.

The method **GaugeCircle** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorUInt16``                              |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit a circle.

The circle with respect to the left or top position of the source view.

Method *GaugeCircle*
^^^^^^^^^^^^^^^^^^^^

``Circle GaugeCircle(ViewLocatorUInt32 source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a circle in the source view.

The method **GaugeCircle** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorUInt32``                              |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit a circle.

The circle with respect to the left or top position of the source view.

Method *GaugeCircle*
^^^^^^^^^^^^^^^^^^^^

``Circle GaugeCircle(ViewLocatorDouble source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a circle in the source view.

The method **GaugeCircle** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorDouble``                              |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit a circle.

The circle with respect to the left or top position of the source view.

Method *GaugeCircle*
^^^^^^^^^^^^^^^^^^^^

``Circle GaugeCircle(ViewLocatorRgbByte source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a circle in the source view.

The method **GaugeCircle** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbByte``                             |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit a circle.

The circle with respect to the left or top position of the source view.

Method *GaugeCircle*
^^^^^^^^^^^^^^^^^^^^

``Circle GaugeCircle(ViewLocatorRgbUInt16 source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a circle in the source view.

The method **GaugeCircle** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbUInt16``                           |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit a circle.

The circle with respect to the left or top position of the source view.

Method *GaugeCircle*
^^^^^^^^^^^^^^^^^^^^

``Circle GaugeCircle(ViewLocatorRgbUInt32 source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a circle in the source view.

The method **GaugeCircle** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbUInt32``                           |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit a circle.

The circle with respect to the left or top position of the source view.

Method *GaugeCircle*
^^^^^^^^^^^^^^^^^^^^

``Circle GaugeCircle(ViewLocatorRgbDouble source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a circle in the source view.

The method **GaugeCircle** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbDouble``                           |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit a circle.

The circle with respect to the left or top position of the source view.

Method *GaugeCircle*
^^^^^^^^^^^^^^^^^^^^

``Circle GaugeCircle(ViewLocatorRgbaByte source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a circle in the source view.

The method **GaugeCircle** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbaByte``                            |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit a circle.

The circle with respect to the left or top position of the source view.

Method *GaugeCircle*
^^^^^^^^^^^^^^^^^^^^

``Circle GaugeCircle(ViewLocatorRgbaUInt16 source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a circle in the source view.

The method **GaugeCircle** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbaUInt16``                          |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit a circle.

The circle with respect to the left or top position of the source view.

Method *GaugeCircle*
^^^^^^^^^^^^^^^^^^^^

``Circle GaugeCircle(ViewLocatorRgbaUInt32 source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a circle in the source view.

The method **GaugeCircle** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbaUInt32``                          |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit a circle.

The circle with respect to the left or top position of the source view.

Method *GaugeCircle*
^^^^^^^^^^^^^^^^^^^^

``Circle GaugeCircle(ViewLocatorRgbaDouble source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a circle in the source view.

The method **GaugeCircle** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbaDouble``                          |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit a circle.

The circle with respect to the left or top position of the source view.

Method *GaugeCircle*
^^^^^^^^^^^^^^^^^^^^

``Circle GaugeCircle(View source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of a circle in the source view.

The method **GaugeCircle** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``View``                                           |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit a circle.

The circle with respect to the left or top position of the source view.

Method *GaugeEllipse*
^^^^^^^^^^^^^^^^^^^^^

``Ellipse GaugeEllipse(ViewLocatorByte source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of an ellipse in the source view.

The method **GaugeEllipse** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorByte``                                |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit an ellipse.

The ellipse with respect to the left or top position of the source view.

Method *GaugeEllipse*
^^^^^^^^^^^^^^^^^^^^^

``Ellipse GaugeEllipse(ViewLocatorUInt16 source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of an ellipse in the source view.

The method **GaugeEllipse** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorUInt16``                              |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit an ellipse.

The ellipse with respect to the left or top position of the source view.

Method *GaugeEllipse*
^^^^^^^^^^^^^^^^^^^^^

``Ellipse GaugeEllipse(ViewLocatorUInt32 source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of an ellipse in the source view.

The method **GaugeEllipse** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorUInt32``                              |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit an ellipse.

The ellipse with respect to the left or top position of the source view.

Method *GaugeEllipse*
^^^^^^^^^^^^^^^^^^^^^

``Ellipse GaugeEllipse(ViewLocatorDouble source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of an ellipse in the source view.

The method **GaugeEllipse** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorDouble``                              |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit an ellipse.

The ellipse with respect to the left or top position of the source view.

Method *GaugeEllipse*
^^^^^^^^^^^^^^^^^^^^^

``Ellipse GaugeEllipse(ViewLocatorRgbByte source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of an ellipse in the source view.

The method **GaugeEllipse** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbByte``                             |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit an ellipse.

The ellipse with respect to the left or top position of the source view.

Method *GaugeEllipse*
^^^^^^^^^^^^^^^^^^^^^

``Ellipse GaugeEllipse(ViewLocatorRgbUInt16 source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of an ellipse in the source view.

The method **GaugeEllipse** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbUInt16``                           |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit an ellipse.

The ellipse with respect to the left or top position of the source view.

Method *GaugeEllipse*
^^^^^^^^^^^^^^^^^^^^^

``Ellipse GaugeEllipse(ViewLocatorRgbUInt32 source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of an ellipse in the source view.

The method **GaugeEllipse** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbUInt32``                           |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit an ellipse.

The ellipse with respect to the left or top position of the source view.

Method *GaugeEllipse*
^^^^^^^^^^^^^^^^^^^^^

``Ellipse GaugeEllipse(ViewLocatorRgbDouble source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of an ellipse in the source view.

The method **GaugeEllipse** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbDouble``                           |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit an ellipse.

The ellipse with respect to the left or top position of the source view.

Method *GaugeEllipse*
^^^^^^^^^^^^^^^^^^^^^

``Ellipse GaugeEllipse(ViewLocatorRgbaByte source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of an ellipse in the source view.

The method **GaugeEllipse** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbaByte``                            |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit an ellipse.

The ellipse with respect to the left or top position of the source view.

Method *GaugeEllipse*
^^^^^^^^^^^^^^^^^^^^^

``Ellipse GaugeEllipse(ViewLocatorRgbaUInt16 source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of an ellipse in the source view.

The method **GaugeEllipse** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbaUInt16``                          |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit an ellipse.

The ellipse with respect to the left or top position of the source view.

Method *GaugeEllipse*
^^^^^^^^^^^^^^^^^^^^^

``Ellipse GaugeEllipse(ViewLocatorRgbaUInt32 source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of an ellipse in the source view.

The method **GaugeEllipse** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbaUInt32``                          |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit an ellipse.

The ellipse with respect to the left or top position of the source view.

Method *GaugeEllipse*
^^^^^^^^^^^^^^^^^^^^^

``Ellipse GaugeEllipse(ViewLocatorRgbaDouble source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of an ellipse in the source view.

The method **GaugeEllipse** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``ViewLocatorRgbaDouble``                          |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit an ellipse.

The ellipse with respect to the left or top position of the source view.

Method *GaugeEllipse*
^^^^^^^^^^^^^^^^^^^^^

``Ellipse GaugeEllipse(View source, GaugingAlgorithms.CircularGaugingOrientation orientation, GaugingAlgorithms.EdgePolarity polarity, GaugingAlgorithms.EdgeSelector selector, System.Double spacing, System.Int32 thickness, System.Double sigma, System.Double threshold)``

Calculates the position of an ellipse in the source view.

The method **GaugeEllipse** has the following parameters:

+-------------------+----------------------------------------------------+---------------+
| Parameter         | Type                                               | Description   |
+===================+====================================================+===============+
| ``source``        | ``View``                                           |               |
+-------------------+----------------------------------------------------+---------------+
| ``orientation``   | ``GaugingAlgorithms.CircularGaugingOrientation``   |               |
+-------------------+----------------------------------------------------+---------------+
| ``polarity``      | ``GaugingAlgorithms.EdgePolarity``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``selector``      | ``GaugingAlgorithms.EdgeSelector``                 |               |
+-------------------+----------------------------------------------------+---------------+
| ``spacing``       | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``thickness``     | ``System.Int32``                                   |               |
+-------------------+----------------------------------------------------+---------------+
| ``sigma``         | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+
| ``threshold``     | ``System.Double``                                  |               |
+-------------------+----------------------------------------------------+---------------+

A set of linear profiles in radial direction (starting at the middle of the source view going outwards) is used to find a set of edge points. The set of points is then used to fit an ellipse.

The ellipse with respect to the left or top position of the source view.

Enumerations
~~~~~~~~~~~~

Enumeration *EdgeGaugingOrientation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum EdgeGaugingOrientation``

TODO documentation missing

The enumeration **EdgeGaugingOrientation** has the following constants:

+-------------------+---------+---------------+
| Name              | Value   | Description   |
+===================+=========+===============+
| ``leftToRight``   | ``0``   |               |
+-------------------+---------+---------------+
| ``topToBottom``   | ``1``   |               |
+-------------------+---------+---------------+
| ``rightToLeft``   | ``2``   |               |
+-------------------+---------+---------------+
| ``bottomToTop``   | ``3``   |               |
+-------------------+---------+---------------+

::

    enum EdgeGaugingOrientation
    {
      leftToRight = 0,
      topToBottom = 1,
      rightToLeft = 2,
      bottomToTop = 3,
    };

TODO documentation missing

Enumeration *EdgePolarity*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum EdgePolarity``

TODO documentation missing

The enumeration **EdgePolarity** has the following constants:

+---------------+---------+---------------+
| Name          | Value   | Description   |
+===============+=========+===============+
| ``rising``    | ``0``   |               |
+---------------+---------+---------------+
| ``falling``   | ``1``   |               |
+---------------+---------+---------------+
| ``both``      | ``2``   |               |
+---------------+---------+---------------+

::

    enum EdgePolarity
    {
      rising = 0,
      falling = 1,
      both = 2,
    };

TODO documentation missing

Enumeration *EdgeSelector*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum EdgeSelector``

TODO documentation missing

The enumeration **EdgeSelector** has the following constants:

+-------------+---------+---------------+
| Name        | Value   | Description   |
+=============+=========+===============+
| ``first``   | ``0``   |               |
+-------------+---------+---------------+
| ``best``    | ``1``   |               |
+-------------+---------+---------------+
| ``last``    | ``2``   |               |
+-------------+---------+---------------+
| ``all``     | ``3``   |               |
+-------------+---------+---------------+

::

    enum EdgeSelector
    {
      first = 0,
      best = 1,
      last = 2,
      all = 3,
    };

TODO documentation missing

Enumeration *CircularGaugingOrientation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum CircularGaugingOrientation``

TODO documentation missing

The enumeration **CircularGaugingOrientation** has the following constants:

+-----------------------+---------+---------------+
| Name                  | Value   | Description   |
+=======================+=========+===============+
| ``insideToOutside``   | ``0``   |               |
+-----------------------+---------+---------------+
| ``outsideToInside``   | ``1``   |               |
+-----------------------+---------+---------------+

::

    enum CircularGaugingOrientation
    {
      insideToOutside = 0,
      outsideToInside = 1,
    };

TODO documentation missing
