Class *UeyeDeviceException*
---------------------------

The uEye specific device\_exception type.

**Namespace:** Ngi

**Module:**

The class **UeyeDeviceException** contains the following enumerations:

+--------------------+------------------------------+
| Enumeration        | Description                  |
+====================+==============================+
| ``ExceptionIds``   | TODO documentation missing   |
+--------------------+------------------------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *UeyeDeviceException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``UeyeDeviceException()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *UeyeDeviceException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``UeyeDeviceException(System.String message)``

Construct an exception from a message.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+

Constructor *UeyeDeviceException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``UeyeDeviceException(System.String message, System.UInt32 id)``

Construct an exception from a message and an id.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+
| ``id``        | ``System.UInt32``   | The exception id.        |
+---------------+---------------------+--------------------------+

Enumerations
~~~~~~~~~~~~

Enumeration *ExceptionIds*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum ExceptionIds``

TODO documentation missing

The enumeration **ExceptionIds** has the following constants:

+---------------------------------+---------+---------------+
| Name                            | Value   | Description   |
+=================================+=========+===============+
| ``notAnError``                  | ``0``   |               |
+---------------------------------+---------+---------------+
| ``nativeUeyeError``             | ``1``   |               |
+---------------------------------+---------+---------------+
| ``cameraIdOutOfRange``          | ``2``   |               |
+---------------------------------+---------+---------------+
| ``gainboostNotSupported``       | ``3``   |               |
+---------------------------------+---------+---------------+
| ``gammaOutOfRange``             | ``4``   |               |
+---------------------------------+---------+---------------+
| ``globalShutterNotSupported``   | ``5``   |               |
+---------------------------------+---------+---------------+

::

    enum ExceptionIds
    {
      notAnError = 0,
      nativeUeyeError = 1,
      cameraIdOutOfRange = 2,
      gainboostNotSupported = 3,
      gammaOutOfRange = 4,
      globalShutterNotSupported = 5,
    };

TODO documentation missing
