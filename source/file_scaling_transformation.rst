Class *ScalingTransformation*
-----------------------------

Scaling transformation of geometric primitives.

**Namespace:** Ngi

**Module:** ImageProcessing

Description
~~~~~~~~~~~

This class provides static methods to transform geometric primitives via a scaling transformation.

The class can transform these geometric primitives: point, vector, direction, line\_segment, ray, line, box, rectangle, circle, ellipse, arc elliptical\_arc, triangle, quadrilateral, polyline, quadratic\_bezier and geometry.

Some primitives retain their nature, such as a point, which is transformed into another point by an scaling transformation.

Some primitives change their nature, such as a circle, which is transformed into an ellipse by a scaling transformation.

Some primitives can have an integer coordinate type on input, but the resulting types are always of floating point coordinate type. This is because the transformation cannot retain the integer nature in general. To illustrate, a point<int> type is transformed and the result is a point<double> type.

Static Methods
~~~~~~~~~~~~~~

Method *Transform*
^^^^^^^^^^^^^^^^^^

``VectorDouble Transform(VectorDouble vector, ScalingMatrix scaling)``

Transform a vector.

The method **Transform** has the following parameters:

+---------------+---------------------+---------------+
| Parameter     | Type                | Description   |
+===============+=====================+===============+
| ``vector``    | ``VectorDouble``    |               |
+---------------+---------------------+---------------+
| ``scaling``   | ``ScalingMatrix``   |               |
+---------------+---------------------+---------------+

A vector transformed with a scaling transform results in another vector.

The scaling transformation is carried out with a matrix multiplication.

The transformed vector.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``PointDouble Transform(PointDouble point, ScalingMatrix scaling)``

Transform a point.

The method **Transform** has the following parameters:

+---------------+---------------------+---------------+
| Parameter     | Type                | Description   |
+===============+=====================+===============+
| ``point``     | ``PointDouble``     |               |
+---------------+---------------------+---------------+
| ``scaling``   | ``ScalingMatrix``   |               |
+---------------+---------------------+---------------+

A point transformed with a scaling transform results in another point.

The scaling transformation is carried out with a matrix multiplication.

The transformed vector.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Direction Transform(Direction direction, ScalingMatrix scaling)``

Transform a direction.

The method **Transform** has the following parameters:

+-----------------+---------------------+---------------+
| Parameter       | Type                | Description   |
+=================+=====================+===============+
| ``direction``   | ``Direction``       |               |
+-----------------+---------------------+---------------+
| ``scaling``     | ``ScalingMatrix``   |               |
+-----------------+---------------------+---------------+

A direction transformed with a scaling transform is another direction.

The scaling transformation is carried out with a matrix multiplication.

The transformed direction.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``LineSegmentDouble Transform(LineSegmentDouble lineSegment, ScalingMatrix scaling)``

A line segment transformed with a scaling transform results in another line segment.

The method **Transform** has the following parameters:

+-------------------+-------------------------+---------------+
| Parameter         | Type                    | Description   |
+===================+=========================+===============+
| ``lineSegment``   | ``LineSegmentDouble``   |               |
+-------------------+-------------------------+---------------+
| ``scaling``       | ``ScalingMatrix``       |               |
+-------------------+-------------------------+---------------+

The scaling transformation is carried out with a matrix multiplication.

The transformed line segment.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``RayDouble Transform(RayDouble ray, ScalingMatrix scaling)``

Transform a ray.

The method **Transform** has the following parameters:

+---------------+---------------------+---------------+
| Parameter     | Type                | Description   |
+===============+=====================+===============+
| ``ray``       | ``RayDouble``       |               |
+---------------+---------------------+---------------+
| ``scaling``   | ``ScalingMatrix``   |               |
+---------------+---------------------+---------------+

A ray transformed with a scaling transform results in another ray.

The scaling transformation is carried out with a matrix multiplication.

The transformed ray.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``LineDouble Transform(LineDouble line, ScalingMatrix scaling)``

Transform a line.

The method **Transform** has the following parameters:

+---------------+---------------------+---------------+
| Parameter     | Type                | Description   |
+===============+=====================+===============+
| ``line``      | ``LineDouble``      |               |
+---------------+---------------------+---------------+
| ``scaling``   | ``ScalingMatrix``   |               |
+---------------+---------------------+---------------+

A line transformed with a scaling transform results in another line.

The scaling transformation is carried out with a matrix multiplication.

The transformed line.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``BoxDouble Transform(BoxDouble box, ScalingMatrix scaling)``

Transform a box.

The method **Transform** has the following parameters:

+---------------+---------------------+---------------+
| Parameter     | Type                | Description   |
+===============+=====================+===============+
| ``box``       | ``BoxDouble``       |               |
+---------------+---------------------+---------------+
| ``scaling``   | ``ScalingMatrix``   |               |
+---------------+---------------------+---------------+

A box transformed with a scaling transform results in another box.

The scaling transformation is carried out with a matrix multiplication.

The transformed box in the form of a rectangle.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Rectangle Transform(Rectangle box, ScalingMatrix scaling)``

Transform a rectangle.

The method **Transform** has the following parameters:

+---------------+---------------------+---------------+
| Parameter     | Type                | Description   |
+===============+=====================+===============+
| ``box``       | ``Rectangle``       |               |
+---------------+---------------------+---------------+
| ``scaling``   | ``ScalingMatrix``   |               |
+---------------+---------------------+---------------+

A rectangle transformed with a scaling transform results in another rectangle.

The scaling transformation is carried out with a matrix multiplication.

The transformed box in the form of a rectangle.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Ellipse Transform(Circle circle, ScalingMatrix scaling)``

Transform a circle.

The method **Transform** has the following parameters:

+---------------+---------------------+---------------+
| Parameter     | Type                | Description   |
+===============+=====================+===============+
| ``circle``    | ``Circle``          |               |
+---------------+---------------------+---------------+
| ``scaling``   | ``ScalingMatrix``   |               |
+---------------+---------------------+---------------+

A circle transformed with a scaling transform results in another circle.

The scaling transformation is carried out with a matrix multiplication.

The transformed circle in the form of an ellipse.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Ellipse Transform(Ellipse ellipse, ScalingMatrix scaling)``

Transform an ellipse.

The method **Transform** has the following parameters:

+---------------+---------------------+---------------+
| Parameter     | Type                | Description   |
+===============+=====================+===============+
| ``ellipse``   | ``Ellipse``         |               |
+---------------+---------------------+---------------+
| ``scaling``   | ``ScalingMatrix``   |               |
+---------------+---------------------+---------------+

An ellipse transformed with a scaling transform results in another ellipse.

The scaling transformation is carried out with a matrix multiplication.

The transformed ellipse.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``EllipticalArc Transform(Arc arc, ScalingMatrix scaling)``

Transform an arc.

The method **Transform** has the following parameters:

+---------------+---------------------+---------------+
| Parameter     | Type                | Description   |
+===============+=====================+===============+
| ``arc``       | ``Arc``             |               |
+---------------+---------------------+---------------+
| ``scaling``   | ``ScalingMatrix``   |               |
+---------------+---------------------+---------------+

An arc transformed with a scaling transform results in another arc.

The scaling transformation is carried out with a matrix multiplication.

The transformed arc.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``EllipticalArc Transform(EllipticalArc ellipticalArc, ScalingMatrix scaling)``

Transform an elliptical arc.

The method **Transform** has the following parameters:

+---------------------+---------------------+---------------+
| Parameter           | Type                | Description   |
+=====================+=====================+===============+
| ``ellipticalArc``   | ``EllipticalArc``   |               |
+---------------------+---------------------+---------------+
| ``scaling``         | ``ScalingMatrix``   |               |
+---------------------+---------------------+---------------+

An elliptical arc transformed with a scaling transform results in another elliptical arc

The scaling transformation is carried out with a matrix multiplication.

The transformed elliptical arc.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``TriangleDouble Transform(TriangleDouble triangle, ScalingMatrix scaling)``

Transform a triangle.

The method **Transform** has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``triangle``   | ``TriangleDouble``   |               |
+----------------+----------------------+---------------+
| ``scaling``    | ``ScalingMatrix``    |               |
+----------------+----------------------+---------------+

A triangle transformed with a scaling transform results in another triangle.

The scaling transformation is carried out with a matrix multiplication.

The transformed triangle.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``QuadrilateralDouble Transform(QuadrilateralDouble quadrilateral, ScalingMatrix scaling)``

Transform a quadrilateral.

The method **Transform** has the following parameters:

+---------------------+---------------------------+---------------+
| Parameter           | Type                      | Description   |
+=====================+===========================+===============+
| ``quadrilateral``   | ``QuadrilateralDouble``   |               |
+---------------------+---------------------------+---------------+
| ``scaling``         | ``ScalingMatrix``         |               |
+---------------------+---------------------------+---------------+

A quadrilateral transformed with a scaling transform results in another quadrilateral.

The scaling transformation is carried out with a matrix multiplication.

The transformed quadrilateral.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``PolylineDouble Transform(PolylineDouble polyline, ScalingMatrix scaling)``

Transform a polyline.

The method **Transform** has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``polyline``   | ``PolylineDouble``   |               |
+----------------+----------------------+---------------+
| ``scaling``    | ``ScalingMatrix``    |               |
+----------------+----------------------+---------------+

A polyline transformed with a scaling transform results in another polyline.

The scaling transformation is carried out with a matrix multiplication.

The transformed polyline.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``QuadraticBezier Transform(QuadraticBezier bezier, IsotropicScalingMatrix isotropicScaling)``

Transform a quadratic bezier curve.

The method **Transform** has the following parameters:

+------------------------+------------------------------+---------------+
| Parameter              | Type                         | Description   |
+========================+==============================+===============+
| ``bezier``             | ``QuadraticBezier``          |               |
+------------------------+------------------------------+---------------+
| ``isotropicScaling``   | ``IsotropicScalingMatrix``   |               |
+------------------------+------------------------------+---------------+

A quadratic bezier curve transformed with a scaling transform results in another quadratic bezier curve.

The scaling transformation is carried out with a matrix multiplication.

The transformed quadratic bezier curve.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``CubicBezier Transform(CubicBezier bezier, ScalingMatrix scaling)``

Transform a cubic bezier curve.

The method **Transform** has the following parameters:

+---------------+---------------------+---------------+
| Parameter     | Type                | Description   |
+===============+=====================+===============+
| ``bezier``    | ``CubicBezier``     |               |
+---------------+---------------------+---------------+
| ``scaling``   | ``ScalingMatrix``   |               |
+---------------+---------------------+---------------+

A cubic bezier curve transformed with a scaling transform results in another cubic bezier curve.

The scaling transformation is carried out with a matrix multiplication.

The transformed cubic bezier curve.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Geometry Transform(Geometry geometry, ScalingMatrix scaling)``

Transform a geometry.

The method **Transform** has the following parameters:

+----------------+---------------------+---------------+
| Parameter      | Type                | Description   |
+================+=====================+===============+
| ``geometry``   | ``Geometry``        |               |
+----------------+---------------------+---------------+
| ``scaling``    | ``ScalingMatrix``   |               |
+----------------+---------------------+---------------+

A geometry transformed with a scaling transform results in another geometry.

The scaling transformation is carried out with a matrix multiplication.

The transformed geometry.
