Class *BarcodeList*
-------------------

**Namespace:** Ngi

**Module:** BarcodeMatrixcode

The class **BarcodeList** implements the following interfaces:

+--------------------+
| Interface          |
+====================+
| ``IListBarcode``   |
+--------------------+

The class **BarcodeList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **BarcodeList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *BarcodeList*
^^^^^^^^^^^^^^^^^^^^^^^^^

``BarcodeList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``Barcode [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``BarcodeEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(Barcode item)``

The method **Add** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``item``    | ``Barcode``   |               |
+-------------+---------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(Barcode item)``

The method **Contains** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``item``    | ``Barcode``   |               |
+-------------+---------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(Barcode item)``

The method **Remove** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``item``    | ``Barcode``   |               |
+-------------+---------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(Barcode item)``

The method **IndexOf** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``item``    | ``Barcode``   |               |
+-------------+---------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, Barcode item)``

The method **Insert** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``item``    | ``Barcode``        |               |
+-------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
