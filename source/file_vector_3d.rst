Class *Vector3d*
----------------

A vector in three-dimensional euclidean space.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Vector3d** implements the following interfaces:

+---------------------------------------------------+
| Interface                                         |
+===================================================+
| ``IEquatableVector3dVector3dCoordinateVariant``   |
+---------------------------------------------------+
| ``ISerializable``                                 |
+---------------------------------------------------+
| ``INotifyPropertyChanged``                        |
+---------------------------------------------------+

The class **Vector3d** contains the following variant parameters:

+------------------+-----------------------------------------+
| Variant          | Description                             |
+==================+=========================================+
| ``Coordinate``   | TODO no brief description for variant   |
+------------------+-----------------------------------------+

The class **Vector3d** contains the following properties:

+------------+-------+-------+-----------------------------------------------+
| Property   | Get   | Set   | Description                                   |
+============+=======+=======+===============================================+
| ``Dx``     | \*    | \*    | The horizontal component of the vector\_3d.   |
+------------+-------+-------+-----------------------------------------------+
| ``Dy``     | \*    | \*    | The vertical component of the vector\_3d.     |
+------------+-------+-------+-----------------------------------------------+
| ``Dz``     | \*    | \*    | The planar component of the vector\_3d.       |
+------------+-------+-------+-----------------------------------------------+

The class **Vector3d** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

A vector\_3d can be seen as a vector in three-dimensional space.

The following operations are implemented: operator == : comparison for equality. operator != : comparison for inequality. operator- : negation, directly implemented. operator+= : addition, directly implemented. operator+ : addition, implemented through boost::additive operator-= : subtraction, directly implemented. operator- : subtraction, implemented through boost::additive operator\ *= : multiplication, directly implemented. operator* : multiplication, implemented through boost::multiplicative operator/= : division, directly implemented. operator/ : division, implemented through boost::multiplicative

Variants
~~~~~~~~

Variant *Coordinate*
^^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Coordinate** has the following types:

+--------------+
| Type         |
+==============+
| ``Int32``    |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Vector3d*
^^^^^^^^^^^^^^^^^^^^^^

``Vector3d()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *Vector3d*
^^^^^^^^^^^^^^^^^^^^^^

``Vector3d(System.Object dx, System.Object dy, System.Object dz)``

Standard constructor.

The constructor has the following parameters:

+-------------+---------------------+-------------------------+
| Parameter   | Type                | Description             |
+=============+=====================+=========================+
| ``dx``      | ``System.Object``   | The horizontal width.   |
+-------------+---------------------+-------------------------+
| ``dy``      | ``System.Object``   | The vertical height.    |
+-------------+---------------------+-------------------------+
| ``dz``      | ``System.Object``   | The planar depth.       |
+-------------+---------------------+-------------------------+

Properties
~~~~~~~~~~

Property *Dx*
^^^^^^^^^^^^^

``System.Object Dx``

The horizontal component of the vector\_3d.

Property *Dy*
^^^^^^^^^^^^^

``System.Object Dy``

The vertical component of the vector\_3d.

Property *Dz*
^^^^^^^^^^^^^

``System.Object Dz``

The planar component of the vector\_3d.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO documentation missing

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO documentation missing
