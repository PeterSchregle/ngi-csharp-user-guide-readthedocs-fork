Class *Int64Enumerator*
-----------------------

**Namespace:** Ngi

**Module:**

The class **Int64Enumerator** implements the following interfaces:

+------------------------+
| Interface              |
+========================+
| ``IEnumerator``        |
+------------------------+
| ``IEnumeratorInt64``   |
+------------------------+

The class **Int64Enumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **Int64Enumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``System.Int64 Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
