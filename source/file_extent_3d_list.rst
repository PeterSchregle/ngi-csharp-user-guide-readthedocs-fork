Class *Extent3dList*
--------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Extent3dList** implements the following interfaces:

+---------------------+
| Interface           |
+=====================+
| ``IListExtent3d``   |
+---------------------+

The class **Extent3dList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **Extent3dList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Extent3dList*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``Extent3dList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``Extent3d [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``Extent3dEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(Extent3d item)``

The method **Add** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``item``    | ``Extent3d``   |               |
+-------------+----------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(Extent3d item)``

The method **Contains** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``item``    | ``Extent3d``   |               |
+-------------+----------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(Extent3d item)``

The method **Remove** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``item``    | ``Extent3d``   |               |
+-------------+----------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(Extent3d item)``

The method **IndexOf** has the following parameters:

+-------------+----------------+---------------+
| Parameter   | Type           | Description   |
+=============+================+===============+
| ``item``    | ``Extent3d``   |               |
+-------------+----------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, Extent3d item)``

The method **Insert** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``item``    | ``Extent3d``       |               |
+-------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
