Class *Profile*
---------------

...

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Profile** contains the following variant parameters:

+-------------+-----------------------------------------+
| Variant     | Description                             |
+=============+=========================================+
| ``Pixel``   | TODO no brief description for variant   |
+-------------+-----------------------------------------+

Description
~~~~~~~~~~~

The base class.

Variants
~~~~~~~~

Variant *Pixel*
^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Pixel** has the following types:

+------------------+
| Type             |
+==================+
| ``Byte``         |
+------------------+
| ``UInt16``       |
+------------------+
| ``UInt32``       |
+------------------+
| ``Double``       |
+------------------+
| ``RgbByte``      |
+------------------+
| ``RgbUInt16``    |
+------------------+
| ``RgbUInt32``    |
+------------------+
| ``RgbDouble``    |
+------------------+
| ``RgbaByte``     |
+------------------+
| ``RgbaUInt16``   |
+------------------+
| ``RgbaUInt32``   |
+------------------+
| ``RgbaDouble``   |
+------------------+
| ``HlsByte``      |
+------------------+
| ``HlsUInt16``    |
+------------------+
| ``HlsDouble``    |
+------------------+
| ``HsiByte``      |
+------------------+
| ``HsiUInt16``    |
+------------------+
| ``HsiDouble``    |
+------------------+
| ``LabByte``      |
+------------------+
| ``LabUInt16``    |
+------------------+
| ``LabDouble``    |
+------------------+
| ``XyzByte``      |
+------------------+
| ``XyzUInt16``    |
+------------------+
| ``XyzDouble``    |
+------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Profile*
^^^^^^^^^^^^^^^^^^^^^

``Profile()``

Constructs an empty profile.

Constructors
~~~~~~~~~~~~

Constructor *Profile*
^^^^^^^^^^^^^^^^^^^^^

``Profile(System.UInt32 width, System.UInt32 height, System.UInt32 depth, System.Object value)``

Constructs a profile of a specific size with all elements not initialized.

The constructor has the following parameters:

+--------------+---------------------+------------------------------------------+
| Parameter    | Type                | Description                              |
+==============+=====================+==========================================+
| ``width``    | ``System.UInt32``   | The width of the constructed profile.    |
+--------------+---------------------+------------------------------------------+
| ``height``   | ``System.UInt32``   | The height of the constructed profile.   |
+--------------+---------------------+------------------------------------------+
| ``depth``    | ``System.UInt32``   | The depth of the constructed profile.    |
+--------------+---------------------+------------------------------------------+
| ``value``    | ``System.Object``   |                                          |
+--------------+---------------------+------------------------------------------+
