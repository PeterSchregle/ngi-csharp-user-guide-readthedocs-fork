Class *Moments*
---------------

Class to hold raw moments of the zeroth up to the third order.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Moments** implements the following interfaces:

+-------------------------+
| Interface               |
+=========================+
| ``IEquatableMoments``   |
+-------------------------+
| ``ISerializable``       |
+-------------------------+

The class **Moments** contains the following properties:

+------------+-------+-------+----------------------------------------------------+
| Property   | Get   | Set   | Description                                        |
+============+=======+=======+====================================================+
| ``M00``    | \*    |       | The zeroth order moment.                           |
+------------+-------+-------+----------------------------------------------------+
| ``M10``    | \*    |       | The horizontal first order moment.                 |
+------------+-------+-------+----------------------------------------------------+
| ``M01``    | \*    |       | The vertical first order moment.                   |
+------------+-------+-------+----------------------------------------------------+
| ``M20``    | \*    |       | The horizontal second order moment.                |
+------------+-------+-------+----------------------------------------------------+
| ``M11``    | \*    |       | The mixed second order moment.                     |
+------------+-------+-------+----------------------------------------------------+
| ``M02``    | \*    |       | The vertical second order moment.                  |
+------------+-------+-------+----------------------------------------------------+
| ``M30``    | \*    |       | The horizontal third order moment.                 |
+------------+-------+-------+----------------------------------------------------+
| ``M21``    | \*    |       | The first (horizontal) mixed third order moment.   |
+------------+-------+-------+----------------------------------------------------+
| ``M12``    | \*    |       | The second (vertical) mixed third order moment.    |
+------------+-------+-------+----------------------------------------------------+
| ``M03``    | \*    |       | The vertical third order moment.                   |
+------------+-------+-------+----------------------------------------------------+

The class **Moments** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

For a nice and short treatment of moments see Chaur-Chin Chen, Improved Moment Invariants for Shape Discrimination [1992].

Moments are a sort of weighted averages of the image pixels' intensities (the pixel intensities may be set to 1 in case of binary moments). They are useful to describe segmented objects. Simple properties of objects found via moments include area (or total intensity), its centroid, and information about its orientation.

Raw moments are not invariant with respect to translation, scaling and rotation.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Moments*
^^^^^^^^^^^^^^^^^^^^^

``Moments()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *Moments*
^^^^^^^^^^^^^^^^^^^^^

``Moments(System.Double m00, System.Double m10, System.Double m01, System.Double m20, System.Double m11, System.Double m02, System.Double m30, System.Double m21, System.Double m12, System.Double m03)``

Constructor.

The constructor has the following parameters:

+-------------+---------------------+----------------------------------------+
| Parameter   | Type                | Description                            |
+=============+=====================+========================================+
| ``m00``     | ``System.Double``   | Zeroth order moment.                   |
+-------------+---------------------+----------------------------------------+
| ``m10``     | ``System.Double``   | First order horizontal moment.         |
+-------------+---------------------+----------------------------------------+
| ``m01``     | ``System.Double``   | First order vertical moment.           |
+-------------+---------------------+----------------------------------------+
| ``m20``     | ``System.Double``   | Second order horizontal moment.        |
+-------------+---------------------+----------------------------------------+
| ``m11``     | ``System.Double``   | Second order mixed moment.             |
+-------------+---------------------+----------------------------------------+
| ``m02``     | ``System.Double``   | Second order vertical moment.          |
+-------------+---------------------+----------------------------------------+
| ``m30``     | ``System.Double``   | Third order horizontal moment.         |
+-------------+---------------------+----------------------------------------+
| ``m21``     | ``System.Double``   | Third order horizontal mixed moment.   |
+-------------+---------------------+----------------------------------------+
| ``m12``     | ``System.Double``   | Third order vertical mixed moment.     |
+-------------+---------------------+----------------------------------------+
| ``m03``     | ``System.Double``   | Third order vertical moment.           |
+-------------+---------------------+----------------------------------------+

Initializes the moments.

Properties
~~~~~~~~~~

Property *M00*
^^^^^^^^^^^^^^

``System.Double M00``

The zeroth order moment.

Property *M10*
^^^^^^^^^^^^^^

``System.Double M10``

The horizontal first order moment.

Property *M01*
^^^^^^^^^^^^^^

``System.Double M01``

The vertical first order moment.

Property *M20*
^^^^^^^^^^^^^^

``System.Double M20``

The horizontal second order moment.

Property *M11*
^^^^^^^^^^^^^^

``System.Double M11``

The mixed second order moment.

Property *M02*
^^^^^^^^^^^^^^

``System.Double M02``

The vertical second order moment.

Property *M30*
^^^^^^^^^^^^^^

``System.Double M30``

The horizontal third order moment.

Property *M21*
^^^^^^^^^^^^^^

``System.Double M21``

The first (horizontal) mixed third order moment.

Property *M12*
^^^^^^^^^^^^^^

``System.Double M12``

The second (vertical) mixed third order moment.

Property *M03*
^^^^^^^^^^^^^^

``System.Double M03``

The vertical third order moment.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
