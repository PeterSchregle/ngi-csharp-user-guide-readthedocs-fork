Class *DtOpenlayersModule*
--------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **DtOpenlayersModule** contains the following properties:

+--------------------------+-------+-------+---------------+
| Property                 | Get   | Set   | Description   |
+==========================+=======+=======+===============+
| ``DeviceName``           | \*    |       |               |
+--------------------------+-------+-------+---------------+
| ``BusType``              | \*    |       |               |
+--------------------------+-------+-------+---------------+
| ``SerialNumber``         | \*    |       |               |
+--------------------------+-------+-------+---------------+
| ``DigitalInputCount``    | \*    |       |               |
+--------------------------+-------+-------+---------------+
| ``DigitalOutputCount``   | \*    |       |               |
+--------------------------+-------+-------+---------------+
| ``DigitalInputBits``     | \*    |       |               |
+--------------------------+-------+-------+---------------+
| ``DigitalOutputBits``    |       | \*    |               |
+--------------------------+-------+-------+---------------+

The class **DtOpenlayersModule** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``ToString``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Constructors
~~~~~~~~~~~~

Constructor *DtOpenlayersModule*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``DtOpenlayersModule(System.String deviceName)``

The constructor has the following parameters:

+------------------+---------------------+---------------+
| Parameter        | Type                | Description   |
+==================+=====================+===============+
| ``deviceName``   | ``System.String``   |               |
+------------------+---------------------+---------------+

Properties
~~~~~~~~~~

Property *DeviceName*
^^^^^^^^^^^^^^^^^^^^^

``System.String DeviceName``

Property *BusType*
^^^^^^^^^^^^^^^^^^

``System.String BusType``

Property *SerialNumber*
^^^^^^^^^^^^^^^^^^^^^^^

``System.UInt32 SerialNumber``

Property *DigitalInputCount*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.UInt32 DigitalInputCount``

Property *DigitalOutputCount*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.UInt32 DigitalOutputCount``

Property *DigitalInputBits*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.UInt64 DigitalInputBits``

Property *DigitalOutputBits*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.UInt64 DigitalOutputBits``

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
