Class *ImageFileformatInfo*
---------------------------

Information about an image fileformat.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **ImageFileformatInfo** contains the following properties:

+-------------------+-------+-------+------------------------------------------------------+
| Property          | Get   | Set   | Description                                          |
+===================+=======+=======+======================================================+
| ``Description``   | \*    | \*    | The description of the image fileformat.             |
+-------------------+-------+-------+------------------------------------------------------+
| ``CanRead``       | \*    | \*    | Is reading supported?                                |
+-------------------+-------+-------+------------------------------------------------------+
| ``CanWrite``      | \*    | \*    | Is writing supported?                                |
+-------------------+-------+-------+------------------------------------------------------+
| ``IsCommon``      | \*    | \*    | Is this a common format?                             |
+-------------------+-------+-------+------------------------------------------------------+
| ``IsRaw``         | \*    | \*    | Is this a raw format?                                |
+-------------------+-------+-------+------------------------------------------------------+
| ``Extensions``    | \*    | \*    | A list of the file extensions for this fileformat.   |
+-------------------+-------+-------+------------------------------------------------------+

The class **ImageFileformatInfo** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

The following operators are implemented for a image\_fileformat\_info: operator == : comparison for equality. operator != : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *ImageFileformatInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageFileformatInfo()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *ImageFileformatInfo*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageFileformatInfo(System.String description, System.Boolean canRead, System.Boolean canWrite, System.Boolean isCommon, System.Boolean isRaw, StringList extensions)``

Constructor.

The constructor has the following parameters:

+-------------------+----------------------+------------------------------------------------------+
| Parameter         | Type                 | Description                                          |
+===================+======================+======================================================+
| ``description``   | ``System.String``    | The description of the image fileformat.             |
+-------------------+----------------------+------------------------------------------------------+
| ``canRead``       | ``System.Boolean``   | Is reading supported?                                |
+-------------------+----------------------+------------------------------------------------------+
| ``canWrite``      | ``System.Boolean``   | Is writing supported?                                |
+-------------------+----------------------+------------------------------------------------------+
| ``isCommon``      | ``System.Boolean``   | Is this a common format?                             |
+-------------------+----------------------+------------------------------------------------------+
| ``isRaw``         | ``System.Boolean``   | Is this a raw format?                                |
+-------------------+----------------------+------------------------------------------------------+
| ``extensions``    | ``StringList``       | A list of the file extensions for this fileformat.   |
+-------------------+----------------------+------------------------------------------------------+

Properties
~~~~~~~~~~

Property *Description*
^^^^^^^^^^^^^^^^^^^^^^

``System.String Description``

The description of the image fileformat.

Property *CanRead*
^^^^^^^^^^^^^^^^^^

``System.Boolean CanRead``

Is reading supported?

Property *CanWrite*
^^^^^^^^^^^^^^^^^^^

``System.Boolean CanWrite``

Is writing supported?

Property *IsCommon*
^^^^^^^^^^^^^^^^^^^

``System.Boolean IsCommon``

Is this a common format?

Property *IsRaw*
^^^^^^^^^^^^^^^^

``System.Boolean IsRaw``

Is this a raw format?

Property *Extensions*
^^^^^^^^^^^^^^^^^^^^^

``StringList Extensions``

A list of the file extensions for this fileformat.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
