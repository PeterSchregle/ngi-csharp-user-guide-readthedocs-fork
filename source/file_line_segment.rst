Class *LineSegment*
-------------------

A line\_segment in two-dimensional euclidean space.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **LineSegment** implements the following interfaces:

+---------------------------------------------------------+
| Interface                                               |
+=========================================================+
| ``IEquatableLineSegmentLineSegmentCoordinateVariant``   |
+---------------------------------------------------------+
| ``ISerializable``                                       |
+---------------------------------------------------------+
| ``INotifyPropertyChanged``                              |
+---------------------------------------------------------+

The class **LineSegment** contains the following variant parameters:

+------------------+-----------------------------------------+
| Variant          | Description                             |
+==================+=========================================+
| ``Coordinate``   | TODO no brief description for variant   |
+------------------+-----------------------------------------+

The class **LineSegment** contains the following properties:

+----------------------+-------+-------+-------------------------------------------------------------------+
| Property             | Get   | Set   | Description                                                       |
+======================+=======+=======+===================================================================+
| ``Origin``           | \*    | \*    | The starting point of the line segment.                           |
+----------------------+-------+-------+-------------------------------------------------------------------+
| ``From``             | \*    |       | Another name for the origin.                                      |
+----------------------+-------+-------+-------------------------------------------------------------------+
| ``Extent``           | \*    | \*    | The extent of the line segment.                                   |
+----------------------+-------+-------+-------------------------------------------------------------------+
| ``Direction``        | \*    | \*    | The direction of the line segment.                                |
+----------------------+-------+-------+-------------------------------------------------------------------+
| ``Dir``              | \*    |       | The direction of the line segment (for backward compatibility).   |
+----------------------+-------+-------+-------------------------------------------------------------------+
| ``Normal``           | \*    |       | The normal direction of the line segment.                         |
+----------------------+-------+-------+-------------------------------------------------------------------+
| ``StartDirection``   | \*    |       | Get the start direction.                                          |
+----------------------+-------+-------+-------------------------------------------------------------------+
| ``StopDirection``    | \*    |       | Get the stop direction.                                           |
+----------------------+-------+-------+-------------------------------------------------------------------+
| ``Endpoint``         | \*    |       | Get the endpoint.                                                 |
+----------------------+-------+-------+-------------------------------------------------------------------+
| ``To``               | \*    |       | Another name for the endpoint.                                    |
+----------------------+-------+-------+-------------------------------------------------------------------+
| ``Center``           | \*    |       | Get the center.                                                   |
+----------------------+-------+-------+-------------------------------------------------------------------+
| ``Width``            | \*    |       | Get the width.                                                    |
+----------------------+-------+-------+-------------------------------------------------------------------+
| ``Height``           | \*    |       | Get the height.                                                   |
+----------------------+-------+-------+-------------------------------------------------------------------+
| ``BoundingBox``      | \*    |       | Get the bounding box of the line segment.                         |
+----------------------+-------+-------+-------------------------------------------------------------------+

The class **LineSegment** contains the following methods:

+---------------------------+-----------------------------------------------------------------------+
| Method                    | Description                                                           |
+===========================+=======================================================================+
| ``GetPointOrientation``   | Returns the orientation of the point relative to this line segment.   |
+---------------------------+-----------------------------------------------------------------------+
| ``Move``                  | Move line segment.                                                    |
+---------------------------+-----------------------------------------------------------------------+
| ``ToString``              | Provide string representation for debugging purposes.                 |
+---------------------------+-----------------------------------------------------------------------+

Description
~~~~~~~~~~~

The line\_segment starts at it's origin p and extends in it's positive direction to its finite endpoint at p+d\*e.

A line\_segment can be seen as a point combined with a direction and an extent in two-dimensional space.

The following operations are implemented: operator== : comparison for equality. operator != : comparison for inequality. operator- : negation, directly implemented. operator+ : addition, implemented through boost::additive operator-= : subtraction, directly implemented. operator- : subtraction, implemented through boost::additive

Variants
~~~~~~~~

Variant *Coordinate*
^^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Coordinate** has the following types:

+--------------+
| Type         |
+==============+
| ``Int32``    |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *LineSegment*
^^^^^^^^^^^^^^^^^^^^^^^^^

``LineSegment()``

Construct a line\_segment from a point, direction vector and extent.

If no parameters are passed, line\_segment from the origin to the point (1,0) is constructed.

Constructors
~~~~~~~~~~~~

Constructor *LineSegment*
^^^^^^^^^^^^^^^^^^^^^^^^^

``LineSegment(Point origin, Direction direction, System.Double extent)``

Construct a line\_segment from a point, direction vector and extent of a different type.

The constructor has the following parameters:

+-----------------+---------------------+------------------+
| Parameter       | Type                | Description      |
+=================+=====================+==================+
| ``origin``      | ``Point``           |                  |
+-----------------+---------------------+------------------+
| ``direction``   | ``Direction``       | The direction.   |
+-----------------+---------------------+------------------+
| ``extent``      | ``System.Double``   | The extent.      |
+-----------------+---------------------+------------------+

Constructor *LineSegment*
^^^^^^^^^^^^^^^^^^^^^^^^^

``LineSegment(Point from, Point to)``

Construct a line\_segment from two points.

The constructor has the following parameters:

+-------------+-------------+--------------------+
| Parameter   | Type        | Description        |
+=============+=============+====================+
| ``from``    | ``Point``   | The point P.       |
+-------------+-------------+--------------------+
| ``to``      | ``Point``   | The other point.   |
+-------------+-------------+--------------------+

Properties
~~~~~~~~~~

Property *Origin*
^^^^^^^^^^^^^^^^^

``Point Origin``

The starting point of the line segment.

Property *From*
^^^^^^^^^^^^^^^

``Point From``

Another name for the origin.

Property *Extent*
^^^^^^^^^^^^^^^^^

``System.Double Extent``

The extent of the line segment.

Property *Direction*
^^^^^^^^^^^^^^^^^^^^

``Direction Direction``

The direction of the line segment.

Property *Dir*
^^^^^^^^^^^^^^

``Direction Dir``

The direction of the line segment (for backward compatibility).

Property *Normal*
^^^^^^^^^^^^^^^^^

``Direction Normal``

The normal direction of the line segment.

Property *StartDirection*
^^^^^^^^^^^^^^^^^^^^^^^^^

``Direction StartDirection``

Get the start direction.

The start direction is opposite to the direction.

Property *StopDirection*
^^^^^^^^^^^^^^^^^^^^^^^^

``Direction StopDirection``

Get the stop direction.

The stop direction is the same as the direction.

Property *Endpoint*
^^^^^^^^^^^^^^^^^^^

``Point Endpoint``

Get the endpoint.

The endpoint sits at the side opposite to the origin.

Property *To*
^^^^^^^^^^^^^

``Point To``

Another name for the endpoint.

Property *Center*
^^^^^^^^^^^^^^^^^

``Point Center``

Get the center.

This function calculates the center of the line segment. The center is at p+d\*e/2.

Property *Width*
^^^^^^^^^^^^^^^^

``System.Object Width``

Get the width.

Property *Height*
^^^^^^^^^^^^^^^^^

``System.Object Height``

Get the height.

Property *BoundingBox*
^^^^^^^^^^^^^^^^^^^^^^

``BoxDouble BoundingBox``

Get the bounding box of the line segment.

The bounding box is an axis aligned box that bounds the line segment. It can optionally take a line thickness into account.

The bounding box of the line segment.

Methods
~~~~~~~

Method *GetPointOrientation*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double GetPointOrientation(Point pt)``

Returns the orientation of the point relative to this line segment.

The method **GetPointOrientation** has the following parameters:

+-------------+-------------+---------------+
| Parameter   | Type        | Description   |
+=============+=============+===============+
| ``pt``      | ``Point``   |               |
+-------------+-------------+---------------+

Returns negative values if the point is to the left, positive values if the point is to the right and zero if the point is on the line segment or the line defined by the line segment. Left and right are used in the sense as defined in a right handed coordinate system; in reality nGI uses a left handed coordinate system and therefore left and right are reversed.

Method *Move*
^^^^^^^^^^^^^

``LineSegment Move(Vector movement)``

Move line segment.

The method **Move** has the following parameters:

+----------------+--------------+--------------------+
| Parameter      | Type         | Description        |
+================+==============+====================+
| ``movement``   | ``Vector``   | Movement vector.   |
+----------------+--------------+--------------------+

Moves a line segment by a vector.

The moved line segment.

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO documentation missing

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO documentation missing
