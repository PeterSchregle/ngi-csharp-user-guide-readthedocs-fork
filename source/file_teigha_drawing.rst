Class *TeighaDrawing*
---------------------

A class holding a drawing parsed from a DXF/DWG file.

**Namespace:** Ngi

**Module:** CadPdf

The class **TeighaDrawing** implements the following interfaces:

+-------------------------------+
| Interface                     |
+===============================+
| ``IEquatableTeighaDrawing``   |
+-------------------------------+

The class **TeighaDrawing** contains the following properties:

+----------------+-------+-------+----------------------------------------+
| Property       | Get   | Set   | Description                            |
+================+=======+=======+========================================+
| ``Bounds``     | \*    | \*    | The outer dimensions of the drawing.   |
+----------------+-------+-------+----------------------------------------+
| ``Contours``   | \*    | \*    | The list of contours.                  |
+----------------+-------+-------+----------------------------------------+

The class **TeighaDrawing** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``Render``     | Render a part of the drawing into an image.             |
+----------------+---------------------------------------------------------+
| ``Render``     | Render the whole drawing into an image.                 |
+----------------+---------------------------------------------------------+
| ``Render``     | Render a part of the drawing into an image.             |
+----------------+---------------------------------------------------------+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

A drawing consists of a box specifying its dimensions, and as a list of contours.

This has been superseded by teigha\_parser2 and teigha\_drawing2. This version is kept for compatibility reasons.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *TeighaDrawing*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TeighaDrawing()``

Constructs an empty teigha\_drawing.

Properties
~~~~~~~~~~

Property *Bounds*
^^^^^^^^^^^^^^^^^

``BoxDouble Bounds``

The outer dimensions of the drawing.

Property *Contours*
^^^^^^^^^^^^^^^^^^^

``CadContourList Contours``

The list of contours.

Methods
~~~~~~~

Method *Render*
^^^^^^^^^^^^^^^

``ImageByte Render(BoxDouble part, System.Double opticalResolution, System.Boolean filled)``

Render a part of the drawing into an image.

The method **Render** has the following parameters:

+--------------+------------+----------------------------------------------------+
| Parameter    | Type       | Description                                        |
+==============+============+====================================================+
| ``part``     | ``BoxDoubl | The part of the image that should be rendered.     |
|              | e``        |                                                    |
+--------------+------------+----------------------------------------------------+
| ``opticalRes | ``System.D | The optical resolution of the rendering.This is    |
| olution``    | ouble``    | the size of a pixel in mm. The inverse is the      |
|              |            | number of pixels per mm, which does not need to be |
|              |            | an integral number.                                |
+--------------+------------+----------------------------------------------------+
| ``filled``   | ``System.B | Draw the contours filled (true) or outlined        |
|              | oolean``   | (false).                                           |
+--------------+------------+----------------------------------------------------+

The part of the image that should be rendered is given by a box. The box coordinates are in the coordinate system of the CAD drawing.

The coordinate system of the CAD drawing is a right-handed coordinate system, where the positive x-axis extends to the right and the positive y-axis extends to the top.

The coordinate system in an image is a left-handed coordinate system, where the positive x-axis extends to the right and the positive y-axis extends to the bottom.

The function calculates an internal coordinate transformation so that the rendering in the image is in the same orientation as in the CAD drawing (its y coordinates have been flipped upside down).

Method *Render*
^^^^^^^^^^^^^^^

``ImageByte Render(System.Double opticalResolution, System.Boolean filled)``

Render the whole drawing into an image.

The method **Render** has the following parameters:

+--------------+------------+----------------------------------------------------+
| Parameter    | Type       | Description                                        |
+==============+============+====================================================+
| ``opticalRes | ``System.D | The optical resolution of the rendering.This is    |
| olution``    | ouble``    | the size of a pixel in mm. The inverse is the      |
|              |            | number of pixels per mm, which does not need to be |
|              |            | an integral number.                                |
+--------------+------------+----------------------------------------------------+
| ``filled``   | ``System.B | Draw the contours filled (true) or outlined        |
|              | oolean``   | (false).                                           |
+--------------+------------+----------------------------------------------------+

The coordinate system of the CAD drawing is a right-handed coordinate system, where the positive x-axis extends to the right and the positive y-axis extends to the top.

The coordinate system in an image is a left-handed coordinate system, where the positive x-axis extends to the right and the positive y-axis extends to the bottom.

The function calculates an internal coordinate transformation so that the rendering in the image is in the same orientation as in the CAD drawing (its y coordinates have been flipped upside down).

Method *Render*
^^^^^^^^^^^^^^^

``ImageByte Render(AffineMatrix transformation, VectorInt32 size, System.Boolean filled)``

Render a part of the drawing into an image.

The method **Render** has the following parameters:

+----------------------+----------------------+--------------------------------------------------------+
| Parameter            | Type                 | Description                                            |
+======================+======================+========================================================+
| ``transformation``   | ``AffineMatrix``     | The transformation matrix.                             |
+----------------------+----------------------+--------------------------------------------------------+
| ``size``             | ``VectorInt32``      | The size of the image that should be rendered.         |
+----------------------+----------------------+--------------------------------------------------------+
| ``filled``           | ``System.Boolean``   | Draw the contours filled (true) or outlined (false).   |
+----------------------+----------------------+--------------------------------------------------------+

The part of the drawing to be rendered is given by a transformation matrix and a size.

The coordinate system of the CAD drawing is a right-handed coordinate system, where the positive x-axis extends to the right and the positive y-axis extends to the top.

The coordinate system in an image is a left-handed coordinate system, where the positive x-axis extends to the right and the positive y-axis extends to the bottom.

If you want to make sure that the image is rendered in the same orientation as the in the CAD drawing, you have to set up the transformation in a way that it does the coordinate flipping along the y-axis.

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
