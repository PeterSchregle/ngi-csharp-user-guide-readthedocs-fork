Class *SubsampleAlgorithms*
---------------------------

Image subsampling functions.

**Namespace:** Ngi

**Module:** ImageProcessing

Description
~~~~~~~~~~~

The class contains functions for image subsampling.

Static Methods
~~~~~~~~~~~~~~

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageByte Subsample(ViewLocatorByte source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+-----------------------+---------------+
| Parameter     | Type                  | Description   |
+===============+=======================+===============+
| ``source``    | ``ViewLocatorByte``   |               |
+---------------+-----------------------+---------------+
| ``xFactor``   | ``System.Int32``      |               |
+---------------+-----------------------+---------------+
| ``yFactor``   | ``System.Int32``      |               |
+---------------+-----------------------+---------------+
| ``zFactor``   | ``System.Int32``      |               |
+---------------+-----------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageUInt16 Subsample(ViewLocatorUInt16 source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+-------------------------+---------------+
| Parameter     | Type                    | Description   |
+===============+=========================+===============+
| ``source``    | ``ViewLocatorUInt16``   |               |
+---------------+-------------------------+---------------+
| ``xFactor``   | ``System.Int32``        |               |
+---------------+-------------------------+---------------+
| ``yFactor``   | ``System.Int32``        |               |
+---------------+-------------------------+---------------+
| ``zFactor``   | ``System.Int32``        |               |
+---------------+-------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageUInt32 Subsample(ViewLocatorUInt32 source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+-------------------------+---------------+
| Parameter     | Type                    | Description   |
+===============+=========================+===============+
| ``source``    | ``ViewLocatorUInt32``   |               |
+---------------+-------------------------+---------------+
| ``xFactor``   | ``System.Int32``        |               |
+---------------+-------------------------+---------------+
| ``yFactor``   | ``System.Int32``        |               |
+---------------+-------------------------+---------------+
| ``zFactor``   | ``System.Int32``        |               |
+---------------+-------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageDouble Subsample(ViewLocatorDouble source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+-------------------------+---------------+
| Parameter     | Type                    | Description   |
+===============+=========================+===============+
| ``source``    | ``ViewLocatorDouble``   |               |
+---------------+-------------------------+---------------+
| ``xFactor``   | ``System.Int32``        |               |
+---------------+-------------------------+---------------+
| ``yFactor``   | ``System.Int32``        |               |
+---------------+-------------------------+---------------+
| ``zFactor``   | ``System.Int32``        |               |
+---------------+-------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageRgbByte Subsample(ViewLocatorRgbByte source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+--------------------------+---------------+
| Parameter     | Type                     | Description   |
+===============+==========================+===============+
| ``source``    | ``ViewLocatorRgbByte``   |               |
+---------------+--------------------------+---------------+
| ``xFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+
| ``yFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+
| ``zFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 Subsample(ViewLocatorRgbUInt16 source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``source``    | ``ViewLocatorRgbUInt16``   |               |
+---------------+----------------------------+---------------+
| ``xFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``yFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``zFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 Subsample(ViewLocatorRgbUInt32 source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``source``    | ``ViewLocatorRgbUInt32``   |               |
+---------------+----------------------------+---------------+
| ``xFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``yFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``zFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageRgbDouble Subsample(ViewLocatorRgbDouble source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``source``    | ``ViewLocatorRgbDouble``   |               |
+---------------+----------------------------+---------------+
| ``xFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``yFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``zFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageRgbaByte Subsample(ViewLocatorRgbaByte source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+---------------------------+---------------+
| Parameter     | Type                      | Description   |
+===============+===========================+===============+
| ``source``    | ``ViewLocatorRgbaByte``   |               |
+---------------+---------------------------+---------------+
| ``xFactor``   | ``System.Int32``          |               |
+---------------+---------------------------+---------------+
| ``yFactor``   | ``System.Int32``          |               |
+---------------+---------------------------+---------------+
| ``zFactor``   | ``System.Int32``          |               |
+---------------+---------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Subsample(ViewLocatorRgbaUInt16 source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+-----------------------------+---------------+
| Parameter     | Type                        | Description   |
+===============+=============================+===============+
| ``source``    | ``ViewLocatorRgbaUInt16``   |               |
+---------------+-----------------------------+---------------+
| ``xFactor``   | ``System.Int32``            |               |
+---------------+-----------------------------+---------------+
| ``yFactor``   | ``System.Int32``            |               |
+---------------+-----------------------------+---------------+
| ``zFactor``   | ``System.Int32``            |               |
+---------------+-----------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Subsample(ViewLocatorRgbaUInt32 source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+-----------------------------+---------------+
| Parameter     | Type                        | Description   |
+===============+=============================+===============+
| ``source``    | ``ViewLocatorRgbaUInt32``   |               |
+---------------+-----------------------------+---------------+
| ``xFactor``   | ``System.Int32``            |               |
+---------------+-----------------------------+---------------+
| ``yFactor``   | ``System.Int32``            |               |
+---------------+-----------------------------+---------------+
| ``zFactor``   | ``System.Int32``            |               |
+---------------+-----------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble Subsample(ViewLocatorRgbaDouble source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+-----------------------------+---------------+
| Parameter     | Type                        | Description   |
+===============+=============================+===============+
| ``source``    | ``ViewLocatorRgbaDouble``   |               |
+---------------+-----------------------------+---------------+
| ``xFactor``   | ``System.Int32``            |               |
+---------------+-----------------------------+---------------+
| ``yFactor``   | ``System.Int32``            |               |
+---------------+-----------------------------+---------------+
| ``zFactor``   | ``System.Int32``            |               |
+---------------+-----------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageHlsByte Subsample(ViewLocatorHlsByte source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+--------------------------+---------------+
| Parameter     | Type                     | Description   |
+===============+==========================+===============+
| ``source``    | ``ViewLocatorHlsByte``   |               |
+---------------+--------------------------+---------------+
| ``xFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+
| ``yFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+
| ``zFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 Subsample(ViewLocatorHlsUInt16 source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``source``    | ``ViewLocatorHlsUInt16``   |               |
+---------------+----------------------------+---------------+
| ``xFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``yFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``zFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageHlsDouble Subsample(ViewLocatorHlsDouble source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``source``    | ``ViewLocatorHlsDouble``   |               |
+---------------+----------------------------+---------------+
| ``xFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``yFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``zFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageHsiByte Subsample(ViewLocatorHsiByte source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+--------------------------+---------------+
| Parameter     | Type                     | Description   |
+===============+==========================+===============+
| ``source``    | ``ViewLocatorHsiByte``   |               |
+---------------+--------------------------+---------------+
| ``xFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+
| ``yFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+
| ``zFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 Subsample(ViewLocatorHsiUInt16 source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``source``    | ``ViewLocatorHsiUInt16``   |               |
+---------------+----------------------------+---------------+
| ``xFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``yFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``zFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageHsiDouble Subsample(ViewLocatorHsiDouble source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``source``    | ``ViewLocatorHsiDouble``   |               |
+---------------+----------------------------+---------------+
| ``xFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``yFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``zFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageLabByte Subsample(ViewLocatorLabByte source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+--------------------------+---------------+
| Parameter     | Type                     | Description   |
+===============+==========================+===============+
| ``source``    | ``ViewLocatorLabByte``   |               |
+---------------+--------------------------+---------------+
| ``xFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+
| ``yFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+
| ``zFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 Subsample(ViewLocatorLabUInt16 source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``source``    | ``ViewLocatorLabUInt16``   |               |
+---------------+----------------------------+---------------+
| ``xFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``yFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``zFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageLabDouble Subsample(ViewLocatorLabDouble source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``source``    | ``ViewLocatorLabDouble``   |               |
+---------------+----------------------------+---------------+
| ``xFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``yFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``zFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageXyzByte Subsample(ViewLocatorXyzByte source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+--------------------------+---------------+
| Parameter     | Type                     | Description   |
+===============+==========================+===============+
| ``source``    | ``ViewLocatorXyzByte``   |               |
+---------------+--------------------------+---------------+
| ``xFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+
| ``yFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+
| ``zFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 Subsample(ViewLocatorXyzUInt16 source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``source``    | ``ViewLocatorXyzUInt16``   |               |
+---------------+----------------------------+---------------+
| ``xFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``yFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``zFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageXyzDouble Subsample(ViewLocatorXyzDouble source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``source``    | ``ViewLocatorXyzDouble``   |               |
+---------------+----------------------------+---------------+
| ``xFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``yFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``zFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``Image Subsample(View source, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+--------------------+---------------+
| Parameter     | Type               | Description   |
+===============+====================+===============+
| ``source``    | ``View``           |               |
+---------------+--------------------+---------------+
| ``xFactor``   | ``System.Int32``   |               |
+---------------+--------------------+---------------+
| ``yFactor``   | ``System.Int32``   |               |
+---------------+--------------------+---------------+
| ``zFactor``   | ``System.Int32``   |               |
+---------------+--------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageByte Subsample(ViewLocatorByte source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+-----------------------+---------------+
| Parameter     | Type                  | Description   |
+===============+=======================+===============+
| ``source``    | ``ViewLocatorByte``   |               |
+---------------+-----------------------+---------------+
| ``region``    | ``Region``            |               |
+---------------+-----------------------+---------------+
| ``xFactor``   | ``System.Int32``      |               |
+---------------+-----------------------+---------------+
| ``yFactor``   | ``System.Int32``      |               |
+---------------+-----------------------+---------------+
| ``zFactor``   | ``System.Int32``      |               |
+---------------+-----------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageUInt16 Subsample(ViewLocatorUInt16 source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+-------------------------+---------------+
| Parameter     | Type                    | Description   |
+===============+=========================+===============+
| ``source``    | ``ViewLocatorUInt16``   |               |
+---------------+-------------------------+---------------+
| ``region``    | ``Region``              |               |
+---------------+-------------------------+---------------+
| ``xFactor``   | ``System.Int32``        |               |
+---------------+-------------------------+---------------+
| ``yFactor``   | ``System.Int32``        |               |
+---------------+-------------------------+---------------+
| ``zFactor``   | ``System.Int32``        |               |
+---------------+-------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageUInt32 Subsample(ViewLocatorUInt32 source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+-------------------------+---------------+
| Parameter     | Type                    | Description   |
+===============+=========================+===============+
| ``source``    | ``ViewLocatorUInt32``   |               |
+---------------+-------------------------+---------------+
| ``region``    | ``Region``              |               |
+---------------+-------------------------+---------------+
| ``xFactor``   | ``System.Int32``        |               |
+---------------+-------------------------+---------------+
| ``yFactor``   | ``System.Int32``        |               |
+---------------+-------------------------+---------------+
| ``zFactor``   | ``System.Int32``        |               |
+---------------+-------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageDouble Subsample(ViewLocatorDouble source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+-------------------------+---------------+
| Parameter     | Type                    | Description   |
+===============+=========================+===============+
| ``source``    | ``ViewLocatorDouble``   |               |
+---------------+-------------------------+---------------+
| ``region``    | ``Region``              |               |
+---------------+-------------------------+---------------+
| ``xFactor``   | ``System.Int32``        |               |
+---------------+-------------------------+---------------+
| ``yFactor``   | ``System.Int32``        |               |
+---------------+-------------------------+---------------+
| ``zFactor``   | ``System.Int32``        |               |
+---------------+-------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageRgbByte Subsample(ViewLocatorRgbByte source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+--------------------------+---------------+
| Parameter     | Type                     | Description   |
+===============+==========================+===============+
| ``source``    | ``ViewLocatorRgbByte``   |               |
+---------------+--------------------------+---------------+
| ``region``    | ``Region``               |               |
+---------------+--------------------------+---------------+
| ``xFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+
| ``yFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+
| ``zFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 Subsample(ViewLocatorRgbUInt16 source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``source``    | ``ViewLocatorRgbUInt16``   |               |
+---------------+----------------------------+---------------+
| ``region``    | ``Region``                 |               |
+---------------+----------------------------+---------------+
| ``xFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``yFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``zFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 Subsample(ViewLocatorRgbUInt32 source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``source``    | ``ViewLocatorRgbUInt32``   |               |
+---------------+----------------------------+---------------+
| ``region``    | ``Region``                 |               |
+---------------+----------------------------+---------------+
| ``xFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``yFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``zFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageRgbDouble Subsample(ViewLocatorRgbDouble source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``source``    | ``ViewLocatorRgbDouble``   |               |
+---------------+----------------------------+---------------+
| ``region``    | ``Region``                 |               |
+---------------+----------------------------+---------------+
| ``xFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``yFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``zFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageRgbaByte Subsample(ViewLocatorRgbaByte source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+---------------------------+---------------+
| Parameter     | Type                      | Description   |
+===============+===========================+===============+
| ``source``    | ``ViewLocatorRgbaByte``   |               |
+---------------+---------------------------+---------------+
| ``region``    | ``Region``                |               |
+---------------+---------------------------+---------------+
| ``xFactor``   | ``System.Int32``          |               |
+---------------+---------------------------+---------------+
| ``yFactor``   | ``System.Int32``          |               |
+---------------+---------------------------+---------------+
| ``zFactor``   | ``System.Int32``          |               |
+---------------+---------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt16 Subsample(ViewLocatorRgbaUInt16 source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+-----------------------------+---------------+
| Parameter     | Type                        | Description   |
+===============+=============================+===============+
| ``source``    | ``ViewLocatorRgbaUInt16``   |               |
+---------------+-----------------------------+---------------+
| ``region``    | ``Region``                  |               |
+---------------+-----------------------------+---------------+
| ``xFactor``   | ``System.Int32``            |               |
+---------------+-----------------------------+---------------+
| ``yFactor``   | ``System.Int32``            |               |
+---------------+-----------------------------+---------------+
| ``zFactor``   | ``System.Int32``            |               |
+---------------+-----------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageRgbaUInt32 Subsample(ViewLocatorRgbaUInt32 source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+-----------------------------+---------------+
| Parameter     | Type                        | Description   |
+===============+=============================+===============+
| ``source``    | ``ViewLocatorRgbaUInt32``   |               |
+---------------+-----------------------------+---------------+
| ``region``    | ``Region``                  |               |
+---------------+-----------------------------+---------------+
| ``xFactor``   | ``System.Int32``            |               |
+---------------+-----------------------------+---------------+
| ``yFactor``   | ``System.Int32``            |               |
+---------------+-----------------------------+---------------+
| ``zFactor``   | ``System.Int32``            |               |
+---------------+-----------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageRgbaDouble Subsample(ViewLocatorRgbaDouble source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+-----------------------------+---------------+
| Parameter     | Type                        | Description   |
+===============+=============================+===============+
| ``source``    | ``ViewLocatorRgbaDouble``   |               |
+---------------+-----------------------------+---------------+
| ``region``    | ``Region``                  |               |
+---------------+-----------------------------+---------------+
| ``xFactor``   | ``System.Int32``            |               |
+---------------+-----------------------------+---------------+
| ``yFactor``   | ``System.Int32``            |               |
+---------------+-----------------------------+---------------+
| ``zFactor``   | ``System.Int32``            |               |
+---------------+-----------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageHlsByte Subsample(ViewLocatorHlsByte source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+--------------------------+---------------+
| Parameter     | Type                     | Description   |
+===============+==========================+===============+
| ``source``    | ``ViewLocatorHlsByte``   |               |
+---------------+--------------------------+---------------+
| ``region``    | ``Region``               |               |
+---------------+--------------------------+---------------+
| ``xFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+
| ``yFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+
| ``zFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageHlsUInt16 Subsample(ViewLocatorHlsUInt16 source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``source``    | ``ViewLocatorHlsUInt16``   |               |
+---------------+----------------------------+---------------+
| ``region``    | ``Region``                 |               |
+---------------+----------------------------+---------------+
| ``xFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``yFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``zFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageHlsDouble Subsample(ViewLocatorHlsDouble source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``source``    | ``ViewLocatorHlsDouble``   |               |
+---------------+----------------------------+---------------+
| ``region``    | ``Region``                 |               |
+---------------+----------------------------+---------------+
| ``xFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``yFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``zFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageHsiByte Subsample(ViewLocatorHsiByte source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+--------------------------+---------------+
| Parameter     | Type                     | Description   |
+===============+==========================+===============+
| ``source``    | ``ViewLocatorHsiByte``   |               |
+---------------+--------------------------+---------------+
| ``region``    | ``Region``               |               |
+---------------+--------------------------+---------------+
| ``xFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+
| ``yFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+
| ``zFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageHsiUInt16 Subsample(ViewLocatorHsiUInt16 source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``source``    | ``ViewLocatorHsiUInt16``   |               |
+---------------+----------------------------+---------------+
| ``region``    | ``Region``                 |               |
+---------------+----------------------------+---------------+
| ``xFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``yFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``zFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageHsiDouble Subsample(ViewLocatorHsiDouble source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``source``    | ``ViewLocatorHsiDouble``   |               |
+---------------+----------------------------+---------------+
| ``region``    | ``Region``                 |               |
+---------------+----------------------------+---------------+
| ``xFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``yFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``zFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageLabByte Subsample(ViewLocatorLabByte source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+--------------------------+---------------+
| Parameter     | Type                     | Description   |
+===============+==========================+===============+
| ``source``    | ``ViewLocatorLabByte``   |               |
+---------------+--------------------------+---------------+
| ``region``    | ``Region``               |               |
+---------------+--------------------------+---------------+
| ``xFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+
| ``yFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+
| ``zFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageLabUInt16 Subsample(ViewLocatorLabUInt16 source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``source``    | ``ViewLocatorLabUInt16``   |               |
+---------------+----------------------------+---------------+
| ``region``    | ``Region``                 |               |
+---------------+----------------------------+---------------+
| ``xFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``yFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``zFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageLabDouble Subsample(ViewLocatorLabDouble source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``source``    | ``ViewLocatorLabDouble``   |               |
+---------------+----------------------------+---------------+
| ``region``    | ``Region``                 |               |
+---------------+----------------------------+---------------+
| ``xFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``yFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``zFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageXyzByte Subsample(ViewLocatorXyzByte source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+--------------------------+---------------+
| Parameter     | Type                     | Description   |
+===============+==========================+===============+
| ``source``    | ``ViewLocatorXyzByte``   |               |
+---------------+--------------------------+---------------+
| ``region``    | ``Region``               |               |
+---------------+--------------------------+---------------+
| ``xFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+
| ``yFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+
| ``zFactor``   | ``System.Int32``         |               |
+---------------+--------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageXyzUInt16 Subsample(ViewLocatorXyzUInt16 source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``source``    | ``ViewLocatorXyzUInt16``   |               |
+---------------+----------------------------+---------------+
| ``region``    | ``Region``                 |               |
+---------------+----------------------------+---------------+
| ``xFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``yFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``zFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``ImageXyzDouble Subsample(ViewLocatorXyzDouble source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+----------------------------+---------------+
| Parameter     | Type                       | Description   |
+===============+============================+===============+
| ``source``    | ``ViewLocatorXyzDouble``   |               |
+---------------+----------------------------+---------------+
| ``region``    | ``Region``                 |               |
+---------------+----------------------------+---------------+
| ``xFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``yFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+
| ``zFactor``   | ``System.Int32``           |               |
+---------------+----------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``Image Subsample(View source, Region region, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Copies a source view to the destination buffer, while subsampling the data in one or more dimensions.

The method **Subsample** has the following parameters:

+---------------+--------------------+---------------+
| Parameter     | Type               | Description   |
+===============+====================+===============+
| ``source``    | ``View``           |               |
+---------------+--------------------+---------------+
| ``region``    | ``Region``         |               |
+---------------+--------------------+---------------+
| ``xFactor``   | ``System.Int32``   |               |
+---------------+--------------------+---------------+
| ``yFactor``   | ``System.Int32``   |               |
+---------------+--------------------+---------------+
| ``zFactor``   | ``System.Int32``   |               |
+---------------+--------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different subsampling factors into account. For example, if the horizontal subsampling factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for subsampling control the subsampling in the three dimensions. The factors can be combined to subsample in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A subsampled image.
