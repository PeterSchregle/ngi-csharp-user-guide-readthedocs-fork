Class *MvizMatrixcodeDecoder*
-----------------------------

Configuration for the mViz based matrixcode decoder.

**Namespace:** Ngi

**Module:** BarcodeMatrixcode

The class **MvizMatrixcodeDecoder** implements the following interfaces:

+---------------------------------------+
| Interface                             |
+=======================================+
| ``IEquatableMvizMatrixcodeDecoder``   |
+---------------------------------------+
| ``ISerializable``                     |
+---------------------------------------+

The class **MvizMatrixcodeDecoder** contains the following properties:

+---------------------+-------+-------+-----------------------------------------------------------------------------------------------------------+
| Property            | Get   | Set   | Description                                                                                               |
+=====================+=======+=======+===========================================================================================================+
| ``DataMatrix``      | \*    | \*    | The Data Matrix decoder.                                                                                  |
+---------------------+-------+-------+-----------------------------------------------------------------------------------------------------------+
| ``QrCode``          | \*    | \*    | The QR Code\_ext decoder.                                                                                 |
+---------------------+-------+-------+-----------------------------------------------------------------------------------------------------------+
| ``Polarities``      | \*    | \*    | Selects the possible contrast of the marking.                                                             |
+---------------------+-------+-------+-----------------------------------------------------------------------------------------------------------+
| ``Views``           | \*    | \*    | Selects the possible orientation of the marking.                                                          |
+---------------------+-------+-------+-----------------------------------------------------------------------------------------------------------+
| ``Shapes``          | \*    | \*    | Selects the possible shapes of the marking.                                                               |
+---------------------+-------+-------+-----------------------------------------------------------------------------------------------------------+
| ``CheckQuality``    | \*    | \*    | Computes the quality indicators, when true.                                                               |
+---------------------+-------+-------+-----------------------------------------------------------------------------------------------------------+
| ``StartLevel``      | \*    | \*    | For large images, you can skip the first pyramid levels for faster execution; try small positive value.   |
+---------------------+-------+-------+-----------------------------------------------------------------------------------------------------------+
| ``AccurateEdges``   | \*    | \*    | When true, a final subpixel fit of the edges is performed for very accurate location.                     |
+---------------------+-------+-------+-----------------------------------------------------------------------------------------------------------+

The class **MvizMatrixcodeDecoder** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

Various matrixcode symbologies can be configured as well as modes of decoding.

The following operators are implemented for a barcode\_decoder: operator == : comparison for equality. operator != : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *MvizMatrixcodeDecoder*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MvizMatrixcodeDecoder()``

Default constructor.

By default, all supported symbologies are enabled..

Constructors
~~~~~~~~~~~~

Constructor *MvizMatrixcodeDecoder*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MvizMatrixcodeDecoder(System.Boolean dataMatrix, System.Boolean qrCode, MvizMatrixcode.PolarityType polarities, MvizMatrixcode.ViewType views, MvizMatrixcode.ShapeType shapes, System.Boolean checkQuality, System.Int32 startLevel, System.Boolean accurateEdges)``

Constructor.

The constructor has the following parameters:

+--------------+------------+----------------------------------------------------+
| Parameter    | Type       | Description                                        |
+==============+============+====================================================+
| ``dataMatrix | ``System.B | Data Matrix.                                       |
| ``           | oolean``   |                                                    |
+--------------+------------+----------------------------------------------------+
| ``qrCode``   | ``System.B | QR Code.                                           |
|              | oolean``   |                                                    |
+--------------+------------+----------------------------------------------------+
| ``polarities | ``MvizMatr | The possible contrast of the marking.              |
| ``           | ixcode.Pol |                                                    |
|              | arityType` |                                                    |
|              | `          |                                                    |
+--------------+------------+----------------------------------------------------+
| ``views``    | ``MvizMatr | The possible orientation of the marking.           |
|              | ixcode.Vie |                                                    |
|              | wType``    |                                                    |
+--------------+------------+----------------------------------------------------+
| ``shapes``   | ``MvizMatr | The possible shapes of the data matrix codes:      |
|              | ixcode.Sha | square, rectangular or both. This is not used for  |
|              | peType``   | QR codes.                                          |
+--------------+------------+----------------------------------------------------+
| ``checkQuali | ``System.B | Check the quiet zone. When true, a quiet zone as   |
| ty``         | oolean``   | large as the standard requires must be present;    |
|              |            | when set to false, it is advisable to disable      |
|              |            | other symbologies to avoid false matches.          |
+--------------+------------+----------------------------------------------------+
| ``startLevel | ``System.I | For large images, you can skip the first pyramid   |
| ``           | nt32``     | levels for faster execution; try small positive    |
|              |            | value.                                             |
+--------------+------------+----------------------------------------------------+
| ``accurateEd | ``System.B | When true, a final subpixel fit of the edges is    |
| ges``        | oolean``   | performed for very accurate location.              |
+--------------+------------+----------------------------------------------------+

Every supported symbology can be switched on or off by the respective boolen parameter. In addition, you can make the decoder work harder (and take longer) by setting the try\_harder parameter.

Properties
~~~~~~~~~~

Property *DataMatrix*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean DataMatrix``

The Data Matrix decoder.

Property *QrCode*
^^^^^^^^^^^^^^^^^

``System.Boolean QrCode``

The QR Code\_ext decoder.

Property *Polarities*
^^^^^^^^^^^^^^^^^^^^^

``MvizMatrixcode.PolarityType Polarities``

Selects the possible contrast of the marking.

Property *Views*
^^^^^^^^^^^^^^^^

``MvizMatrixcode.ViewType Views``

Selects the possible orientation of the marking.

Property *Shapes*
^^^^^^^^^^^^^^^^^

``MvizMatrixcode.ShapeType Shapes``

Selects the possible shapes of the marking.

Property *CheckQuality*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean CheckQuality``

Computes the quality indicators, when true.

Property *StartLevel*
^^^^^^^^^^^^^^^^^^^^^

``System.Int32 StartLevel``

For large images, you can skip the first pyramid levels for faster execution; try small positive value.

Property *AccurateEdges*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean AccurateEdges``

When true, a final subpixel fit of the edges is performed for very accurate location.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
