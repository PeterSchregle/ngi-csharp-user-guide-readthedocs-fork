Class *CameraBufferConverter*
-----------------------------

**Namespace:** Ngi

**Module:** Camera

Description
~~~~~~~~~~~

Static Methods
~~~~~~~~~~~~~~

Method *RgbFlipRg*
^^^^^^^^^^^^^^^^^^

``ImageRgbByte RgbFlipRg(ViewLocatorRgbByte source)``

Flips the R and G compnenets of an RGB image.

The method **RgbFlipRg** has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``source``   | ``ViewLocatorRgbByte``   |               |
+--------------+--------------------------+---------------+

Method *RgbFlipRg*
^^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 RgbFlipRg(ViewLocatorRgbUInt16 source)``

Flips the R and G compnenets of an RGB image.

The method **RgbFlipRg** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt16``   |               |
+--------------+----------------------------+---------------+

Method *RgbFlipRg*
^^^^^^^^^^^^^^^^^^

``ImageRgbUInt32 RgbFlipRg(ViewLocatorRgbUInt32 source)``

Flips the R and G compnenets of an RGB image.

The method **RgbFlipRg** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbUInt32``   |               |
+--------------+----------------------------+---------------+

Method *RgbFlipRg*
^^^^^^^^^^^^^^^^^^

``ImageRgbDouble RgbFlipRg(ViewLocatorRgbDouble source)``

Flips the R and G compnenets of an RGB image.

The method **RgbFlipRg** has the following parameters:

+--------------+----------------------------+---------------+
| Parameter    | Type                       | Description   |
+==============+============================+===============+
| ``source``   | ``ViewLocatorRgbDouble``   |               |
+--------------+----------------------------+---------------+

Method *RgbFlipRg*
^^^^^^^^^^^^^^^^^^

``Image RgbFlipRg(View source)``

Flips the R and G compnenets of an RGB image.

The method **RgbFlipRg** has the following parameters:

+--------------+------------+---------------+
| Parameter    | Type       | Description   |
+==============+============+===============+
| ``source``   | ``View``   |               |
+--------------+------------+---------------+

Method *Demosaic*
^^^^^^^^^^^^^^^^^

``ImageRgbByte Demosaic(ViewLocatorByte source, System.Int32 cfaOffset)``

De-bayer raw image data to produce an RGB image.

The method **Demosaic** has the following parameters:

+-----------------+-----------------------+---------------+
| Parameter       | Type                  | Description   |
+=================+=======================+===============+
| ``source``      | ``ViewLocatorByte``   |               |
+-----------------+-----------------------+---------------+
| ``cfaOffset``   | ``System.Int32``      |               |
+-----------------+-----------------------+---------------+

Method *Demosaic*
^^^^^^^^^^^^^^^^^

``ImageRgbUInt16 Demosaic(ViewLocatorUInt16 source, System.Int32 cfaOffset)``

De-bayer raw image data to produce an RGB image.

The method **Demosaic** has the following parameters:

+-----------------+-------------------------+---------------+
| Parameter       | Type                    | Description   |
+=================+=========================+===============+
| ``source``      | ``ViewLocatorUInt16``   |               |
+-----------------+-------------------------+---------------+
| ``cfaOffset``   | ``System.Int32``        |               |
+-----------------+-------------------------+---------------+

Method *Demosaic*
^^^^^^^^^^^^^^^^^

``Image Demosaic(View source, System.Int32 cfaOffset)``

De-bayer raw image data to produce an RGB image.

The method **Demosaic** has the following parameters:

+-----------------+--------------------+---------------+
| Parameter       | Type               | Description   |
+=================+====================+===============+
| ``source``      | ``View``           |               |
+-----------------+--------------------+---------------+
| ``cfaOffset``   | ``System.Int32``   |               |
+-----------------+--------------------+---------------+
