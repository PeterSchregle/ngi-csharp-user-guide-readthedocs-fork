Class *CameraManager*
---------------------

The camera manager.

**Namespace:** Ngi

**Module:** Camera

The class **CameraManager** contains the following properties:

+------------+--------+--------+----------------------------------------------------+
| Property   | Get    | Set    | Description                                        |
+============+========+========+====================================================+
| ``Availabl | \*     |        | The list of available camera support modules in    |
| eModuleFil |        |        | the form of the lower case filename (without path  |
| eIds``     |        |        | and extension) of the dll.                         |
+------------+--------+--------+----------------------------------------------------+
| ``ModuleId | \*     | \*     |                                                    |
| sToLoad``  |        |        |                                                    |
+------------+--------+--------+----------------------------------------------------+
| ``Availabl | \*     | \*     |                                                    |
| eCamerasFl |        |        |                                                    |
| ag``       |        |        |                                                    |
+------------+--------+--------+----------------------------------------------------+
| ``Availabl | \*     |        | The list of available cameras.                     |
| eCameras`` |        |        |                                                    |
+------------+--------+--------+----------------------------------------------------+

The class **CameraManager** contains the following methods:

+------------------------------+--------------------------------+
| Method                       | Description                    |
+==============================+================================+
| ``UpdateAvailableCameras``   | Scan for available cvameras.   |
+------------------------------+--------------------------------+

Description
~~~~~~~~~~~

There should be only a single instance of the camera\_manager.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *CameraManager*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``CameraManager()``

Properties
~~~~~~~~~~

Property *AvailableModuleFileIds*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``StringList AvailableModuleFileIds``

The list of available camera support modules in the form of the lower case filename (without path and extension) of the dll.

Used to identify the camera support modules without loading them (only then we could access their "Id")

Property *ModuleIdsToLoad*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``StringList ModuleIdsToLoad``

Property *AvailableCamerasFlag*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean AvailableCamerasFlag``

Property *AvailableCameras*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``CameraInfoList AvailableCameras``

The list of available cameras.

Call update\_available\_cameras to search for cameras.

Methods
~~~~~~~

Method *UpdateAvailableCameras*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void UpdateAvailableCameras()``

Scan for available cvameras.

Only call once after creating the camera\_manager.
