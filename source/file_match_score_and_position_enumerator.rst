Class *MatchScoreAndPositionEnumerator*
---------------------------------------

**Namespace:** Ngi

**Module:** TemplateMatching

The class **MatchScoreAndPositionEnumerator** implements the following interfaces:

+----------------------------------------+
| Interface                              |
+========================================+
| ``IEnumerator``                        |
+----------------------------------------+
| ``IEnumeratorMatchScoreAndPosition``   |
+----------------------------------------+

The class **MatchScoreAndPositionEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **MatchScoreAndPositionEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``MatchScoreAndPosition Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
