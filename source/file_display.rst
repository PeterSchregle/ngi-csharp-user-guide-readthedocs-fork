Class *Display*
---------------

The display.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Display** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **Display** contains the following variant parameters:

+--------------+----------------------------------------------------------------+
| Variant      | Description                                                    |
+==============+================================================================+
| ``Widget``   | The widget variant that can be displayed inside the display.   |
+--------------+----------------------------------------------------------------+

Description
~~~~~~~~~~~

Ths class is used to create display windows of different types. It can be used to display an image, a palette or a polyline curve (histogram or palette).

The window can be created as a top-level window, as a child window or by subclassing an already existing window.

In order to display the variant types, you need to pass a widget of the type of display that you want to one of the constructors.

Variants
~~~~~~~~

Variant *Widget*
^^^^^^^^^^^^^^^^

The widget variant that can be displayed inside the display.

The variant parameter **Widget** has the following types:

+-------------------------------------------+
| Type                                      |
+===========================================+
| ``WidgetImageViewLocatorByte``            |
+-------------------------------------------+
| ``WidgetImageViewLocatorUInt16``          |
+-------------------------------------------+
| ``WidgetImageViewLocatorUInt32``          |
+-------------------------------------------+
| ``WidgetImageViewLocatorDouble``          |
+-------------------------------------------+
| ``WidgetImageViewLocatorRgbByte``         |
+-------------------------------------------+
| ``WidgetImageViewLocatorRgbUInt16``       |
+-------------------------------------------+
| ``WidgetImageViewLocatorRgbUInt32``       |
+-------------------------------------------+
| ``WidgetImageViewLocatorRgbDouble``       |
+-------------------------------------------+
| ``WidgetImageViewLocatorRgbaByte``        |
+-------------------------------------------+
| ``WidgetImageViewLocatorRgbaUInt16``      |
+-------------------------------------------+
| ``WidgetImageViewLocatorRgbaUInt32``      |
+-------------------------------------------+
| ``WidgetImageViewLocatorRgbaDouble``      |
+-------------------------------------------+
| ``WidgetImageViewLocatorHlsByte``         |
+-------------------------------------------+
| ``WidgetImageViewLocatorHlsUInt16``       |
+-------------------------------------------+
| ``WidgetImageViewLocatorHlsDouble``       |
+-------------------------------------------+
| ``WidgetImageViewLocatorHsiByte``         |
+-------------------------------------------+
| ``WidgetImageViewLocatorHsiUInt16``       |
+-------------------------------------------+
| ``WidgetImageViewLocatorHsiDouble``       |
+-------------------------------------------+
| ``WidgetImageViewLocatorLabByte``         |
+-------------------------------------------+
| ``WidgetImageViewLocatorLabUInt16``       |
+-------------------------------------------+
| ``WidgetImageViewLocatorLabDouble``       |
+-------------------------------------------+
| ``WidgetImageViewLocatorXyzByte``         |
+-------------------------------------------+
| ``WidgetImageViewLocatorXyzUInt16``       |
+-------------------------------------------+
| ``WidgetImageViewLocatorXyzDouble``       |
+-------------------------------------------+
| ``WidgetPaletteViewLocatorRgbaByte``      |
+-------------------------------------------+
| ``WidgetPolylineViewLocatorByte``         |
+-------------------------------------------+
| ``WidgetPolylineViewLocatorUInt16``       |
+-------------------------------------------+
| ``WidgetPolylineViewLocatorUInt32``       |
+-------------------------------------------+
| ``WidgetPolylineViewLocatorDouble``       |
+-------------------------------------------+
| ``WidgetPolylineViewLocatorRgbByte``      |
+-------------------------------------------+
| ``WidgetPolylineViewLocatorRgbUInt16``    |
+-------------------------------------------+
| ``WidgetPolylineViewLocatorRgbUInt32``    |
+-------------------------------------------+
| ``WidgetPolylineViewLocatorRgbDouble``    |
+-------------------------------------------+
| ``WidgetPolylineViewLocatorRgbaByte``     |
+-------------------------------------------+
| ``WidgetPolylineViewLocatorRgbaUInt16``   |
+-------------------------------------------+
| ``WidgetPolylineViewLocatorRgbaUInt32``   |
+-------------------------------------------+
| ``WidgetPolylineViewLocatorRgbaDouble``   |
+-------------------------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

Different widget variants can be displayed: Images of different types and color models, palettes, and histograms or profiles of different types.

Constructors
~~~~~~~~~~~~

Constructor *Display*
^^^^^^^^^^^^^^^^^^^^^

``Display(Widget widget, System.String title)``

Constructs a top-level window (with title bar and frames).

The constructor has the following parameters:

+--------------+---------------------+-----------------------------+
| Parameter    | Type                | Description                 |
+==============+=====================+=============================+
| ``widget``   | ``Widget``          | The widget to display.      |
+--------------+---------------------+-----------------------------+
| ``title``    | ``System.String``   | The initial window title.   |
+--------------+---------------------+-----------------------------+

The size of the window is determined by measuring its content.

A real operating system window has been created after this constructor has returned.

Constructor *Display*
^^^^^^^^^^^^^^^^^^^^^

``Display(Widget widget, System.String title, System.Boolean visible)``

Constructs a top-level window (with title bar and frames).

The constructor has the following parameters:

+---------------+----------------------+------------------------------------------------------------------------------------------------+
| Parameter     | Type                 | Description                                                                                    |
+===============+======================+================================================================================================+
| ``widget``    | ``Widget``           | The widget to display. The function makes a copy of the widget and uses the copy internally.   |
+---------------+----------------------+------------------------------------------------------------------------------------------------+
| ``title``     | ``System.String``    | The initial window title.                                                                      |
+---------------+----------------------+------------------------------------------------------------------------------------------------+
| ``visible``   | ``System.Boolean``   | The window visiblity.                                                                          |
+---------------+----------------------+------------------------------------------------------------------------------------------------+

The size of the window is determined by measuring its content.

A real operating system window has been created after this constructor has returned.

Constructor *Display*
^^^^^^^^^^^^^^^^^^^^^

``Display(Widget widget, VectorInt32 size, System.String title)``

Constructs a top-level window (with title bar and frames).

The constructor has the following parameters:

+--------------+------------+----------------------------------------------------+
| Parameter    | Type       | Description                                        |
+==============+============+====================================================+
| ``widget``   | ``Widget`` | The widget to display.                             |
+--------------+------------+----------------------------------------------------+
| ``size``     | ``VectorIn | The initial window and embedded widget\_image      |
|              | t32``      | size. Suitable space is added for window           |
|              |            | adornments, such as title bar and borders.         |
+--------------+------------+----------------------------------------------------+
| ``title``    | ``System.S | The initial window title.                          |
|              | tring``    |                                                    |
+--------------+------------+----------------------------------------------------+

A real operating system window has been created after this constructor has returned.

Constructor *Display*
^^^^^^^^^^^^^^^^^^^^^

``Display(Widget widget, VectorInt32 size, System.String title, System.Boolean visible)``

Constructs a top-level window (with title bar and frames).

The constructor has the following parameters:

+--------------+------------+----------------------------------------------------+
| Parameter    | Type       | Description                                        |
+==============+============+====================================================+
| ``widget``   | ``Widget`` | The widget to display. The function makes a copy   |
|              |            | of the widget and uses the copy internally.        |
+--------------+------------+----------------------------------------------------+
| ``size``     | ``VectorIn | The initial window and embedded widget\_image      |
|              | t32``      | size. Suitable space is added for window           |
|              |            | adornments, such as title bar and borders.         |
+--------------+------------+----------------------------------------------------+
| ``title``    | ``System.S | The initial window title.                          |
|              | tring``    |                                                    |
+--------------+------------+----------------------------------------------------+
| ``visible``  | ``System.B | The window visiblity.                              |
|              | oolean``   |                                                    |
+--------------+------------+----------------------------------------------------+

A real operating system window has been created after this constructor has returned.

Constructor *Display*
^^^^^^^^^^^^^^^^^^^^^

``Display(System.IntPtr parent, Widget widget, BoxInt32 position)``

Constructs a child window.

The constructor has the following parameters:

+----------------+---------------------+----------------------------------------------------------------+
| Parameter      | Type                | Description                                                    |
+================+=====================+================================================================+
| ``parent``     | ``System.IntPtr``   | Parent window handle.                                          |
+----------------+---------------------+----------------------------------------------------------------+
| ``widget``     | ``Widget``          | The widget to display.                                         |
+----------------+---------------------+----------------------------------------------------------------+
| ``position``   | ``BoxInt32``        | The position of the child window with respect to the parent.   |
+----------------+---------------------+----------------------------------------------------------------+

A real operating system window has been created after this constructor has returned.

Constructor *Display*
^^^^^^^^^^^^^^^^^^^^^

``Display(System.IntPtr parent, Widget widget, BoxInt32 position, System.Boolean visible)``

Constructs a child window.

The constructor has the following parameters:

+----------------+----------------------+------------------------------------------------------------------------------------------------+
| Parameter      | Type                 | Description                                                                                    |
+================+======================+================================================================================================+
| ``parent``     | ``System.IntPtr``    | Parent window handle.                                                                          |
+----------------+----------------------+------------------------------------------------------------------------------------------------+
| ``widget``     | ``Widget``           | The widget to display. The function makes a copy of the widget and uses the copy internally.   |
+----------------+----------------------+------------------------------------------------------------------------------------------------+
| ``position``   | ``BoxInt32``         | The position of the child window with respect to the parent.                                   |
+----------------+----------------------+------------------------------------------------------------------------------------------------+
| ``visible``    | ``System.Boolean``   | The window visiblity.                                                                          |
+----------------+----------------------+------------------------------------------------------------------------------------------------+

A real operating system window has been created after this constructor has returned.

Constructor *Display*
^^^^^^^^^^^^^^^^^^^^^

``Display(System.IntPtr window, Widget widget)``

Construct a display.

The constructor has the following parameters:

+--------------+---------------------+---------------------------------------+
| Parameter    | Type                | Description                           |
+==============+=====================+=======================================+
| ``window``   | ``System.IntPtr``   | The window handle to be subclassed.   |
+--------------+---------------------+---------------------------------------+
| ``widget``   | ``Widget``          | Pointer to the widget to display.     |
+--------------+---------------------+---------------------------------------+

Subclasses an existing window.
