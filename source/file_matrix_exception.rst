Class *MatrixException*
-----------------------

The basic ngi exception type.

**Namespace:** Ngi

**Module:**

The class **MatrixException** contains the following enumerations:

+--------------------+------------------------------+
| Enumeration        | Description                  |
+====================+==============================+
| ``ExceptionIds``   | TODO documentation missing   |
+--------------------+------------------------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *MatrixException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixException()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *MatrixException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixException(System.String message)``

Construct an exception from a message.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+

Constructor *MatrixException*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MatrixException(System.String message, System.UInt32 id)``

Construct an exception from a message and an id.

The constructor has the following parameters:

+---------------+---------------------+--------------------------+
| Parameter     | Type                | Description              |
+===============+=====================+==========================+
| ``message``   | ``System.String``   | The exception message.   |
+---------------+---------------------+--------------------------+
| ``id``        | ``System.UInt32``   | The exception id.        |
+---------------+---------------------+--------------------------+

Enumerations
~~~~~~~~~~~~

Enumeration *ExceptionIds*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum ExceptionIds``

TODO documentation missing

The enumeration **ExceptionIds** has the following constants:

+-----------------------------------+---------+---------------+
| Name                              | Value   | Description   |
+===================================+=========+===============+
| ``notAnError``                    | ``0``   |               |
+-----------------------------------+---------+---------------+
| ``argumentOutOfRange``            | ``1``   |               |
+-----------------------------------+---------+---------------+
| ``matrixMustBeSquare``            | ``2``   |               |
+-----------------------------------+---------+---------------+
| ``matrixIsSingular``              | ``3``   |               |
+-----------------------------------+---------+---------------+
| ``matrixIsRankDeficient``         | ``4``   |               |
+-----------------------------------+---------+---------------+
| ``matrixIsNotSymmetric``          | ``5``   |               |
+-----------------------------------+---------+---------------+
| ``matrixIsNotPositiveDefinite``   | ``6``   |               |
+-----------------------------------+---------+---------------+
| ``matrixDimensionsDoNotMatch``    | ``7``   |               |
+-----------------------------------+---------+---------------+

::

    enum ExceptionIds
    {
      notAnError = 0,
      argumentOutOfRange = 1,
      matrixMustBeSquare = 2,
      matrixIsSingular = 3,
      matrixIsRankDeficient = 4,
      matrixIsNotSymmetric = 5,
      matrixIsNotPositiveDefinite = 6,
      matrixDimensionsDoNotMatch = 7,
    };

TODO documentation missing
