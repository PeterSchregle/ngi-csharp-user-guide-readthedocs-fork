Class *Rgba*
------------

The RGBA color type.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Rgba** contains the following variant parameters:

+-----------------+-----------------------------------------+
| Variant         | Description                             |
+=================+=========================================+
| ``Component``   | TODO no brief description for variant   |
+-----------------+-----------------------------------------+

The class **Rgba** contains the following properties:

+-----------------+-------+-------+---------------------------------------+
| Property        | Get   | Set   | Description                           |
+=================+=======+=======+=======================================+
| ``Red``         | \*    | \*    | The red color component.              |
+-----------------+-------+-------+---------------------------------------+
| ``Green``       | \*    | \*    | The green color component.            |
+-----------------+-------+-------+---------------------------------------+
| ``Blue``        | \*    | \*    | The blue color component.             |
+-----------------+-------+-------+---------------------------------------+
| ``Alpha``       | \*    | \*    | The alpha color component.            |
+-----------------+-------+-------+---------------------------------------+
| ``Minimum``     | \*    |       | The minimum color component.          |
+-----------------+-------+-------+---------------------------------------+
| ``Maximum``     | \*    |       | The maximum color component.          |
+-----------------+-------+-------+---------------------------------------+
| ``Gray``        | \*    |       | The color converted to gray.          |
+-----------------+-------+-------+---------------------------------------+
| ``MonoAlpha``   | \*    |       | The color converted to mono\_alpha.   |
+-----------------+-------+-------+---------------------------------------+
| ``Rgb``         | \*    |       | The color converted to rgb.           |
+-----------------+-------+-------+---------------------------------------+

The class **Rgba** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

The RGBA color type template allows to be instantiated for the basic integer and floating point types.

The color primitives are in blue\_ - green\_ - red\_ - alpha\_ order, to match the most obvious bitmap organization and ease blitting operations.

Variants
~~~~~~~~

Variant *Component*
^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Component** has the following types:

+--------------+
| Type         |
+==============+
| ``Int8``     |
+--------------+
| ``Byte``     |
+--------------+
| ``Int16``    |
+--------------+
| ``UInt16``   |
+--------------+
| ``Int32``    |
+--------------+
| ``UInt32``   |
+--------------+
| ``Int64``    |
+--------------+
| ``UInt64``   |
+--------------+
| ``Single``   |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Rgba*
^^^^^^^^^^^^^^^^^^

``Rgba()``

Default constructor.

This constructor sets the four components to 0. This corresponds to a color that is pure black with zero opacity, i.e. full transparency. This color is therefore invisible.

Constructors
~~~~~~~~~~~~

Constructor *Rgba*
^^^^^^^^^^^^^^^^^^

``Rgba(System.Object gray)``

Standard constructor.

The constructor has the following parameters:

+-------------+---------------------+-----------------------+
| Parameter   | Type                | Description           |
+=============+=====================+=======================+
| ``gray``    | ``System.Object``   | The gray component.   |
+-------------+---------------------+-----------------------+

This constructor initializes the three color components with a gray value and the alpha component with an alpha value.

The constructor also serves as a conversion operator from the scalar type T to type rgba<T>.

Constructor *Rgba*
^^^^^^^^^^^^^^^^^^

``Rgba(System.Object red, System.Object green, System.Object blue, System.Object alpha)``

Standard constructor.

The constructor has the following parameters:

+-------------+---------------------+--------------------------+
| Parameter   | Type                | Description              |
+=============+=====================+==========================+
| ``red``     | ``System.Object``   |                          |
+-------------+---------------------+--------------------------+
| ``green``   | ``System.Object``   |                          |
+-------------+---------------------+--------------------------+
| ``blue``    | ``System.Object``   |                          |
+-------------+---------------------+--------------------------+
| ``alpha``   | ``System.Object``   | The alpha\_ component.   |
+-------------+---------------------+--------------------------+

This constructor initializes the three color components with a gray value and the alpha component with an alpha value.

The constructor also serves as a conversion operator from the scalar type mono<T> to type rgba<T>.

Properties
~~~~~~~~~~

Property *Red*
^^^^^^^^^^^^^^

``System.Object Red``

The red color component.

Property *Green*
^^^^^^^^^^^^^^^^

``System.Object Green``

The green color component.

Property *Blue*
^^^^^^^^^^^^^^^

``System.Object Blue``

The blue color component.

Property *Alpha*
^^^^^^^^^^^^^^^^

``System.Object Alpha``

The alpha color component.

Property *Minimum*
^^^^^^^^^^^^^^^^^^

``System.Object Minimum``

The minimum color component.

Types with an alpha channel are special, since the alpha channel is something different that has nothing to do with the vector character. The alpha channel does not take part in the calculation.

Property *Maximum*
^^^^^^^^^^^^^^^^^^

``System.Object Maximum``

The maximum color component.

Types with an alpha channel are special, since the alpha channel is something different that has nothing to do with the vector character. The alpha channel does not take part in the calculation.

Property *Gray*
^^^^^^^^^^^^^^^

``System.Object Gray``

The color converted to gray.

Property *MonoAlpha*
^^^^^^^^^^^^^^^^^^^^

``MonoAlpha MonoAlpha``

The color converted to mono\_alpha.

Property *Rgb*
^^^^^^^^^^^^^^

``Rgb Rgb``

The color converted to rgb.

Static Methods
~~~~~~~~~~~~~~

Method *FromString*
^^^^^^^^^^^^^^^^^^^

``Rgba FromString(System.String value)``

Parse a value from a string.

The method **FromString** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``value``   | ``System.String``   |               |
+-------------+---------------------+---------------+

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
