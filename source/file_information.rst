Class *Information*
-------------------

Information functions.

**Namespace:** Ngi

**Module:**

Description
~~~~~~~~~~~

The class contains a group of functions that retrieve the version and build date.

Static Methods
~~~~~~~~~~~~~~

Method *Version*
^^^^^^^^^^^^^^^^

``System.String Version()``

The version number of nGI.

The version number is a string combined of the major and minor version numbers, as well as the step and the build number of nGI.

Method *MajorVersion*
^^^^^^^^^^^^^^^^^^^^^

``System.Int32 MajorVersion()``

The major version number of NGI.

The full version number consists of the major and minor version numbers, as well as the step and the build numbers of nGI.

Method *MinorVersion*
^^^^^^^^^^^^^^^^^^^^^

``System.Int32 MinorVersion()``

The minor version number of nGI.

The full version number consists of the major and minor version numbers, as well as the step and the build numbers of nGI.

Method *StepNumber*
^^^^^^^^^^^^^^^^^^^

``System.Int32 StepNumber()``

The step number of nGI.

The full version number consists of the major and minor version numbers, as well as the step and the build numbers of nGI.

Method *BuildNumber*
^^^^^^^^^^^^^^^^^^^^

``System.Int32 BuildNumber()``

The build number of nGI.

The full version number consists of the major and minor version numbers, as well as the step and the build numbers of nGI.

Method *BuildDate*
^^^^^^^^^^^^^^^^^^

``System.String BuildDate()``

The build date of nGI.

The compilation date of nGI. The date is a constant length string literal of the form Mmm dd yyyy. Mmm is the english abbreviated month name. The first character of date dd is a space if the value is less than 10.
