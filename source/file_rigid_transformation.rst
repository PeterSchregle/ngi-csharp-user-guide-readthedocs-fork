Class *RigidTransformation*
---------------------------

Rotation transformation of geometric primitives.

**Namespace:** Ngi

**Module:** ImageProcessing

Description
~~~~~~~~~~~

This class provides static methods to transform geometric primitives via a rotation transformation.

The class can transform these geometric primitives: point, vector, direction, line\_segment, ray, line, box, rectangle, circle, ellipse, arc elliptical\_arc, triangle, quadrilateral, polyline, quadratic\_bezier and geometry.

All primitives retain their nature, when transformed by a rotation transform.

Some primitives can have an integer coordinate type on input, but the resulting types are always of floating point coordinate type. This is because the transformation cannot retain the integer nature in general. To illustrate, a point<int> type is transformed and the result is a point<double> type.

Static Methods
~~~~~~~~~~~~~~

Method *Transform*
^^^^^^^^^^^^^^^^^^

``PointDouble Transform(PointDouble point, RigidMatrix rigidMatrix)``

Transform a point.

The method **Transform** has the following parameters:

+-------------------+-------------------+---------------+
| Parameter         | Type              | Description   |
+===================+===================+===============+
| ``point``         | ``PointDouble``   |               |
+-------------------+-------------------+---------------+
| ``rigidMatrix``   | ``RigidMatrix``   |               |
+-------------------+-------------------+---------------+

A point transformed with an rigid transformation results in another point.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed point.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``VectorDouble Transform(VectorDouble vector, RigidMatrix rigidMatrix)``

Transform a vector.

The method **Transform** has the following parameters:

+-------------------+--------------------+---------------+
| Parameter         | Type               | Description   |
+===================+====================+===============+
| ``vector``        | ``VectorDouble``   |               |
+-------------------+--------------------+---------------+
| ``rigidMatrix``   | ``RigidMatrix``    |               |
+-------------------+--------------------+---------------+

A vector transformed with an rigid transformation results in another vector.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed vector.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Direction Transform(Direction direction, RigidMatrix rigidMatrix)``

Transform a direction.

The method **Transform** has the following parameters:

+-------------------+-------------------+---------------+
| Parameter         | Type              | Description   |
+===================+===================+===============+
| ``direction``     | ``Direction``     |               |
+-------------------+-------------------+---------------+
| ``rigidMatrix``   | ``RigidMatrix``   |               |
+-------------------+-------------------+---------------+

A direction transformed with an rigid transformation results in another direction.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed direction.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``LineSegmentDouble Transform(LineSegmentDouble lineSegment, RigidMatrix rigidMatrix)``

A line segment transformed with an rigid transformation results in another line segment.

The method **Transform** has the following parameters:

+-------------------+-------------------------+---------------+
| Parameter         | Type                    | Description   |
+===================+=========================+===============+
| ``lineSegment``   | ``LineSegmentDouble``   |               |
+-------------------+-------------------------+---------------+
| ``rigidMatrix``   | ``RigidMatrix``         |               |
+-------------------+-------------------------+---------------+

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed line segment.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``RayDouble Transform(RayDouble ray, RigidMatrix rigidMatrix)``

Transform a ray.

The method **Transform** has the following parameters:

+-------------------+-------------------+---------------+
| Parameter         | Type              | Description   |
+===================+===================+===============+
| ``ray``           | ``RayDouble``     |               |
+-------------------+-------------------+---------------+
| ``rigidMatrix``   | ``RigidMatrix``   |               |
+-------------------+-------------------+---------------+

A ray transformed with an rigid transformation results in another ray.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed ray.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``LineDouble Transform(LineDouble line, RigidMatrix rigidMatrix)``

Transform a line.

The method **Transform** has the following parameters:

+-------------------+-------------------+---------------+
| Parameter         | Type              | Description   |
+===================+===================+===============+
| ``line``          | ``LineDouble``    |               |
+-------------------+-------------------+---------------+
| ``rigidMatrix``   | ``RigidMatrix``   |               |
+-------------------+-------------------+---------------+

A line transformed with an rigid transformation results in another line.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed line.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Rectangle Transform(BoxDouble box, RigidMatrix rigidMatrix)``

Transform a box.

The method **Transform** has the following parameters:

+-------------------+-------------------+---------------+
| Parameter         | Type              | Description   |
+===================+===================+===============+
| ``box``           | ``BoxDouble``     |               |
+-------------------+-------------------+---------------+
| ``rigidMatrix``   | ``RigidMatrix``   |               |
+-------------------+-------------------+---------------+

A box transformed with an rigid transformation results in a rectangle.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed box in the form of a rectangle.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Rectangle Transform(Rectangle box, RigidMatrix rigidMatrix)``

Transform a rectangle.

The method **Transform** has the following parameters:

+-------------------+-------------------+---------------+
| Parameter         | Type              | Description   |
+===================+===================+===============+
| ``box``           | ``Rectangle``     |               |
+-------------------+-------------------+---------------+
| ``rigidMatrix``   | ``RigidMatrix``   |               |
+-------------------+-------------------+---------------+

A rectangle transformed with an rigid transformation results in a rectangle.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed box in the form of a rectangle.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Circle Transform(Circle circle, RigidMatrix rigidMatrix)``

Transform a circle.

The method **Transform** has the following parameters:

+-------------------+-------------------+---------------+
| Parameter         | Type              | Description   |
+===================+===================+===============+
| ``circle``        | ``Circle``        |               |
+-------------------+-------------------+---------------+
| ``rigidMatrix``   | ``RigidMatrix``   |               |
+-------------------+-------------------+---------------+

A circle transformed with an rigid transformation results in an circle.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed circle in the form of an ellipse.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Ellipse Transform(Ellipse ellipse, RigidMatrix rigidMatrix)``

Transform an ellipse.

The method **Transform** has the following parameters:

+-------------------+-------------------+---------------+
| Parameter         | Type              | Description   |
+===================+===================+===============+
| ``ellipse``       | ``Ellipse``       |               |
+-------------------+-------------------+---------------+
| ``rigidMatrix``   | ``RigidMatrix``   |               |
+-------------------+-------------------+---------------+

An ellipse transformed with an rigid transformation results in another ellipse.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed ellipse.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Arc Transform(Arc arc, RigidMatrix rigidMatrix)``

Transform an arc.

The method **Transform** has the following parameters:

+-------------------+-------------------+---------------+
| Parameter         | Type              | Description   |
+===================+===================+===============+
| ``arc``           | ``Arc``           |               |
+-------------------+-------------------+---------------+
| ``rigidMatrix``   | ``RigidMatrix``   |               |
+-------------------+-------------------+---------------+

An arc transformed with an rigid transformation results in an elliptical arc.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed elliptical arc.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``EllipticalArc Transform(EllipticalArc ellipticalArc, RigidMatrix rigidMatrix)``

Transform an elliptical arc.

The method **Transform** has the following parameters:

+---------------------+---------------------+---------------+
| Parameter           | Type                | Description   |
+=====================+=====================+===============+
| ``ellipticalArc``   | ``EllipticalArc``   |               |
+---------------------+---------------------+---------------+
| ``rigidMatrix``     | ``RigidMatrix``     |               |
+---------------------+---------------------+---------------+

An elliptical arc transformed with an rigid transformation results in another elliptical arc

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed elliptical arc.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``TriangleDouble Transform(TriangleDouble triangle, RigidMatrix rigidMatrix)``

Transform a triangle.

The method **Transform** has the following parameters:

+-------------------+----------------------+---------------+
| Parameter         | Type                 | Description   |
+===================+======================+===============+
| ``triangle``      | ``TriangleDouble``   |               |
+-------------------+----------------------+---------------+
| ``rigidMatrix``   | ``RigidMatrix``      |               |
+-------------------+----------------------+---------------+

A triangle transformed with an rigid transformation results in another triangle.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed triangle.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``QuadrilateralDouble Transform(QuadrilateralDouble quadrilateral, RigidMatrix rigidMatrix)``

Transform a quadrilateral.

The method **Transform** has the following parameters:

+---------------------+---------------------------+---------------+
| Parameter           | Type                      | Description   |
+=====================+===========================+===============+
| ``quadrilateral``   | ``QuadrilateralDouble``   |               |
+---------------------+---------------------------+---------------+
| ``rigidMatrix``     | ``RigidMatrix``           |               |
+---------------------+---------------------------+---------------+

A quadrilateral transformed with an rigid transformation results in another quadrilateral.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed quadrilateral.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``PolylineDouble Transform(PolylineDouble polyline, RigidMatrix rigidMatrix)``

Transform a polyline.

The method **Transform** has the following parameters:

+-------------------+----------------------+---------------+
| Parameter         | Type                 | Description   |
+===================+======================+===============+
| ``polyline``      | ``PolylineDouble``   |               |
+-------------------+----------------------+---------------+
| ``rigidMatrix``   | ``RigidMatrix``      |               |
+-------------------+----------------------+---------------+

A polyline transformed with an rigid transformation results in another polyline.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed polyline.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``QuadraticBezier Transform(QuadraticBezier bezier, RigidMatrix rigidMatrix)``

Transform a quadratic bezier curve.

The method **Transform** has the following parameters:

+-------------------+-----------------------+---------------+
| Parameter         | Type                  | Description   |
+===================+=======================+===============+
| ``bezier``        | ``QuadraticBezier``   |               |
+-------------------+-----------------------+---------------+
| ``rigidMatrix``   | ``RigidMatrix``       |               |
+-------------------+-----------------------+---------------+

A quadratic bezier curve transformed with an rigid transformation results in another quadratic bezier curve.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed quadratic bezier curve.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``CubicBezier Transform(CubicBezier bezier, RigidMatrix rigidMatrix)``

Transform a cubic bezier curve.

The method **Transform** has the following parameters:

+-------------------+-------------------+---------------+
| Parameter         | Type              | Description   |
+===================+===================+===============+
| ``bezier``        | ``CubicBezier``   |               |
+-------------------+-------------------+---------------+
| ``rigidMatrix``   | ``RigidMatrix``   |               |
+-------------------+-------------------+---------------+

A cubic bezier curve transformed with an rigid transformation results in another cubic bezier curve.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed cubic bezier curve.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Geometry Transform(Geometry geometry, RigidMatrix rigidMatrix)``

Transform a geometry.

The method **Transform** has the following parameters:

+-------------------+-------------------+---------------+
| Parameter         | Type              | Description   |
+===================+===================+===============+
| ``geometry``      | ``Geometry``      |               |
+-------------------+-------------------+---------------+
| ``rigidMatrix``   | ``RigidMatrix``   |               |
+-------------------+-------------------+---------------+

A geometry transformed with an rigid transformation results in another geometry.

The rigid transformation is carried out by rigid transformation of each geometry element.

Returns the transformed geometry.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``PointDouble Transform(PointDouble point, Pose pose)``

Transform a point.

The method **Transform** has the following parameters:

+-------------+-------------------+---------------+
| Parameter   | Type              | Description   |
+=============+===================+===============+
| ``point``   | ``PointDouble``   |               |
+-------------+-------------------+---------------+
| ``pose``    | ``Pose``          |               |
+-------------+-------------------+---------------+

A point transformed with an rigid transformation results in another point.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed point.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``VectorDouble Transform(VectorDouble vector, Pose pose)``

Transform a vector.

The method **Transform** has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``vector``   | ``VectorDouble``   |               |
+--------------+--------------------+---------------+
| ``pose``     | ``Pose``           |               |
+--------------+--------------------+---------------+

A vector transformed with an rigid transformation results in another vector.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed vector.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Direction Transform(Direction direction, Pose pose)``

Transform a direction.

The method **Transform** has the following parameters:

+-----------------+-----------------+---------------+
| Parameter       | Type            | Description   |
+=================+=================+===============+
| ``direction``   | ``Direction``   |               |
+-----------------+-----------------+---------------+
| ``pose``        | ``Pose``        |               |
+-----------------+-----------------+---------------+

A direction transformed with an rigid transformation results in another direction.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed direction.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``LineSegmentDouble Transform(LineSegmentDouble lineSegment, Pose pose)``

A line segment transformed with an rigid transformation results in another line segment.

The method **Transform** has the following parameters:

+-------------------+-------------------------+---------------+
| Parameter         | Type                    | Description   |
+===================+=========================+===============+
| ``lineSegment``   | ``LineSegmentDouble``   |               |
+-------------------+-------------------------+---------------+
| ``pose``          | ``Pose``                |               |
+-------------------+-------------------------+---------------+

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed line segment.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``RayDouble Transform(RayDouble ray, Pose pose)``

Transform a ray.

The method **Transform** has the following parameters:

+-------------+-----------------+---------------+
| Parameter   | Type            | Description   |
+=============+=================+===============+
| ``ray``     | ``RayDouble``   |               |
+-------------+-----------------+---------------+
| ``pose``    | ``Pose``        |               |
+-------------+-----------------+---------------+

A ray transformed with an rigid transformation results in another ray.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed ray.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``LineDouble Transform(LineDouble line, Pose pose)``

Transform a line.

The method **Transform** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``line``    | ``LineDouble``   |               |
+-------------+------------------+---------------+
| ``pose``    | ``Pose``         |               |
+-------------+------------------+---------------+

A line transformed with an rigid transformation results in another line.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed line.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Rectangle Transform(BoxDouble box, Pose pose)``

Transform a box.

The method **Transform** has the following parameters:

+-------------+-----------------+---------------+
| Parameter   | Type            | Description   |
+=============+=================+===============+
| ``box``     | ``BoxDouble``   |               |
+-------------+-----------------+---------------+
| ``pose``    | ``Pose``        |               |
+-------------+-----------------+---------------+

A box transformed with an rigid transformation results in a rectangle.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed box in the form of a rectangle.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Rectangle Transform(Rectangle box, Pose pose)``

Transform a rectangle.

The method **Transform** has the following parameters:

+-------------+-----------------+---------------+
| Parameter   | Type            | Description   |
+=============+=================+===============+
| ``box``     | ``Rectangle``   |               |
+-------------+-----------------+---------------+
| ``pose``    | ``Pose``        |               |
+-------------+-----------------+---------------+

A rectangle transformed with an rigid transformation results in a rectangle.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed box in the form of a rectangle.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Circle Transform(Circle circle, Pose pose)``

Transform a circle.

The method **Transform** has the following parameters:

+--------------+--------------+---------------+
| Parameter    | Type         | Description   |
+==============+==============+===============+
| ``circle``   | ``Circle``   |               |
+--------------+--------------+---------------+
| ``pose``     | ``Pose``     |               |
+--------------+--------------+---------------+

A circle transformed with an rigid transformation results in an circle.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed circle in the form of an ellipse.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Ellipse Transform(Ellipse ellipse, Pose pose)``

Transform an ellipse.

The method **Transform** has the following parameters:

+---------------+---------------+---------------+
| Parameter     | Type          | Description   |
+===============+===============+===============+
| ``ellipse``   | ``Ellipse``   |               |
+---------------+---------------+---------------+
| ``pose``      | ``Pose``      |               |
+---------------+---------------+---------------+

An ellipse transformed with an rigid transformation results in another ellipse.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed ellipse.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Arc Transform(Arc arc, Pose pose)``

Transform an arc.

The method **Transform** has the following parameters:

+-------------+------------+---------------+
| Parameter   | Type       | Description   |
+=============+============+===============+
| ``arc``     | ``Arc``    |               |
+-------------+------------+---------------+
| ``pose``    | ``Pose``   |               |
+-------------+------------+---------------+

An arc transformed with an rigid transformation results in an elliptical arc.

Currently, only pure translation is supported.

Returns the transformed elliptical arc.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``EllipticalArc Transform(EllipticalArc ellipticalArc, Pose pose)``

Transform an elliptical arc.

The method **Transform** has the following parameters:

+---------------------+---------------------+---------------+
| Parameter           | Type                | Description   |
+=====================+=====================+===============+
| ``ellipticalArc``   | ``EllipticalArc``   |               |
+---------------------+---------------------+---------------+
| ``pose``            | ``Pose``            |               |
+---------------------+---------------------+---------------+

An elliptical arc transformed with an rigid transformation results in another elliptical arc

Returns the transformed elliptical arc.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``TriangleDouble Transform(TriangleDouble triangle, Pose pose)``

Transform a triangle.

The method **Transform** has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``triangle``   | ``TriangleDouble``   |               |
+----------------+----------------------+---------------+
| ``pose``       | ``Pose``             |               |
+----------------+----------------------+---------------+

A triangle transformed with an rigid transformation results in another triangle.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed triangle.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``QuadrilateralDouble Transform(QuadrilateralDouble quadrilateral, Pose pose)``

Transform a quadrilateral.

The method **Transform** has the following parameters:

+---------------------+---------------------------+---------------+
| Parameter           | Type                      | Description   |
+=====================+===========================+===============+
| ``quadrilateral``   | ``QuadrilateralDouble``   |               |
+---------------------+---------------------------+---------------+
| ``pose``            | ``Pose``                  |               |
+---------------------+---------------------------+---------------+

A quadrilateral transformed with an rigid transformation results in another quadrilateral.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed quadrilateral.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``PolylineDouble Transform(PolylineDouble polyline, Pose pose)``

Transform a polyline.

The method **Transform** has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``polyline``   | ``PolylineDouble``   |               |
+----------------+----------------------+---------------+
| ``pose``       | ``Pose``             |               |
+----------------+----------------------+---------------+

A polyline transformed with an rigid transformation results in another polyline.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed polyline.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``QuadraticBezier Transform(QuadraticBezier polyline, Pose pose)``

Transform a quadratic bezier curve.

The method **Transform** has the following parameters:

+----------------+-----------------------+---------------+
| Parameter      | Type                  | Description   |
+================+=======================+===============+
| ``polyline``   | ``QuadraticBezier``   |               |
+----------------+-----------------------+---------------+
| ``pose``       | ``Pose``              |               |
+----------------+-----------------------+---------------+

A quadratic bezier curve transformed with an rigid transformation results in another quadratic bezier curve.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed quadratic bezier curve.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``CubicBezier Transform(CubicBezier bezier, Pose pose)``

Transform a cubic bezier curve.

The method **Transform** has the following parameters:

+--------------+-------------------+---------------+
| Parameter    | Type              | Description   |
+==============+===================+===============+
| ``bezier``   | ``CubicBezier``   |               |
+--------------+-------------------+---------------+
| ``pose``     | ``Pose``          |               |
+--------------+-------------------+---------------+

A cubic bezier curve transformed with an rigid transformation results in another cubic bezier curve.

The rigid transformation is carried out with a matrix multiplication.

Returns the transformed cubic bezier curve.

Method *Transform*
^^^^^^^^^^^^^^^^^^

``Geometry Transform(Geometry geometry, Pose pose)``

Transform a geometry.

The method **Transform** has the following parameters:

+----------------+----------------+---------------+
| Parameter      | Type           | Description   |
+================+================+===============+
| ``geometry``   | ``Geometry``   |               |
+----------------+----------------+---------------+
| ``pose``       | ``Pose``       |               |
+----------------+----------------+---------------+

A geometry transformed with an rigid transformation results in another geometry.

The rigid transformation is carried out by rigid transformation of each geometry element.

Returns the transformed geometry.
