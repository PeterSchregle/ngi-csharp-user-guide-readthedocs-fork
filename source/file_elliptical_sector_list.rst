Class *EllipticalSectorList*
----------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **EllipticalSectorList** implements the following interfaces:

+-----------------------------+
| Interface                   |
+=============================+
| ``IListEllipticalSector``   |
+-----------------------------+

The class **EllipticalSectorList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **EllipticalSectorList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *EllipticalSectorList*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``EllipticalSectorList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``EllipticalSector [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``EllipticalSectorEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(EllipticalSector item)``

The method **Add** has the following parameters:

+-------------+------------------------+---------------+
| Parameter   | Type                   | Description   |
+=============+========================+===============+
| ``item``    | ``EllipticalSector``   |               |
+-------------+------------------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(EllipticalSector item)``

The method **Contains** has the following parameters:

+-------------+------------------------+---------------+
| Parameter   | Type                   | Description   |
+=============+========================+===============+
| ``item``    | ``EllipticalSector``   |               |
+-------------+------------------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(EllipticalSector item)``

The method **Remove** has the following parameters:

+-------------+------------------------+---------------+
| Parameter   | Type                   | Description   |
+=============+========================+===============+
| ``item``    | ``EllipticalSector``   |               |
+-------------+------------------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(EllipticalSector item)``

The method **IndexOf** has the following parameters:

+-------------+------------------------+---------------+
| Parameter   | Type                   | Description   |
+=============+========================+===============+
| ``item``    | ``EllipticalSector``   |               |
+-------------+------------------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, EllipticalSector item)``

The method **Insert** has the following parameters:

+-------------+------------------------+---------------+
| Parameter   | Type                   | Description   |
+=============+========================+===============+
| ``index``   | ``System.Int32``       |               |
+-------------+------------------------+---------------+
| ``item``    | ``EllipticalSector``   |               |
+-------------+------------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
