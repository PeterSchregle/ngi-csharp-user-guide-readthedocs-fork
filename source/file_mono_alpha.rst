Class *MonoAlpha*
-----------------

The mono\_alpha pixel color type.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **MonoAlpha** contains the following variant parameters:

+-----------------+-----------------------------------------+
| Variant         | Description                             |
+=================+=========================================+
| ``Component``   | TODO no brief description for variant   |
+-----------------+-----------------------------------------+

The class **MonoAlpha** contains the following properties:

+-------------+-------+-------+----------------------------------------+
| Property    | Get   | Set   | Description                            |
+=============+=======+=======+========================================+
| ``Gray``    | \*    | \*    | The mono\_chrome component property.   |
+-------------+-------+-------+----------------------------------------+
| ``Alpha``   | \*    | \*    | The alpha\_ component property.        |
+-------------+-------+-------+----------------------------------------+

The class **MonoAlpha** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

The mono\_alpha pixel color type template allows to be instantiated for the basic integer and floating point types.

mono\_ alpha\_ is a pixel type with two components: a mono\_chrome intensity and an opacity alpha\_.

The color primitives are in mono\_ - alpha\_ order.

This class does not provide an implicit conversion operator to the rgba type, but the other way round: the rgba type provides a conversion from the rgb type, so that buffers of this type can be conveniently displayed.

Variants
~~~~~~~~

Variant *Component*
^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Component** has the following types:

+--------------+
| Type         |
+==============+
| ``Int8``     |
+--------------+
| ``Byte``     |
+--------------+
| ``Int16``    |
+--------------+
| ``UInt16``   |
+--------------+
| ``Int32``    |
+--------------+
| ``UInt32``   |
+--------------+
| ``Int64``    |
+--------------+
| ``UInt64``   |
+--------------+
| ``Single``   |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *MonoAlpha*
^^^^^^^^^^^^^^^^^^^^^^^

``MonoAlpha()``

Default constructor.

This constructor sets the two components to 0. This corresponds to a color that is pure black with zero opacity, i.e. full transparency. This color is therefore invisble.

Constructors
~~~~~~~~~~~~

Constructor *MonoAlpha*
^^^^^^^^^^^^^^^^^^^^^^^

``MonoAlpha(System.Object gray, System.Object alpha)``

Standard constructor.

The constructor has the following parameters:

+-------------+---------------------+--------------------------+
| Parameter   | Type                | Description              |
+=============+=====================+==========================+
| ``gray``    | ``System.Object``   | The gray component.      |
+-------------+---------------------+--------------------------+
| ``alpha``   | ``System.Object``   | The alpha\_ component.   |
+-------------+---------------------+--------------------------+

This constructor initializes the gray components with a gray value and the alpha\_ component with an alpha\_ value.

The constructor also serves as a conversion operator from the scalar type T to type mono\_alpha<T>.

Properties
~~~~~~~~~~

Property *Gray*
^^^^^^^^^^^^^^^

``System.Object Gray``

The mono\_chrome component property.

Property *Alpha*
^^^^^^^^^^^^^^^^

``System.Object Alpha``

The alpha\_ component property.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
