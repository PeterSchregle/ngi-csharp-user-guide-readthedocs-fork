Class *CountToolGroupList*
--------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **CountToolGroupList** implements the following interfaces:

+---------------------------+
| Interface                 |
+===========================+
| ``IListCountToolGroup``   |
+---------------------------+

The class **CountToolGroupList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **CountToolGroupList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *CountToolGroupList*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``CountToolGroupList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``CountToolGroup [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``CountToolGroupEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(CountToolGroup item)``

The method **Add** has the following parameters:

+-------------+----------------------+---------------+
| Parameter   | Type                 | Description   |
+=============+======================+===============+
| ``item``    | ``CountToolGroup``   |               |
+-------------+----------------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(CountToolGroup item)``

The method **Contains** has the following parameters:

+-------------+----------------------+---------------+
| Parameter   | Type                 | Description   |
+=============+======================+===============+
| ``item``    | ``CountToolGroup``   |               |
+-------------+----------------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(CountToolGroup item)``

The method **Remove** has the following parameters:

+-------------+----------------------+---------------+
| Parameter   | Type                 | Description   |
+=============+======================+===============+
| ``item``    | ``CountToolGroup``   |               |
+-------------+----------------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(CountToolGroup item)``

The method **IndexOf** has the following parameters:

+-------------+----------------------+---------------+
| Parameter   | Type                 | Description   |
+=============+======================+===============+
| ``item``    | ``CountToolGroup``   |               |
+-------------+----------------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, CountToolGroup item)``

The method **Insert** has the following parameters:

+-------------+----------------------+---------------+
| Parameter   | Type                 | Description   |
+=============+======================+===============+
| ``index``   | ``System.Int32``     |               |
+-------------+----------------------+---------------+
| ``item``    | ``CountToolGroup``   |               |
+-------------+----------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
