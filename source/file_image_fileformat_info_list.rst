Class *ImageFileformatInfoList*
-------------------------------

A list of regions.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **ImageFileformatInfoList** implements the following interfaces:

+--------------------------------+
| Interface                      |
+================================+
| ``IListImageFileformatInfo``   |
+--------------------------------+

The class **ImageFileformatInfoList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **ImageFileformatInfoList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

It is a list.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *ImageFileformatInfoList*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``ImageFileformatInfoList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``ImageFileformatInfo [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``ImageFileformatInfoEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(ImageFileformatInfo item)``

The method **Add** has the following parameters:

+-------------+---------------------------+---------------+
| Parameter   | Type                      | Description   |
+=============+===========================+===============+
| ``item``    | ``ImageFileformatInfo``   |               |
+-------------+---------------------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(ImageFileformatInfo item)``

The method **Contains** has the following parameters:

+-------------+---------------------------+---------------+
| Parameter   | Type                      | Description   |
+=============+===========================+===============+
| ``item``    | ``ImageFileformatInfo``   |               |
+-------------+---------------------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(ImageFileformatInfo item)``

The method **Remove** has the following parameters:

+-------------+---------------------------+---------------+
| Parameter   | Type                      | Description   |
+=============+===========================+===============+
| ``item``    | ``ImageFileformatInfo``   |               |
+-------------+---------------------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(ImageFileformatInfo item)``

The method **IndexOf** has the following parameters:

+-------------+---------------------------+---------------+
| Parameter   | Type                      | Description   |
+=============+===========================+===============+
| ``item``    | ``ImageFileformatInfo``   |               |
+-------------+---------------------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, ImageFileformatInfo item)``

The method **Insert** has the following parameters:

+-------------+---------------------------+---------------+
| Parameter   | Type                      | Description   |
+=============+===========================+===============+
| ``index``   | ``System.Int32``          |               |
+-------------+---------------------------+---------------+
| ``item``    | ``ImageFileformatInfo``   |               |
+-------------+---------------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
