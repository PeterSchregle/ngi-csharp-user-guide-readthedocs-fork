Class *Locator*
---------------

A template that provides a random-access locator as a basis for a mutable or a const locator.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Locator** implements the following interfaces:

+--------------------------------------------+
| Interface                                  |
+============================================+
| ``IEquatableLocatorLocatorPixelVariant``   |
+--------------------------------------------+

The class **Locator** contains the following variant parameters:

+-------------+-----------------------------------------+
| Variant     | Description                             |
+=============+=========================================+
| ``Pixel``   | TODO no brief description for variant   |
+-------------+-----------------------------------------+

The class **Locator** contains the following properties:

+------------------+-------+-------+------------------------+
| Property         | Get   | Set   | Description            |
+==================+=======+=======+========================+
| ``PixelPitch``   | \*    |       | The pixel pitch.       |
+------------------+-------+-------+------------------------+
| ``LinePitch``    | \*    |       | The line pitch.        |
+------------------+-------+-------+------------------------+
| ``PlanePitch``   | \*    |       | The plane pitch.       |
+------------------+-------+-------+------------------------+
| ``Data``         | \*    |       | Pointer to the data.   |
+------------------+-------+-------+------------------------+
| ``[index]``      | \*    | \*    | Indexer.               |
+------------------+-------+-------+------------------------+

The class **Locator** contains the following methods:

+---------------------+---------------------------------------------------------+
| Method              | Description                                             |
+=====================+=========================================================+
| ``Subsample``       | Returns a subsampling locator.                          |
+---------------------+---------------------------------------------------------+
| ``Reverse``         | Returns a reverse locator.                              |
+---------------------+---------------------------------------------------------+
| ``DimensionSwap``   | Returns a dimension swap locator.                       |
+---------------------+---------------------------------------------------------+
| ``ToString``        | Provide string representation for debugging purposes.   |
+---------------------+---------------------------------------------------------+

The class **Locator** contains the following enumerations:

+-----------------------+------------------------------+
| Enumeration           | Description                  |
+=======================+==============================+
| ``DimensionSwapId``   | TODO documentation missing   |
+-----------------------+------------------------------+

Description
~~~~~~~~~~~

A locator is a kind of a 'three-dimensional iterator' that's used within ngi to address buffers in a three-dimensional fashion.

.. figure:: images/locator.png
   :alt: 

A locator can also be seen as an adapter to a block of data. Given a pointer to the block of data plus three-dimensional shape information - such as pixel\_pitch, line\_pitch, and plane\_pitch, - a locator can be created from any block of data.

Similar to an iterator that can move in one dimension, a locator can move in three dimensions (horizontal, vertical and planar).

In the same way as an iterator can point to some location out of bounds, a locator can also point to three dimensional locations that are out of bounds. Dereferencing a locator that is out of bounds is forbidden, and the result is undefined.

The movement in three dimensions is accomplished by using three different pitches or strides. The first pitch - the pixel\_pitch - is used to move in the horizontal dimension. Incrementing the locator by the pixel\_pitch moves to the horizontal neighbor at the right. The second pitch - the line\_pitch - is used to move in the vertical dimension. The line\_pitch is the amount of increments needed to go from the current line to the next (usually this is the width of the image plus some optional padding). Incrementing the locator by the line\_pitch moves to the vertical neighbor at the bottom. The third pitch - the plane\_pitch - is used to move in the planar dimension. The plane\_pitch is the amount of increments needed to go from the current plane to the next (usually this is the width of the image plus padding multiplied by the height of the image). Incrementing the locator by the plane\_pitch moves to the planar neighbor. A locator can be compared

Variants
~~~~~~~~

Variant *Pixel*
^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Pixel** has the following types:

+------------------+
| Type             |
+==================+
| ``Byte``         |
+------------------+
| ``UInt16``       |
+------------------+
| ``UInt32``       |
+------------------+
| ``Double``       |
+------------------+
| ``RgbByte``      |
+------------------+
| ``RgbUInt16``    |
+------------------+
| ``RgbUInt32``    |
+------------------+
| ``RgbDouble``    |
+------------------+
| ``RgbaByte``     |
+------------------+
| ``RgbaUInt16``   |
+------------------+
| ``RgbaUInt32``   |
+------------------+
| ``RgbaDouble``   |
+------------------+
| ``HlsByte``      |
+------------------+
| ``HlsUInt16``    |
+------------------+
| ``HlsDouble``    |
+------------------+
| ``HsiByte``      |
+------------------+
| ``HsiUInt16``    |
+------------------+
| ``HsiDouble``    |
+------------------+
| ``LabByte``      |
+------------------+
| ``LabUInt16``    |
+------------------+
| ``LabDouble``    |
+------------------+
| ``XyzByte``      |
+------------------+
| ``XyzUInt16``    |
+------------------+
| ``XyzDouble``    |
+------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Locator*
^^^^^^^^^^^^^^^^^^^^^

``Locator()``

Default-construct a three-dimensional locator.

A locator constructed with this constructor can only serve to be instantiated. It cannot be used to iterate over some data.

Constructors
~~~~~~~~~~~~

Constructor *Locator*
^^^^^^^^^^^^^^^^^^^^^

``Locator(System.IntPtr data, System.Int32 pixelPitch, System.Int32 linePitch, System.Int32 planePitch)``

Construct a three-dimensional locator from a one-dimensional block of data.

The constructor has the following parameters:

+------------------+---------------------+---------------------------------------------------------------------------------------------------+
| Parameter        | Type                | Description                                                                                       |
+==================+=====================+===================================================================================================+
| ``data``         | ``System.IntPtr``   | Pointer to the data.                                                                              |
+------------------+---------------------+---------------------------------------------------------------------------------------------------+
| ``pixelPitch``   | ``System.Int32``    | The pixel pitch. This is the number of bytes to move to the next pixel horizontally.              |
+------------------+---------------------+---------------------------------------------------------------------------------------------------+
| ``linePitch``    | ``System.Int32``    | The line pitch. This is the number of bytes to move to the next pixel vertically.                 |
+------------------+---------------------+---------------------------------------------------------------------------------------------------+
| ``planePitch``   | ``System.Int32``    | The plane pitch. This is the number of bytes to move to the next pixel in the planar direction.   |
+------------------+---------------------+---------------------------------------------------------------------------------------------------+

In addition to the pointer to the data you need to pass three pitch values, expressed in a number of bytes.

Properties
~~~~~~~~~~

Property *PixelPitch*
^^^^^^^^^^^^^^^^^^^^^

``System.Int32 PixelPitch``

The pixel pitch.

This is the number of bytes to go from one pixel to the next, i.e. to perform horizontal navigation.

Property *LinePitch*
^^^^^^^^^^^^^^^^^^^^

``System.Int32 LinePitch``

The line pitch.

This is the number of bytes to go from one line to the next, i.e. to perform vertical navigation.

Property *PlanePitch*
^^^^^^^^^^^^^^^^^^^^^

``System.Int32 PlanePitch``

The plane pitch.

This is the number of bytes to go from one plane to the next, i.e. to perform planar navigation.

Property *Data*
^^^^^^^^^^^^^^^

``System.IntPtr Data``

Pointer to the data.

Property *[index]*
^^^^^^^^^^^^^^^^^^

``System.Object [index]``

Indexer.

Methods
~~~~~~~

Method *Subsample*
^^^^^^^^^^^^^^^^^^

``Locator Subsample(System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

Returns a subsampling locator.

The method **Subsample** has the following parameters:

+---------------+--------------------+------------------------------------------------------------------------------------------------------------------+
| Parameter     | Type               | Description                                                                                                      |
+===============+====================+==================================================================================================================+
| ``xFactor``   | ``System.Int32``   | The horizontal subsampling factor (>= 1).                                                                        |
+---------------+--------------------+------------------------------------------------------------------------------------------------------------------+
| ``yFactor``   | ``System.Int32``   | The vertical subsampling factor (>= 1). This defaults to 1 in order to ease one-dimensional subsampling.         |
+---------------+--------------------+------------------------------------------------------------------------------------------------------------------+
| ``zFactor``   | ``System.Int32``   | The planar subsampling factor (>= 1). This defaults to 1 in order to ease one- or two-dimensional subsampling.   |
+---------------+--------------------+------------------------------------------------------------------------------------------------------------------+

This adjusts the pitches according to the subsampling factors, so that the locator performs subsampled movement.

Method *Reverse*
^^^^^^^^^^^^^^^^

``Locator Reverse(System.Boolean xReverse, System.Boolean yReverse, System.Boolean zReverse)``

Returns a reverse locator.

The method **Reverse** has the following parameters:

+----------------+----------------------+-------------------------------------------------------------------------------------------------------+
| Parameter      | Type                 | Description                                                                                           |
+================+======================+=======================================================================================================+
| ``xReverse``   | ``System.Boolean``   | The horizontal reverse flag.                                                                          |
+----------------+----------------------+-------------------------------------------------------------------------------------------------------+
| ``yReverse``   | ``System.Boolean``   | The vertical reverse flag. This defaults to false in order to ease one-dimensional reversing.         |
+----------------+----------------------+-------------------------------------------------------------------------------------------------------+
| ``zReverse``   | ``System.Boolean``   | The planar reverse flag. This defaults to false in order to ease one- or two-dimensional reversing.   |
+----------------+----------------------+-------------------------------------------------------------------------------------------------------+

This adjusts the pitches according to the reverse flags, so that the locator performs reverse movement in the specified dimensions.

This function does not change the position of the locator; be careful to not move outside bounds.

Method *DimensionSwap*
^^^^^^^^^^^^^^^^^^^^^^

``Locator DimensionSwap(Locator.DimensionSwapId swap)``

Returns a dimension swap locator.

The method **DimensionSwap** has the following parameters:

+-------------+-------------------------------+----------------------------+
| Parameter   | Type                          | Description                |
+=============+===============================+============================+
| ``swap``    | ``Locator.DimensionSwapId``   | The dimension swap code.   |
+-------------+-------------------------------+----------------------------+

This adjusts the pitches of the locator according to the dimension order, so that the locator is suitable for movement with swapped dimensions.

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Enumerations
~~~~~~~~~~~~

Enumeration *DimensionSwapId*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum DimensionSwapId``

TODO documentation missing

The enumeration **DimensionSwapId** has the following constants:

+---------------+---------+---------------+
| Name          | Value   | Description   |
+===============+=========+===============+
| ``xyz2xyz``   | ``1``   |               |
+---------------+---------+---------------+
| ``xyz2xzy``   | ``2``   |               |
+---------------+---------+---------------+
| ``xyz2yxz``   | ``3``   |               |
+---------------+---------+---------------+
| ``xyz2yzx``   | ``4``   |               |
+---------------+---------+---------------+
| ``xyz2zxy``   | ``5``   |               |
+---------------+---------+---------------+
| ``xyz2zyx``   | ``6``   |               |
+---------------+---------+---------------+

::

    enum DimensionSwapId
    {
      xyz2xyz = 1,
      xyz2xzy = 2,
      xyz2yxz = 3,
      xyz2yzx = 4,
      xyz2zxy = 5,
      xyz2zyx = 6,
    };

TODO documentation missing
