Class *MatchScoreAndPositionList*
---------------------------------

**Namespace:** Ngi

**Module:** TemplateMatching

The class **MatchScoreAndPositionList** implements the following interfaces:

+----------------------------------+
| Interface                        |
+==================================+
| ``IListMatchScoreAndPosition``   |
+----------------------------------+

The class **MatchScoreAndPositionList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **MatchScoreAndPositionList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *MatchScoreAndPositionList*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``MatchScoreAndPosition [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``MatchScoreAndPositionEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(MatchScoreAndPosition item)``

The method **Add** has the following parameters:

+-------------+-----------------------------+---------------+
| Parameter   | Type                        | Description   |
+=============+=============================+===============+
| ``item``    | ``MatchScoreAndPosition``   |               |
+-------------+-----------------------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(MatchScoreAndPosition item)``

The method **Contains** has the following parameters:

+-------------+-----------------------------+---------------+
| Parameter   | Type                        | Description   |
+=============+=============================+===============+
| ``item``    | ``MatchScoreAndPosition``   |               |
+-------------+-----------------------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(MatchScoreAndPosition item)``

The method **Remove** has the following parameters:

+-------------+-----------------------------+---------------+
| Parameter   | Type                        | Description   |
+=============+=============================+===============+
| ``item``    | ``MatchScoreAndPosition``   |               |
+-------------+-----------------------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(MatchScoreAndPosition item)``

The method **IndexOf** has the following parameters:

+-------------+-----------------------------+---------------+
| Parameter   | Type                        | Description   |
+=============+=============================+===============+
| ``item``    | ``MatchScoreAndPosition``   |               |
+-------------+-----------------------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, MatchScoreAndPosition item)``

The method **Insert** has the following parameters:

+-------------+-----------------------------+---------------+
| Parameter   | Type                        | Description   |
+=============+=============================+===============+
| ``index``   | ``System.Int32``            |               |
+-------------+-----------------------------+---------------+
| ``item``    | ``MatchScoreAndPosition``   |               |
+-------------+-----------------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
