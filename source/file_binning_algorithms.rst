Class *BinningAlgorithms*
-------------------------

Image binning functions.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **BinningAlgorithms** contains the following enumerations:

+-----------------------+------------------------------+
| Enumeration           | Description                  |
+=======================+==============================+
| ``BinningFunction``   | TODO documentation missing   |
+-----------------------+------------------------------+

Description
~~~~~~~~~~~

The class contains a group of functions for binning images with a specific binning functions in the horizontal, vertical and planar direction.

Static Methods
~~~~~~~~~~~~~~

Method *Bin*
^^^^^^^^^^^^

``ImageByte Bin(ViewLocatorByte source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorByte``                     |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageUInt16 Bin(ViewLocatorUInt16 source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorUInt16``                   |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageUInt32 Bin(ViewLocatorUInt32 source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorUInt32``                   |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageDouble Bin(ViewLocatorDouble source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorDouble``                   |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageRgbByte Bin(ViewLocatorRgbByte source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorRgbByte``                  |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageRgbUInt16 Bin(ViewLocatorRgbUInt16 source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorRgbUInt16``                |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageRgbUInt32 Bin(ViewLocatorRgbUInt32 source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorRgbUInt32``                |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageRgbDouble Bin(ViewLocatorRgbDouble source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorRgbDouble``                |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageRgbaByte Bin(ViewLocatorRgbaByte source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorRgbaByte``                 |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageRgbaUInt16 Bin(ViewLocatorRgbaUInt16 source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorRgbaUInt16``               |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageRgbaUInt32 Bin(ViewLocatorRgbaUInt32 source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorRgbaUInt32``               |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageRgbaDouble Bin(ViewLocatorRgbaDouble source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorRgbaDouble``               |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageHlsByte Bin(ViewLocatorHlsByte source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorHlsByte``                  |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageHlsUInt16 Bin(ViewLocatorHlsUInt16 source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorHlsUInt16``                |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageHlsDouble Bin(ViewLocatorHlsDouble source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorHlsDouble``                |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageHsiByte Bin(ViewLocatorHsiByte source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorHsiByte``                  |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageHsiUInt16 Bin(ViewLocatorHsiUInt16 source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorHsiUInt16``                |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageHsiDouble Bin(ViewLocatorHsiDouble source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorHsiDouble``                |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageLabByte Bin(ViewLocatorLabByte source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorLabByte``                  |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageLabUInt16 Bin(ViewLocatorLabUInt16 source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorLabUInt16``                |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageLabDouble Bin(ViewLocatorLabDouble source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorLabDouble``                |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageXyzByte Bin(ViewLocatorXyzByte source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorXyzByte``                  |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageXyzUInt16 Bin(ViewLocatorXyzUInt16 source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorXyzUInt16``                |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageXyzDouble Bin(ViewLocatorXyzDouble source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorXyzDouble``                |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``Image Bin(View source, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and a binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``View``                                |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageByte Bin(ViewLocatorByte source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorByte``                     |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageUInt16 Bin(ViewLocatorUInt16 source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorUInt16``                   |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageUInt32 Bin(ViewLocatorUInt32 source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorUInt32``                   |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageDouble Bin(ViewLocatorDouble source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorDouble``                   |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageRgbByte Bin(ViewLocatorRgbByte source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorRgbByte``                  |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageRgbUInt16 Bin(ViewLocatorRgbUInt16 source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorRgbUInt16``                |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageRgbUInt32 Bin(ViewLocatorRgbUInt32 source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorRgbUInt32``                |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageRgbDouble Bin(ViewLocatorRgbDouble source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorRgbDouble``                |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageRgbaByte Bin(ViewLocatorRgbaByte source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorRgbaByte``                 |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageRgbaUInt16 Bin(ViewLocatorRgbaUInt16 source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorRgbaUInt16``               |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageRgbaUInt32 Bin(ViewLocatorRgbaUInt32 source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorRgbaUInt32``               |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageRgbaDouble Bin(ViewLocatorRgbaDouble source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorRgbaDouble``               |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageHlsByte Bin(ViewLocatorHlsByte source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorHlsByte``                  |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageHlsUInt16 Bin(ViewLocatorHlsUInt16 source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorHlsUInt16``                |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageHlsDouble Bin(ViewLocatorHlsDouble source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorHlsDouble``                |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageHsiByte Bin(ViewLocatorHsiByte source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorHsiByte``                  |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageHsiUInt16 Bin(ViewLocatorHsiUInt16 source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorHsiUInt16``                |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageHsiDouble Bin(ViewLocatorHsiDouble source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorHsiDouble``                |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageLabByte Bin(ViewLocatorLabByte source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorLabByte``                  |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageLabUInt16 Bin(ViewLocatorLabUInt16 source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorLabUInt16``                |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageLabDouble Bin(ViewLocatorLabDouble source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorLabDouble``                |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageXyzByte Bin(ViewLocatorXyzByte source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorXyzByte``                  |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageXyzUInt16 Bin(ViewLocatorXyzUInt16 source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorXyzUInt16``                |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``ImageXyzDouble Bin(ViewLocatorXyzDouble source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``ViewLocatorXyzDouble``                |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Method *Bin*
^^^^^^^^^^^^

``Image Bin(View source, Region region, BinningAlgorithms.BinningFunction binning, System.Int32 xFactor, System.Int32 yFactor, System.Int32 zFactor)``

This function reads values from the source view, bins them according to binning factors and the binning function and writes them to the destination buffer.

The method **Bin** has the following parameters:

+---------------+-----------------------------------------+---------------+
| Parameter     | Type                                    | Description   |
+===============+=========================================+===============+
| ``source``    | ``View``                                |               |
+---------------+-----------------------------------------+---------------+
| ``region``    | ``Region``                              |               |
+---------------+-----------------------------------------+---------------+
| ``binning``   | ``BinningAlgorithms.BinningFunction``   |               |
+---------------+-----------------------------------------+---------------+
| ``xFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``yFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+
| ``zFactor``   | ``System.Int32``                        |               |
+---------------+-----------------------------------------+---------------+

The region parameter constrains the function to the region inside of the view only.

The destination buffer size is determined by the size of the source view, taking the different binning factors into account. For example, if the horizontal binning factor is 2 then the source view width is twice as big as the destination buffer width.

The three factors for binning control the binning in the three dimensions. The factors can be combined to bin in more than one dimension at the same time. If all three factors are set to 1, this function will default to a mere copy.

The algorithm supports parallel execution on multiple cores.

/return A binned buffer.

Enumerations
~~~~~~~~~~~~

Enumeration *BinningFunction*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum BinningFunction``

TODO documentation missing

The enumeration **BinningFunction** has the following constants:

+------------------------+---------+---------------+
| Name                   | Value   | Description   |
+========================+=========+===============+
| ``bfArithmeticMean``   | ``0``   |               |
+------------------------+---------+---------------+
| ``bfGeometricMean``    | ``1``   |               |
+------------------------+---------+---------------+
| ``bfHarmonicMean``     | ``2``   |               |
+------------------------+---------+---------------+
| ``bfQuadraticMean``    | ``3``   |               |
+------------------------+---------+---------------+
| ``bfMinimum``          | ``4``   |               |
+------------------------+---------+---------------+
| ``bfMaximum``          | ``5``   |               |
+------------------------+---------+---------------+
| ``bfMidrange``         | ``6``   |               |
+------------------------+---------+---------------+
| ``bfMedian``           | ``7``   |               |
+------------------------+---------+---------------+

::

    enum BinningFunction
    {
      bfArithmeticMean = 0,
      bfGeometricMean = 1,
      bfHarmonicMean = 2,
      bfQuadraticMean = 3,
      bfMinimum = 4,
      bfMaximum = 5,
      bfMidrange = 6,
      bfMedian = 7,
    };

TODO documentation missing
