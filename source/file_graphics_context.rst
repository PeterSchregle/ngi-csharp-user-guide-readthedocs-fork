Class *GraphicsContext*
-----------------------

The graphics context.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **GraphicsContext** contains the following properties:

+---------------------+-------+-------+---------------------------------------+
| Property            | Get   | Set   | Description                           |
+=====================+=======+=======+=======================================+
| ``Window``          | \*    |       | The window handle.                    |
+---------------------+-------+-------+---------------------------------------+
| ``Update``          | \*    |       | The update rectangle of the widget.   |
+---------------------+-------+-------+---------------------------------------+
| ``RenderedImage``   | \*    |       | The rendered image.                   |
+---------------------+-------+-------+---------------------------------------+

The class **GraphicsContext** contains the following methods:

+--------------------------+----------------------------------------------------------------+
| Method                   | Description                                                    |
+==========================+================================================================+
| ``Reset``                | Reset the graphics context.                                    |
+--------------------------+----------------------------------------------------------------+
| ``Clear``                | Clear the background to the specified color.                   |
+--------------------------+----------------------------------------------------------------+
| ``BeginScene``           | Signal that rendering is about to start.                       |
+--------------------------+----------------------------------------------------------------+
| ``EndScene``             | Signal that rendering has finished.                            |
+--------------------------+----------------------------------------------------------------+
| ``DrawPoint``            | Draw a point.                                                  |
+--------------------------+----------------------------------------------------------------+
| ``DrawDecoratedPoint``   | Draw a point in a decorated fashion.                           |
+--------------------------+----------------------------------------------------------------+
| ``DrawLine``             | Draw a line.                                                   |
+--------------------------+----------------------------------------------------------------+
| ``DrawRay``              | Draw a ray.                                                    |
+--------------------------+----------------------------------------------------------------+
| ``DrawLineSegment``      | Draw a line segment.                                           |
+--------------------------+----------------------------------------------------------------+
| ``DrawPolyline``         | Draw a polyline.                                               |
+--------------------------+----------------------------------------------------------------+
| ``DrawArc``              | Draw an arc.                                                   |
+--------------------------+----------------------------------------------------------------+
| ``DrawEllipticalArc``    | Draw an elliptical arc.                                        |
+--------------------------+----------------------------------------------------------------+
| ``DrawPolygon``          | Draw a polygon outline.                                        |
+--------------------------+----------------------------------------------------------------+
| ``FillPolygon``          | Draw a polygon interior.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawTriangle``         | Draw a triangle outline.                                       |
+--------------------------+----------------------------------------------------------------+
| ``FillTriangle``         | Draw a triangle interior.                                      |
+--------------------------+----------------------------------------------------------------+
| ``DrawRectangle``        | Draw a rectangle outline.                                      |
+--------------------------+----------------------------------------------------------------+
| ``FillRectangle``        | Fill a rectangle interior.                                     |
+--------------------------+----------------------------------------------------------------+
| ``DrawQuadrilateral``    | Draw a quadrilateral outline.                                  |
+--------------------------+----------------------------------------------------------------+
| ``FillQuadrilateral``    | Draw a quadrilateral interior.                                 |
+--------------------------+----------------------------------------------------------------+
| ``DrawOrthogonalGrid``   | Draw a rectangular grid.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawPolarGrid``        | Draw a polar grid.                                             |
+--------------------------+----------------------------------------------------------------+
| ``DrawBox``              | Draw a box outline.                                            |
+--------------------------+----------------------------------------------------------------+
| ``FillBox``              | Fill a box interior.                                           |
+--------------------------+----------------------------------------------------------------+
| ``DrawCircle``           | Draw a circle outline.                                         |
+--------------------------+----------------------------------------------------------------+
| ``FillCircle``           | Fill a circle interior.                                        |
+--------------------------+----------------------------------------------------------------+
| ``DrawEllipse``          | Draw an ellipse outline.                                       |
+--------------------------+----------------------------------------------------------------+
| ``FillEllipse``          | Fill an ellipse interior.                                      |
+--------------------------+----------------------------------------------------------------+
| ``DrawText``             | Draw a text string.                                            |
+--------------------------+----------------------------------------------------------------+
| ``MeasureText``          |                                                                |
+--------------------------+----------------------------------------------------------------+
| ``FillRegionList``       | Draw a list of regions.                                        |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawView``             | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``DrawViewScaled``       | Draw a view as an image.                                       |
+--------------------------+----------------------------------------------------------------+
| ``GetTransform``         | Get the transformation matrix set into this graphics object.   |
+--------------------------+----------------------------------------------------------------+
| ``SetTransform``         | Set the transformation matrix for this graphics object.        |
+--------------------------+----------------------------------------------------------------+

The class **GraphicsContext** contains the following enumerations:

+---------------------+------------------------------+
| Enumeration         | Description                  |
+=====================+==============================+
| ``TextAlignment``   | TODO documentation missing   |
+---------------------+------------------------------+

Description
~~~~~~~~~~~

The graphics context bundles all graphics state and operations into one object.

Constructors
~~~~~~~~~~~~

Constructor *GraphicsContext*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``GraphicsContext(System.IntPtr window, VectorInt32 size)``

Construct the graphics context from a window.

The constructor has the following parameters:

+--------------+---------------------+---------------+
| Parameter    | Type                | Description   |
+==============+=====================+===============+
| ``window``   | ``System.IntPtr``   | The window.   |
+--------------+---------------------+---------------+
| ``size``     | ``VectorInt32``     | The size.     |
+--------------+---------------------+---------------+

Properties
~~~~~~~~~~

Property *Window*
^^^^^^^^^^^^^^^^^

``System.IntPtr Window``

The window handle.

Property *Update*
^^^^^^^^^^^^^^^^^

``BoxInt32 Update``

The update rectangle of the widget.

Property *RenderedImage*
^^^^^^^^^^^^^^^^^^^^^^^^

``ImageRgbByte RenderedImage``

The rendered image.

This member can only be called with a bitmap-related graphics\_context. It throws an exception if called with a HWND-related graphics\_context.

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Reset the graphics context.

Method *Clear*
^^^^^^^^^^^^^^

``void Clear(RgbByte background)``

Clear the background to the specified color.

The method **Clear** has the following parameters:

+------------------+---------------+-------------------------+
| Parameter        | Type          | Description             |
+==================+===============+=========================+
| ``background``   | ``RgbByte``   | The background color.   |
+------------------+---------------+-------------------------+

Method *BeginScene*
^^^^^^^^^^^^^^^^^^^

``void BeginScene()``

Signal that rendering is about to start.

Method *EndScene*
^^^^^^^^^^^^^^^^^

``void EndScene()``

Signal that rendering has finished.

Method *DrawPoint*
^^^^^^^^^^^^^^^^^^

``void DrawPoint(PenByte fill, PointDouble location)``

Draw a point.

The method **DrawPoint** has the following parameters:

+----------------+-------------------+---------------+
| Parameter      | Type              | Description   |
+================+===================+===============+
| ``fill``       | ``PenByte``       |               |
+----------------+-------------------+---------------+
| ``location``   | ``PointDouble``   |               |
+----------------+-------------------+---------------+

A point is a geometric primitive that, like in it's mathematical sense, has a location in space but no extent.

It is drawn as a little filled circle. The size of the circle is defined by the width of the pen.get\_brush(). The color of the circle is defined by the pen color. The center of the circle is defined by the geometric primitive point.

Method *DrawDecoratedPoint*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void DrawDecoratedPoint(PenByte fill, PointDouble location, System.Int32 style, System.Double size, System.Double rotation)``

Draw a point in a decorated fashion.

The method **DrawDecoratedPoint** has the following parameters:

+----------------+---------------------+-----------------------------------------+
| Parameter      | Type                | Description                             |
+================+=====================+=========================================+
| ``fill``       | ``PenByte``         |                                         |
+----------------+---------------------+-----------------------------------------+
| ``location``   | ``PointDouble``     |                                         |
+----------------+---------------------+-----------------------------------------+
| ``style``      | ``System.Int32``    | The style of the decoration.            |
+----------------+---------------------+-----------------------------------------+
| ``size``       | ``System.Double``   | The size of the decoration.             |
+----------------+---------------------+-----------------------------------------+
| ``rotation``   | ``System.Double``   | The rotation angle of the decoration.   |
+----------------+---------------------+-----------------------------------------+

A point is a geometric primitive that, like in it's mathematical sense, has a location in space but no extent.

It is drawn in various decorated styles, i.e. as a filled disk, a filled square, various arrows or bars. The decorated forms are those from the pen's line\_end\_style\_type enumeration.

Method *DrawLine*
^^^^^^^^^^^^^^^^^

``void DrawLine(PenByte contour, LineDouble location)``

Draw a line.

The method **DrawLine** has the following parameters:

+----------------+------------------+---------------+
| Parameter      | Type             | Description   |
+================+==================+===============+
| ``contour``    | ``PenByte``      |               |
+----------------+------------------+---------------+
| ``location``   | ``LineDouble``   |               |
+----------------+------------------+---------------+

A line is a geometric primitive that, like in it's mathematical sense, is defined by a point and a direction, and extends into infinity at both ends.

This function draws the line. The visual aspects of the line are defined by a pen, and the position of the line is defined by the geometric primitive line.

Method *DrawRay*
^^^^^^^^^^^^^^^^

``void DrawRay(PenByte contour, RayDouble location)``

Draw a ray.

The method **DrawRay** has the following parameters:

+----------------+-----------------+---------------+
| Parameter      | Type            | Description   |
+================+=================+===============+
| ``contour``    | ``PenByte``     |               |
+----------------+-----------------+---------------+
| ``location``   | ``RayDouble``   |               |
+----------------+-----------------+---------------+

A ray is a geometric primitive that is defined by a point and a direction, and extends into infinity in the positive direction.

This function draws the ray. The visual aspects of the ray are defined by a pen, and the position of the ray is defined by the geometric primitive ray.

Method *DrawLineSegment*
^^^^^^^^^^^^^^^^^^^^^^^^

``void DrawLineSegment(PenByte contour, LineSegmentDouble location)``

Draw a line segment.

The method **DrawLineSegment** has the following parameters:

+----------------+-------------------------+---------------+
| Parameter      | Type                    | Description   |
+================+=========================+===============+
| ``contour``    | ``PenByte``             |               |
+----------------+-------------------------+---------------+
| ``location``   | ``LineSegmentDouble``   |               |
+----------------+-------------------------+---------------+

A line\_segment is a geometric primitive that is defined by a point, a direction and an extent, and extends from p to p+d\*e.

This function draws the line segment. The visual aspects of the line segment are defined by a pen, and the position of the line segment is defined by the geometric primitive line\_segment.

Method *DrawPolyline*
^^^^^^^^^^^^^^^^^^^^^

``void DrawPolyline(PenByte contour, PolylineDouble location)``

Draw a polyline.

The method **DrawPolyline** has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``contour``    | ``PenByte``          |               |
+----------------+----------------------+---------------+
| ``location``   | ``PolylineDouble``   |               |
+----------------+----------------------+---------------+

A polyline is a geometric primitive that is defined by an ordered set of points and the line segments between them. It is open, i.e. there is no implicit line segment between the starting and the ending point.

This function draws the polyline. The visual aspects of the polyline are defined by a pen, and the position of the polyline is defined by the geometric primitive polyline.

Method *DrawArc*
^^^^^^^^^^^^^^^^

``void DrawArc(PenByte contour, Arc location)``

Draw an arc.

The method **DrawArc** has the following parameters:

+----------------+---------------+---------------+
| Parameter      | Type          | Description   |
+================+===============+===============+
| ``contour``    | ``PenByte``   |               |
+----------------+---------------+---------------+
| ``location``   | ``Arc``       |               |
+----------------+---------------+---------------+

An arc is a geometric primitive.

This function draws an arc. The visual aspects of the arc are defined by a pen, the position and shape of the arc is defined by the geometric primitive arc.

Method *DrawEllipticalArc*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void DrawEllipticalArc(PenByte contour, EllipticalArc location)``

Draw an elliptical arc.

The method **DrawEllipticalArc** has the following parameters:

+----------------+---------------------+---------------+
| Parameter      | Type                | Description   |
+================+=====================+===============+
| ``contour``    | ``PenByte``         |               |
+----------------+---------------------+---------------+
| ``location``   | ``EllipticalArc``   |               |
+----------------+---------------------+---------------+

An elliptical arc is a geometric primitive.

This function draws an elliptical arc. The visual aspects of the arc are defined by a pen, the position and shape of the arc is defined by the geometric primitive elliptical\_arc.

Method *DrawPolygon*
^^^^^^^^^^^^^^^^^^^^

``void DrawPolygon(PenByte contour, PolylineDouble location)``

Draw a polygon outline.

The method **DrawPolygon** has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``contour``    | ``PenByte``          |               |
+----------------+----------------------+---------------+
| ``location``   | ``PolylineDouble``   |               |
+----------------+----------------------+---------------+

A polygon is a geometric primitive that is defined by an ordered set of points and the line segments between them. It is closed, i.e. there is an implicit line segment between the starting and the ending point.

This function draws the polygon outline. The visual aspects of the polygon are defined by a pen, and the position of the polygon is defined by the geometric primitive polyline.

Method *FillPolygon*
^^^^^^^^^^^^^^^^^^^^

``void FillPolygon(SolidColorBrushByte interior, PolylineDouble location)``

Draw a polygon interior.

The method **FillPolygon** has the following parameters:

+----------------+---------------------------+---------------+
| Parameter      | Type                      | Description   |
+================+===========================+===============+
| ``interior``   | ``SolidColorBrushByte``   |               |
+----------------+---------------------------+---------------+
| ``location``   | ``PolylineDouble``        |               |
+----------------+---------------------------+---------------+

A polygon is a geometric primitive that is defined by an ordered set of points and the line segments between them. It is closed, i.e. there is an implicit line segment between the starting and the ending point.

This function draws the polygon interior. The visual aspects of the polygon are defined by a brush, and the position of the polygon is defined by the geometric primitive polyline.

Method *DrawTriangle*
^^^^^^^^^^^^^^^^^^^^^

``void DrawTriangle(PenByte contour, TriangleDouble location)``

Draw a triangle outline.

The method **DrawTriangle** has the following parameters:

+----------------+----------------------+---------------+
| Parameter      | Type                 | Description   |
+================+======================+===============+
| ``contour``    | ``PenByte``          |               |
+----------------+----------------------+---------------+
| ``location``   | ``TriangleDouble``   |               |
+----------------+----------------------+---------------+

A triangle is a geometric primitive that is defined by three points and the line segments between them. It is closed, i.e. there is an implicit line segment between the starting and the ending point.

This function draws the triangle outline. The visual aspects of the triangle are defined by a pen, and the position of the triangle is defined by the geometric primitive triangle.

Method *FillTriangle*
^^^^^^^^^^^^^^^^^^^^^

``void FillTriangle(SolidColorBrushByte interior, TriangleDouble location)``

Draw a triangle interior.

The method **FillTriangle** has the following parameters:

+----------------+---------------------------+---------------+
| Parameter      | Type                      | Description   |
+================+===========================+===============+
| ``interior``   | ``SolidColorBrushByte``   |               |
+----------------+---------------------------+---------------+
| ``location``   | ``TriangleDouble``        |               |
+----------------+---------------------------+---------------+

A triangle is a geometric primitive that is defined by three points and the line segments between them. It is closed, i.e. there is an implicit line segment between the starting and the ending point.

This function draws the triangle interior. The visual aspects of the triangle are defined by a brush, and the position of the triangle is defined by the geometric primitive triangle.

Method *DrawRectangle*
^^^^^^^^^^^^^^^^^^^^^^

``void DrawRectangle(PenByte contour, Rectangle location)``

Draw a rectangle outline.

The method **DrawRectangle** has the following parameters:

+----------------+-----------------+---------------+
| Parameter      | Type            | Description   |
+================+=================+===============+
| ``contour``    | ``PenByte``     |               |
+----------------+-----------------+---------------+
| ``location``   | ``Rectangle``   |               |
+----------------+-----------------+---------------+

A rectangle is a geometric primitive.

This function draws the rectangle outline. The visual aspects of the rectangle are defined by a pen, and the position of the rectangle is defined by the geometric primitive rectangle.

Method *FillRectangle*
^^^^^^^^^^^^^^^^^^^^^^

``void FillRectangle(SolidColorBrushByte interior, Rectangle location)``

Fill a rectangle interior.

The method **FillRectangle** has the following parameters:

+----------------+---------------------------+---------------+
| Parameter      | Type                      | Description   |
+================+===========================+===============+
| ``interior``   | ``SolidColorBrushByte``   |               |
+----------------+---------------------------+---------------+
| ``location``   | ``Rectangle``             |               |
+----------------+---------------------------+---------------+

A rectangle is a geometric primitive.

This function draws the rectangle outline. The visual aspects of the rectangle are defined by a brush, and the position of the rectangle is defined by the geometric primitive rectangle.

Method *DrawQuadrilateral*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void DrawQuadrilateral(PenByte contour, QuadrilateralDouble location)``

Draw a quadrilateral outline.

The method **DrawQuadrilateral** has the following parameters:

+----------------+---------------------------+---------------+
| Parameter      | Type                      | Description   |
+================+===========================+===============+
| ``contour``    | ``PenByte``               |               |
+----------------+---------------------------+---------------+
| ``location``   | ``QuadrilateralDouble``   |               |
+----------------+---------------------------+---------------+

A quadrilateral is a geometric primitive that is defined by four points and the line segments between them. It is closed, i.e. there is an implicit line segment between the starting and the ending point.

This function draws the quadrilateral outline. The visual aspects of the quadrilateral are defined by a pen, and the position of the quadrilateral is defined by the geometric primitive quadrilateral.

Method *FillQuadrilateral*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``void FillQuadrilateral(SolidColorBrushByte interior, QuadrilateralDouble location)``

Draw a quadrilateral interior.

The method **FillQuadrilateral** has the following parameters:

+----------------+---------------------------+---------------+
| Parameter      | Type                      | Description   |
+================+===========================+===============+
| ``interior``   | ``SolidColorBrushByte``   |               |
+----------------+---------------------------+---------------+
| ``location``   | ``QuadrilateralDouble``   |               |
+----------------+---------------------------+---------------+

A quadrilateral is a geometric primitive that is defined by four points and the line segments between them. It is closed, i.e. there is an implicit line segment between the starting and the ending point.

This function draws the quadrilateral interior. The visual aspects of the quadrilateral are defined by a brush, and the position of the quadrilateral is defined by the geometric primitive quadrilateral.

Method *DrawOrthogonalGrid*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void DrawOrthogonalGrid(PenByte contour, PointDouble origin, VectorDouble spacing)``

Draw a rectangular grid.

The method **DrawOrthogonalGrid** has the following parameters:

+--------------+------------+----------------------------------------------------+
| Parameter    | Type       | Description                                        |
+==============+============+====================================================+
| ``contour``  | ``PenByte` |                                                    |
|              | `          |                                                    |
+--------------+------------+----------------------------------------------------+
| ``origin``   | ``PointDou | The origin of the grid.                            |
|              | ble``      |                                                    |
+--------------+------------+----------------------------------------------------+
| ``spacing``  | ``VectorDo | The spacing of the grid. This function assumes     |
|              | uble``     | that both the horizontal and the vertical spacing  |
|              |            | are not zero and positive. If either is zero or    |
|              |            | negative the corresponding grid lines will not be  |
|              |            | drawn.                                             |
+--------------+------------+----------------------------------------------------+

A rectangular grid is defined by an origin and a spacing. It extends over the whole geometric plane.

Method *DrawPolarGrid*
^^^^^^^^^^^^^^^^^^^^^^

``void DrawPolarGrid(PenByte contour, PointDouble origin, VectorDouble spacing, System.Double angle, System.Int32 sectors)``

Draw a polar grid.

The method **DrawPolarGrid** has the following parameters:

+---------------+---------------------+---------------------------+
| Parameter     | Type                | Description               |
+===============+=====================+===========================+
| ``contour``   | ``PenByte``         |                           |
+---------------+---------------------+---------------------------+
| ``origin``    | ``PointDouble``     | The origin of the grid.   |
+---------------+---------------------+---------------------------+
| ``spacing``   | ``VectorDouble``    |                           |
+---------------+---------------------+---------------------------+
| ``angle``     | ``System.Double``   | The angle of the grid.    |
+---------------+---------------------+---------------------------+
| ``sectors``   | ``System.Int32``    | The number of sectors.    |
+---------------+---------------------+---------------------------+

A polar grid is defined by an origin, a radius, an angle and a number of sectors. It extends over the whole geometric plane.

Method *DrawBox*
^^^^^^^^^^^^^^^^

``void DrawBox(PenByte contour, BoxDouble location)``

Draw a box outline.

The method **DrawBox** has the following parameters:

+----------------+-----------------+---------------+
| Parameter      | Type            | Description   |
+================+=================+===============+
| ``contour``    | ``PenByte``     |               |
+----------------+-----------------+---------------+
| ``location``   | ``BoxDouble``   |               |
+----------------+-----------------+---------------+

A box is a geometric primitive that constitutes an axis-parallel rectangle.

This function draws the box outline. The visual aspects of the box are defined by a pen, and the position of the box is defined by the geometric primitive box.

Method *FillBox*
^^^^^^^^^^^^^^^^

``void FillBox(SolidColorBrushByte interior, BoxDouble location)``

Fill a box interior.

The method **FillBox** has the following parameters:

+----------------+---------------------------+---------------+
| Parameter      | Type                      | Description   |
+================+===========================+===============+
| ``interior``   | ``SolidColorBrushByte``   |               |
+----------------+---------------------------+---------------+
| ``location``   | ``BoxDouble``             |               |
+----------------+---------------------------+---------------+

A box is a geometric primitive that constitutes an axis-parallel rectangle.

This function draws the box interior. The visual aspects of the box are defined by a brush, and the position of the box is defined by the geometric primitive box.

Method *DrawCircle*
^^^^^^^^^^^^^^^^^^^

``void DrawCircle(PenByte contour, Circle location)``

Draw a circle outline.

The method **DrawCircle** has the following parameters:

+----------------+---------------+---------------+
| Parameter      | Type          | Description   |
+================+===============+===============+
| ``contour``    | ``PenByte``   |               |
+----------------+---------------+---------------+
| ``location``   | ``Circle``    |               |
+----------------+---------------+---------------+

A circle is a geometric primitive.

This function draws the circle outline. The visual aspects of the circle are defined by a pen, and the position of the circle is defined by the geometric primitive circle.

Method *FillCircle*
^^^^^^^^^^^^^^^^^^^

``void FillCircle(SolidColorBrushByte interior, Circle location)``

Fill a circle interior.

The method **FillCircle** has the following parameters:

+----------------+---------------------------+---------------+
| Parameter      | Type                      | Description   |
+================+===========================+===============+
| ``interior``   | ``SolidColorBrushByte``   |               |
+----------------+---------------------------+---------------+
| ``location``   | ``Circle``                |               |
+----------------+---------------------------+---------------+

A circle is a geometric primitive.

This function draws the circle interior. The visual aspects of the circle are defined by a brush, and the position of the circle is defined by the geometric primitive circle.

Method *DrawEllipse*
^^^^^^^^^^^^^^^^^^^^

``void DrawEllipse(PenByte contour, Ellipse location)``

Draw an ellipse outline.

The method **DrawEllipse** has the following parameters:

+----------------+---------------+---------------+
| Parameter      | Type          | Description   |
+================+===============+===============+
| ``contour``    | ``PenByte``   |               |
+----------------+---------------+---------------+
| ``location``   | ``Ellipse``   |               |
+----------------+---------------+---------------+

An ellipse is a geometric primitive.

This function draws the ellipse outline. The visual aspects of the ellipse are defined by a pen, and the position of the ellipse is defined by the geometric primitive ellipse.

Method *FillEllipse*
^^^^^^^^^^^^^^^^^^^^

``void FillEllipse(SolidColorBrushByte interior, Ellipse location)``

Fill an ellipse interior.

The method **FillEllipse** has the following parameters:

+----------------+---------------------------+---------------+
| Parameter      | Type                      | Description   |
+================+===========================+===============+
| ``interior``   | ``SolidColorBrushByte``   |               |
+----------------+---------------------------+---------------+
| ``location``   | ``Ellipse``               |               |
+----------------+---------------------------+---------------+

An ellipse is a geometric primitive.

This function draws the ellipse interior. The visual aspects of the ellipse are defined by a brush, and the position of the ellipse is defined by the geometric primitive ellipse.

Method *DrawText*
^^^^^^^^^^^^^^^^^

``void DrawText(SolidColorBrushByte textSolidColorBrush, System.String text, BoxDouble location, Font textFont, GraphicsContext.TextAlignment horizontalAlignment, GraphicsContext.TextAlignment verticalAlignment)``

Draw a text string.

The method **DrawText** has the following parameters:

+---------------------------+-------------------------------------+--------------------------------------------------------+
| Parameter                 | Type                                | Description                                            |
+===========================+=====================================+========================================================+
| ``textSolidColorBrush``   | ``SolidColorBrushByte``             |                                                        |
+---------------------------+-------------------------------------+--------------------------------------------------------+
| ``text``                  | ``System.String``                   | The text to be drawn.                                  |
+---------------------------+-------------------------------------+--------------------------------------------------------+
| ``location``              | ``BoxDouble``                       |                                                        |
+---------------------------+-------------------------------------+--------------------------------------------------------+
| ``textFont``              | ``Font``                            |                                                        |
+---------------------------+-------------------------------------+--------------------------------------------------------+
| ``horizontalAlignment``   | ``GraphicsContext.TextAlignment``   | The horizontal alignment of the text within the box.   |
+---------------------------+-------------------------------------+--------------------------------------------------------+
| ``verticalAlignment``     | ``GraphicsContext.TextAlignment``   | The vertical alignment of the text within the box.     |
+---------------------------+-------------------------------------+--------------------------------------------------------+

This function draws the text. The visual aspects of the text are defined by a font and by a brush. The position of the text is defined by a box and by two horizontal and vertical alignment flags.

Method *MeasureText*
^^^^^^^^^^^^^^^^^^^^

``VectorDouble MeasureText(System.String text, Font textFont)``

The method **MeasureText** has the following parameters:

+----------------+---------------------+---------------+
| Parameter      | Type                | Description   |
+================+=====================+===============+
| ``text``       | ``System.String``   |               |
+----------------+---------------------+---------------+
| ``textFont``   | ``Font``            |               |
+----------------+---------------------+---------------+

Method *FillRegionList*
^^^^^^^^^^^^^^^^^^^^^^^

``void FillRegionList(RegionList regions, ColorListRgbaByte colors, System.Double alpha)``

Draw a list of regions.

The method **FillRegionList** has the following parameters:

+---------------+-------------------------+------------------------+
| Parameter     | Type                    | Description            |
+===============+=========================+========================+
| ``regions``   | ``RegionList``          | The list of regions.   |
+---------------+-------------------------+------------------------+
| ``colors``    | ``ColorListRgbaByte``   | The list of colors.    |
+---------------+-------------------------+------------------------+
| ``alpha``     | ``System.Double``       | Overall alpha.         |
+---------------+-------------------------+------------------------+

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(ViewLocatorByte pixels, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+-----------------------+--------------------------------------------+
| Parameter      | Type                  | Description                                |
+================+=======================+============================================+
| ``pixels``     | ``ViewLocatorByte``   | The pixels.                                |
+----------------+-----------------------+--------------------------------------------+
| ``position``   | ``PointDouble``       | The position where to put the buffer to.   |
+----------------+-----------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``     | Overall alpha.                             |
+----------------+-----------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(ViewLocatorUInt16 pixels, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+-------------------------+--------------------------------------------+
| Parameter      | Type                    | Description                                |
+================+=========================+============================================+
| ``pixels``     | ``ViewLocatorUInt16``   | The pixels.                                |
+----------------+-------------------------+--------------------------------------------+
| ``position``   | ``PointDouble``         | The position where to put the buffer to.   |
+----------------+-------------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``       | Overall alpha.                             |
+----------------+-------------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(ViewLocatorUInt32 pixels, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+-------------------------+--------------------------------------------+
| Parameter      | Type                    | Description                                |
+================+=========================+============================================+
| ``pixels``     | ``ViewLocatorUInt32``   | The pixels.                                |
+----------------+-------------------------+--------------------------------------------+
| ``position``   | ``PointDouble``         | The position where to put the buffer to.   |
+----------------+-------------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``       | Overall alpha.                             |
+----------------+-------------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(ViewLocatorDouble pixels, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+-------------------------+--------------------------------------------+
| Parameter      | Type                    | Description                                |
+================+=========================+============================================+
| ``pixels``     | ``ViewLocatorDouble``   | The pixels.                                |
+----------------+-------------------------+--------------------------------------------+
| ``position``   | ``PointDouble``         | The position where to put the buffer to.   |
+----------------+-------------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``       | Overall alpha.                             |
+----------------+-------------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(ViewLocatorRgbByte pixels, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+--------------------------+--------------------------------------------+
| Parameter      | Type                     | Description                                |
+================+==========================+============================================+
| ``pixels``     | ``ViewLocatorRgbByte``   | The pixels.                                |
+----------------+--------------------------+--------------------------------------------+
| ``position``   | ``PointDouble``          | The position where to put the buffer to.   |
+----------------+--------------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``        | Overall alpha.                             |
+----------------+--------------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(ViewLocatorRgbUInt16 pixels, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+----------------------------+--------------------------------------------+
| Parameter      | Type                       | Description                                |
+================+============================+============================================+
| ``pixels``     | ``ViewLocatorRgbUInt16``   | The pixels.                                |
+----------------+----------------------------+--------------------------------------------+
| ``position``   | ``PointDouble``            | The position where to put the buffer to.   |
+----------------+----------------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``          | Overall alpha.                             |
+----------------+----------------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(ViewLocatorRgbUInt32 pixels, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+----------------------------+--------------------------------------------+
| Parameter      | Type                       | Description                                |
+================+============================+============================================+
| ``pixels``     | ``ViewLocatorRgbUInt32``   | The pixels.                                |
+----------------+----------------------------+--------------------------------------------+
| ``position``   | ``PointDouble``            | The position where to put the buffer to.   |
+----------------+----------------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``          | Overall alpha.                             |
+----------------+----------------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(ViewLocatorRgbDouble pixels, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+----------------------------+--------------------------------------------+
| Parameter      | Type                       | Description                                |
+================+============================+============================================+
| ``pixels``     | ``ViewLocatorRgbDouble``   | The pixels.                                |
+----------------+----------------------------+--------------------------------------------+
| ``position``   | ``PointDouble``            | The position where to put the buffer to.   |
+----------------+----------------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``          | Overall alpha.                             |
+----------------+----------------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(ViewLocatorRgbaByte pixels, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+---------------------------+--------------------------------------------+
| Parameter      | Type                      | Description                                |
+================+===========================+============================================+
| ``pixels``     | ``ViewLocatorRgbaByte``   | The pixels.                                |
+----------------+---------------------------+--------------------------------------------+
| ``position``   | ``PointDouble``           | The position where to put the buffer to.   |
+----------------+---------------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``         | Overall alpha.                             |
+----------------+---------------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(ViewLocatorRgbaUInt16 pixels, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+-----------------------------+--------------------------------------------+
| Parameter      | Type                        | Description                                |
+================+=============================+============================================+
| ``pixels``     | ``ViewLocatorRgbaUInt16``   | The pixels.                                |
+----------------+-----------------------------+--------------------------------------------+
| ``position``   | ``PointDouble``             | The position where to put the buffer to.   |
+----------------+-----------------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``           | Overall alpha.                             |
+----------------+-----------------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(ViewLocatorRgbaUInt32 pixels, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+-----------------------------+--------------------------------------------+
| Parameter      | Type                        | Description                                |
+================+=============================+============================================+
| ``pixels``     | ``ViewLocatorRgbaUInt32``   | The pixels.                                |
+----------------+-----------------------------+--------------------------------------------+
| ``position``   | ``PointDouble``             | The position where to put the buffer to.   |
+----------------+-----------------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``           | Overall alpha.                             |
+----------------+-----------------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(ViewLocatorRgbaDouble pixels, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+-----------------------------+--------------------------------------------+
| Parameter      | Type                        | Description                                |
+================+=============================+============================================+
| ``pixels``     | ``ViewLocatorRgbaDouble``   | The pixels.                                |
+----------------+-----------------------------+--------------------------------------------+
| ``position``   | ``PointDouble``             | The position where to put the buffer to.   |
+----------------+-----------------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``           | Overall alpha.                             |
+----------------+-----------------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(View pixels, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+---------------------+--------------------------------------------+
| Parameter      | Type                | Description                                |
+================+=====================+============================================+
| ``pixels``     | ``View``            | The pixels.                                |
+----------------+---------------------+--------------------------------------------+
| ``position``   | ``PointDouble``     | The position where to put the buffer to.   |
+----------------+---------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``   | Overall alpha.                             |
+----------------+---------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(ViewLocatorByte pixels, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+-----------------------+----------------------------------------------+
| Parameter      | Type                  | Description                                  |
+================+=======================+==============================================+
| ``pixels``     | ``ViewLocatorByte``   | The pixels.                                  |
+----------------+-----------------------+----------------------------------------------+
| ``position``   | ``PointDouble``       | The position where to put the buffer to.     |
+----------------+-----------------------+----------------------------------------------+
| ``offset``     | ``System.Double``     | The offset to be added to the pixel value.   |
+----------------+-----------------------+----------------------------------------------+
| ``scale``      | ``System.Double``     | The scale factor to be applied,              |
+----------------+-----------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``     | Overall alpha.                               |
+----------------+-----------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(ViewLocatorUInt16 pixels, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+-------------------------+----------------------------------------------+
| Parameter      | Type                    | Description                                  |
+================+=========================+==============================================+
| ``pixels``     | ``ViewLocatorUInt16``   | The pixels.                                  |
+----------------+-------------------------+----------------------------------------------+
| ``position``   | ``PointDouble``         | The position where to put the buffer to.     |
+----------------+-------------------------+----------------------------------------------+
| ``offset``     | ``System.Double``       | The offset to be added to the pixel value.   |
+----------------+-------------------------+----------------------------------------------+
| ``scale``      | ``System.Double``       | The scale factor to be applied,              |
+----------------+-------------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``       | Overall alpha.                               |
+----------------+-------------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(ViewLocatorUInt32 pixels, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+-------------------------+----------------------------------------------+
| Parameter      | Type                    | Description                                  |
+================+=========================+==============================================+
| ``pixels``     | ``ViewLocatorUInt32``   | The pixels.                                  |
+----------------+-------------------------+----------------------------------------------+
| ``position``   | ``PointDouble``         | The position where to put the buffer to.     |
+----------------+-------------------------+----------------------------------------------+
| ``offset``     | ``System.Double``       | The offset to be added to the pixel value.   |
+----------------+-------------------------+----------------------------------------------+
| ``scale``      | ``System.Double``       | The scale factor to be applied,              |
+----------------+-------------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``       | Overall alpha.                               |
+----------------+-------------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(ViewLocatorDouble pixels, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+-------------------------+----------------------------------------------+
| Parameter      | Type                    | Description                                  |
+================+=========================+==============================================+
| ``pixels``     | ``ViewLocatorDouble``   | The pixels.                                  |
+----------------+-------------------------+----------------------------------------------+
| ``position``   | ``PointDouble``         | The position where to put the buffer to.     |
+----------------+-------------------------+----------------------------------------------+
| ``offset``     | ``System.Double``       | The offset to be added to the pixel value.   |
+----------------+-------------------------+----------------------------------------------+
| ``scale``      | ``System.Double``       | The scale factor to be applied,              |
+----------------+-------------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``       | Overall alpha.                               |
+----------------+-------------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(ViewLocatorRgbByte pixels, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+--------------------------+----------------------------------------------+
| Parameter      | Type                     | Description                                  |
+================+==========================+==============================================+
| ``pixels``     | ``ViewLocatorRgbByte``   | The pixels.                                  |
+----------------+--------------------------+----------------------------------------------+
| ``position``   | ``PointDouble``          | The position where to put the buffer to.     |
+----------------+--------------------------+----------------------------------------------+
| ``offset``     | ``System.Double``        | The offset to be added to the pixel value.   |
+----------------+--------------------------+----------------------------------------------+
| ``scale``      | ``System.Double``        | The scale factor to be applied,              |
+----------------+--------------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``        | Overall alpha.                               |
+----------------+--------------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(ViewLocatorRgbUInt16 pixels, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+----------------------------+----------------------------------------------+
| Parameter      | Type                       | Description                                  |
+================+============================+==============================================+
| ``pixels``     | ``ViewLocatorRgbUInt16``   | The pixels.                                  |
+----------------+----------------------------+----------------------------------------------+
| ``position``   | ``PointDouble``            | The position where to put the buffer to.     |
+----------------+----------------------------+----------------------------------------------+
| ``offset``     | ``System.Double``          | The offset to be added to the pixel value.   |
+----------------+----------------------------+----------------------------------------------+
| ``scale``      | ``System.Double``          | The scale factor to be applied,              |
+----------------+----------------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``          | Overall alpha.                               |
+----------------+----------------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(ViewLocatorRgbUInt32 pixels, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+----------------------------+----------------------------------------------+
| Parameter      | Type                       | Description                                  |
+================+============================+==============================================+
| ``pixels``     | ``ViewLocatorRgbUInt32``   | The pixels.                                  |
+----------------+----------------------------+----------------------------------------------+
| ``position``   | ``PointDouble``            | The position where to put the buffer to.     |
+----------------+----------------------------+----------------------------------------------+
| ``offset``     | ``System.Double``          | The offset to be added to the pixel value.   |
+----------------+----------------------------+----------------------------------------------+
| ``scale``      | ``System.Double``          | The scale factor to be applied,              |
+----------------+----------------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``          | Overall alpha.                               |
+----------------+----------------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(ViewLocatorRgbDouble pixels, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+----------------------------+----------------------------------------------+
| Parameter      | Type                       | Description                                  |
+================+============================+==============================================+
| ``pixels``     | ``ViewLocatorRgbDouble``   | The pixels.                                  |
+----------------+----------------------------+----------------------------------------------+
| ``position``   | ``PointDouble``            | The position where to put the buffer to.     |
+----------------+----------------------------+----------------------------------------------+
| ``offset``     | ``System.Double``          | The offset to be added to the pixel value.   |
+----------------+----------------------------+----------------------------------------------+
| ``scale``      | ``System.Double``          | The scale factor to be applied,              |
+----------------+----------------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``          | Overall alpha.                               |
+----------------+----------------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(ViewLocatorRgbaByte pixels, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+---------------------------+----------------------------------------------+
| Parameter      | Type                      | Description                                  |
+================+===========================+==============================================+
| ``pixels``     | ``ViewLocatorRgbaByte``   | The pixels.                                  |
+----------------+---------------------------+----------------------------------------------+
| ``position``   | ``PointDouble``           | The position where to put the buffer to.     |
+----------------+---------------------------+----------------------------------------------+
| ``offset``     | ``System.Double``         | The offset to be added to the pixel value.   |
+----------------+---------------------------+----------------------------------------------+
| ``scale``      | ``System.Double``         | The scale factor to be applied,              |
+----------------+---------------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``         | Overall alpha.                               |
+----------------+---------------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(ViewLocatorRgbaUInt16 pixels, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+-----------------------------+----------------------------------------------+
| Parameter      | Type                        | Description                                  |
+================+=============================+==============================================+
| ``pixels``     | ``ViewLocatorRgbaUInt16``   | The pixels.                                  |
+----------------+-----------------------------+----------------------------------------------+
| ``position``   | ``PointDouble``             | The position where to put the buffer to.     |
+----------------+-----------------------------+----------------------------------------------+
| ``offset``     | ``System.Double``           | The offset to be added to the pixel value.   |
+----------------+-----------------------------+----------------------------------------------+
| ``scale``      | ``System.Double``           | The scale factor to be applied,              |
+----------------+-----------------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``           | Overall alpha.                               |
+----------------+-----------------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(ViewLocatorRgbaUInt32 pixels, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+-----------------------------+----------------------------------------------+
| Parameter      | Type                        | Description                                  |
+================+=============================+==============================================+
| ``pixels``     | ``ViewLocatorRgbaUInt32``   | The pixels.                                  |
+----------------+-----------------------------+----------------------------------------------+
| ``position``   | ``PointDouble``             | The position where to put the buffer to.     |
+----------------+-----------------------------+----------------------------------------------+
| ``offset``     | ``System.Double``           | The offset to be added to the pixel value.   |
+----------------+-----------------------------+----------------------------------------------+
| ``scale``      | ``System.Double``           | The scale factor to be applied,              |
+----------------+-----------------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``           | Overall alpha.                               |
+----------------+-----------------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(ViewLocatorRgbaDouble pixels, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+-----------------------------+----------------------------------------------+
| Parameter      | Type                        | Description                                  |
+================+=============================+==============================================+
| ``pixels``     | ``ViewLocatorRgbaDouble``   | The pixels.                                  |
+----------------+-----------------------------+----------------------------------------------+
| ``position``   | ``PointDouble``             | The position where to put the buffer to.     |
+----------------+-----------------------------+----------------------------------------------+
| ``offset``     | ``System.Double``           | The offset to be added to the pixel value.   |
+----------------+-----------------------------+----------------------------------------------+
| ``scale``      | ``System.Double``           | The scale factor to be applied,              |
+----------------+-----------------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``           | Overall alpha.                               |
+----------------+-----------------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(View pixels, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+---------------------+----------------------------------------------+
| Parameter      | Type                | Description                                  |
+================+=====================+==============================================+
| ``pixels``     | ``View``            | The pixels.                                  |
+----------------+---------------------+----------------------------------------------+
| ``position``   | ``PointDouble``     | The position where to put the buffer to.     |
+----------------+---------------------+----------------------------------------------+
| ``offset``     | ``System.Double``   | The offset to be added to the pixel value.   |
+----------------+---------------------+----------------------------------------------+
| ``scale``      | ``System.Double``   | The scale factor to be applied,              |
+----------------+---------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``   | Overall alpha.                               |
+----------------+---------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(ViewLocatorByte pixels, ViewLocatorRgbaByte palette, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+---------------------------+--------------------------------------------+
| Parameter      | Type                      | Description                                |
+================+===========================+============================================+
| ``pixels``     | ``ViewLocatorByte``       | The pixels.                                |
+----------------+---------------------------+--------------------------------------------+
| ``palette``    | ``ViewLocatorRgbaByte``   |                                            |
+----------------+---------------------------+--------------------------------------------+
| ``position``   | ``PointDouble``           | The position where to put the buffer to.   |
+----------------+---------------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``         | Overall alpha.                             |
+----------------+---------------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(ViewLocatorUInt16 pixels, ViewLocatorRgbaByte palette, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+---------------------------+--------------------------------------------+
| Parameter      | Type                      | Description                                |
+================+===========================+============================================+
| ``pixels``     | ``ViewLocatorUInt16``     | The pixels.                                |
+----------------+---------------------------+--------------------------------------------+
| ``palette``    | ``ViewLocatorRgbaByte``   |                                            |
+----------------+---------------------------+--------------------------------------------+
| ``position``   | ``PointDouble``           | The position where to put the buffer to.   |
+----------------+---------------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``         | Overall alpha.                             |
+----------------+---------------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(ViewLocatorUInt32 pixels, ViewLocatorRgbaByte palette, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+---------------------------+--------------------------------------------+
| Parameter      | Type                      | Description                                |
+================+===========================+============================================+
| ``pixels``     | ``ViewLocatorUInt32``     | The pixels.                                |
+----------------+---------------------------+--------------------------------------------+
| ``palette``    | ``ViewLocatorRgbaByte``   |                                            |
+----------------+---------------------------+--------------------------------------------+
| ``position``   | ``PointDouble``           | The position where to put the buffer to.   |
+----------------+---------------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``         | Overall alpha.                             |
+----------------+---------------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(ViewLocatorDouble pixels, ViewLocatorRgbaByte palette, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+---------------------------+--------------------------------------------+
| Parameter      | Type                      | Description                                |
+================+===========================+============================================+
| ``pixels``     | ``ViewLocatorDouble``     | The pixels.                                |
+----------------+---------------------------+--------------------------------------------+
| ``palette``    | ``ViewLocatorRgbaByte``   |                                            |
+----------------+---------------------------+--------------------------------------------+
| ``position``   | ``PointDouble``           | The position where to put the buffer to.   |
+----------------+---------------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``         | Overall alpha.                             |
+----------------+---------------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(ViewLocatorRgbByte pixels, ViewLocatorRgbaByte palette, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+---------------------------+--------------------------------------------+
| Parameter      | Type                      | Description                                |
+================+===========================+============================================+
| ``pixels``     | ``ViewLocatorRgbByte``    | The pixels.                                |
+----------------+---------------------------+--------------------------------------------+
| ``palette``    | ``ViewLocatorRgbaByte``   |                                            |
+----------------+---------------------------+--------------------------------------------+
| ``position``   | ``PointDouble``           | The position where to put the buffer to.   |
+----------------+---------------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``         | Overall alpha.                             |
+----------------+---------------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(ViewLocatorRgbUInt16 pixels, ViewLocatorRgbaByte palette, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+----------------------------+--------------------------------------------+
| Parameter      | Type                       | Description                                |
+================+============================+============================================+
| ``pixels``     | ``ViewLocatorRgbUInt16``   | The pixels.                                |
+----------------+----------------------------+--------------------------------------------+
| ``palette``    | ``ViewLocatorRgbaByte``    |                                            |
+----------------+----------------------------+--------------------------------------------+
| ``position``   | ``PointDouble``            | The position where to put the buffer to.   |
+----------------+----------------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``          | Overall alpha.                             |
+----------------+----------------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(ViewLocatorRgbUInt32 pixels, ViewLocatorRgbaByte palette, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+----------------------------+--------------------------------------------+
| Parameter      | Type                       | Description                                |
+================+============================+============================================+
| ``pixels``     | ``ViewLocatorRgbUInt32``   | The pixels.                                |
+----------------+----------------------------+--------------------------------------------+
| ``palette``    | ``ViewLocatorRgbaByte``    |                                            |
+----------------+----------------------------+--------------------------------------------+
| ``position``   | ``PointDouble``            | The position where to put the buffer to.   |
+----------------+----------------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``          | Overall alpha.                             |
+----------------+----------------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(ViewLocatorRgbDouble pixels, ViewLocatorRgbaByte palette, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+----------------------------+--------------------------------------------+
| Parameter      | Type                       | Description                                |
+================+============================+============================================+
| ``pixels``     | ``ViewLocatorRgbDouble``   | The pixels.                                |
+----------------+----------------------------+--------------------------------------------+
| ``palette``    | ``ViewLocatorRgbaByte``    |                                            |
+----------------+----------------------------+--------------------------------------------+
| ``position``   | ``PointDouble``            | The position where to put the buffer to.   |
+----------------+----------------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``          | Overall alpha.                             |
+----------------+----------------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(ViewLocatorRgbaByte pixels, ViewLocatorRgbaByte palette, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+---------------------------+--------------------------------------------+
| Parameter      | Type                      | Description                                |
+================+===========================+============================================+
| ``pixels``     | ``ViewLocatorRgbaByte``   | The pixels.                                |
+----------------+---------------------------+--------------------------------------------+
| ``palette``    | ``ViewLocatorRgbaByte``   |                                            |
+----------------+---------------------------+--------------------------------------------+
| ``position``   | ``PointDouble``           | The position where to put the buffer to.   |
+----------------+---------------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``         | Overall alpha.                             |
+----------------+---------------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(ViewLocatorRgbaUInt16 pixels, ViewLocatorRgbaByte palette, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+-----------------------------+--------------------------------------------+
| Parameter      | Type                        | Description                                |
+================+=============================+============================================+
| ``pixels``     | ``ViewLocatorRgbaUInt16``   | The pixels.                                |
+----------------+-----------------------------+--------------------------------------------+
| ``palette``    | ``ViewLocatorRgbaByte``     |                                            |
+----------------+-----------------------------+--------------------------------------------+
| ``position``   | ``PointDouble``             | The position where to put the buffer to.   |
+----------------+-----------------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``           | Overall alpha.                             |
+----------------+-----------------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(ViewLocatorRgbaUInt32 pixels, ViewLocatorRgbaByte palette, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+-----------------------------+--------------------------------------------+
| Parameter      | Type                        | Description                                |
+================+=============================+============================================+
| ``pixels``     | ``ViewLocatorRgbaUInt32``   | The pixels.                                |
+----------------+-----------------------------+--------------------------------------------+
| ``palette``    | ``ViewLocatorRgbaByte``     |                                            |
+----------------+-----------------------------+--------------------------------------------+
| ``position``   | ``PointDouble``             | The position where to put the buffer to.   |
+----------------+-----------------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``           | Overall alpha.                             |
+----------------+-----------------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(ViewLocatorRgbaDouble pixels, ViewLocatorRgbaByte palette, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+-----------------------------+--------------------------------------------+
| Parameter      | Type                        | Description                                |
+================+=============================+============================================+
| ``pixels``     | ``ViewLocatorRgbaDouble``   | The pixels.                                |
+----------------+-----------------------------+--------------------------------------------+
| ``palette``    | ``ViewLocatorRgbaByte``     |                                            |
+----------------+-----------------------------+--------------------------------------------+
| ``position``   | ``PointDouble``             | The position where to put the buffer to.   |
+----------------+-----------------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``           | Overall alpha.                             |
+----------------+-----------------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawView*
^^^^^^^^^^^^^^^^^

``void DrawView(View pixels, View palette, PointDouble position, System.Double alpha)``

Draw a view as an image.

The method **DrawView** has the following parameters:

+----------------+---------------------+--------------------------------------------+
| Parameter      | Type                | Description                                |
+================+=====================+============================================+
| ``pixels``     | ``View``            | The pixels.                                |
+----------------+---------------------+--------------------------------------------+
| ``palette``    | ``View``            |                                            |
+----------------+---------------------+--------------------------------------------+
| ``position``   | ``PointDouble``     | The position where to put the buffer to.   |
+----------------+---------------------+--------------------------------------------+
| ``alpha``      | ``System.Double``   | Overall alpha.                             |
+----------------+---------------------+--------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(ViewLocatorByte pixels, ViewLocatorRgbaByte palette, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+---------------------------+----------------------------------------------+
| Parameter      | Type                      | Description                                  |
+================+===========================+==============================================+
| ``pixels``     | ``ViewLocatorByte``       | The pixels.                                  |
+----------------+---------------------------+----------------------------------------------+
| ``palette``    | ``ViewLocatorRgbaByte``   |                                              |
+----------------+---------------------------+----------------------------------------------+
| ``position``   | ``PointDouble``           | The position where to put the buffer to.     |
+----------------+---------------------------+----------------------------------------------+
| ``offset``     | ``System.Double``         | The offset to be added to the pixel value.   |
+----------------+---------------------------+----------------------------------------------+
| ``scale``      | ``System.Double``         | The scale factor to be applied,              |
+----------------+---------------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``         | Overall alpha.                               |
+----------------+---------------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(ViewLocatorUInt16 pixels, ViewLocatorRgbaByte palette, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+---------------------------+----------------------------------------------+
| Parameter      | Type                      | Description                                  |
+================+===========================+==============================================+
| ``pixels``     | ``ViewLocatorUInt16``     | The pixels.                                  |
+----------------+---------------------------+----------------------------------------------+
| ``palette``    | ``ViewLocatorRgbaByte``   |                                              |
+----------------+---------------------------+----------------------------------------------+
| ``position``   | ``PointDouble``           | The position where to put the buffer to.     |
+----------------+---------------------------+----------------------------------------------+
| ``offset``     | ``System.Double``         | The offset to be added to the pixel value.   |
+----------------+---------------------------+----------------------------------------------+
| ``scale``      | ``System.Double``         | The scale factor to be applied,              |
+----------------+---------------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``         | Overall alpha.                               |
+----------------+---------------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(ViewLocatorUInt32 pixels, ViewLocatorRgbaByte palette, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+---------------------------+----------------------------------------------+
| Parameter      | Type                      | Description                                  |
+================+===========================+==============================================+
| ``pixels``     | ``ViewLocatorUInt32``     | The pixels.                                  |
+----------------+---------------------------+----------------------------------------------+
| ``palette``    | ``ViewLocatorRgbaByte``   |                                              |
+----------------+---------------------------+----------------------------------------------+
| ``position``   | ``PointDouble``           | The position where to put the buffer to.     |
+----------------+---------------------------+----------------------------------------------+
| ``offset``     | ``System.Double``         | The offset to be added to the pixel value.   |
+----------------+---------------------------+----------------------------------------------+
| ``scale``      | ``System.Double``         | The scale factor to be applied,              |
+----------------+---------------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``         | Overall alpha.                               |
+----------------+---------------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(ViewLocatorDouble pixels, ViewLocatorRgbaByte palette, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+---------------------------+----------------------------------------------+
| Parameter      | Type                      | Description                                  |
+================+===========================+==============================================+
| ``pixels``     | ``ViewLocatorDouble``     | The pixels.                                  |
+----------------+---------------------------+----------------------------------------------+
| ``palette``    | ``ViewLocatorRgbaByte``   |                                              |
+----------------+---------------------------+----------------------------------------------+
| ``position``   | ``PointDouble``           | The position where to put the buffer to.     |
+----------------+---------------------------+----------------------------------------------+
| ``offset``     | ``System.Double``         | The offset to be added to the pixel value.   |
+----------------+---------------------------+----------------------------------------------+
| ``scale``      | ``System.Double``         | The scale factor to be applied,              |
+----------------+---------------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``         | Overall alpha.                               |
+----------------+---------------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(ViewLocatorRgbByte pixels, ViewLocatorRgbaByte palette, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+---------------------------+----------------------------------------------+
| Parameter      | Type                      | Description                                  |
+================+===========================+==============================================+
| ``pixels``     | ``ViewLocatorRgbByte``    | The pixels.                                  |
+----------------+---------------------------+----------------------------------------------+
| ``palette``    | ``ViewLocatorRgbaByte``   |                                              |
+----------------+---------------------------+----------------------------------------------+
| ``position``   | ``PointDouble``           | The position where to put the buffer to.     |
+----------------+---------------------------+----------------------------------------------+
| ``offset``     | ``System.Double``         | The offset to be added to the pixel value.   |
+----------------+---------------------------+----------------------------------------------+
| ``scale``      | ``System.Double``         | The scale factor to be applied,              |
+----------------+---------------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``         | Overall alpha.                               |
+----------------+---------------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(ViewLocatorRgbUInt16 pixels, ViewLocatorRgbaByte palette, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+----------------------------+----------------------------------------------+
| Parameter      | Type                       | Description                                  |
+================+============================+==============================================+
| ``pixels``     | ``ViewLocatorRgbUInt16``   | The pixels.                                  |
+----------------+----------------------------+----------------------------------------------+
| ``palette``    | ``ViewLocatorRgbaByte``    |                                              |
+----------------+----------------------------+----------------------------------------------+
| ``position``   | ``PointDouble``            | The position where to put the buffer to.     |
+----------------+----------------------------+----------------------------------------------+
| ``offset``     | ``System.Double``          | The offset to be added to the pixel value.   |
+----------------+----------------------------+----------------------------------------------+
| ``scale``      | ``System.Double``          | The scale factor to be applied,              |
+----------------+----------------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``          | Overall alpha.                               |
+----------------+----------------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(ViewLocatorRgbUInt32 pixels, ViewLocatorRgbaByte palette, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+----------------------------+----------------------------------------------+
| Parameter      | Type                       | Description                                  |
+================+============================+==============================================+
| ``pixels``     | ``ViewLocatorRgbUInt32``   | The pixels.                                  |
+----------------+----------------------------+----------------------------------------------+
| ``palette``    | ``ViewLocatorRgbaByte``    |                                              |
+----------------+----------------------------+----------------------------------------------+
| ``position``   | ``PointDouble``            | The position where to put the buffer to.     |
+----------------+----------------------------+----------------------------------------------+
| ``offset``     | ``System.Double``          | The offset to be added to the pixel value.   |
+----------------+----------------------------+----------------------------------------------+
| ``scale``      | ``System.Double``          | The scale factor to be applied,              |
+----------------+----------------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``          | Overall alpha.                               |
+----------------+----------------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(ViewLocatorRgbDouble pixels, ViewLocatorRgbaByte palette, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+----------------------------+----------------------------------------------+
| Parameter      | Type                       | Description                                  |
+================+============================+==============================================+
| ``pixels``     | ``ViewLocatorRgbDouble``   | The pixels.                                  |
+----------------+----------------------------+----------------------------------------------+
| ``palette``    | ``ViewLocatorRgbaByte``    |                                              |
+----------------+----------------------------+----------------------------------------------+
| ``position``   | ``PointDouble``            | The position where to put the buffer to.     |
+----------------+----------------------------+----------------------------------------------+
| ``offset``     | ``System.Double``          | The offset to be added to the pixel value.   |
+----------------+----------------------------+----------------------------------------------+
| ``scale``      | ``System.Double``          | The scale factor to be applied,              |
+----------------+----------------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``          | Overall alpha.                               |
+----------------+----------------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(ViewLocatorRgbaByte pixels, ViewLocatorRgbaByte palette, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+---------------------------+----------------------------------------------+
| Parameter      | Type                      | Description                                  |
+================+===========================+==============================================+
| ``pixels``     | ``ViewLocatorRgbaByte``   | The pixels.                                  |
+----------------+---------------------------+----------------------------------------------+
| ``palette``    | ``ViewLocatorRgbaByte``   |                                              |
+----------------+---------------------------+----------------------------------------------+
| ``position``   | ``PointDouble``           | The position where to put the buffer to.     |
+----------------+---------------------------+----------------------------------------------+
| ``offset``     | ``System.Double``         | The offset to be added to the pixel value.   |
+----------------+---------------------------+----------------------------------------------+
| ``scale``      | ``System.Double``         | The scale factor to be applied,              |
+----------------+---------------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``         | Overall alpha.                               |
+----------------+---------------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(ViewLocatorRgbaUInt16 pixels, ViewLocatorRgbaByte palette, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+-----------------------------+----------------------------------------------+
| Parameter      | Type                        | Description                                  |
+================+=============================+==============================================+
| ``pixels``     | ``ViewLocatorRgbaUInt16``   | The pixels.                                  |
+----------------+-----------------------------+----------------------------------------------+
| ``palette``    | ``ViewLocatorRgbaByte``     |                                              |
+----------------+-----------------------------+----------------------------------------------+
| ``position``   | ``PointDouble``             | The position where to put the buffer to.     |
+----------------+-----------------------------+----------------------------------------------+
| ``offset``     | ``System.Double``           | The offset to be added to the pixel value.   |
+----------------+-----------------------------+----------------------------------------------+
| ``scale``      | ``System.Double``           | The scale factor to be applied,              |
+----------------+-----------------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``           | Overall alpha.                               |
+----------------+-----------------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(ViewLocatorRgbaUInt32 pixels, ViewLocatorRgbaByte palette, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+-----------------------------+----------------------------------------------+
| Parameter      | Type                        | Description                                  |
+================+=============================+==============================================+
| ``pixels``     | ``ViewLocatorRgbaUInt32``   | The pixels.                                  |
+----------------+-----------------------------+----------------------------------------------+
| ``palette``    | ``ViewLocatorRgbaByte``     |                                              |
+----------------+-----------------------------+----------------------------------------------+
| ``position``   | ``PointDouble``             | The position where to put the buffer to.     |
+----------------+-----------------------------+----------------------------------------------+
| ``offset``     | ``System.Double``           | The offset to be added to the pixel value.   |
+----------------+-----------------------------+----------------------------------------------+
| ``scale``      | ``System.Double``           | The scale factor to be applied,              |
+----------------+-----------------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``           | Overall alpha.                               |
+----------------+-----------------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(ViewLocatorRgbaDouble pixels, ViewLocatorRgbaByte palette, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+-----------------------------+----------------------------------------------+
| Parameter      | Type                        | Description                                  |
+================+=============================+==============================================+
| ``pixels``     | ``ViewLocatorRgbaDouble``   | The pixels.                                  |
+----------------+-----------------------------+----------------------------------------------+
| ``palette``    | ``ViewLocatorRgbaByte``     |                                              |
+----------------+-----------------------------+----------------------------------------------+
| ``position``   | ``PointDouble``             | The position where to put the buffer to.     |
+----------------+-----------------------------+----------------------------------------------+
| ``offset``     | ``System.Double``           | The offset to be added to the pixel value.   |
+----------------+-----------------------------+----------------------------------------------+
| ``scale``      | ``System.Double``           | The scale factor to be applied,              |
+----------------+-----------------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``           | Overall alpha.                               |
+----------------+-----------------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *DrawViewScaled*
^^^^^^^^^^^^^^^^^^^^^^^

``void DrawViewScaled(View pixels, View palette, PointDouble position, System.Double offset, System.Double scale, System.Double alpha)``

Draw a view as an image.

The method **DrawViewScaled** has the following parameters:

+----------------+---------------------+----------------------------------------------+
| Parameter      | Type                | Description                                  |
+================+=====================+==============================================+
| ``pixels``     | ``View``            | The pixels.                                  |
+----------------+---------------------+----------------------------------------------+
| ``palette``    | ``View``            |                                              |
+----------------+---------------------+----------------------------------------------+
| ``position``   | ``PointDouble``     | The position where to put the buffer to.     |
+----------------+---------------------+----------------------------------------------+
| ``offset``     | ``System.Double``   | The offset to be added to the pixel value.   |
+----------------+---------------------+----------------------------------------------+
| ``scale``      | ``System.Double``   | The scale factor to be applied,              |
+----------------+---------------------+----------------------------------------------+
| ``alpha``      | ``System.Double``   | Overall alpha.                               |
+----------------+---------------------+----------------------------------------------+

The function allocates a bitmap in 32 bit BGRA format. It copies the pixels from the source view into the bitmap, while adding an offset and scaling by scale, performing potential color space conversion on the fly, and draws the resulting bitmap to the screen at the specified position.

Method *GetTransform*
^^^^^^^^^^^^^^^^^^^^^

``AffineMatrix GetTransform()``

Get the transformation matrix set into this graphics object.

Method *SetTransform*
^^^^^^^^^^^^^^^^^^^^^

``void SetTransform(AffineMatrix transform)``

Set the transformation matrix for this graphics object.

The method **SetTransform** has the following parameters:

+-----------------+--------------------+---------------+
| Parameter       | Type               | Description   |
+=================+====================+===============+
| ``transform``   | ``AffineMatrix``   |               |
+-----------------+--------------------+---------------+

Enumerations
~~~~~~~~~~~~

Enumeration *TextAlignment*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum TextAlignment``

TODO documentation missing

The enumeration **TextAlignment** has the following constants:

+-------------------+----------+---------------+
| Name              | Value    | Description   |
+===================+==========+===============+
| ``alignNear``     | ``-1``   |               |
+-------------------+----------+---------------+
| ``alignCenter``   | ``0``    |               |
+-------------------+----------+---------------+
| ``alignFar``      | ``1``    |               |
+-------------------+----------+---------------+

::

    enum TextAlignment
    {
      alignNear = -1,
      alignCenter = 0,
      alignFar = 1,
    };

TODO documentation missing
