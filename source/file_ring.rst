Class *Ring*
------------

The geometric ring primitive.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Ring** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``IEquatableRing``           |
+------------------------------+
| ``ISerializable``            |
+------------------------------+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **Ring** contains the following properties:

+---------------------+-------+-------+-------------------------------------+
| Property            | Get   | Set   | Description                         |
+=====================+=======+=======+=====================================+
| ``Circle``          | \*    | \*    | The position of the ring.           |
+---------------------+-------+-------+-------------------------------------+
| ``Thickness``       | \*    | \*    | The thickness of the ring.          |
+---------------------+-------+-------+-------------------------------------+
| ``InnerCircle``     | \*    |       | The inner circle of the ring.       |
+---------------------+-------+-------+-------------------------------------+
| ``OuterCircle``     | \*    |       | The outer circle of the ring.       |
+---------------------+-------+-------+-------------------------------------+
| ``Area``            | \*    |       | The area of the ring.               |
+---------------------+-------+-------+-------------------------------------+
| ``Circumference``   | \*    |       | The circumference of the ring.      |
+---------------------+-------+-------+-------------------------------------+
| ``BoundingBox``     | \*    |       | Get the bounding box of the ring.   |
+---------------------+-------+-------+-------------------------------------+

The class **Ring** contains the following methods:

+------------------------+----------------------------------------------------------+
| Method                 | Description                                              |
+========================+==========================================================+
| ``Normalize``          | A normalized ring has a positive radius and thickness.   |
+------------------------+----------------------------------------------------------+
| ``NormalizeInPlace``   | A normalized ring has a positive radius and thickness.   |
+------------------------+----------------------------------------------------------+
| ``ToString``           | Provide string representation for debugging purposes.    |
+------------------------+----------------------------------------------------------+

Description
~~~~~~~~~~~

A ring can be seen as two circles with the same center but different radii.

The following operations are implemented: operator == : comparison for equality. operator != : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Ring*
^^^^^^^^^^^^^^^^^^

``Ring()``

Default constructor.

The default constructor builds a ring with its center at the origin and zero inner and outer radii.

Constructors
~~~~~~~~~~~~

Constructor *Ring*
^^^^^^^^^^^^^^^^^^

``Ring(Circle circle, System.Double thickness)``

Standard constructor.

The constructor has the following parameters:

+-----------------+---------------------+------------------+
| Parameter       | Type                | Description      |
+=================+=====================+==================+
| ``circle``      | ``Circle``          | The circle.      |
+-----------------+---------------------+------------------+
| ``thickness``   | ``System.Double``   | The thickness.   |
+-----------------+---------------------+------------------+

Construct a ring from its circle and thickness.

Properties
~~~~~~~~~~

Property *Circle*
^^^^^^^^^^^^^^^^^

``Circle Circle``

The position of the ring.

Property *Thickness*
^^^^^^^^^^^^^^^^^^^^

``System.Double Thickness``

The thickness of the ring.

Property *InnerCircle*
^^^^^^^^^^^^^^^^^^^^^^

``Circle InnerCircle``

The inner circle of the ring.

Property *OuterCircle*
^^^^^^^^^^^^^^^^^^^^^^

``Circle OuterCircle``

The outer circle of the ring.

Property *Area*
^^^^^^^^^^^^^^^

``System.Double Area``

The area of the ring.

Property *Circumference*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double Circumference``

The circumference of the ring.

Property *BoundingBox*
^^^^^^^^^^^^^^^^^^^^^^

``BoxDouble BoundingBox``

Get the bounding box of the ring.

The bounding box is an axis aligned box that bounds the ring.

The bounding box of the ring.

Methods
~~~~~~~

Method *Normalize*
^^^^^^^^^^^^^^^^^^

``Ring Normalize()``

A normalized ring has a positive radius and thickness.

This method checks these conditions and returns a normalized variant of the ring.

Method *NormalizeInPlace*
^^^^^^^^^^^^^^^^^^^^^^^^^

``void NormalizeInPlace()``

A normalized ring has a positive radius and thickness.

This method checks these conditions and returns a normalized variant of the ring. It modifies the ring in place.

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO no brief description for variant

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO no description for variant
