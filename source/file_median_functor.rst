Class *MedianFunctor*
---------------------

This function object can be used to calculate the median.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **MedianFunctor** contains the following variant parameters:

+------------+-----------------------------------------+
| Variant    | Description                             |
+============+=========================================+
| ``Type``   | TODO no brief description for variant   |
+------------+-----------------------------------------+

The class **MedianFunctor** contains the following properties:

+--------------+-------+-------+------------------------------+
| Property     | Get   | Set   | Description                  |
+==============+=======+=======+==============================+
| ``Count``    | \*    |       | The count of the elements.   |
+--------------+-------+-------+------------------------------+
| ``Median``   | \*    |       | The median.                  |
+--------------+-------+-------+------------------------------+

The class **MedianFunctor** contains the following methods:

+--------------------------+-----------------------------+
| Method                   | Description                 |
+==========================+=============================+
| ``ProcessNextElement``   | Process the next element.   |
+--------------------------+-----------------------------+

Description
~~~~~~~~~~~

Usually the function object is used in algorithms such as for\_each. You can use it both for algorithms in the STL as well as for algorithms in ngi.

This functor is independent of size.

Variants
~~~~~~~~

Variant *Type*
^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Type** has the following types:

+------------------+
| Type             |
+==================+
| ``Byte``         |
+------------------+
| ``UInt16``       |
+------------------+
| ``UInt32``       |
+------------------+
| ``Double``       |
+------------------+
| ``RgbByte``      |
+------------------+
| ``RgbUInt16``    |
+------------------+
| ``RgbUInt32``    |
+------------------+
| ``RgbDouble``    |
+------------------+
| ``RgbaByte``     |
+------------------+
| ``RgbaUInt16``   |
+------------------+
| ``RgbaUInt32``   |
+------------------+
| ``RgbaDouble``   |
+------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *MedianFunctor*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``MedianFunctor()``

Construct the functor.

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

The count of the elements.

Property *Median*
^^^^^^^^^^^^^^^^^

``System.Object Median``

The median.

Methods
~~~~~~~

Method *ProcessNextElement*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``void ProcessNextElement(System.Object element)``

Process the next element.

The method **ProcessNextElement** has the following parameters:

+---------------+---------------------+----------------------+
| Parameter     | Type                | Description          |
+===============+=====================+======================+
| ``element``   | ``System.Object``   | The element value.   |
+---------------+---------------------+----------------------+
