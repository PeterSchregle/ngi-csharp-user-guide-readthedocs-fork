Class *Mono*
------------

The mono color type.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Mono** contains the following variant parameters:

+-----------------+-----------------------------------------+
| Variant         | Description                             |
+=================+=========================================+
| ``Component``   | TODO no brief description for variant   |
+-----------------+-----------------------------------------+

The class **Mono** contains the following properties:

+------------+-------+-------+--------------------------------------+
| Property   | Get   | Set   | Description                          |
+============+=======+=======+======================================+
| ``Gray``   | \*    | \*    | The gray color component property.   |
+------------+-------+-------+--------------------------------------+

The class **Mono** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

The mono color type template allows to be instantiated for the basic integer and floating point types.

Physically, the color primitives are in gray - green - red order, to match the most obvious bitmap organization. Logically, however, the color primitives are in red - green - gray order. The logical order is reflected in the parameter order in methods.

This class does not provide an implicit conversion operator to the mono\_alpha type, but the other way round: the mono\_alpha type provides a conversion from the mono type, so that buffers of this type can be conveniently displayed.

Variants
~~~~~~~~

Variant *Component*
^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Component** has the following types:

+--------------+
| Type         |
+==============+
| ``Int8``     |
+--------------+
| ``Byte``     |
+--------------+
| ``Int16``    |
+--------------+
| ``UInt16``   |
+--------------+
| ``Int32``    |
+--------------+
| ``UInt32``   |
+--------------+
| ``Int64``    |
+--------------+
| ``UInt64``   |
+--------------+
| ``Single``   |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Mono*
^^^^^^^^^^^^^^^^^^

``Mono()``

Default constructor.

This constructor sets the gray component to 0. This corresponds to a color that is pure black.

Constructors
~~~~~~~~~~~~

Constructor *Mono*
^^^^^^^^^^^^^^^^^^

``Mono(System.Object gray)``

Standard constructor.

The constructor has the following parameters:

+-------------+---------------------+-----------------------+
| Parameter   | Type                | Description           |
+=============+=====================+=======================+
| ``gray``    | ``System.Object``   | The gray component.   |
+-------------+---------------------+-----------------------+

This constructor initializes the mono with a gray value.

The constructor also serves as a conversion operator from the scalar type T to type mono<T>.

Properties
~~~~~~~~~~

Property *Gray*
^^^^^^^^^^^^^^^

``System.Object Gray``

The gray color component property.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
