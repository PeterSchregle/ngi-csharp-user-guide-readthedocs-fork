Class *BoxEnumerator*
---------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **BoxEnumerator** implements the following interfaces:

+------------------------------------------+
| Interface                                |
+==========================================+
| ``IEnumerator``                          |
+------------------------------------------+
| ``IEnumeratorBoxEnumeratorBoxVariant``   |
+------------------------------------------+

The class **BoxEnumerator** contains the following variant parameters:

+-----------+-----------------------------------------+
| Variant   | Description                             |
+===========+=========================================+
| ``Box``   | TODO no brief description for variant   |
+-----------+-----------------------------------------+

The class **BoxEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **BoxEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *Box*
^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Box** has the following types:

+-----------------+
| Type            |
+=================+
| ``BoxInt32``    |
+-----------------+
| ``BoxDouble``   |
+-----------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Box Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
