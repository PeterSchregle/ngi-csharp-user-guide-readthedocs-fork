Class *Font*
------------

A drawing font.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Font** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``IEquatableFont``           |
+------------------------------+
| ``ISerializable``            |
+------------------------------+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **Font** contains the following properties:

+--------------+-------+-------+---------------------------+
| Property     | Get   | Set   | Description               |
+==============+=======+=======+===========================+
| ``Family``   | \*    | \*    | The family of the font.   |
+--------------+-------+-------+---------------------------+
| ``Size``     | \*    | \*    | The size of the font.     |
+--------------+-------+-------+---------------------------+
| ``Style``    | \*    | \*    | The style of the font.    |
+--------------+-------+-------+---------------------------+

The class **Font** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

The class **Font** contains the following enumerations:

+---------------------+------------------------------+
| Enumeration         | Description                  |
+=====================+==============================+
| ``FontStyleType``   | TODO documentation missing   |
+---------------------+------------------------------+

Description
~~~~~~~~~~~

A drawing font has a font family, point size and style. It is used to draw text.

The following operators are implemented for a font: operator == : comparison for equality. operator != : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Font*
^^^^^^^^^^^^^^^^^^

``Font()``

Standard constructor.

Constructors
~~~~~~~~~~~~

Constructor *Font*
^^^^^^^^^^^^^^^^^^

``Font(System.String family)``

Copy constructor.

The constructor has the following parameters:

+--------------+---------------------+---------------+
| Parameter    | Type                | Description   |
+==============+=====================+===============+
| ``family``   | ``System.String``   |               |
+--------------+---------------------+---------------+

Constructor *Font*
^^^^^^^^^^^^^^^^^^

``Font(System.String family, System.Double size)``

The constructor has the following parameters:

+--------------+---------------------+---------------+
| Parameter    | Type                | Description   |
+==============+=====================+===============+
| ``family``   | ``System.String``   |               |
+--------------+---------------------+---------------+
| ``size``     | ``System.Double``   |               |
+--------------+---------------------+---------------+

Constructor *Font*
^^^^^^^^^^^^^^^^^^

``Font(System.String family, System.Double size, Font.FontStyleType style)``

The constructor has the following parameters:

+--------------+--------------------------+---------------+
| Parameter    | Type                     | Description   |
+==============+==========================+===============+
| ``family``   | ``System.String``        |               |
+--------------+--------------------------+---------------+
| ``size``     | ``System.Double``        |               |
+--------------+--------------------------+---------------+
| ``style``    | ``Font.FontStyleType``   |               |
+--------------+--------------------------+---------------+

Properties
~~~~~~~~~~

Property *Family*
^^^^^^^^^^^^^^^^^

``System.String Family``

The family of the font.

Property *Size*
^^^^^^^^^^^^^^^

``System.Double Size``

The size of the font.

Property *Style*
^^^^^^^^^^^^^^^^

``Font.FontStyleType Style``

The style of the font.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Enumerations
~~~~~~~~~~~~

Enumeration *FontStyleType*
^^^^^^^^^^^^^^^^^^^^^^^^^^^

``enum FontStyleType``

TODO documentation missing

The enumeration **FontStyleType** has the following constants:

+-----------------+---------+---------------+
| Name            | Value   | Description   |
+=================+=========+===============+
| ``regular``     | ``0``   |               |
+-----------------+---------+---------------+
| ``bold``        | ``1``   |               |
+-----------------+---------+---------------+
| ``italic``      | ``2``   |               |
+-----------------+---------+---------------+
| ``underline``   | ``4``   |               |
+-----------------+---------+---------------+
| ``strikeout``   | ``8``   |               |
+-----------------+---------+---------------+

::

    enum FontStyleType
    {
      regular = 0,
      bold = 1,
      italic = 2,
      underline = 4,
      strikeout = 8,
    };

TODO documentation missing

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO no brief description for variant

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO no description for variant
