Class *Pose*
------------

A pose consists of a location and a rotation.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Pose** implements the following interfaces:

+----------------------+
| Interface            |
+======================+
| ``IEquatablePose``   |
+----------------------+
| ``ISerializable``    |
+----------------------+

The class **Pose** contains the following properties:

+-----------------+-------+-------+------------------------------------+
| Property        | Get   | Set   | Description                        |
+=================+=======+=======+====================================+
| ``Position``    | \*    |       | The positional part of the pose.   |
+-----------------+-------+-------+------------------------------------+
| ``Direction``   | \*    |       | The rotational part of the pose.   |
+-----------------+-------+-------+------------------------------------+
| ``Inverse``     | \*    |       | The inverse of the pose.           |
+-----------------+-------+-------+------------------------------------+

The class **Pose** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

It specifies a pose of an object in two-dimensional space.

A pose consists of a point, which specifies the location of a part, and a direction, which specifies the rotation of a part. A pose can be used to specify the components of a rigid transform.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Pose*
^^^^^^^^^^^^^^^^^^

``Pose()``

Default constructor.

This constructor initializes the zero pose (at origin and oriented horizontally).

Constructors
~~~~~~~~~~~~

Constructor *Pose*
^^^^^^^^^^^^^^^^^^

``Pose(PointDouble position, Direction direction)``

Construct a pose from a point and a direction.

The constructor has the following parameters:

+-----------------+-------------------+-----------------+
| Parameter       | Type              | Description     |
+=================+===================+=================+
| ``position``    | ``PointDouble``   | The position.   |
+-----------------+-------------------+-----------------+
| ``direction``   | ``Direction``     | The rotation.   |
+-----------------+-------------------+-----------------+

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``PointDouble Position``

The positional part of the pose.

Property *Direction*
^^^^^^^^^^^^^^^^^^^^

``Direction Direction``

The rotational part of the pose.

Property *Inverse*
^^^^^^^^^^^^^^^^^^

``Pose Inverse``

The inverse of the pose.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
