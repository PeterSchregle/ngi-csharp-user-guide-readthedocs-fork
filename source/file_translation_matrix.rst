Class *TranslationMatrix*
-------------------------

A matrix useful for 2D geometric translation.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **TranslationMatrix** implements the following interfaces:

+-----------------------------------+
| Interface                         |
+===================================+
| ``IEquatableTranslationMatrix``   |
+-----------------------------------+
| ``ISerializable``                 |
+-----------------------------------+

The class **TranslationMatrix** contains the following properties:

+-------------------+-------+-------+----------------------------------------------------+
| Property          | Get   | Set   | Description                                        |
+===================+=======+=======+====================================================+
| ``Translation``   | \*    | \*    | The translation vector of the matrix.              |
+-------------------+-------+-------+----------------------------------------------------+
| ``M11``           | \*    |       | The element m11 (row 1, column 1) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M12``           | \*    |       | The element m12 (row 1, column 2) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M13``           | \*    |       | The element m13 (row 1, column 3) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M21``           | \*    |       | The element m21 (row 2, column 1) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M22``           | \*    |       | The element m22 (row 2, column 2) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M23``           | \*    |       | The element m23 (row 2, column 3) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M31``           | \*    |       | The element m31 (row 3, column 1) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M32``           | \*    |       | The element m32 (row 3, column 2) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M33``           | \*    |       | The element m33 (row 3, column 3) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``[index]``       | \*    |       | The matrix values as a parameterized property.     |
+-------------------+-------+-------+----------------------------------------------------+
| ``Determinant``   | \*    |       | The determinant of the matrix.                     |
+-------------------+-------+-------+----------------------------------------------------+
| ``Trace``         | \*    |       | The trace of the matrix.                           |
+-------------------+-------+-------+----------------------------------------------------+
| ``Inverse``       | \*    |       | The inverse of the matrix.                         |
+-------------------+-------+-------+----------------------------------------------------+

The class **TranslationMatrix** contains the following methods:

+-----------------------+--------------------------------------------+
| Method                | Description                                |
+=======================+============================================+
| ``ResetToIdentity``   | Reset the matrix to the identity matrix.   |
+-----------------------+--------------------------------------------+
| ``Translate``         | Translate the matrix in place.             |
+-----------------------+--------------------------------------------+

Description
~~~~~~~~~~~

The values in the matrix are arranged like this:

\| \| \| m11 m12 m13 \|

.. raw:: html

   <table rows="7" cols="1">

1 0 translation\_.dx()

m21 m22 m23

0 1 translation\_.dy()

m31 m32 m33

0 0 1

.. raw:: html

   </table>

The first two rows and the last column is not stored within this object but implicit.

The determinant of such a matrix is always 1.

The trace of such a matrix is always 3.

The size is implicitely 3x3 and related properties are not implemented.

The inverse can be quickly calculated by negating the translation vector, and the result is still a translation matrix.

The transpose no longer would be a translation matrix and is therefore not implemented. If you need to calculate the transpose, you need to convert this matrix to a perspective\_matrix or a matrix and calculate the transpose there.

This storage assumes row-vectors and post-multiplication for transformations.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *TranslationMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TranslationMatrix()``

Default constructor.

By default this is the identity matrix.

Constructors
~~~~~~~~~~~~

Constructor *TranslationMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TranslationMatrix(VectorDouble translation)``

Constructor.

The constructor has the following parameters:

+-------------------+--------------------+---------------------------+
| Parameter         | Type               | Description               |
+===================+====================+===========================+
| ``translation``   | ``VectorDouble``   | The translation vector.   |
+-------------------+--------------------+---------------------------+

Constructs a translation matrix that translates by the given vector.

Constructor *TranslationMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``TranslationMatrix(System.Double dx, System.Double dy)``

Constructor.

The constructor has the following parameters:

+-------------+---------------------+-------------------------------------------------------+
| Parameter   | Type                | Description                                           |
+=============+=====================+=======================================================+
| ``dx``      | ``System.Double``   | The horizontal component of the translation vector.   |
+-------------+---------------------+-------------------------------------------------------+
| ``dy``      | ``System.Double``   | The vertical component of the translation vector.     |
+-------------+---------------------+-------------------------------------------------------+

Constructs a translation matrix that translates by the given vector.

Properties
~~~~~~~~~~

Property *Translation*
^^^^^^^^^^^^^^^^^^^^^^

``VectorDouble Translation``

The translation vector of the matrix.

Property *M11*
^^^^^^^^^^^^^^

``System.Double M11``

The element m11 (row 1, column 1) of the matrix.

Property *M12*
^^^^^^^^^^^^^^

``System.Double M12``

The element m12 (row 1, column 2) of the matrix.

Property *M13*
^^^^^^^^^^^^^^

``System.Double M13``

The element m13 (row 1, column 3) of the matrix.

Property *M21*
^^^^^^^^^^^^^^

``System.Double M21``

The element m21 (row 2, column 1) of the matrix.

Property *M22*
^^^^^^^^^^^^^^

``System.Double M22``

The element m22 (row 2, column 2) of the matrix.

Property *M23*
^^^^^^^^^^^^^^

``System.Double M23``

The element m23 (row 2, column 3) of the matrix.

Property *M31*
^^^^^^^^^^^^^^

``System.Double M31``

The element m31 (row 3, column 1) of the matrix.

Property *M32*
^^^^^^^^^^^^^^

``System.Double M32``

The element m32 (row 3, column 2) of the matrix.

Property *M33*
^^^^^^^^^^^^^^

``System.Double M33``

The element m33 (row 3, column 3) of the matrix.

Property *[index]*
^^^^^^^^^^^^^^^^^^

``System.Double [index]``

The matrix values as a parameterized property.

This property allows you to retrieve the implicit values also, but you cannot set them.

Property *Determinant*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double Determinant``

The determinant of the matrix.

Property *Trace*
^^^^^^^^^^^^^^^^

``System.Double Trace``

The trace of the matrix.

Property *Inverse*
^^^^^^^^^^^^^^^^^^

``TranslationMatrix Inverse``

The inverse of the matrix.

Static Methods
~~~~~~~~~~~~~~

Method *Multiply*
^^^^^^^^^^^^^^^^^

``TranslationMatrix Multiply(TranslationMatrix a, TranslationMatrix b)``

Multiply two matrices.

The method **Multiply** has the following parameters:

+-------------+-------------------------+---------------+
| Parameter   | Type                    | Description   |
+=============+=========================+===============+
| ``a``       | ``TranslationMatrix``   |               |
+-------------+-------------------------+---------------+
| ``b``       | ``TranslationMatrix``   |               |
+-------------+-------------------------+---------------+

The product.

Methods
~~~~~~~

Method *ResetToIdentity*
^^^^^^^^^^^^^^^^^^^^^^^^

``void ResetToIdentity()``

Reset the matrix to the identity matrix.

The identity matrix has the following form. \| 1 0 0 \| \| 0 1 0 \| \| 0 0 1 \|A reference to this object.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``void Translate(VectorDouble translation)``

Translate the matrix in place.

The method **Translate** has the following parameters:

+-------------------+--------------------+---------------------------+
| Parameter         | Type               | Description               |
+===================+====================+===========================+
| ``translation``   | ``VectorDouble``   | The translation vector.   |
+-------------------+--------------------+---------------------------+

Return a reference to the matrix.
