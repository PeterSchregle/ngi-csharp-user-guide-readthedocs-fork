Class *GeometryEnumerator*
--------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **GeometryEnumerator** implements the following interfaces:

+---------------------------+
| Interface                 |
+===========================+
| ``IEnumerator``           |
+---------------------------+
| ``IEnumeratorGeometry``   |
+---------------------------+

The class **GeometryEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **GeometryEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Geometry Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
