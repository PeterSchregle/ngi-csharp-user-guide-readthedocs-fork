Class *Geometry*
----------------

A geometry in two-dimensional euclidean space.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Geometry** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``IEquatableGeometry``       |
+------------------------------+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **Geometry** contains the following properties:

+-------------------+-------+-------+-----------------------------------------------------+
| Property          | Get   | Set   | Description                                         |
+===================+=======+=======+=====================================================+
| ``Elements``      | \*    |       | The vector of geometric elements in the geometry.   |
+-------------------+-------+-------+-----------------------------------------------------+
| ``IsClosed``      | \*    | \*    | Flag whether it is an open or closed geometry.      |
+-------------------+-------+-------+-----------------------------------------------------+
| ``BoundingBox``   | \*    |       | Get the bounding box of the geometry.               |
+-------------------+-------+-------+-----------------------------------------------------+

The class **Geometry** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

A geometry can be seen as sequence of connected line\_segments, arcs and elliptical arcs. The elements of a geometry are defined by points and associated geometric primitives.

The following operations are implemented: operator == : comparison for equality. operator != : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Geometry*
^^^^^^^^^^^^^^^^^^^^^^

``Geometry()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *Geometry*
^^^^^^^^^^^^^^^^^^^^^^

``Geometry(PointDouble point)``

Construct a geometry from a point.

The constructor has the following parameters:

+-------------+-------------------+-----------------------------------+
| Parameter   | Type              | Description                       |
+=============+===================+===================================+
| ``point``   | ``PointDouble``   | A const reference to the point.   |
+-------------+-------------------+-----------------------------------+

Properties
~~~~~~~~~~

Property *Elements*
^^^^^^^^^^^^^^^^^^^

``GeometryElementList Elements``

The vector of geometric elements in the geometry.

Property *IsClosed*
^^^^^^^^^^^^^^^^^^^

``System.Boolean IsClosed``

Flag whether it is an open or closed geometry.

Property *BoundingBox*
^^^^^^^^^^^^^^^^^^^^^^

``BoxDouble BoundingBox``

Get the bounding box of the geometry.

The bounding box is an axis aligned box that bounds the geometry The bounding box of the geometry.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO no brief description for variant

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO no description for variant
