Class *WidgetBaseList*
----------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetBaseList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+

The class **WidgetBaseList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetBaseList*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetBaseList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``WidgetBaseEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(WidgetBase item)``

The method **Add** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``item``    | ``WidgetBase``   |               |
+-------------+------------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(WidgetBase item)``

The method **Contains** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``item``    | ``WidgetBase``   |               |
+-------------+------------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(WidgetBase item)``

The method **Remove** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``item``    | ``WidgetBase``   |               |
+-------------+------------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(WidgetBase item)``

The method **IndexOf** has the following parameters:

+-------------+------------------+---------------+
| Parameter   | Type             | Description   |
+=============+==================+===============+
| ``item``    | ``WidgetBase``   |               |
+-------------+------------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, WidgetBase item)``

The method **Insert** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``item``    | ``WidgetBase``     |               |
+-------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
