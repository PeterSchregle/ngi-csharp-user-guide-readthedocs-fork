Class *FlusserMoments*
----------------------

Class to hold Flusser's moments.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **FlusserMoments** implements the following interfaces:

+--------------------------------+
| Interface                      |
+================================+
| ``IEquatableFlusserMoments``   |
+--------------------------------+
| ``ISerializable``              |
+--------------------------------+

The class **FlusserMoments** contains the following properties:

+------------+-------+-------+------------------------------+
| Property   | Get   | Set   | Description                  |
+============+=======+=======+==============================+
| ``I1``     | \*    |       | The first Flusser moment.    |
+------------+-------+-------+------------------------------+
| ``I2``     | \*    |       | The second Flusser moment.   |
+------------+-------+-------+------------------------------+
| ``I3``     | \*    |       | The third Flusser moment.    |
+------------+-------+-------+------------------------------+
| ``I4``     | \*    |       | The fourth Flusser moment.   |
+------------+-------+-------+------------------------------+

The class **FlusserMoments** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

For a nice and short treatment of flusser moments see Jan Flusser, Thomas Suk: Affine moment invariants: A new tool for character recognition.

Moments are a sort of weighted averages of the image pixels' intensities (the pixel intensities may be set to 1 in case of binary moments). They are useful to describe segmented objects. Simple properties of objects found via moments include area (or total intensity), its centroid, and information about its orientation.

The Flusser moments are invariant under affine transformations.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *FlusserMoments*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``FlusserMoments()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *FlusserMoments*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``FlusserMoments(CentralMoments mu)``

Calculates the Flusser moments from some scale invariant moments.

The constructor has the following parameters:

+-------------+----------------------+--------------------+
| Parameter   | Type                 | Description        |
+=============+======================+====================+
| ``mu``      | ``CentralMoments``   | Central moments.   |
+-------------+----------------------+--------------------+

Initializes the moments.

Properties
~~~~~~~~~~

Property *I1*
^^^^^^^^^^^^^

``System.Double I1``

The first Flusser moment.

Property *I2*
^^^^^^^^^^^^^

``System.Double I2``

The second Flusser moment.

Property *I3*
^^^^^^^^^^^^^

``System.Double I3``

The third Flusser moment.

Property *I4*
^^^^^^^^^^^^^

``System.Double I4``

The fourth Flusser moment.

Static Methods
~~~~~~~~~~~~~~

Method *CalculateI1*
^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateI1(CentralMoments mu)``

Calculate i1 from central moments.

The method **CalculateI1** has the following parameters:

+-------------+----------------------+---------------+
| Parameter   | Type                 | Description   |
+=============+======================+===============+
| ``mu``      | ``CentralMoments``   |               |
+-------------+----------------------+---------------+

i1 is the first Flusser moment.

First Flusser moment.

Method *CalculateI2*
^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateI2(CentralMoments mu)``

Calculate i2 from central moments.

The method **CalculateI2** has the following parameters:

+-------------+----------------------+---------------+
| Parameter   | Type                 | Description   |
+=============+======================+===============+
| ``mu``      | ``CentralMoments``   |               |
+-------------+----------------------+---------------+

i2 is the second Flusser moment.

Second Flusser moment.

Method *CalculateI3*
^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateI3(CentralMoments mu)``

Calculate i3 from central moments.

The method **CalculateI3** has the following parameters:

+-------------+----------------------+---------------+
| Parameter   | Type                 | Description   |
+=============+======================+===============+
| ``mu``      | ``CentralMoments``   |               |
+-------------+----------------------+---------------+

i3 is the third Flusser moment.

Third Flusser moment.

Method *CalculateI4*
^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateI4(CentralMoments mu)``

Calculate i4 from central moments.

The method **CalculateI4** has the following parameters:

+-------------+----------------------+---------------+
| Parameter   | Type                 | Description   |
+=============+======================+===============+
| ``mu``      | ``CentralMoments``   |               |
+-------------+----------------------+---------------+

i4 is the fourth Flusser moment.

Fourth Flusser moment.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
