Class *GeometryArc*
-------------------

The following operations are implemented: operator== : comparison for equality.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **GeometryArc** implements the following interfaces:

+------------------+
| Interface        |
+==================+
| ``IEquatable``   |
+------------------+

The class **GeometryArc** contains the following properties:

+--------------+-------+-------+------------------------------------+
| Property     | Get   | Set   | Description                        |
+==============+=======+=======+====================================+
| ``Radius``   | \*    | \*    | The radius of the arc.             |
+--------------+-------+-------+------------------------------------+
| ``Sweep``    | \*    | \*    | The drawing direction of an arc.   |
+--------------+-------+-------+------------------------------------+
| ``Size``     | \*    | \*    | The size of an arc.                |
+--------------+-------+-------+------------------------------------+

The class **GeometryArc** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+
| ``ToArc``      | Convert to a standalone arc.                            |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

operator!= : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *GeometryArc*
^^^^^^^^^^^^^^^^^^^^^^^^^

``GeometryArc()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *GeometryArc*
^^^^^^^^^^^^^^^^^^^^^^^^^

``GeometryArc(PointDouble point)``

Construct a default geometry element arc from a point.

The constructor has the following parameters:

+-------------+-------------------+---------------+
| Parameter   | Type              | Description   |
+=============+===================+===============+
| ``point``   | ``PointDouble``   |               |
+-------------+-------------------+---------------+

Constructor *GeometryArc*
^^^^^^^^^^^^^^^^^^^^^^^^^

``GeometryArc(PointDouble point, System.Double radius, Arc.SweepDirection sweep, Arc.ArcSize size)``

Construct a geometry element arc.

The constructor has the following parameters:

+--------------+--------------------------+------------------------+
| Parameter    | Type                     | Description            |
+==============+==========================+========================+
| ``point``    | ``PointDouble``          | The target point.      |
+--------------+--------------------------+------------------------+
| ``radius``   | ``System.Double``        | The radius.            |
+--------------+--------------------------+------------------------+
| ``sweep``    | ``Arc.SweepDirection``   | The sweep direction.   |
+--------------+--------------------------+------------------------+
| ``size``     | ``Arc.ArcSize``          | The arc size.          |
+--------------+--------------------------+------------------------+

Properties
~~~~~~~~~~

Property *Radius*
^^^^^^^^^^^^^^^^^

``System.Double Radius``

The radius of the arc.

Property *Sweep*
^^^^^^^^^^^^^^^^

``Arc.SweepDirection Sweep``

The drawing direction of an arc.

Property *Size*
^^^^^^^^^^^^^^^

``Arc.ArcSize Size``

The size of an arc.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Method *ToArc*
^^^^^^^^^^^^^^

``Arc ToArc(PointDouble startPoint)``

Convert to a standalone arc.

The method **ToArc** has the following parameters:

+------------------+-------------------+---------------+
| Parameter        | Type              | Description   |
+==================+===================+===============+
| ``startPoint``   | ``PointDouble``   |               |
+------------------+-------------------+---------------+
