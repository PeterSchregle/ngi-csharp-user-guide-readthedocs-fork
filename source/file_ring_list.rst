Class *RingList*
----------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **RingList** implements the following interfaces:

+-----------------+
| Interface       |
+=================+
| ``IListRing``   |
+-----------------+

The class **RingList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **RingList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *RingList*
^^^^^^^^^^^^^^^^^^^^^^

``RingList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``Ring [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``RingEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(Ring item)``

The method **Add** has the following parameters:

+-------------+------------+---------------+
| Parameter   | Type       | Description   |
+=============+============+===============+
| ``item``    | ``Ring``   |               |
+-------------+------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(Ring item)``

The method **Contains** has the following parameters:

+-------------+------------+---------------+
| Parameter   | Type       | Description   |
+=============+============+===============+
| ``item``    | ``Ring``   |               |
+-------------+------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(Ring item)``

The method **Remove** has the following parameters:

+-------------+------------+---------------+
| Parameter   | Type       | Description   |
+=============+============+===============+
| ``item``    | ``Ring``   |               |
+-------------+------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(Ring item)``

The method **IndexOf** has the following parameters:

+-------------+------------+---------------+
| Parameter   | Type       | Description   |
+=============+============+===============+
| ``item``    | ``Ring``   |               |
+-------------+------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, Ring item)``

The method **Insert** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+
| ``item``    | ``Ring``           |               |
+-------------+--------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
