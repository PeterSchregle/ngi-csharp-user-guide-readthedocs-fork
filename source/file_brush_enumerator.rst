Class *BrushEnumerator*
-----------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **BrushEnumerator** implements the following interfaces:

+--------------------------------------------------------+
| Interface                                              |
+========================================================+
| ``IEnumerator``                                        |
+--------------------------------------------------------+
| ``IEnumeratorBrushEnumeratorSolidColorBrushVariant``   |
+--------------------------------------------------------+

The class **BrushEnumerator** contains the following variant parameters:

+-----------------------+-----------------------------------------+
| Variant               | Description                             |
+=======================+=========================================+
| ``SolidColorBrush``   | TODO no brief description for variant   |
+-----------------------+-----------------------------------------+

The class **BrushEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **BrushEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Variants
~~~~~~~~

Variant *SolidColorBrush*
^^^^^^^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **SolidColorBrush** has the following types:

+-----------------------------+
| Type                        |
+=============================+
| ``SolidColorBrushByte``     |
+-----------------------------+
| ``SolidColorBrushUInt16``   |
+-----------------------------+
| ``SolidColorBrushUInt32``   |
+-----------------------------+
| ``SolidColorBrushDouble``   |
+-----------------------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``SolidColorBrush Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
