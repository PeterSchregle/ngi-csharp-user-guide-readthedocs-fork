Class *StringList*
------------------

A list of regions.

**Namespace:** Ngi

**Module:**

The class **StringList** implements the following interfaces:

+-------------------+
| Interface         |
+===================+
| ``IListString``   |
+-------------------+

The class **StringList** contains the following properties:

+-------------------+-------+-------+---------------+
| Property          | Get   | Set   | Description   |
+===================+=======+=======+===============+
| ``Count``         | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsFixedSize``   | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``IsReadOnly``    | \*    |       |               |
+-------------------+-------+-------+---------------+
| ``[index]``       | \*    | \*    |               |
+-------------------+-------+-------+---------------+

The class **StringList** contains the following methods:

+---------------------+---------------+
| Method              | Description   |
+=====================+===============+
| ``GetEnumerator``   |               |
+---------------------+---------------+
| ``Add``             |               |
+---------------------+---------------+
| ``Clear``           |               |
+---------------------+---------------+
| ``Contains``        |               |
+---------------------+---------------+
| ``Remove``          |               |
+---------------------+---------------+
| ``IndexOf``         |               |
+---------------------+---------------+
| ``Insert``          |               |
+---------------------+---------------+
| ``RemoveAt``        |               |
+---------------------+---------------+
| ``ToString``        |               |
+---------------------+---------------+

Description
~~~~~~~~~~~

It is a list.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *StringList*
^^^^^^^^^^^^^^^^^^^^^^^^

``StringList()``

Properties
~~~~~~~~~~

Property *Count*
^^^^^^^^^^^^^^^^

``System.Int32 Count``

Property *IsFixedSize*
^^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsFixedSize``

Property *IsReadOnly*
^^^^^^^^^^^^^^^^^^^^^

``System.Boolean IsReadOnly``

Property *[index]*
^^^^^^^^^^^^^^^^^^

``System.String [index]``

Methods
~~~~~~~

Method *GetEnumerator*
^^^^^^^^^^^^^^^^^^^^^^

``StringEnumerator GetEnumerator()``

Method *Add*
^^^^^^^^^^^^

``void Add(System.String item)``

The method **Add** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``item``    | ``System.String``   |               |
+-------------+---------------------+---------------+

Method *Clear*
^^^^^^^^^^^^^^

``void Clear()``

Method *Contains*
^^^^^^^^^^^^^^^^^

``System.Boolean Contains(System.String item)``

The method **Contains** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``item``    | ``System.String``   |               |
+-------------+---------------------+---------------+

Method *Remove*
^^^^^^^^^^^^^^^

``System.Boolean Remove(System.String item)``

The method **Remove** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``item``    | ``System.String``   |               |
+-------------+---------------------+---------------+

Method *IndexOf*
^^^^^^^^^^^^^^^^

``System.Int32 IndexOf(System.String item)``

The method **IndexOf** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``item``    | ``System.String``   |               |
+-------------+---------------------+---------------+

Method *Insert*
^^^^^^^^^^^^^^^

``void Insert(System.Int32 index, System.String item)``

The method **Insert** has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``index``   | ``System.Int32``    |               |
+-------------+---------------------+---------------+
| ``item``    | ``System.String``   |               |
+-------------+---------------------+---------------+

Method *RemoveAt*
^^^^^^^^^^^^^^^^^

``void RemoveAt(System.Int32 index)``

The method **RemoveAt** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``index``   | ``System.Int32``   |               |
+-------------+--------------------+---------------+

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
