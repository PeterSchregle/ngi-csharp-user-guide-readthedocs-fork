Class *Date*
------------

A date.

**Namespace:** Ngi

**Module:**

The class **Date** implements the following interfaces:

+----------------------+
| Interface            |
+======================+
| ``IEquatableDate``   |
+----------------------+
| ``ISerializable``    |
+----------------------+

The class **Date** contains the following properties:

+-------------+-------+-------+-------------------------------+
| Property    | Get   | Set   | Description                   |
+=============+=======+=======+===============================+
| ``Year``    | \*    | \*    | The year part of the date.    |
+-------------+-------+-------+-------------------------------+
| ``Month``   | \*    | \*    | The month part of the date.   |
+-------------+-------+-------+-------------------------------+
| ``Day``     | \*    | \*    | The day part of the date.     |
+-------------+-------+-------+-------------------------------+

The class **Date** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

A date consists of a day (of the month), a month (of the year) and a year. It is used to specify calendar dates.

The date class does not do any validation. Non-existing dates can be specified.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Date*
^^^^^^^^^^^^^^^^^^

``Date()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *Date*
^^^^^^^^^^^^^^^^^^

``Date(System.Int32 year, System.Int32 month, System.Int32 day)``

Standard constructor.

The constructor has the following parameters:

+-------------+--------------------+----------------------------+
| Parameter   | Type               | Description                |
+=============+====================+============================+
| ``year``    | ``System.Int32``   | The year.                  |
+-------------+--------------------+----------------------------+
| ``month``   | ``System.Int32``   | The month (of the year).   |
+-------------+--------------------+----------------------------+
| ``day``     | ``System.Int32``   | The day (of the month).    |
+-------------+--------------------+----------------------------+

Properties
~~~~~~~~~~

Property *Year*
^^^^^^^^^^^^^^^

``System.Int32 Year``

The year part of the date.

Property *Month*
^^^^^^^^^^^^^^^^

``System.Int32 Month``

The month part of the date.

Property *Day*
^^^^^^^^^^^^^^

``System.Int32 Day``

The day part of the date.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
