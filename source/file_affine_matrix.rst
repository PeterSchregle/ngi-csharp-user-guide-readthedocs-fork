Class *AffineMatrix*
--------------------

A matrix class useful for affine 2D geometric transformations.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **AffineMatrix** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``IEquatableAffineMatrix``   |
+------------------------------+
| ``ISerializable``            |
+------------------------------+

The class **AffineMatrix** contains the following properties:

+-------------------+-------+-------+----------------------------------------------------+
| Property          | Get   | Set   | Description                                        |
+===================+=======+=======+====================================================+
| ``M11``           | \*    |       | The element m11 (row 1, column 1) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M12``           | \*    |       | The element m12 (row 1, column 2) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M13``           | \*    |       | The element m13 (row 1, column 3) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M21``           | \*    |       | The element m21 (row 2, column 1) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M22``           | \*    |       | The element m22 (row 2, column 2) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M23``           | \*    |       | The element m23 (row 2, column 3) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M31``           | \*    |       | The element m31 (row 3, column 1) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M32``           | \*    |       | The element m32 (row 3, column 2) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``M33``           | \*    |       | The element m33 (row 3, column 3) of the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``[index]``       | \*    | \*    | The matrix values as a parametrized property.      |
+-------------------+-------+-------+----------------------------------------------------+
| ``Determinant``   | \*    |       | The determinant of the matrix.                     |
+-------------------+-------+-------+----------------------------------------------------+
| ``Trace``         | \*    |       | The trace of the matrix.                           |
+-------------------+-------+-------+----------------------------------------------------+
| ``Inverse``       | \*    |       | The inverse of the matrix.                         |
+-------------------+-------+-------+----------------------------------------------------+
| ``Translation``   | \*    |       | Retrieve the translation vector from the matrix.   |
+-------------------+-------+-------+----------------------------------------------------+
| ``Scaling``       | \*    |       | Retrieve the scaling vector from the matrix.       |
+-------------------+-------+-------+----------------------------------------------------+
| ``Rotation``      | \*    |       | Retrieve the rotation from the matrix.             |
+-------------------+-------+-------+----------------------------------------------------+

The class **AffineMatrix** contains the following methods:

+-----------------------+-----------------------------------------------------+
| Method                | Description                                         |
+=======================+=====================================================+
| ``ResetToIdentity``   | Reset the matrix to the identity matrix in place.   |
+-----------------------+-----------------------------------------------------+
| ``Scale``             | Scale the matrix in place.                          |
+-----------------------+-----------------------------------------------------+
| ``Translate``         | Translate the matrix in place.                      |
+-----------------------+-----------------------------------------------------+
| ``Rotate``            | Rotate the matrix in place.                         |
+-----------------------+-----------------------------------------------------+

Description
~~~~~~~~~~~

The matrix has the following form:

.. raw:: latex

   \begin{bmatrix}  m_1_1 & m_1_2 & m_1_3 \\  m_2_1 & m_2_2 & m_2_3 \\ 0 & 0 & 1  \end{bmatrix}

If the values are accessed in array form, the following equalities hold:

values[0] = m11

values[1] = m21

values[2] = m12

values[3] = m22

values[4] = m13

values[5] = m23

The size of the matrix is implicitely 3x3. Only the values of the first two rows are stored in the matrix. The values of the third row are fixed to 0, 0, 1.

The inverse of an affine matrix is still an affine matrix. TODO Serialization of objects to/from a data\_list.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *AffineMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``AffineMatrix()``

Default constructor.

Presets the matrix as the identity matrix: $ I={bmatrix} 1 & 0 & 0  0 & 1 & 0  0 & 0 & 1 {bmatrix} $

Constructors
~~~~~~~~~~~~

Constructor *AffineMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``AffineMatrix(System.Double m11, System.Double m12, System.Double m13, System.Double m21, System.Double m22, System.Double m23)``

Constructor.

The constructor has the following parameters:

+-------------+---------------------+--------------------+
| Parameter   | Type                | Description        |
+=============+=====================+====================+
| ``m11``     | ``System.Double``   | The element m11.   |
+-------------+---------------------+--------------------+
| ``m12``     | ``System.Double``   | The element m12.   |
+-------------+---------------------+--------------------+
| ``m13``     | ``System.Double``   | The element m13.   |
+-------------+---------------------+--------------------+
| ``m21``     | ``System.Double``   | The element m21.   |
+-------------+---------------------+--------------------+
| ``m22``     | ``System.Double``   | The element m22.   |
+-------------+---------------------+--------------------+
| ``m23``     | ``System.Double``   | The element m23.   |
+-------------+---------------------+--------------------+

Presets the matrix with specific values for the elements of the first two rows:

$ M={bmatrix} m\_1\_1 & m\_1\_2 & m\_1\_3  m\_2\_1 & m\_2\_2 & m\_2\_3 0 & 0 & 1 {bmatrix} $

Constructor *AffineMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``AffineMatrix(RotationMatrix rhs)``

Conversion constructor.

The constructor has the following parameters:

+-------------+----------------------+------------------------+
| Parameter   | Type                 | Description            |
+=============+======================+========================+
| ``rhs``     | ``RotationMatrix``   | The right hand side.   |
+-------------+----------------------+------------------------+

Converts a rotation matrix to an affine matrix.

Constructor *AffineMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``AffineMatrix(TranslationMatrix rhs)``

Conversion constructor.

The constructor has the following parameters:

+-------------+-------------------------+------------------------+
| Parameter   | Type                    | Description            |
+=============+=========================+========================+
| ``rhs``     | ``TranslationMatrix``   | The right hand side.   |
+-------------+-------------------------+------------------------+

Converts a translation matrix to an affine matrix.

Constructor *AffineMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``AffineMatrix(RigidMatrix rhs)``

Conversion constructor.

The constructor has the following parameters:

+-------------+-------------------+------------------------+
| Parameter   | Type              | Description            |
+=============+===================+========================+
| ``rhs``     | ``RigidMatrix``   | The right hand side.   |
+-------------+-------------------+------------------------+

Converts a rigid matrix to an affine matrix.

Constructor *AffineMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``AffineMatrix(IsotropicScalingMatrix rhs)``

Conversion constructor.

The constructor has the following parameters:

+-------------+------------------------------+------------------------+
| Parameter   | Type                         | Description            |
+=============+==============================+========================+
| ``rhs``     | ``IsotropicScalingMatrix``   | The right hand side.   |
+-------------+------------------------------+------------------------+

Converts an isotropic scaling matrix to an affine matrix.

Constructor *AffineMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``AffineMatrix(ScalingMatrix rhs)``

Conversion constructor.

The constructor has the following parameters:

+-------------+---------------------+------------------------+
| Parameter   | Type                | Description            |
+=============+=====================+========================+
| ``rhs``     | ``ScalingMatrix``   | The right hand side.   |
+-------------+---------------------+------------------------+

Converts a scaling matrix to an affine matrix.

Constructor *AffineMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``AffineMatrix(SimilarityMatrix rhs)``

Conversion constructor.

The constructor has the following parameters:

+-------------+------------------------+------------------------+
| Parameter   | Type                   | Description            |
+=============+========================+========================+
| ``rhs``     | ``SimilarityMatrix``   | The right hand side.   |
+-------------+------------------------+------------------------+

Converts a similarity matrix to an affine matrix.

Constructor *AffineMatrix*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``AffineMatrix(Pose rhs)``

Conversion constructor.

The constructor has the following parameters:

+-------------+------------+------------------------+
| Parameter   | Type       | Description            |
+=============+============+========================+
| ``rhs``     | ``Pose``   | The right hand side.   |
+-------------+------------+------------------------+

Converts a pose to an affine matrix.

Properties
~~~~~~~~~~

Property *M11*
^^^^^^^^^^^^^^

``System.Double M11``

The element m11 (row 1, column 1) of the matrix.

Property *M12*
^^^^^^^^^^^^^^

``System.Double M12``

The element m12 (row 1, column 2) of the matrix.

Property *M13*
^^^^^^^^^^^^^^

``System.Double M13``

The element m13 (row 1, column 3) of the matrix.

Property *M21*
^^^^^^^^^^^^^^

``System.Double M21``

The element m21 (row 2, column 1) of the matrix.

Property *M22*
^^^^^^^^^^^^^^

``System.Double M22``

The element m22 (row 2, column 2) of the matrix.

Property *M23*
^^^^^^^^^^^^^^

``System.Double M23``

The element m23 (row 2, column 3) of the matrix.

Property *M31*
^^^^^^^^^^^^^^

``System.Double M31``

The element m31 (row 3, column 1) of the matrix.

Property *M32*
^^^^^^^^^^^^^^

``System.Double M32``

The element m32 (row 3, column 2) of the matrix.

Property *M33*
^^^^^^^^^^^^^^

``System.Double M33``

The element m33 (row 3, column 3) of the matrix.

Property *[index]*
^^^^^^^^^^^^^^^^^^

``System.Double [index]``

The matrix values as a parametrized property.

This property allows you to retrieve the implicit values also, but you cannot set them.

Property *Determinant*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double Determinant``

The determinant of the matrix.

Property *Trace*
^^^^^^^^^^^^^^^^

``System.Double Trace``

The trace of the matrix.

Property *Inverse*
^^^^^^^^^^^^^^^^^^

``AffineMatrix Inverse``

The inverse of the matrix.

Property *Translation*
^^^^^^^^^^^^^^^^^^^^^^

``VectorDouble Translation``

Retrieve the translation vector from the matrix.

An affine transformation matrix can be composed of translation, scale, and rotation. This method retrieves the translation.

Property *Scaling*
^^^^^^^^^^^^^^^^^^

``VectorDouble Scaling``

Retrieve the scaling vector from the matrix.

An affine transformation matrix can be composed of translation, scale, and rotation. This method retrieves the scaling.

Property *Rotation*
^^^^^^^^^^^^^^^^^^^

``VectorDouble Rotation``

Retrieve the rotation from the matrix.

An affine transformation matrix can be composed of translation, scale, and rotation. This method retrieves the rotation. Normally, both rotation values should be the same or very close. If not, this indicates that there is considerable shear.

Static Methods
~~~~~~~~~~~~~~

Method *Multiply*
^^^^^^^^^^^^^^^^^

``AffineMatrix Multiply(AffineMatrix a, AffineMatrix b)``

Multiply two matrices.

The method **Multiply** has the following parameters:

+-------------+--------------------+---------------+
| Parameter   | Type               | Description   |
+=============+====================+===============+
| ``a``       | ``AffineMatrix``   |               |
+-------------+--------------------+---------------+
| ``b``       | ``AffineMatrix``   |               |
+-------------+--------------------+---------------+

Return the product of the two matrices.

Methods
~~~~~~~

Method *ResetToIdentity*
^^^^^^^^^^^^^^^^^^^^^^^^

``void ResetToIdentity()``

Reset the matrix to the identity matrix in place.

Returns a reference to this object.

Method *Scale*
^^^^^^^^^^^^^^

``void Scale(VectorDouble scaling)``

Scale the matrix in place.

The method **Scale** has the following parameters:

+---------------+--------------------+-----------------------+
| Parameter     | Type               | Description           |
+===============+====================+=======================+
| ``scaling``   | ``VectorDouble``   | The scaling vector.   |
+---------------+--------------------+-----------------------+

Return a reference to the matrix.

Method *Translate*
^^^^^^^^^^^^^^^^^^

``void Translate(VectorDouble translation)``

Translate the matrix in place.

The method **Translate** has the following parameters:

+-------------------+--------------------+---------------------------+
| Parameter         | Type               | Description               |
+===================+====================+===========================+
| ``translation``   | ``VectorDouble``   | The translation vector.   |
+-------------------+--------------------+---------------------------+

Return a reference to the matrix.

Method *Rotate*
^^^^^^^^^^^^^^^

``void Rotate(System.Double angle)``

Rotate the matrix in place.

The method **Rotate** has the following parameters:

+-------------+---------------------+-----------------------+
| Parameter   | Type                | Description           |
+=============+=====================+=======================+
| ``angle``   | ``System.Double``   | The rotation angle.   |
+-------------+---------------------+-----------------------+

Return a reference to the matrix.
