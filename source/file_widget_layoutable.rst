Class *WidgetLayoutable*
------------------------

The abstract widget\_layoutable type.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetLayoutable** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetLayoutable** contains the following properties:

+-----------------------+-------+-------+----------------------------------------------------------------------+
| Property              | Get   | Set   | Description                                                          |
+=======================+=======+=======+======================================================================+
| ``ContentBox``        | \*    | \*    | The content box of the widget as determined by the layout process.   |
+-----------------------+-------+-------+----------------------------------------------------------------------+
| ``Margin``            | \*    | \*    | The margin around the content.                                       |
+-----------------------+-------+-------+----------------------------------------------------------------------+
| ``BackgroundBrush``   | \*    | \*    | The background of the widget.                                        |
+-----------------------+-------+-------+----------------------------------------------------------------------+
| ``Clip``              | \*    | \*    | Clip drawing to the widget bounds.                                   |
+-----------------------+-------+-------+----------------------------------------------------------------------+
| ``LayoutTransform``   | \*    | \*    | The layout transform.                                                |
+-----------------------+-------+-------+----------------------------------------------------------------------+
| ``MarginBox``         | \*    |       | Get the margin box of the widget.                                    |
+-----------------------+-------+-------+----------------------------------------------------------------------+

Description
~~~~~~~~~~~

A widget\_layoutable is also interactive.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetLayoutable*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetLayoutable()``

Default constructor.

The default constructor creates a widget with default settings.

Properties
~~~~~~~~~~

Property *ContentBox*
^^^^^^^^^^^^^^^^^^^^^

``BoxDouble ContentBox``

The content box of the widget as determined by the layout process.

The layout() method must have been executed at least once in order for this property to be correct.

Property *Margin*
^^^^^^^^^^^^^^^^^

``ThicknessDouble Margin``

The margin around the content.

The margin is an outer border around a widget. A margin has left, top, right and bottom values that may all be different.

The margin is important when a widget is laid out within a container.

The default value of the margin is zero for all four values.

Property *BackgroundBrush*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``SolidColorBrushByte BackgroundBrush``

The background of the widget.

The background of the widget is specified with a brush.

The default value of the background is transparent black.

Property *Clip*
^^^^^^^^^^^^^^^

``System.Boolean Clip``

Clip drawing to the widget bounds.

Children of the widget are clipped within the widget bounds. The widget bounds are not defined within this abstract base class, but in each concrete widget class.

Property *LayoutTransform*
^^^^^^^^^^^^^^^^^^^^^^^^^^

``AffineMatrix LayoutTransform``

The layout transform.

Property *MarginBox*
^^^^^^^^^^^^^^^^^^^^

``BoxDouble MarginBox``

Get the margin box of the widget.

Starting with the layout\_box, the margin thickness is added. The resulting box is returned.
