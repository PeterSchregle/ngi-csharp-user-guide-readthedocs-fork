Class *Index3dEnumerator*
-------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Index3dEnumerator** implements the following interfaces:

+--------------------------+
| Interface                |
+==========================+
| ``IEnumerator``          |
+--------------------------+
| ``IEnumeratorIndex3d``   |
+--------------------------+

The class **Index3dEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **Index3dEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Index3d Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
