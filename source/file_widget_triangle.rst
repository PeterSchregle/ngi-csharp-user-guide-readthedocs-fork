Class *WidgetTriangle*
----------------------

A widget\_triangle displays a triangle on a surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetTriangle** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetTriangle** contains the following properties:

+----------------+-------+-------+-----------------------------------------+
| Property       | Get   | Set   | Description                             |
+================+=======+=======+=========================================+
| ``Position``   | \*    | \*    | The position of the widget\_triangle.   |
+----------------+-------+-------+-----------------------------------------+
| ``Outline``    | \*    | \*    | The outline of the widget\_triangle.    |
+----------------+-------+-------+-----------------------------------------+
| ``Fill``       | \*    | \*    | The fill of the widget\_triangle.       |
+----------------+-------+-------+-----------------------------------------+

Description
~~~~~~~~~~~

Various settings can be made for a widget\_triangle. These settings affect the look and the behavior of the widget\_triangle.

The widget\_triangle supports interactivity via the widget\_interactive base class, i.e. it can be selected, move around. In general it can react on mouse and keyboard interactions. A widget\_triangle has interactive widget capabilities.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetTriangle*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetTriangle()``

Default constructor.

The default constructor creates a widget with default settings. By default the points are all at the origin, the outline is one-pixel wide black and the fill is black transparent.

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``TriangleDouble Position``

The position of the widget\_triangle.

The position of a widget\_triangle is specified with a triangle<>.

Property *Outline*
^^^^^^^^^^^^^^^^^^

``PenByte Outline``

The outline of the widget\_triangle.

The outline of the widget\_triangle is specified with a pen<>.

Property *Fill*
^^^^^^^^^^^^^^^

``SolidColorBrushByte Fill``

The fill of the widget\_triangle.

The fill of the widget\_triangle is specified with a brush.
