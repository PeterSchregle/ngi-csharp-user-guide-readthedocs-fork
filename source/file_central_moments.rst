Class *CentralMoments*
----------------------

Class to hold central moments of the zeroth, second and third order.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **CentralMoments** implements the following interfaces:

+--------------------------------+
| Interface                      |
+================================+
| ``IEquatableCentralMoments``   |
+--------------------------------+
| ``ISerializable``              |
+--------------------------------+

The class **CentralMoments** contains the following properties:

+----------------------------------+-------+-------+----------------------------------------------------+
| Property                         | Get   | Set   | Description                                        |
+==================================+=======+=======+====================================================+
| ``Mu00``                         | \*    |       | The zeroth order horizontal central moment.        |
+----------------------------------+-------+-------+----------------------------------------------------+
| ``Mu20``                         | \*    |       | The second order horizontal central moment.        |
+----------------------------------+-------+-------+----------------------------------------------------+
| ``Mu11``                         | \*    |       | The second order mixed central moment.             |
+----------------------------------+-------+-------+----------------------------------------------------+
| ``Mu02``                         | \*    |       | The second order vertical central moment.          |
+----------------------------------+-------+-------+----------------------------------------------------+
| ``Mu30``                         | \*    |       | The third order horizontal central moment.         |
+----------------------------------+-------+-------+----------------------------------------------------+
| ``Mu21``                         | \*    |       | The third order horizontal mixed central moment.   |
+----------------------------------+-------+-------+----------------------------------------------------+
| ``Mu12``                         | \*    |       | The third order vertical mixed central moment.     |
+----------------------------------+-------+-------+----------------------------------------------------+
| ``Mu03``                         | \*    |       | The third order vertical central moment.           |
+----------------------------------+-------+-------+----------------------------------------------------+
| ``EquivalentEllipseRadius``      | \*    |       | The radii of an equivalent ellipse.                |
+----------------------------------+-------+-------+----------------------------------------------------+
| ``EquivalentEllipseDirection``   | \*    |       | The angle of an equivalent ellipse.                |
+----------------------------------+-------+-------+----------------------------------------------------+

The class **CentralMoments** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

For a nice and short treatment of moments see Chaur-Chin Chen, Improved Moment Invariants for Shape Discrimination [1992].

Moments are a sort of weighted averages of the image pixels' intensities (the pixel intensities may be set to 1 in case of binary moments). They are useful to describe segmented objects. Simple properties of objects found via moments include area (or total intensity), its centroid, and information about its orientation.

The central\_moments are invariant with respect to translation and scaling, but not with rotation.

The zeroth and first order central moments are not contained in this class, because they are 1 by definition.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *CentralMoments*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``CentralMoments()``

Default constructor.

Constructors
~~~~~~~~~~~~

Constructor *CentralMoments*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``CentralMoments(Moments m)``

Standard constructor.

The constructor has the following parameters:

+-------------+---------------+---------------------------+
| Parameter   | Type          | Description               |
+=============+===============+===========================+
| ``m``       | ``Moments``   | Non-normalized moments.   |
+-------------+---------------+---------------------------+

Initializes the central moments by calculating it from raw moments.

Properties
~~~~~~~~~~

Property *Mu00*
^^^^^^^^^^^^^^^

``System.Double Mu00``

The zeroth order horizontal central moment.

Property *Mu20*
^^^^^^^^^^^^^^^

``System.Double Mu20``

The second order horizontal central moment.

Property *Mu11*
^^^^^^^^^^^^^^^

``System.Double Mu11``

The second order mixed central moment.

Property *Mu02*
^^^^^^^^^^^^^^^

``System.Double Mu02``

The second order vertical central moment.

Property *Mu30*
^^^^^^^^^^^^^^^

``System.Double Mu30``

The third order horizontal central moment.

Property *Mu21*
^^^^^^^^^^^^^^^

``System.Double Mu21``

The third order horizontal mixed central moment.

Property *Mu12*
^^^^^^^^^^^^^^^

``System.Double Mu12``

The third order vertical mixed central moment.

Property *Mu03*
^^^^^^^^^^^^^^^

``System.Double Mu03``

The third order vertical central moment.

Property *EquivalentEllipseRadius*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``VectorDouble EquivalentEllipseRadius``

The radii of an equivalent ellipse.

The object is treated as if it were an ellipse with the same moments, and the radii of such an ellipse are calculated.

Property *EquivalentEllipseDirection*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``Direction EquivalentEllipseDirection``

The angle of an equivalent ellipse.

The object is treated as if it were an ellipse with the same moments, and the angle of such an ellipse is calculated.

Static Methods
~~~~~~~~~~~~~~

Method *CalculateMu20*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateMu20(Moments m)``

Calculate mu20 from moments.

The method **CalculateMu20** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``m``       | ``Moments``   |               |
+-------------+---------------+---------------+

mu20 is is a second order central moment.

The mu20 central moment.

Method *CalculateMu11*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateMu11(Moments m)``

Calculate mu11 from moments.

The method **CalculateMu11** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``m``       | ``Moments``   |               |
+-------------+---------------+---------------+

mu11 is is a second order central moment.

The mu11 central moment.

Method *CalculateMu02*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateMu02(Moments m)``

Calculate mu02 from moments.

The method **CalculateMu02** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``m``       | ``Moments``   |               |
+-------------+---------------+---------------+

mu02 is is a second order central moment.

The mu02 central moment.

Method *CalculateMu30*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateMu30(Moments m)``

Calculate mu30 from moments.

The method **CalculateMu30** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``m``       | ``Moments``   |               |
+-------------+---------------+---------------+

mu30 is is a third order central moment.

The mu30 central moment.

Method *CalculateMu21*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateMu21(Moments m)``

Calculate mu21 from moments.

The method **CalculateMu21** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``m``       | ``Moments``   |               |
+-------------+---------------+---------------+

mu21 is is a third order central moment.

The mu21 central moment.

Method *CalculateMu12*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateMu12(Moments m)``

Calculate mu12 from moments.

The method **CalculateMu12** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``m``       | ``Moments``   |               |
+-------------+---------------+---------------+

mu12 is is a third order central moment.

The mu12 central moment.

Method *CalculateMu03*
^^^^^^^^^^^^^^^^^^^^^^

``System.Double CalculateMu03(Moments m)``

Calculate mu03 from moments.

The method **CalculateMu03** has the following parameters:

+-------------+---------------+---------------+
| Parameter   | Type          | Description   |
+=============+===============+===============+
| ``m``       | ``Moments``   |               |
+-------------+---------------+---------------+

mu03 is is a third order central moment.

The mu03 central moment.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.
