Class *OcrReader*
-----------------

**Namespace:** Ngi

**Module:** OcrOcv

The class **OcrReader** contains the following methods:

+----------------+--------------------------+
| Method         | Description              |
+================+==========================+
| ``ReadText``   | Read text in an image.   |
+----------------+--------------------------+

Description
~~~~~~~~~~~

Constructors
~~~~~~~~~~~~

Constructor *OcrReader*
^^^^^^^^^^^^^^^^^^^^^^^

``OcrReader(System.String dataDirectory)``

Standard constructor.

The constructor has the following parameters:

+---------------------+---------------------+---------------------------------------------------+
| Parameter           | Type                | Description                                       |
+=====================+=====================+===================================================+
| ``dataDirectory``   | ``System.String``   | The parent of "tessdata" with a terminating "/"   |
+---------------------+---------------------+---------------------------------------------------+

Methods
~~~~~~~

Method *ReadText*
^^^^^^^^^^^^^^^^^

``System.String ReadText(ViewLocatorByte imageData)``

Read text in an image.

The method **ReadText** has the following parameters:

+-----------------+-----------------------+---------------+
| Parameter       | Type                  | Description   |
+=================+=======================+===============+
| ``imageData``   | ``ViewLocatorByte``   |               |
+-----------------+-----------------------+---------------+
