Class *Ellipse*
---------------

The geometric ellipse primitive.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Ellipse** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``IEquatableEllipse``        |
+------------------------------+
| ``ISerializable``            |
+------------------------------+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **Ellipse** contains the following properties:

+---------------------+-------+-------+----------------------------------------+
| Property            | Get   | Set   | Description                            |
+=====================+=======+=======+========================================+
| ``Center``          | \*    | \*    | The center of the ellipse.             |
+---------------------+-------+-------+----------------------------------------+
| ``Radius``          | \*    | \*    | The radius of the ellipse.             |
+---------------------+-------+-------+----------------------------------------+
| ``Diameter``        | \*    |       | The diameter of the ellipse.           |
+---------------------+-------+-------+----------------------------------------+
| ``Direction``       | \*    | \*    | The direction of the ellipse.          |
+---------------------+-------+-------+----------------------------------------+
| ``Area``            | \*    |       | The area of the ellipse.               |
+---------------------+-------+-------+----------------------------------------+
| ``Circumference``   | \*    |       | The circumference of the circle.       |
+---------------------+-------+-------+----------------------------------------+
| ``BoundingBox``     | \*    |       | Get the bounding box of the ellipse.   |
+---------------------+-------+-------+----------------------------------------+
| ``Eccentricity``    | \*    |       | The eccentricity of the ellipse.       |
+---------------------+-------+-------+----------------------------------------+
| ``Anisotropy``      | \*    |       | The anisotropy of the ellipse.         |
+---------------------+-------+-------+----------------------------------------+

The class **Ellipse** contains the following methods:

+------------------------+---------------------------------------------------------+
| Method                 | Description                                             |
+========================+=========================================================+
| ``ToString``           | Provide string representation for debugging purposes.   |
+------------------------+---------------------------------------------------------+
| ``Normalize``          | Normalize an ellipse.                                   |
+------------------------+---------------------------------------------------------+
| ``NormalizeInPlace``   | Normalize an ellipse.                                   |
+------------------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

An ellipse can be seen as a point at the center combined with a vector in two-dimensional space that gives the two radii, plus an additional rotation angle given in the form of a direction that rotates the whole ellipse around its center.

The following operations are implemented: operator == : comparison for equality. operator != : comparison for inequality.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Ellipse*
^^^^^^^^^^^^^^^^^^^^^

``Ellipse()``

Default constructor.

Construct an ellipse from a center, a radius vector and a rotation angle.

Constructors
~~~~~~~~~~~~

Constructor *Ellipse*
^^^^^^^^^^^^^^^^^^^^^

``Ellipse(PointDouble center, VectorDouble radius, Direction angle)``

Constructs specific ellipse from circle (conversion).

The constructor has the following parameters:

+--------------+--------------------+---------------+
| Parameter    | Type               | Description   |
+==============+====================+===============+
| ``center``   | ``PointDouble``    |               |
+--------------+--------------------+---------------+
| ``radius``   | ``VectorDouble``   |               |
+--------------+--------------------+---------------+
| ``angle``    | ``Direction``      |               |
+--------------+--------------------+---------------+

Properties
~~~~~~~~~~

Property *Center*
^^^^^^^^^^^^^^^^^

``PointDouble Center``

The center of the ellipse.

Property *Radius*
^^^^^^^^^^^^^^^^^

``VectorDouble Radius``

The radius of the ellipse.

Property *Diameter*
^^^^^^^^^^^^^^^^^^^

``VectorDouble Diameter``

The diameter of the ellipse.

Property *Direction*
^^^^^^^^^^^^^^^^^^^^

``Direction Direction``

The direction of the ellipse.

Property *Area*
^^^^^^^^^^^^^^^

``System.Double Area``

The area of the ellipse.

Property *Circumference*
^^^^^^^^^^^^^^^^^^^^^^^^

``System.Double Circumference``

The circumference of the circle.

The ellipse circumference is difficult to calculate. We use the Ramanujan approximation formula here.

Property *BoundingBox*
^^^^^^^^^^^^^^^^^^^^^^

``BoxDouble BoundingBox``

Get the bounding box of the ellipse.

The bounding box is an axis aligned box that bounds the ellipse This follows part of the algorithm published here: http://fridrich.blogspot.de/2011/06/bounding-box-of-svg-elliptical-arc.html

It uses the parametric description of an ellipse: x(theta) = cx + rx\ *cos(theta)*\ cos(alpha) - ry\ *sin(theta)*\ sin(alpha) y(theta) = cy + rx\ *cos(theta)*\ sin(alpha) + ry\ *sin(theta)*\ cos(alpha)

where cx and cy are the coordinates of the centre of the ellipse, rx and ry are the radii and alpha is the x-axis-rotation.

The extreme x values are found for theta\_x1 = -atan(ry*tan(alpha)/rx) and theta\_x2 = M\_PI -atan(ry*\ tan(alpha)/rx)

The extreme y values are found for theta\_y1 = atan(ry/(tan(alpha)\ *rx)) and theta\_y2 = M\_PI + atan(ry/(tan(alpha)*\ rx)) The bounding box of the ellipse.

Property *Eccentricity*
^^^^^^^^^^^^^^^^^^^^^^^

``System.Double Eccentricity``

The eccentricity of the ellipse.

Property *Anisotropy*
^^^^^^^^^^^^^^^^^^^^^

``System.Double Anisotropy``

The anisotropy of the ellipse.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Method *Normalize*
^^^^^^^^^^^^^^^^^^

``Ellipse Normalize()``

Normalize an ellipse.

A normalized ellipse has positive radiii.

This method checks these conditions and returns a normalized variant of the ellipse.

Method *NormalizeInPlace*
^^^^^^^^^^^^^^^^^^^^^^^^^

``void NormalizeInPlace()``

Normalize an ellipse.

A normalized ellipse has positive radiii.

This method checks these conditions and returns a normalized variant of the ellipse. It modifies the ellipse in place.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO no brief description for variant

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO no description for variant
