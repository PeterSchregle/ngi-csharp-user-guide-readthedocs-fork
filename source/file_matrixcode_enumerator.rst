Class *MatrixcodeEnumerator*
----------------------------

**Namespace:** Ngi

**Module:** BarcodeMatrixcode

The class **MatrixcodeEnumerator** implements the following interfaces:

+-----------------------------+
| Interface                   |
+=============================+
| ``IEnumerator``             |
+-----------------------------+
| ``IEnumeratorMatrixcode``   |
+-----------------------------+

The class **MatrixcodeEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **MatrixcodeEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``Matrixcode Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
