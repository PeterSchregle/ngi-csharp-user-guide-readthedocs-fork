Class *SerialPort*
------------------

The serial port.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **SerialPort** contains the following properties:

+------------+-------+-------+------------------------------------------+
| Property   | Get   | Set   | Description                              |
+============+=======+=======+==========================================+
| ``Port``   | \*    |       | The port specifier (COM1, COM2, etc.).   |
+------------+-------+-------+------------------------------------------+

The class **SerialPort** contains the following enumerations:

+----------------+------------------------------+
| Enumeration    | Description                  |
+================+==============================+
| ``Parity``     | TODO documentation missing   |
+----------------+------------------------------+
| ``StopBits``   | TODO documentation missing   |
+----------------+------------------------------+

Description
~~~~~~~~~~~

Constructors
~~~~~~~~~~~~

Constructor *SerialPort*
^^^^^^^^^^^^^^^^^^^^^^^^

``SerialPort(System.String port, System.UInt32 baudrate, System.UInt32 byteSize, SerialPort.Parity parity, SerialPort.StopBits stopBits, System.UInt32 readTimeout)``

Construct the Serial port.

The constructor has the following parameters:

+-------------------+---------------------------+-------------------------------------------------+
| Parameter         | Type                      | Description                                     |
+===================+===========================+=================================================+
| ``port``          | ``System.String``         | The port specifier (COM1, COM2, etc.).          |
+-------------------+---------------------------+-------------------------------------------------+
| ``baudrate``      | ``System.UInt32``         | Baudrate for the serial port. (bps)             |
+-------------------+---------------------------+-------------------------------------------------+
| ``byteSize``      | ``System.UInt32``         |                                                 |
+-------------------+---------------------------+-------------------------------------------------+
| ``parity``        | ``SerialPort.Parity``     | Parity. Does not enable that parity checking.   |
+-------------------+---------------------------+-------------------------------------------------+
| ``stopBits``      | ``SerialPort.StopBits``   |                                                 |
+-------------------+---------------------------+-------------------------------------------------+
| ``readTimeout``   | ``System.UInt32``         |                                                 |
+-------------------+---------------------------+-------------------------------------------------+

Properties
~~~~~~~~~~

Property *Port*
^^^^^^^^^^^^^^^

``System.String Port``

The port specifier (COM1, COM2, etc.).

Enumerations
~~~~~~~~~~~~

Enumeration *Parity*
^^^^^^^^^^^^^^^^^^^^

``enum Parity``

TODO documentation missing

The enumeration **Parity** has the following constants:

+-------------+---------+---------------+
| Name        | Value   | Description   |
+=============+=========+===============+
| ``no``      | ``0``   |               |
+-------------+---------+---------------+
| ``odd``     | ``1``   |               |
+-------------+---------+---------------+
| ``even``    | ``2``   |               |
+-------------+---------+---------------+
| ``mark``    | ``3``   |               |
+-------------+---------+---------------+
| ``space``   | ``4``   |               |
+-------------+---------+---------------+

::

    enum Parity
    {
      no = 0,
      odd = 1,
      even = 2,
      mark = 3,
      space = 4,
    };

TODO documentation missing

Enumeration *StopBits*
^^^^^^^^^^^^^^^^^^^^^^

``enum StopBits``

TODO documentation missing

The enumeration **StopBits** has the following constants:

+--------------------+---------+---------------+
| Name               | Value   | Description   |
+====================+=========+===============+
| ``one``            | ``0``   |               |
+--------------------+---------+---------------+
| ``onePointFive``   | ``1``   |               |
+--------------------+---------+---------------+
| ``two``            | ``2``   |               |
+--------------------+---------+---------------+

::

    enum StopBits
    {
      one = 0,
      onePointFive = 1,
      two = 2,
    };

TODO documentation missing
