Class *CameraInfoEnumerator*
----------------------------

**Namespace:** Ngi

**Module:** Camera

The class **CameraInfoEnumerator** implements the following interfaces:

+-----------------------------+
| Interface                   |
+=============================+
| ``IEnumerator``             |
+-----------------------------+
| ``IEnumeratorCameraInfo``   |
+-----------------------------+

The class **CameraInfoEnumerator** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Current``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **CameraInfoEnumerator** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``Reset``      |               |
+----------------+---------------+
| ``MoveNext``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Properties
~~~~~~~~~~

Property *Current*
^^^^^^^^^^^^^^^^^^

``CameraInfo Current``

Methods
~~~~~~~

Method *Reset*
^^^^^^^^^^^^^^

``void Reset()``

Method *MoveNext*
^^^^^^^^^^^^^^^^^

``System.Boolean MoveNext()``
