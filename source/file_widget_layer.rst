Class *WidgetLayer*
-------------------

A widget\_layer implements a layer on a surface.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **WidgetLayer** implements the following interfaces:

+------------------------------+
| Interface                    |
+==============================+
| ``INotifyPropertyChanged``   |
+------------------------------+

The class **WidgetLayer** contains the following properties:

+----------------+-------+-------+------------------------------+
| Property       | Get   | Set   | Description                  |
+================+=======+=======+==============================+
| ``Position``   | \*    | \*    | The position of the layer.   |
+----------------+-------+-------+------------------------------+

Description
~~~~~~~~~~~

A layer doesn't do any drawing on it's own (besides paiting itself with the background\_brush), it This widget supports layout.

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *WidgetLayer*
^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetLayer()``

Default constructor.

By default the layer is of zero width at the origin.

Constructors
~~~~~~~~~~~~

Constructor *WidgetLayer*
^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetLayer(BoxDouble position)``

Constructor.

The constructor has the following parameters:

+----------------+-----------------+--------------------------+
| Parameter      | Type            | Description              |
+================+=================+==========================+
| ``position``   | ``BoxDouble``   | The size of the layer.   |
+----------------+-----------------+--------------------------+

The constructor creates a layer at a specific position.

Constructor *WidgetLayer*
^^^^^^^^^^^^^^^^^^^^^^^^^

``WidgetLayer(VectorDouble size)``

Constructor.

The constructor has the following parameters:

+-------------+--------------------+--------------------------+
| Parameter   | Type               | Description              |
+=============+====================+==========================+
| ``size``    | ``VectorDouble``   | The size of the layer.   |
+-------------+--------------------+--------------------------+

The constructor creates a layer of a specific size at position (0, 0).

Properties
~~~~~~~~~~

Property *Position*
^^^^^^^^^^^^^^^^^^^

``BoxDouble Position``

The position of the layer.
