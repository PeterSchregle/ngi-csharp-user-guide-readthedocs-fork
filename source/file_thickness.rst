Class *Thickness*
-----------------

The thickness describes the thickness of a frame around a rectangle.

**Namespace:** Ngi

**Module:** ImageProcessing

The class **Thickness** implements the following interfaces:

+-----------------------------------------------------+
| Interface                                           |
+=====================================================+
| ``IEquatableThicknessThicknessCoordinateVariant``   |
+-----------------------------------------------------+
| ``ISerializable``                                   |
+-----------------------------------------------------+
| ``INotifyPropertyChanged``                          |
+-----------------------------------------------------+

The class **Thickness** contains the following variant parameters:

+------------------+-----------------------------------------+
| Variant          | Description                             |
+==================+=========================================+
| ``Coordinate``   | TODO no brief description for variant   |
+------------------+-----------------------------------------+

The class **Thickness** contains the following properties:

+--------------+-------+-------+---------------------------------------------+
| Property     | Get   | Set   | Description                                 |
+==============+=======+=======+=============================================+
| ``Left``     | \*    | \*    | The left side of the thickness.             |
+--------------+-------+-------+---------------------------------------------+
| ``Top``      | \*    | \*    | The top side of the thickness.              |
+--------------+-------+-------+---------------------------------------------+
| ``Right``    | \*    | \*    | The right side of the thickness.            |
+--------------+-------+-------+---------------------------------------------+
| ``Bottom``   | \*    | \*    | The bottom side of the thickness.           |
+--------------+-------+-------+---------------------------------------------+
| ``Size``     | \*    |       | The size corresponding to this thickness.   |
+--------------+-------+-------+---------------------------------------------+

The class **Thickness** contains the following methods:

+----------------+---------------------------------------------------------+
| Method         | Description                                             |
+================+=========================================================+
| ``ToString``   | Provide string representation for debugging purposes.   |
+----------------+---------------------------------------------------------+

Description
~~~~~~~~~~~

Four values describe the left, top, right and bottom sides of the rectangle, respectively. A thickness is used as margin or padding.

Variants
~~~~~~~~

Variant *Coordinate*
^^^^^^^^^^^^^^^^^^^^

TODO no brief description for variant

The variant parameter **Coordinate** has the following types:

+--------------+
| Type         |
+==============+
| ``Int32``    |
+--------------+
| ``Double``   |
+--------------+

The full type of the concrete class can be built by appending the variant type after the class name.

TODO no description for variant

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *Thickness*
^^^^^^^^^^^^^^^^^^^^^^^

``Thickness()``

Constructs a thickness with the same value for all sides.

If no parameter is passed, a zero thickness is constructed.

If a parameter is passed, all four thickness sides are set to this value.

Constructors
~~~~~~~~~~~~

Constructor *Thickness*
^^^^^^^^^^^^^^^^^^^^^^^

``Thickness(System.Object all)``

Constructs a thickness with different values for all sides.

The constructor has the following parameters:

+-------------+---------------------+---------------+
| Parameter   | Type                | Description   |
+=============+=====================+===============+
| ``all``     | ``System.Object``   |               |
+-------------+---------------------+---------------+

All four thickness sides are set to specific values.

Constructor *Thickness*
^^^^^^^^^^^^^^^^^^^^^^^

``Thickness(System.Object left, System.Object top, System.Object right, System.Object bottom)``

Copy construct another thickness.

The constructor has the following parameters:

+--------------+---------------------+---------------+
| Parameter    | Type                | Description   |
+==============+=====================+===============+
| ``left``     | ``System.Object``   |               |
+--------------+---------------------+---------------+
| ``top``      | ``System.Object``   |               |
+--------------+---------------------+---------------+
| ``right``    | ``System.Object``   |               |
+--------------+---------------------+---------------+
| ``bottom``   | ``System.Object``   |               |
+--------------+---------------------+---------------+

Properties
~~~~~~~~~~

Property *Left*
^^^^^^^^^^^^^^^

``System.Object Left``

The left side of the thickness.

Property *Top*
^^^^^^^^^^^^^^

``System.Object Top``

The top side of the thickness.

Property *Right*
^^^^^^^^^^^^^^^^

``System.Object Right``

The right side of the thickness.

Property *Bottom*
^^^^^^^^^^^^^^^^^

``System.Object Bottom``

The bottom side of the thickness.

Property *Size*
^^^^^^^^^^^^^^^

``Vector Size``

The size corresponding to this thickness.

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``

Provide string representation for debugging purposes.

Events
~~~~~~

Event *PropertyChanged*
^^^^^^^^^^^^^^^^^^^^^^^

``void PropertyChanged(System.String propertyName)``

TODO documentation missing

The event **PropertyChanged** has the following parameters:

+--------------------+---------------------+---------------+
| Parameter          | Type                | Description   |
+====================+=====================+===============+
| ``propertyName``   | ``System.String``   |               |
+--------------------+---------------------+---------------+

TODO documentation missing
