Class *DtOpenlayersSystem*
--------------------------

**Namespace:** Ngi

**Module:** ImageProcessing

The class **DtOpenlayersSystem** contains the following properties:

+---------------+-------+-------+---------------+
| Property      | Get   | Set   | Description   |
+===============+=======+=======+===============+
| ``Version``   | \*    |       |               |
+---------------+-------+-------+---------------+
| ``Modules``   | \*    |       |               |
+---------------+-------+-------+---------------+

The class **DtOpenlayersSystem** contains the following methods:

+----------------+---------------+
| Method         | Description   |
+================+===============+
| ``ToString``   |               |
+----------------+---------------+

Description
~~~~~~~~~~~

Default Constructor
~~~~~~~~~~~~~~~~~~~

Constructor *DtOpenlayersSystem*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``DtOpenlayersSystem()``

Properties
~~~~~~~~~~

Property *Version*
^^^^^^^^^^^^^^^^^^

``System.String Version``

Property *Modules*
^^^^^^^^^^^^^^^^^^

``StringList Modules``

Methods
~~~~~~~

Method *ToString*
^^^^^^^^^^^^^^^^^

``System.String ToString()``
